<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="auto">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-866d50d.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-866d50d.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-866d50d.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-866d50d.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kickass.to/sc-866d50d.js');

        sc('setHost', 'a.kickass.to');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '866d50d',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-866d50d.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div  data-sc-slot="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div  data-sc-slot="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka ka-settings"></i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka ka-search"></i></button></div>
			</form>
		</div>
        <div  data-sc-slot="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag7">1080p</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/adele/" class="tag2">adele</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/apk/" class="tag2">apk</a>
	<a href="/search/arrow/" class="tag2">arrow</a>
	<a href="/search/big%20bang%20theory/" class="tag2">big bang theory</a>
	<a href="/search/christmas/" class="tag4">christmas</a>
	<a href="/search/creed/" class="tag2">creed</a>
	<a href="/search/crimson%20peak/" class="tag2">crimson peak</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/dual%20audio%20hindi/" class="tag3">dual audio hindi</a>
	<a href="/search/elementary/" class="tag2">elementary</a>
	<a href="/search/empire/" class="tag2">empire</a>
	<a href="/search/etrg/" class="tag3">etrg</a>
	<a href="/search/french/" class="tag3">french</a>
	<a href="/search/goosebumps/" class="tag2">goosebumps</a>
	<a href="/search/hindi/" class="tag10">hindi</a>
	<a href="/search/hindi%202015/" class="tag6">hindi 2015</a>
	<a href="/search/is%20safe%201/" class="tag5">is safe 1</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/legend/" class="tag2">legend</a>
	<a href="/search/limitless/" class="tag2">limitless</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/one%20punch%20man/" class="tag2">one punch man</a>
	<a href="/search/prem%20ratan%20dhan%20payo/" class="tag2">prem ratan dhan payo</a>
	<a href="/search/sicario/" class="tag2">sicario</a>
	<a href="/search/south%20park/" class="tag2">south park</a>
	<a href="/search/spectre/" class="tag3">spectre</a>
	<a href="/search/star%20wars/" class="tag4">star wars</a>
	<a href="/search/tamasha/" class="tag2">tamasha</a>
	<a href="/search/tamil/" class="tag5">tamil</a>
	<a href="/search/telugu/" class="tag4">telugu</a>
	<a href="/search/telugu%202015/" class="tag3">telugu 2015</a>
	<a href="/search/the%20big%20bang%20theory/" class="tag2">the big bang theory</a>
	<a href="/search/the%20big%20bang%20theory%20s09e10/" class="tag2">the big bang theory s09e10</a>
	<a href="/search/the%20flash/" class="tag2">the flash</a>
	<a href="/search/the%20martian/" class="tag4">the martian</a>
	<a href="/search/the%20walking%20dead/" class="tag4">the walking dead</a>
	<a href="/search/ufc/" class="tag2">ufc</a>
	<a href="/search/walking%20dead/" class="tag2">walking dead</a>
	<a href="/search/windows/" class="tag3">windows</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11723663,0" class="icommentjs kaButton smallButton rightButton" href="/pan-2015-brrip-xvid-etrg-t11723663.html#comment">208 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/pan-2015-brrip-xvid-etrg-t11723663.html" class="cellMainLink">Pan 2015 BRRip XViD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="740814519">706.5 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T23:34:44+00:00">08 Dec 2015, 23:34:44</span></td>
			<td class="green center">16585</td>
			<td class="red lasttd center">9009</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11727424,0" class="icommentjs kaButton smallButton rightButton" href="/riot-2015-truefrench-dvdrip-xvid-slay3r-filoumoutonrip-t11727424.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/riot-2015-truefrench-dvdrip-xvid-slay3r-filoumoutonrip-t11727424.html" class="cellMainLink">Riot 2015 TRUEFRENCH DVDRiP XviD-Slay3R filoumoutonRIP</a></div>
			</td>
			<td class="nobr center" data-sort="735627777">701.55 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T16:18:27+00:00">09 Dec 2015, 16:18:27</span></td>
			<td class="green center">12729</td>
			<td class="red lasttd center">3336</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743263,0" class="icommentjs kaButton smallButton rightButton" href="/you-call-it-passion-2015-720p-hdrip-h264-cinefox-movietam-t11743263.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/you-call-it-passion-2015-720p-hdrip-h264-cinefox-movietam-t11743263.html" class="cellMainLink">You Call It Passion 2015 720p HDRip H264-CINEFOX [MovietaM]</a></div>
			</td>
			<td class="nobr center" data-sort="2613390864">2.43 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T12:37:46+00:00">12 Dec 2015, 12:37:46</span></td>
			<td class="green center">11721</td>
			<td class="red lasttd center">2750</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11729507,0" class="icommentjs kaButton smallButton rightButton" href="/the-martian-2015-540p-hdrip-korsub-x264-aac2-0-fgt-t11729507.html#comment">137 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-martian-2015-540p-hdrip-korsub-x264-aac2-0-fgt-t11729507.html" class="cellMainLink">The Martian 2015 540p HDRip KORSUB x264 AAC2 0-FGT</a></div>
			</td>
			<td class="nobr center" data-sort="2348413458">2.19 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T21:57:33+00:00">09 Dec 2015, 21:57:33</span></td>
			<td class="green center">9638</td>
			<td class="red lasttd center">4538</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739205,0" class="icommentjs kaButton smallButton rightButton" href="/the-ridiculous-6-2015-webrip-x264-qcf-t11739205.html#comment">32 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-ridiculous-6-2015-webrip-x264-qcf-t11739205.html" class="cellMainLink">The Ridiculous 6 2015 WEBRiP x264-QCF</a></div>
			</td>
			<td class="nobr center" data-sort="818813350">780.88 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T16:42:01+00:00">11 Dec 2015, 16:42:01</span></td>
			<td class="green center">8180</td>
			<td class="red lasttd center">5224</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11733058,0" class="icommentjs kaButton smallButton rightButton" href="/crimson-peak-2015-hdrip-xvid-etrg-t11733058.html#comment">63 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/crimson-peak-2015-hdrip-xvid-etrg-t11733058.html" class="cellMainLink">Crimson Peak 2015 HDRip XviD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="742933507">708.52 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T13:27:37+00:00">10 Dec 2015, 13:27:37</span></td>
			<td class="green center">7673</td>
			<td class="red lasttd center">5232</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11732425,0" class="icommentjs kaButton smallButton rightButton" href="/the-wave-2015-norwegian-1080p-bluray-x264-dts-jyk-t11732425.html#comment">28 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-wave-2015-norwegian-1080p-bluray-x264-dts-jyk-t11732425.html" class="cellMainLink">The Wave 2015 NORWEGIAN 1080p BluRay x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2837834959">2.64 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T10:48:34+00:00">10 Dec 2015, 10:48:34</span></td>
			<td class="green center">6307</td>
			<td class="red lasttd center">3612</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738001,0" class="icommentjs kaButton smallButton rightButton" href="/heist-2015-brrip-xvid-ac3-evo-t11738001.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/heist-2015-brrip-xvid-ac3-evo-t11738001.html" class="cellMainLink">Heist 2015 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1442073624">1.34 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T11:28:11+00:00">11 Dec 2015, 11:28:11</span></td>
			<td class="green center">4371</td>
			<td class="red lasttd center">5952</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/heist-2015-truefrench-dvdrip-xvid-avitech-filoumoutonrip-t11740859.html" class="cellMainLink">Heist 2015 TRUEFRENCH DVDRiP XViD-AViTECH filoumoutonRIP</a></div>
			</td>
			<td class="nobr center" data-sort="734296941">700.28 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T23:52:45+00:00">11 Dec 2015, 23:52:45</span></td>
			<td class="green center">4924</td>
			<td class="red lasttd center">2341</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11723431,0" class="icommentjs kaButton smallButton rightButton" href="/pan-2015-720p-brrip-x264-aac-etrg-t11723431.html#comment">105 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/pan-2015-720p-brrip-x264-aac-etrg-t11723431.html" class="cellMainLink">Pan 2015 720p BRRip x264 AAC-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="877244179">836.61 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T22:17:40+00:00">08 Dec 2015, 22:17:40</span></td>
			<td class="green center">5097</td>
			<td class="red lasttd center">1839</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739765,0" class="icommentjs kaButton smallButton rightButton" href="/american-hero-2015-1080p-web-dl-dd5-1-h264-rarbg-t11739765.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/american-hero-2015-1080p-web-dl-dd5-1-h264-rarbg-t11739765.html" class="cellMainLink">American Hero 2015 1080p WEB-DL DD5 1 H264-RARBG</a></div>
			</td>
			<td class="nobr center" data-sort="3488674243">3.25 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T18:45:03+00:00">11 Dec 2015, 18:45:03</span></td>
			<td class="green center">2713</td>
			<td class="red lasttd center">6240</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743273,0" class="icommentjs kaButton smallButton rightButton" href="/the-sound-of-a-flower-2015-720p-hdrip-h264-aac-pchd-movietam-t11743273.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-sound-of-a-flower-2015-720p-hdrip-h264-aac-pchd-movietam-t11743273.html" class="cellMainLink">The Sound Of A Flower 2015 720p HDRip H264 AAC-PCHD [MovietaM]</a></div>
			</td>
			<td class="nobr center" data-sort="2663095205">2.48 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T12:39:35+00:00">12 Dec 2015, 12:39:35</span></td>
			<td class="green center">4604</td>
			<td class="red lasttd center">1288</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11741209,0" class="icommentjs kaButton smallButton rightButton" href="/life-of-josutty-2015-malayalam-dvdrip-x264-aac-5-1-e-subs-mbrhdrg-t11741209.html#comment">10 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/life-of-josutty-2015-malayalam-dvdrip-x264-aac-5-1-e-subs-mbrhdrg-t11741209.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/life-of-josutty-2015-malayalam-dvdrip-x264-aac-5-1-e-subs-mbrhdrg-t11741209.html" class="cellMainLink">Life of Josutty (2015) Malayalam DVDRip x264 AAC 5.1 E-Subs-MBRHDRG</a></div>
			</td>
			<td class="nobr center" data-sort="833916508">795.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T01:32:28+00:00">12 Dec 2015, 01:32:28</span></td>
			<td class="green center">3411</td>
			<td class="red lasttd center">3055</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11734977,0" class="icommentjs kaButton smallButton rightButton" href="/the-girl-king-2015-hdrip-xvid-ac3-evo-t11734977.html#comment">35 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-girl-king-2015-hdrip-xvid-ac3-evo-t11734977.html" class="cellMainLink">The Girl King 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1484657311">1.38 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T20:40:56+00:00">10 Dec 2015, 20:40:56</span></td>
			<td class="green center">3072</td>
			<td class="red lasttd center">3071</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11722932,0" class="icommentjs kaButton smallButton rightButton" href="/pan-2015-720p-bluray-x264-sparks-rarbg-t11722932.html#comment">47 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/pan-2015-720p-bluray-x264-sparks-rarbg-t11722932.html" class="cellMainLink">Pan 2015 720p BluRay x264-SPARKS[rarbg]</a></div>
			</td>
			<td class="nobr center" data-sort="4698269607">4.38 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T20:17:36+00:00">08 Dec 2015, 20:17:36</span></td>
			<td class="green center">3001</td>
			<td class="red lasttd center">2023</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11730388,0" class="icommentjs kaButton smallButton rightButton" href="/arrow-s04e09-hdtv-x264-lol-ettv-t11730388.html#comment">248 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrow-s04e09-hdtv-x264-lol-ettv-t11730388.html" class="cellMainLink">Arrow S04E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="310218735">295.85 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T02:00:24+00:00">10 Dec 2015, 02:00:24</span></td>
			<td class="green center">19387</td>
			<td class="red lasttd center">2290</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11724102,0" class="icommentjs kaButton smallButton rightButton" href="/the-flash-2014-s02e09-hdtv-x264-lol-ettv-t11724102.html#comment">338 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-flash-2014-s02e09-hdtv-x264-lol-ettv-t11724102.html" class="cellMainLink">The Flash 2014 S02E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="258610951">246.63 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T02:00:26+00:00">09 Dec 2015, 02:00:26</span></td>
			<td class="green center">18247</td>
			<td class="red lasttd center">2109</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11735871,0" class="icommentjs kaButton smallButton rightButton" href="/the-big-bang-theory-s09e10-hdtv-x264-lol-rartv-t11735871.html#comment">77 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-big-bang-theory-s09e10-hdtv-x264-lol-rartv-t11735871.html" class="cellMainLink">The Big Bang Theory S09E10 HDTV x264-LOL[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="139399486">132.94 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T01:29:10+00:00">11 Dec 2015, 01:29:10</span></td>
			<td class="green center">12011</td>
			<td class="red lasttd center">1738</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11735971,0" class="icommentjs kaButton smallButton rightButton" href="/the-vampire-diaries-s07e09-hdtv-x264-lol-ettv-t11735971.html#comment">62 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-vampire-diaries-s07e09-hdtv-x264-lol-ettv-t11735971.html" class="cellMainLink">The Vampire Diaries S07E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="243505464">232.22 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T02:00:20+00:00">11 Dec 2015, 02:00:20</span></td>
			<td class="green center">11854</td>
			<td class="red lasttd center">2028</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11736161,0" class="icommentjs kaButton smallButton rightButton" href="/the-originals-s03e09-hdtv-x264-lol-ettv-t11736161.html#comment">58 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-originals-s03e09-hdtv-x264-lol-ettv-t11736161.html" class="cellMainLink">The Originals S03E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="238582762">227.53 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T03:01:23+00:00">11 Dec 2015, 03:01:23</span></td>
			<td class="green center">9451</td>
			<td class="red lasttd center">1588</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11724345,0" class="icommentjs kaButton smallButton rightButton" href="/marvels-agents-of-s-h-i-e-l-d-s03e10-hdtv-x264-killers-ettv-t11724345.html#comment">175 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/marvels-agents-of-s-h-i-e-l-d-s03e10-hdtv-x264-killers-ettv-t11724345.html" class="cellMainLink">Marvels Agents of S H I E L D S03E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="305697183">291.54 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T03:27:19+00:00">09 Dec 2015, 03:27:19</span></td>
			<td class="green center">8527</td>
			<td class="red lasttd center">823</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11730625,0" class="icommentjs kaButton smallButton rightButton" href="/supernatural-s11e09-hdtv-x264-lol-ettv-t11730625.html#comment">117 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/supernatural-s11e09-hdtv-x264-lol-ettv-t11730625.html" class="cellMainLink">Supernatural S11E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="240484237">229.34 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T03:01:22+00:00">10 Dec 2015, 03:01:22</span></td>
			<td class="green center">8339</td>
			<td class="red lasttd center">819</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11730548,0" class="icommentjs kaButton smallButton rightButton" href="/modern-family-s07e09-hdtv-x264-killers-ettv-t11730548.html#comment">57 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/modern-family-s07e09-hdtv-x264-killers-ettv-t11730548.html" class="cellMainLink">Modern Family S07E09 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="208619214">198.95 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T02:35:23+00:00">10 Dec 2015, 02:35:23</span></td>
			<td class="green center">8075</td>
			<td class="red lasttd center">726</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11730745,0" class="icommentjs kaButton smallButton rightButton" href="/south-park-s19e10-hdtv-x264-killers-ettv-t11730745.html#comment">93 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/south-park-s19e10-hdtv-x264-killers-ettv-t11730745.html" class="cellMainLink">South Park S19E10 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="98684180">94.11 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T03:33:18+00:00">10 Dec 2015, 03:33:18</span></td>
			<td class="green center">7533</td>
			<td class="red lasttd center">311</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11730957,0" class="icommentjs kaButton smallButton rightButton" href="/american-horror-story-s05e09-hdtv-x264-fum-ettv-t11730957.html#comment">60 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/american-horror-story-s05e09-hdtv-x264-fum-ettv-t11730957.html" class="cellMainLink">American Horror Story S05E09 HDTV x264-FUM[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="246310203">234.9 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T04:38:19+00:00">10 Dec 2015, 04:38:19</span></td>
			<td class="green center">5944</td>
			<td class="red lasttd center">542</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11741397,0" class="icommentjs kaButton smallButton rightButton" href="/grimm-s05e06-hdtv-x264-fleet-rartv-t11741397.html#comment">41 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/grimm-s05e06-hdtv-x264-fleet-rartv-t11741397.html" class="cellMainLink">Grimm S05E06 HDTV x264-FLEET[rartv]</a></div>
			</td>
			<td class="nobr center" data-sort="284556218">271.37 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T03:00:24+00:00">12 Dec 2015, 03:00:24</span></td>
			<td class="green center">5078</td>
			<td class="red lasttd center">1610</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11735804,0" class="icommentjs kaButton smallButton rightButton" href="/elementary-s04e05-hdtv-x264-lol-ettv-t11735804.html#comment">41 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/elementary-s04e05-hdtv-x264-lol-ettv-t11735804.html" class="cellMainLink">Elementary S04E05 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="222466024">212.16 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T01:03:23+00:00">11 Dec 2015, 01:03:23</span></td>
			<td class="green center">5082</td>
			<td class="red lasttd center">573</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11724560,0" class="icommentjs kaButton smallButton rightButton" href="/the-victorias-secret-fashion-show-2015-hdtv-x264-batv-ettv-t11724560.html#comment">56 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-victorias-secret-fashion-show-2015-hdtv-x264-batv-ettv-t11724560.html" class="cellMainLink">The Victorias Secret Fashion Show 2015 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="693310387">661.19 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T04:22:20+00:00">09 Dec 2015, 04:22:20</span></td>
			<td class="green center">4296</td>
			<td class="red lasttd center">947</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11742588,0" class="icommentjs kaButton smallButton rightButton" href="/ufc-the-ultimate-fighter-22-finale-hdtv-x264-ebi-t11742588.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-the-ultimate-fighter-22-finale-hdtv-x264-ebi-t11742588.html" class="cellMainLink">UFC The Ultimate Fighter 22 Finale HDTV x264-Ebi</a></div>
			</td>
			<td class="nobr center" data-sort="1940919069">1.81 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T08:55:38+00:00">12 Dec 2015, 08:55:38</span></td>
			<td class="green center">2830</td>
			<td class="red lasttd center">2697</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11741869,0" class="icommentjs kaButton smallButton rightButton" href="/z-nation-s02e14-hdtv-x264-killers-ettv-t11741869.html#comment">19 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/z-nation-s02e14-hdtv-x264-killers-ettv-t11741869.html" class="cellMainLink">Z Nation S02E14 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="455161156">434.08 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T05:26:25+00:00">12 Dec 2015, 05:26:25</span></td>
			<td class="green center">3263</td>
			<td class="red lasttd center">1340</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11740200,0" class="icommentjs kaButton smallButton rightButton" href="/lacrim-r-i-p-r-o-vol-2-t11740200.html#comment">5 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/lacrim-r-i-p-r-o-vol-2-t11740200.html" class="cellMainLink">Lacrim - R I P R O Vol 2</a></div>
			</td>
			<td class="nobr center" data-sort="95548450">91.12 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T20:52:50+00:00">11 Dec 2015, 20:52:50</span></td>
			<td class="green center">2745</td>
			<td class="red lasttd center">356</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11732622,0" class="icommentjs kaButton smallButton rightButton" href="/r-kelly-the-buffet-deluxe-edition-2015-mp3-vbr-h4ckus-glodls-t11732622.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/r-kelly-the-buffet-deluxe-edition-2015-mp3-vbr-h4ckus-glodls-t11732622.html" class="cellMainLink">R. Kelly - The Buffet [Deluxe Edition] [2015] [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="125451730">119.64 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T11:32:16+00:00">10 Dec 2015, 11:32:16</span></td>
			<td class="green center">1670</td>
			<td class="red lasttd center">232</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11728090,0" class="icommentjs kaButton smallButton rightButton" href="/50-cent-the-kanan-tape-320kbps-2015-official-mixjoint-t11728090.html#comment">33 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/50-cent-the-kanan-tape-320kbps-2015-official-mixjoint-t11728090.html" class="cellMainLink">50 Cent - The Kanan Tape [320Kbps] [2015] [Official] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="61345929">58.5 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T18:24:17+00:00">09 Dec 2015, 18:24:17</span></td>
			<td class="green center">1237</td>
			<td class="red lasttd center">166</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11726894,0" class="icommentjs kaButton smallButton rightButton" href="/va-christmas-collection-2015-vol-02-2015-mp3-320kbps-h4ckus-glodls-t11726894.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-christmas-collection-2015-vol-02-2015-mp3-320kbps-h4ckus-glodls-t11726894.html" class="cellMainLink">VA - Christmas Collection 2015 VOL 02 [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="4352130401">4.05 <span>GB</span></td>
			<td class="center">641</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T14:24:43+00:00">09 Dec 2015, 14:24:43</span></td>
			<td class="green center">857</td>
			<td class="red lasttd center">788</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11741546,0" class="icommentjs kaButton smallButton rightButton" href="/adele-25-deluxe-edition-2015-scavvykid-t11741546.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/adele-25-deluxe-edition-2015-scavvykid-t11741546.html" class="cellMainLink">Adele - 25 [Deluxe Edition] [2015] [ScavvyKiD]</a></div>
			</td>
			<td class="nobr center" data-sort="105846997">100.94 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T04:06:32+00:00">12 Dec 2015, 04:06:32</span></td>
			<td class="green center">965</td>
			<td class="red lasttd center">338</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11731761,0" class="icommentjs kaButton smallButton rightButton" href="/august-alsina-this-thing-called-life-rnb-2015-t11731761.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/august-alsina-this-thing-called-life-rnb-2015-t11731761.html" class="cellMainLink">August Alsina - This Thing Called Life [RNB 2015]</a></div>
			</td>
			<td class="nobr center" data-sort="115567009">110.21 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T08:12:14+00:00">10 Dec 2015, 08:12:14</span></td>
			<td class="green center">1026</td>
			<td class="red lasttd center">128</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11725330,0" class="icommentjs kaButton smallButton rightButton" href="/the-playlist-christmas-hits-2015-mp3-divxtotal-t11725330.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-playlist-christmas-hits-2015-mp3-divxtotal-t11725330.html" class="cellMainLink">The Playlist: Christmas Hits 2015 MP3-DiVXTOTAL</a></div>
			</td>
			<td class="nobr center" data-sort="232961400">222.17 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T07:58:00+00:00">09 Dec 2015, 07:58:00</span></td>
			<td class="green center">815</td>
			<td class="red lasttd center">101</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11734205,0" class="icommentjs kaButton smallButton rightButton" href="/va-car-audio-best-of-the-month-sound-clinic-special-edition-2015-mp3-320-kbps-t11734205.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-car-audio-best-of-the-month-sound-clinic-special-edition-2015-mp3-320-kbps-t11734205.html" class="cellMainLink">VA - Car Audio. Best of the Month! [Sound Clinic - Special Edition] (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="2654642670">2.47 <span>GB</span></td>
			<td class="center">181</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T17:33:30+00:00">10 Dec 2015, 17:33:30</span></td>
			<td class="green center">371</td>
			<td class="red lasttd center">355</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738839,0" class="icommentjs kaButton smallButton rightButton" href="/va-hot-parade-winter-2016-2cd-2015-mp3-320-kbps-t11738839.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-hot-parade-winter-2016-2cd-2015-mp3-320-kbps-t11738839.html" class="cellMainLink">VA - Hot Parade Winter 2016 [2CD] (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="354861157">338.42 <span>MB</span></td>
			<td class="center">48</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T15:20:39+00:00">11 Dec 2015, 15:20:39</span></td>
			<td class="green center">402</td>
			<td class="red lasttd center">249</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738924,0" class="icommentjs kaButton smallButton rightButton" href="/elvis-presley-elvis-ultimate-christmas-2cd-2015-mp3-320-kbps-t11738924.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/elvis-presley-elvis-ultimate-christmas-2cd-2015-mp3-320-kbps-t11738924.html" class="cellMainLink">Elvis Presley - Elvis: Ultimate Christmas [2CD] (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="203621312">194.19 <span>MB</span></td>
			<td class="center">33</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T15:43:03+00:00">11 Dec 2015, 15:43:03</span></td>
			<td class="green center">378</td>
			<td class="red lasttd center">140</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738018,0" class="icommentjs kaButton smallButton rightButton" href="/pentatonix-that-s-christmas-to-me-deluxe-edition-2015-mp3-vbr-h4ckus-glodls-t11738018.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/pentatonix-that-s-christmas-to-me-deluxe-edition-2015-mp3-vbr-h4ckus-glodls-t11738018.html" class="cellMainLink">Pentatonix - That&#039;s Christmas To Me [Deluxe Edition] [2015] [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="88092672">84.01 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T11:33:31+00:00">11 Dec 2015, 11:33:31</span></td>
			<td class="green center">321</td>
			<td class="red lasttd center">114</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11734372,0" class="icommentjs kaButton smallButton rightButton" href="/young-buck-10-pints-320kbps-2015-official-mixjoint-t11734372.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/young-buck-10-pints-320kbps-2015-official-mixjoint-t11734372.html" class="cellMainLink">Young Buck - 10 Pints [320Kbps] [2015] [Official] (MixJoint)</a></div>
			</td>
			<td class="nobr center" data-sort="83884520">80 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T18:06:42+00:00">10 Dec 2015, 18:06:42</span></td>
			<td class="green center">288</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11737981,0" class="icommentjs kaButton smallButton rightButton" href="/bruce-springsteen-the-essential-bruce-springsteen-remastered-2015-mp3-vbr-h4ckus-glodls-t11737981.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bruce-springsteen-the-essential-bruce-springsteen-remastered-2015-mp3-vbr-h4ckus-glodls-t11737981.html" class="cellMainLink">Bruce Springsteen - The Essential Bruce Springsteen (Remastered) [2015] [MP3-VBR] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="293753894">280.15 <span>MB</span></td>
			<td class="center">41</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T11:19:47+00:00">11 Dec 2015, 11:19:47</span></td>
			<td class="green center">239</td>
			<td class="red lasttd center">77</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738818,0" class="icommentjs kaButton smallButton rightButton" href="/va-a-jazz-christmas-2cd-2015-mp3-320-kbps-t11738818.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-a-jazz-christmas-2cd-2015-mp3-320-kbps-t11738818.html" class="cellMainLink">VA - A Jazz Christmas [2CD] (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="237769597">226.75 <span>MB</span></td>
			<td class="center">37</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T15:16:17+00:00">11 Dec 2015, 15:16:17</span></td>
			<td class="green center">190</td>
			<td class="red lasttd center">85</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11727690,0" class="icommentjs kaButton smallButton rightButton" href="/va-luxemusic-dance-super-chart-vol-45-2015-mp3-320kbps-h4ckus-glodls-t11727690.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-luxemusic-dance-super-chart-vol-45-2015-mp3-320kbps-h4ckus-glodls-t11727690.html" class="cellMainLink">VA - LUXEmusic - Dance Super Chart Vol.45 [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="542781052">517.64 <span>MB</span></td>
			<td class="center">53</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T17:13:05+00:00">09 Dec 2015, 17:13:05</span></td>
			<td class="green center">165</td>
			<td class="red lasttd center">90</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11727165,0" class="icommentjs kaButton smallButton rightButton" href="/fallout-4-v-1-2-37-2015-r-g-mechanics-t11727165.html#comment">54 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/fallout-4-v-1-2-37-2015-r-g-mechanics-t11727165.html" class="cellMainLink">Fallout 4 [v 1.2.37] (2015) [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="20218963563">18.83 <span>GB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T15:31:19+00:00">09 Dec 2015, 15:31:19</span></td>
			<td class="green center">1557</td>
			<td class="red lasttd center">1578</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738185,0" class="icommentjs kaButton smallButton rightButton" href="/xatab-repack-the-sims-4-deluxe-edition-v1-13-104-1010-2015-t11738185.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/xatab-repack-the-sims-4-deluxe-edition-v1-13-104-1010-2015-t11738185.html" class="cellMainLink">[XATAB] Repack:The Sims 4: Deluxe Edition [v1.13.104.1010] (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="11248815170">10.48 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T12:08:00+00:00">11 Dec 2015, 12:08:00</span></td>
			<td class="green center">1606</td>
			<td class="red lasttd center">1163</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11734286,0" class="icommentjs kaButton smallButton rightButton" href="/the-sims-4-get-together-addon-reloaded-t11734286.html#comment">101 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/the-sims-4-get-together-addon-reloaded-t11734286.html" class="cellMainLink">The Sims 4 Get Together Addon-RELOADED</a></div>
			</td>
			<td class="nobr center" data-sort="6392942977">5.95 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T17:51:41+00:00">10 Dec 2015, 17:51:41</span></td>
			<td class="green center">1166</td>
			<td class="red lasttd center">1608</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11733757,0" class="icommentjs kaButton smallButton rightButton" href="/lightning-returns-final-fantasy-xiii-codex-t11733757.html#comment">126 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/lightning-returns-final-fantasy-xiii-codex-t11733757.html" class="cellMainLink">LIGHTNING RETURNS FINAL FANTASY XIII-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="20635096514">19.22 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T16:12:47+00:00">10 Dec 2015, 16:12:47</span></td>
			<td class="green center">538</td>
			<td class="red lasttd center">2004</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11732472,0" class="icommentjs kaButton smallButton rightButton" href="/dying-light-ultimate-edition-v-1-6-2-dlcs-2015-pc-repack-by-xatab-cpul-t11732472.html#comment">48 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dying-light-ultimate-edition-v-1-6-2-dlcs-2015-pc-repack-by-xatab-cpul-t11732472.html" class="cellMainLink">Dying Light: Ultimate Edition [v 1.6.2 + DLCs] (2015) PC | RePack by xatab [CPUL]</a></div>
			</td>
			<td class="nobr center" data-sort="10670125005">9.94 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T10:54:42+00:00">10 Dec 2015, 10:54:42</span></td>
			<td class="green center">924</td>
			<td class="red lasttd center">650</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11733063,0" class="icommentjs kaButton smallButton rightButton" href="/guilty-gear-xrd-sign-codex-t11733063.html#comment">32 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/guilty-gear-xrd-sign-codex-t11733063.html" class="cellMainLink">GUILTY GEAR Xrd SIGN-CODEX</a></div>
			</td>
			<td class="nobr center" data-sort="10694071898">9.96 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T13:29:59+00:00">10 Dec 2015, 13:29:59</span></td>
			<td class="green center">463</td>
			<td class="red lasttd center">922</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739756,0" class="icommentjs kaButton smallButton rightButton" href="/total-war-attila-update-6-dlcs-rus-2015-pc-repack-Ð¾Ñ-xatab-t11739756.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/total-war-attila-update-6-dlcs-rus-2015-pc-repack-Ð¾Ñ-xatab-t11739756.html" class="cellMainLink">Total War: ATTILA [Update 6 + DLCs][RUS] (2015) PC | RePack Ð¾Ñ xatab</a></div>
			</td>
			<td class="nobr center" data-sort="9939043226">9.26 <span>GB</span></td>
			<td class="center">10</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T18:41:46+00:00">11 Dec 2015, 18:41:46</span></td>
			<td class="green center">574</td>
			<td class="red lasttd center">367</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738444,0" class="icommentjs kaButton smallButton rightButton" href="/the-sims-4-deluxe-edition-all-dlcs-add-ons-inc-spooky-stuff-get-together-multi17-v1-13-104-1010-fitgirl-repack-t11738444.html#comment">97 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/the-sims-4-deluxe-edition-all-dlcs-add-ons-inc-spooky-stuff-get-together-multi17-v1-13-104-1010-fitgirl-repack-t11738444.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-sims-4-deluxe-edition-all-dlcs-add-ons-inc-spooky-stuff-get-together-multi17-v1-13-104-1010-fitgirl-repack-t11738444.html" class="cellMainLink">The Sims 4: Deluxe Edition + ALL DLCs/Add-ons, inc. Spooky Stuff &amp; Get Together (MULTI17, v1.13.104.1010) [FitGirl Repack]</a></div>
			</td>
			<td class="nobr center" data-sort="10890296729">10.14 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T13:21:34+00:00">11 Dec 2015, 13:21:34</span></td>
			<td class="green center">257</td>
			<td class="red lasttd center">980</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11729924,0" class="icommentjs kaButton smallButton rightButton" href="/dead-reckoning-3-setup-exe-t11729924.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dead-reckoning-3-setup-exe-t11729924.html" class="cellMainLink">Dead Reckoning 3_Setup.exe</a></div>
			</td>
			<td class="nobr center" data-sort="812255976">774.63 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T23:44:57+00:00">09 Dec 2015, 23:44:57</span></td>
			<td class="green center">663</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738481,0" class="icommentjs kaButton smallButton rightButton" href="/fitgirl-repack-the-sims-4-deluxe-edition-v1-13-104-1010-2015-en-ru-t11738481.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/fitgirl-repack-the-sims-4-deluxe-edition-v1-13-104-1010-2015-en-ru-t11738481.html" class="cellMainLink">[FitGirl Repack]The Sims 4: Deluxe Edition [v1.13.104.1010](2015)[En-Ru]</a></div>
			</td>
			<td class="nobr center" data-sort="10890296727">10.14 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T13:31:04+00:00">11 Dec 2015, 13:31:04</span></td>
			<td class="green center">387</td>
			<td class="red lasttd center">247</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11722573,0" class="icommentjs kaButton smallButton rightButton" href="/hurtworld-0-3-1-4-2015-pc-repack-by-rg-alkad-t11722573.html#comment">17 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/hurtworld-0-3-1-4-2015-pc-repack-by-rg-alkad-t11722573.html" class="cellMainLink">Hurtworld [0.3.1.4] (2015) PC | RePack by RG Alkad</a></div>
			</td>
			<td class="nobr center" data-sort="247621139">236.15 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T18:37:54+00:00">08 Dec 2015, 18:37:54</span></td>
			<td class="green center">463</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11737064,0" class="icommentjs kaButton smallButton rightButton" href="/english-version-lightning-returns-final-fantasy-xiii-inc-all-dlc-s-repack-by-corepack-t11737064.html#comment">38 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/english-version-lightning-returns-final-fantasy-xiii-inc-all-dlc-s-repack-by-corepack-t11737064.html" class="cellMainLink">English Version - Lightning Returns Final Fantasy XIII Inc. All DLC&#039;s - Repack by CorePack</a></div>
			</td>
			<td class="nobr center" data-sort="8190567989">7.63 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T06:58:55+00:00">11 Dec 2015, 06:58:55</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">619</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11733854,0" class="icommentjs kaButton smallButton rightButton" href="/galactic-civilizations-iii-v1-50-6-dlc-2015-repack-xatab-t11733854.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/galactic-civilizations-iii-v1-50-6-dlc-2015-repack-xatab-t11733854.html" class="cellMainLink">Galactic Civilizations III [v1.50 +6 DLC] (2015) RePack [Xatab]</a></div>
			</td>
			<td class="nobr center" data-sort="6034838944">5.62 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T16:30:47+00:00">10 Dec 2015, 16:30:47</span></td>
			<td class="green center">354</td>
			<td class="red lasttd center">126</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11731588,0" class="icommentjs kaButton smallButton rightButton" href="/immortal-love-letter-from-the-past-collector-s-edition-asg-t11731588.html#comment">15 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/immortal-love-letter-from-the-past-collector-s-edition-asg-t11731588.html" class="cellMainLink">Immortal Love - Letter From The Past Collector&#039;s Edition [ASG]</a></div>
			</td>
			<td class="nobr center" data-sort="1082022945">1.01 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T07:25:06+00:00">10 Dec 2015, 07:25:06</span></td>
			<td class="green center">374</td>
			<td class="red lasttd center">63</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11723709,0" class="icommentjs kaButton smallButton rightButton" href="/might-and-magic-heroes-vii-deluxe-edition-v1-60-2015-repack-xatab-t11723709.html#comment">20 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/might-and-magic-heroes-vii-deluxe-edition-v1-60-2015-repack-xatab-t11723709.html" class="cellMainLink">Might and Magic Heroes VII: Deluxe Edition [v1.60] (2015) Repack [Xatab]</a></div>
			</td>
			<td class="nobr center" data-sort="9933369288">9.25 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T23:47:58+00:00">08 Dec 2015, 23:47:58</span></td>
			<td class="green center">337</td>
			<td class="red lasttd center">119</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11742955,0" class="icommentjs kaButton smallButton rightButton" href="/gimp-2-8-16-setup-exe-t11742955.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/gimp-2-8-16-setup-exe-t11742955.html" class="cellMainLink">gimp-2 8 16-setup exe</a></div>
			</td>
			<td class="nobr center" data-sort="96819488">92.33 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T10:48:59+00:00">12 Dec 2015, 10:48:59</span></td>
			<td class="green center">1786</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11722415,0" class="icommentjs kaButton smallButton rightButton" href="/internet-download-manager-idm-6-25-build-5-registered-32bit-64bit-patch-crackingpatching-t11722415.html#comment">97 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/internet-download-manager-idm-6-25-build-5-registered-32bit-64bit-patch-crackingpatching-t11722415.html" class="cellMainLink">Internet Download Manager (IDM) 6.25 Build 5 Registered (32bit + 64bit Patch) [CrackingPatching]</a></div>
			</td>
			<td class="nobr center" data-sort="10374825">9.89 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T18:12:32+00:00">08 Dec 2015, 18:12:32</span></td>
			<td class="green center">1212</td>
			<td class="red lasttd center">371</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11724270,0" class="icommentjs kaButton smallButton rightButton" href="/microsoft-office-professional-plus-2016-v16-0-4312-1000-incl-activator-team-os-t11724270.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-office-professional-plus-2016-v16-0-4312-1000-incl-activator-team-os-t11724270.html" class="cellMainLink">Microsoft Office Professional Plus 2016 v16.0.4312.1000 Incl Activator-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="3214685944">2.99 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T03:05:45+00:00">09 Dec 2015, 03:05:45</span></td>
			<td class="green center">506</td>
			<td class="red lasttd center">584</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11726936,0" class="icommentjs kaButton smallButton rightButton" href="/ultimate-adobe-photoshop-plug-ins-bundle-2015-12-team-os-t11726936.html#comment">39 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/ultimate-adobe-photoshop-plug-ins-bundle-2015-12-team-os-t11726936.html" class="cellMainLink">Ultimate Adobe Photoshop Plug-ins Bundle 2015.12-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="7814400887">7.28 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T14:41:40+00:00">09 Dec 2015, 14:41:40</span></td>
			<td class="green center">408</td>
			<td class="red lasttd center">490</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11722212,0" class="icommentjs kaButton smallButton rightButton" href="/adobe-flash-player-20-0-0-228-235-final-2015-pc-repack-by-d-akov-t11722212.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-flash-player-20-0-0-228-235-final-2015-pc-repack-by-d-akov-t11722212.html" class="cellMainLink">Adobe Flash Player 20.0.0.228 / 235 Final (2015) PC | + RePack by D!akov</a></div>
			</td>
			<td class="nobr center" data-sort="114362162">109.06 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T17:30:29+00:00">08 Dec 2015, 17:30:29</span></td>
			<td class="green center">504</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11725342,0" class="icommentjs kaButton smallButton rightButton" href="/microsoft-s-office-2016-professional-plus-latest-activator-64-bit-appzdam-t11725342.html#comment">64 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-s-office-2016-professional-plus-latest-activator-64-bit-appzdam-t11725342.html" class="cellMainLink">Microsoft&#039;s Office 2016 Professional Plus (Latest) + Activator [64 bit] - AppzDam</a></div>
			</td>
			<td class="nobr center" data-sort="863752809">823.74 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T08:01:03+00:00">09 Dec 2015, 08:01:03</span></td>
			<td class="green center">365</td>
			<td class="red lasttd center">208</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738189,0" class="icommentjs kaButton smallButton rightButton" href="/keys-for-eset-kaspersky-avast-dr-web-avira-december-11-2015-t11738189.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/keys-for-eset-kaspersky-avast-dr-web-avira-december-11-2015-t11738189.html" class="cellMainLink">KEYS for ESET, Kaspersky, Avast, Dr.Web, Avira [December 11] (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="3354537">3.2 <span>MB</span></td>
			<td class="center">120</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T12:09:28+00:00">11 Dec 2015, 12:09:28</span></td>
			<td class="green center">236</td>
			<td class="red lasttd center">222</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11723211,0" class="icommentjs kaButton smallButton rightButton" href="/hotspot-shield-vpn-elite-5-20-7-multilingual-patch-4realtorrentz-t11723211.html#comment">47 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/hotspot-shield-vpn-elite-5-20-7-multilingual-patch-4realtorrentz-t11723211.html" class="cellMainLink">Hotspot Shield VPN Elite 5.20.7 Multilingual + Patch [4realtorrentz]</a></div>
			</td>
			<td class="nobr center" data-sort="28634379">27.31 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T21:38:39+00:00">08 Dec 2015, 21:38:39</span></td>
			<td class="green center">326</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11733654,0" class="icommentjs kaButton smallButton rightButton" href="/ccleaner-v5-12-5431-all-editions-2015-repack-portable-kpojiuk-t11733654.html#comment">13 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/ccleaner-v5-12-5431-all-editions-2015-repack-portable-kpojiuk-t11733654.html" class="cellMainLink">CCleaner v5.12.5431: All Editions (2015) Repack &amp; Portable|[ KpoJIuK ]</a></div>
			</td>
			<td class="nobr center" data-sort="4522000">4.31 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T15:52:02+00:00">10 Dec 2015, 15:52:02</span></td>
			<td class="green center">256</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11734401,0" class="icommentjs kaButton smallButton rightButton" href="/adobe-photoshop-cc-2015-1-20151114-r-301-2015-portable-portablewares-t11734401.html#comment">8 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/adobe-photoshop-cc-2015-1-20151114-r-301-2015-portable-portablewares-t11734401.html" class="cellMainLink">Adobe Photoshop CC 2015.1 [20151114.r.301] (2015) Portable [PortableWares]</a></div>
			</td>
			<td class="nobr center" data-sort="469173024">447.44 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T18:12:04+00:00">10 Dec 2015, 18:12:04</span></td>
			<td class="green center">232</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11725478,0" class="icommentjs kaButton smallButton rightButton" href="/vmware-workstation-pro-12-1-0-build-3272444-64bit-eng-serial-at-team-t11725478.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/vmware-workstation-pro-12-1-0-build-3272444-64bit-eng-serial-at-team-t11725478.html" class="cellMainLink">VMware Workstation Pro 12.1.0 Build 3272444 - 64bit [ENG] [Serial] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center" data-sort="297901396">284.1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T08:30:41+00:00">09 Dec 2015, 08:30:41</span></td>
			<td class="green center">223</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11723493,0" class="icommentjs kaButton smallButton rightButton" href="/driverpack-solution-15-12-driver-packs-15-12-1-2015-iso-t11723493.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/driverpack-solution-15-12-driver-packs-15-12-1-2015-iso-t11723493.html" class="cellMainLink">DriverPack Solution 15.12 [+ driver packs 15.12.1] (2015) [ISO]</a></div>
			</td>
			<td class="nobr center" data-sort="12415006720">11.56 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T22:33:13+00:00">08 Dec 2015, 22:33:13</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">166</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11737937,0" class="icommentjs kaButton smallButton rightButton" href="/solidworks-2016-sp1-0-x64-incl-serial-activator-team-os-t11737937.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/solidworks-2016-sp1-0-x64-incl-serial-activator-team-os-t11737937.html" class="cellMainLink">SolidWorks 2016 SP1.0 (x64) Incl Serial+Activator-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="11270668476">10.5 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T11:08:50+00:00">11 Dec 2015, 11:08:50</span></td>
			<td class="green center">61</td>
			<td class="red lasttd center">220</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11738593,0" class="icommentjs kaButton smallButton rightButton" href="/windows-10-enterprise-x64-oem-multi-14-dec-2015-generation2-t11738593.html#comment">26 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-enterprise-x64-oem-multi-14-dec-2015-generation2-t11738593.html" class="cellMainLink">Windows 10 Enterprise X64 OEM MULTi-14 Dec 2015 {Generation2}</a></div>
			</td>
			<td class="nobr center" data-sort="7029334253">6.55 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T14:12:09+00:00">11 Dec 2015, 14:12:09</span></td>
			<td class="green center">63</td>
			<td class="red lasttd center">208</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11725935,0" class="icommentjs kaButton smallButton rightButton" href="/acronis-true-image-2016-19-0-build-6027-bootcd-media-add-ons-2015-pc-repack-by-kpojiuk-t11725935.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/acronis-true-image-2016-19-0-build-6027-bootcd-media-add-ons-2015-pc-repack-by-kpojiuk-t11725935.html" class="cellMainLink">Acronis True Image 2016 19.0 Build 6027 + BootCD + Media Add-ons (2015) PC | RePack by KpoJIuk</a></div>
			</td>
			<td class="nobr center" data-sort="1143361198">1.06 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T10:14:59+00:00">09 Dec 2015, 10:14:59</span></td>
			<td class="green center">149</td>
			<td class="red lasttd center">35</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11743390,0" class="icommentjs kaButton smallButton rightButton" href="/horriblesubs-gakusen-toshi-asterisk-11-720p-mkv-t11743390.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-gakusen-toshi-asterisk-11-720p-mkv-t11743390.html" class="cellMainLink">[HorribleSubs] Gakusen Toshi Asterisk - 11 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="337625623">321.98 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T13:10:03+00:00">12 Dec 2015, 13:10:03</span></td>
			<td class="green center">801</td>
			<td class="red lasttd center">557</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-heavy-object-10-raw-mx-1280x720-x264-aac-mp4-t11739904.html" class="cellMainLink">[Leopard-Raws] Heavy Object - 10 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="506375655">482.92 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T19:20:03+00:00">11 Dec 2015, 19:20:03</span></td>
			<td class="green center">771</td>
			<td class="red lasttd center">266</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11722826,0" class="icommentjs kaButton smallButton rightButton" href="/berserk-complete-series-dual-audio-1080p-hevc-x265-t11722826.html#comment">22 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/berserk-complete-series-dual-audio-1080p-hevc-x265-t11722826.html" class="cellMainLink">Berserk (Complete Series) [DUAL-AUDIO] [1080p] [HEVC] [x265]</a></div>
			</td>
			<td class="nobr center" data-sort="4848305929">4.52 <span>GB</span></td>
			<td class="center">36</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T19:37:09+00:00">08 Dec 2015, 19:37:09</span></td>
			<td class="green center">330</td>
			<td class="red lasttd center">395</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11722469,0" class="icommentjs kaButton smallButton rightButton" href="/berserk-the-golden-age-arc-complete-dual-audio-1080p-hevc-x265-t11722469.html#comment">18 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/berserk-the-golden-age-arc-complete-dual-audio-1080p-hevc-x265-t11722469.html" class="cellMainLink">Berserk The Golden Age Arc (Complete) [DUAL-AUDIO] [1080p] [HEVC] [x265]</a></div>
			</td>
			<td class="nobr center" data-sort="3106975422">2.89 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T18:18:17+00:00">08 Dec 2015, 18:18:17</span></td>
			<td class="green center">242</td>
			<td class="red lasttd center">223</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739386,0" class="icommentjs kaButton smallButton rightButton" href="/ninja-scoll-the-animated-series-480p-hevc-x265-t11739386.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ninja-scoll-the-animated-series-480p-hevc-x265-t11739386.html" class="cellMainLink">Ninja Scoll (The Animated Series) [480p] [HEVC] [x265]</a></div>
			</td>
			<td class="nobr center" data-sort="1369944204">1.28 <span>GB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T17:20:37+00:00">11 Dec 2015, 17:20:37</span></td>
			<td class="green center">193</td>
			<td class="red lasttd center">217</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739355,0" class="icommentjs kaButton smallButton rightButton" href="/ninja-scroll-1993-movie-dual-audio-1080p-hevc-x265-t11739355.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ninja-scroll-1993-movie-dual-audio-1080p-hevc-x265-t11739355.html" class="cellMainLink">Ninja Scroll (1993 Movie) [DUAL-AUDIO] [1080p] [HEVC] [x265]</a></div>
			</td>
			<td class="nobr center" data-sort="810279039">772.74 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T17:13:41+00:00">11 Dec 2015, 17:13:41</span></td>
			<td class="green center">136</td>
			<td class="red lasttd center">125</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11732740,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-naruto-shippuuden-441-english-subbed-480p-kami-t11732740.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-naruto-shippuuden-441-english-subbed-480p-kami-t11732740.html" class="cellMainLink">[AnimeRG] Naruto Shippuuden - 441 [English Subbed][480p] [KaMi]</a></div>
			</td>
			<td class="nobr center" data-sort="39777002">37.93 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T12:03:13+00:00">10 Dec 2015, 12:03:13</span></td>
			<td class="green center">150</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-one-punch-man-oad-576p-e2b53313-mkv-t11741971.html" class="cellMainLink">[AnimeRG] One Punch Man OAD [576p] [E2B53313].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="241210836">230.04 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T06:00:23+00:00">12 Dec 2015, 06:00:23</span></td>
			<td class="green center">79</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11733949,0" class="icommentjs kaButton smallButton rightButton" href="/shepardtds-the-world-god-only-knows-ii-dual-audio-720p-8-bit-x265-hevc-1-12-complete-t11733949.html#comment">6 <i class="ka ka-comment"></i></a>					<a class="icon16" href="/shepardtds-the-world-god-only-knows-ii-dual-audio-720p-8-bit-x265-hevc-1-12-complete-t11733949.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/shepardtds-the-world-god-only-knows-ii-dual-audio-720p-8-bit-x265-hevc-1-12-complete-t11733949.html" class="cellMainLink">[ShepardTDS] The World God Only Knows II Dual Audio [720p 8-bit x265 HEVC] 1-12 Complete</a></div>
			</td>
			<td class="nobr center" data-sort="2110320900">1.97 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T16:47:19+00:00">10 Dec 2015, 16:47:19</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fff-noragami-aragoto-07-09-t11741222.html" class="cellMainLink">[FFF] Noragami Aragoto - 07-09</a></div>
			</td>
			<td class="nobr center" data-sort="1238458806">1.15 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T01:40:02+00:00">12 Dec 2015, 01:40:02</span></td>
			<td class="green center">77</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/iorchid-naruto-shippuden-shippuuden-441-480p-eng-sub-mkv-t11732960.html" class="cellMainLink">[iORcHiD] Naruto Shippuden (Shippuuden) - 441 [480p][Eng Sub].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="93753857">89.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T12:58:50+00:00">10 Dec 2015, 12:58:50</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11729478,0" class="icommentjs kaButton smallButton rightButton" href="/animerg-dragon-ball-super-022-480p-kami-mkv-t11729478.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-022-480p-kami-mkv-t11729478.html" class="cellMainLink">[AnimeRG] Dragon Ball Super 022 - [480p] [KaMi].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="76973035">73.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T21:54:29+00:00">09 Dec 2015, 21:54:29</span></td>
			<td class="green center">70</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11732717,0" class="icommentjs kaButton smallButton rightButton" href="/naruto-shippuden-441-english-subbed-480p-arizone-t11732717.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-441-english-subbed-480p-arizone-t11732717.html" class="cellMainLink">Naruto Shippuden 441 [ENGLISH SUBBED] 480p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="62563378">59.67 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T11:57:40+00:00">10 Dec 2015, 11:57:40</span></td>
			<td class="green center">29</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/arrg-fairy-tail-s2-88-263-english-subbed-720p-lucifer22-t11741321.html" class="cellMainLink">[ARRG] Fairy Tail S2 - 88 (263) English Subbed 720p [Lucifer22]</a></div>
			</td>
			<td class="nobr center" data-sort="133657391">127.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-12T02:20:08+00:00">12 Dec 2015, 02:20:08</span></td>
			<td class="green center">29</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-ushio-to-tora-2015-24-720p-aac-mp4-t11739556.html" class="cellMainLink">[BakedFish] Ushio to Tora (2015) - 24 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="316854838">302.18 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T17:58:05+00:00">11 Dec 2015, 17:58:05</span></td>
			<td class="green center">28</td>
			<td class="red lasttd center">9</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11729666,0" class="icommentjs kaButton smallButton rightButton" href="/marvel-week-12-09-2015-nem-t11729666.html#comment">25 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/marvel-week-12-09-2015-nem-t11729666.html" class="cellMainLink">Marvel Week+ (12-09-2015) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="718932927">685.63 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T22:34:30+00:00">09 Dec 2015, 22:34:30</span></td>
			<td class="green center">722</td>
			<td class="red lasttd center">311</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11727041,0" class="icommentjs kaButton smallButton rightButton" href="/dc-week-12-09-2015-aka-dc-you-week-28-nem-t11727041.html#comment">27 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dc-week-12-09-2015-aka-dc-you-week-28-nem-t11727041.html" class="cellMainLink">DC Week+ (12-09-2015) (aka DC YOU Week 28) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="733610145">699.63 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T15:06:27+00:00">09 Dec 2015, 15:06:27</span></td>
			<td class="green center">514</td>
			<td class="red lasttd center">223</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11724577,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-magazines-bundle-december-9-2015-true-pdf-t11724577.html#comment">14 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-december-9-2015-true-pdf-t11724577.html" class="cellMainLink">Assorted Magazines Bundle - December 9 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="749060995">714.36 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T04:29:43+00:00">09 Dec 2015, 04:29:43</span></td>
			<td class="green center">454</td>
			<td class="red lasttd center">328</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11727094,0" class="icommentjs kaButton smallButton rightButton" href="/why-motivating-people-doesn-t-work-and-what-does-susan-fowler-t11727094.html#comment">9 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/why-motivating-people-doesn-t-work-and-what-does-susan-fowler-t11727094.html" class="cellMainLink">Why Motivating People Doesn&#039;t Work . . . and What Does - Susan Fowler</a></div>
			</td>
			<td class="nobr center" data-sort="3011470">2.87 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T15:16:21+00:00">09 Dec 2015, 15:16:21</span></td>
			<td class="green center">570</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11727483,0" class="icommentjs kaButton smallButton rightButton" href="/the-walking-dead-149-2015-digital-zone-empire-cbr-t11727483.html#comment">24 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-walking-dead-149-2015-digital-zone-empire-cbr-t11727483.html" class="cellMainLink">The Walking Dead 149 (2015) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="34437928">32.84 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T16:33:41+00:00">09 Dec 2015, 16:33:41</span></td>
			<td class="green center">574</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11736618,0" class="icommentjs kaButton smallButton rightButton" href="/computer-gadget-mags-december-11-2015-true-pdf-t11736618.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/computer-gadget-mags-december-11-2015-true-pdf-t11736618.html" class="cellMainLink">Computer &amp; Gadget Mags - December 11 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="273220177">260.56 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T04:56:54+00:00">11 Dec 2015, 04:56:54</span></td>
			<td class="green center">440</td>
			<td class="red lasttd center">194</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11732357,0" class="icommentjs kaButton smallButton rightButton" href="/pc-pro-ditch-your-isp-s-router-and-how-to-triple-your-sped-and-destroy-dead-spots-february-2016-true-pdf-t11732357.html#comment">11 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/pc-pro-ditch-your-isp-s-router-and-how-to-triple-your-sped-and-destroy-dead-spots-february-2016-true-pdf-t11732357.html" class="cellMainLink">PC Pro - Ditch Your ISP&#039;s Router + And how to Triple Your Sped and Destroy Dead Spots (February 2016) (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="48846484">46.58 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T10:35:22+00:00">10 Dec 2015, 10:35:22</span></td>
			<td class="green center">520</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11726349,0" class="icommentjs kaButton smallButton rightButton" href="/secret-wars-08-of-09-2016-digital-zone-empire-cbr-nem-t11726349.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/secret-wars-08-of-09-2016-digital-zone-empire-cbr-nem-t11726349.html" class="cellMainLink">Secret Wars 08 (of 09) (2016) (Digital) (Zone-Empire).cbr (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="40451436">38.58 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T12:03:46+00:00">09 Dec 2015, 12:03:46</span></td>
			<td class="green center">374</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11736766,0" class="icommentjs kaButton smallButton rightButton" href="/drawing-made-easy-learn-sketching-pencil-drawing-and-doodling-t11736766.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/drawing-made-easy-learn-sketching-pencil-drawing-and-doodling-t11736766.html" class="cellMainLink">Drawing Made Easy Learn Sketching. Pencil Drawing and Doodling</a></div>
			</td>
			<td class="nobr center" data-sort="6453703">6.15 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T05:39:36+00:00">11 Dec 2015, 05:39:36</span></td>
			<td class="green center">352</td>
			<td class="red lasttd center">32</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11722121,0" class="icommentjs kaButton smallButton rightButton" href="/fine-cooking-soups-stews-no-fail-recipes-for-every-season-2013-pdf-gooner-t11722121.html#comment">10 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/fine-cooking-soups-stews-no-fail-recipes-for-every-season-2013-pdf-gooner-t11722121.html" class="cellMainLink">Fine Cooking - Soups &amp; Stews - No-Fail Recipes for Every Season (2013).pdf Gooner</a></div>
			</td>
			<td class="nobr center" data-sort="10745002">10.25 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T17:02:38+00:00">08 Dec 2015, 17:02:38</span></td>
			<td class="green center">341</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11730681,0" class="icommentjs kaButton smallButton rightButton" href="/assorted-tech-computer-magazines-bundle-december-10-2015-true-pdf-t11730681.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/assorted-tech-computer-magazines-bundle-december-10-2015-true-pdf-t11730681.html" class="cellMainLink">Assorted Tech &amp; Computer Magazines Bundle - December 10 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="190869476">182.03 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T03:18:38+00:00">10 Dec 2015, 03:18:38</span></td>
			<td class="green center">254</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11727034,0" class="icommentjs kaButton smallButton rightButton" href="/star-wars-annual-001-2016-digital-minutemen-midas-cbr-t11727034.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/star-wars-annual-001-2016-digital-minutemen-midas-cbr-t11727034.html" class="cellMainLink">Star Wars Annual 001 (2016) (digital) (Minutemen-Midas).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="53950986">51.45 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T15:04:47+00:00">09 Dec 2015, 15:04:47</span></td>
			<td class="green center">255</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11726331,0" class="icommentjs kaButton smallButton rightButton" href="/amazing-spider-man-004-2016-digital-zone-empire-cbr-t11726331.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/amazing-spider-man-004-2016-digital-zone-empire-cbr-t11726331.html" class="cellMainLink">Amazing Spider-Man 004 (2016) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="44125088">42.08 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T12:00:21+00:00">09 Dec 2015, 12:00:21</span></td>
			<td class="green center">189</td>
			<td class="red lasttd center">5</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11728390,0" class="icommentjs kaButton smallButton rightButton" href="/ultimates-002-2016-digital-zone-empire-cbr-t11728390.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/ultimates-002-2016-digital-zone-empire-cbr-t11728390.html" class="cellMainLink">Ultimates 002 (2016) (Digital) (Zone-Empire).cbr</a></div>
			</td>
			<td class="nobr center" data-sort="43924822">41.89 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T19:04:04+00:00">09 Dec 2015, 19:04:04</span></td>
			<td class="green center">152</td>
			<td class="red lasttd center">4</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11725070,0" class="icommentjs kaButton smallButton rightButton" href="/0-day-week-of-2015-12-02-t11725070.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/0-day-week-of-2015-12-02-t11725070.html" class="cellMainLink">0-Day Week of 2015.12.02</a></div>
			</td>
			<td class="nobr center" data-sort="6865397498">6.39 <span>GB</span></td>
			<td class="center">147</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T06:46:25+00:00">09 Dec 2015, 06:46:25</span></td>
			<td class="green center">87</td>
			<td class="red lasttd center">112</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739283,0" class="icommentjs kaButton smallButton rightButton" href="/va-jazz-standards-collection-2015-flac-t11739283.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-jazz-standards-collection-2015-flac-t11739283.html" class="cellMainLink">VA - Jazz Standards Collection (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="1064140435">1014.84 <span>MB</span></td>
			<td class="center">59</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T16:57:21+00:00">11 Dec 2015, 16:57:21</span></td>
			<td class="green center">233</td>
			<td class="red lasttd center">97</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/depeche-mode-never-feel-the-silence-remixed-by-isaac-junkie-2015-flac-t11740052.html" class="cellMainLink">Depeche Mode - Never Feel The Silence (Remixed by Isaac Junkie) (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="297655314">283.87 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T20:09:55+00:00">11 Dec 2015, 20:09:55</span></td>
			<td class="green center">196</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11733660,0" class="icommentjs kaButton smallButton rightButton" href="/tom-petty-the-heartbreakers-southern-accents-2015-24-96-hd-flac-t11733660.html#comment">4 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tom-petty-the-heartbreakers-southern-accents-2015-24-96-hd-flac-t11733660.html" class="cellMainLink">Tom Petty &amp; The Heartbreakers - Southern Accents (2015) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="981044236">935.6 <span>MB</span></td>
			<td class="center">26</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T15:53:05+00:00">10 Dec 2015, 15:53:05</span></td>
			<td class="green center">183</td>
			<td class="red lasttd center">58</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11737758,0" class="icommentjs kaButton smallButton rightButton" href="/u2-live-at-accorhotels-arena-paris-dec-2015-innocence-experience-tour-flac-t11737758.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/u2-live-at-accorhotels-arena-paris-dec-2015-innocence-experience-tour-flac-t11737758.html" class="cellMainLink">U2 - Live at AccorHotels Arena Paris Dec 2015 (iNNOCENCE + eXPERIENCE Tour) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="843597296">804.52 <span>MB</span></td>
			<td class="center">33</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T10:20:47+00:00">11 Dec 2015, 10:20:47</span></td>
			<td class="green center">167</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11725282,0" class="icommentjs kaButton smallButton rightButton" href="/frank-sinatra-come-swing-with-me-2015-24-192-hd-flac-t11725282.html#comment">7 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/frank-sinatra-come-swing-with-me-2015-24-192-hd-flac-t11725282.html" class="cellMainLink">Frank Sinatra - Come Swing With Me! (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1409143506">1.31 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T07:44:22+00:00">09 Dec 2015, 07:44:22</span></td>
			<td class="green center">135</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11726951,0" class="icommentjs kaButton smallButton rightButton" href="/faces-the-complete-faces-1971-1973-2014-24-96-hd-flac-t11726951.html#comment">6 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/faces-the-complete-faces-1971-1973-2014-24-96-hd-flac-t11726951.html" class="cellMainLink">Faces - The Complete Faces 1971-1973 (2014) [24-96 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="3542944213">3.3 <span>GB</span></td>
			<td class="center">61</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T14:45:28+00:00">09 Dec 2015, 14:45:28</span></td>
			<td class="green center">118</td>
			<td class="red lasttd center">52</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                						
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/elvis-presley-elvis-ultimate-christmas-2cd-2015-flac-mr-p2d-t11738195.html" class="cellMainLink">Elvis Presley - Elvis Ultimate Christmas [2CD] (2015) FLAC [Mr.P2d]</a></div>
			</td>
			<td class="nobr center" data-sort="601929507">574.04 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T12:10:27+00:00">11 Dec 2015, 12:10:27</span></td>
			<td class="green center">125</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11725576,0" class="icommentjs kaButton smallButton rightButton" href="/erykah-badu-but-you-caint-use-my-phone-2015-flac-t11725576.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/erykah-badu-but-you-caint-use-my-phone-2015-flac-t11725576.html" class="cellMainLink">Erykah Badu - But You Caint Use My Phone (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="215233773">205.26 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T08:48:49+00:00">09 Dec 2015, 08:48:49</span></td>
			<td class="green center">128</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11729561,0" class="icommentjs kaButton smallButton rightButton" href="/oscar-peterson-plays-porgy-bess-2015-24-192-hd-flac-t11729561.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/oscar-peterson-plays-porgy-bess-2015-24-192-hd-flac-t11729561.html" class="cellMainLink">Oscar Peterson - Plays Porgy &amp; Bess (2015) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1912661923">1.78 <span>GB</span></td>
			<td class="center">27</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T22:06:11+00:00">09 Dec 2015, 22:06:11</span></td>
			<td class="green center">115</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11739157,0" class="icommentjs kaButton smallButton rightButton" href="/va-winter-chillout-lounge-2015-2015-flac-t11739157.html#comment">1 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-winter-chillout-lounge-2015-2015-flac-t11739157.html" class="cellMainLink">VA - Winter Chillout Lounge 2015 (2015) FLAC</a></div>
			</td>
			<td class="nobr center" data-sort="2077857516">1.94 <span>GB</span></td>
			<td class="center">35</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-11T16:34:50+00:00">11 Dec 2015, 16:34:50</span></td>
			<td class="green center">100</td>
			<td class="red lasttd center">65</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11734964,0" class="icommentjs kaButton smallButton rightButton" href="/shostakovich-cello-concertos-1-2-truls-mÃ¸rk-mariss-jansons-london-philharmonic-t11734964.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/shostakovich-cello-concertos-1-2-truls-mÃ¸rk-mariss-jansons-london-philharmonic-t11734964.html" class="cellMainLink">Shostakovich - Cello Concertos 1 &amp; 2 - Truls MÃ¸rk, Mariss Jansons, London Philharmonic</a></div>
			</td>
			<td class="nobr center" data-sort="241894277">230.69 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T20:38:11+00:00">10 Dec 2015, 20:38:11</span></td>
			<td class="green center">121</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11723149,0" class="icommentjs kaButton smallButton rightButton" href="/cream-the-very-best-of-1995-flac-soup-t11723149.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/cream-the-very-best-of-1995-flac-soup-t11723149.html" class="cellMainLink">Cream - The Very Best Of (1995) FLAC Soup</a></div>
			</td>
			<td class="nobr center" data-sort="475263851">453.25 <span>MB</span></td>
			<td class="center">31</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T21:12:41+00:00">08 Dec 2015, 21:12:41</span></td>
			<td class="green center">119</td>
			<td class="red lasttd center">9</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11723014,0" class="icommentjs kaButton smallButton rightButton" href="/john-lennon-the-alternate-walls-and-bridges-2005-flac-t11723014.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/john-lennon-the-alternate-walls-and-bridges-2005-flac-t11723014.html" class="cellMainLink">John Lennon - The Alternate Walls And Bridges (2005) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="445915665">425.26 <span>MB</span></td>
			<td class="center">36</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-08T20:36:49+00:00">08 Dec 2015, 20:36:49</span></td>
			<td class="green center">104</td>
			<td class="red lasttd center">17</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11725995,0" class="icommentjs kaButton smallButton rightButton" href="/third-world-reggae-greats-flac-tntvillage-t11725995.html#comment">3 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/third-world-reggae-greats-flac-tntvillage-t11725995.html" class="cellMainLink">Third World - Reggae Greats [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="254506821">242.72 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-09T10:29:06+00:00">09 Dec 2015, 10:29:06</span></td>
			<td class="green center">97</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">
                	<a rel="11732295,0" class="icommentjs kaButton smallButton rightButton" href="/paolo-conte-concerti-flac-tntvillage-t11732295.html#comment">2 <i class="ka ka-comment"></i></a>					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/paolo-conte-concerti-flac-tntvillage-t11732295.html" class="cellMainLink">Paolo Conte - Concerti [Flac][TntVillage]</a></div>
			</td>
			<td class="nobr center" data-sort="547894655">522.51 <span>MB</span></td>
			<td class="center">35</td>
			<td class="center"><span class="timeago" data-age=1 title="2015-12-10T10:21:18+00:00">10 Dec 2015, 10:21:18</span></td>
			<td class="green center">64</td>
			<td class="red lasttd center">10</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+((i+1)%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div  data-sc-slot="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="spareBlock">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div  data-sc-slot="_7063408f1c01d50e0dc2d833186ce962" data-sc-params="{ 'searchQuery': '' }"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/daz-studio-and-poser-stuff-request-thread-v2/?unread=17204691">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Daz Studio and Poser Stuff - Request Thread V2
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/Dreamstar9/">Dreamstar9</a></span></span> <time class="timeago" datetime="2015-12-12T17:07:03+00:00">12 Dec 2015, 17:07</time></span>
	</li>
		<li>
		<a href="/community/show/club-protocol-uploads/?unread=17204690">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				&quot;Club Protocol&quot; Uploads
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/pradyutvam2/">pradyutvam2</a></span></span> <time class="timeago" datetime="2015-12-12T17:07:01+00:00">12 Dec 2015, 17:07</time></span>
	</li>
		<li>
		<a href="/community/show/ae-releases/?unread=17204687">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				AE Releases
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/AdiEnt/">AdiEnt</a></span></span> <time class="timeago" datetime="2015-12-12T17:06:18+00:00">12 Dec 2015, 17:06</time></span>
	</li>
		<li>
		<a href="/community/show/stargate-thread/?unread=17204677">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Stargate Thread
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_2"><a class="plain" href="/user/Bretts_Dr/">Bretts_Dr</a></span></span> <time class="timeago" datetime="2015-12-12T17:03:06+00:00">12 Dec 2015, 17:03</time></span>
	</li>
		<li>
		<a href="/community/show/latest-book-uploads-v3/?unread=17204674">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Latest Book Uploads V3
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/pradyutvam2/">pradyutvam2</a></span></span> <time class="timeago" datetime="2015-12-12T17:02:04+00:00">12 Dec 2015, 17:02</time></span>
	</li>
		<li>
		<a href="/community/show/kat-tv-uploaders-unite-post-your-dmca-d-torrents-here/?unread=17204670">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				KAT TV Uploaders Unite! Post your DMCA&#039;d Torrents Here!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/JoyBell/">JoyBell</a></span></span> <time class="timeago" datetime="2015-12-12T17:00:22+00:00">12 Dec 2015, 17:00</time></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/new-site-rules/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				New site Rules
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-15T14:18:43+00:00">15 Oct 2015, 14:18</time></span>
	</li>
	<li>
		<a href="/blog/post/look-mama-i-m-popular/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Look, mama, I&#039;m popular!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-10-05T17:42:40+00:00">05 Oct 2015, 17:42</time></span>
	</li>
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <time class="timeago" datetime="2015-09-01T16:13:36+00:00">01 Sep 2015, 16:13</time></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/OptimusPr1me/post/war-what-is-it-good-for/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> War, what IS it good for ..</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/OptimusPr1me/">OptimusPr1me</a> <time class="timeago" datetime="2015-12-12T10:02:48+00:00">12 Dec 2015, 10:02</time></span></li>
	<li><a href="/blog/SantaPringles/post/pringlescan-anime-recommendations-vol-35-wicked-city-movie/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Pringlescan Anime Recommendations Vol. 35 Wicked City(movie)</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/SantaPringles/">SantaPringles</a> <time class="timeago" datetime="2015-12-12T08:53:54+00:00">12 Dec 2015, 08:53</time></span></li>
	<li><a href="/blog/Humbug60/post/trai-takes-second-stab-at-net-neutrality-in-new-consultation-paper-hosted-blog-for-grinch-15/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> TRAI Takes Second Stab at Net Neutrality in New Consultation Paper (hosted blog for Grinch.15 )</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/Humbug60/">Humbug60</a> <time class="timeago" datetime="2015-12-12T00:24:46+00:00">12 Dec 2015, 00:24</time></span></li>
	<li><a href="/blog/TheDels/post/civilization-v-review/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Civilization V Review</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/TheDels/">TheDels</a> <time class="timeago" datetime="2015-12-11T14:02:59+00:00">11 Dec 2015, 14:02</time></span></li>
	<li><a href="/blog/Bubanee/post/christmas-collection-both-flac-and-mp3/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Christmas Collection both FLAC and MP3.</p></a><span class="explanation">by <a class="plain aclColor_3" href="/user/Bubanee/">Bubanee</a> <time class="timeago" datetime="2015-12-11T12:40:59+00:00">11 Dec 2015, 12:40</time></span></li>
	<li><a href="/blog/Ultragrinch/post/news-break-new-year-s-day-has-been-moved-to-today-dascubadude/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> News Break: New Year&#039;s Day has been moved to Today(DaScubaDude)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Ultragrinch/">Ultragrinch</a> <time class="timeago" datetime="2015-12-11T08:00:12+00:00">11 Dec 2015, 08:00</time></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/code%20black/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				code black
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20challenge%20bloodline/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the challenge bloodline
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/medusa/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				medusa
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/mp4%20movies/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Mp4 movies
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/gilf/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				gilf
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/fares/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				fares
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/karbala%20film/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				karbala film
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/teenmarvel/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				TeenMarvel
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20incredible%20hulk/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the incredible hulk
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%2Bloft/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the+loft
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/transporter%203%202008/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Transporter 3 2008
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i class="sliderBoxToggle ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a href="#" onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a href="#" onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a href="#" onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a href="#" onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a href="#" onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a href="#" onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a href="#" onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a href="#" onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a href="#" onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a href="#" onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a href="#" onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a href="#" onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a href="#" onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a href="#" onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a href="#" onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a href="#" onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a href="#" onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a href="#" onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a href="#" onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a href="#" onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a href="#" onclick="setLanguage('he', '.kat.cr');return false;" class="plain">Hebrew</a></li>
                                <li><a href="#" onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a href="#" onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a href="#" onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a href="#" onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a href="#" onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a href="#" onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a href="#" onclick="setLanguage('lv', '.kat.cr');return false;" class="plain">Latvian</a></li>
                                <li><a href="#" onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a href="#" onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a href="#" onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a href="#" onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a href="#" onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a href="#" onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a href="#" onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a href="#" onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a href="#" onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a href="#" onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a href="#" onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a href="#" onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a href="#" onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a href="#" onclick="setLanguage('bsc', '.kat.cr');return false;" class="plain">Serbian-Cyrillic (ijekavica)</a></li>
                                <li><a href="#" onclick="setLanguage('si', '.kat.cr');return false;" class="plain">Sinhala</a></li>
                                <li><a href="#" onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a href="#" onclick="setLanguage('sl', '.kat.cr');return false;" class="plain">Slovenian</a></li>
                                <li><a href="#" onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a href="#" onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a href="#" onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a href="#" onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a href="#" onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a href="#" onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a href="#" onclick="setLanguage('ur', '.kat.cr');return false;" class="plain">Urdu</a></li>
                                <li><a href="#" onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" data-nop href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
        		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
        <li><a href="https://kastatus.com">KAT status</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div  data-sc-slot="_673e31f53f8166159b8e996c4124765b"></div>
        <div  data-sc-slot="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Organization",
"name": "KickassTorrents",
"url": "https://kat.cr",
"logo": "https://kat.cr/content/images/kickasslogo.png",
"sameAs": [
    "https://www.facebook.com/Official.KAT.Fanclub",
    "https://twitter.com/kickasstorrents"
]
}
</script>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "WebSite",
"url": "https://kat.cr",
"potentialAction": {
    "@type": "SearchAction",
    "target": "https://kat.cr/usearch/{q}/",
    "query-input": {
        "@type": "PropertyValueSpecification",
        "valueRequired": true,
        "valueName": "q"
    }
}
}
</script>
<script>
 sc('addGlobal', 'pagetype', 'front');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
