<!DOCTYPE html>
<html id="atomic" lang="en-US" class="atomic my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr desktop Desktop bkt201">
<head>
    <title>Yahoo</title><meta http-equiv="x-dns-prefetch-control" content="on"><link rel="dns-prefetch" href="//s.yimg.com"><link rel="preconnect" href="//s.yimg.com"><link rel="dns-prefetch" href="//y.analytics.yahoo.com"><link rel="preconnect" href="//y.analytics.yahoo.com"><link rel="dns-prefetch" href="//geo.query.yahoo.com"><link rel="preconnect" href="//geo.query.yahoo.com"><link rel="dns-prefetch" href="//csc.beap.bc.yahoo.com"><link rel="preconnect" href="//csc.beap.bc.yahoo.com"><link rel="dns-prefetch" href="//geo.yahoo.com"><link rel="preconnect" href="//geo.yahoo.com"><link rel="dns-prefetch" href="//comet.yahoo.com"><link rel="preconnect" href="//comet.yahoo.com">    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="description" content="News, email and search are just the beginning. Discover more every day. Find your yodel.">
    <meta name="keywords" content="yahoo, yahoo home page, yahoo homepage, yahoo search, yahoo mail, yahoo messenger, yahoo games, news, finance, sport, entertainment">
    <meta property="og:title" content="Yahoo" />
    <meta property="og:type" content='website' />
    <meta property="og:url" content="http://www.yahoo.com" />
    <meta property="og:description" content="News, email and search are just the beginning. Discover more every day. Find your yodel."/>
    <meta property="og:image" content="https://s.yimg.com/dh/ap/default/130909/y_200_a.png"/>
    <meta property="og:site_name" content="Yahoo" />
    <meta property="fb:app_id" content="90376669494" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="icon" sizes="any" mask href="/sy/os/mit/media/p/common/images/favicon_new-7483e38.svg">
<meta name="theme-color" content="#400090">
    <link rel="shortcut icon" href="/sy/rz/l/favicon.ico" />
    <link rel="canonical" href="https://www.yahoo.com/" />        <link href="/sy/os/fp/atomic-css.271ed811.css" rel="stylesheet" type="text/css">
            
    
    
    
    <!-- streaming unlocked -->

    <!-- MapleTop -->
    
    
<link rel="stylesheet" type="text/css" href="/sy/zz/combo?nn/lib/metro/g/myy/advance_base_rc4_0.0.36.css&nn/lib/metro/g/myy/font_rc4_spdy_0.0.35.css&nn/lib/metro/g/myy/yahoo20_grid_0.0.101.css&nn/lib/metro/g/myy/video_styles_0.0.23.css&nn/lib/metro/g/myy/advance_color_0.0.7.css&nn/lib/metro/g/theme/viewer_modal_center_0.0.11.css&nn/lib/metro/g/theme/yglyphs-legacy_0.0.5.css&nn/lib/metro/g/sda/fp_sda_0.0.4.css&nn/lib/metro/g/sda/sda_advance_0.0.8.css&nn/lib/metro/g/fpfooter/advance_0.0.4.css&/os/stencil/3.1.0/styles-ltr.css&/os/yc/css/bundle.c60a6d54.css" />
    <link rel="search" type="application/opensearchdescription+xml" href="https://search.yahoo.com/opensearch.xml" title="Yahoo Search" />
    
    <script>
    var myYahoostartTime = new Date(),
        afPerfHeadStart=new Date().getTime(),
        ie;

    
    document.documentElement.className += ' JsEnabled jsenabled';</script>
    

<style>.breakingnews.gradient-1 {
    background: #ff7617; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #ff7617 0%, #ff9b00 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#ff7617), color-stop(65%,#ff9b00)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* IE10+ */
    background: linear-gradient(45deg,  #ff7617 0%,#ff9b00 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7617', endColorstr='#ff9b00',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}

.breakingnews.gradient-2 {
    background: #e91857; /* Old browsers */
    background: -moz-linear-gradient(45deg,  #e91857 0%, #ff353c 65%); /* FF3.6+ */
    background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#e91857), color-stop(65%,#ff353c)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* Opera 11.10+ */
    background: -ms-linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* IE10+ */
    background: linear-gradient(45deg,  #e91857 0%,#ff353c 65%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e91857', endColorstr='#ff353c',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}</style>
<style>.js-stream-adfdb-reason {
    display: none;
}
.js-stream-adfdb-options .js-stream-adfdb-other-selected + .js-stream-adfdb-reason {
    display: block;
}

.stream-collapse {
    max-height: 0;
    -webkit-transition: max-height 0.3s ease;
    -moz-transition: max-height 0.3s ease;
    -o-transition: max-height 0.3s ease;
    transition: max-height 0.3s ease;
}

.RevealNested-on .ActionDislike {
    display: none;
}

.streamv2 .stream-share-open .js-stream-share-panel {
    height: auto !important;
    opacity: 1 !important;
}

/**
* Tooltips
*/
.js-stream-actions a:hover .ActionTooltip.hide,
.js-stream-actions .ActionTooltip,
.js-stream-actions a:active .ActionTooltip,
.js-stream-actions a:focus .ActionTooltip {
    clip: rect(1px 1px 1px 1px);
    clip: rect(1px,1px,1px,1px);
    height: 1px;
    width: 1px;
    overflow: hidden;
    line-height: 1.4;
    white-space: nowrap
}

.js-stream-actions a:hover .ActionTooltip {
    clip: rect(auto auto auto auto);
    clip: auto;
    height: auto;
    width: auto;
    overflow: visible;
    -webkit-transform: translateX(-50%) !important;
    -ms-transform: translateX(-50%) !important;
    transform: translateX(-50%) !important;
    *min-width: 80px;
    *margin-right: -48px;
    width: 136px;
    text-align: center;
}

.streamv2 .js-stream-dense .strm-left {
    max-width: 190px;
    width: 29%;
}

.streamv2 .js-stream-sparse .strm-left {
    width: 33%;
    max-width: 230px;
}
.streamv2 .js-stream-sparse .strm-right {
    width: 57%;
}
.streamv2 .js-stream-sparse .strm-full {
    width: 90%;
}

.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-left {
    width: 62.825%;
    max-width: 441px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-left {
    width: 72%;
    max-width: 508px;
}

.streamv2 .js-stream-dense .strm-right-menu-roundup .strm-right,
.streamv2 .js-stream-dense .strm-gs-tile-roundup .strm-right {
    width: auto;
    max-width: none;
}


.streamv2 .strm-right-menu-roundup .strm-right .js-stream-content-link:before {
    content: '';
    vertical-align: middle;
    height: 100%;
    display: inline-block;
}

.streamv2 .strm-chevron {
    display: none;
}

.streamv2 .strm-right-menu-roundup .js-stream-content-link.selected .strm-chevron,
.streamv2 .strm-right-menu-roundup .js-stream-content-link:hover .strm-chevron {
    display: block;
}

.streamv2 .rounded-img {
    border-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-main-img .rounded-img {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.streamv2 .js-stream-sparse .storyline-img-0 {
    border-bottom-left-radius: 3px;
}
.streamv2 .js-stream-sparse .storyline-img-1 {
    border-bottom-right-radius: 3px;
}

.ua-ff#atomic .strm-headline-label {
    margin-bottom: 4px;
}
/* cluster image gradient transparent to dark overlay */
.streamv2 .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 32%, rgba(0,0,0,0.65) 97%, rgba(0,0,0,0.65) 98%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(32%,rgba(0,0,0,0)), color-stop(97%,rgba(0,0,0,0.65)), color-stop(98%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 32%,rgba(0,0,0,0.65) 97%,rgba(0,0,0,0.65) 98%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

/* cluster image gradient transparent to dark overlay */
.streamv2 .js-stream-roundup .strm-img-gradient {
    background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 52%, rgba(0,0,0,0.85) 107%, rgba(0,0,0,0.85) 78%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(52%,rgba(0,0,0,0)), color-stop(107%,rgba(0,0,0,0.85)), color-stop(78%,rgba(0,0,0,0.85))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 52%,rgba(0,0,0,0.85) 107%,rgba(0,0,0,0.85) 78%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
}

.streamv2 .strm-upsell-gradient-right {
    background: -moz-linear-gradient(right,  rgba(255,255,255,0.005) 0%, rgba(255,255,255,0.90) 85%, rgba(255,255,255,0.99) 98%); /* FF3.6+ */
    background: -webkit-linear-gradient(right,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(right,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(right,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* IE10+ */
    background: linear-gradient(to right, rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* W3C */
}
.streamv2 .strm-upsell-gradient-left {
    background: -moz-linear-gradient(left,  rgba(255,255,255,0.005) 0%, rgba(255,255,255,0.90) 85%, rgba(255,255,255,0.99) 98%); /* FF3.6+ */
    background: -webkit-linear-gradient(left,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(left,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* Opera 11.10+ */
    background: -ms-linear-gradient(left,  rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* IE10+ */
    background: linear-gradient(to left, rgba(255,255,255,0.005) 0%,rgba(255,255,255,0.90) 85%,rgba(255,255,255,0.99) 98%); /* W3C */
}

.ua-ie10 .streamv2 .js-stream-related-content ul,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip > ul {
    margin-right: -7px !important;
}

.ua-ie10 .streamv2 .js-stream-related-content li,
.ua-ie10 .streamv2 .js-stream-roundup-filmstrip .W\(25\%\) {
    width: 24.5% !important;
}

.streamv2 .js-stream-side-buttons li:nth-child(1) .ActionTooltip {
    top: -1px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(1) .ActionTooltip {
    top: 0px;
}

.streamv2 .js-stream-side-buttons li:nth-child(2) .ActionTooltip {
    top: 22px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(2) .ActionTooltip {
    top: 40px;
}
.streamv2 .js-stream-side-buttons li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons li:nth-child(3) .js-stream-share-panel {
    top: 45px;
}
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .ActionTooltip,
.streamv2 .js-stream-side-buttons.has-comments li:nth-child(3) .js-stream-share-panel {
    top: 64px;
}
.streamv2 .js-stream-side-buttons li:nth-child(4) .js-stream-share-panel {
    top: 86px;
}

.streamv2 .js-stream-side-buttons .ActionComments {
    margin-top: 6px;
}
.streamv2 .js-stream-side-buttons.has-comments .ActionComments {
    margin-top: 0px;
}
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\$c_icon\),
.streamv2 .js-stream-side-buttons .ActionComments:hover .C\(\#96989f\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\$c_icon\),
.streamv2 .js-stream-drawer .js-stream-comments-button:hover .C\(\#96989f\) {
    color: inherit !important;
}
.streamv2 .js-stream-side-buttons .ActionComments .ActionTooltip {
    margin-top: 15px;
}

.js-stream-comment-counter-update {
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
}

/* Only set opacity to 0 for animation when js is enabled and css3 supported */
.JsEnabled .streamv2 .js-stream-comment-hidden:nth-of-type(1n) {
    opacity: 0;
}

.JsEnabled .streamv2 .animated {
    -webkit-animation-duration: 1.5s;
    animation-duration: 1.5s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
}

@-webkit-keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

@keyframes fadeOut {
    from {
        opacity: 1;
    }
    to {
        opacity: 0;
    }
}

.JsEnabled .streamv2 .fadeOut {
    -webkit-animation-name: fadeOut;
    animation-name: fadeOut;
}

@-webkit-keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@keyframes fadeIn {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #96989f;
    }
}

@-webkit-keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

@keyframes fadeInNtk {
    from {
        opacity: 0;
        color: #188fff;
    }
    to {
        opacity: 1;
        color: #fff;
    }
}

.JsEnabled .streamv2 .fadeIn {
    -webkit-animation-name: fadeIn;
    animation-name: fadeIn;
}


.JsEnabled .streamv2 .js-stream-roundup-filmstrip .fadeIn {
    -webkit-animation-name: fadeInNtk;
    animation-name: fadeInNtk;
}

.streamv2 .js-stream-comment .js-stream-cmnt-up:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-up.selected {
    color: #1AC567 !important;
}
.streamv2 .js-stream-comment .js-stream-cmnt-down:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-down.selected,
.streamv2 .js-stream-comment .js-stream-cmnt-flag:hover,
.streamv2 .js-stream-comment .js-stream-cmnt-flag.selected {
    color: #F0162F !important;
}

/* to show the dislike ad tooltip */
#atomic .Collapse-opened .js-stream-related-item-ad {
    overflow: visible;
}

.js-stream-upsell-carousel .js-stream-upsell-left.Disabled,
.js-stream-upsell-carousel .js-stream-upsell-right.Disabled,
.js-stream-upsell-carousel .strm-upsell-gradient-left.Disabled,
.js-stream-upsell-carousel .strm-upsell-gradient-right.Disabled {
    display: none;
}
.js-stream-upsell-carousel .js-stream-upsell-left,
.js-stream-upsell-carousel .js-stream-upsell-right {
    opacity: 0;
}
.js-stream-upsell-carousel:hover .js-stream-upsell-left,
.js-stream-upsell-carousel:hover .js-stream-upsell-right {
    opacity: 0.7;
}
.js-stream-upsell-carousel .js-stream-upsell-left:hover,
.js-stream-upsell-carousel .js-stream-upsell-right:hover {
    opacity: 1;
}

.ua-ie .streamv2 .strm-stretch {
    background-color: #fff;
    opacity: 0;
    filter: alpha(opacity=1);
}
</style>
 

<style>.js-activitylist-item .orb {
    background-position: 0 0;
    width: 80px;
    height: 80px;
}

.js-activitylist-item .hexagon {
    background-position: -81px 0;
    width: 80px;
    height: 90px;
}

.js-activitylist-item .ribbon {
    background-position: -162px 0;
    width: 91px;
    height: 31px;
}
</style>
 

<style>#uh-search .yui3-aclist {
    width: inherit !important;
}
#uh-search .yui3-aclist-content {
    background-color: #fff;
    text-align: left;
    border: 1px solid #ccc;
    margin-top: 1px;
    border-top: 0;
}
#uh-search .yui3-aclist-list {
    margin: 0;
    line-height: 1.1;
}
#uh-search .yui3-aclist-item {
    padding: 5px 0 6px 0;
    font-size: 18px;
    font-weight: bold;
}
#uh-search .yui3-aclist-item:hover,
#uh-search .yui3-aclist-item-active {
    background-color: #c6d7ff;
}
#uh-search .yui3-aclist-item {
    padding-left: 10px;
    padding-right: 10px;
}
#uh-search .yui3-highlight {
    font-weight: 200;
}
#uh-search-box::-ms-clear {
    display: none;
}


/* Instant Search Styles */

#uh-search .yui3-aclist-item {
    line-height: 0.9;
}
#InstantSearchMask {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s linear 0.15s, opacity 0.15s linear;
}
.iSearch-display .Col1,
.iSearch-display .Col2,
.iSearch-display .Col3 {
    height: 0;
    overflow: hidden;
}
.iSearch-display #InstantSearch {
    visibility: visible;
}
.iSearch-mask #InstantSearchMask {
    visibility: visible;
    opacity: 1;
    transition: visibility 0s linear, opacity 0.15s linear;
}
.iSearch-loading.iSearch-mask #InstantSearchMask,
.iSearch-loading #InstantSearchMask {
    visibility: visible;
    opacity: .7;
}


/* Firefox 28 and below fix */
#uh-search-box,
#uh-ghost-box,
.instant-filters,
.instant-results {
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
/* ----------------------- */
</style>
 

<style>@charset "UTF-8";.Lb-Close-Icon,.icon-share-close,.icons-arrow,.icons-slideshow-icon{background-color:transparent;background-image:url(/sy/os/publish-images/news/2014-04-23/65fcff60-cb23-11e3-bead-55c0602d5659_icons-sb930b067ee.png);background-repeat:no-repeat}#Share .icon-share-close{width:30px;height:25px;background-size:cover;background-position:8px -72px}.Hl-Viewer{background:#fff}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay{position:absolute;color:#fff;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodâ¦EiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDAwMDAiIHN0b3Atb3BhY2l0eT0iMC45NSIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==);background:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,rgba(0,0,0,0)),color-stop(100%,rgba(0,0,0,.95)));background:-moz-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:-webkit-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95));background:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.95))}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline{text-shadow:0 1px 0 #000}.Hl-Viewer .Card-Item .Lead-l~.Cover-Overlay .Headline-Box{position:absolute}.Hl-Viewer .Content-Body{color:#000;font-weight:300;letter-spacing:.3px;word-wrap:break-word;word-break:break-word}.Hl-Viewer .Content-Body>p{margin-bottom:1.45em;margin-top:1.45em}.Hl-Viewer .Content-Body h1,.Hl-Viewer .Content-Body h2,.Hl-Viewer .Content-Body h3,.Hl-Viewer .Content-Body h4,.Hl-Viewer .Content-Body h5,.Hl-Viewer .Content-Body h6{font-weight:400}.Hl-Viewer .Content-Body h1{font-size:18px;font-size:1.8rem}.Hl-Viewer .Content-Body h2{font-size:17px;font-size:1.7rem}.Hl-Viewer .Content-Body h3{font-size:16px;font-size:1.6rem}.Hl-Viewer .Content-Body h4{font-size:15px;font-size:1.5rem}.Hl-Viewer .Content-Body h5{font-size:14px;font-size:1.4rem}.Hl-Viewer .Content-Body h6{font-size:13px;font-size:1.3rem}.Hl-Viewer .icons-slideshow-icon{left:5%;top:5%;height:46px;width:47px;background-size:cover}.Hl-Viewer blockquote{padding:5px 10px;quotes:"\201C" "\201C" "\201C" "\201C";line-height:18px}.Hl-Viewer blockquote:before{color:#ccc;content:open-quote;font-size:3em;line-height:.1em;vertical-align:-.4em}.Hl-Viewer blockquote p{display:inline;color:#747474;font-weight:400}.Hl-Viewer blockquote p:after{content:"\A";white-space:pre}.Hl-Viewer .Credit,.Hl-Viewer .Provider{letter-spacing:.5px}.JsEnabled .ImageLoader-Delayed,.JsEnabled .ImageLoader-Loaded{-moz-transition-duration:.2s;-o-transition-duration:.2s;-webkit-transition-duration:.2s;transition-duration:.2s;-moz-transition-property:opacity;-o-transition-property:opacity;-webkit-transition-property:opacity;transition-property:opacity;-moz-transition-timing-function:ease-out;-o-transition-timing-function:ease-out;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}#Stencil .Bdrs-100{border-top-left-radius:100px;border-top-right-radius:100px;border-bottom-left-radius:100px;border-bottom-right-radius:100px}.Slideshow-Lightbox .lb-meta-content{padding-bottom:30px;background:#fff}.Slideshow-Lightbox .lb-meta-content .lb-meta-txt-container-full,.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-short{display:none}.Slideshow-Lightbox .lb-meta-content.expand .lb-meta-txt-container-full{display:block}.Slideshow-Lightbox .hide-meta .lb-meta-content{display:none}.Slideshow-Lightbox .Lb-Close-Icon{background-size:cover;width:47px;height:47px;z-index:202;background-position:0 -46px}.Reader-open .BrandBar,.Reader-open .rmp-TDYDBM{display:none}.Content-Body .Editorial-Left{float:left;margin-right:30px}.Content-Body .Editorial-Right{float:right;margin-left:30px}.Content-Body .Editorial-Left,.Content-Body .Editorial-Right{margin-top:0}#hl-viewer .Content-Col,.MagOn .Content-Col{margin-left:300px;margin-right:300px;min-height:100px}.HideCentralColumn .Content-Col{margin-left:150px;margin-right:150px}#hl-viewer{min-height:500px}.ShareBtns a:hover{opacity:.3;filter:alpha(opacity=33)}.modal-actions{bottom:3px}.modal-actions .ShareBtns a:hover{opacity:1;filter:alpha(opacity=100)}#hl-viewer:focus{outline:0}#hl-viewer .Timestamp{color:#abaeb7}#hl-viewer .Viewer-Close-Btn{position:fixed;top:10px;width:28px;height:28px;color:#fff;background-color:#2D1152;font-size:16px;margin-left:11px;z-index:5}#hl-viewer .ShareBtns .Share-Btn{border-radius:50px;width:17px;height:17px;line-height:1.3}#hl-viewer .ShareBtns .Share-Btn:hover{text-decoration:none}#hl-viewer .slideshow-carousel{background:#212124;padding-bottom:66%}#hl-viewer .lb-meta-content{border-bottom:1px solid #e8e8e8}#hl-viewer .lb-meta-caption-container{-webkit-font-smoothing:antialiased}#hl-viewer .lb-meta-txt-container.caption-scroll{max-height:4.3em;overflow-y:auto}#hl-viewer .viewer-wrapper{background:#fff;margin:0 0 30px 58px;min-width:980px}#hl-viewer .viewer-wrapper.mega-modal{min-height:750px}#hl-viewer .content-modal,#hl-viewer .js-viewer-slot-readMore{display:inline-block;width:640px;margin-right:29px;vertical-align:top}#hl-viewer .js-slider-item.first .content-modal{padding-top:0}#hl-viewer .Content-Body{padding-bottom:50px;border-bottom:1px solid #c8c8c8}#hl-viewer .Content-Body i{font-style:italic}#hl-viewer .js-slider-item.last .Content-Body{padding-bottom:0;border-bottom:none}#hl-viewer .Content-Body p:last-child{margin-bottom:0!important}#hl-viewer .js-slider-item.multirightrail .Content-Body.multirightrail{border-bottom:none}#hl-viewer .js-slider-item.multirightrail .content-modal{padding-top:0}#hl-viewer .js-slider-item.multirightrail{border-top:1px solid #B2B2B2;padding-top:50px}#hl-viewer .js-slider-item.multirightrail.first{padding-top:0;border-top:none}#hl-viewer .modal-aside.single{position:absolute;right:0;float:none;width:300px}#hl-viewer .modal-aside{display:inline-block;width:300px;position:relative;vertical-align:top;z-index:1}#hl-viewer .slideshow-carousel .main-col{padding-top:15px}#hl-viewer .index-count{color:#878c91;position:absolute;left:12px;bottom:7px;-webkit-font-smoothing:antialiased}#hl-viewer .js-lb-next,#hl-viewer .js-lb-prev{top:50%;z-index:3;width:28px;color:#878c91;font-size:30px;margin-top:-17px}#hl-viewer .js-lb-next:hover,#hl-viewer .js-lb-prev:hover{color:#fff}#hl-viewer .js-lb-next.Disabled,#hl-viewer .js-lb-prev.Disabled{display:none}#hl-viewer .js-lb-next:focus,#hl-viewer .js-lb-prev:focus{outline:0}#hl-viewer .slideshowv2 .slv2-carousel{padding-bottom:78%}#hl-viewer .slideshowv2 .slv2-carousel .main-col{padding-top:20px;padding-bottom:60px}#hl-viewer .slideshowv2 .slv2-carousel .js-thm-sl-icon{color:#fff;font-size:25px;top:12px;display:none}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-slider-ct{background-color:rgba(33,33,35,.5);border-radius:10px}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-car-mask{width:300px;opacity:1}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .js-thm-sl-icon{display:inline-block}#hl-viewer .slideshowv2 .slv2-carousel .sl-thumb-wide .sl-pic{width:52px;height:52px}#hl-viewer .slideshowv2 .js-lb-next,#hl-viewer .slideshowv2 .js-lb-prev{opacity:.8;background:#444;color:#fff;width:45px;height:45px;padding:0;border-radius:3px}#hl-viewer .slideshowv2 .js-lb-next:hover,#hl-viewer .slideshowv2 .js-lb-prev:hover{background:#333;opacity:1}#hl-viewer .slideshowv2 .index-count{bottom:inherit;left:inherit;color:#000;position:relative;font-size:19px;font-size:1.9rem;font-weight:500;margin-bottom:8px}#hl-viewer .slideshowv2 .slide-title{font-weight:700;margin-bottom:4px}#hl-viewer .slideshowv2 .lb-meta-txt-container a:hover,#hl-viewer .slideshowv2 .lb-meta-txt-container a:link,#hl-viewer .slideshowv2 .lb-meta-txt-container a:visited{color:#188fff}#hl-viewer .slideshowv2 .sl-thumbnails{bottom:10px}#hl-viewer .slideshowv2 .sl-slider-ct{width:350px}#hl-viewer .slideshowv2 .sl-car-mask{width:150px;opacity:.5;transition:height .2s}#hl-viewer .slideshowv2 .sl-carousel .Selected .sl-pic{border:2px solid #fff}#hl-viewer .slideshowv2 .sl-carousel .sl-pic{width:22px;height:22px;border:2px solid transparent}#hl-viewer .Headline{line-height:1.2;margin-bottom:12px;font-size:30px;font-size:3rem}#hl-viewer .Headline-Box{margin-bottom:10px}#hl-viewer .Off-Network{border:1px solid #188FFF;padding:8px 20px;color:#188FFF!important;font-weight:700;text-decoration:none}#hl-viewer .Off-Network:hover{color:#fff!important;background:#0078ff;border-color:#0078ff}#hl-viewer .Content-Body p,#hl-viewer .remaining-body p{margin-bottom:1.1em;margin-top:0}#hl-viewer .Content-Body p p,#hl-viewer .remaining-body p p{margin:0}#hl-viewer img.Mb-20+p{margin-top:0}#hl-viewer .Content-Body,#hl-viewer .remaining-body{font-weight:400;font-size:15px;font-size:1.55rem;line-height:1.5;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .Content-Body #photo_copyright,#hl-viewer .Content-Body .caption,#hl-viewer .Content-Body .credit,#hl-viewer .Content-Body .source,#hl-viewer .remaining-body #photo_copyright,#hl-viewer .remaining-body .caption,#hl-viewer .remaining-body .credit,#hl-viewer .remaining-body .source{font-size:11px;font-size:1.1rem;color:#b2b2b2}#hl-viewer h2,#hl-viewer h3{font-size:22px;font-size:2.2rem;margin-bottom:.6em}#hl-viewer h4,#hl-viewer h5,#hl-viewer h6{font-size:19px;font-size:1.9rem;margin-bottom:.6em}#hl-viewer .Embed-Img{margin-bottom:20px}#hl-viewer .image{margin-bottom:20px;display:block;line-height:1}#hl-viewer .image .Embed-Img{margin-bottom:0}#hl-viewer .sidekick.fp h4{font-size:13px;font-size:1.3rem;margin-bottom:0}#hl-viewer .twitter-tweet-rendered{margin:10px auto}#hl-viewer #photo_copyright{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif!important}#hl-viewer .alignright{float:right;margin:0 0 10px 10px}#hl-viewer .alignleft{float:left;margin:0 10px 10px 0}#hl-viewer .pmc-related-type{margin-right:10px}#hl-viewer blockquote{line-height:1.6}#hl-viewer p:empty{display:none}#hl-viewer .Ad-Viewer blockquote p,#hl-viewer .Hl-Viewer blockquote p{display:block}#hl-viewer .hl-ad-LREC{height:250px}#hl-viewer .source{font-size:14px;font-size:1.4rem}#hl-viewer .attribution{font-size:12px}#hl-viewer .first .Fixed-Header{top:-100px}#hl-viewer .Fixed-Header{transition:opacity .8s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .8s ease-in-out,top .6s ease-in-out;position:fixed;top:0;width:600px;padding:14px 0 0}#hl-viewer .Fixed-Header .source{max-width:540px}#hl-viewer .Fixed-Header .modal-actions{top:11px;left:540px;width:100px!important}#hl-viewer .Fixed-Header-Wrap{position:fixed;top:-100px;width:100%;z-index:4;height:48px;background-color:#fff;margin-left:-57px;border-bottom:1px solid #e5e5e5;transition:opacity .6s ease-in-out,top .6s ease-in-out;-webkit-transition:opacity .6s ease-in-out,top .6s ease-in-out;opacity:0;box-shadow:0 2px 4px -1px rgba(0,9,30,.1)}#hl-viewer .fadein{opacity:1}#hl-viewer .backfill-video-ads{width:300px;height:169px}#hl-viewer .backfill-video-ads h3{font-size:16px;font-size:1.6rem}#hl-viewer .backfill-video-ads h4{font-size:15px;font-size:1.5rem}#hl-viewer .backfill-video-ads .yvp-setting-btn{display:none}#hl-viewer .SidekickTV .list-view-item{display:inline-block;zoom:1;letter-spacing:normal;word-spacing:normal;text-rendering:auto;vertical-align:top}#hl-viewer .SidekickTV .SidekickTVArrow{border-bottom:40px solid transparent;border-left:40px solid}#hl-viewer .SidekickTV .ad-sponsored{color:#959595;font-size:11px;margin-right:0;padding-right:0}#hl-viewer .continue_reading .arraw,#hl-viewer .js-viewer-view-article .arraw{border-left:5px solid transparent;border-right:5px solid transparent;border-top:6px solid #000;vertical-align:middle}#hl-viewer .continue_reading:hover{color:#188FFF}#hl-viewer .continue_reading:hover .arraw{border-top-color:#188FFF}#hl-viewer .Mt-neg-40{margin-top:-40px}#hl-viewer .Mt-neg-30{margin-top:-30px}#hl-viewer .modal-sidekick-following .sidekick.fp{margin-top:-20px}#hl-viewer figure{margin:0}#hl-viewer iframe{border:none}#hl-viewer #viewer-end{height:1px;width:1px;display:block}#hl-viewer #viewer-end:focus{outline:0}#hl-viewer iframe.modal-embed-iframe{width:100%;max-width:640px;height:0;transition:height .2s ease-in-out}.ShareBtns .Share-Btn{padding:5px;border:1px solid #abaeb7}.ShareBtns .Share-Btn:hover{opacity:1;filter:alpha(opacity=100)}.ShareBtns .Tumblr-Btn{color:#35506d}.ShareBtns .Tumblr-Btn:hover{color:#35506d;background-color:#3593d3}.ShareBtns .Facebook-Btn{color:#3b5998}.ShareBtns .Facebook-Btn:hover{color:#3b5998;background-color:#4e91f2}.ShareBtns .Twitter-Btn{color:#00aced}.ShareBtns .Twitter-Btn:hover{color:#00aced;background-color:#94e8ff}.ShareBtns .Mail-Btn{background-position:-1px 161px;color:#0a80e3}.ShareBtns .Mail-Btn:hover{color:#0a80e3;background-color:#94e8ff}.ShareBtns .StickyPholder{display:inline-block}.ShareBtns .Viewer-Close-Btn{background-position:0 185px;padding:5px;margin-left:40px}.ShareBtns .Viewer-Close-Btn.Sticky{margin-left:-19px}#ModalSticker{max-width:1180px}#ModalSticker.Sticky{display:block!important;opacity:1;filter:alpha(opacity=100)}#ModalSticker .content-modal{border-bottom-width:3px;border-color:#000}#ModalSticker .ShareBtns{position:relative;float:right;margin-top:5px}.Modal-Credit{max-width:200px}.video-stage{margin-right:240px}.video-side-stage{width:240px}.Nav-Start{display:none}.First-Item .Nav-Start{display:block}.First-Item .Nav-Next,.First-Item .Nav-Prev{display:none}.slide-nav-arrow{background-color:#009bfb}.slide-nav-arrow:hover{opacity:1;filter:alpha(opacity=100)}.modal-sidekick,.modal-sidekick-following{display:inline-block}.js-sidekick-container{border-top:1px solid #f1f1f5}.modal-tooltip:hover .modal-tooltip\:h_H\(a\){height:auto!important}.modal-tooltip:hover .modal-tooltip\:h_Op\(1\){opacity:1!important}.c-modal-red{color:#ff156f}.lrec-before-loading{width:300px;height:250px;border:1px solid #e0e4e9;-webkit-transition:height 1.5s;-moz-transition:height 1.5s;-ms-transition:height 1.5s;-o-transition:height 1.5s;transition:height 1.5s}.js-header-searchbox{display:none;position:absolute;height:30px;left:730px;top:9px;border-radius:2px}.js-header-searchbox .srch-input{width:268px;height:100%;line-height:inherit;vertical-align:top;outline:0;box-shadow:none;border:none;border:1px solid #b0b0b0;border-radius:2px 0 0 2px;padding:0 10px}.js-header-searchbox .srch-input:focus{border-color:#0179ff}.js-header-searchbox button{width:32px;height:100%;background-color:#0179ff;font-size:16px;font-weight:700;color:#fff;border-radius:0 2px 2px 0;margin-left:-4px}.Z-6{z-index:6}.Z-7{z-index:7}.Z-8{z-index:8}.Z-9{z-index:9}.fixZindex{z-index:5!important}.viewer-right .js-slider-item.multirightrail.first{margin-top:25px!important}.viewer-right .Viewer-Close-Btn{border-radius:20px!important;font-size:17px!important;height:37px!important;margin-left:-18px!important;top:87px!important;width:37px!important}.viewer-right .viewer-wrapper{border-radius:6px;margin:1px!important;padding:0 0 30px 40px!important}.viewer-right.stream-sparse .viewer-wrapper{top:-12px}@media screen and (min-width:1100px){.js-header-searchbox{display:inline-block}}.Pt-35{padding-top:35px}.Pt-7{padding-top:7px}.JsEnabled .ImageLoader-Delayed{background:#f5f5f5}</style>
 

<style>#applet_p_63794 .Bd-0{border:0}#applet_p_63794 .Bd-1{border-width:1px}#applet_p_63794 .Bdx-1{border-right-width:1px;border-left-width:1px}#applet_p_63794 .Bdy-1{border-top-width:1px;border-bottom-width:1px}#applet_p_63794 .Bd-t{border-top-width:1px}#applet_p_63794 .Bd-end{border-right-width:1px}#applet_p_63794 .Bd-b{border-bottom-width:1px}#applet_p_63794 .Bd-start{border-left-width:1px}#applet_p_63794 .Bdt-0{border-top:0}#applet_p_63794 .Bdend-0{border-right:0}#applet_p_63794 .Bdb-0{border-bottom:0}#applet_p_63794 .Bdstart-0{border-left:0}#applet_p_63794 .Bdrs-0{border-radius:0}#applet_p_63794 .Bdtrrs-0{border-top-right-radius:0}#applet_p_63794 .Bdtlrs-0{border-top-left-radius:0}#applet_p_63794 .Bdbrrs-0{border-bottom-right-radius:0}#applet_p_63794 .Bdblrs-0{border-bottom-left-radius:0}#applet_p_63794 .Bdrs-100{border-radius:100px}#applet_p_63794 .Bdtrrs-100{border-top-right-radius:100px}#applet_p_63794 .Bdtlrs-100{border-top-left-radius:100px}#applet_p_63794 .Bdbrrs-100{border-bottom-right-radius:100px}#applet_p_63794 .Bdblrs-100{border-bottom-left-radius:100px}#applet_p_63794 .Bdrs-300{border-radius:300px}#applet_p_63794 .Bdtrrs-300{border-top-right-radius:300px}#applet_p_63794 .Bdtlrs-300{border-top-left-radius:300px}#applet_p_63794 .Bdbrrs-300{border-bottom-right-radius:300px}#applet_p_63794 .Bdblrs-300{border-bottom-left-radius:300px}#applet_p_63794 .Bdrs{border-radius:3px}#applet_p_63794 .Bdtrrs{border-top-right-radius:3px}#applet_p_63794 .Bdtlrs{border-top-left-radius:3px}#applet_p_63794 .Bdbrrs{border-bottom-right-radius:3px}#applet_p_63794 .Bdblrs{border-bottom-left-radius:3px}#applet_p_63794 .Ff{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-a{font-family:Georgia,"Times New Roman",serif}#applet_p_63794 .Ff-b{font-family:Helvetica,Arial,sans-serif}#applet_p_63794 .Ff-c{font-family:"Monotype Corsiva","Comic Sans MS",cursive}#applet_p_63794 .Ff-d{font-family:Capitals,Impact,fantasy}#applet_p_63794 .Ff-e{font-family:Monaco,"Courier New",monospace}#applet_p_63794 .Fz-0{font-size:0}#applet_p_63794 .Fz-3xs{font-size:7px}#applet_p_63794 .Fz-2xs{font-size:9px}#applet_p_63794 .Fz-xs{font-size:11px;}#applet_p_63794 .Fz-s{font-size:13px;}#applet_p_63794 .Fz-m{font-size:15px;}#applet_p_63794 .Bgc-t{background-color:transparent}#applet_p_63794 .Bgi-n{background-image:none}#applet_p_63794 .Bg-n{background:0 0}#applet_p_63794 .Bgcp-bb{background-clip:border-box}#applet_p_63794 .Bgcp-pb{background-clip:padding-box}#applet_p_63794 .Bgcp-cb{background-clip:content-box}#applet_p_63794 .Bgo-pb{background-origin:padding-box}#applet_p_63794 .Bgo-bb{background-origin:border-box}#applet_p_63794 .Bgo-cb{background-origin:content-box}#applet_p_63794 .Bgz-a{background-size:auto}#applet_p_63794 .Bgz-ct{background-size:contain}#applet_p_63794 .Bgz-cv{background-size:cover;background-position:50% 15%}#applet_p_63794 .Bdcl-c{border-collapse:collapse}#applet_p_63794 .Bdcl-s{border-collapse:separate}#applet_p_63794 .Bxz-cb{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}#applet_p_63794 .Bxz-bb{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#applet_p_63794 .Bxsh-n{box-shadow:none}#applet_p_63794 .Cl-n{clear:none}#applet_p_63794 .Cl-b{clear:both}#applet_p_63794 .Cl-start{clear:left}#applet_p_63794 .Cl-end{clear:right}#applet_p_63794 .Cur-d{cursor:default}#applet_p_63794 .Cur-he{cursor:help}#applet_p_63794 .Cur-m{cursor:move}#applet_p_63794 .Cur-na{cursor:not-allowed}#applet_p_63794 .Cur-nsr{cursor:ns-resize}#applet_p_63794 .Cur-p{cursor:pointer}#applet_p_63794 .Cur-cr{cursor:col-resize}#applet_p_63794 .Cur-w{cursor:wait}#applet_p_63794 .Cur-a{cursor:auto!important}#applet_p_63794 .D-n{display:none}#applet_p_63794 .D-b{display:block}#applet_p_63794 .D-i{display:inline}#applet_p_63794 .D-ib{display:inline-block;*display:inline;zoom:1}#applet_p_63794 .D-tb{display:table}#applet_p_63794 .D-tbr{display:table-row}#applet_p_63794 .D-tbc{display:table-cell}#applet_p_63794 .D-li{display:list-item}#applet_p_63794 .D-ri{display:run-in}#applet_p_63794 .D-cp{display:compact}#applet_p_63794 .D-itb{display:inline-table}#applet_p_63794 .D-tbcl{display:table-column}#applet_p_63794 .D-tbclg{display:table-column-group}#applet_p_63794 .D-tbhg{display:table-header-group}#applet_p_63794 .D-tbfg{display:table-footer-group}#applet_p_63794 .D-tbrg{display:table-row-group}#applet_p_63794 .Fl-n{float:none}#applet_p_63794 .Fl-start{float:left;_display:inline}#applet_p_63794 .Fl-end{float:right;_display:inline}#applet_p_63794 .Fw-n{font-weight:400}#applet_p_63794 .Fw-b{font-weight:700;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}#applet_p_63794 .Fw-100{font-weight:100;*font-weight:normal}#applet_p_63794 .Fw-200{font-weight:200;*font-weight:normal}#applet_p_63794 .Fw-400{font-weight:400;*font-weight:normal}#applet_p_63794 .Fw-br{font-weight:bolder}#applet_p_63794 .Fw-lr{font-weight:lighter}#applet_p_63794 .Fs-n{font-style:normal}#applet_p_63794 .Fs-i{font-style:italic}#applet_p_63794 .Fv-sc{font-variant:small-caps}#applet_p_63794 .Fv-n{font-variant:normal}#applet_p_63794 .H-0{height:0}#applet_p_63794 .H-50{height:50%}#applet_p_63794 .H-100{height:100%}#applet_p_63794 .H-a{height:auto}#applet_p_63794 .H-n,#applet_p_63794 .h-n{-webkit-hyphens:none;-moz-hyphens:none;-ms-hyphens:none;hyphens:none}#applet_p_63794 .List-n{list-style-type:none}#applet_p_63794 .List-d{list-style-type:disc}#applet_p_63794 .List-c{list-style-type:circle}#applet_p_63794 .List-s{list-style-type:square}#applet_p_63794 .List-dc{list-style-type:decimal}#applet_p_63794 .List-dclz{list-style-type:decimal-leading-zero}#applet_p_63794 .List-lr{list-style-type:lower-roman}#applet_p_63794 .List-ur{list-style-type:upper-roman}#applet_p_63794 .Lisi-n{list-style-image:none}#applet_p_63794 .Lh-0{line-height:0}#applet_p_63794 .Lh-01{line-height:.1}#applet_p_63794 .Lh-02{line-height:.2}#applet_p_63794 .Lh-03{line-height:.3}#applet_p_63794 .Lh-04{line-height:.4}#applet_p_63794 .Lh-05{line-height:.5}#applet_p_63794 .Lh-06{line-height:.6}#applet_p_63794 .Lh-07{line-height:.7}#applet_p_63794 .Lh-08{line-height:.8}#applet_p_63794 .Lh-09{line-height:.9}#applet_p_63794 .Lh-1{line-height:1}#applet_p_63794 .Lh-11{line-height:1.1}#applet_p_63794 .Lh-12{line-height:1.2}#applet_p_63794 .Lh-125{line-height:1.25}#applet_p_63794 .Lh-13{line-height:1.3}#applet_p_63794 .Lh-14{line-height:1.4}#applet_p_63794 .Lh-15{line-height:1.5}#applet_p_63794 .Lh-16{line-height:1.6}#applet_p_63794 .Lh-17{line-height:1.7}#applet_p_63794 .Lh-18{line-height:1.8}#applet_p_63794 .Lh-19{line-height:1.9}#applet_p_63794 .Lh-2{line-height:2}#applet_p_63794 .Lh-21{line-height:2.1}#applet_p_63794 .Lh-22{line-height:2.2}#applet_p_63794 .Lh-23{line-height:2.3}#applet_p_63794 .Lh-24{line-height:2.4}#applet_p_63794 .Lh-25{line-height:2.5}#applet_p_63794 .Lh-3{line-height:3}#applet_p_63794 .Lh-reset{line-height:normal}#applet_p_63794 .M-0{margin:0}#applet_p_63794 .M-2{margin:2px}#applet_p_63794 .M-4{margin:4px}#applet_p_63794 .M-6{margin:6px}#applet_p_63794 .M-8{margin:8px}#applet_p_63794 .M-10{margin:10px}#applet_p_63794 .M-12{margin:12px}#applet_p_63794 .M-14{margin:14px}#applet_p_63794 .M-16{margin:16px}#applet_p_63794 .M-18{margin:18px}#applet_p_63794 .M-20{margin:20px}#applet_p_63794 .Mx-1{margin-right:1px;margin-left:1px}#applet_p_63794 .Mx-2{margin-right:2px;margin-left:2px}#applet_p_63794 .Mx-4{margin-right:4px;margin-left:4px}#applet_p_63794 .Mx-6{margin-right:6px;margin-left:6px}#applet_p_63794 .Mx-8{margin-right:8px;margin-left:8px}#applet_p_63794 .Mx-10{margin-right:10px;margin-left:10px}#applet_p_63794 .Mx-12{margin-right:12px;margin-left:12px}#applet_p_63794 .Mx-14{margin-right:14px;margin-left:14px}#applet_p_63794 .Mx-16{margin-right:16px;margin-left:16px}#applet_p_63794 .Mx-18{margin-right:18px;margin-left:18px}#applet_p_63794 .Mx-20{margin-right:20px;margin-left:20px}#applet_p_63794 .My-1{margin-top:1px;margin-bottom:1px}#applet_p_63794 .My-2{margin-top:2px;margin-bottom:2px}#applet_p_63794 .My-4{margin-top:4px;margin-bottom:4px}#applet_p_63794 .My-6{margin-top:6px;margin-bottom:6px}#applet_p_63794 .My-8{margin-top:8px;margin-bottom:8px}#applet_p_63794 .My-10{margin-top:10px;margin-bottom:10px}#applet_p_63794 .My-12{margin-top:12px;margin-bottom:12px}#applet_p_63794 .My-14{margin-top:14px;margin-bottom:14px}#applet_p_63794 .My-16{margin-top:16px;margin-bottom:16px}#applet_p_63794 .My-18{margin-top:18px;margin-bottom:18px}#applet_p_63794 .My-20{margin-top:20px;margin-bottom:20px}#applet_p_63794 .Mt-1{margin-top:1px}#applet_p_63794 .Mb-1{margin-bottom:1px}#applet_p_63794 .Mstart-1{margin-left:1px}#applet_p_63794 .Mend-1{margin-right:1px}#applet_p_63794 .Mt-2{margin-top:2px}#applet_p_63794 .Mb-2{margin-bottom:2px}#applet_p_63794 .Mstart-2{margin-left:2px}#applet_p_63794 .Mend-2{margin-right:2px}#applet_p_63794 .Mt-4{margin-top:4px}#applet_p_63794 .Mb-4{margin-bottom:4px}#applet_p_63794 .Mstart-4{margin-left:4px}#applet_p_63794 .Mend-4{margin-right:4px}#applet_p_63794 .Mt-6{margin-top:6px}#applet_p_63794 .Mb-6{margin-bottom:6px}#applet_p_63794 .Mstart-6{margin-left:6px}#applet_p_63794 .Mend-6{margin-right:6px}#applet_p_63794 .Mt-8{margin-top:8px}#applet_p_63794 .Mb-8{margin-bottom:8px}#applet_p_63794 .Mstart-8{margin-left:8px}#applet_p_63794 .Mend-8{margin-right:8px}#applet_p_63794 .Mt-10{margin-top:10px}#applet_p_63794 .Mb-10{margin-bottom:10px}#applet_p_63794 .Mstart-10{margin-left:10px}#applet_p_63794 .Mend-10{margin-right:10px}#applet_p_63794 .Mt-12{margin-top:12px}#applet_p_63794 .Mb-12{margin-bottom:12px}#applet_p_63794 .Mstart-12{margin-left:12px}#applet_p_63794 .Mend-12{margin-right:12px}#applet_p_63794 .Mt-14{margin-top:14px}#applet_p_63794 .Mb-14{margin-bottom:14px}#applet_p_63794 .Mstart-14{margin-left:14px}#applet_p_63794 .Mend-14{margin-right:14px}#applet_p_63794 .Mt-16{margin-top:16px}#applet_p_63794 .Mb-16{margin-bottom:16px}#applet_p_63794 .Mstart-16{margin-left:16px}#applet_p_63794 .Mend-16{margin-right:16px}#applet_p_63794 .Mt-18{margin-top:18px}#applet_p_63794 .Mb-18{margin-bottom:18px}#applet_p_63794 .Mstart-18{margin-left:18px}#applet_p_63794 .Mend-18{margin-right:18px}#applet_p_63794 .Mt-20{margin-top:20px}#applet_p_63794 .Mb-20{margin-bottom:20px}#applet_p_63794 .Mstart-20{margin-left:20px}#applet_p_63794 .Mend-20{margin-right:20px}#applet_p_63794 .Mt-30{margin-top:30px}#applet_p_63794 .Mb-30{margin-bottom:30px}#applet_p_63794 .Mstart-30{margin-left:30px}#applet_p_63794 .Mend-30{margin-right:30px}#applet_p_63794 .Mt-40{margin-top:40px}#applet_p_63794 .Mb-40{margin-bottom:40px}#applet_p_63794 .Mstart-40{margin-left:40px}#applet_p_63794 .Mend-40{margin-right:40px}#applet_p_63794 .Mt-50{margin-top:50px}#applet_p_63794 .Mb-50{margin-bottom:50px}#applet_p_63794 .Mstart-50{margin-left:50px}#applet_p_63794 .Mend-50{margin-right:50px}#applet_p_63794 .Mt-60{margin-top:60px}#applet_p_63794 .Mb-60{margin-bottom:60px}#applet_p_63794 .Mstart-60{margin-left:60px}#applet_p_63794 .Mend-60{margin-right:60px}#applet_p_63794 .Mt-70{margin-top:70px}#applet_p_63794 .Mb-70{margin-bottom:70px}#applet_p_63794 .Mstart-70{margin-left:70px}#applet_p_63794 .Mend-70{margin-right:70px}#applet_p_63794 .Mt-neg-1{margin-top:-1px}#applet_p_63794 .Mb-neg-1{margin-bottom:-1px}#applet_p_63794 .Mstart-neg-1{margin-left:-1px}#applet_p_63794 .Mend-neg-1{margin-right:-1px}#applet_p_63794 .Mt-neg-4{margin-top:-4px}#applet_p_63794 .Mb-neg-4{margin-bottom:-4px}#applet_p_63794 .Mstart-neg-4{margin-left:-4px}#applet_p_63794 .Mend-neg-4{margin-right:-4px}#applet_p_63794 .Mt-neg-6{margin-top:-6px}#applet_p_63794 .Mb-neg-6{margin-bottom:-6px}#applet_p_63794 .Mstart-neg-6{margin-left:-6px}#applet_p_63794 .Mend-neg-6{margin-right:-6px}#applet_p_63794 .Mt-neg-8{margin-top:-8px}#applet_p_63794 .Mb-neg-8{margin-bottom:-8px}#applet_p_63794 .Mstart-neg-8{margin-left:-8px}#applet_p_63794 .Mend-neg-8{margin-right:-8px}#applet_p_63794 .Mt-neg-10{margin-top:-10px}#applet_p_63794 .Mb-neg-10{margin-bottom:-10px}#applet_p_63794 .Mstart-neg-10{margin-left:-10px}#applet_p_63794 .Mend-neg-10{margin-right:-10px}#applet_p_63794 .Mt-neg-12{margin-top:-12px}#applet_p_63794 .Mb-neg-12{margin-bottom:-12px}#applet_p_63794 .Mstart-neg-12{margin-left:-12px}#applet_p_63794 .Mend-neg-12{margin-right:-12px}#applet_p_63794 .Mt-neg-14{margin-top:-14px}#applet_p_63794 .Mb-neg-14{margin-bottom:-14px}#applet_p_63794 .Mstart-neg-14{margin-left:-14px}#applet_p_63794 .Mend-neg-14{margin-right:-14px}#applet_p_63794 .Mt-neg-16{margin-top:-16px}#applet_p_63794 .Mb-neg-16{margin-bottom:-16px}#applet_p_63794 .Mstart-neg-16{margin-left:-16px}#applet_p_63794 .Mend-neg-16{margin-right:-16px}#applet_p_63794 .Mt-neg-18{margin-top:-18px}#applet_p_63794 .Mb-neg-18{margin-bottom:-18px}#applet_p_63794 .Mstart-neg-18{margin-left:-18px}#applet_p_63794 .Mend-neg-18{margin-right:-18px}#applet_p_63794 .Mt-neg-20{margin-top:-20px}#applet_p_63794 .Mb-neg-20{margin-bottom:-20px}#applet_p_63794 .Mstart-neg-20{margin-left:-20px}#applet_p_63794 .Mend-neg-20{margin-right:-20px}#applet_p_63794 .Mstart-50\%{margin-left:50%}#applet_p_63794 .M-a{margin:auto}#applet_p_63794 .Mx-a{margin-right:auto;margin-left:auto}#applet_p_63794 .\!Mx-a{margin-right:auto!important;margin-left:auto!important}#applet_p_63794 .Mstart-a{margin-left:auto}#applet_p_63794 .Mend-a{margin-right:auto}#applet_p_63794 .Mt-0,#applet_p_63794 .My-0{margin-top:0}#applet_p_63794 .Mb-0,#applet_p_63794 .My-0{margin-bottom:0}#applet_p_63794 .Mstart-0,#applet_p_63794 .Mx-0{margin-left:0}#applet_p_63794 .Mend-0,#applet_p_63794 .Mx-0{margin-right:0}#applet_p_63794 .Miw-0{min-width:0}#applet_p_63794 .Miw-10{min-width:10%}#applet_p_63794 .Miw-15{min-width:15%}#applet_p_63794 .Miw-20{min-width:20%}#applet_p_63794 .Miw-25{min-width:25%}#applet_p_63794 .Miw-30{min-width:30%}#applet_p_63794 .Miw-35{min-width:35%}#applet_p_63794 .Miw-40{min-width:40%}#applet_p_63794 .Miw-45{min-width:45%}#applet_p_63794 .Miw-50{min-width:50%}#applet_p_63794 .Miw-60{min-width:60%}#applet_p_63794 .Miw-70{min-width:70%}#applet_p_63794 .Miw-80{min-width:80%}#applet_p_63794 .Miw-90{min-width:90%}#applet_p_63794 .Miw-100{min-width:100%}#applet_p_63794 .Maw-n{max-width:none}#applet_p_63794 .Maw-10{max-width:10%}#applet_p_63794 .Maw-15{max-width:15%}#applet_p_63794 .Maw-20{max-width:20%}#applet_p_63794 .Maw-25{max-width:25%}#applet_p_63794 .Maw-30{max-width:30%}#applet_p_63794 .Maw-35{max-width:35%}#applet_p_63794 .Maw-40{max-width:40%}#applet_p_63794 .Maw-45{max-width:45%}#applet_p_63794 .Maw-50{max-width:50%}#applet_p_63794 .Maw-60{max-width:60%}#applet_p_63794 .Maw-70{max-width:70%}#applet_p_63794 .Maw-80{max-width:80%}#applet_p_63794 .Maw-90{max-width:90%}#applet_p_63794 .Maw-99{max-width:99%}#applet_p_63794 .Maw-100{max-width:100%}#applet_p_63794 .Mih-0{min-height:0}#applet_p_63794 .Mih-50{min-height:50%;_height:50%}#applet_p_63794 .Mih-60{min-height:60%;_height:60%}#applet_p_63794 .Mih-100{min-height:100%;_height:100%}#applet_p_63794 .Mah-n{max-height:none}#applet_p_63794 .Mah-0{max-height:0}#applet_p_63794 .Mah-50{max-height:50%}#applet_p_63794 .Mah-60{max-height:60%}#applet_p_63794 .Mah-100{max-height:100%}#applet_p_63794 .O-0{outline:0}#applet_p_63794 .T-0{top:0}#applet_p_63794 .B-0{bottom:0}#applet_p_63794 .Start-0{left:0}#applet_p_63794 .End-0{right:0}#applet_p_63794 .T-10{top:10%}#applet_p_63794 .B-10{bottom:10%}#applet_p_63794 .Start-10{left:10%}#applet_p_63794 .End-10{right:10%}#applet_p_63794 .T-25{top:25%}#applet_p_63794 .B-25{bottom:25%}#applet_p_63794 .Start-25{left:25%}#applet_p_63794 .End-25{right:25%}#applet_p_63794 .T-50{top:50%}#applet_p_63794 .B-50{bottom:50%}#applet_p_63794 .Start-50{left:50%}#applet_p_63794 .End-50{right:50%}#applet_p_63794 .T-75{top:75%}#applet_p_63794 .B-75{bottom:75%}#applet_p_63794 .Start-75{left:75%}#applet_p_63794 .End-75{right:75%}#applet_p_63794 .T-100{top:100%}#applet_p_63794 .B-100{bottom:100%}#applet_p_63794 .Start-100{left:100%}#applet_p_63794 .End-100{right:100%}#applet_p_63794 .T-a{top:auto!important}#applet_p_63794 .B-a{bottom:auto!important}#applet_p_63794 .Start-a{left:auto!important}#applet_p_63794 .End-a{right:auto!important}#applet_p_63794 .Op-0{opacity:0;filter:alpha(opacity=0)}#applet_p_63794 .Op-33{opacity:.33;filter:alpha(opacity=33)}#applet_p_63794 .Op-50{opacity:.5;filter:alpha(opacity=50)}#applet_p_63794 .Op-66{opacity:.66;filter:alpha(opacity=66)}#applet_p_63794 .Op-100{opacity:1;filter:alpha(opacity=100)}#applet_p_63794 .Ov-h{overflow:hidden;zoom:1}#applet_p_63794 .Ov-v{overflow:visible}#applet_p_63794 .Ov-s{overflow:scroll}#applet_p_63794 .Ov-a{overflow:auto}#applet_p_63794 .Ovs-t{-webkit-overflow-scrolling:touch}#applet_p_63794 .Ovx-v{overflow-x:visible}#applet_p_63794 .Ovx-h{overflow-x:hidden}#applet_p_63794 .Ovx-s{overflow-x:scroll}#applet_p_63794 .Ovx-a{overflow-x:auto}#applet_p_63794 .Ovy-v{overflow-y:visible}#applet_p_63794 .Ovy-h{overflow-y:hidden}#applet_p_63794 .Ovy-s{overflow-y:scroll}#applet_p_63794 .Ovy-a{overflow-y:auto}#applet_p_63794 .P-0{padding:0}#applet_p_63794 .P-1{padding:1px}#applet_p_63794 .P-2{padding:2px}#applet_p_63794 .P-4{padding:4px}#applet_p_63794 .P-6{padding:6px}#applet_p_63794 .P-8{padding:8px}#applet_p_63794 .P-10{padding:10px}#applet_p_63794 .P-12{padding:12px}#applet_p_63794 .P-14{padding:14px}#applet_p_63794 .P-16{padding:16px}#applet_p_63794 .P-18{padding:18px}#applet_p_63794 .P-20{padding:20px}#applet_p_63794 .P-30{padding:30px}#applet_p_63794 .Px-1{padding-right:1px;padding-left:1px}#applet_p_63794 .Px-2{padding-right:2px;padding-left:2px}#applet_p_63794 .Px-4{padding-right:4px;padding-left:4px}#applet_p_63794 .Px-6{padding-right:6px;padding-left:6px}#applet_p_63794 .Px-8{padding-right:8px;padding-left:8px}#applet_p_63794 .Px-10{padding-right:10px;padding-left:10px}#applet_p_63794 .Px-12{padding-right:12px;padding-left:12px}#applet_p_63794 .Px-14{padding-right:14px;padding-left:14px}#applet_p_63794 .Px-16{padding-right:16px;padding-left:16px}#applet_p_63794 .Px-18{padding-right:18px;padding-left:18px}#applet_p_63794 .Px-20{padding-right:20px;padding-left:20px}#applet_p_63794 .Px-30{padding-right:30px;padding-left:30px}#applet_p_63794 .Py-1{padding-top:1px;padding-bottom:1px}#applet_p_63794 .Py-2{padding-top:2px;padding-bottom:2px}#applet_p_63794 .Py-4{padding-top:4px;padding-bottom:4px}#applet_p_63794 .Py-6{padding-top:6px;padding-bottom:6px}#applet_p_63794 .Py-8{padding-top:8px;padding-bottom:8px}#applet_p_63794 .Py-10{padding-top:10px;padding-bottom:10px}#applet_p_63794 .Py-12{padding-top:12px;padding-bottom:12px}#applet_p_63794 .Py-14{padding-top:14px;padding-bottom:14px}#applet_p_63794 .Py-16{padding-top:16px;padding-bottom:16px}#applet_p_63794 .Py-18{padding-top:18px;padding-bottom:18px}#applet_p_63794 .Py-20{padding-top:20px;padding-bottom:20px}#applet_p_63794 .Py-30{padding-top:30px;padding-bottom:30px}#applet_p_63794 .Pt-1{padding-top:1px}#applet_p_63794 .Pb-1{padding-bottom:1px}#applet_p_63794 .Pstart-1{padding-left:1px}#applet_p_63794 .Pend-1{padding-right:1px}#applet_p_63794 .Pt-2{padding-top:2px}#applet_p_63794 .Pb-2{padding-bottom:2px}#applet_p_63794 .Pstart-2{padding-left:2px}#applet_p_63794 .Pend-2{padding-right:2px}#applet_p_63794 .Pt-4{padding-top:4px}#applet_p_63794 .Pb-4{padding-bottom:4px}#applet_p_63794 .Pstart-4{padding-left:4px}#applet_p_63794 .Pend-4{padding-right:4px}#applet_p_63794 .Pt-6{padding-top:6px}#applet_p_63794 .Pb-6{padding-bottom:6px}#applet_p_63794 .Pstart-6{padding-left:6px}#applet_p_63794 .Pend-6{padding-right:6px}#applet_p_63794 .Pt-8{padding-top:8px}#applet_p_63794 .Pb-8{padding-bottom:8px}#applet_p_63794 .Pstart-8{padding-left:8px}#applet_p_63794 .Pend-8{padding-right:8px}#applet_p_63794 .Pt-10{padding-top:10px}#applet_p_63794 .Pb-10{padding-bottom:10px}#applet_p_63794 .Pstart-10{padding-left:10px}#applet_p_63794 .Pend-10{padding-right:10px}#applet_p_63794 .Pt-12{padding-top:12px}#applet_p_63794 .Pb-12{padding-bottom:12px}#applet_p_63794 .Pstart-12{padding-left:12px}#applet_p_63794 .Pend-12{padding-right:12px}#applet_p_63794 .Pt-14{padding-top:14px}#applet_p_63794 .Pb-14{padding-bottom:14px}#applet_p_63794 .Pstart-14{padding-left:14px}#applet_p_63794 .Pend-14{padding-right:14px}#applet_p_63794 .Pt-16{padding-top:16px}#applet_p_63794 .Pb-16{padding-bottom:16px}#applet_p_63794 .Pstart-16{padding-left:16px}#applet_p_63794 .Pend-16{padding-right:16px}#applet_p_63794 .Pt-18{padding-top:18px}#applet_p_63794 .Pb-18{padding-bottom:18px}#applet_p_63794 .Pstart-18{padding-left:18px}#applet_p_63794 .Pend-18{padding-right:18px}#applet_p_63794 .Pt-20{padding-top:20px}#applet_p_63794 .Pend-20{padding-right:20px}#applet_p_63794 .Pb-20{padding-bottom:20px}#applet_p_63794 .Pstart-20{padding-left:20px}#applet_p_63794 .Pt-30{padding-top:30px}#applet_p_63794 .Pend-30{padding-right:30px}#applet_p_63794 .Pb-30{padding-bottom:30px}#applet_p_63794 .Pstart-30{padding-left:30px}#applet_p_63794 .Pt-0,#applet_p_63794 .Py-0{padding-top:0}#applet_p_63794 .Pb-0,#applet_p_63794 .Py-0{padding-bottom:0}#applet_p_63794 .Pstart-0,#applet_p_63794 .Px-0{padding-left:0}#applet_p_63794 .Pend-0,#applet_p_63794 .Px-0{padding-right:0}#applet_p_63794 .Pe-n{pointer-events:none}#applet_p_63794 .Pe-a{pointer-events:auto}#applet_p_63794 .Pos-s{position:static}#applet_p_63794 .Pos-a{position:absolute}#applet_p_63794 .Pos-r{position:relative}#applet_p_63794 .Pos-f{position:fixed}#applet_p_63794 .Ws-n::-webkit-scrollbar{-webkit-appearance:none}#applet_p_63794 .Tbl-f{table-layout:fixed}#applet_p_63794 .Tbl-a{table-layout:auto}#applet_p_63794 .Ta-c{text-align:center}#applet_p_63794 .Ta-j{text-align:justify}#applet_p_63794 .Ta-start{text-align:left}#applet_p_63794 .Ta-end{text-align:right}#applet_p_63794 .Td-n{text-decoration:none!important}#applet_p_63794 .Td-u,#applet_p_63794 .Td-u\:h:hover{text-decoration:underline}#applet_p_63794 .Tr-a{-webkit-text-rendering:auto;-moz-text-rendering:auto;-ms-text-rendering:auto;-o-text-rendering:auto;text-rendering:auto}#applet_p_63794 .Tr-os{-webkit-text-rendering:optimizeSpeed;-moz-text-rendering:optimizeSpeed;-ms-text-rendering:optimizeSpeed;-o-text-rendering:optimizeSpeed;text-rendering:optimizeSpeed}#applet_p_63794 .Tr-ol{-webkit-text-rendering:optimizeLegibility;-moz-text-rendering:optimizeLegibility;-ms-text-rendering:optimizeLegibility;-o-text-rendering:optimizeLegibility;text-rendering:optimizeLegibility}#applet_p_63794 .Tr-gp{-webkit-text-rendering:geometricPrecision;-moz-text-rendering:geometricPrecision;-ms-text-rendering:geometricPrecision;-o-text-rendering:geometricPrecision;text-rendering:geometricPrecision}#applet_p_63794 .Tr-i{-webkit-text-rendering:inherit;-moz-text-rendering:inherit;-ms-text-rendering:inherit;-o-text-rendering:inherit;text-rendering:inherit}#applet_p_63794 .Tt-n{text-transform:none}#applet_p_63794 .Tt-u{text-transform:uppercase}#applet_p_63794 .Tt-c{text-transform:capitalize}#applet_p_63794 .Tt-l{text-transform:lowercase}#applet_p_63794 .Tsh{text-shadow:0 1px 1px #000}#applet_p_63794 .Tsh-n{text-shadow:none}#applet_p_63794 .Us-n{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}#applet_p_63794 .Va-sup{vertical-align:super}#applet_p_63794 .Va-t{vertical-align:top}#applet_p_63794 .Va-tt{vertical-align:text-top}#applet_p_63794 .Va-m{vertical-align:middle}#applet_p_63794 .Va-bl{vertical-align:baseline}#applet_p_63794 .Va-b{vertical-align:bottom}#applet_p_63794 .Va-tb{vertical-align:text-bottom}#applet_p_63794 .Va-sub{vertical-align:sub}#applet_p_63794 .V-v{visibility:visible}#applet_p_63794 .V-h{visibility:hidden}#applet_p_63794 .V-c{visibility:collapse}#applet_p_63794 .Whs-nw{white-space:nowrap}#applet_p_63794 .Whs-n{white-space:normal}#applet_p_63794 .W-a{width:auto}#applet_p_63794 .W-0{width:0}#applet_p_63794 .W-1{width:1%}#applet_p_63794 .W-10{width:10%;*width:9.6%}#applet_p_63794 .W-15{width:15%;*width:14.8%}#applet_p_63794 .W-20{width:20%;*width:19.5%}#applet_p_63794 .W-25{width:25%;*width:24.5%}#applet_p_63794 .W-30{width:30%;*width:29.6%}#applet_p_63794 .W-33{width:33.33%;*width:33%}#applet_p_63794 .W-35{width:35%;*width:34.9%}#applet_p_63794 .W-40{width:40%;*width:39.5%}#applet_p_63794 .W-45{width:45%;*width:44.9%}#applet_p_63794 .W-50{width:50%;*width:49.5%}#applet_p_63794 .W-55{width:55%;*width:54.8%}#applet_p_63794 .W-60{width:60%}#applet_p_63794 .W-66{width:66.66%}#applet_p_63794 .W-70{width:70%}#applet_p_63794 .W-75{width:75%}#applet_p_63794 .W-80{width:80%}#applet_p_63794 .W-90{width:90%}#applet_p_63794 .W-100{width:100%}#applet_p_63794 .Wpx-1{width:1px}#applet_p_63794 .Wpx-2{width:2px}#applet_p_63794 .Wpx-4{width:4px}#applet_p_63794 .Wpx-6{width:6px}#applet_p_63794 .Wpx-8{width:8px}#applet_p_63794 .Wpx-10{width:10px}#applet_p_63794 .Wpx-12{width:12px}#applet_p_63794 .Wpx-14{width:14px}#applet_p_63794 .Wpx-16{width:16px}#applet_p_63794 .Wpx-18{width:18px}#applet_p_63794 .Wpx-20{width:20px}#applet_p_63794 .Wpx-24{width:24px}#applet_p_63794 .Wpx-26{width:26px}#applet_p_63794 .Wpx-28{width:28px}#applet_p_63794 .Wpx-30{width:30px}#applet_p_63794 .Wpx-32{width:32px}#applet_p_63794 .Wob-ba{word-break:break-all}#applet_p_63794 .Wob-n{word-break:normal!important}#applet_p_63794 .Wow-bw{word-wrap:break-word}#applet_p_63794 .Wow-n{word-wrap:normal!important}#applet_p_63794 .Z-0{z-index:0}#applet_p_63794 .Z-1{z-index:1}#applet_p_63794 .Z-3{z-index:3}#applet_p_63794 .Z-5{z-index:5}#applet_p_63794 .Z-7{z-index:7}#applet_p_63794 .Z-10{z-index:10}#applet_p_63794 .Z-a{z-index:auto!important}#applet_p_63794 .Zoom-1{zoom:1}

#applet_p_63794 .StencilRoot input {
    font-size: 13px;    
}

#applet_p_63794 .StencilRoot button {
    font-size: 14px;
    line-height: normal;
}
</style>

    
    
    </head>
    <body class="my3columns  l-out Pos-r https fp fp-v2 rc1 fp-default mini-uh-on viewer-right ltr    stream-dense" dir="ltr">
        
        
        
                    <div id="darla-assets-top">
            <script type='text/javascript' src='/sy/rq/darla/2-9-9/js/g-r-min.js'></script>
            <script>
    var resourceTimingAssets = null;
    if (window.performance && window.performance.mark && window.performance.getEntriesByName) {
        resourceTimingAssets = {'darlaJsLoaded' : 'https://www.yahoo.com/sy/rq/darla/2-9-9/js/g-r-min.js'};
        window.performance.mark('darlaJsLoaded');
    }
</script>
            </div>
                <script type="text/javascript">
        var rapidPageConfig = {
            rapidEarlyConfig : {},
            rapidConfig: {"compr_type":"deflate","tracked_mods":[],"spaceid":2023538075,"click_timeout":300,"track_right_click":true,"apv":true,"apv_time":0,"yql_host":"","test_id":"201","client_only":0,"pageview_on_init":true,"perf_navigationtime":2,"addmodules_timeout":500,"keys":{"_rid":"fmuulqdbfinfk","mrkt":"us","pt":1,"ver":"megastrm","uh_vw":0,"colo":"slw72.fp.bf1.yahoo.com","navtype":"server"},"viewability":true},
            rapidSingleInstance: 0,
            ywaCF: "",
            ywaActionMap: "",
            ywaOutcomeMap: "" 
        };

        if (rapidPageConfig.rapidEarlyConfig.keys) {
            rapidPageConfig.rapidEarlyConfig.keys.bw = document.body.offsetWidth;
            rapidPageConfig.rapidEarlyConfig.keys.bh = document.body.offsetHeight;
        }
        rapidPageConfig.rapidConfig.keys.bw = document.body.offsetWidth;
        rapidPageConfig.rapidConfig.keys.bh = document.body.offsetHeight;
        </script>
        
                            
                    
                    
                    
                    
                    
                    

    <div id="UH">
        <div id="UH-ColWrap">
            <div id="applet_p_30345894" class=" M-0 js-applet header Zoom-1  Mb-0 " data-applet-guid="p_30345894" data-applet-type="header" data-applet-params="_suid:30345894" data-i13n="auto:true;sec:hd" data-i13n-sec="hd" data-ylk="rspns:nav;t1:a1;t2:hd;itc:0;"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-uh-wrapper" class="Bds(n) Bdc($headerBdr) Scrolling_Bdc($headerBdrScroll) has-scrolled_Bdc($headerBdrScroll) Bdbw(1px) ua-ie7_Bdbs(s)! ua-ie8_Bdbs(s)! Scrolling_Bxsh($headerShadow) has-scrolled_Bxsh($headerShadow) Bgc(#fff) T(0) Start(0) End(0) Pos(f) Z(3) Zoom">
    
    <div id="mega-topbar" class="Pos(r) H(22px) mini-header_Mt(-19px) Reader-open_Mt(-19px) Bg($topbarBgc) Bxsh($topbarShadow) Z(7)"   data-ylk="rspns:nav;t1:a1;t2:hd;t3:tb;sec:hd;itc:0;elm:itm;elmt:pty;">
    <ul class="Pos(r) Miw(1000px) Pstart(9px) Lh(1.7) Reader-open_Op(0) mini-header_Op(0)" role="navigation">
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:home;t5:home;cpos:1;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) Icon-Fp2 IconHome"></i>Home</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mail.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mail;t5:mail;cpos:2;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mail</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.flickr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:flickr;t5:flickr;cpos:3;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Flickr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://www.tumblr.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:tumblr;t5:tumblr;cpos:4;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Tumblr</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://answers.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:answers;t5:answers;cpos:5;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Answers</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://groups.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:groups;t5:groups;cpos:6;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Groups</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(22px) Mend(8px)">
            <a href="https://mobile.yahoo.com/" class="C(#fff) Fz(13px)"   data-ylk="slk:mobile;t5:mobile;cpos:7;" tabindex="1"><i class="Pos(a) Fz(15px) Start(10px) T(-2px) "></i>Mobile</a>
        </li>
    
    
    
        <li class="D(ib) Mstart(10px) navigation-menu">
            <a href="https://everything.yahoo.com/" class="navigation-menu-title Pos(r) C(#fff) Pstart(12px) Pend(24px) Py(4px) Fz(13px) menu-open_C($topbarMenu) menu-open_Bgc(#fff) menu-open_Z(8) rapidnofollow"   data-ylk="rspns:op;t5:more;slk:more;itc:1;elmt:mu;cpos:8;" tabindex="1">More<i class="Pos(a) Pt(2px) Pstart(6px) Fw(b) Icon-Fp2 IconDownCaret"></i></a>
            <div class="Pos(a) Bgc(#fff) Bxsh($topbarMenuShadow) Z(7) V(h) Op(0) menu-open_V(v) menu-open_Op(1)">
                <ul class="Px(12px) Py(5px)">
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/politics/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:politics;t5:politics;cpos:9;" tabindex="1">Politics</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/celebrity/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:celebrity;t5:celebrity;cpos:10;" tabindex="1">Celebrity</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/movies/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:movies;t5:movies;cpos:11;" tabindex="1">Movies</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/music/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:music;t5:music;cpos:12;" tabindex="1">Music</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tv/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tv;t5:tv;cpos:13;" tabindex="1">TV</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/style/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:style;t5:style;cpos:14;" tabindex="1">Style</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/beauty/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:beauty;t5:beauty;cpos:15;" tabindex="1">Beauty</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/tech/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:tech;t5:tech;cpos:16;" tabindex="1">Tech</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://shopping.yahoo.com/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:shopping;t5:shopping;cpos:17;" tabindex="1">Shopping</a>
                    </li>
                
                    <li class="D(b)">
                        <a href="https://www.yahoo.com/autos/" class="D(b) Fz(13px) C($topbarMenu) Py(3px) Td(n) Td(u):h"   data-ylk="slk:autos;t5:autos;cpos:18;" tabindex="1">Autos</a>
                    </li>
                
                </ul>
            </div>
        </li>
    
    
    
    
        <li id="mega-top-ff-promo" class="Pos(a) End(0) D(ib) Mend(18px) Pstart(14px) D(n)!">
            <a class="C(#fff) Pstart(4px) Td(n)! Td(u)!:h Fz(13px)" href="https://www.mozilla.org/firefox/new/?utm_source=yahoo&amp;utm_medium=referral&amp;utm_campaign=y-uh&amp;utm_content=y-install-new-firefox" target="_blank" tabindex="1"   data-ylk="t5:ff-promo;slk:ff-promo;t4:pty-mu;">
                <img id="yucs-ff-img" class="Pend(4px) Va(m) ua-ie7_D(n)" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAMAAAAM7l6QAAADAFBMVEVMaXEAAAAAAAAAAAAAAAAAAADCOicAAAAAAAAAAACjICQAAADNXCvISyxtx+5wyvGbICONHiAAAACxNCLlfS3keCjhcSlhxO5swuqwLiRkxesAAAAAAABWst/fgi3pvhX73he6PiqzKCWSHSDBRinhbyfJWTLefC9QDxHneynZaC/dbCuuMCnshirukDVkxe1gx+5gxO1XptVYrNpPvejXWiVaERM3CwyLIR3YXSaN1/e5Xi5rGRUuDAmV1OvrkSKLq7LuoBLimiP3zE3e2Z361EL0ziTUoCX0uR7eXCXhYiXORSbdWCbfYCXbVCbXTyXiaibjbiYMXp/ZUSbJPifkdCbkcCXcVybGOybLQSYFU5QVbKzBNSffXybgZibQSSbjZyXneCXVTCYDotjmeyYGTYzogiTokCIKl9DsoR/ohyMFSYfnbCW6LCa9MiYWKlwRL2NEuekeeLgWhcHvrxz/4gKwISfwhyMYHUvcXycKq99hwu5PuurqmCETZ6gLOXE2tuc+tud4zfL+9sP/8JUYfrz4zwwfc7PzvB34+OP+50b95V//74PXWSX//fIVdLQRjMfTSibmgCZrPjwMNWv+6HMLR4Aos+Rpxe/OYyYHQXsPkMr1xh0qptr/+dZIvevfaCb2mCOqYTYisuRTvusXJVYdIVABqN3ndCWOU0K0WjMHndQYr+LrhSatIiXfcCW4KSYtWHLqonDPUSbVkDBkw+6INjgsKVB5ZFDXdiYmksQknr+5n0TitSYLWJgfqt711XROqtk3pNdJns7QlTD676z/7GH/7WujuLjufSNDMUZHTWt/clbjjSRgSDpMMD5Jiavos4myc1qaeGPQfTaNfze9Wy0/Z28hLV7ZrybheUcpR20qs+X71gcuPVr62UerQy/BcSx0n3k4mc6+vFXooB9VsN/RaiSOoWvCnUL4zTuckUaOy9HF5N7tlCB4gFcals53in9Vn8bS6+2MxeJ+sMTz+PuTuMl2lpuGs9SyxK8qcJzk7fTy0UFagZb83juJ9+V4AAAASXRSTlMADhgiCC/+AVME/Dv+/vb8/vJHNG3duPLt+rwTYn8t9/6he3eA8vPmZOZCue7pnuNpzWIgPrSjjd/nEPPTl/6H8mTjtP7C0/yLJ0Hq1wAAAxVJREFUKM9jYIADEyl9cT5+cT5DAwZMICvRxD9jazUQzGjil7JClTSWaNpa01hYU7NkSU1h45u3drIIOV09I74ZBw/N3T+9oXD+/MLGhrkHOz87OEJlRconNT17vGvXi71P7jQAJafvn3voU3Pnd2eItGbYgisv9+w5krl395rF0xevqcr4cPjL15ZOF1ewtGpvb+CKlPTap4+Sl1dVVS1PzphdfPjb0ZZ5XXIgaZ64nLDQlKzanat2r04GgtVA6daffVOPHnMDSaflJASCpGduDtyyMiMjY/aE4tai9j/NU48rAGXVTyaApNPSn/Pe2nR5AlCuv7+oPfpEc09PKVB6bY5XXEBY4qT09MpNFy8AJaOj6+rqfv9rmdpc6gH0llesVwDY9MpL7UVFQI1BQfuifp1o6e5TkGcQTfT09I0LAJmeWRcN1AmSTM3+0Tdvat//IwyigT6esXEBAWxlK88EgUBUamr2tFkfO5t7mrvdGcR8g32A2vPKIk6nRoFAdvasWfX173uOHW/528EgVukPlPcqi/RcNC07O3Xj9dRp9VOmFLzu6u7qmlPCoF2ZlOTnE8sWHhe6c8eOLZklNzdOKci/d+BAd2lpiRODFo93fLCfZ7xnHDDoJmWlrX94u75g6dX7r0rb2t7ZMzCoRUZODA73A7o/IDDAN37bjYKC/Ar5trttHSWTgMGi4h3iDTTe07d3M++Dbdvz8/OXVpybM6ejJDPLGhTmvDFQ+bhrk7cvq6hYdmrR2Y7MzPSsmZYgacUIsHy4Z6xXHM/k85NXhSYuXDhpUuh6HlNwhLPlgeSDQQYAgx8Y/uXliYGBC3z9IclFOC8XKD/R3y8cqMDXKzBsRVhcnJdnvBxEml0pNzcmIhJkAFBBrJeXr6+vp3+IMCdEmpNRuWxDHlA+PskfqAIEvNl0WJlY2KHSzBpcZUADQrzj4yf6+yclsUVIcsCl2VmYWDnMuNZtAKmIjIyMyJO0kOFmZoQaDpSXZubmMBcUEODiEhAUtLGVEWJlgsuCFTAyMbNyC3EAgRA3K7M0CyfYZABvvj+RWMmoDQAAAABJRU5ErkJggg==" width="15" height="15" alt="Firefox">Install the new Firefox<span>&nbsp;Â»</span>
            </a>
        </li>
        <script>!function(){var a,b,c,d="; "+document.cookie,e=d.split("; DSS="),f=new RegExp("sdts=(\\d+)");e&&2===e.length&&(a=f.exec(e[1]),a&&2===a.length&&(b=a[1])),(!b||b&&parseInt((new Date).getTime())/1e3-b>604800)&&(c=document.getElementById("mega-top-ff-promo"),c.className=c.className.replace(/D\(n\)/g,""))}();</script>
    
    </ul>
</div>

    
    <div id="mega-uh" class="M(a) Maw(1256px) Miw(1000px) Pos(r) Pt(22px) Pb(24px) Reader-open_Pt(13px) mini-header_Pt(13px) Reader-open_Pb(14px) mini-header_Pb(14px) Z(6)">
        <h1 class="Fz(0) Pos(a) Pstart(15px) Reader-open_Pt(0px) mini-header_Pt(0px) search-mini-header_Pstart(10px) ua-ie7_Mt(4px)">
    <a id="uh-logo" href="https://www.yahoo.com/" class="D(ib) H(47px) W($bigLogoWidth) Bgi($logoImage) Bgr(nr) Bgz($bigLogoWidth) Reader-open_Bgz($mediumLogoWidth) mini-header_Bgz($mediumLogoWidth) search-mini-header_Bgz($searchLogoWidth) Bgz($smallLogoWidth)!--sm1024 Bgp($bigLogoPos) Reader-open_Bgp($mediumLogoPos) mini-header_Bgp($mediumLogoPos) search-mini-header_Bgp($searchLogoPos) Bgp($searchLogoPos)!--sm1024 ua-ie7_Bgi($logoImageIe) ua-ie7_Mstart(-170px) ua-ie8_Bgi($logoImageIe) "   data-ylk="rspns:nav;t1:a1;t2:hd;sec:hd;itc:0;slk:logo;elm:img;elmt:logo;" tabindex="1">
        <b class="Hidden">Yahoo</b>
    </a>
</h1>

        <ul class="Pos(a) End(15px) List(n) Mt(3px) Reader-open_Mt(1px) mini-header_Mt(1px)" role="menubar">
            
    <li class="Pos(r) Fl(start) Mend(26px)">
        <a id="uh-signin" class="Bdc($signInBtn) Bdrs(5px) Bds(s) Bdw(2px) Bgc($signInBtn):h C($signInBtn) C(#fff):h D(ib) Ell Fz(14px) Fw(b) Py(2px) Mt(5px) Ta(c) Td(n):h Miw(78px) H(18px)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;" tabindex="4"><div class="Miw(78px) Ta(c) Pos(a) T(0) Lh($userNavTextLh)">Sign in</div></a>
    </li>


            
    <li id="uh-notifications" class="uh-menu uh-notifications Fl(start) D(ib) Mend(13px) Cur(p) ua-ie7_D(n) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <button class="uh-menu-btn Pos(r) Bd(0) Px(8px) Pt(1px) Lh(1.3)" aria-label="Notifications" aria-haspopup="true" aria-expanded="true" tabindex="5" title="Notifications">
            <i class="Lh($userNotifIconLh) C($mailBtn) Fz(28px) Grid-U Icon-Fp2 IconBell uh-notifications-icon"></i>
            <span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) End(-4px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-notification-count W(22px)"></span>
        </button>
        <div class="uh-notification-panel uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Ovy(a) Trs($menuTransition) Cur(d) V(h) Op(0) Mah(0) uh-notifications:h_V(v) uh-notifications:h_Op(1) uh-notifications:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" aria-label="Notifications" role="menu" tabindex="5">
            <div class="uh-notifications-loading Py(16px) Px(24px) Ta(c)">
                <div class="W(30px) H(30px) Mx(a) My(12px) Bgz(30px) Bgi($animatedSpinner) ua-ie8_D(n)"></div>
                <span class="C($mailTstamp) D(n) ua-ie8_D(b)">Loading Updates</span>
            </div>
        </div>
    </li>


            
    <li id="uh-mail" class="uh-menu uh-mail D(ib) Mstart(14px) ua-ie8_Pb(10px) ua-ie9_Pb(10px)" role="menuitem">
        <a id="uh-mail-link" href="https://mail.yahoo.com/" class="Pos(r) D(ib) Ta(s) Td(n):h uh-menu-btn" aria-label="Mail" aria-haspopup="true" role="button" aria-expanded="true" tabindex="6"><i class="Lh($userNavIconLh) W(28px) Mend(8px) C($mailBtn) Fz(30px) Grid-U Icon-Fp2 IconMail uh-mail-icon"></i><span class="Lh($userNavTextLh) D(ib) C($mailBtn) Fz(14px) Fw(b) Va(t)">Mail</span><span class="Bgc($mailBadge) Bdrs(11px) C(#fff) D(n) display-count_D(b) Start(16px) Fz(11px) Fw(b) Pos(a) Py(4px) Ta(c) T(-7px) uh-mail-count W(22px)"></span>
        </a>
        
            <div class="uh-mail-preview uh-menu-panel Pos(a) End(0) T(45px) W(280px) Bgc($menuBgc) Bd Bdc($menuBdr) Bdrs(4px) Bxsh($menuShadow) Trs($menuTransition) V(h) Op(0) Mah(0) uh-mail:h_V(v) uh-mail:h_Op(1) uh-mail:h_Mah($panelMah) panel-open_V(v) panel-open_Op(1) panel-open_Mah($panelMah)" role="menu" aria-label="Mail" tabindex="6">
                
                    <div class="Px(24px) Py(20px) Ta(c)">
                        <a class="C($menuLink) Fw(b) Td(n)" href="https://login.yahoo.com/config/login?.src=fpctx&amp;.intl=us&amp;.lang=en-US&amp;.done=https%3A%2F%2Fwww.yahoo.com" data-action-outcome="lgn"  data-ylk="t3:usr;elm:btn;elmt:lgn;">Sign in</a>  to view your mail
                    </div>
                
            </div>
        
    </li>


        </ul>
        <div id="uh-search" class="Pos(r) Mend(375px) Mstart(206px) search-mini-header_Mstart(112px) Mstart(120px)--sm1024 Maw(595px) H(40px) Reader-open_H(40px) mini-header_H(40px) search-mini-header_Maw(685px) Maw(745px)--sm1024 Va(t)">
    <form name="input" class="glowing glow" action="https://search.yahoo.com/search;_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-" method="get" data-assist-action="https://search.yahoo.com/search;_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-">
        <table class="Bdcl(s)" role="presentation">
            <tbody>
                <tr>
                    <td class="Pos(r) P(0) W(100%) H(40px) Reader-open_H(40px) mini-header_H(40px) D(tb) ua-safari_D(tbc) Tbl(f)">
                        <input id="uh-search-box" type="text" name="p" class="Pos(a) T(0) Start(0) Bd Bdc($searchBdrFoc):f Bdc($searchBdr) glow_Bdc($searchBtnGlow) Bgc(t) Bxsh(n) Fz(18px) M(0) O(0) Px(10px) W(100%) H(inh) Z(2) Bxz(bb) Bdrs(2px) ua-ie7_H(36px) ua-ie7_Lh(36px) ua-ie8_Lh(36px) ua-ie7_W(92%)" autofocus autocomplete="off" autocapitalize="off" aria-label="Search" tabindex="2">
                        
                        
                            <input type="hidden" data-fr="yfp-t-201" name="fr" value="yfp-t-201" />
                        
                            <input type="hidden" data-fp="1" name="fp" value="1" />
                        
                            <input type="hidden" data-toggle="1" name="toggle" value="1" />
                        
                            <input type="hidden" data-cop="mss" name="cop" value="mss" />
                        
                            <input type="hidden" data-ei="UTF-8" name="ei" value="UTF-8" />
                        
                    </td>
                    <td class="Miw(5px)"></td>
                    <td>
                        <button id="uh-search-button" type="submit" class="rapid-noclick-resp C(#fff) Fz(16px) H(40px) W(140px) Reader-open_H(40px) mini-header_H(40px) Px(26px) Py(0) Whs(nw) Bdrs(3px) Bdw(0) Bgc($searchBtn) glow_Bgc($searchBtnGlow) Bxsh($searchBtnShadow) glow_Bxsh($searchBtnShadowGlow) Lh(1)"   data-ylk="rspns:nav;t1:a1;t2:srch;sec:srch;slk:srchweb;elm:btn;elmt:srch;tar:search.yahoo.com;tar_uri:/search;itc:0;" tabindex="3">
                        <span class="search-default-label Fw(500) ">Search Web</span>
                        <span class="search-short-label Fw(500) D(n) ">Search</span>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    
    <ul id="Skip-links" class="Pos(a)">
        <li><a href="#Navigation" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f" tabindex="3">Skip to Navigation</a></li>
        <li><a href="#Main" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f" tabindex="3">Skip to Main content</a></li>
        <li><a href="#Aside" class="W(0) O(h) D(ib) Whs(nw) Pos(a) Bg(#0078ff) C(#fff) Op(0) W(a):f Op(1):f P(5px):f" tabindex="3">Skip to Related content</a></li>
    </ul>
    
</div>

    <script>
!function(){var e,t,a,n,c=window,o=document,r=o.getElementById("uh-search-box"),g=function(c){try{a=c.keyCode,n=c.target?c.target:c.srcElement,e=r.value.length,n===r&&0===e&&a>=32&&40>=a?r.blur():"EMBED"===n.tagName||"OBJECT"===n.tagName||"INPUT"===n.tagName||"TEXTAREA"===n.tagName||c.altKey||c.metaKey||c.ctrlKey||!(32>a||a>40)||9==a||13==a||16==a||27==a||(e&&(r.setSelectionRange?r.setSelectionRange(e,e):r.createTextRange&&(t=r.createTextRange(),t.moveStart("character",e),t.select())),r.focus())}catch(c){}};r&&(c.addEventListener?c.addEventListener("keydown",g,!1):c.attachEvent&&o.attachEvent("onkeydown",g),setTimeout(function(){r.focus()},500))}();
</script>



    </div>
</div>

 </div> </div> </div>            <!-- App close -->
            </div>
        </div>
    </div><!-- Header -->
    <div id="Stencil">
    <div id="Reader" class="js-viewer-viewerwrapper">
        <div class="ReaderWrap">
            <div class="InnerWrap">
                <div id="applet_p_50000101" class=" App_v2  M-0 js-applet viewer Zoom-1  App-Chromeless " data-applet-guid="p_50000101" data-applet-type="viewer" data-applet-params="_suid:50000101" data-i13n="auto:true;sec:app-view" data-i13n-sec="app-view" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div id="hl-viewer" class="js-slider Hl-Viewer Ov-h desktop" role="document" aria-labelledby="modal-header" aria-live="polite" aria-hidden="true" tabindex="-1">
    
    
        <button id="closebtn" class="Viewer-Close-Btn Bdr-0 D-b Fw(b) js-close-content-viewer rapid-noclick-resp Z-7">
            <i class="Icon-Fp2 IconActionCross"></i>
            <b class="Td(n) Hidden">Close this content, you can also use the Escape key at anytime</b>
        </button>
        <div class="viewer-wrapper Ov-h mega-modal">
            <div class="">
                
                    <div class="content-container"></div>
                
                
                
                
                <div class="footer-ads">
                    <div id="hl-ad-FSRVY-0" class="D-ib">
                        <div id="hl-ad-FSRVY-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT9-0" class="D-ib">
                        <div id="hl-ad-FOOT9-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                    <div id="hl-ad-FOOT-0" class="D-ib">
                        <div id="hl-ad-FOOT-0-0-iframe" class="hl-display-ads"></div>
                    </div>
                </div>
                <span id="viewer-end" tabindex="0"></span>
            </div>
        </div>
    

    


</div>

 </div> </div> </div>            <!-- App close -->
            </div>
            </div>
        </div>
        <div id="Overlay"></div>
        <div class="Reader-bg"></div>
    </div>
</div>
    
    <div id="Masterwrap" class="slider js-viewer-pagewrapper">
        <div class="page">
            <div id="Billboard-ad">
                                                    <div id="my-adsMAST" class="D-n">
                    <div id="my-adsMAST-iframe">
                        
                        <!-- MAST has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136pu4bne(gid%24d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st%241459183093215213,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%241253csoi0,aid%24qLcH4mKLDz0-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=MAST)"></noscript>
                    </div>
                </div>
                                <div id="ad-north-base" class=""><div id="ad-north" class="BillboardAd"></div></div>
            </div>
            
            <div class="Col1" role="navigation" id="Navigation" tabindex="-1">
                <div class="Col1-stack" data-plugin="sticker" data-sticker-top="75px">
                                <div id="applet_p_50000195" class=" M-0 js-applet navrailv2 Zoom-1  Mb-20 " data-applet-guid="p_50000195" data-applet-type="navrailv2" data-applet-params="_suid:50000195" data-i13n="auto:true;sec:nav" data-i13n-sec="nav" data-ylk="rspns:nav;itc:0;t1:a2;t2:nav;elm:itm;"> <!-- App open -->
        <!-- src=mdbm type=popular mod=td-applet-navlinks-atomic ins=p_50000195 ts=1459182101.5424 --><div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

    

    

    <ul class="navlinks-list D(tbc) W(134px) Mstart(6px)">
    
        <li class="navlink  Py(5px)">
            <a href=https://mail.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMail C($MailNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Mail</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://news.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconNews C($NewsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">News</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://sports.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconSports C($SportsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Sports</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://finance.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconFinance C($FinanceNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Finance</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/autos/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconAutos C($AutosNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Autos</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/celebrity/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconCelebrity C($CelebrityNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Celebrity</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://shopping.yahoo.com class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconShopping C($ShoppingNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Shopping</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/movies/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMovies C($MoviesNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Movies</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/politics/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconPolitics C($PoliticsNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Politics</span>
            </a>
        </li>
    
        <li class="navlink  Py(5px)">
            <a href=https://www.yahoo.com/beauty/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconBeauty C($BeautyNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Beauty</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Style</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">Tech</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">TV</span>
            </a>
        </li>
    
        <li class="navlink D(n)--maxh700 Py(5px)">
            <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b)">
                <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) Miw(30px) H(28px) Ta(c)"></i>
                <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h V(h)--sm1024 Col1-stack:h_V(v) Whs(nw)">More on Yahoo</span>
            </a>
        </li>
    
        <li class="D(b)--maxh700 Py(5px) js-navlinks-seemore Pos(r) D(n) O(0)"  role="button" tabindex="0" aria-haspopup="true" aria-label="More Navlinks">
            <i class="Icon-Fp2 IconStreamShare Va(m) Pend(6px) Fz(22px) D(tbc) Miw(29px) Ta(c) C($MoreNav)"></i>
            <span class="Va(m) D(tbc) Fz(14px) Fw(b) V(h)--sm1024 C($navlink) Col1-stack:h_V(v) C($navlinkHover):h Cur(p)">More</span>

            <div class="js-navlinks-seemore-menu D(n) js-navlinks-menu-open_D(ib) Va(b) Pos(a) B(-14px) Start(60px) Pstart(60px)">
                <ul class="Bxsh($menuShadow) Bd($submenuBdr) Bdrs(3px) Bgc(#fff) Cur(d) Py(13px) Px(20px) Miw(140px)">
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/style/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconStyle C($StyleNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Style</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tech/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTech C($TechNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">Tech</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://www.yahoo.com/tv/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconTV C($TVNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">TV</span>
                        </a>
                    </li>
                
                    <li class="navlink Py(5px) D(n) D(b)--maxh700">
                        <a href=https://everything.yahoo.com/ class="C($navlink) Td(n):h D(b) ">
                            <i class="Icon-Fp2 IconMoreOnYahoo C($MoreOnYahooNav) Va(m) Pend(6px) Fz(23px) D(tbc) W(24px) H(28px) Ta(c)"></i>
                            <span class="Va(m) D(tbc) Fz(14px) Fw(b) C($navlinkHover):h Whs(nw)">More on Yahoo</span>
                        </a>
                    </li>
                
                </ul>
            </div>
        </li>
    </ul>


 </div> </div> </div> <!-- yrid: FVr5VgAAAAAcVwAAx68b6g.. | edgepipe: 0 | authed: 0 | ynet: 1 | ssl: 1 | spdy: 0 | ytee: 0 | mode: fallback | bucket: 201 | colo: bf1 | device: desktop | bot: 0 | environment: prod | lang: en-US | partner: default | site: fp | region: US | intl: us | tz: America/Los_Angeles -->
<!-- tdserver-my.fp.production.manhattan.bf1.yahoo.com (pprd3-node5075-lh1.manhattan.bf1.yahoo.com) | Served by ynodejs | Mon Mar 28 2016 16:21:41 GMT+0000 (UTC) -->            <!-- App close -->
            </div>                           <div id="my-adsTXTL" class="Mt-10 Mb-20 Pt-20 Pos-r ad-txtl D-n">
                    <div class="Mx-a" id="my-adsTXTL-iframe">
                        <noscript>
<!-- APT Vendor: Right Media, Format: Standard Graphical -->
<style>
	#fc_align{
		position: static !important;
		width: 120px !important;
		margin: auto !important;
	}
</style>
<!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136pu4bne(gid%24d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st%241459183093215213,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413aovmmfr,aid%24xBsI4mKLDz0-,bi%242275188051,agp%243476502551,cr%244451009051,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=TXTL)"></noscript>
                    </div>
                </div>
                </div>
            </div>
            <div class="Col2">
                <div id="Main" class="Col2-stack" role="content" tabindex="-1">
                    <div id="Banner">
                        
                    </div><!-- Banner -->
                                <div id="applet_p_50000173" class=" M-0 js-applet streamv2 Zoom-1  Mb-20 " data-applet-guid="p_50000173" data-applet-type="streamv2" data-applet-params="_suid:50000173" data-i13n="auto:true;sec:strm;useViewability:true" data-i13n-sec="strm" data-ylk="t1:a3;t2:strm;t3:ct;cat:default;rspns:nav;itc:0;" data-applet-init="now"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  

<div data-uuid-list="p_50000173">
    
      
<div class="Px(15px) Py(11px) Mb(22px) C(#fff) breakingnews gradient-1  js-stream-followable Bdrs(3px) Pos(r)" data-followid="60f73942-c8f9-11e5-bc86-fa163e798f6a" data-followname="Presidential primaries">
    <div>
        <span class="D(tbc) Fz(15px) Fw(b) Lh(20px) Pend(8px) Whs(nw)">Coming up:</span>
        <div class="D(tbc) Lh(20px)">
            
                <a class="js-stream-follow-name Fz(13px) C(#fff)  js-content-viewer rapid-noclick-resp rapidnofollow" href="https://www.yahoo.com/katiecouric/donald-trumps-latest-jabs-at-ted-cruz-145642696.html" data-uuid="cbc30066-4a69-3a93-b95c-e1c494d29db0">Washington Post editor discusses the presidential candidatesâ feuds</a>
            
            
                <span class="btnWrap">
                    <button title="unfollow" class="Fw(b) followed_D(i) D(n) Va(b) P(0) Bd(0) unfollowBtn C(#fff) O(n) ua-ie7_W(100%)"   data-ylk="rapid-base:undefined;subsec:breakingnews;r:false;ccode:false;t4:false;elm:btn;elmt:unfollow;itc:1;aid:60f73942-c8f9-11e5-bc86-fa163e798f6a;" data-action-outcome="dclent">
                    <i class="unfollowIcon unfollowBtn:h_D(b) D(n) Fl(start) Fz(12px) Fw(b) W(19px) H(19px) Lh(20px) Ta(c) Icon-Fp2 IconStarOutline C(#fff)"></i><i class="followingIcon unfollowBtn:h_D(n) Icon-Fp2 Fl(start) IconStarFilled Fz(12px) Fw(b) W(19px) H(19px) Lh(20px) Ta(c) C(#fff)"></i>
                    <div class="unfollowTxt Mstart(20px) Fz(13px) Tt(c) C(#fff) H(20px) Lh(20px) W(56px)">
                        <div class="Fl(start) Lh(20px)">Unfollow</div>
                        </div>
                    </button>
                    <button title="follow" class="Va(b) P(0) Bd(0) Fw(b) followed_D(n) D(i) followBtn C(#fff) O(n) ua-ie7_W(100%)"   data-ylk="rapid-base:undefined;subsec:breakingnews;r:false;ccode:false;t4:false;elm:btn;elmt:follow;itc:1;aid:60f73942-c8f9-11e5-bc86-fa163e798f6a;" data-action-outcome="dclent">
                        <i class="followBtn:h_D(n) followed_D(n) Icon-Fp2 Fl(start) IconStarOutline Fz(13px) W(19px) H(19px) Lh(20px) Ta(c)"></i><i class="followBtn:h_D(b) followed_D(n) D(n) Icon-Fp2 Fl(start) IconStarFilled Fz(13px) W(19px) H(19px) Lh(20px) Ta(c) followBtn:h_C(#fff)"></i><div class="followed_D(n) Mstart(20px) Fz(13px) Tt(c) H(20px) W(54px) Trsdu(.1s) Trsp($trsp_width)"><div class="Fl(start) Lh(20px)">Follow</div></div>
                    </button>
                </span>
            
        </div>
    </div>
</div>


    

    
    <ul class=" js-stream-tmpl-items js-stream-dense  js-stream-hover-enable My(0) Mb(0) Wow(bw)" id="Stream">
    
        
    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Nathan_Deal|WIKIID:LGBT_community|WIKIID:Belief|YCT:001000780|YCT:001000661|YCT:001000681"  data-uuid="0e757951-3f47-31b5-b712-dd25cfd39de2" data-cauuid="0e757951-3f47-31b5-b712-dd25cfd39de2" data-type="article" data-cluster="0e757951-3f47-31b5-b712-dd25cfd39de2"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Pt(12px) Pb(17px)"><div class="js-stream-roundup js-stream-roundup-filmstrip Pos(r) Wow(bw) Cf">
    <div class="strm-headline Pos(r)">
        <a href="https://gma.yahoo.com/georgia-governor-said-veto-anti-lgbt-bill-145552868--abc-news-topstories.html" class="Pos(r) D(ib) Z(1) js-stream-content-link js-content-title js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:0e757951-3f47-31b5-b712-dd25cfd39de2;refcnt:4;subsec:needtoknow;imgt:ss;g:0e757951-3f47-31b5-b712-dd25cfd39de2;aid:id-3599730;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:Georgia governor to veto 'anti-LGBT' bill;elm:img;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
        
        <img src="/sy/uu/api/res/1.2/I7SnmJdWHMzeBnNwULm1wg--/Zmk9c3RyaW07aD0yNzQ7cHlvZmY9MDtxPTk1O3c9NzAyO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032816/images/smush/georgia_ipad_1459176596.jpg.cf.jpg" class="W(100%) rounded-img" alt="">
        
        <div class="Pos(a) T(0) Start(0) W(100%) H(100%) Ov(h)" title="Georgia governor plans to veto religious exemption bill. (AP)" >
            <div class="Pos(a) B(0) M(15px) Mb(11px) Mend(120px) C(#fff) Z(1)">
                <h2 class="js-stream-item-title Fz(21px) Fw(b) Mb(3px) Lh(24px) Td(u):h">Georgia governor to veto 'anti-LGBT' bill</h2>
                <span class="stream-summary Fz(13px) C(#d9d9db) Mend(5px)">Gov. Nathan Deal decision comes after companies and Hollywood heavyweights threatened to boycott the state if the bill was enacted.</span><span class="Fw(b) D(ib) Va(b) Lh(18px) Td(u):h">Details Â»</span></div>
            <div class="strm-img-gradient W(100%) H(100%) rounded-img"></div>
        </div>
        </a>
        <div class="Pos(a) End(13px) T(10px) W(30px) Mend(2px) Ta(end) Z(2)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) C(white)" data-cmntnum="1738"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow) js-stream-comment-hidden">1738</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) Tsh($comment_shadow)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C(white) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:0e757951-3f47-31b5-b712-dd25cfd39de2;refcnt:4;subsec:1738;imgt:ss;g:0e757951-3f47-31b5-b712-dd25cfd39de2;aid:id-3599730;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) Tsh($comment_shadow) ActionComments:h_C($signin_blue)"></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C(white) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) Tsh($comment_shadow)"></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C(white) P(14px)" role="button" tabindex="0"   data-ylk="cpos:1;cposy:1;bpos:1;pos:1;ss_cid:0e757951-3f47-31b5-b712-dd25cfd39de2;refcnt:4;subsec:1738;imgt:ss;g:0e757951-3f47-31b5-b712-dd25cfd39de2;aid:id-3599730;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) Tsh($comment_shadow)"></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C(white) Tsh($comment_shadow) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
    </div>
    <ul class="P(0) Mt(7px) Mstart(-2px) Mend(7px) Fz(12px) Whs(nw) Lts(-.31em)">
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="http://sports.yahoo.com/blogs/ncaab-the-dagger/why-roy-williams--blood-might-be-north-carolina-s-secret-weapon-045416343.html" data-uuid="7e5f69cf-40a9-391e-af37-f3a5d6568f2e" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:2;bpos:1;pos:2;ss_cid:0e757951-3f47-31b5-b712-dd25cfd39de2;refcnt:4;subsec:needtoknow;imgt:ss;g:7e5f69cf-40a9-391e-af37-f3a5d6568f2e;aid:id-3599722;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:Tar Heels coach injured cutting down nets;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/C53LgiFo7HVooayDuv.llA--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032816/images/smush/williams_ipad_1459173365.jpg.cf.jpg" class="W(100%) rounded-img" title="North Carolina coach Roy Williams. (Getty Images)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Tar Heels coach injured cutting down nets</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/katiecouric/house-republicans-speak-candidly-about-trump-210816631.html" data-uuid="a39b7400-b217-357c-9dd1-20988814dd99" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:3;bpos:1;pos:3;ss_cid:0e757951-3f47-31b5-b712-dd25cfd39de2;refcnt:4;subsec:needtoknow;imgt:ss;g:a39b7400-b217-357c-9dd1-20988814dd99;aid:id-3599676;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:GOP members slam Trump's lack of 'civility';elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/F1KBBtm2ktx5xCifIrOEpg--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032816/images/smush/republicans2_ipad_1459179437.jpeg.jpg.cf.jpg" class="W(100%) rounded-img" title="Republicans speak candidly about Trump. (Yahoo News)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">GOP members slam Trump's lack of 'civility'</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="http://sports.yahoo.com/news/excerpt--in--the-arm---a-search-for-the-new-frontier-of-building-healthy-baseball-pitchers-043238884-mlb.html" data-uuid="3c616156-c4d1-3fc5-9f5d-b9de0699e241" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:4;bpos:1;pos:4;ss_cid:0e757951-3f47-31b5-b712-dd25cfd39de2;refcnt:4;subsec:needtoknow;imgt:ss;g:3c616156-c4d1-3fc5-9f5d-b9de0699e241;aid:id-3599706;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:How baseball pitchers strive for health;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/QGN.b.zH026UwB2tUpKDBQ--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032816/images/smush/Arm2ipad_ipad_1459148966.jpg.cf.jpg" class="W(100%) rounded-img" title="In âThe Arm: Inside the Blllion-Dollar Mystery of the Most Valuable Commodity in Sports,â Yahooâs Jeff Passan digs deep into the reality and mythology of pitching. (Yahoo Sports)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">How baseball pitchers strive for health</h3>
            </a>
        </li>
        
        
        
        <li class="Pos(r) Va(t) D(ib) Bxz(bb) Whs(n) W(25%) Ov(h)">
            <a href="https://www.yahoo.com/music/music-mogul-diddy-job-charter-school-founder-120029026.html" data-uuid="fac6a496-6b6c-3d3d-9543-5c88928a4f04" class="js-stream-content-link js-stream-item-title js-content-title js-content-viewer rapid-noclick-resp rapidnofollow O(n):f C($m_blue):f Td(n) Fz(13px) Fw(b) Lts(n) Whs(n) D(b) Px(2px)"    data-ylk="cpos:1;cposy:5;bpos:1;pos:5;ss_cid:0e757951-3f47-31b5-b712-dd25cfd39de2;refcnt:4;subsec:needtoknow;imgt:ss;g:fac6a496-6b6c-3d3d-9543-5c88928a4f04;aid:id-3599741;expb:0;ct:1;pkgt:need_to_know;grpt:roundup;cnt_tpc:Need To Know;slk:Music mogul 'Diddy' founds a charter school;elm:rhdln;elmt:ct;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" >
                <img src="/sy/uu/api/res/1.2/i7GUEKqGHXiSGUdyf1A1tg--/Zmk9c3RyaW07aD0xNjA7cHlvZmY9MDtxPTgwO3c9MzQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://l.yimg.com/nn/fp/rsz/032816/images/smush/PuffyIpad_ipad_1459177174.jpg.cf.jpg" class="W(100%) rounded-img" title="Sean &quot;Diddy&quot; Combs, the founder of Capital Preparatory Harlem Charter School. (Matt Sayles/Invision/AP)" alt="">
                <h3 class="D(b) Mt(5px) Mend(10px) LineClamp(2,30px) Fz(12px) Fw(b) C($link) js-stream-content-link:h>C($m_blue) js-stream-content-link:f_C($m_blue)">Music mogul 'Diddy' founds a charter school</h3>
            </a>
        </li>
        
        
    </ul>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Jon_Lovitz|WIKIID:Jessica_Lowndes|WIKIID:Twitter|YCT:001000031|YCT:001000069|YCT:001000075"  data-uuid="ae5ddaea-62c7-36f8-a413-0367bc406060" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="https://www.yahoo.com/celebrity/jessica-lowndes-and-jon-lovitz-reveal-their-125602307.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;subsec:603;imgt:ss;g:ae5ddaea-62c7-36f8-a413-0367bc406060;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Celebrity;elm:img;elmt:ct;r:4000026610S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/uu/api/res/1.2/5PKOz28z8XiDFlTKDJaYIQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/cd/resizer/2.0/FIT_TO_WIDTH-w750/def489a966c42b67abf76b1c5fa7adb83b0bfa8b.jpg.cf.jpg" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_celebrity) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="celebrity">Celebrity</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="https://www.yahoo.com/celebrity/jessica-lowndes-and-jon-lovitz-reveal-their-125602307.html" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:2;cposy:6;bpos:1;pos:1;subsec:603;imgt:ss;g:ae5ddaea-62c7-36f8-a413-0367bc406060;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Celebrity;elm:hdln;elmt:ct;r:4000026610S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Jessica Lowndes and Jon Lovitz Reveal Their &#39;Secret&#39; Relationship - and They&#39;re Calling Out &#39;Haters&#39; Over Their 31-Year Age Gap</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">Jessica Lowndes and Jon Lovitz (Twitter)He may be â#TooOldToUseHashtags,â butÂ Jon Lovitz has still won Jessica Lowndesâs heart. The Saturday Night Live alum, 58, and Lowndes, 27, revealed their relationship in a series of posts on social media, over the</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Yahoo Premium Partners</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="603"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">603</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:2;cposy:6;bpos:1;pos:1;subsec:603;imgt:ss;g:ae5ddaea-62c7-36f8-a413-0367bc406060;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Celebrity;r:4000026610S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:2;cposy:6;bpos:1;pos:1;subsec:603;imgt:ss;g:ae5ddaea-62c7-36f8-a413-0367bc406060;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:Celebrity;r:4000026610S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad js-video-content Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=98sAVG8GIS8703s8LbZcT5osO7Fo6GpwPsT_0I2Iq9oQnlB5FyxjpX7ilhdstEPupitJ9fUoIe.5WIkkTRZhsQPYKehYIxT6Y8a4Jp8wAfnOZ6WjSZXHp3OLf9YEp12XWEzzP83LmsUlpfm2YpYzfeVPXezsOhAuwXqpOq5i4kOoyGk2Asv.Wz.3cfTZufxpSovOwB0g2rG0xLnvLEd8vofuupFuXfLs7R6SVJ3_XVodyjKmtPCveuuN2YkyOrImziJ6N.grhVkIMbu8djkVhFJEjSd1hJqObkBY3AWRfb9swTWO4xekhohr9a4gJzIefFalk9PYnpd1NhJDdCcoeprnPam4.5j7v5qWAQMRfMO3ZIdXPkgvzstC6N9EwBAZF58-&amp;ap=4" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15rbg797e(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31777728599,dmn$ferrerorocher.fr,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1362284,pbid$1,seid$4250754))&amp;r=1459183093282&amp;al=$(AD_FEEDBACK)" data-uuid="31777728599">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=vGn4HIMGIS_V9wcmxr33bOdjJIJ2jRD5FAC9mN4btKDwya9axyQOBOvOGRipUcbbfcRqcJyGo7y_nsV9pgAPC6LoXifvS7gjcm42yg1c_MrYxR1QxOWl1cmOB3W9BfglYernnWG0FRw5gFTZOa9JGODTxSYFmEBLQX2EcuYjvVWXNbwH0bqKNe6s8EywpFsJ8lRNryzkE5djA9Q0F6L4KieAaPyKC9ciVX9MsFcoe1t6IxY1YxAEXs7mtOZm823gQ4FkxJQ1aqF7daJOy8aC7WQ0PQgi94qG3egjKljfjRTcugI..orp5g3rPUEZ_0_aM84rLOegVY12.dEPNmhH.uObwVynRmvLJ4_B07sr1YUxv6yR.v11AuqaXoACyBXQnMw4CG93tUKt.Zzk1IXVqw1AeamspDXxJUl0O61UlLJ3KfQ2u53fXKoFhKSgwg9Blu4LLYnPKB8lQbYGx7mmuHj0ZP1PE1y3vhjFvp.Xcz.QzGDaGEdClQf4S8dT2z2ValszSTf0xj8F2xXt7zQZ.DyVWraXLXjo2NdzRq.lXiARgrMMDbmWm3JqYh9L.t1UjbkIEpWgqzV7yb5yPU9WYqralG6Olsd4BsWhxQ--%26lp=http%3A%2F%2Fferrero.solution.weborama.fr%2Ffcgi-bin%2Fdispatch.fcgi%3Fa.A%3Dcl%26a.si%3D1141%26a.te%3D324%26a.ycp%3D%26a.ra%3D76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700%26g.lu%3D" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i) js-video-target js-video-image" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:ct;g:31777728599;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/uu/api/res/1.2/3gcNV_XtqdYVLCvHVc3KBQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1458127951192-8173.jpg.cf.jpg" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img" alt="">
                </a>
            </div>
            
            <div class="strm-right Fl(start) W(66%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:sp;g:31777728599;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:info;g:31777728599;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <div class="Pos(r)">
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=vGn4HIMGIS_V9wcmxr33bOdjJIJ2jRD5FAC9mN4btKDwya9axyQOBOvOGRipUcbbfcRqcJyGo7y_nsV9pgAPC6LoXifvS7gjcm42yg1c_MrYxR1QxOWl1cmOB3W9BfglYernnWG0FRw5gFTZOa9JGODTxSYFmEBLQX2EcuYjvVWXNbwH0bqKNe6s8EywpFsJ8lRNryzkE5djA9Q0F6L4KieAaPyKC9ciVX9MsFcoe1t6IxY1YxAEXs7mtOZm823gQ4FkxJQ1aqF7daJOy8aC7WQ0PQgi94qG3egjKljfjRTcugI..orp5g3rPUEZ_0_aM84rLOegVY12.dEPNmhH.uObwVynRmvLJ4_B07sr1YUxv6yR.v11AuqaXoACyBXQnMw4CG93tUKt.Zzk1IXVqw1AeamspDXxJUl0O61UlLJ3KfQ2u53fXKoFhKSgwg9Blu4LLYnPKB8lQbYGx7mmuHj0ZP1PE1y3vhjFvp.Xcz.QzGDaGEdClQf4S8dT2z2ValszSTf0xj8F2xXt7zQZ.DyVWraXLXjo2NdzRq.lXiARgrMMDbmWm3JqYh9L.t1UjbkIEpWgqzV7yb5yPU9WYqralG6Olsd4BsWhxQ--%26lp=http%3A%2F%2Fferrero.solution.weborama.fr%2Ffcgi-bin%2Fdispatch.fcgi%3Fa.A%3Dcl%26a.si%3D1141%26a.te%3D324%26a.ycp%3D%26a.ra%3D76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700%26g.lu%3D" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:ad;g:31777728599;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class=" D(b) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>Åuf magique de PÃ¢ques</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u>
                    </a>
                </h3>
                <div>
                    <p class=" stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">CÃ©lÃ©brez PÃ¢ques et charmez vos invitÃ©s avec une dÃ©licate attention Ã  fabriquer vous-mÃªme</p>
                </div><div class="Pos(r)">
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=vGn4HIMGIS_V9wcmxr33bOdjJIJ2jRD5FAC9mN4btKDwya9axyQOBOvOGRipUcbbfcRqcJyGo7y_nsV9pgAPC6LoXifvS7gjcm42yg1c_MrYxR1QxOWl1cmOB3W9BfglYernnWG0FRw5gFTZOa9JGODTxSYFmEBLQX2EcuYjvVWXNbwH0bqKNe6s8EywpFsJ8lRNryzkE5djA9Q0F6L4KieAaPyKC9ciVX9MsFcoe1t6IxY1YxAEXs7mtOZm823gQ4FkxJQ1aqF7daJOy8aC7WQ0PQgi94qG3egjKljfjRTcugI..orp5g3rPUEZ_0_aM84rLOegVY12.dEPNmhH.uObwVynRmvLJ4_B07sr1YUxv6yR.v11AuqaXoACyBXQnMw4CG93tUKt.Zzk1IXVqw1AeamspDXxJUl0O61UlLJ3KfQ2u53fXKoFhKSgwg9Blu4LLYnPKB8lQbYGx7mmuHj0ZP1PE1y3vhjFvp.Xcz.QzGDaGEdClQf4S8dT2z2ValszSTf0xj8F2xXt7zQZ.DyVWraXLXjo2NdzRq.lXiARgrMMDbmWm3JqYh9L.t1UjbkIEpWgqzV7yb5yPU9WYqralG6Olsd4BsWhxQ--%26lp=http%3A%2F%2Fferrero.solution.weborama.fr%2Ffcgi-bin%2Fdispatch.fcgi%3Fa.A%3Dcl%26a.si%3D1141%26a.te%3D324%26a.ycp%3D%26a.ra%3D76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700%26g.lu%3D" target="_blank"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:ad;g:31777728599;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Ferrero Rocher</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:3;cposy:7;bpos:1;pos:1;ad:1;elmt:op;g:31777728599;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000661|YCT:001000705|YCT:001000680" data-offnet="1" data-uuid="91140e98-8f83-3058-bb7a-cf99e366e9d1" data-type="article" data-cluster="33c27b12-07d3-4ab5-9853-99a35f669397"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://www.ibtimes.com/north-korea-calls-its-multiple-launch-rocket-system-horrible-nightmare-us-says-2344018" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:4;cposy:8;bpos:1;pos:1;ss_cid:33c27b12-07d3-4ab5-9853-99a35f669397;refcnt:2;imgt:ss;g:91140e98-8f83-3058-bb7a-cf99e366e9d1;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:img;elmt:ct;r:4000022180S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/cQBdxA9lKIuK653P1bKB5g--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/f418a705fb25861a08124a71430cb391" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px) Pos(r)">
            <div class="strm-headline-label js-content-label C(c_world) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="world">World</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.ibtimes.com/north-korea-calls-its-multiple-launch-rocket-system-horrible-nightmare-us-says-2344018" class="O(n):f C($m_blue):f D(b)  js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:4;cposy:8;bpos:1;pos:1;ss_cid:33c27b12-07d3-4ab5-9853-99a35f669397;refcnt:2;imgt:ss;g:91140e98-8f83-3058-bb7a-cf99e366e9d1;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:hdln;elmt:ct;r:4000022180S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>North Korea Calls Its Multiple Launch Rocket System A âHorrible Nightmareâ For US, Says Washington Is Scared</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">The North Korean state-run media slammed Washington&#39;s response to the formerâs test-fire of a new-type large-caliber multiple launch rocket system last week and said Monday that the U.S. was scared of the system. The test-fire occurred last Monday and amid accusations that Pyongyang was increasing provocations in the region with its missile and nuclear tests. The latest statement by North Korea was made by its state-controlled website Uriminzokkiri, which added that the U.S. has deployed strategic and tactical nuclear weapons in South Korea after calculating that the efforts would halt Pyongyangâs nuclear advancement. However, the statement said that Washingtonâs âwild dream went up in smoke</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">International Business Times</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.huffingtonpost.com/doug-bandow/how-to-convince-china-to_b_9554496.html?ncid=txtlnkusaolp00000592" data-uuid="38d40b80-7f4d-3626-aeac-11f6997952a3" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:4;cposy:9;bpos:1;pos:2;ss_cid:33c27b12-07d3-4ab5-9853-99a35f669397;refcnt:2;imgt:ss;g:38d40b80-7f4d-3626-aeac-11f6997952a3;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:rhdln;elmt:ct;r:4000022180S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/Kc3Ek5xtPbtnuexDmN6VOg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/3840a9ebae72efb71f7eb702271c1cfe" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">How to Convince China to Cooperate Against North Korea</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">The Huffington Post</span></div></a>
            
            
            
                <a href="http://mashable.com/2016/03/26/north-korea-destroys-washington-propaganda-video/?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+Mashable+%28Mashable%29" data-uuid="47501b6f-fe32-3248-a903-87b1dab4bfa2" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:4;cposy:10;bpos:1;pos:3;ss_cid:33c27b12-07d3-4ab5-9853-99a35f669397;refcnt:2;imgt:ss;g:47501b6f-fe32-3248-a903-87b1dab4bfa2;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;elm:rhdln;elmt:ct;r:4000022180S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="tech"><img src="/sy/uu/api/res/1.2/07lhmQsMRuPgW0MQutkTIA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/577d6432490a76c1ef1c81181a953829" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">North Korea propaganda video depicts nuclear missile obliterating Washington</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Mashable</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:4;cposy:8;bpos:1;pos:1;ss_cid:33c27b12-07d3-4ab5-9853-99a35f669397;refcnt:2;imgt:ss;g:91140e98-8f83-3058-bb7a-cf99e366e9d1;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:World;r:4000022180S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Jacob_Martin|YCT:001000713|YCT:001000804|YCT:001000780|YCT:001000717" data-offnet="1" data-uuid="087d92e4-b248-3998-afc9-fce7b0bbf815" data-type="article" data-cluster="040721b0-8948-48be-a55e-a3bd66583e2c"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://nypost.com/2016/03/27/protesters-disrupt-easter-mass-at-st-pats-frighten-worshipers/" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:040721b0-8948-48be-a55e-a3bd66583e2c;refcnt:2;imgt:ss;g:087d92e4-b248-3998-afc9-fce7b0bbf815;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;elm:img;elmt:ct;r:4000026250S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/D8Eyl6AQfP9vmlFc3arXNg--/Zmk9c3RyaW07aD0zODA7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/13166b401894b27245be71d08e99bcfd" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px) Pos(r)">
            <div class="strm-headline-label js-content-label C(c_entertainment) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="entertainment">Entertainment</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://nypost.com/2016/03/27/protesters-disrupt-easter-mass-at-st-pats-frighten-worshipers/" class="O(n):f C($m_blue):f D(b)  js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:040721b0-8948-48be-a55e-a3bd66583e2c;refcnt:2;imgt:ss;g:087d92e4-b248-3998-afc9-fce7b0bbf815;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;elm:hdln;elmt:ct;r:4000026250S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Protesters disrupt Easter Mass at St. Patâs, frighten worshipers</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Worshipers attending Easter Mass at St. Patrickâs Cathedral got the scare of their lives when a band of unruly protesters disrupted the event. âI thought they were going to blow themselves up,â worshiper Carol Forester, 50, confessed. A group of six animal-rights protesters abruptly leaped up from a pew in the middle of the service and shouted, âEaster is a time for love! No more shedding animal blood!â while holding up signs of animals pleading for their lives. About 20 minutes into Rev. Damian OâConnellâs noon Mass, protester Jacob Martin, 23, rose out of his seat in the center of the church and started to walk down the aisle while shouting into a bullhorn that âonly the devilâ could create</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">New York Post</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://www.chicagotribune.com/suburbs/daily-southtown/news/ct-sta-easter-oak-forest-st-0328-20160327-story.html" data-uuid="ba09fd07-77c0-32fa-b9e3-b8f9d4c53a15" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:5;cposy:12;bpos:1;pos:2;ss_cid:040721b0-8948-48be-a55e-a3bd66583e2c;refcnt:2;imgt:ss;g:ba09fd07-77c0-32fa-b9e3-b8f9d4c53a15;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;elm:rhdln;elmt:ct;r:4000026250S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/5fYoQIeIpXgqNryW6N0n.Q--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/a144f493fd54b8ce552911e1a202131c" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Hundreds pack Oak Forest church for Easter Mass</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Chicago Tribune</span></div></a>
            
            
            
                <a href="http://news.yahoo.com/video/queen-attends-traditional-easter-mass-163250618.html" data-uuid="48bb1f57-fece-3ba0-989b-e0e467ebef5f" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:5;cposy:13;bpos:1;pos:3;ss_cid:040721b0-8948-48be-a55e-a3bd66583e2c;refcnt:2;imgt:ss;g:48bb1f57-fece-3ba0-989b-e0e467ebef5f;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;elm:rhdln;elmt:ct;r:4000026250S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/uu/api/res/1.2/bPDVahJgc8mi._7vg4sJkA--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/video/video.reutersnews.com/2016-03-27T161431Z_1_LOP000HXLLVLH_RTRMADP_BASEIMAGE-960X540_BRITAIN-ROYALS-QUEEN-EASTER-ROUGH-CUT.JPG.cf.jpg" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Queen attends traditional Easter Mass</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Reuters Videos</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:5;cposy:11;bpos:1;pos:1;ss_cid:040721b0-8948-48be-a55e-a3bd66583e2c;refcnt:2;imgt:ss;g:087d92e4-b248-3998-afc9-fce7b0bbf815;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Entertainment;r:4000026250S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Tammy_Miller|YCT:001000288|YCT:001000780"  data-uuid="f31f237f-922f-3c3a-81e9-d2f96c277346" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://news.yahoo.com/never-call-her-again-daughter-133319884.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:6;cposy:14;bpos:1;pos:1;subsec:2208;imgt:ss;g:f31f237f-922f-3c3a-81e9-d2f96c277346;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000020950S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/uu/api/res/1.2/qBK3RcBKoZsxjxBcWl01qw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/people_218/ba0f26442bd3664e9c9f88c704d704b6" height="107" width="190" class="W(100%) BackgroundPic rounded-img" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_us) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://news.yahoo.com/never-call-her-again-daughter-133319884.html" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:6;cposy:14;bpos:1;pos:1;subsec:2208;imgt:ss;g:f31f237f-922f-3c3a-81e9-d2f96c277346;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000020950S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>&#39;I Will Never Call Her Again&#39;: Daughter of Missing Indiana Woman Discovered 42 Years Later Has No Plans for a Happy Reunion</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">After more than 40 years of thinking her mother was dead, Tammy Miller got the surprise of her life this week when she found out her that her mother was actually alive and living under a new name 1,200 miles away. &quot;I&apos;m angry,&quot; Miller, 45, tells PEOPLE in her first extensive interview. Miller&apos;s mother, Lula Ann Gillespie-Miller, left her Laurel, Indiana, home in 1974 at the age of 28, leaving behind Tammy and her other three small children - another girl and two boys.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">People</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="2208"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">2208</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:14;bpos:1;pos:1;subsec:2208;imgt:ss;g:f31f237f-922f-3c3a-81e9-d2f96c277346;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000020950S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:6;cposy:14;bpos:1;pos:1;subsec:2208;imgt:ss;g:f31f237f-922f-3c3a-81e9-d2f96c277346;ct:1;pkgt:orphan_img;grpt:storyCluster;cnt_tpc:U.S.;r:4000020950S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:United_States_Customs_Service|WIKIID:Semi-submersible|WIKIID:Narco-submarine|WIKIID:Pacific_Ocean|YCT:001000667|YCT:001000780"  data-uuid="a426e21e-770e-3815-96d7-f7c400a825e1" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://finance.yahoo.com/news/us-agents-nearly-caught-194-191227843.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:7;cposy:15;bpos:1;pos:1;subsec:535;imgt:ss;g:a426e21e-770e-3815-96d7-f7c400a825e1;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;elm:img;elmt:ct;r:4000010130S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/pq4YJi0g9oGjh1e4DZKMBQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/US_agents_nearly_caught_194-6ac3fed76f5241008896ea67b3bbc28d')" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_us) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="us">U.S.</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://finance.yahoo.com/news/us-agents-nearly-caught-194-191227843.html" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:7;cposy:15;bpos:1;pos:1;subsec:535;imgt:ss;g:a426e21e-770e-3815-96d7-f7c400a825e1;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;elm:hdln;elmt:ct;r:4000010130S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>US agents nearly caught $194 million worth of cocaine in a narco submarine</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">A US Navy P-3 Orion Maritime patrol aircraft takes off from Incirlik airbase in the southern city of Adana, Turkey, July 26, 2015. US Customs and Border Protection Air and Marine Operations agents had a close call with a high-value target in the Pacific Ocean. Agents later intercepted the semi-submersible, on which they found more than 12,800 pounds of cocaine, an amount with an estimated value of $193,939,000, according to aÂ CBP release issued on March 24.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Business Insider</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="535"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">535</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:15;bpos:1;pos:1;subsec:535;imgt:ss;g:a426e21e-770e-3815-96d7-f7c400a825e1;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;r:4000010130S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:7;cposy:15;bpos:1;pos:1;subsec:535;imgt:ss;g:a426e21e-770e-3815-96d7-f7c400a825e1;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:U.S.;r:4000010130S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    
<li class="js-stream-ad-noview  js-stream-ad  Wow(bw) Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)" data-beacon="https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&amp;es=w1Xw7J4GIS.bz9SOqpo2Po4avih4P66eGD6EBDpSQh9F28RJAHVCVbD8w44U55KMj0kT2wQhdwbj49Uu8p5HFELR8dnI6_KHRvSWwA67oTduHahWUdBze3qPvGVRnAgyLWcLS8tYMNHUHjwuyFhIFbJSndJhowSRkWfGI8xGzwMPvr6qwVOJc6TY7AMV7Nd35xaEPOAY.3dNF5.6Xz4YHssfOLty2ZmbgFf1m9BG26GPg7tXkt9Bi3uSSwh0ySnWW2udS2Mu4zC5pGl9JXl6cDBncQ2bfqPvTSxdm8IghZ0FV_jb8JmeV2fl6RsJ9biNr0V.s7Bcm237jmdd7FjlX_kQ7fXI12uDCN8mmjsLzpl4lqxp2uTTtCWyVcW5buJPn0k_D1hwcw--&amp;ap=9" data-ad-feedback-beacon="https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&amp;bs=(15nn0t8ck(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31834348738,dmn$c31834348738,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1167625,pbid$1,seid$4250754))&amp;r=1459183093282&amp;al=$(AD_FEEDBACK)" data-uuid="31834348738">
    <div class="js-stream-item-wrap Py(15px) Pos(r)">
        <div class="Pos(r) Wow(bw) Cf Pend(30px)">
            
            <div class="strm-left Fl(start) Pos(r) Mend(18px)">
                <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=sWrxzggGIS8J6IOt2wxOpz8P.I5dMeMa2NoiZ5nkahCqnNe29z.si1c5.vp1G08is.sO_E0rblr_631sEjvmS7r51SwOWF029NRttLHy9VLjTH.BEz6PHmRiLU44QsZTy_yQnqWICKr8quZ3engPGW1XG4G63cDk0qdgZqDbykR7vV.cjSHf54NrOsV1G2F9eKM.TCXi9Pp2laf71LHCEE8WYabxWAWMycNtYuKiV4k4DJIhfyfqHYVqjXvx5Znxl102q46i9I.FWCYg3j3BfO7Wb_XlcaqIP8Gaxeboo3pPLEBAqhbYdPQYGv7GEh9wmEToqKvVTI8SiGJiyFqhMrFzS2p5D6ijiyPWU1IeGgecY069n32d9gtB21rf9cdpEsKrRYK2UsCf4e_am2k8rTTHGCVY9qDqDt3r2EUcwNpvxpsLoZRAOhThQrEtBiaJbBDtN_3.dhs14g3g2A0ANF_hHRLb1HsndLRdRZt5wRGPL_H0aFD_Hi4WtcL1HyGPC1I-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ff237962d-3d60-4fc4-9627-5445dad3618f%3Fad%3D3" class="streamImage Pos(r) D(ib) W(100%) ua-ie8_D(i)" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ct;g:31834348738;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:img;r:false;ccode:mega_global_ranking_hlv2_up_based;" >
                    <img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="fea-ad H(100%) W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/5ndGLYVzYZEgEEj60lQsJA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1458911382048-7237.jpg.cf.jpg')" alt="">
                </a>
            </div>
            
            <div class="strm-right Fl(start) W(66%)">
                
                <div class="Fw(b) Lh(17px) Mb(4px)">
                    <a class="Mend(4px) Fz(13px) D(ib) Va(m) Td(n) C($sponsored) Tt(c)" href="http://help.yahoo.com/kb/index?page=content&amp;y=PROD_FRONT&amp;locale=en_US&amp;id=SLN14553" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:sp;g:31834348738;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Sponsored</a>
                    <a class="D(ib) Va(m) Td(n) adlink C($sponsored)" href="https://info.yahoo.com/privacy/us/yahoo/relevantads.html" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:info;g:31834348738;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;"><i class="Icon-Fp2 IconMoneyball Fz(13px) D(ib)"></i></a>
                </div>
                
                <div class="Pos(r)">
                <h3 class="Mb(4px) Lh(21px)">
                    <a href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=sWrxzggGIS8J6IOt2wxOpz8P.I5dMeMa2NoiZ5nkahCqnNe29z.si1c5.vp1G08is.sO_E0rblr_631sEjvmS7r51SwOWF029NRttLHy9VLjTH.BEz6PHmRiLU44QsZTy_yQnqWICKr8quZ3engPGW1XG4G63cDk0qdgZqDbykR7vV.cjSHf54NrOsV1G2F9eKM.TCXi9Pp2laf71LHCEE8WYabxWAWMycNtYuKiV4k4DJIhfyfqHYVqjXvx5Znxl102q46i9I.FWCYg3j3BfO7Wb_XlcaqIP8Gaxeboo3pPLEBAqhbYdPQYGv7GEh9wmEToqKvVTI8SiGJiyFqhMrFzS2p5D6ijiyPWU1IeGgecY069n32d9gtB21rf9cdpEsKrRYK2UsCf4e_am2k8rTTHGCVY9qDqDt3r2EUcwNpvxpsLoZRAOhThQrEtBiaJbBDtN_3.dhs14g3g2A0ANF_hHRLb1HsndLRdRZt5wRGPL_H0aFD_Hi4WtcL1HyGPC1I-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ff237962d-3d60-4fc4-9627-5445dad3618f%3Fad%3D3" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ad;g:31834348738;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:hdln;r:false;ccode:mega_global_ranking_hlv2_up_based;" class=" D(b) js-stream-content-link js-stream-item-title js-content-title Td(n) Fz(17px) Fw(b) C($link) C($m_blue):h O(n):f C($m_blue):f" >
                        <span>Un Nouvel Algorithme De Trading Secoue La France</span>
                        <u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u>
                    </a>
                </h3>
                <div>
                    <p class=" stream-summary LineClamp(2,36px) Lh(17px) C($gray) Bfv(h)">Ce nouvel algorithme de trading vient d'Ãªtre rÃ©vÃ©lÃ© au public et est en train de secouer toute la France.</p>
                </div><div class="Pos(r)">
                    <a class="Fz(11px) D(ib) Td(n) C(gray_dark) Tt(c) Mt(4px)" href="https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&amp;es=sWrxzggGIS8J6IOt2wxOpz8P.I5dMeMa2NoiZ5nkahCqnNe29z.si1c5.vp1G08is.sO_E0rblr_631sEjvmS7r51SwOWF029NRttLHy9VLjTH.BEz6PHmRiLU44QsZTy_yQnqWICKr8quZ3engPGW1XG4G63cDk0qdgZqDbykR7vV.cjSHf54NrOsV1G2F9eKM.TCXi9Pp2laf71LHCEE8WYabxWAWMycNtYuKiV4k4DJIhfyfqHYVqjXvx5Znxl102q46i9I.FWCYg3j3BfO7Wb_XlcaqIP8Gaxeboo3pPLEBAqhbYdPQYGv7GEh9wmEToqKvVTI8SiGJiyFqhMrFzS2p5D6ijiyPWU1IeGgecY069n32d9gtB21rf9cdpEsKrRYK2UsCf4e_am2k8rTTHGCVY9qDqDt3r2EUcwNpvxpsLoZRAOhThQrEtBiaJbBDtN_3.dhs14g3g2A0ANF_hHRLb1HsndLRdRZt5wRGPL_H0aFD_Hi4WtcL1HyGPC1I-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ff237962d-3d60-4fc4-9627-5445dad3618f%3Fad%3D3" target="_blank"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:ad;g:31834348738;ct:1;pkgt:sponsored_img;grpt:singlestory;elm:itm;r:false;ccode:mega_global_ranking_hlv2_up_based;">Preditrend</a>
                    
                </div>
            </div>
            <div class="Pos(a) End(-7px) T(0) W(30px) Mend(2px) Ta(end)">
    <ul class="js-stream-side-buttons js-stream-actions">
         <li class="ActionDislike Lh(10px)">
            <a href="javascript:void(0)" class="js-stream-dislike-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(10px)" role="button"   data-ylk="cpos:8;cposy:16;bpos:1;pos:1;ad:1;elmt:op;g:31834348738;ct:1;pkgt:sponsored_img;grpt:singlestory;r:false;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;itc:1;rspns:op;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) T(-10px) End(-38px)! Py(5px) js-stream-dislike-button>Start(50%) js-stream-dislike-button:h>Start(a) C(#000)">Dislike</b><i class="Icon-Fp2 IconActionCross Fz(16px) C(#000):h js-stream-ad_D(n) js-stream-ad:h_D(i)"></i></a>
        </li>
    </ul>
</div>

        </div>
    </div>
</li>


    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Sergei_Fyodorov|YCT:001000742"  data-uuid="4a468fa6-203d-33df-b889-843ac9ea20e2" data-type="article"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://news.yahoo.com/cavemans-best-friends-preserved-ice-age-puppies-awe-052035084.html" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:9;cposy:17;bpos:1;pos:1;subsec:387;imgt:ss;g:4a468fa6-203d-33df-b889-843ac9ea20e2;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Science;elm:img;elmt:ct;r:4000017320S00001;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/EIX6eaLTmA6ecR3zbF4uUA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_us/News/afp.com/Part-DV-DV2228067-1-1-0.jpg.cf.jpg')" alt="">

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_science) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="science">Science</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://news.yahoo.com/cavemans-best-friends-preserved-ice-age-puppies-awe-052035084.html" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:9;cposy:17;bpos:1;pos:1;subsec:387;imgt:ss;g:4a468fa6-203d-33df-b889-843ac9ea20e2;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Science;elm:hdln;elmt:ct;r:4000017320S00001;ccode:mega_global_ranking_hlv2_up_based;" ><span>Caveman&#39;s best friends? Preserved Ice Age puppies awe scientists</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">The hunters searching for mammoth tusks were drawn to the steep riverbank by a deposit of ancient bones. To their astonishment, they discovered an Ice Age puppy&#39;s snout peeking out from the permafrost. Five years later, a pair of puppies perfectly preserved in Russia&#39;s far northeast region of Yakutia and dating back 12,460 years has mobilised scientists across the world.</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">AFP</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="387"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">387</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:17;bpos:1;pos:1;subsec:387;imgt:ss;g:4a468fa6-203d-33df-b889-843ac9ea20e2;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Science;r:4000017320S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:9;cposy:17;bpos:1;pos:1;subsec:387;imgt:ss;g:4a468fa6-203d-33df-b889-843ac9ea20e2;ct:1;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Science;r:4000017320S00001;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) js-stream-cluster Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="WIKIID:Donald_Trump|WIKIID:Ted_Cruz|YCT:001000671|YCT:001000661" data-offnet="1" data-uuid="72e6998d-d33e-3f67-91e1-c771f4cfd89b" data-type="article" data-cluster="69d340bc-91d0-4e0d-ac9b-38f81c5a6bfd"  data-hosted="NON_HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="strm-default-clusters Pos(r) Wow(bw) Cf Pend(30px) ">

    
    <div class="strm-left Fl(start) Mend(18px) Pos(r) "><a href="http://www.cnn.com/2016/03/27/politics/ted-cruz-could-win-more-delegates-than-donald-trump-in-louisiana/index.html" class="streamImage Pos(r) D(ib) js-stream-content-link W(100%)   js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:10;cposy:18;bpos:1;pos:1;ss_cid:69d340bc-91d0-4e0d-ac9b-38f81c5a6bfd;refcnt:2;imgt:ss;g:72e6998d-d33e-3f67-91e1-c771f4cfd89b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:img;elmt:ct;r:4000022320S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="190" width="190" class="W(100%) H(a) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/A_eRD6CQRL0A_q2XUcEoYw--/Zmk9c3RyaW07aD0xOTA7cHlvZmY9MDtxPTk1O3c9MTkwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/fd8e0c4f61513f4ff1a0590c05d6fdb3')" alt="">

</a></div>
    
    <div class="strm-right Pos(r) Mstart(29%)"><div class="strm-headline Pend(14px) Pos(r)">
            <div class="strm-headline-label js-content-label C(c_politics) Fw(b) Mb(4px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="politics">Politics</div>
            <h3 class="Mb(4px) Mb(0px)--md1160 LineClamp(2,42px) Lh(21px)">
                <a href="http://www.cnn.com/2016/03/27/politics/ted-cruz-could-win-more-delegates-than-donald-trump-in-louisiana/index.html" class="O(n):f C($m_blue):f D(b)  js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:10;cposy:18;bpos:1;pos:1;ss_cid:69d340bc-91d0-4e0d-ac9b-38f81c5a6bfd;refcnt:2;imgt:ss;g:72e6998d-d33e-3f67-91e1-c771f4cfd89b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:hdln;elmt:ct;r:4000022320S00008;ccode:mega_global_ranking_hlv2_up_based;" rel="nofollow" data-attrcat="news"><span>Trump threatens lawsuit over delegate rules</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,34px) Lh(17px) C($gray) Bfv(h) D(n)--md1160">Washington (CNN)Ted Cruz came in second in the recent Louisiana Republican primary behind Donald Trump but could win more delegates -- and the the real estate mogul is crying foul. Trump beat the Texas senator in the March 5 contest by 3.6%. Under party rules the pair each won 18 delegates. But Cruz&#39;s campaign is using its organization muscle to sway ten more delegates toward his camp, The Wall Street Journal reported on Friday -- a situation that seems to have caught Trump&#39;s ire. &quot;Just to show you how unfair Republican primary politics can be, I won the State of Louisiana and get less delegates than Cruz-Lawsuit coming,&quot; Trump tweeted Sunday evening. Trump&#39;s missive reflects the still-unclear</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Tt(c) Mt(2px)">CNN</span>
            </div>
        </div>
        
        <div class="Mt(8px) Mt(7px)--md1160 Ov(h)">
            
            
                <a href="http://nation.foxnews.com/2016/03/27/cruz-trump-war-wives-new-campaign-low-underscores-donalds-problem-female-voters" data-uuid="ad00743c-5742-3b74-bcf1-96903e5a50e2" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Mb(6px)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb) Pend(13px)"    data-ylk="cpos:10;cposy:19;bpos:1;pos:2;ss_cid:69d340bc-91d0-4e0d-ac9b-38f81c5a6bfd;refcnt:2;imgt:ss;g:ad00743c-5742-3b74-bcf1-96903e5a50e2;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000022320S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/JPyhlyRxQ1bnEiKs5D7oNg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://slingstone.zenfs.com/offnetwork/78c7a09e6dec2445ab74cea488d872c4')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Cruz-Trump War of the Wives: A New Campaign Low That Underscores 'The Donald's' Problem with Female Voters</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">Fox Nation</span></div></a>
            
            
            
                <a href="https://gma.yahoo.com/donald-trump-blames-ted-cruz-spat-leading-national-150622479.html" data-uuid="1a775ef3-2024-334f-b5ca-80f5938ee101" class="O(n):f C($m_blue):f Pos(r) Mih(70px) Mih(0)--md1160 Pstart(82px) Pstart(0)--md1160 Fl(start) W(50%) W(a)--md1160 strm-sl-link js-stream-content-link js-stream-item-title js-content-title js-sl js-content-viewer rapid-noclick-resp rapidnofollow C($link) C($m_blue):h Td(n) Fz(12px) Bxz(bb)"    data-ylk="cpos:10;cposy:20;bpos:1;pos:3;ss_cid:69d340bc-91d0-4e0d-ac9b-38f81c5a6bfd;refcnt:2;imgt:ss;g:1a775ef3-2024-334f-b5ca-80f5938ee101;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;elm:rhdln;elmt:ct;r:4000022320S00008;ccode:mega_global_ranking_hlv2_up_based;t4:rel;" rel="nofollow" data-attrcat="news"><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="70" width="70" class="Maw(100%) H(a) Mend(12px) Va(t) Pos(a) Start(0) rounded-img D(n)--md1160 ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/hXg.MR6xUmiweBgf55Wrxg--/Zmk9c3RyaW07aD0xNDA7cHlvZmY9MDtxPTkwO3c9MTQwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en_us/gma/us.abcnews.go.com/AP_Trump_Cruz_debate_bc_160327_16x9_992.jpg.cf.jpg')" alt=""><div class="D(ib) W(100%)"><div class="Mend(7px) Lh(15px) LineClamp(3,45px) Fw(b) D(i)--md1160">Donald Trump, Ted Cruz Trade Barbs Over National Enquirer Report</div><span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(b) Mt(2px) Whs(pl) Tt(c) D(i)--md1160">ABC News</span></div></a>
            
            
        </div>
        
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions js-stream-dislike-disabled">
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:10;cposy:18;bpos:1;pos:1;ss_cid:69d340bc-91d0-4e0d-ac9b-38f81c5a6bfd;refcnt:2;imgt:ss;g:72e6998d-d33e-3f67-91e1-c771f4cfd89b;ct:1;pkgt:cluster_all_img;grpt:storyCluster;cnt_tpc:Politics;r:4000022320S00008;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    

    <li class="js-stream-content Pos(r) Mx(a) Mt(-1px) Bxz(bb) Bgc(#fff)  " data-interests="YCT:001000931"  data-uuid="52858f64-611c-39ee-9e0d-9957abfabd79" data-type="slideshow"  data-hosted="HOSTED">
    <div class="js-stream-item-wrap Pos(r) Py(12px)"><div class="Pos(r) Wow(bw) Cf Pend(30px)">

    
    <div class="strm-left Fl(start) Pos(r) Pend(18px)"><div class="Pos(r)"><a href="http://news.yahoo.com/photos/10-leather-tote-bags-help-143131428/" class="Pos(r) D(ib) streamImage js-stream-content-link W(100%)  js-content-viewer rapid-noclick-resp rapidnofollow"    data-ylk="cpos:11;cposy:21;bpos:1;pos:1;subsec:9;imgt:ss;g:52858f64-611c-39ee-9e0d-9957abfabd79;ct:4;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Style;elm:img;elmt:ct;r:4000004720S00004;ccode:mega_global_ranking_hlv2_up_based;" ><img src="/sy/os/mit/ape/m/81f43c2/t.gif" height="107" width="190" class="W(100%) BackgroundPic rounded-img ImageLoader" style="background-image:url('https://s.yimg.com/uu/api/res/1.2/YlOwd8O1jAtgk8rnO61AtA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/http://media.zenfs.com/en-US/homerun/elle_570/498fcad01ad0c047ac0bb7ea3368bb72')" alt="">

    <span class="Pos(a) T(50%) Start(50%) Mstart(-33px) Mt(-33px) orb-sprite slideshow-icon Bgp(-228px,-32px) W(66px) H(66px) Ta(c) Va(m)"></span>

</a>
        </div>
    </div>
    
    <div class="strm-right Fl(start) W(66%)"><div class="strm-headline Pos(r)">
            <div class="js-content-label C(c_style) Fw(b) Mb(6px) Lh(12px) Fz(13px) Tt(c)" data-cat-label="style">Style</div>
            <h3 class="Mb(4px) LineClamp(3,57px) Lh(19px)">
                <a href="http://news.yahoo.com/photos/10-leather-tote-bags-help-143131428/" class=" O(n):f C($m_blue):f js-stream-content-link js-stream-item-title js-content-viewer rapid-noclick-resp rapidnofollow js-content-title C($link) C($m_blue):h Td(n) Fz(17px) Fw(b)"    data-ylk="cpos:11;cposy:21;bpos:1;pos:1;subsec:9;imgt:ss;g:52858f64-611c-39ee-9e0d-9957abfabd79;ct:4;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Style;elm:hdln;elmt:ct;r:4000004720S00004;ccode:mega_global_ranking_hlv2_up_based;" ><span>10 Leather Tote Bags That Will Help You Chicly Carry Your S*it</span><u class="Pos(a) T(0) Start(0) W(100%) H(100%) strm-stretch"></u></a>
            </h3>
            <div>
                <p class=" stream-summary LineClamp(2,33px) Lh(17px) C($gray) Bfv(h)">âIf you must carry everything but the kitchen sink, do so in style.ââ</p>
                <span class="Fz(11px) Td(n) C(gray_dark) Fw(n) D(ib) Mt(5px) Tt(c)">Elle</span>
            </div>
        </div>
    </div>

    <div class="Pos(a) End(0) T(0) W(30px) Mstart(12px) Mend(2px) Ta(end)">
<ul class="js-stream-side-buttons js-stream-actions js-stream-tumblr-actions has-comments js-stream-dislike-disabled">
    
    <li class="ActionComments D(ib) O(n) Mstart(-1px) Pos(r) " data-cmntnum="9"><span class="js-stream-comment-counter D(b) Fz(11px) Ta(c) Fw(b) C(#96989f) js-stream-comment-hidden">9</span><span class="js-stream-comment-counter-update D(b) Fz(11px) Ta(c) Fw(b) C(#96989f)"></span><a href="javascript:void(0)" class="Pos(r) js-stream-comments-button-drawer Td(n) O(n) C($c_icon) Px(14px) Pb(5px) Pt(25px)" role="button" tabindex="0"   data-ylk="cpos:11;cposy:21;bpos:1;pos:1;subsec:9;imgt:ss;g:52858f64-611c-39ee-9e0d-9957abfabd79;ct:4;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Style;r:4000004720S00004;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;slk:cmmt;itc:1;"><b aria-live="polite" class="js-stream-comment-label ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) Lh(14px) Py(7px) End(-25px) C($signin_blue) js-stream-comments-button>Start(50%) js-stream-comments-button:h>Start(a)">Comments</b><i class="Icon-Fp2 IconComments Fz(21px) "></i></a>
    </li>
    
    
    <li class="ActionLike D(ib) O(n) Mt(4px) Mstart(-1px) Lh(20px)">
        
        
        <a href="javascript:void(0)" class="js-stream-like-button rapid-noclick-resp rapidnofollow Td(n) O(n) C($c_icon) P(14px) js-stream-button-loggedout" role="button" tabindex="0" data-ylk="slk:like;t4:drwr;elm:btn;elmt:lgn-yh;" data-action-outcome="lgn"><b aria-live="polite" class="ActionTooltip Pos(a) ActionTooltipSignin Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Py(5px) js-stream-like-button>Start(50%) js-stream-like-button:h>Start(a)"><span class='Td-u'>Sign-in</span> to like</b><i class="Icon-Fp2 IconTumblrHeartOutline Fz(21px) "></i></a>
        
        
    </li>
    
    
    <li class="ActionReblog D(ib) O(n) Mt(1px) Lh(20px)">
        <a target="_blank" class="js-stream-reblog-button Td(n) O(n) C($c_icon) P(14px)" role="button" tabindex="0"   data-ylk="cpos:11;cposy:21;bpos:1;pos:1;subsec:9;imgt:ss;g:52858f64-611c-39ee-9e0d-9957abfabd79;ct:4;pkgt:orphan_img;grpt:singlestory;cnt_tpc:Style;r:4000004720S00004;ccode:mega_global_ranking_hlv2_up_based;t4:ctrl;elm:btn;elmt:sh-tu-reblog;itc:0;"><b aria-live="polite" class="ActionTooltip Pos(a) Bd($bdr) Bdc($bdr_darkgrey) Bdrs(3px) Bgc(#fff) Bxsh($menuShadow) End(-40px) Lh(14px) Py(7px) js-stream-reblog-button>Start(50%) js-stream-reblog-button:h>Start(a)">Reblog on Tumblr</b><i class="Icon-Fp2 IconTumblrReblog Fz(21px) "></i></a>
    </li>
    
    
    <li class="D(ib) O(n) stream-share Cur(p) Mstart(1px) Lh(20px)" tabindex="0">
        <i class="Icon-Fp2 IconStreamShare Fz(21px) Px(14px) C($c_icon) js-stream-share-icon"></i>
        <div class="js-stream-share-panel End(-2px) Pos(a) Ta(start) H(0) W(169px) Z(11) Op(0) Ov(h) stream-share:h_Op(1) stream-share:h_H(a)"></div>
    </li>
    
    
</ul>

</div>
</div>
</div>
    
    </li>
    


    
    </ul>
</div>

<img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="16" height="16" class="D(b) Mx(a) My(12px) js-stream-load-more ImageLoader" style="background-image:url('https://s.yimg.com/zz/nn/lib/metro/g/my/anim_loading_sm.gif')" alt="">


 </div> </div> </div>  <div class="App-Ft Row"> </div>            <!-- App close -->
            </div>
                </div>
            </div>
            <div class="Col3" id="Aside" role="complementary" tabindex="-1">
                <div class="Col3-stack" data-sticker-top="75px" >
                                <div id="applet_p_32209491" class=" M-0 js-applet trending Zoom-1  Mb(30px) " data-applet-guid="p_32209491" data-applet-type="trending" data-applet-params="_suid:32209491" data-i13n="auto:true;sec:tc-ts" data-i13n-sec="tc-ts"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div id="mega-trending" class="slingstone-selected"><h2 class="trending-title Cur(p) D(ib) W(50%) Fz(15px) Ell Ov(v) gifts-selected_C($disabledHeading) Trs($trendTrs)" data-category="slingstone">Trending Now</h2><ul class="Pos(r) Mt(10px)">
        <li class="trending-list" data-category="slingstone">
            <ul class="M(0) Mstart(-8px) ua-ie8_W(100%) ua-ie7_W(100%) Op(1) gifts-selected_Op(0) gifts-selected_V(h) Trs($trendTrs)"><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Phife+Dawg&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Phife Dawg"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:1;bpos:1;ccode:trending_search;g:2c99bfdb-3050-30fc-9e55-716530486283;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">1.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Phife Dawg</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Lamar+Odom&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Lamar Odom"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:2;bpos:1;ccode:trending_search;g:1c901be5-3edb-3b06-baba-4ec7e5fff660;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">2.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Lamar Odom</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Ivana+Trump&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Ivana Trump"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:3;bpos:1;ccode:trending_search;g:c0f14d98-b804-35e1-840c-713765ad0e7f;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">3.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Ivana Trump</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Miley+Cyrus&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Miley Cyrus"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:4;bpos:1;ccode:trending_search;g:9e7ef02c-2344-3fe4-b825-e49901d70c5b;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">4.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Miley Cyrus</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  Fl(start) Cl(start) ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Apple+iPad+Pro&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Apple iPad Pro"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:5;bpos:1;ccode:trending_search;g:c53a7ffb-50b3-39da-813b-2644805aa66d;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">5.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Apple iPad Pro</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Diet+plans&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Diet plans"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:6;bpos:1;ccode:trending_search;g:e1f0b2f4-6753-3229-9f3e-d592c62d5344;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">6.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Diet plans</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Nicole+Kidman&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Nicole Kidman"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:7;bpos:1;ccode:trending_search;g:5f1a3993-562c-303f-8e78-6786ba01ddfb;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">7.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Nicole Kidman</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Taylor+Kinney&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Taylor Kinney"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:8;bpos:1;ccode:trending_search;g:715a9d4e-f0ad-3451-912e-ffee685a66a8;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">8.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Taylor Kinney</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Map+quest&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Map quest"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:9;bpos:1;ccode:trending_search;g:3f3cd665-6d09-3efe-b567-a6ba82c42909;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">9.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Map quest</span>
                    </a>
                </li><li class="trending-item Pt(7px) D(ib) Bxz(bb) W(50%)  ua-ie7_Pend(5px)">
                    <a href="https://search.yahoo.com/search?cs=bz&amp;p=Subaru+Impreza+2017&amp;fr=fp-tts-201&amp;fr2=p:fp,m:tn,ct:all,pg:1,stl:crsl,b:201" class="Td(n):h C($link) Fz(13px) Ell D(ib)" title="Subaru Impreza 2017"   data-ylk="rspns:nav;t1:a4;t2:tc-ts;t3:ct;sec:tc-ts;cat:trendin;cpos:10;bpos:1;ccode:trending_search;g:dc0dedf3-1890-33a1-bd1f-8e6f43470e0f;elm:itm;elmt:topic;itc:0;">
                        <span class="D(ib) W(1.3em) Ta(e) C(#000)">10.</span><span class="C($searchBlue):h Fw(b) Mstart(2px)"> Subaru Impreza 2017</span>
                    </a>
                </li></ul>
        </li></ul>
</div>
 </div> </div> </div>            <!-- App close -->
            </div>                            <div id="my-adsFPAD-base">
                    <div id="my-adsFPAD" class="D-n sda-DAPF">
                        <script>
                            rtAdStart = window.performance && window.performance.now && window.performance.now();
                        </script>
                        <div class="Mx-a" id="my-adsFPAD-iframe">
                            <!-- FPAD has no ad contents. Other Ad position will take over this ad --><noscript>
<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136pu4bne(gid%24d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st%241459183093215213,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%241254a5f2l,aid%24cO8G4mKLDz0-,cr%24-1,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=FPAD)"></noscript>
                        </div>
                        <script>
                            rtAdDone = window.performance && window.performance.now && window.performance.now();
                        </script>
                    </div>
                </div>                           <div id="my-adsLREC-base" class="lrec-bgcolor">
                   <div id="my-adsLREC" class="Ta-c Pos-r Z-1 Ht-250 Mb-20">
                        <div id="my-adsLREC-iframe">
                            <noscript>
<style type="text/css">
.CAN_ad .yadslug {
    position: absolute !important; right: 1px; top:1px; display:inline-block
!important; z-index : 999;
    color:#999 !important;text-decoration:none;background:#fff
url('https://secure.footprint.net/yieldmanager/apex/mediastore/adchoice_1.png') no-repeat 100% 0
!important;cursor:hand !important;height:12px !important;padding:0px 14px 0px
1px !important;display:inline-block !important;
}
.CAN_ad .yadslug span {display:none !important;}
.CAN_ad .yadslug:hover {zoom: 1;}
.CAN_ad .yadslug:hover span {display:inline-block !important;color:#999
!important;}
.CAN_ad .yadslug:hover span, .CAN_ad .yadslug:hover {font:11px arial
!important;}
</style>
<div class="CAN_ad" style="display:inline-block;position: relative;">
<a class="yadslug" href="http://info.yahoo.com/relevantads/" target="_blank"><span>AdChoices</span></a><a href="https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3dmc3ZGxiNihnaWQkZDVxWjVEY3lMak5WSF82SVZ2bGQ5QVdxTWpBd01WYjVYZlVSaU5FRSxzdCQxNDU5MTgzMDkzMjE1MjEzLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkZjdfWnpkSy5vWERNZGEwVjVqWWJNZyxsbmckZW4tdXMsY3IkNDQ5NTI2MTU1MSx2JDIuMCxhaWQkakZNSDRtS0xEejAtLGJpJDIyOTc3Nzc1NTEsbW1lJDk2ODI5NDE3MTEyNTY0NzE0NDksciQwLHJkJDE0MXZnbXVndix5b28kMSxhZ3AkMzUwOTEwMjA1MSxhcCRMUkVDKSk/1/*http://r.prizegrab.com/aff_c?offer_id=415&amp;aff_id=1069&amp;aff_sub4=cpc&amp;aff_sub=Yahoo5kCash&amp;aff_sub2=YahooPremium&amp;aff_sub5=YellowGoose" target="_blank"><img src="https://s.yimg.com/gs/apex/mediastore/1e9e6281-e4b7-40ae-9498-e88d3e096171" alt="" title="" width="300" height="250" border="0/"></a><!--QYZ 2297777551,4495261551,;;LREC;2023538075;1-->
</div>
<img width="1" height="1" alt="" src="https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&amp;bs=(136pu4bne(gid%24d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st%241459183093215213,si%244452051,sp%242023538075,pv%241,v%242.0))&amp;t=J_3-D_3&amp;al=(as%2413atf9dln,aid%24jFMH4mKLDz0-,bi%242297777551,agp%243509102051,cr%244495261551,ct%2425,at%24H,eob%24gd1_match_id=-1:ypos=LREC)"></noscript>
                        </div>
                    </div>
                </div>            <div id="applet_p_63794" class=" App_v2  M-0 js-applet weather Zoom-1  Mb(30px) " data-applet-guid="p_63794" data-applet-type="weather" data-applet-params="_suid:63794" data-i13n="auto:true;sec:app-wea" data-i13n-sec="app-wea"> <!-- App open -->
        <div class="App-Hd" data-region="header"> <div class="js-applet-view-container-header"> <div class="StencilRoot">
    
        <a href="https://weather.yahoo.com" class="C(#000) C($m_blue):h Td(n)">
            <h2 class="Fz(15px) Fw(b) Mb(18px) D(ib) Grid-U App-Title js-show-panel">
                <span class="js-city-name">
                    
                        
                            Poitou-Charentes
                        
                    
                </span>
            </h2>
        </a>
    
    
    <div class="Grid-U">
        <div class="LocationPanel Pos(r) Fl(end) Z(3)"></div>
    </div>
    
</div>
 </div> </div>  <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  <div class="D(ib)">
    
    <ul class="P(0) Mt(0) Mend(0) Cl(b) Td(n) Mb(10px) Mstart(-2px) forecasts forecasts-7153327">
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/state/poitou-charentes-7153327/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Today</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png" width="43" height="43" alt=" Scattered Thunderstorms" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">57&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">47&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/state/poitou-charentes-7153327/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Tue</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/mostly_cloudy_day_night.png" width="43" height="43" alt=" Scattered Thunderstorms" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">58&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">44&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) Pend(30px)">
            <a href="https://weather.yahoo.com/fr/state/poitou-charentes-7153327/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Wed</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/mostly_cloudy_day_night.png" width="43" height="43" alt=" Scattered Thunderstorms" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">64&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">47&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
         <li class="Pos(r) Ta(c) D(tbc) ">
            <a href="https://weather.yahoo.com/fr/state/poitou-charentes-7153327/" class="C(#000) Td(n)"   data-ylk="itc:0;elm:itm;elmt:ct;rspns:nav;">
                <h3 class="Fz(13px) Mb(7px)">Thu</h3>
                <img class="D(b) M(a)" src="/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png" width="43" height="43" alt=" Scattered Thunderstorms" />
                <span class="Mstart(6px) D(tbc) Mt(2px)">
                    <b class="Fz(13px) Fw(b) D(tbc) Pend(3px)">52&#xb0;<b class="Hidden">f High</b></b>
                    <b class="Fz(13px) Fw(b) C($temp_low) D(tbc)">45&#xb0;<b class="Hidden">f Low</b></b>
                </span>
            </a>
        </li>
        
    </ul>
    
</div>
 </div> </div> </div>            <!-- App close -->
            </div>            <div id="applet_p_63796" class=" App_v2  M-0 js-applet scores Zoom-1  Mb-20 " data-applet-guid="p_63796" data-applet-type="scores" data-applet-params="_suid:63796" data-i13n="auto:true;sec:app-scor" data-i13n-sec="app-scor"> <!-- App open -->
        <div class="App-Hd" data-region="header"> <div class="js-applet-view-container-header"> <div class='Mb(10px)'>
    <h2 class="D(ib) Va(t) Fz(15px) App-Title">Scoreboard</h2>
    <form class="D(ib) Va(t) Miw(150px) SelectBox ysp-league Pos(r) Fl(end) ua-ie7_D(b)! ua-ie7_Fl(n)!" action="" autocomplete="off">
        <!-- the character here will be plugged before the string in the 'dropdown' -->
        
            <div class='SelectBox-Pick Bd(0) P(0) Ta(end)'><b class='SelectBox-Text Fl(n)! Fw(500)'>
            
            
                Trending
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            </b> <i class='Icon Fl(n)! Va(m)'>&#xe002;</i></div>
            <!-- '.Start(0)' needs to be set on this select box so things work in a LTR context -->
            <select name="league" class='Start(0) js-applet-action' data-plugin='selectbox' data-applet-action="savesettings" data-applet-actioncfg="src:form.ysp-league;showview:main" data-setting="league" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:itm;elmt:cat;itc:1;cat:trending">
                
                    
                        <option value="" selected>Trending</option>
                    
                
                    
                        <option value="my" >My Teams</option>
                    
                
                    
                        <option value="nfl" >NFL</option>
                    
                
                    
                        <option value="mlb" >MLB</option>
                    
                
                    
                        <option value="nba" >NBA</option>
                    
                
                    
                        <option value="nhl" >NHL</option>
                    
                
                    
                        <option value="ncaaf" >NCAAF</option>
                    
                
                    
                        <option value="ncaab" >NCAAB</option>
                    
                
                    
                        
                    
                
                    
                        <option value="soccer.l.mls" >MLS</option>
                    
                
                    
                        <option value="soccer.l.fbgb" >Premier League</option>
                    
                
                    
                        <option value="soccer.l.fbchampions" >Champions League</option>
                    
                
                    
                        <option value="soccer.l.fbes" >La Liga</option>
                    
                
                    
                        <option value="soccer.l.fbde" >Bundesliga</option>
                    
                
                    
                        <option value="soccer.l.fbit" >Serie A</option>
                    
                
                    
                        <option value="soccer.l.fbfr" >Ligue 1</option>
                    
                
            </select>
        
    </form>

    
</div>
 </div> </div>  <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  
<style>
  .LiveUpdate {background-color: #fbdb56 !important;}
</style>
<div class="Pos(r) T(24px) Bdb Bdbw(4px) Bdbs(s) Bdc(#f1f1f5) Top(20px) W(100%)"></div>
<ul class="Pos(r) Fz(13px) SimpleTabs Td(n) Mb(10px)">
    <li class="D(ib) Va(t) ">
        <a class="Mend(20px) Pb(4px) Cur(p) C($dimmed) tab-selected_C(#000)! Fw(500) Td(n) tab-selected_Bdb($selectedTabBd) rapidnofollow" data-range="prev" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:tab;elmt:day;itc:1;cat:trending">
            
            
            
            Yesterday
            
        </a>
    </li>
    <li class="D(ib) Va(t)  tab-selected">
        <a class="Mend(20px) Pb(4px) Cur(p) C($dimmed) Fw(500) Td(n) tab-selected_C(#000)! tab-selected_Bdb($selectedTabBd) rapidnofollow" data-range="curr" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:tab;elmt:day;itc:1;cat:trending">
            
            
            
            Today
            
        </a>
    </li>
    <li class="D(ib) Va(t) ">
        <a class="Mend(20px) Pb(4px) Cur(p) C($dimmed) tab-selected_C(#000)! Fw(500) Td(n) tab-selected_Bdb($selectedTabBd) rapidnofollow" data-range="next" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:tab;elmt:day;itc:1;cat:trending">
            
            
            
            Tomorrow
            
        </a>
    </li>
</ul>

<div class="Mb(8px)">

    
    <div data-model="TDScoresModel@mlb.g.360328123" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/mlb/minnesota-twins-pittsburgh-pirates-360328123/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:trending">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Minnesota" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/ipt/min_nonWhite_70x70.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Minnesota
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Pittsburgh" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/hH1KFOCft37n5vEYz7WFPQ--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/pit.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Pittsburgh
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            1:05 pm ET
                        </span>
                        <span class="D(ib)">
                            
                                
                                    RTPT
                                
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>


    
    <div data-model="TDScoresModel@mlb.g.360328102" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/mlb/baltimore-orioles-boston-red-sox-360328102/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:trending">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Baltimore" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/hup4V3rUE93lhosKYEfWnA--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/bal.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Baltimore
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Boston" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/udeRnIoJ1umOWq4EBwLh1g--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/bos.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Boston
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            1:05 pm ET
                        </span>
                        <span class="D(ib)">
                            
                                
                                    ESPN
                                
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>


    
    <div data-model="TDScoresModel@mlb.g.360328124" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/mlb/new-york-mets-st-louis-cardinals-360328124/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:trending">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="NY Mets" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/L0TvpYyHNca2hLv.5N1Wuw--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/nym.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    NY Mets
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="St. Louis" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/pCfG0fYWcjONmUUeVNsg7A--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/stl.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    St. Louis
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            1:05 pm ET
                        </span>
                        <span class="D(ib)">
                            
                                
                                    FSMW
                                
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>


    
    <div data-model="TDScoresModel@mlb.g.360328120" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/mlb/miami-marlins-washington-nationals-360328120/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:trending">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Miami" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/nHinqaJ6E4O4WLAw6KVJdg--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/mia.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Miami
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Washington" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/7v74wzyG7loC9TD05mbMHA--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/was.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Washington
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            1:05 pm ET
                        </span>
                        <span class="D(ib)">
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>


    
    <div data-model="TDScoresModel@mlb.g.360328114" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/mlb/philadelphia-phillies-toronto-blue-jays-360328114/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:trending">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Philadelphia" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/eOKlKRA2_9oz1bSP1PGrOQ--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/phi.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Philadelphia
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Toronto" class="ImageLoader" style="background-image:url('https://s.yimg.com/iu/api/res/1.2/jPz_e971v9bjpfhXnBgIhg--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/tor.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Toronto
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            1:07 pm ET
                        </span>
                        <span class="D(ib)">
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>


    
    <div data-model="TDScoresModel@mlb.g.360328107" data-tmpl="change:td-applet-scores-atomic-templates-score" data-bindcfg="replace:true;class:LiveUpdate">



    <div class="Cur(p) ysp-game-link Row Bgc(#f1f1f5):h Bgc(#f1f1f5):f" >
    <a class="Td(n)" href="https://sports.yahoo.com/mlb/san-diego-padres-kansas-city-royals-360328107/" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;cat:trending">
    <ul class="Whs(nw) My(7px) C(#000)">
        <li class="D(ib) Fw(n) Whs(nw) Ta(start) W(50%) Va(m) Mend(10px)">
            <div class="Pb(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="San Diego" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/i/us/sp/v/mlb/teams/20131113/84/500x500/sdg_70x70.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    San Diego
                </b>
            </div>
            <div class="Pt(2px) Ov(h)">
                <img src="/sy/os/mit/ape/m/81f43c2/t.gif" width="24" height="24" alt="Kansas City" class="ImageLoader" style="background-image:url('https://s.yimg.com/xe/ipt/kan_nonWhite_70x70.png')">
                
                    <b class="Lh(24px) Mstart(4px)">
                
                    Kansas City
                </b>
            </div>
        </li>


        
            
            
        
        


        
        
            
            
                <li class="D(ib) Va(m)">
                    <div class="Fz(11px)">
                        <span class="D(b)">
                            4:05 pm ET
                        </span>
                        <span class="D(ib)">
                            
                        </span>

                        
                            
                            
                            
                        

                    </div>
                    
                </li>
            
            
            
        
        
    </ul>
    </a>
    </div>


</div>



</div>

 </div> </div> </div>  <div class="App-Ft Row">  <div data-region="footer" class="Fl-start Pos-r Z-1"> <div class="js-applet-view-container-footer"> <a class="D(ib) Mb(28px) Cur(p) C($link) C($m_blue):h C($m_blue):f Fw(500) Td(n)" id="ysp-more-scores" href="" data-ylk="t1:a4;t2:scrbrd;sec:scrbrd;elm:itm;elmt:mr;itc:0;cat:trending;">More scores &raquo;</a>
 </div> </div> </div>            <!-- App close -->
            </div>            <div id="applet_p_50000171" class=" M-0 js-applet activitylist Zoom-1  Mb-20 " data-applet-guid="p_50000171" data-applet-type="activitylist" data-applet-params="_suid:50000171" data-i13n="auto:true;sec:ltst" data-i13n-sec="ltst"> <!-- App open -->
        <div class="App-Bd"> <div class="App-Main" data-region="main"> <div class="js-applet-view-container-main">  
<ul class="Mb(0) P(0) js-activitylist-list">

<li class="js-activitylist-item My(20px)" data-id="141843723149">
    
    <a href="https://www.yahoo.com/news/who-s-really-voting-for-trump---portraits-beyond-the-polls-061622809.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:3544acef-b380-3ea0-a83e-a09de7d9d05a;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/WwiTKBR56noLSg8SOKIC.A--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160325/trumpsupporters.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Politics</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Whoâs really voting for Trump: Portraits beyond the polls</span>
    </span>
    </a>
    
</li>

<li class="js-activitylist-item Mt(20px)" data-id="141821179339">
    
    <a href="http://sports.yahoo.com/blogs/ncaab-the-dagger/stunning-second-half-rally-sends-syracuse-to-unlikely-final-four-003239760.html" class="Td(n):h"   data-ylk="pkgt:2;ct:16;rspns:nav;t1:a4;t2:ltst;t3:post;elm:img;elmt:ct;itc:0;cat:false;g:b707a20c-1175-30f5-b06c-fa6cbcbac9e2;">
    <div class="Ta(c) Pos(r)">
        <img src="/sy/uu/api/res/1.2/iQEj.02RxUrIxsiZkHSnzg--/Zmk9ZmlsbDtoPTQxNDtweW9mZj0wO3E9ODA7dz00MTQ7c209MTthcHBpZD15dGFjaHlvbg--/http://l.yimg.com/dh/ap/default/160327/cuse2.jpg" width="207px" height="207px" class="Bdrs(115px)" alt="">
        
        <div class="orb-sprite C($description) Fz(11px) Pos(a) B(-5px) End(24px) D(tb) orb Bdrs(34px)">
            
        </div>
    </div><span class="D(b) Ta(c) Td(n) M(0) Pt(16px) Pb(23px) Px(50px)">
        <div class="C(#400090) Tt(c) Fw(b) Fz(12px) Mb(1px)">Yahoo Sports</div>
        <span class="C($title) C($m_blue):h Fw(b) Fz(16px) Lh(18px)">Stunning second-half rally sends Syracuse to unlikely Final Four</span>
    </span>
    </a>
    
</li>

</ul>

 </div> </div> </div>            <!-- App close -->
            </div>                           <div id="sticky-lrec2-footer" data-plugin="sticker" data-sticker-top="80px" data-sticker-forceposition="top">
               <div class="lrec-bgcolor">
               <div id="my-adsLREC2" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250" data-autorotate="1" data-autoeventrt="10000" data-config={"pos":"LREC2","id":"LREC2","clean":"my-adsLREC2","dest":"my-adsLREC2-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}} data-lrec3replace="1" data-lrec4enabled="1">
                   <div class='Mx-a Ta-c' id="my-adsLREC2-iframe">
                        
                   </div>
               </div>
                               <div id="my-adsLREC3" class="Ta-c Mt-10 Mb-20 Pos-r Ht-250 D-n" data-config={"pos":"LREC3","id":"LREC3","clean":"my-adsLREC3","dest":"my-adsLREC3-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}}>
                    <div class='Mx-a Ta-c' id="my-adsLREC3-iframe">
                    </div>
               </div>
               <div id="my-adsLREC2-fallback" class="D-n">
                    <a href="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16563095&PluID=0&ord=${CACHEBUSTER}" style="background:url(https://www.yahoo.com/sy/nn/lib/metro/300_250_Human_Touch_mail.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;margin:auto;"></a><img width="1" height="1" alt="" src="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=16563095&PluID=0&ord=${CACHEBUSTER}&rtu=-1" style="display:none">
               </div>
               
               </div>
                               <div class="lrec-bgcolor">
                <div id="my-adsLREC4" class="Ta-c Mt(20px) Mb-20 Pos-r Ht-250" data-config={"pos":"LREC4","id":"LREC4","clean":"my-adsLREC4","dest":"my-adsLREC4-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":1}}>
                    <div class='Mx-a Ta-c' id="my-adsLREC4-iframe">
                    </div>
               </div>
                <div id="my-adsLREC4-fallback" class="Mt(20px) D-n">
                    <a href="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16991760&PluID=0&ord=${CACHEBUSTER}" style="background:url(https://s.yimg.com/nn/lib/metro/DailyFantasy_BN_Baseball_300x250-min.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;margin:auto;"></a><img width="1" height="1" alt="" src="https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=19&mc=imp&pli=16991760&PluID=0&ord=${CACHEBUSTER}&rtu=-1" style="display:none">
               </div>
               </div>
               
                    <div id="Footer" class="Row Pstart-20 Pend-10 Pos-r" role="contentinfo">
                         <ul class="Bdrs(3px) Px(20px) Py(14px) Ta(c)" role="contentinfo" id="fp-footer"><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/legal/us/yahoo/utos/terms/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Terms</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Privacy</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://advertising.yahoo.com/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Advertise</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://info.yahoo.com/privacy/us/yahoo/relevantads.html" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">About our Ads</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://careers.yahoo.com/us" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Careers</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="http://help.yahoo.com/l/us/yahoo/helpcentral/" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Help</a></li><li class="D(ib) mouseover Mend(6px) Lh(22px) "><a href="https://yahoo.uservoice.com/forums/341361-yahoo-home" class="Tt(c) Fz(13px) C($link) Op(.4) Td(n) mouseover:h_Op(1) C($m_blue):h" data-ylk="t1:a4;t2:ft;t3:lst;sec:ft;elm:itm;elmt:link;itc:0;rspns:nav;">Feedback</a></li></ul></div>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>





        
                        <div id="darla-assets-bottom">
                    <script type="text/x-safeframe" id="fc" _ver="2-9-9">{ "positions": [ { "html": "<!-- SpaceID=2023538075 loc=FPAD noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;FPAD;2023538075;2-->", "id": "FPAD", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['cO8G4mKLDz0-']='(as$1254a5f2l,aid$cO8G4mKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$1254a5f2l,aid$cO8G4mKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$1254a5f2l,aid$cO8G4mKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=FPAD)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1.4591830932152E+15", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "0", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": {"pt":"0","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","pvid":"d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<style type=\"text/css\">\n.CAN_ad .yadslug {\n    position: absolute !important; right: 1px; top:1px; display:inline-block\n!important; z-index : 999;\n    color:#999 !important;text-decoration:none;background:#fff\nurl('https://secure.footprint.net/yieldmanager/apex/mediastore/adchoice_1.png') no-repeat 100% 0\n!important;cursor:hand !important;height:12px !important;padding:0px 14px 0px\n1px !important;display:inline-block !important;\n}\n.CAN_ad .yadslug span {display:none !important;}\n.CAN_ad .yadslug:hover {zoom: 1;}\n.CAN_ad .yadslug:hover span {display:inline-block !important;color:#999\n!important;}\n.CAN_ad .yadslug:hover span, .CAN_ad .yadslug:hover {font:11px arial\n!important;}\n</style>    \n<div class=\"CAN_ad\" style=\"display:inline-block;position: relative;\">\n<a class=\"yadslug\"\nhref=\"http://info.yahoo.com/relevantads/\"\ntarget=\"_blank\"><span>AdChoices</span></a><a href=\"https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3dmc3ZGxiNihnaWQkZDVxWjVEY3lMak5WSF82SVZ2bGQ5QVdxTWpBd01WYjVYZlVSaU5FRSxzdCQxNDU5MTgzMDkzMjE1MjEzLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkZjdfWnpkSy5vWERNZGEwVjVqWWJNZyxsbmckZW4tdXMsY3IkNDQ5NTI2MTU1MSx2JDIuMCxhaWQkakZNSDRtS0xEejAtLGJpJDIyOTc3Nzc1NTEsbW1lJDk2ODI5NDE3MTEyNTY0NzE0NDksciQwLHJkJDE0MXZnbXVndix5b28kMSxhZ3AkMzUwOTEwMjA1MSxhcCRMUkVDKSk/1/*http://r.prizegrab.com/aff_c?offer_id=415&aff_id=1069&aff_sub4=cpc&aff_sub=Yahoo5kCash&aff_sub2=YahooPremium&aff_sub5=YellowGoose\" target=\"_blank\"><img src=\"https://s.yimg.com/gs/apex/mediastore/1e9e6281-e4b7-40ae-9498-e88d3e096171\" alt=\"\" title=\"\" width=300 height=250 border=0/></a><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2297777551,4495261551,;;LREC;2023538075;1--></div>", "id": "LREC", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['jFMH4mKLDz0-']='(as$13atf9dln,aid$jFMH4mKLDz0-,bi$2297777551,agp$3509102051,cr$4495261551,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13atf9dln,aid$jFMH4mKLDz0-,bi$2297777551,agp$3509102051,cr$4495261551,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13atf9dln,aid$jFMH4mKLDz0-,bi$2297777551,agp$3509102051,cr$4495261551,ct$25,at$H,eob$gd1_match_id=-1:ypos=LREC)", "impID": "jFMH4mKLDz0-", "supp_ugc": "0", "placementID": "3509102051", "creativeID": "4495261551", "serveTime": "1459183093215213", "behavior": "non_exp", "adID": "9682941711256471449", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "300x250", "bookID": "2297777551", "serveType": "-1", "slotID": "1", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(16k40ib9j(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,srv$1,si$4452051,ct$25,exp$1459190293215213,adv$28978287254,li$3508443051,cr$4495261551,dmn$r.prizegrab.com,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1459190293215\", \"fdb_intl\": \"en-US\" }", "slotData": {"pt":"0","bamt":"10000000000.000000","namt":"0.002000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","adjf":"1.000000","alpha":"0.003176","ffrac":"0.003176","pcpm":"-1.000000","fc":"true","dimpr":3602902.000000,"ecpm":2.000000,"sdate":"1458619200","edate":"1459479600","bimpr":5000000.000000,"pimpr":1397098.000000,"spltp":0.000000,"frp":"false","pvid":"d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- SpaceID=2023538075 loc=MAST noad --><!-- fac-gd2-noad --><!-- gd2-status-2 --><!--QYZ CMS_NONE_SELECTED,,;;MAST;2023538075;2-->", "id": "MAST", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['qLcH4mKLDz0-']='(as$1253csoi0,aid$qLcH4mKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$1253csoi0,aid$qLcH4mKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$1253csoi0,aid$qLcH4mKLDz0-,cr$-1,ct$25,at$H,eob$gd1_match_id=-1:ypos=MAST)", "impID": "", "supp_ugc": "0", "placementID": "-1", "creativeID": "-1", "serveTime": "1459183093215213", "behavior": "non_exp", "adID": "#2", "matchID": "#2", "err": "invalid_space", "facStatus": {}, "hasExternal": 0, "size": "", "bookID": "-1", "serveType": "-1", "slotID": "2", "fdb": "{ \"fdb_url\": \"http:\\/\\/beap-bc.yahoo.com\\/af?bv=1.0.0&bs=(15ir45r6b(gid$jmTVQDk4LjHHbFsHU5jMkgKkMTAuNwAAAACljpkK,st$1402537233026922,srv$1,si$13303551,adv$25941429036,ct$25,li$3239250051,exp$1402544433026922,cr$4154984551,pbid$25372728133,v$1.0))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1402544433026\", \"fdb_intl\": \"en-us\" , \"d\" : \"1\" }", "slotData": {"pt":"0","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","pvid":"d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } },{ "html": "<!-- APT Vendor: Right Media, Format: Standard Graphical -->\n<style>\n	#fc_align{\n		position: static !important;\n		width: 120px !important;\n		margin: auto !important;\n	}\n</style>\n<SCR"+"IPT TYPE=\"text/javascr"+"ipt\" SRC=\"https://na.ads.yahoo.com/yax/banner?ve=1&tt=1&si=251157193&asz=120x60&u=https://www.yahoo.com&gdAdId=xBsI4mKLDz0-&gdUuid=d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE&gdSt=1459183093215213&publisher_blob=${RS}|d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE|2023538075|TXTL|1459183093.163317|2-9-9:ysd:1&pub_redirect=https://beap-bc.yahoo.com/yc/YnY9MS4wLjAmYnM9KDE3aXExNGs0YihnaWQkZDVxWjVEY3lMak5WSF82SVZ2bGQ5QVdxTWpBd01WYjVYZlVSaU5FRSxzdCQxNDU5MTgzMDkzMjE1MjEzLHNpJDQ0NTIwNTEsc3AkMjAyMzUzODA3NSxjdCQyNSx5YngkZjdfWnpkSy5vWERNZGEwVjVqWWJNZyxsbmckZW4tdXMsY3IkNDQ1MTAwOTA1MSx2JDIuMCxhaWQkeEJzSTRtS0xEejAtLGJpJDIyNzUxODgwNTEsbW1lJDk1OTMzMjA3NzYxNzQzNDk0MDgsciQwLHlvbyQxLGFncCQzNDc2NTAyNTUxLGFwJFRYVEwpKQ/2/*&K=1\"></SCR"+"IPT><scr"+"ipt>var url = \"\"; if(url && url.search(\"http\") != -1){document.write('<scr"+"ipt src=\"' + url + '\"><\\/scr"+"ipt>');}</scr"+"ipt><!--QYZ 2275188051,4451009051,;;TXTL;2023538075;1-->", "id": "TXTL", "meta": { "y": { "cscHTML": "<scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_d==null)window.xzq_d=new Object();\nwindow.xzq_d['xBsI4mKLDz0-']='(as$13aovmmfr,aid$xBsI4mKLDz0-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)';\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13aovmmfr,aid$xBsI4mKLDz0-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)\"></noscr"+"ipt>", "cscURI": "https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3&al=(as$13aovmmfr,aid$xBsI4mKLDz0-,bi$2275188051,agp$3476502551,cr$4451009051,ct$25,at$H,eob$gd1_match_id=-1:ypos=TXTL)", "impID": "xBsI4mKLDz0-", "supp_ugc": "0", "placementID": "3476502551", "creativeID": "4451009051", "serveTime": "1459183093215213", "behavior": "non_exp", "adID": "9593320776174349408", "matchID": "999999.999999.999999.999999", "err": "", "facStatus": {}, "hasExternal": 0, "size": "120x45", "bookID": "2275188051", "serveType": "-1", "slotID": "3", "fdb": "{ \"fdb_url\": \"https:\\\/\\\/beap-bc.yahoo.com\\\/af\\\/us?bv=1.0.0&bs=(16033q34v(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,srv$1,si$4452051,ct$25,exp$1459190293215213,adv$26679945979,li$3474892551,cr$4451009051,v$1.0,pbid$20459933223,seid$146750051))&al=(type${type},cmnt${cmnt},subo${subo})&r=10\", \"fdb_on\": \"1\", \"fdb_exp\": \"1459190293215\", \"fdb_intl\": \"en-US\" }", "slotData": {"pt":"3","bamt":"10000000000.000000","namt":"0.000000","isLiveAdPreview":"false","is_ad_feedback":"false","trusted_custom":"false","isCompAds":"false","adjf":"1.000000","alpha":"1.000000","ffrac":"1.000000","pcpm":"-1.000000","fc":"false","ecpm":0.000000,"sdate":"1446744408","edate":"1514782799","bimpr":0.000000,"pimpr":-545014720.000000,"spltp":100.000000,"frp":"false","pvid":"d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE"}, "adc": "{\"label\":\"AdChoices\",\"url\":\"https:\\/\\/info.yahoo.com\\/privacy\\/us\\/yahoo\\/relevantads.html\",\"close\":\"Close\",\"closeAd\":\"Close Ad\",\"showAd\":\"Show ad\",\"collapse\":\"Collapse\",\"fdb\":\"I don't like this ad\",\"code\":\"en-us\"}", "is3rd": "0", "userProvidedData": {} } } } ], "meta": { "y": { "pageEndHTML": "<scr"+"ipt language=javascr"+"ipt>\n(function(){window.xzq_p=function(R){M=R};window.xzq_svr=function(R){J=R};function F(S){var T=document;if(T.xzq_i==null){T.xzq_i=new Array();T.xzq_i.c=0}var R=T.xzq_i;R[++R.c]=new Image();R[R.c].src=S}window.xzq_sr=function(){var S=window;var Y=S.xzq_d;if(Y==null){return }if(J==null){return }var T=J+M;if(T.length>P){C();return }var X=\"\";var U=0;var W=Math.random();var V=(Y.hasOwnProperty!=null);var R;for(R in Y){if(typeof Y[R]==\"string\"){if(V&&!Y.hasOwnProperty(R)){continue}if(T.length+X.length+Y[R].length<=P){X+=Y[R]}else{if(T.length+Y[R].length>P){}else{U++;N(T,X,U,W);X=Y[R]}}}}if(U){U++}N(T,X,U,W);C()};function N(R,U,S,T){if(U.length>0){R+=\"&al=\"}F(R+U+\"&s=\"+S+\"&r=\"+T)}function C(){window.xzq_d=null;M=null;J=null}function K(R){xzq_sr()}function B(R){xzq_sr()}function L(U,V,W){if(W){var R=W.toString();var T=U;var Y=R.match(new RegExp(\"\\\\\\\\(([^\\\\\\\\)]*)\\\\\\\\)\"));Y=(Y[1].length>0?Y[1]:\"e\");T=T.replace(new RegExp(\"\\\\\\\\([^\\\\\\\\)]*\\\\\\\\)\",\"g\"),\"(\"+Y+\")\");if(R.indexOf(T)<0){var X=R.indexOf(\"{\");if(X>0){R=R.substring(X,R.length)}else{return W}R=R.replace(new RegExp(\"([^a-zA-Z0-9$_])this([^a-zA-Z0-9$_])\",\"g\"),\"$1xzq_this$2\");var Z=T+\";var rv = f( \"+Y+\",this);\";var S=\"{var a0 = '\"+Y+\"';var ofb = '\"+escape(R)+\"' ;var f = new Function( a0, 'xzq_this', unescape(ofb));\"+Z+\"return rv;}\";return new Function(Y,S)}else{return W}}return V}window.xzq_eh=function(){if(E||I){this.onload=L(\"xzq_onload(e)\",K,this.onload,0);if(E&&typeof (this.onbeforeunload)!=O){this.onbeforeunload=L(\"xzq_dobeforeunload(e)\",B,this.onbeforeunload,0)}}};window.xzq_s=function(){setTimeout(\"xzq_sr()\",1)};var J=null;var M=null;var Q=navigator.appName;var H=navigator.appVersion;var G=navigator.userAgent;var A=parseInt(H);var D=Q.indexOf(\"Microsoft\");var E=D!=-1&&A>=4;var I=(Q.indexOf(\"Netscape\")!=-1||Q.indexOf(\"Opera\")!=-1)&&A>=4;var O=\"undefined\";var P=2000})();\n</scr"+"ipt><scr"+"ipt language=javascr"+"ipt>\nif(window.xzq_svr)xzq_svr('https://csc.beap.bc.yahoo.com/');\nif(window.xzq_p)xzq_p('yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3');\nif(window.xzq_s)xzq_s();\n</scr"+"ipt><noscr"+"ipt><img width=1 height=1 alt=\"\" src=\"https://csc.beap.bc.yahoo.com/yi?bv=1.0.0&bs=(136pu4bne(gid$d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE,st$1459183093215213,si$4452051,sp$2023538075,pv$1,v$2.0))&t=J_3-D_3\"></noscr"+"ipt><scr"+"ipt>(function(c){var d=\"https://\",a=c&&c.JSON,e=\"ypcdb\",g=document,b;function j(n,q,p,o){var m,r;try{m=new Date();m.setTime(m.getTime()+o*1000);g.cookie=[n,\"=\",encodeURIComponent(q),\"; domain=\",p,\"; path=/; max-age=\",o,\"; expires=\",m.toUTCString()].join(\"\")}catch(r){}}function k(m){return function(){i(m)}}function i(n){var m,o;try{m=new Image();m.onerror=m.onload=function(){m.onerror=m.onload=null;m=null};m.src=n}catch(o){}}function f(o){var p=\"\",n,s,r,q;if(o){try{n=o.match(/^https?:\\/\\/([^\\/\\?]*)(yahoo\\.com|yimg\\.com|flickr\\.com|yahoo\\.net|rivals\\.com)(:\\d+)?([\\/\\?]|$)/);if(n&&n[2]){p=n[2]}n=(n&&n[1])||null;s=n?n.length-1:-1;r=n&&s>=0?n[s]:null;if(r&&r!=\".\"&&r!=\"/\"){p=\"\"}}catch(q){p=\"\"}}return p}function l(B,n,q,m,p){var u,s,t,A,r,F,z,E,C,y,o,D,x,v=1000,w=v;try{b=location}catch(z){b=null}try{if(a){C=a.parse(p)}else{y=new Function(\"return \"+p);C=y()}}catch(z){C=null}if(y){y=null}try{s=b.hostname;t=b.protocol;if(t){t+=\"//\"}}catch(z){s=t=\"\"}if(!s){try{A=g.URL||b.href||\"\";r=A.match(/^((http[s]?)\\:[\\/]+)?([^:\\/\\s]+|[\\:\\dabcdef\\.]+)/i);if(r&&r[1]&&r[3]){t=r[1]||\"\";s=r[3]||\"\"}}catch(z){t=s=\"\"}}if(!s||!C||!t||!q){return}A=g.URL||b.href||\"\";E=f(A);if(!E||g.cookie.indexOf(\"ypcdb=\"+n)>-1){return}if(t===d){q=m}u=0;while(F=q[u++]){o=F.lastIndexOf(\"=\");if(o!=-1){D=F.substr(1+o);x=C[D];if(x){setTimeout(k(t+F+x),w);w+=v}}}u=0;while(F=B[u++]){setTimeout(k(t+F),w);w+=v}setTimeout(function(){j(e,n,E,86400)},w)}function h(){l([],'8df05e4c51f0a00c27d5d2553966f64d',['csync.flickr.com/csync?ver=2.1','csync.yahooapis.com/csync?ver=2.1'],['csync.flickr.com/csync?ver=2.1','csync.yahooapis.com/csync?ver=2.1'],'{\"2.1\":\"&id=23351&value=5n7ihu1osvasx%26o%3d3%26f%3d6t&optout=&timeout=1459183093&sig=11g0d8ktu\"}')}if(c.addEventListener){c.addEventListener(\"load\",h,false)}else{if(c.attachEvent){c.attachEvent(\"onload\",h)}else{c.onload=h}}})(window);\n</scr"+"ipt>", "pos_list": [ "FPAD","LREC","MAST","TXTL" ], "filtered": [  ], "spaceID": "2023538075", "host": "www.yahoo.com", "lookupTime": "82", "k2_uri": "", "fac_rt": "-1", "serveTime":"1459183093215213", "pvid": "d5qZ5DcyLjNVH_6IVvld9AWqMjAwMVb5XfURiNEE", "tID": "darla_prefetch_1459183093214_610041089_1", "npv": "0", "ep": "{\"site-attribute\":\"fdn=1 Y-BUCKET=\\\"201\\\" ctout=320\",\"secure\":true,\"lang\":\"en-US\",\"ref\":\"https:\\/\\/www.yahoo.com\",\"filter\":\"no_expandable;\",\"darlaID\":\"darla_instance_1459183093213_1025680345_0\"}", "pe": "CWZ1bmN0aW9uIGRwZWQoKSB7IAoJaWYod2luZG93Lnh6cV9kPT1udWxsKXdpbmRvdy54enFfZD1uZXcgT2JqZWN0KCk7CndpbmRvdy54enFfZFsnY084RzRtS0xEejAtJ109JyhhcyQxMjU0YTVmMmwsYWlkJGNPOEc0bUtMRHowLSxjciQtMSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1GUEFEKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydqRk1ING1LTER6MC0nXT0nKGFzJDEzYXRmOWRsbixhaWQkakZNSDRtS0xEejAtLGJpJDIyOTc3Nzc1NTEsYWdwJDM1MDkxMDIwNTEsY3IkNDQ5NTI2MTU1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1MUkVDKSc7CglpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWydxTGNING1LTER6MC0nXT0nKGFzJDEyNTNjc29pMCxhaWQkcUxjSDRtS0xEejAtLGNyJC0xLGN0JDI1LGF0JEgsZW9iJGdkMV9tYXRjaF9pZD0tMTp5cG9zPU1BU1QpJztpZih3aW5kb3cueHpxX2Q9PW51bGwpd2luZG93Lnh6cV9kPW5ldyBPYmplY3QoKTsKd2luZG93Lnh6cV9kWyd4QnNJNG1LTER6MC0nXT0nKGFzJDEzYW92bW1mcixhaWQkeEJzSTRtS0xEejAtLGJpJDIyNzUxODgwNTEsYWdwJDM0NzY1MDI1NTEsY3IkNDQ1MTAwOTA1MSxjdCQyNSxhdCRILGVvYiRnZDFfbWF0Y2hfaWQ9LTE6eXBvcz1UWFRMKSc7CgkJIH07CmRwZWQudHJhbnNJRCA9ICJkYXJsYV9wcmVmZXRjaF8xNDU5MTgzMDkzMjE0XzYxMDA0MTA4OV8xIjsKCglmdW5jdGlvbiBkcGVyKCkgeyAKCQppZih3aW5kb3cueHpxX3N2cil4enFfc3ZyKCdodHRwczovL2NzYy5iZWFwLmJjLnlhaG9vLmNvbS8nKTsKaWYod2luZG93Lnh6cV9wKXh6cV9wKCd5aT9idj0xLjAuMCZicz0oMTM2cHU0Ym5lKGdpZCRkNXFaNURjeUxqTlZIXzZJVnZsZDlBV3FNakF3TVZiNVhmVVJpTkVFLHN0JDE0NTkxODMwOTMyMTUyMTMsc2kkNDQ1MjA1MSxzcCQyMDIzNTM4MDc1LHB2JDEsdiQyLjApKSZ0PUpfMy1EXzMnKTsKaWYod2luZG93Lnh6cV9zKXh6cV9zKCk7CgoKCShmdW5jdGlvbihjKXt2YXIgZD0iaHR0cHM6Ly8iLGE9YyYmYy5KU09OLGU9InlwY2RiIixnPWRvY3VtZW50LGI7ZnVuY3Rpb24gaihuLHEscCxvKXt2YXIgbSxyO3RyeXttPW5ldyBEYXRlKCk7bS5zZXRUaW1lKG0uZ2V0VGltZSgpK28qMTAwMCk7Zy5jb29raWU9W24sIj0iLGVuY29kZVVSSUNvbXBvbmVudChxKSwiOyBkb21haW49IixwLCI7IHBhdGg9LzsgbWF4LWFnZT0iLG8sIjsgZXhwaXJlcz0iLG0udG9VVENTdHJpbmcoKV0uam9pbigiIil9Y2F0Y2gocil7fX1mdW5jdGlvbiBrKG0pe3JldHVybiBmdW5jdGlvbigpe2kobSl9fWZ1bmN0aW9uIGkobil7dmFyIG0sbzt0cnl7bT1uZXcgSW1hZ2UoKTttLm9uZXJyb3I9bS5vbmxvYWQ9ZnVuY3Rpb24oKXttLm9uZXJyb3I9bS5vbmxvYWQ9bnVsbDttPW51bGx9O20uc3JjPW59Y2F0Y2gobyl7fX1mdW5jdGlvbiBmKG8pe3ZhciBwPSIiLG4scyxyLHE7aWYobyl7dHJ5e249by5tYXRjaCgvXmh0dHBzPzpcL1wvKFteXC9cP10qKSh5YWhvb1wuY29tfHlpbWdcLmNvbXxmbGlja3JcLmNvbXx5YWhvb1wubmV0fHJpdmFsc1wuY29tKSg6XGQrKT8oW1wvXD9dfCQpLyk7aWYobiYmblsyXSl7cD1uWzJdfW49KG4mJm5bMV0pfHxudWxsO3M9bj9uLmxlbmd0aC0xOi0xO3I9biYmcz49MD9uW3NdOm51bGw7aWYociYmciE9Ii4iJiZyIT0iLyIpe3A9IiJ9fWNhdGNoKHEpe3A9IiJ9fXJldHVybiBwfWZ1bmN0aW9uIGwoQixuLHEsbSxwKXt2YXIgdSxzLHQsQSxyLEYseixFLEMseSxvLEQseCx2PTEwMDAsdz12O3RyeXtiPWxvY2F0aW9ufWNhdGNoKHope2I9bnVsbH10cnl7aWYoYSl7Qz1hLnBhcnNlKHApfWVsc2V7eT1uZXcgRnVuY3Rpb24oInJldHVybiAiK3ApO0M9eSgpfX1jYXRjaCh6KXtDPW51bGx9aWYoeSl7eT1udWxsfXRyeXtzPWIuaG9zdG5hbWU7dD1iLnByb3RvY29sO2lmKHQpe3QrPSIvLyJ9fWNhdGNoKHope3M9dD0iIn1pZighcyl7dHJ5e0E9Zy5VUkx8fGIuaHJlZnx8IiI7cj1BLm1hdGNoKC9eKChodHRwW3NdPylcOltcL10rKT8oW146XC9cc10rfFtcOlxkYWJjZGVmXC5dKykvaSk7aWYociYmclsxXSYmclszXSl7dD1yWzFdfHwiIjtzPXJbM118fCIifX1jYXRjaCh6KXt0PXM9IiJ9fWlmKCFzfHwhQ3x8IXR8fCFxKXtyZXR1cm59QT1nLlVSTHx8Yi5ocmVmfHwiIjtFPWYoQSk7aWYoIUV8fGcuY29va2llLmluZGV4T2YoInlwY2RiPSIrbik+LTEpe3JldHVybn1pZih0PT09ZCl7cT1tfXU9MDt3aGlsZShGPXFbdSsrXSl7bz1GLmxhc3RJbmRleE9mKCI9Iik7aWYobyE9LTEpe0Q9Ri5zdWJzdHIoMStvKTt4PUNbRF07aWYoeCl7c2V0VGltZW91dChrKHQrRit4KSx3KTt3Kz12fX19dT0wO3doaWxlKEY9Qlt1KytdKXtzZXRUaW1lb3V0KGsodCtGKSx3KTt3Kz12fXNldFRpbWVvdXQoZnVuY3Rpb24oKXtqKGUsbixFLDg2NDAwKX0sdyl9ZnVuY3Rpb24gaCgpe2woW10sJzhkZjA1ZTRjNTFmMGEwMGMyN2Q1ZDI1NTM5NjZmNjRkJyxbJ2NzeW5jLmZsaWNrci5jb20vY3N5bmM/dmVyPTIuMScsJ2NzeW5jLnlhaG9vYXBpcy5jb20vY3N5bmM/dmVyPTIuMSddLFsnY3N5bmMuZmxpY2tyLmNvbS9jc3luYz92ZXI9Mi4xJywnY3N5bmMueWFob29hcGlzLmNvbS9jc3luYz92ZXI9Mi4xJ10sJ3siMi4xIjoiJmlkPTIzMzUxJnZhbHVlPTVuN2lodTFvc3Zhc3glMjZvJTNkMyUyNmYlM2Q2dCZvcHRvdXQ9JnRpbWVvdXQ9MTQ1OTE4MzA5MyZzaWc9MTFnMGQ4a3R1In0nKX1pZihjLmFkZEV2ZW50TGlzdGVuZXIpe2MuYWRkRXZlbnRMaXN0ZW5lcigibG9hZCIsaCxmYWxzZSl9ZWxzZXtpZihjLmF0dGFjaEV2ZW50KXtjLmF0dGFjaEV2ZW50KCJvbmxvYWQiLGgpfWVsc2V7Yy5vbmxvYWQ9aH19fSkod2luZG93KTsKCgoJCiB9OwpkcGVyLnRyYW5zSUQgPSJkYXJsYV9wcmVmZXRjaF8xNDU5MTgzMDkzMjE0XzYxMDA0MTA4OV8xIjsKCg==", "pym": "" } } } </script>    <script type="text/javascript">
        var pageloadValidAds = ["TXTL","LREC"];
        var mastRotationEnabled = 0;
        var bucketSAEnabled = true;
        var facCustomTimout = 380;
        var w = window, D = w.DARLA,
            C = {"useYAC":0,"usePE":0,"servicePath":"https:\/\/www.yahoo.com\/sdarla\/php\/fc.php","xservicePath":"","beaconPath":"https:\/\/www.yahoo.com\/sdarla\/php\/b.php","renderPath":"","allowFiF":false,"srenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/html\/r-sf.html","renderFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/html\/r-sf.html","sfbrenderPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/html\/r-sf.html","msgPath":"https:\/\/www.yahoo.com\/sdarla\/2-9-9\/html\/msg.html","cscPath":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/html\/r-csc.html","root":"sdarla","edgeRoot":"http:\/\/l.yimg.com\/rq\/darla\/2-9-9","sedgeRoot":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9","version":"2-9-9","tpbURI":"","hostFile":"https:\/\/s.yimg.com\/rq\/darla\/2-9-9\/js\/g-r-min.js","beaconsDisabled":true,"rotationTimingDisabled":true,"fdb_locale":"What don't you like about this ad?|It's offensive|Something else|Thank you for helping us improve your Yahoo experience|It's not relevant|It's distracting|I don't like this ad|Send|Done|Why do I see ads?|Learn more about your feedback.","positions":{"DEFAULT":{"supports":false},"FPAD":[],"LREC":{"w":300,"h":250},"MAST":[],"TXTL":{"w":120,"h":45}},"lang":"en-US"},
            _adPerfBeaconData,
            _pendingAds = [],
            _adLT = [];
        var safeframeOptinPositions = {"FPAD":true};
                window._navStart = function _navStart () {
            if (window.performance) {
                if (window.performance.clearMeasures) {
                    try {
                        window.performance.clearMeasures();
                    } catch (ex) {
                    }
                }
                if (window.performance.clearMarks) {
                    try {
                        window.performance.clearMarks();
                    } catch (ex) {
                    }
                }
                _perfMark('MODAL_OPEN');
            }
            window._pendingAds = [];
            window._perfBackfillAdBeacon = false;
            window._perfVideoInitBeacon = false;
            window._adPerfBeaconData = {positions:{}};
        };
        window._adCallStart = function _adCallStart () {
            window._perfMark('MODAL_AD_EVENT_START');
        };
        window._isModalOpen = function _isModalOpen () {
            return (document.getElementsByTagName('html')[0].className.match(/Reader-open/) && !document.getElementsByTagName('html')[0].className.match(/Reader-closed/));
        };
        window._perfMark = function _perfMark (name) {
            if (window.performance && window.performance.mark) {
                window.performance.mark(name);
            }
        };
        window._perfBackfillAd = function _perfBackfillAd() {
                for (var i in window._adPerfBeaconData.positions) {
                    if (window._adPerfBeaconData.positions.hasOwnProperty(i) && i.match(/LREC-/)) {
                        window._perfMark('PSTMSG_' + i);
                        window._adPerfBeaconData.positions[i].backfill = true;
                    } else {
                        window._adPerfBeaconData.positions[i].backfill = false;
                    }
                }
        };
        window._backfillVideoStart = function _backfillVideoStart() {
            window._perfMark('PSTMSG_VIDSTART');
        };
        window._perfModalFetchStart = function _perfModalFetchStart() {
            window._perfMark('MODAL_FETCH_START');
        };
        window._perfModalFetchEnd = function _perfModalFetchEnd() {
            window._perfMark('MODAL_FETCH_END');
        };
        window._perfModalVideoInit = function _perfModalVideoInit() {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfVideoInitBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfVideoInitBeacon = true;
                window._perfMark('MODAL_VIDEO_INIT');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                modalVideoInit = Math.round(window.performance.getEntriesByName('MODAL_VIDEO_INIT')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'modal_video_playback_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            modalVideoInit: modalVideoInit
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._backfillVideoPlaybackStart = function _backfillVideoPlaybackStart(isAd) {
            var modalStart,
                videoInit,
                rapidPerfData,
                rapidInstance;
            if (!window._perfBackfillAdBeacon && window._isModalOpen() &&
                window.performance && window.performance.getEntriesByName) {

                window._perfBackfillAdBeacon = true;
                window._perfMark('PSTMSG_PBSTART');
                modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0].startTime;
                videoInit = Math.round(window.performance.getEntriesByName('PSTMSG_VIDSTART')[0].startTime - modalStart);
                playbackStart = Math.round(window.performance.getEntriesByName('PSTMSG_PBSTART')[0].startTime - modalStart);

                rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                rapidPerfData = {
                    perf_usertime: {
                        utm: [{
                            name: 'brightroll_backfill_info',
                            site : window._adPerfBeaconData.property || 'fp',
                            lang: window.Af.context.lang,
                            region: window.Af.context.region,
                            device: window.Af.context.device,
                            rid: window.Af.context.rid,
                            bucket: '201',
                            videoInit: videoInit,
                            videoAd: isAd,
                            playbackStart: playbackStart
                }]}};
                if (rapidInstance) {
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        window._adRenderComplete = function _adRenderComplete() {
            if (window.performance && window.performance.getEntriesByName) {
                var modalStart = window.performance.getEntriesByName('MODAL_OPEN')[0];
                var modalFetchStart = window.performance.getEntriesByName('MODAL_FETCH_START');
                var modalFetchEnd = window.performance.getEntriesByName('MODAL_FETCH_END');
                var rapidCustomData = [];
                var darlaFetchPerf= {};
                var rapidInstance = (YMedia.Af && YMedia.Af.rapid && YMedia.Af.rapid.tracker) ? YMedia.Af.rapid.tracker : null;
                var modalStartTime = modalStart.startTime;

                darlaFetchPerf['name'] = 'darla_info';
                darlaFetchPerf['spaceid'] = window._adPerfBeaconData.spaceId;
                darlaFetchPerf['pvid'] = window._adPerfBeaconData.pvid;
                darlaFetchPerf['site'] = window._adPerfBeaconData.property || 'fp';
                darlaFetchPerf['lang'] = window.Af.context.lang;
                darlaFetchPerf['region'] =  window.Af.context.region;
                darlaFetchPerf['device'] =  window.Af.context.device;
                darlaFetchPerf['rid'] =  window.Af.context.rid;
                darlaFetchPerf['bucket'] = '201';

                if (modalFetchStart && modalFetchStart.length > 0) {
                    darlaFetchPerf['modalFetchStart'] =  Math.round(modalFetchStart[0].startTime -modalStartTime);
                    darlaFetchPerf['modalFetchEnd'] =  Math.round(modalFetchEnd[0].startTime -modalStartTime);
                }
                darlaFetchPerf['eventStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_EVENT_START')[0].startTime -modalStartTime);
                darlaFetchPerf['wsStart'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_START')[0].startTime - modalStartTime);
                darlaFetchPerf['wsEnd'] =  Math.round(window.performance.getEntriesByName('MODAL_AD_WS_END')[0].startTime - modalStartTime);
                rapidCustomData.push(darlaFetchPerf);

                for (var i in window._adPerfBeaconData.positions) {
                    var darlaPositionPerf = {};
                    var posInfo = window._adPerfBeaconData.positions.hasOwnProperty(i) && window._adPerfBeaconData.positions[i];
                    if(posInfo) {
                        darlaPositionPerf['name'] = i + '_info';
                        darlaPositionPerf['valid'] = posInfo.valid ? 1 : 0;
                        if (posInfo.valid) {
                            darlaPositionPerf['renderStart'] =  Math.round(window.performance.getEntriesByName('ADSTART_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['renderEnd'] =  Math.round(window.performance.getEntriesByName('ADEND_' + i)[0].startTime - modalStartTime);
                            darlaPositionPerf['bookId'] = posInfo.bookId;
                            darlaPositionPerf['creativeId'] = posInfo.creativeId;
                            darlaPositionPerf['placementId'] = posInfo.placementId;
                            darlaPositionPerf['size'] = posInfo.size;
                            darlaPositionPerf['backfill'] = posInfo.backfill ? 1 : 0;
                            if (posInfo.backfill) {
                                darlaPositionPerf['pstmsg'] = Math.round(window.performance.getEntriesByName('PSTMSG_' + i)[0].startTime - modalStartTime);
                            }
                        }
                        rapidCustomData.push(darlaPositionPerf);
                    }
                }

                if (rapidInstance) {
                    var customPerfData = {'utm': rapidCustomData};
                    var rapidPerfData = {perf_usertime: customPerfData};
                    rapidInstance.beaconPerformanceData(rapidPerfData);
                }
            }
        };
        if (D && C) {
            C.positions = {"FPAD":{"clean":"my-adsFPAD","dest":"my-adsFPAD-iframe","metaSize":1,"fdb":"true,","supports":{"resize-to":1,"exp-ovr":1,"exp-push":1,"lyr":1}},"LREC":{"pos":"LREC","clean":"my-adsLREC","dest":"my-adsLREC-iframe","metaSize":true,"w":300,"h":250,"fdb":true,"supports":{"exp-ovr":0,"lyr":0}},"MAST":{"pos":"MAST","clean":"my-adsMAST","dest":"my-adsMAST-iframe","fr":"expIfr_exp","rmxp":0,"metaSize":true,"w":970,"h":250,"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"closeBtn":{"mode":2,"useShow":1,"adc":0}},"TXTL":{"pos":"TXTL","id":"TXTL","clean":"my-adsTXTL","dest":"my-adsTXTL-iframe","w":120,"h":170,"fr":"expIfr_exp","rmxp":0,"css":".ad-tl2b {overflow:hidden; text-align:left;} p {margin:0px;} a {color:#020e65;} a:hover {color:#0078ff;} .y-fp-pg-controls {margin-top:5px; margin-bottom:5px;} #tl1_slug {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px; color:#999;} #fc_align a {font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:11px;} a:link {text-decoration:none;}"}};
            C.k2={"res":{"rate":100,"pos":["LREC","MAST","FPAD","LREC2","LREC3","LREC-0","LREC2-0","LREC3-0","MAST-0","LDRB-0","SPL2-0","SPL-0","MON-0"]}};
            C.k2E2ERate=100;
C.k2Rate=100;
C.so=1;

            C.events = {"DEFAULT":{"clw":{"LREC-0":{"blocked_by":"MON-0"},"MON-0":{"blocked_by":"LREC-0"},"MAST-0":{"blocked_by":"LDRB-0,SPL-0"},"SPL-0":{"blocked_by":"LDRB-0,MAST-0"},"LDRB-0":{"blocked_by":"MAST-0,SPL-0"}}}};
            
                    C.onStartRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_START');
            }
        };
                    C.onFinishRequest = function(action_event_name) {
            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('MODAL_AD_WS_END');
            }
        };
                        C.onFinishParse = function(eventName, result) {
                var ps = result.ps(),
                    modalOpen = false,
                    position, posItem, curAd, curEvt,
                    validPositions = {},
                    isMONFetch = false;
                            if (window._isModalOpen && window._isModalOpen()) {
                window._perfMark('AD_PARSE');
                modalOpen = true;
                window._adPerfBeaconData.spaceId = result.spaceID;
                window._adPerfBeaconData.pvid = result.pvid;
                curEvt = DARLA.evtSettings(eventName);
                if (curEvt.property) {
                    window._adPerfBeaconData.property = curEvt.property;
                }
            }
                        function fireBeacon(position, size, pageMode) {
            if (position && size && pageMode) {
                try {
                    var img = new Image();
                    img.src = '/p.gif?beaconType=darla&pos=' + position + '&size=' + size + '&mode=' +pageMode + '&bucket=' + 201 + '&rid=' + "fmuulqdbfinfk";
                } catch (e) {
                    if (console) {
                        console.log('failed to send darla beacon :', e);
                    }
                }
            }
        }

                if (ps && ps.length) {
                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
            for (i = 0, l = ps.length; i < l; i++) {
                position = ps[i];
                posItem = result.item(position);
                if (posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    validPositions[position] = false;
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = false;
                    }
                } else {
                    if (position.indexOf("MON") > -1) {
                        isMONFetch = true;
                    }
                    validPositions[position] = true;
                }
            }
            if (YMedia && YMedia.Af && YMedia.Af.Event && YMedia.Af.Event.fire) {
                YMedia.Af.Event.fire('sidekickrefresh', validPositions);
            }
        }

                    for (i = 0, l = ps.length; i < l; i++) {
                        position = ps[i];
                        posItem = result.item(position);
                                    if (modalOpen) {
                if (posItem.err || posItem.hasErr) {
                    window._adPerfBeaconData.positions[position] = {error: true};
                } else if (posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size + ''=== '1x1')) {
                    window._adPerfBeaconData.positions[position] = {size: '1x1'};
                } else {
                    window._adPerfBeaconData.positions[position] = {valid: true};
                    if (posItem.meta && posItem.meta.y) {
                        window._adPerfBeaconData.positions[position].size = posItem.meta.y.size;
                        window._adPerfBeaconData.positions[position].bookId = posItem.meta.y.bookID;
                        window._adPerfBeaconData.positions[position].creativeId = posItem.meta.y.creativeID;
                        window._adPerfBeaconData.positions[position].placementId = posItem.meta.y.placementID;
                    }
                    window._pendingAds.push(position);
                }
            }
                        if (posItem && posItem.conf && posItem.conf.clean) {
                            curAd = document.getElementById(posItem.conf.clean);
                            if (curAd) {
                                            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                var posName = position.split('-')[0];
                if ((posName === "LDRB" || posName === "MAST" || posName === "SPL" || posName === "SPL2") &&
                   (!posItem.hasErr && posItem.size + '' !== '1x1')) {
                    curAd.className = curAd.className.replace('D-n', 'D-ib');
                    if (posName !== "SPL" && posName !== "SPL2") {
                        curAd.className = curAd.className + " Bdb-Grey-1";
                    }
                }
            }
            if (eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') {
                if (position.match(/^SPL/)) {
                    if (posItem.hasErr || posItem.size + '' === '1x1') {
                        if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                            YMedia.one(curAd).setStyle('padding-top', '0px');
                        }
                    } else if (YMedia && YMedia.one('#' + posItem.conf.clean + '-wrap')) {
                        var splashWrap = YMedia.one('#' + posItem.conf.clean + '-wrap');
                        splashWrap.setStyle('padding-top', (5/12*100)+'%');
                        splashWrap.setStyle('width', '100%');
                        splashWrap.setStyle('height', '0px');
                        splashWrap.addClass('Mt-20 Mb-50');
                    }
                }
            }
            if ((eventName === 'hlAdsAll' || eventName === 'hlAdsCustom') && position.indexOf("LREC") > -1) {
              if (posItem && posItem.conf && posItem.conf.clean) {
                var fallbackDiv = document.getElementById(posItem.conf.clean + "-fallback");
                }
                if ((posItem.hasErr || posItem.size + '' === '1x1' || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1'))  && (!isMONFetch || position !== 'LREC-0')) {
                    var lrecBackfillString = "<a href=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=20&mc=click&pli=16563095&PluID=0&ord=${CACHEBUSTER}\" style=\"background:url(https:\/\/www.yahoo.com\/sy\/nn\/lib\/metro\/300_250_Human_Touch_mail.jpg) 0 0 no-repeat;height:250px;width:300px;display:block;margin:auto;\"><\/a><img width=\"1\" height=\"1\" alt=\"\" src=\"https:\/\/bs.serving-sys.com\/BurstingPipe\/adServer.bs?cn=tf&c=19&mc=imp&pli=16563095&PluID=0&ord=${CACHEBUSTER}&rtu=-1\" style=\"display:none\">";
                    if (fallbackDiv) {
                       fallbackDiv.innerHTML = lrecBackfillString;
                       fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                    }
                    curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    curAd.className = curAd.className.replace(/hl-ad-LREC/, '');
                } else {
                    if (fallbackDiv && fallbackDiv.className.indexOf("D-n") < 0) {
                      fallbackDiv.className += ' D-n';
                    }
                    if (isMONFetch && position === 'LREC-0') {
                        curAd.parentElement.style.height = '600px';
                        var curAd = document.getElementById(posItem.conf.clean);
                        if (curAd.className.indexOf("D-n") < 0  && curAd.className.indexOf("D-ib") >= 0) {
                          curAd.className = curAd.className.replace(/D-ib/, 'D-n');
                        }
                    }
                }
            }
            if (eventName === 'prefetch' && position.indexOf("LREC") > -1) {
                curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
            }
            if (eventName === 'fetch_selective_ad_lrec2' || eventName === 'fetch_selective_ad_lrec3') {
                var fallbackDiv;
                if (position === 'LREC2' || position === 'LREC3') {
                    fallbackDiv = document.getElementById("my-adsLREC2-fallback");
                } else {
                    fallbackDiv = document.getElementById("my-ads" + position + "-fallback");
                }

                if (fallbackDiv) {
                    if (posItem.hasErr || (posItem.meta && posItem.meta.y && posItem.meta.y.size && posItem.meta.y.size+'' === '1x1')) {
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                        curAd.className = curAd.className.replace('Ht-250', '');
                        // hide div if ad fetch failed or is 1x1
                        if (curAd.className.indexOf("D-n") < 0) {
                            curAd.className += ' D-n';
                        }
                        fallbackDiv.className = fallbackDiv.className.replace('D-n', '');
                    } else {
                        if (fallbackDiv.className.indexOf("D-n") < 0) {
                            fallbackDiv.className += ' D-n';
                        }
                        curAd.className = curAd.className.replace('D-n', '');
                    }

                    if (position === 'LREC3') {
                        var lrec2Div = document.getElementById("my-adsLREC2");
                        if (lrec2Div && lrec2Div.className.indexOf("D-n") < 0) {
                            lrec2Div.className += ' D-n';
                        }
                    }
                }
            }


                            }
                        }
                    }
                }
                            if (modalOpen && window._pendingAds.length === 0) {
                window._adRenderComplete();
            }
            };

                        C.onStartPosRender = function(posItem) {
                if (window.performance  && window.performance.now) {
                    var ltime = window.performance.now(),
                        posId = posItem && posItem.pos;
                    _adLT.push(['ADSTART_'+posId, Math.round(ltime)]);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADSTART_' + posId);
                }
            };

                        C.onBeforeStartPosRender = function(posItem) {
                if (posItem && safeframeOptinPositions && safeframeOptinPositions[posItem.pos]) {
                    if (posItem.html && posItem.html.match(/<!--[^>]*sfoptout[^>]*-->/)) {
                        return true;
                    }
                }
            };

                        C.onFinishPosRender = function(posId, reqList, posItem) {
                var curAd = document.getElementById("my-ads"+posId),
                    adIndex,
                    posSize = (posItem && posItem.meta && posItem.meta.value("size", "y"));

                // Get clean div for ad position in case defined
                if (posItem && posItem.conf && posItem.conf.clean) {
                    curAd = document.getElementById(posItem.conf.clean);
                }
                if (curAd) {
                    // Let ad take its original size, remove default height given to ad div
                    curAd.className = curAd.className.replace('Ht-250', '');
                    curAd.className = curAd.className.replace('Ht-MAST', '');

                    if(posSize && posSize != "1x1") {
                        curAd.className = curAd.className.replace('D-n', '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-bgcolor/, '');
                        curAd.parentElement.className = curAd.parentElement.className.replace(/lrec-before-loading/, '');
                    }
                }

                if (window.performance !== undefined && window.performance.now !== undefined) {
                    var whiteListedAds = {"LREC":"my-adsLREC-base","MAST":"my-adsMAST","TL1":"my-adsTL1","TXTL":"my-adsTXTL","LREC-0":"hl-ad-LREC-0","MON-0":"hl-ad-MON-0","MAST-0":"hl-ad-MAST-0","LDRB-0":"hl-ad-LDRB-0","SPL2-0":"hl-ad-SPL2-0","SPL-0":"hl-ad-SPL-0"},
                      ltime = window.performance.now();
                     _adLT.push(['ADEND_'+posId, Math.round(ltime)]);
                    setTimeout(function () {
                        if (window.YAFT !== undefined && window.YAFT.isInitialized() && whiteListedAds[posId]) {
                            // Trigger custom timing for LREC ad position
                            window.YAFT.triggerCustomTiming(whiteListedAds[posId], '', ltime);
                        }
                    },300);
                }
                if (window._isModalOpen && window._isModalOpen()) {
                    window._perfMark('ADEND_' + posId);
                    adIndex = window._pendingAds.indexOf(posId);
                    if (adIndex >= 0) {
                        window._pendingAds.splice(adIndex, 1);
                        if (window._pendingAds.length === 0) {
                            window._adRenderComplete();
                        }
                    }
                }
            };

            C.onBeforePosMsg = function(msg_name, position) {
        // Make these configurable for INTLS
        var maxWidth = 970,
            maxHeight = 600,
            newWidth,
            newHeight,
            origWidth,
            origHeight,
            pos;

        if('MAST' !== position) {
            return;
        }

        if (msg_name === 'resize-to') {
            newWidth = arguments[2];
            newHeight = arguments[3];
        }
        else if (msg_name === 'exp-push' || msg_name === 'exp-ovr') {
            pos = $sf.host.get('MAST'),
            origWidth = pos.conf.w;
            origHeight = pos.conf.h;
            //"exp-ovr" or "exp-push", position id, delta X, delta Y, push (true /false), top increase, left increase, right increase, bottom increase
            newWidth = origWidth + arguments[6] + arguments[7];
            newHeight = origHeight + arguments[5] + arguments[8];
        }
        if(newWidth > maxWidth || newHeight > maxHeight) {
            return true;
        }
    };
                        //call back when the ad is expanded or collapsed
            C.onPosMsg = function (msg_name, data, msg_data)  {
                var visible;
                if(msg_name == "collapse" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdExpanded', '');
                    bodyTag.className += " " +  "mastAdCollapsed";
                }
                if(msg_name == "exp-push" && data == "MAST") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace('mastAdCollapsed', '');
                    bodyTag.className += " " +  "mastAdExpanded";
                }

                /* generic ad expansion logic */
                if(msg_name == "collapse") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className = bodyTag.className.replace(data + "-ad-expanded", '');
                }

                if(msg_name == "exp-ovr") {
                    var bodyTag = document.getElementsByTagName("body")[0];
                    bodyTag.className += " " + data + "-ad-expanded";
                }

                if (msg_name === 'geom-update') {
                    visible = D.render.RenderMgr.get(data).viewedAt > 0;
                    // geom-update event will always be available when Y is available
                    if (YMedia && visible) {
                        YMedia.Global.fire('ads:beacon', {id: data});
                    }
                }
                                if (msg_name === 'cmsg') {
                    var splashConf = DARLA.posSettings(data)
                    if (YMedia && splashConf && splashConf.clean) {
                        var splashNode = document.getElementById(splashConf.clean + '-wrap');
                        if (splashNode) {
                            if (msg_data === 'splash-expand') {
                                YMedia.one(splashNode).setStyle('padding-top', (9/16*100)+'%');
                                if (typeof splashNode.scrollIntoView === 'function') {
                                    splashNode.scrollIntoView();
                                }
                            } else if (msg_data === 'splash-collapse') {
                                YMedia.one(splashNode).setStyle('padding-top', (5/12*100)+'%');
                            }
                        }
                    }
                }
            };

            

            


            if ("OK" == D.config(C)) {
                setTimeout(function() {
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_RSTART', Math.round(ltime)]);
                    }
                    var w = window,
                        d = document,
                        e = d.documentElement,
                        g = d.getElementsByTagName('body')[0],
                        winWidth = w.innerWidth || e.clientWidth || g.clientWidth;
                    if (true && winWidth < 1024 && window.pageloadValidAds && (window.pageloadValidAds.indexOf('TXTL') >= 0)) {
                        var prefetched = D.prefetched();
                        var txtlindex = prefetched.indexOf('TXTL');
                        if (txtlindex >= 0) {
                            delete(prefetched[txtlindex]);
                            for (var i=0; i < prefetched.length; i++) {
                                if (prefetched.hasOwnProperty(i)) {
                                    D.render(prefetched[i]);
                                }
                            }
                        } else {
                            D.render();
                        }
                    } else {
                        D.render();
                    }
                    if (window.performance  && window.performance.now) {
                        var ltime = window.performance.now();
                        _adLT.push(['DARLA_REND', Math.round(ltime)]);
                    }
                }, 2);
            }
        }
    </script>
                </div>
        
        

        
        
        <input type="hidden" id="afhistorystate">
    <!-- bottom -->
    
                    <script type="text/javascript" src="/sy/zz/combo?yui:/3.18.0/yui/yui-min.js&/ss/rapid-3.29.1.js&/os/mit/td/aperollup-min-2aa81d5c_desktop_advance.js"></script>
                    <script type="text/javascript" src="/sy/zz/combo?&&/os/mit/td/td-applet-stream-atomic-2.0.484/r-min.js&/os/mit/td/td-applet-mega-header-1.0.204/r-min.js&/os/mit/td/td-applet-viewer-0.1.2153/r-min.js&/os/mit/td/td-applet-navlinks-atomic-0.0.57/r-min.js&/os/mit/td/td-applet-scores-atomic-1.0.10/r-min.js" async defer></script>                                <script type="text/javascript">                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                        YUI.Env.core.push.apply(YUI.Env.core,["loader-angus","loader-ape-af","loader-ape-applet","loader-ape-location","loader-ape-pipe","loader-ape-social","loader-applet-server","loader-assembler","loader-dust-helpers","loader-finance-streamer","loader-highlander-client","loader-live-event-data","loader-mjata","loader-stencil","loader-td-api","loader-td-dev-info","loader-td-lib-entertainment","loader-td-lib-hovercards","loader-td-lib-livevideo","loader-td-lib-social","loader-td-applet-stream-atomic","loader-td-applet-mega-header","loader-td-applet-viewer","loader-td-applet-navlinks-atomic","loader-td-applet-trending-atomic","loader-td-applet-scores-atomic","loader-td-applet-sidekick","loader-td-applet-related-content","loader-td-applet-mega-comments","loader-td-applet-weather-atomic","loader-td-applet-scores-atomic"]);
YUI.add("loader-ape-af",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-beacon":{group:"ape-af",requires:["json-stringify","querystring-stringify-simple","io-base"]},"af-bootstrap":{group:"ape-af",requires:["af-utils"]},"af-cache":{group:"ape-af",requires:["af-beacon","json","gallery-storage-lite"]},"af-compositeview":{group:"ape-af",requires:["parallel","af-utils"]},"af-comscore":{group:"ape-af",requires:["af-beacon","af-config","querystring-stringify-simple"]},"af-config":{group:"ape-af",requires:["af-utils","cookie"]},"af-content":{group:"ape-af",requires:["af-config","af-dom","af-utils","json-parse","mjata-rest-http","node-pluginhost","querystring-stringify"]},"af-cookie":{group:"ape-af",requires:["cookie","json"]},"af-dom":{group:"ape-af",requires:["node-base","node-core"]},"af-dwelltime":{group:"ape-af",requires:["af-rapid","json-stringify"]},"af-eu-tracking":{group:"ape-af",requires:["af-beacon","media-agof-tracking"]},"af-event":{group:"ape-af",requires:["event-custom"]},"af-history":{group:"ape-af",requires:["json"]},"af-message":{group:"ape-af",langBundles:["strings"],requires:["ape-af-templates-message","af-utils","node-base","selector-css3"]},"af-pageviz":{group:"ape-af",requires:["event-custom"]},"af-poll":{group:"ape-af",requires:["af-pageviz"]},"af-rapid":{group:"ape-af",requires:["af-bootstrap","af-utils","media-rapid-tracking","node-core"]},"af-sync":{group:"ape-af",requires:["af-transport","af-utils"]},"af-trans":{group:"ape-af",requires:["node-base","transition","af-utils"]},"af-transport":{group:"ape-af",requires:["af-beacon","af-config","af-utils","json-stringify","tdapi-remotestore"]},"af-utils":{group:"ape-af",requires:["cookie","intl","oop","querystring-parse-simple"]},"af-viewport-loader":{group:"ape-af",requires:["node","event-base"]},"ape-af-lang-strings":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ar-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_de-de":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_el-gr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ae":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-au":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-gb":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ie":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-in":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-jo":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-my":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-nz":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-ph":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-sg":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_en-za":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ar":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-cl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-co":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-es":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-mx":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-pe":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-us":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_es-ve":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-ca":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_fr-fr":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_id-id":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_it-it":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-be":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_nl-nl":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_pt-br":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_ro-ro":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_sv-se":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_vi-vn":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-hk":{group:"ape-af",requires:["intl"]},"ape-af-lang-strings_zh-hant-tw":{group:"ape-af",requires:["intl"]},"ape-af-templates-message":{group:"ape-af",requires:["template-base","dust"]},"gallery-storage-lite":{group:"ape-af",requires:["event-base","event-custom","event-custom-complex","json","node-base"]},"media-agof-tracking":{group:"ape-af"},"media-rapid-tracking":{group:"ape-af",requires:["event-custom","base","node"]},"media-rmp":{group:"ape-af",requires:["node","io-base","async-queue","get"]},"tdapi-remotestore":{group:"ape-af",requires:["mjata-store","mjata-rest-http","json"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-applet",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-applet":{group:"ape-applet",requires:["af-applet-dom","af-applet-model","af-applet-headerview","af-bootstrap","af-compositeview","af-config","af-utils","array-extras","event-custom","node","stencil-tooltip"]},"af-applet-action":{group:"ape-applet",requires:["af-applet-dom","af-utils","node-event-delegate","node-base"]},"af-applet-dom":{group:"ape-applet",requires:["node-core","af-trans"]},"af-applet-editview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-moreinfo","ape-applet-templates-remove"]},"af-applet-headerview":{group:"ape-applet",requires:["af-applet-view","stencil-selectbox"]},"af-applet-init":{group:"ape-applet",requires:["af-applet","af-beacon","af-bootstrap","af-config","af-utils","event-touch","oop"]},"af-applet-load":{group:"ape-applet",requires:["af-applet-dom","af-message","af-transport","ape-applet-templates-reload"]},"af-applet-mgr":{group:"ape-applet",requires:["af-applet-dom","af-beacon","af-content","af-transport","af-utils"]},"af-applet-model":{group:"ape-applet",langBundles:["strings"],requires:["af-bootstrap","af-config","af-sync","base-build","mjata-json","model"]},"af-applet-savesettings":{group:"ape-applet",requires:["af-dom","af-message"]},"af-applet-settingsview":{group:"ape-applet",requires:["af-applet-view","ape-applet-templates-settingswrap"]},"af-applet-view":{group:"ape-applet",requires:["af-bootstrap","af-utils","base-build","dust","view"]},"af-applet-viewmgr":{group:"ape-applet",requires:["af-applet-dom"]},"af-applets":{group:"ape-applet",requires:["af-applet-action","af-applet-init","af-applet-load","af-applet-mgr","af-applet-savesettings","af-applet-viewmgr","af-dom","af-utils"]},"ape-applet-lang-strings":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ar-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_de-de":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_el-gr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ae":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-au":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-gb":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ie":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-in":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-jo":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-my":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-nz":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-ph":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-sg":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_en-za":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ar":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-cl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-co":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-es":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-mx":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-pe":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-us":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_es-ve":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-ca":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_fr-fr":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_id-id":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_it-it":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-be":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_nl-nl":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_pt-br":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_ro-ro":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_sv-se":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_vi-vn":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-hk":{group:"ape-applet",requires:["intl"]},"ape-applet-lang-strings_zh-hant-tw":{group:"ape-applet",requires:["intl"]},"ape-applet-templates-moreinfo":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-reload":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-remove":{group:"ape-applet",requires:["template-base","dust"]},"ape-applet-templates-settingswrap":{group:"ape-applet",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-location",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-location-detection":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-detection"]},"af-location-panel":{group:"ape-location",langBundles:["strings"],requires:["view","mjata-model-store","af-config","af-message","af-locations","af-utils","ape-location-templates-location-list","ape-location-templates-location-panel","stencil-toggle","stencil-tooltip"]},"af-locations":{group:"ape-location",requires:["af-sync","af-utils","mjata-model-base","mjata-model","mjata-lazy-modellist","mjata-model-store"]},"ape-location-lang-strings":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ar-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_de-de":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_el-gr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ae":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-au":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-gb":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ie":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-in":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-jo":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-my":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-nz":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-ph":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-sg":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_en-za":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ar":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-cl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-co":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-es":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-mx":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-pe":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-us":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_es-ve":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-ca":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_fr-fr":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_id-id":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_it-it":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-be":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_nl-nl":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_pt-br":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_ro-ro":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_sv-se":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_vi-vn":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-hk":{group:"ape-location",requires:["intl"]},"ape-location-lang-strings_zh-hant-tw":{group:"ape-location",requires:["intl"]},"ape-location-templates-location-detection":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-list":{group:"ape-location",requires:["template-base","dust"]},"ape-location-templates-location-panel":{group:"ape-location",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-pipe",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-comet":{group:"ape-pipe",requires:["af-beacon","af-config","comet","event-custom-base","json-parse"]},"af-pipe":{group:"ape-pipe",requires:["af-beacon","af-comet","af-config","af-utils","event-custom-base"]},comet:{group:"ape-pipe"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-ape-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"af-social":{group:"ape-social",requires:[]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-dust-helpers",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{dust:{group:"dust-helpers",requires:["stencil-imageloader","moment","intl-helper"]},"dust-helper-intl":{es:!0,group:"dust-helpers",requires:["intl-messageformat"]},"intl-helper":{group:"dust-helpers",requires:["dust-helper-intl"]},"intl-messageformat":{es:!0,group:"dust-helpers"},moment:{group:"dust-helpers"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-mjata",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mjata-bind-model2dom":{group:"mjata",requires:["mjata-model-store","mjata-template","mjata-util","node-base","node-core"]},"mjata-binder":{group:"mjata",requires:["mjata-bind-model2dom"]},"mjata-json":{group:"mjata"},"mjata-lazy-modellist":{group:"mjata",requires:["lazy-model-list","mjata-model-store"]},"mjata-model":{group:"mjata",requires:["model","mjata-json","mjata-model-store"]},"mjata-model-base":{group:"mjata",requires:["base","mjata-model","mjata-modellist","mjata-model-store"]},"mjata-model-store":{group:"mjata",requires:["event-custom","promise"]},"mjata-modellist":{group:"mjata",requires:["model-list","mjata-model-store"]},"mjata-queue":{group:"mjata"},"mjata-rest-http":{group:"mjata",requires:["json-stringify","io-base","io-xdr"]},"mjata-store":{group:"mjata",requires:["mjata-queue","mjata-util"]},"mjata-template":{group:"mjata",requires:["dust"]},"mjata-util":{group:"mjata"}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-stencil",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{hammer:{group:"stencil"},stencil:{group:"stencil"},"stencil-base":{group:"stencil",requires:["stencil","yui-base","event-base","event-delegate","event-mouseenter","json-parse","dom-base","node-base","node-pluginhost","selector"]},"stencil-bquery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-style","node-base","node-pluginhost"]},"stencil-carousel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-hover"]},"stencil-fx":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","pluginhost","event-custom","selector-css3"]},"stencil-fx-collapse":{group:"stencil",requires:["stencil-fx","node-style"]},"stencil-fx-fade":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-fx-flip":{group:"stencil",requires:["stencil-toggle","stencil-fx"]},"stencil-gallery":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","transition"]},"stencil-imageloader":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-screen","node-style","node-pluginhost","array-extras"]},"stencil-lightbox":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","shim-plugin"]},"stencil-scrollview":{group:"stencil",requires:["node-base","node-style","hammer"]},"stencil-selectbox":{group:"stencil",requires:["stencil-base"]},"stencil-slider":{group:"stencil",requires:["stencil"]},"stencil-source":{group:"stencil",requires:["node","oop","pluginhost","mjata-util"]},"stencil-source-af":{group:"stencil",requires:["node","stencil-source","mjata-model-store","mjata-bind-model2dom","af-applets"]},"stencil-source-dom":{group:"stencil",requires:["stencil-source"]},"stencil-source-rmp":{group:"stencil",requires:["stencil-source","stencil-source-url","media-rmp"]},"stencil-source-simple":{group:"stencil",requires:["stencil-source"]},"stencil-source-url":{group:"stencil",requires:["stencil-source","io-base","io-xdr"]},"stencil-sticker":{group:"stencil",requires:["node-base","node-style","node-screen"]},"stencil-tabpanel":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-pluginhost"]},"stencil-toggle":{group:"stencil",requires:["stencil-base","yui-base","dom-base","node-base","node-style","node-pluginhost","event-focus","event-mouseenter","json-parse"]},"stencil-tooltip":{group:"stencil",requires:["stencil-base","yui-base","dom-base","dom-screen","node-base","node-pluginhost","node-style","stencil-source"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-comments",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-mega-comments-mainview":{group:"td-applet-mega-comments",requires:["af-applet-view","af-transport","af-utils","af-rapid","stencil-tooltip","td-applet-mega-comments-templates-styles"]},"td-applet-mega-comments-templates-activities":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-activity-comments"]},"td-applet-mega-comments-templates-activity-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body","td-applet-mega-comments-templates-activity-replies"]},"td-applet-mega-comments-templates-activity-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-avatar":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-comments":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-comments-body"]},"td-applet-mega-comments-templates-comments-body":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-comments-editor-tools"]},"td-applet-mega-comments-templates-comments-editor-tools":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-abuse":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-comment":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-delete":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-form-reply":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-main":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-form-comment","td-applet-mega-comments-templates-form-reply","td-applet-mega-comments-templates-form-abuse","td-applet-mega-comments-templates-form-delete"]},"td-applet-mega-comments-templates-main-activities":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-activities"]},"td-applet-mega-comments-templates-replies":{group:"td-applet-mega-comments",requires:["template-base","dust","td-applet-mega-comments-templates-avatar","td-applet-mega-comments-templates-replies-body"]},"td-applet-mega-comments-templates-replies-body":{group:"td-applet-mega-comments",requires:["template-base","dust"]},"td-applet-mega-comments-templates-styles":{group:"td-applet-mega-comments",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-mega-header",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"mega-header-plugin-account-switch":{group:"td-applet-mega-header",requires:["node","event-custom","event-move","event-mouseenter","td-applet-mega-header-constants","get","anim","json-parse","escape"]},"mega-header-plugin-autocomplete":{group:"td-applet-mega-header",requires:["node","autocomplete","autocomplete-highlighters","autocomplete-list","td-applet-mega-header-constants"]},"mega-header-plugin-avatar":{group:"td-applet-mega-header",requires:["jsonp","td-applet-mega-header-constants"]},"mega-header-plugin-instant":{group:"td-applet-mega-header",requires:["node","event","af-event","json","jsonp","querystring","td-applet-mega-header-constants"]},"mega-header-plugin-mailpreview":{group:"td-applet-mega-header",requires:["jsonp","af-event","td-applet-mega-header-constants"]},"mega-header-plugin-notifications":{group:"td-applet-mega-header",requires:["af-event","af-cache","td-applet-mega-header-constants"]},"mega-header-plugin-username":{group:"td-applet-mega-header",requires:["jsonp"]},"td-applet-mega-header-constants":{group:"td-applet-mega-header"},"td-applet-mega-header-mainview":{group:"td-applet-mega-header",requires:["af-applet-view","node","td-applet-mega-header-constants","mega-header-plugin-autocomplete","mega-header-plugin-avatar","mega-header-plugin-username","mega-header-plugin-mailpreview","mega-header-plugin-notifications","mega-header-plugin-instant","mega-header-plugin-account-switch"]},"td-applet-mega-header-templates-accountSwitchPanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-autofocus_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-firefoxPromo_script":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-instant_filters":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-logo":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mail":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-mailpreview":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-main":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-topbar","td-applet-mega-header-templates-logo","td-applet-mega-header-templates-profile","td-applet-mega-header-templates-notifications","td-applet-mega-header-templates-mail","td-applet-mega-header-templates-search","td-applet-mega-header-templates-instant_filters"]},"td-applet-mega-header-templates-notificationpanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-notifications":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-profile":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-accountSwitchPanel","td-applet-mega-header-templates-profilePanel"]},"td-applet-mega-header-templates-profilePanel":{group:"td-applet-mega-header",requires:["template-base","dust"]},"td-applet-mega-header-templates-search":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-autofocus_script"]},"td-applet-mega-header-templates-topbar":{group:"td-applet-mega-header",requires:["template-base","dust","td-applet-mega-header-templates-firefoxPromo_script"]},"td-mega-header-model":{group:"td-applet-mega-header",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-navlinks-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-navlinks-atomic-templates-main":{group:"td-applet-navlinks-atomic",requires:["template-base","dust"]},"td-applet-navlinks-mainview":{group:"td-applet-navlinks-atomic",requires:["af-applet-view"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-related-content",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-related-content-model":{group:"td-applet-related-content",requires:["model","af-event"]},"td-applet-related-content-templates-main":{group:"td-applet-related-content",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-scores-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-scores-atomic-templates-footer":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-header":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-header.chiclet":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-header.collegefootball":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-score-mini-cf"]},"td-applet-scores-atomic-templates-heading-headtohead":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-main.chiclet":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-score-chiclet"]},"td-applet-scores-atomic-templates-main.collegefootball":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-main.scorestrip":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-score-mini"]},"td-applet-scores-atomic-templates-main.scorethin":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-score"]},"td-applet-scores-atomic-templates-main.tease":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-meta-mlb":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-score":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-score-chiclet":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-score-mini":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-score-mini-cf":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-settings":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-tab.header":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-tennis-match":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-applet-scores-atomic-templates-tennis.schedule":{group:"td-applet-scores-atomic",requires:["template-base","dust","td-applet-scores-atomic-templates-tennis-match"]},"td-applet-scores-atomic-templates-tennis.tournaments":{group:"td-applet-scores-atomic",requires:["template-base","dust"]},"td-scores-atomic-footerview":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-utils"]},"td-scores-atomic-headerview":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-utils"]},"td-scores-atomic-mainview":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-config","af-poll","mjata-binder","stencil-toggle","td-applet-scores-atomic-templates-score","td-scores-atomic-model"]},"td-scores-atomic-model":{group:"td-applet-scores-atomic",requires:["mjata-model","mjata-model-base"]},"td-scores-atomic-modellist":{group:"td-applet-scores-atomic",requires:["mjata-model-base","mjata-modellist","af-sync","td-scores-atomic-model"]},"td-scores-collegefootball-modellist":{group:"td-applet-scores-atomic",requires:["mjata-model-base","mjata-modellist","af-sync","td-scores-atomic-model"]},"td-tennis-schedule-view":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-config","calendar","datatype-date"]},"td-tennis-tournaments-model":{group:"td-applet-scores-atomic",requires:["model","af-sync"]},"td-tennis-tournaments-view":{group:"td-applet-scores-atomic",requires:["af-applet-view","af-config","td-tennis-tournaments-model"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-sidekick",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-sidekick-templates-item":{group:"td-applet-sidekick",requires:["template-base","dust"]},"td-applet-sidekick-templates-main":{group:"td-applet-sidekick",requires:["template-base","dust","td-applet-sidekick-templates-item"]},"td-sidekick-mainview":{group:"td-applet-sidekick",requires:["af-applet-view","af-config","af-event","stencil"]},"td-sidekick-model":{group:"td-applet-sidekick",requires:["af-sync","base-build","model"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-stream-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"stream-actiondrawer-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-cache","af-event","af-utils","event-outside","stencil-fx","stencil-fx-collapse"]},"stream-needtoknow-anim":{group:"td-applet-stream-atomic",requires:["af-applet-view","af-pageviz"]},"stream-onboard-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","stencil-fx","stencil-fx-collapse"]},"td-applet-comments-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-interest-model-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-sync"]},"td-applet-stream-appletmodel-v2":{group:"td-applet-stream-atomic",requires:["mjata-model-base","af-cookie","af-event","af-utils","af-applet-model","af-beacon","af-poll","td-applet-stream-model-v2","td-applet-stream-items-model-v2"]},"td-applet-stream-atomic-templates-breaking_news":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-debug":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_action":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_desktop":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_flyout":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-drawer_share":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-errormsg":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-ad_dislike":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_dislike_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-default":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_clusters":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable"]},"td-applet-stream-atomic-templates-item-default_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-followable","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-featured_ad":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike"]},"td-applet-stream-atomic-templates-item-featured_ad_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-ad_dislike_v2"]},"td-applet-stream-atomic-templates-item-featured_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-filmstrip":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-followable":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-gs_tile":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-inline_video_v2":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon","td-applet-stream-atomic-templates-item-storyline_images","td-applet-stream-atomic-templates-item-storyline"]},"td-applet-stream-atomic-templates-item-needtoknow_actions":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-play_icon":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-right_menu":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-right_menu_featured"]},"td-applet-stream-atomic-templates-item-right_menu_featured":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-item-play_icon"]},"td-applet-stream-atomic-templates-item-storyline":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_images":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-item-storyline_upsell":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-atomic-templates-items":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-debug"]},"td-applet-stream-atomic-templates-main":{group:"td-applet-stream-atomic",requires:["template-base","dust","td-applet-stream-atomic-templates-breaking_news","td-applet-stream-atomic-templates-errormsg"]},"td-applet-stream-atomic-templates-related":{group:"td-applet-stream-atomic",requires
:["template-base","dust"]},"td-applet-stream-atomic-templates-removeditem":{group:"td-applet-stream-atomic",requires:["template-base","dust"]},"td-applet-stream-baseview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view","node-scroll-info","af-beacon"]},"td-applet-stream-headerview-v2":{group:"td-applet-stream-atomic",requires:["af-applet-view"]},"td-applet-stream-items-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-mainview-v2":{group:"td-applet-stream-atomic",requires:["af-beacon","af-cookie","af-cache","af-event","af-pipe","stencil-imageloader","td-applet-stream-baseview-v2"]},"td-applet-stream-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-onboarding-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-payoff-model-v2":{group:"td-applet-stream-atomic",requires:["model","af-sync"]},"td-applet-stream-related-model-atomic":{group:"td-applet-stream-atomic",requires:["model","af-sync"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-trending-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-trending-atomic-mainview":{group:"td-applet-trending-atomic",requires:["af-applet-view","node"]},"td-applet-trending-atomic-templates-main":{group:"td-applet-trending-atomic",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-viewer",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-ads-model":{group:"td-applet-viewer",requires:["af-sync","base-build","model","af-beacon"]},"td-applet-viewer-templates-ad_fdb_thank_you":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-ads_story":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-cards":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-debug","td-applet-viewer-templates-fallback","td-applet-viewer-templates-footer"]},"td-applet-viewer-templates-carousel":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-content_body":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow","td-applet-viewer-templates-ads_story"]},"td-applet-viewer-templates-content_body_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-mag_slideshow"]},"td-applet-viewer-templates-credit":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-debug":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-drawer_feedback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-fallback":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-footer":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-header":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-share_btns_mega"]},"td-applet-viewer-templates-header_ads":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-lightbox":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel"]},"td-applet-viewer-templates-lightbox_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-carousel","td-applet-viewer-templates-thumbnail_items"]},"td-applet-viewer-templates-mag_slideshow":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-main":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-cards","td-applet-viewer-templates-lightbox"]},"td-applet-viewer-templates-modal_aside_mega":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-modal_lrecs_mega"]},"td-applet-viewer-templates-modal_lrecs_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-reblog":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-share_btns_mega":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-sidekicktv":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-slideshow":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-lightbox_mega","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-story_cover","td-applet-viewer-templates-content_body"]},"td-applet-viewer-templates-story_cover":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-video","td-applet-viewer-templates-header"]},"td-applet-viewer-templates-thumbnail_items":{group:"td-applet-viewer",requires:["template-base","dust"]},"td-applet-viewer-templates-video":{group:"td-applet-viewer",requires:["template-base","dust","td-applet-viewer-templates-header","td-applet-viewer-templates-content_body_mega","td-applet-viewer-templates-modal_aside_mega","td-applet-viewer-templates-header_ads","td-applet-viewer-templates-content_body"]},"td-viewer-ads":{group:"td-applet-viewer",requires:["af-beacon","af-config","af-event"]},"td-viewer-mainview":{group:"td-applet-viewer",requires:["af-applet-view","af-beacon","af-cache","af-config","af-event","stencil","angus-slider","event-tap","td-viewer-ads","td-viewer-slideshow"]},"td-viewer-model":{group:"td-applet-viewer",requires:["af-cache","af-sync","base-build","model","af-beacon"]},"td-viewer-slideshow":{group:"td-applet-viewer",requires:["stencil","angus-slider","event-tap"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-applet-weather-atomic",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-applet-weather-atomic-appletmodel":{group:"td-applet-weather-atomic",requires:["mjata-model-base","af-applet-model","mjata-model-store"]},"td-applet-weather-atomic-headerview":{group:"td-applet-weather-atomic",requires:["af-applet-view","mjata-model-store"]},"td-applet-weather-atomic-liteview":{group:"td-applet-weather-atomic",requires:["td-applet-weather-atomic-mainview"]},"td-applet-weather-atomic-mainview":{group:"td-applet-weather-atomic",requires:["af-applet-view","af-message","stencil-fx","stencil-fx-collapse"]},"td-applet-weather-atomic-model":{group:"td-applet-weather-atomic",requires:["model","af-sync"]},"td-applet-weather-atomic-templates-header":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-header.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-list-lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.chiclet":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.error.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.hovercard":{group:"td-applet-weather-atomic",requires:["template-base","dust"]},"td-applet-weather-atomic-templates-main.lite":{group:"td-applet-weather-atomic",requires:["template-base","dust","td-applet-weather-atomic-templates-list-lite"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-dev-info",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"td-dev-info":{group:"td-dev-info",requires:["overlay","node-core","td-dev-info-templates-perf"]},"td-dev-info-templates-init":{group:"td-dev-info",requires:["template-base","dust"]},"td-dev-info-templates-perf":{group:"td-dev-info",requires:["template-base","dust"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.add("loader-td-lib-social",function(e,t){YUI.Env[e.version].modules=YUI.Env[e.version].modules||{},e.mix(YUI.Env[e.version].modules,{"social-sharing-lib":{group:"td-lib-social",requires:["base","node-base","event-base","event-custom","stencil-lightbox","stencil-tooltip"]},"td-lib-social-templates-mtf":{group:"td-lib-social",requires:["template-base","dust","td-lib-social-templates-mtf_styles"]},"td-lib-social-templates-mtf_styles":{group:"td-lib-social",requires:["template-base","dust"]},"td-social-email-autocomplete":{group:"td-lib-social",requires:["autocomplete","autocomplete-highlighters","io","json","lang","node-base","node-core","yql"]},"td-social-mtf-popup":{group:"td-lib-social",requires:["base","event-base","io","autosuggest-standalone-loader","autosuggest-compose-utils","td-social-email-autocomplete","node-base","gallery-node-tokeninput"]}})},"@VERSION@",{requires:["loader-base"]});
YUI.applyConfig({"groups":{"ape-af":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-af-0.0.327/","root":"os/mit/td/ape-af-0.0.327/"}}});
YUI.applyConfig({"groups":{"ape-applet":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-applet-0.0.207/","root":"os/mit/td/ape-applet-0.0.207/"}}});
YUI.applyConfig({"groups":{"ape-location":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-location-1.0.9/","root":"os/mit/td/ape-location-1.0.9/"}}});
YUI.applyConfig({"groups":{"ape-pipe":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-pipe-0.0.64/","root":"os/mit/td/ape-pipe-0.0.64/"}}});
YUI.applyConfig({"groups":{"ape-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/ape-social-0.0.5/","root":"os/mit/td/ape-social-0.0.5/"}}});
YUI.applyConfig({"groups":{"dust-helpers":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/dust-helpers-0.0.144/","root":"os/mit/td/dust-helpers-0.0.144/"}}});
YUI.applyConfig({"groups":{"mjata":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/mjata-0.4.35/","root":"os/mit/td/mjata-0.4.35/"}}});
YUI.applyConfig({"groups":{"stencil":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/stencil-3.1.0/","root":"os/mit/td/stencil-3.1.0/"}}});
YUI.applyConfig({"groups":{"td-dev-info":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-dev-info-0.0.30/","root":"os/mit/td/td-dev-info-0.0.30/"}}});
YUI.applyConfig({"groups":{"td-lib-social":{"combine":true,"filter":"min","comboSep":"&","comboBase":"https://s.yimg.com/zz/combo?","base":"https://s.yimg.com/os/mit/td/td-lib-social-0.1.207/","root":"os/mit/td/td-lib-social-0.1.207/"}}});
YUI.applyConfig({"modules":{"IntlPolyfill":{"fullpath":"https:\u002F\u002Fs.yimg.com\u002Fzz\u002Fcombo?yui:platform\u002Fintl\u002F0.1.4\u002FIntl.min.js&yui:platform\u002Fintl\u002F0.1.4\u002Flocale-data\u002Fjsonp\u002F{lang}.js","condition":{"name":"IntlPolyfill","trigger":"intl-messageformat","test":function (Y) {
                        return !Y.config.global.Intl;
                    },"when":"before"},"configFn":function (mod) {
                    var lang = 'en-US';
                    if (window.YUI_config && window.YUI_config.lang && window.IntlAvailableLangs && window.IntlAvailableLangs[window.YUI_config.lang]) {
                        lang = window.YUI_config.lang;
                    }
                    mod.fullpath = mod.fullpath.replace('{lang}', lang);
                    return true;
                }}}});
                    });
                });                </script>

<script type="text/javascript">
                YUI().use('node-base', function(Y){
                    Y.on("domready", function(e) {
                                        
                        (function(root) {
            root.YUI_config = root.YUI_config || {};
            root.YUI_config.lang = 'en-US';
        }(this));
            var YMedia = YUI({
                
                bootstrap: true,
                lang: 'en-US',
                comboBase: 'https://s.yimg.com/zz/combo?',
                comboSep: '&',
                root: 'yui:' + YUI.version + '/',
                filter: 'min',
                combine: true,
                maxURLLength: 2000,
                groups: {
                    arcade : {
                        base: 'https://s.yimg.com/nn/',
                        combine: true,
                        comboSep: '&',
                        comboBase: 'https://s.yimg.com/zz/combo?',
                        root: '',
                        modules: {
                                                'type_appscontainer_smartphone': {
                        'requires': ['node','node-event-delegate','anim','transition','event-move','stencil','stencil-scrollview'],
                        'path': '/nn/lib/metro/g/appscontainer/appscontainer_smartphone_0.0.18.js'
                    },
                    'type_customizedbutton': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/customizedbutton/customizedbutton_0.0.9.js'
                    },
                    'type_events_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/events/events_0.0.3.js'
                    },
                    'type_geminiads': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/geminiads/geminiads_0.0.4.js'
                    },
                    'type_myy': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid','af-eu-tracking'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_myy_stub_rapid': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app'],
                        'path': '/nn/lib/metro/g/myy/myy_stub_rapid_0.0.4.js'
                    },
                    'type_myy_mobile': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/myy_mobile_0.0.9.js'
                    },
                    'type_myy_viewer': {
                        'requires': ['highlander-client'],
                        'path': '/nn/lib/metro/g/myy/myy_viewer_0.0.10.js'
                    },
                    'type_advance': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/advance_0.0.4.js'
                    },
                    'type_video_manager': {
                        'requires': ['af-content','af-event','base','event-synthetic','node-core','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/video_manager_0.0.129.js'
                    },
                    'type_video_stage': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/video_stage_0.0.8.js'
                    },
                    'type_yahoodotcom_client': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/yahoodotcom_client_0.0.15.js'
                    },
                    'type_myy_scroller': {
                        'requires': ['node'],
                        'path': '/nn/lib/metro/g/myy/myy_scroller_0.0.8.js'
                    },
                    'type_rapidworker_1_1': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_1_0.0.4.js'
                    },
                    'type_rapidworker_1_2': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/rapidworker_1_2_0.0.3.js'
                    },
                    'type_featurecue': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/featurecue_0.0.14.js'
                    },
                    'type_fp': {
                        'requires': ['stencil-toggle','stencil','stencil-sticker','stencil-lightbox','stencil-bquery','af-applets','app','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/myy_0.0.41.js'
                    },
                    'type_addtomy': {
                        'requires': ['node','event','io','panel','io-base','transition','json-stringify'],
                        'path': '/nn/lib/metro/g/myy/addtomy_0.0.21.js'
                    },
                    'af-applet-basemodel': {
                        'requires': ['af-config','af-sync','af-utils','model'],
                        'path': '/nn/lib/metro/g/myy/af_applet_basemodel_0.0.3.js'
                    },
                    'af-applet-contentmodel': {
                        'requires': ['af-applet-basemodel'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentmodel_0.0.3.js'
                    },
                    'af-applet-baseview': {
                        'requires': ['af-dom','view'],
                        'path': '/nn/lib/metro/g/myy/af_applet_baseview_0.0.3.js'
                    },
                    'af-applet-contentview': {
                        'requires': ['af-applet-baseview','af-trans'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentview_0.0.3.js'
                    },
                    'af-applet-contentsettingsview': {
                        'requires': ['af-applet-contentview','af-utils','ape-applet-templates-settingswrap'],
                        'path': '/nn/lib/metro/g/myy/af_applet_contentsettingsview_0.0.3.js'
                    },
                    'af-applet-dd': {
                        'requires': ['af-applet-dom','af-message','event-custom-base'],
                        'path': '/nn/lib/metro/g/myy/af_applet_dd_0.0.4.js'
                    },
                    'type_abu': {
                        'requires': ['af-event'],
                        'path': '/nn/lib/metro/g/myy/abu_0.0.16.js'
                    },
                    'type_abu_event': {
                        'requires': ['event-synthetic','node-scroll-info'],
                        'path': '/nn/lib/metro/g/myy/abu_event_0.0.2.js'
                    },
                    'type_abu_video': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_0.0.7.js'
                    },
                    'type_abu_video_manager': {
                        'requires': ['af-content','af-event','base','event-custom','type_abu_event','type_abu_video','node-core'],
                        'path': '/nn/lib/metro/g/myy/abu_video_manager_0.0.20.js'
                    },
                    'type_advance_desktop': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop_0.0.8.js'
                    },
                    'type_advance_desktop_viewer': {
                        'requires': ['stencil','stencil-base','stencil-sticker','af-applets','base','af-rapid','highlander-client'],
                        'path': '/nn/lib/metro/g/myy/advance_desktop-viewer_0.0.3.js'
                    },
                    'type_app_declarations': {
                        'requires': ['af-cookie'],
                        'path': '/nn/lib/metro/g/myy/app_declarations_0.0.6.js'
                    },
                    'type_partner_att_enus_foreseealive': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-alive_0.0.3.js'
                    },
                    'type_partner_att_enus_foreseetrigger': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/foresee/en-us/foresee-trigger_0.0.3.js'
                    },
                    'pure_client_darla': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/pure_client_darla_0.0.2.js'
                    },
                    'type_idletimer': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myy/idletimer_0.0.1.js'
                    },
                    'type_myycontentdb': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myycontentdb/myycontentdb_0.0.54.js'
                    },
                    'type_uh_init': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/myyheader/uh_init_0.0.13.js'
                    },
                    'type_myymail_mainview': {
                        'requires': ['af-applet-contentsettingsview','stencil-selectbox','stencil-toggle'],
                        'path': '/nn/lib/metro/g/myymail/myymail-mainview_0.0.23.js'
                    },
                    'type_myymail_appletmodel': {
                        'requires': ['mjata-model-base','af-applet-contentmodel'],
                        'path': '/nn/lib/metro/g/myymail/myymail-appletmodel_0.0.16.js'
                    },
                    'type_myyrss_js': {
                        'requires': ['af-applet-contentmodel','af-applet-contentsettingsview'],
                        'path': '/nn/lib/metro/g/myyrss/myyrss_0.0.13.js'
                    },
                    'type_nux': {
                        'requires': ['node-base','event-base','panel','io-base','json-stringify','af-transport'],
                        'path': '/nn/lib/metro/g/nux/nux_0.0.44.js'
                    },
                    'type_optin': {
                        'requires': ['node','event','io','panel','io-base'],
                        'path': '/nn/lib/metro/g/optin/optin_0.0.33.js'
                    },
                    'type_changelayout': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/pagenav/changelayout_0.0.40.js'
                    },
                    'type_changetheme': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/changetheme_0.0.50.js'
                    },
                    'type_addmodule': {
                        'requires': ['node','event','io','json-parse','json-stringify'],
                        'path': '/nn/lib/metro/g/pagenav/addmodule_0.0.16.js'
                    },
                    'type_addpage': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/addpage_0.0.27.js'
                    },
                    'type_pagenav': {
                        'requires': ['af-transport'],
                        'path': '/nn/lib/metro/g/pagenav/pagenav_0.0.35.js'
                    },
                    'type_reco': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/reco/reco_0.0.10.js'
                    },
                    'type_sda': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sda_0.0.37.js'
                    },
                    'type_sdarotate': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/sda/sdarotate_0.0.20.js'
                    },
                    'type_signoutcta': {
                        'requires': ['node','event'],
                        'path': '/nn/lib/metro/g/signoutcta/signoutcta_0.0.9.js'
                    },
                    'type_windowshade_js': {
                        'requires': [],
                        'path': '/nn/lib/metro/g/windowshade/windowshade_0.0.4.js'
                    }
                        }
                    }
                }
            });

            if (!YMedia.config.patches || !YMedia.config.patches.length) {
                YMedia.config.patches = [
                    function patchLangBundlesRequires(Y, loader) {
                        var getRequires = loader.getRequires;
                        loader.getRequires = function (mod) {
                            var i, j, m, name, mods, loadDefaultBundle,
                                locales = Y.config.lang || [],
                                r = getRequires.apply(this, arguments);
                            // expanding requirements with optional requires
                            if (mod.langBundles && !mod.langBundlesExpanded) {
                                mod.langBundlesExpanded = [];
                                locales = typeof locales === 'string' ? [locales] : locales.concat();
                                for (i = 0; i < mod.langBundles.length; i += 1) {
                                    mods = [];
                                    loadDefaultBundle = false;
                                    name = mod.group + '-lang-' + mod.langBundles[i];
                                    for (j = 0; j < locales.length; j += 1) {
                                        m = this.getModule(name + '_' + locales[j].toLowerCase());
                                        if (m) {
                                            mods.push(m);
                                        } else {
                                            // if one of the requested locales is missing,
                                            // the default lang should be fetched
                                            loadDefaultBundle = true;
                                        }
                                    }
                                    if (!mods.length || loadDefaultBundle) {
                                        // falling back to the default lang bundle when needed
                                        m = this.getModule(name);
                                        if (m) {
                                            mods.push(m);
                                        }
                                    }
                                    // adding requirements for each lang bundle
                                    // (duplications are not a problem since they will be deduped)
                                    for (j = 0; j < mods.length; j += 1) {
                                        mod.langBundlesExpanded = mod.langBundlesExpanded.concat(this.getRequires(mods[j]), [mods[j].name]);
                                    }
                                }
                            }
                            return mod.langBundlesExpanded && mod.langBundlesExpanded.length ?
                                    [].concat(mod.langBundlesExpanded, r) : r;
                        };
                    }
            ];
        }
        for (var i = 0; i < YMedia.config.patches.length; i += 1) {YMedia.config.patches[i](YMedia, YMedia.Env._loader);}

        
                        YUI().use('node-base', function(Y) {
                    Y.Global.fire('ymediaReady', {e: YMedia});
                });

                    });
                });        YUI().use('node-base', function(Y) {
        Y.Global.on('ymediaReady', function(data) {
            YMedia = data.e;
    YMedia.applyConfig({"groups":{"td-applet-mega-header":{"base":"/sy/os/mit/td/td-applet-mega-header-1.0.204/","root":"os/mit/td/td-applet-mega-header-1.0.204/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_30345894"] = {"applet_type":"td-applet-mega-header","views":{"main":{"yui_module":"td-applet-mega-header-mainview","yui_class":"TD.Applet.MegaHeaderMainView","config":{"alphatar":{"enabled":true,"urlPath":"https://s.yimg.com/rz/uh/alphatars/","imgType":".png"},"avatar":{"serverCall":false,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"format":"json","_maxage":900}}},"useAvatar":true,"bucket":"201","followable":{"enabled":true,"uri":"/_td_api"},"hasMailPreview":true,"hasMail":true,"hasNotifications":true,"hasProfile":true,"iSearch":{"isEnabled":false,"instantTrending":false,"frcode":"yfp-t-201","frcodeTrending":"fp-tts-201","spaceid":97162737,"cacheMaxAge":30000,"pageViewDelay":1000,"comscoreDelay":3000,"timeout":2500,"highConfidence":true,"autocomplete":{"additionalParams":{"appid":"is","nresults":4},"max_results":4},"api":{"protocol":"https://","host":"search.yahoo.com","path":"/search?","tmpl":"ISRCHA:A0124"},"filterUrls":{"web":"https://search.yahoo.com/search?p=","images":"https://images.search.yahoo.com/search/images?p=","video":"https://video.search.yahoo.com/search/video?p=","news":"https://news.search.yahoo.com/search?p=","local":"https://search.yahoo.com/local/s?p=","answers":"https://answers.search.yahoo.com/search/search_result?p=","shopping":"https://shopping.search.yahoo.com/search?p="}},"isFallback":false,"loginUrl":"https://login.yahoo.com/config/login?.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","logoutUrl":"https://login.yahoo.com/config/login?logout=1&.direct=2&.src=fpctx&.intl=us&.lang=en-US&.done=https%3A%2F%2Fwww.yahoo.com","mail":{"compose":{"url":"https://mrd.mail.yahoo.com/compose"},"count":{"api":{"protocol":"https","host":"mg.mail.yahoo.com","path":"/mailservices/v1/newmailcount","query":{"appid":"UnivHeader"}},"refreshInterval":120,"maxCountDisplay":99},"preview":{"prefetch":true,"api":{"protocol":"https","host":"ucs.query.yahoo.com","path":"/v1/console/yql","query":{"q":"select messageInfo.receivedDate, messageInfo.mid, messageInfo.flags.isRead, messageInfo.from.name, messageInfo.subject from ymail.messages where numMid=\"3\" limit 6","format":"json"}},"urls":{"message":"https://mrd.mail.yahoo.com/msg?fid=Inbox&src=hp&mid="}},"url":"https://mail.yahoo.com/"},"miniheader":true,"miniheaderYPos":0,"notifications":{"inlineReader":true,"maxDisplay":0,"maxUpsellsDisplay":10,"count":{"api":{"uri":"/_td_api","requestType":"clustersUpdatesCount"},"refreshInterval":120,"maxCountDisplay":99},"list":{"api":{"uri":"/_td_api","requestType":"clustersList","image_tag":"pc:size=square,pc:size=small_fixed"}}},"search":{"action":"https://search.yahoo.com/search","assistYlc":";_ylc=X3oDMTFiaHBhMnJmBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2hhc3Q-","ylc":";_ylc=X3oDMTFiN25laTRvBF9TAzIwMjM1MzgwNzUEaXRjAzEEc2VjA3NyY2hfcWEEc2xrA3NyY2h3ZWI-","queries":[{"name":"fr","value":"yfp-t-201"},{"name":"fp","value":"1"},{"name":"toggle","value":"1"},{"name":"cop","value":"mss"},{"name":"ei","value":"UTF-8"}],"autocomplete":{"ghostEnabled":false,"host":"https://search.yahoo.com/sugg/gossip/gossip-us-ura/","crumbKey":"gossip","theme":{"highlight":{"color":"black","background":"#c6d7ff"}},"plugin":{"minQueryLength":3,"resultHighlighter":"phraseMatch"}}},"searchFormGlow":true,"searchMiniHeader":false,"accountSwitchData":{"enabled":""}}}},"templates":{"main":{"yui_module":"td-applet-mega-header-templates-main","template_name":"td-applet-mega-header-templates-main"},"mailpreview":{"yui_module":"td-applet-mega-header-templates-mailpreview","template_name":"td-applet-mega-header-templates-mailpreview"},"accountSwitchPanel":{"yui_module":"td-applet-mega-header-templates-accountSwitchPanel","template_name":"td-applet-mega-header-templates-accountSwitchPanel"},"notificationpanel":{"yui_module":"td-applet-mega-header-templates-notificationpanel","template_name":"td-applet-mega-header-templates-notificationpanel"},"topbar":{"yui_module":"td-applet-mega-header-templates-topbar","template_name":"td-applet-mega-header-templates-topbar"}},"i18n":{"ACCOUNT_INFO":"Account Info","ACCOUNT_SWITCH_WELCOME_1":"Easily switch between multiple Yahoo accounts using the new","ACCOUNT_SWITCH_WELCOME_2":"Click \"Add account\" below to get started!","ACCOUNT_SWITCH_COOKIE_ERR":"It looks like you switched accounts. Refresh the browser to view your personalized page.","ACCOUNT_SWITCH_CRUMB_ERR":"Your account data may be out of sync.<br>Refresh the page to see your accounts.","ACCOUNT_SWITCH_ACCOUNT_MANAGER":"Account Manager","ADD_ACCOUNT":"Add account","ADD_MANAGE_ACCOUNT":"Add or manage accounts","ANSWERS":"Answers","CLEAR_SEARCH":"Clear search query","COMPOSE":"Compose","COMPOSE_CAPS":"COMPOSE","CORPMAIL":"Corp Mail","DEVELOPING_NOW":"Developing Now","ENTER_KEY":"Enter","FIREFOX_PROMO_TEXT":"Install the new Firefox","FOLLOW":"Follow","FOLLOWING":"Following","FROM":"From","GO_TO_MAIL":"Go To Mail","GO_TO_MAIL_CAPS":"GO TO MAIL","HAVE_NO_NEW_MESSAGES":"You have no new messages.","HAVE_NO_UPDATES":"Check back later for updates on stories you are following.","HOME":"Home","IMAGES":"Images","LOADING_MAIL":"Loading Mail Preview","LOADING_UPDATES":"Loading Updates","LOCAL":"Local","MAIL":"Mail","MAIL_CAPS":"MAIL","MENU":"Menu","NEW":"New","NEWS":"News","NEW_MESSAGE":"new message.","NEW_MESSAGES":"new messages.","NOT_FOLLOWING":"Follow a story and get updates here.","NOTIFICATIONS":"Notifications","PRESS":"Press ","PROFILE":"Profile","REL_DAYS":"{0}d","REL_DAYS_AGO":"{0} days ago","REL_HOURS":"{0}h","REL_HOURS_AGO":"{0} hours ago","REL_MINS":"{0}m","REL_MINS_AGO":"{0} minutes ago","REL_MONTHS":"{0}mo","REL_MONTHS_AGO":"{0} months ago","REL_SECS":"{0}s","REL_SECS_AGO":"{0} seconds ago","REL_WEEKS":"{0}wk","REL_WEEKS_AGO":"{0} weeks ago","REL_YEARS":"{0}yr","REL_YEARS_AGO":"{0} years ago","SEARCH":"Search","SEARCH_WEB":"Search Web","SETTINGS":"Settings","SETTINGS_CAPS":"SETTINGS","SHOPPING":"Shopping","SIGNIN":"Sign in","SIGNIN_CAPS":"SIGN IN","SIGNOUT":"Sign Out","SIGNOUT_ALL":"Sign out all","SIGNOUT_CAPS":"SIGN OUT","START_TYPING":"Start typing...","SUBJECT":"Subject","TO_SAVE_FOLLOWS":" to save and get updates.","TO_SEARCH":" to search.","TO_VIEW_NOTIF":" to view your notifications","TO_VIEW_MAIL":" to view your mail","UNABLE_TO_PREVIEW_MAIL":"We are unable to preview your mail.","UNABLE_TO_FETCH_UPDATES":"Check back later for updates on stories you are following.","UNFOLLOW":"Unfollow","VIDEO":"Video","WEB":"Web","WELCOME_BACK":"Welcome back","YOU_HAVE":"You have","SKIP_ASIDE":"Skip to Related content","SKIP_MAIN":"Skip to Main content","SKIP_NAV":"Skip to Navigation"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-viewer":{"base":"/sy/os/mit/td/td-applet-viewer-0.1.2153/","root":"os/mit/td/td-applet-viewer-0.1.2153/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000101"] = {"applet_type":"td-applet-viewer","models":{"viewer":{"yui_module":"td-viewer-model","yui_class":"TD.Viewer.Model","config":{"dataFetch":{"NUM_PREFETCH_ITEMS":20,"NUM_RENDER_ITEMS":1,"NUM_BATCH_PHOTOS":25,"STALE_DATA_FETCH":1800},"cache":{"max-age":{"SLIDESHOW":300,"STORY":600,"VIDEO":3600},"stale-while-revalidate":{"SLIDESHOW":43200,"STORY":43200,"VIDEO":43200}},"uiConfig":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false},"config":{"ui.enableMyComments":true}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"embeddedIframes":{"iframeEmbedHost":"https://s.yimg.com","iframeEmbedMaxPolls":5,"iframeEmbedUrl":"https://s.yimg.com/os/yc/html/embed-iframe-min.7b3468ff.html","instagram":{"className":"instagram-media","name":"instagram","scriptSrc":"//platform.instagram.com/en_US/embeds.js"},"twitter":{"className":"twitter-tweet","name":"tweet","scriptSrc":"//platform.twitter.com/widgets.js"}},"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableMobileLREC":false,"enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalExtraLargeAds":true,"enableModalLRECBackfill":true,"enableModalOmnitureBeacon":true,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableModalVideoDocking":false,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","mobileAdPos":"LREC-9,LREC2-9","mobileAdPosNonHosted":"LREC-9","mobileAdPosOne":"hl-ad-LREC-9","mobileAdPosTwo":"hl-ad-LREC2-9","mobileAdPositionThreshold":250,"modalExtLargeAdPosName":"LDRB2","modalExtLargeAdPosNameAddIndex":false,"modalExtLargeAdPosMaxNum":3,"nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"videoEnrichment":true,"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"enableMyComments":1,"promo":"{\"sports\":{\"image\":\"https://s.yimg.com/dh/ap/sports/mlb/fantasy_baseball_promo_640x70.jpg\",\"title\":\"Sign up for Yahoo Fantasy Baseball\",\"url\":\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16497578&PluID=0&\"}}","lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"},"enableCategories":true,"enableEntities":true,"useUuidPrefix":true,"optimisticPrefetch":false}},"ads":{"yui_module":"td-ads-model","yui_class":"TD.Ads.Model","config":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"}},"applet_model":{"models":["viewer","ads"]}},"views":{"main":{"yui_module":"td-viewer-mainview","yui_class":"TD.Viewer.MainView","config":{"ads":{"enableAdsFeedback":0,"enableVideoSpaceid":1,"curveball":{"sectionId":4250754,"positionThreshold":200,"enabled":false,"count":5,"lowerBound":2,"fromContentAPI":0,"fromCurveballAPI":1,"enableBeaconListenerOnly":true,"enableVisibilityRect":true,"adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html"},"displayAds":{"config":{"LDRP-9":{"w":"768","h":"1"},"LDRB-9":{"w":"728","h":"90","supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LREC-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"LREC2-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"WPS-9":{"w":"320","h":"50"},"WP-9":{"w":"320","h":"50"},"sa":"LREC='300x250;1x1' LREC2='300x250;1x1' LREC3='300x250;1x1' MON='300x600;1x1' megamodal=true","LREC3-9":{"w":"300","h":"250","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"LDRB2-9":{"supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LDRB3-9":{"supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LDRB4-9":{"supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"LDRB5-9":{"supports":{"exp-ovr":1,"exp-push":1},"fdb":true,"z":11},"MAST-9":{"supports":{"exp-ovr":1,"exp-push":1,"resize-to":1},"fdb":false,"closeBtn":{"adc":0,"mode":2,"useShow":1},"metaSize":true,"z":11},"MON-9":{"w":"300","h":"600","fdb":true,"supports":{"exp-ovr":1,"exp-push":1},"z":11},"SPL-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"SPL2-9":{"flex":"both","supports":{"cmsg":1},"fdb":false,"meta":{"type":"stream"},"z":11,"css":".Mags-FontA {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 300;} .Mags-FontA.Size1 {font-size: 13px;}.Mags-FontA.Size2 {font-size: 16px;} .Mags-FontA.Size3 {font-size: 20px;}.Mags-FontA.Size4 {font-size: 22px;} .Mags-FontA.Size5 {font-size: 33px;}.Mags-FontA.Size6 {font-size: 35px;} .Mags-FontA.Size7 {font-size: 58px;}.Mags-FontA.Size8 {font-size: 70px;} .Mags-FontA.Size9 {font-size: 100px;} .Mags-FontB {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;} .Mags-FontB.Size1 {font-size: 18px;} .Mags-FontC {font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight: 400;}.Mags-FontC.Size1 {font-size: 11px;}.Mags-FontC.Size2 {font-size: 14px;} .Mags-FontC.Size3 {font-size: 16px;}.Mags-FontC.Size4 {font-size: 20px;} .Mags-FontC.Size5 {font-size: 30px;}.Mags-FontC.Size6 {font-size: 32px;} .Mags-FontC.Size7 {font-size: 52px;}"},"FSRVY-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1,"bg":1,"lyr":1},"z":11},"FOOT9-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11},"FOOT-9":{"w":"1","h":"1","supports":{"exp-ovr":1,"exp-push":1},"z":11}},"enabled":1,"enabledAuto":false,"enabledDeferRenderAds":false,"forced_modalspaceid":"","enabledMultiAds":false,"positions":{"aboveFold":["LREC-9"],"belowFold":[],"custom":["MAST-9","LDRB-9","SPL-9","SPL2-9"]},"modalposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FSRVY-9,FOOT9-9,MON-9","lrecTimeout":"","enableK2BeaconConf":true,"enableBucketSiteAttribute":true,"offnetSpaceid":"1197762606","offnetposition":"LREC-9","magazineposition":"LREC-9,MAST-9,LDRB-9,SPL-9,FOOT-9,MON-9"},"YVAP":{"accountId":"904","playContext":"default","spaceId":null},"displayAdsFP":{"enabled":0,"pos":"FPAD,LREC","config":[{"id":"FPAD","dest":"my-adsFPAD","pos":"FPAD","clean":"my-adsFPAD-base","h":"250","w":"300"},{"id":"LREC","dest":"my-adsLREC","pos":"LREC","clean":"my-adsLREC-base","h":"250","w":"300"}]},"modalYVAP":{"news":"145","finance":"193","sports":"82","gma":"145","default":"904"},"modalBackfillYVAP":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalPlayContext":{"news":"default","finance":"default","sports":"default","gma":"gma","autos":"bmprpreover","beauty":"bmprpreover","celebrity":"bmprpreover","food":"bmprpreover","health":"bmprpreover","makers":"bmprpreover","movies":"bmprpreover","music":"bmprpreover","parenting":"bmprpreover","politics":"bmprpreover","style":"bmprpreover","tech":"bmprpreover","travel":"bmprpreover","tv":"bmprpreover"},"modalBackfillExpName":"sidekickTVlrecbackfillHTML5","modalBackfillExpType":"right-rail-autoplay","modalBackfillAdTitlePrefix":"UP NEXT: ","modalBackfillYVAPHTML5":{"news":"1115","finance":"1116","sports":"1118","default":"1115"},"modalBackfillExpNameHTML5":"sidekickTVlrecbackfillHTML5"},"category":"","hlView":true,"js":{"videoplayer":"https://yep.video.yahoo.com/js/3/videoplayer-min.js?r=&ypv=","omniture":"https://s.yimg.com/os/mit/media/m/news/omniture-mega-min-78f5e30.js","embed":["click-capture-min","utils"]},"redirectNoContent":true,"rendered":false,"search":{"action":"https://search.yahoo.com/search","frcode":"yfp-t-201-m"},"spaceid":0,"ui":{"adsMeta":true,"alphatar":{"enabled":true,"defaultProfile":"https://s.yimg.com/dh/ap/social/profile/profile_b48.png"},"branding":{"gma":{"src":"https://s.yimg.com/dh/ap/default/151203/GMA-banner.jpg","alt":"Good Morning America"}},"comments":{"enabled":true,"proxy":1,"applet":"td-applet-mega-comments","offnet":{"enabled":false},"config":{"ui.enableMyComments":true}},"comments_allow_supression":true,"comments_preview":true,"comments_writes_enabled":true,"disableSlideshowMON":true,"embeddedIframes":{"iframeEmbedHost":"https://s.yimg.com","iframeEmbedMaxPolls":5,"iframeEmbedUrl":"https://s.yimg.com/os/yc/html/embed-iframe-min.7b3468ff.html","instagram":{"className":"instagram-media","name":"instagram","scriptSrc":"//platform.instagram.com/en_US/embeds.js"},"twitter":{"className":"twitter-tweet","name":"tweet","scriptSrc":"//platform.twitter.com/widgets.js"}},"enableCategories":true,"enableCaptionScroll":false,"enableEntities":true,"enableContinueReading":"","enableMobileLREC":false,"enableModalBackfillViewportDetection":false,"enableModalBackfillVideoAds":true,"enableModalBackfillVideoAdsHtml5":true,"enableModalBackfillVideoAdsIframe":false,"enableModalContentPV":true,"enableModalCustomEvent":true,"enableModalLargeAds":true,"enableModalLargeAdsOffnetwork":true,"enableModalExtraLargeAds":true,"enableModalLRECBackfill":true,"enableModalOmnitureBeacon":true,"enableModalSingleAutoplay":true,"enableModalSponsoredAds":true,"enableModalVideoDocking":false,"enableSlideshowv2":true,"enableSlideshowv2Thumbnails":false,"enableSidekickAdditionalRendering":true,"enableSidekickCommentsRefresh":true,"enableSidekickDynamicHeight":true,"enableSidekickFederationCall":true,"enableLike":true,"enableLongCaption":true,"enableReblog":true,"enableReadMore":true,"enableShare":true,"fixedHeader":false,"forceRedisRead":true,"imageResize":true,"footer":true,"inlineView":true,"lazyLoad":true,"lead":{"minWidth":600,"resizedWidth":300},"licensedFullBody":false,"linkYlk":"itc:0,elm:context_link;","loadAppletDelay":"","loginUrl":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US","mobileAdPos":"LREC-9,LREC2-9","mobileAdPosNonHosted":"LREC-9","mobileAdPosOne":"hl-ad-LREC-9","mobileAdPosTwo":"hl-ad-LREC2-9","mobileAdPositionThreshold":250,"modalExtLargeAdPosName":"LDRB2","modalExtLargeAdPosNameAddIndex":false,"modalExtLargeAdPosMaxNum":3,"nydcContent":true,"optimisticPrefetch":false,"readMoreMinScore":"0.7","renderAdsOnly":false,"resizeSlideshowEnabled":true,"searchBox":false,"share":{"tumblr":{"url":"//www.tumblr.com/widgets/share/tool/preview","post_url":"https://www.tumblr.com/widgets/share/tool?shareSource=legacy&canonicalUrl=&posttype=link&"}},"singleRightRail":false,"slots":{"sidekickdesktop":{"name":"td-applet-sidekick","condition":"all","config":{"disableAppClass":1,"site":"fp","ui.template":"megastream","estHeight":230,"countMargin":5,"ads.display.start":2,"ads.display.frequency":4,"enableAdsFeedback":1,"enableStreamViewCall":1,"mode":"","viewer":"1","condition":"all","maxBatchCount":"35","smartCrop":"1","maxTotalCount":"90","wideImageDesign":"1","uiTemplate":"megacards"}},"readMore":{"name":"td-applet-related-content","condition":"all","config":{"disableAppClass":1,"site":"fp","viewer_enabled":1,"smart_crop":1,"count":"4","image_size":"img:159x159|2|80","min_count":"3","request_count":"20"}},"sidekick":{"config":{"enableAdsFeedback":1},"condition":"none"}},"slotsOrder":[],"sticker":true,"stickerTarget":"#SearchBar-Wrapper-Mini","stream":{"enabled":false,"config":{"header":0,"ui.dislike":0,"ui.filters":0,"ui.hq_ad_template":"featured_ad","ui.like":0,"ui.save":0,"ui.templates.all.gap":1,"ui.templates.all.start":5,"ui.templates.featured.batch_max":3,"ui.templates.featured.gap":2,"ui.viewer_off_network":1,"ui.viewer":1}},"swipe":false,"pcsExclusions":false,"useCapSummary":true,"useUuidPrefix":true,"useSsYcts":true,"lcpBodyEnabled":false,"videoEventsWaitTime":300,"videoPlayer":{"version":"","env":""},"videoEnrichment":true,"yqlResizeEnabled":true,"estSideAdsHeight":610,"estSideNoAdsHeight":60,"mode":"mega-modal","sidekickBatchDelay":500,"sidekickFollowingDelay":3000,"sidekickMaxBatchCount":35,"sidekickMaxTotalCount":90,"slotsSide":["sidekickdesktop"],"experience":"","useContentSite":1,"enable":{"cluster":{"items":1}},"enableMyComments":1,"promo":"{\"sports\":{\"image\":\"https://s.yimg.com/dh/ap/sports/mlb/fantasy_baseball_promo_640x70.jpg\",\"title\":\"Sign up for Yahoo Fantasy Baseball\",\"url\":\"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=16497578&PluID=0&\"}}","lrecBackfill":{"img":"https://s.yimg.com/os/mit/media/m/ads/images/backfill/sports-dailyfantasy-v2-a966da5.jpg","link":"https://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=tf&c=20&mc=click&pli=14816902&PluID=0&ord=${CACHEBUSTER}"},"sidekickFixCount":"5"},"clusterEnabled":false}}},"templates":{"main":{"yui_module":"td-applet-viewer-templates-main","template_name":"td-applet-viewer-templates-main"},"cards":{"yui_module":"td-applet-viewer-templates-cards","template_name":"td-applet-viewer-templates-cards"},"carousel":{"yui_module":"td-applet-viewer-templates-carousel","template_name":"td-applet-viewer-templates-carousel"},"story":{"yui_module":"td-applet-viewer-templates-story","template_name":"td-applet-viewer-templates-story"},"reblog":{"yui_module":"td-applet-viewer-templates-reblog","template_name":"td-applet-viewer-templates-reblog"},"video":{"yui_module":"td-applet-viewer-templates-video","template_name":"td-applet-viewer-templates-video"},"slideshow":{"yui_module":"td-applet-viewer-templates-slideshow","template_name":"td-applet-viewer-templates-slideshow"},"header":{"yui_module":"td-applet-viewer-templates-header","template_name":"td-applet-viewer-templates-header"},"content_body":{"yui_module":"td-applet-viewer-templates-content_body","template_name":"td-applet-viewer-templates-content_body"},"content_body_mega":{"yui_module":"td-applet-viewer-templates-content_body_mega","template_name":"td-applet-viewer-templates-content_body_mega"},"story_cover":{"yui_module":"td-applet-viewer-templates-story_cover","template_name":"td-applet-viewer-templates-story_cover"},"lightbox":{"yui_module":"td-applet-viewer-templates-lightbox","template_name":"td-applet-viewer-templates-lightbox"},"lightbox_mega":{"yui_module":"td-applet-viewer-templates-lightbox_mega","template_name":"td-applet-viewer-templates-lightbox_mega"},"fallback":{"yui_module":"td-applet-viewer-templates-fallback","template_name":"td-applet-viewer-templates-fallback"},"ads_story":{"yui_module":"td-applet-viewer-templates-ads_story","template_name":"td-applet-viewer-templates-ads_story"},"header_ads":{"yui_module":"td-applet-viewer-templates-header_ads","template_name":"td-applet-viewer-templates-header_ads"},"footer":{"yui_module":"td-applet-viewer-templates-footer","template_name":"td-applet-viewer-templates-footer"},"share_btns":{"yui_module":"td-applet-viewer-templates-share_btns","template_name":"td-applet-viewer-templates-share_btns"},"share_btns_mega":{"yui_module":"td-applet-viewer-templates-share_btns_mega","template_name":"td-applet-viewer-templates-share_btns_mega"},"mag_slideshow":{"yui_module":"td-applet-viewer-templates-mag_slideshow","template_name":"td-applet-viewer-templates-mag_slideshow"},"drawer":{"yui_module":"td-applet-viewer-templates-drawer_feedback","template_name":"td-applet-viewer-templates-drawer_feedback"},"thank_you":{"yui_module":"td-applet-viewer-templates-ad_fdb_thank_you","template_name":"td-applet-viewer-templates-ad_fdb_thank_you"},"sidekicktv":{"yui_module":"td-applet-viewer-templates-sidekicktv","template_name":"td-applet-viewer-templates-sidekicktv"}},"i18n":{"PREVIOUS":"Previous","Next":"Next","MORE":"...","LESS":"less","NOCONTENT_FUNNY":"Feeding the engineers.  Will be back soon...","NOCONTENT_READMORE":"Read more on \"{0}\"","OFFNETWORK":"Read more on {0}","ELLIPSIS":"{0} ...","VIDEO_DURATION":"Duration {0}","SPONSORED":"Sponsored","SWIPE_FOR_NEXT":"Swipe for next article","INSTALL_NOW":"Install now","FULL_ARTICLE":"Read Full Article","ARTICLE_SUMMARY":"Read Summary","READ_MORE":"Read More","AD":"Advertisement","SHARE_EMAIL":"Share to Mail","CLOSE":"Close","RELATED_NEWS":"Related news","START_SLIDESHOW":"Start Slideshow","CONTINUE_READING":"Continue Reading","CLICK_FOR_VIDEOS":"Click here to watch the video","CLICK_FOR_PHOTOS":"Click here to view photos","AD_FDB1":"It's offensive to me","AD_FDB2":"I keep seeing this","AD_FDB3":"It's not relevant to me","AD_FDB4":"Something else","AD_FDB_HEADING":"Why don't you like this ad ?","AD_REVIEW":"We'll review and make changes needed.","AD_THANKYOU":"Thank you for your feedback","AD_SUBMIT":"Submit","AD_FDB_ERORR":"Please select option. To help us making your experience better.","AD_FDB_SOMETHING_ELSE":"Tell us, why you don't like this ad?","DONE":"Done","READ_ARTICLE":"Read more","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","REBLOG":"Reblog","REBLOG_TUMBLR":"Reblog on Tumblr","UNDO":"Undo","SIGN_IN_TO_LIKE":"Sign in to like","LIKABLE_ARTICLE_SIGN_IN":"Sign in to see more stories you like.","SIGN_IN_2":"Sign in","READ_FULL_ARTICLE":"Read full article","COMMENTS":"Comments","FACEBOOK":"Share","TWITTER":"Tweet","EMAIL":"Email","CLOSE_VIEWER":"Close this content, you can also use the Escape key at anytime"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};window.ViewerClickCapture||(window.ViewerClickCapture=function(){function a(b,c,d){return b?b.tagName===c&&b.className&&b.className.indexOf(d)>=0?b:a(b.parentNode,c,d):!1}function b(b){var c=a(b.target,"A","js-content-viewer");c&&(d=b,b.preventDefault&&b.preventDefault())}function c(){!e&&f.removeEventListener&&f.removeEventListener("click",b),e=!0}var d,e,f=document.documentElement;return f.addEventListener&&(f.addEventListener("click",b),window.setTimeout(function(){c()},4e3)),{clear:function(){d=null},last:function(){return d},disable:c}}());window.ViewerUtils||!function(){function a(){function a(a){return a&&"object"==typeof a&&"number"==typeof a.length&&g.call(a)===h||!1}function b(a){return"string"==typeof a||a&&"object"==typeof a&&g.call(a)===i||!1}function c(a){return"undefined"==typeof a}function d(a){return null===a}function e(a){return c(a)||d(a)}var f=Object.prototype,g=f.toString,h="[object Array]",i="[object String]";return{isArray:Array.isArray||a,isNull:d,isString:b,isUndefined:c,isVoid:e}}function b(b,c,d){var e=a();if(!b)return d;if(!c)return b;!e.isArray(c)&&e.isString(c)&&(c=c.split("."));for(var f=0,g=c.length;b&&g>f;f++)b=b[c[f]];return e.isVoid(b)?d:b}window.ViewerUtils={getObjValue:b}}();
YMedia.applyConfig({"groups":{"td-applet-navlinks-atomic":{"base":"https://s.yimg.com/os/mit/td/td-applet-navlinks-atomic-0.0.57/","root":"os/mit/td/td-applet-navlinks-atomic-0.0.57/","combine":true,"filter":"min","comboBase":"https://s.yimg.com/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000195"] = {"applet_type":"td-applet-navlinks-atomic","views":{"main":{"yui_module":"td-applet-navlinks-mainview","yui_class":"TD.Applet.NavlinksMainView"}},"i18n":{"TITLE":"navlinks-atomic","TOPICS":"Topics"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-stream-atomic":{"base":"/sy/os/mit/td/td-applet-stream-atomic-2.0.484/","root":"os/mit/td/td-applet-stream-atomic-2.0.484/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000173"] = {"applet_type":"td-applet-stream-atomic","models":{"stream":{"yui_module":"td-applet-stream-model-v2","yui_class":"TD.Applet.StreamModel2","data":{"ccode":"mega_global_ranking_hlv2_up_based","more_items":[{"type":"unknown","id":"aa010681-f4f2-33fc-8d13-413ea873c2e8","instrument":"4000010910S00002","is_eligible":"true"},{"type":"ad","id":"31834384116","title":"Comment assurer l'inassurable ?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=wj3tgKEGIS_zdxOKpE6ppwX4QUPHF7s5djNRF8SufLv1Y4g0gVScXsGlBz26d8wpjW9nQKDkz0g0vk3gVgS.MO4A7h4ukA9yePM14TsT1SJ86Tsw4McO_9qiqeljVw1eJfz28px.KYxfaYRKuBD55XuAqzU0_pZ3rXWlUpbutXcUj0ASDJsLKec3EbZL52TA_yeLy_LHQ3FJQZoIKEvdGJr.e9SdrMwbji7zT2SwYggxiN.IL4rkJu42rQKuW_uepwS8ySFU97tfOlpR_m7NshV_mXMVywJPf8YljEoRlqIuQ6Hxl2Zg5t_l1V4RMWME8FtPanhldfzy56I2N_rbRY_.zzx6JUjnKMDAiTEVmErtAu0_P8jMYBM38zjjjnny.7kl73pX7mZKpJS11ros5HNIFS3Hhfi1zMdiynQeI5RR3P1G.ZQr8zlGdDlSQILfXPgPRmlHaDAyK0aojMYiuQL5VFym.xWBTh42cyoUD5MpMjN7FF_.c1arOidnWYF8LbPFreEt7dmXt5EUdHVJzFbb_Xwrm1voVOQp%26lp=https%3A%2F%2Faxagmdfrfr.solution.weborama.fr%2Ffcgi-bin%2Fdispatch.fcgi%3Fa.A%3Dcl%26a.si%3D2927%26a.te%3D95%26a.ra%3D%5BRANDOM%5D%26g.lu%3D","publisher":"Axa","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=E74oZ2QGIS.bKynxDqpCYkzQI09gt5tdZj4iepBF5qVkpAp4dNRq.7Fl4RObzaRK94vm3x3I8cAliUVzNpQeIHXq4bDhxWtgR8JWYUVe.5MKqjKhhjAs4S.gw15CB9vNdfO0g3sNH4rDRwM3txnP9a9y8v4QsD_oHvHUZOE0F.A3puU__QY7vqytg_oAQhN6G6W6HM7_OjGPU1eCHFhQjupwfs8T9rrBpEMGFyhUH8prneZm_LELuzn4GFJsQKRsGejDyI0g4QlR623Hzlq30178R24iHh6tDidnPnGKFqune0ost6XF70L.b7Vm5756tbmUqP9Xf4oieW8gjaphD5stnsfl53gdQzHI6is9ATSQTJfnZ1j4l4TrgJfnebXdjajnjJIgGmMdDSYtx31vrV_skFFXOzo-&ap=14","imprTrackingUrls":["https://axagmdfrfr.solution.weborama.fr/fcgi-bin/dispatch.fcgi?a.A=im&a.si=2927&a.te=95&a.he=1&a.wi=1&a.hr=p&a.ra=[RANDOM]"],"ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15n6ejst5(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31834384116,dmn$c31834384116,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1355646,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/_j99Ewg4ky4gyLwENQMb0A--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1458725652073-9347.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Il est possible d'assurer toutes sortes de choses, des papilles gustatives aux fusÃ©es ! Comment innover pour toujours mieux protÃ©ger ?","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31834384116","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":0},{"type":"unknown","id":"40874d25-f034-370d-816a-af6c16deca93","instrument":"4000023910S00008","is_eligible":"false"},{"type":"unknown","id":"47501b6f-fe32-3248-a903-87b1dab4bfa2","instrument":"4000024910S00008","is_eligible":"false"},{"type":"unknown","id":"eede3e78-79fe-3e69-ae01-ea1f678071ce","instrument":"4000019450S00008","is_eligible":"false"},{"type":"unknown","id":"e24beb07-8e0c-3e09-a603-c0a00ff1a5d1","instrument":"4000019630S00008","is_eligible":"false"},{"type":"ad","id":"31770753203","title":"Nouvelle Renault Megane","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=TlJ22N4GIS9eyRRKJWTjF6Vhv_xQ5q73DW6wFT1A__4aqy_5nB0cFEfFOrx.xUaO9aiB6qoOQ2IjDnk65ryBG.DikSlxmyq4VDuEUVl7V0.bRV81AcM.2vMI.q_mrm.7Im8ESJbXR_03SJGmE5.uyaW.1A_AsUCnJfcXw9I70pxEDyGP3B.uKs5KMgaOWccaN5krqBsWPO7N_nvCAYsz..Vy9vzgZlx8LONybrUcQaCQKz3VQcWifoYfXp4jobvp2dqvCEPY3BQ1.RvSvwdNSVuJ6gyw_bcelIlIJeD6L4F_VL8p5YEPEPpXP98qPBl_pxFp8v9b69s3thjFubcpMCM4N0TDy.5Lhdsi6RlPrx_qkLvaUX8JiOxse.X208.0bObKc.EBVjmnZy_66dql2EB5x7ylPWsXy7TuAU3Yvtz21.hVsyQCJXssW5uSi9WCUGXZXOLAjJfqzxqrHGFOm84AGRQuXhmWIy9yJcyC04ftU0cIAeaedL8U7w--%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F302647485%3B129744907%3Br","publisher":"Renault","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=iRhYj3MGIS8e5lI9TCwj5ifM744RNxxajAtCY3Dh.to9Zpq.nm2vnXhzJ6HknO7_mAtF3IFSe_bzEJVysKaO7JwspSDqwswqbyDOX51Le8gPqMlq9vQEqbUH_SjnS5jrC1W4ItnxbTIpsn4Sskox3GFF3pIuJzKadV0R1EM_Agf1trHr0fCx8Tvkwm8qGc_JLz.CqzoaYL7uWfLtL.uhB0_cF1efToe04TUAiFT7ag1l0n5OHoIU817WtzNIzOdsLrFDM9Jb6md2R1oX_IBQFJrWlmxUQCgMxI8v8wehEmCV8w2cfqjbNpD0rqdVVn.ET4_toimyklXdB_kozpvE1CvmYPYPaKJdfgScD3ogtD_W1svYlfPNgDYBTg5DCOCOqrGPnSBahg70&ap=19","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15ne45u92(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31770753203,dmn$c31770753203,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1116468,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/wEkxFWLjTHT6LdG_yPTdWA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1457951182152-2637.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"DÃ©couvrez Nouvelle Renault Megane, Ã  partir de 229â¬/mois, sans apport, sans condition de reprise, et avec 4 ans dâentretien et de garantie.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":2,"cposy":2,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31770753203","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":1},{"type":"unknown","id":"94ad2a1a-7dfd-32fe-903d-3003c5f99414","instrument":"4000017590S00008","is_eligible":"false"},{"type":"unknown","id":"236a9c4a-6f8b-3fdd-86d3-90c455a1dfa6","instrument":"4000010190S00001","is_eligible":"true"},{"type":"unknown","id":"09e205d8-324f-3aaf-bde6-5f802f7ef115","instrument":"4000023220S00008","is_eligible":"false"},{"type":"unknown","id":"25081be9-18c7-3a6a-a4f6-7de8d3c4c807","instrument":"4000007010S00001","is_eligible":"true"},{"type":"ad","id":"31703404649","title":"Les joueurs du monde entier ont attendu ce jeu!","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=jJoWssoGIS_sAfLSAEqM1oKU6j8RoZ8MaJE3xWjHj_.C1k6EFfX6vkqPBbfMT7DwOVV7vUk0cBa.74ZsKTvIu6fiBHIOkySQnSmA0hEc7Ta_jRtUtjE_Sa6oeBrIL1xLfESc7ydijHKjRzIQnCTv4MEUetRG9elSzytEZGfLB6H.d4BA42G5MjZ_41XmLdlBHO.PAAldYHSeXVRSMs5cBB.2WQnKaM_h7czH8LcjJAqrjFQdVO_JRynVUvAZUVdZoRJY1MgqDvkHrwWPZlWmck6GG8zpk5fRF5xNX71VYobQ.6Avd6Rfk6cGPy_zi.YppdkQcTLYa8exMsAk1y.hMYE_2rOKI.teECluK_1IjqCrL_tG1neHQcsyyHY_IFDDmJlnAIgZEh7IMQX0mqkiPw3TDP3Gjg3vLOJmCFaYXgZDaFQ35rkiw56dnvKYhGAxXUVp7TdkRfOyGa0JBOVXR9S3fXBRJ04ZeGZ89R9f7L7G9OcbdmLeiHr2zmh6IZNzeE.W2ktQr7NM06Mo48BOmEQt_exg7d65tGbqr2ysa1cG4ZF7XuLRoBc.wqWuL9919jSdqq9SJjLiVGeYGtvDS_9f4dDqzdUGW0awN2wZPpbTN2vAG4fe1W3LAXIuTB0KqOrXLtw-%26lp=http%3A%2F%2Fwww.mnbasd77.com%2Faff_c%3Foffer_id%3D1384%26aff_id%3D1065%26url_id%3D4561%26source%3Dysa%26aff_sub%3DFR%26aff_sub2%3Dn244%26aff_sub3%3D31703404649%26aff_sub4%3DBoundary%26aff_sub5%3DB","publisher":"Plarium","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=HGsbNkIGIS_e.AeaWhoz_cLZU89hMeaMYbmR8JyfqwxT40X_6gu61JyMVruJISKGAylzlMZIYMqti0apVRZOKlCq4R1gEPbd.nNk.LVZ71.sGGDMHQGXHajbuIYg92cQOgjYM9QQbQy50sqyzvebROWWGlOUIXO9KxRexYsgTEvXFxViLQ9t.cAM9pZGbQAf5vjzV_5XuPpjNVj05HfMToPttKO6bU8HY48bATDjkkuwGAoHUXvloMxR_lYvd9r3gO1OQpxzSzkXEUspIqPPqb._VDegT8QVuMLXJGhWbnM_wogwM3PnlGweEGnysQz3OB.MJ3QcZLdM4Kz556uZVFQH4XYahi7BonjOWlqwQ37yjXgmfTzsn1TDiqbC5txG8jtI7A--&ap=24","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15junutc0(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31703404649,dmn$plarium.com,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$5584,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/DgYfTZXzge.YZ1nidgd0kA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454677871802-8151.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Pourquoi il te faut jouer Ã  ce jeu MMO de stratÃ©gie gratuit et addictif? Le MMORPG le plus excitant auquel tu as jamais jouÃ© ! Ne passe pas Ã  cÃ´tÃ©!","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":3,"cposy":3,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31703404649","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":2},{"type":"unknown","id":"5104ae72-5d19-3cf2-a0f0-e8162ae8aa02","instrument":"4000017360S00008","is_eligible":"false"},{"type":"unknown","id":"0577eb89-2025-37d0-bf53-fce3575615e2","instrument":"4000007330S00001","is_eligible":"true"},{"type":"unknown","id":"454352e1-3a13-33a3-8241-13bf74bfba53","instrument":"4000020270S00008","is_eligible":"false"},{"type":"unknown","id":"3526a371-8559-3519-9828-74a158ee8ae4","instrument":"4000023730S00001","is_eligible":"true"},{"type":"ad","id":"31760810223","title":"Investissez dans le neuf","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=H7UMX2MGIS87pXxJ8v.ZfRRyB.PnC9DnJjoK8gc2i3_Ss8fIkN4VLboBARdp_az3O339cj7_xJBvpvfgDq4sFmQVXVMdL5EzTh3P23wCej42MXQTCnwgP8C4ECb7ypBzm._RysLzKq9q3f0lARWzbj3afShStml0MdbfGjf3HWdhe0g.QxI2kV5HBUHB2L5PXo73krJsBKSprk34woyjo61JC1iua2gx0ufdVVMNw_IuRd1xTSEYS8PRu.BD06WToqdIOxMhEic5GqHLVE7P8RUkB2xLbqx4S0wZbWogjVDzsSOB84bPlzfXmtWQ1SQEQOZFKYJjW159MS9l9cOH.Lc8nRNpJ9Yik4jQ23EInxDhFl7L6Di0XrCd2Qa1nnsvRChUTgKvjyf8sMxHx0mgCokkFYI1KxB_S5QBYE8_taGdL1ZUEvOoa7pGcIRoZ7o8J40ETGsbtgch19AeyFXVf2kcuBi1A_zXUMUbLkIlmD.GNpJUnlI6et5KXu2nIKk5yH5jTBQ-%26lp=http%3A%2F%2Fwww4.smartadserver.com%2Fcall%2Fcliccommand%2F15488205%2F%5Btimestamp%5D%3F","publisher":"Nexity","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=iEvuw9wGIS9UgRq2MrQiHJKBv8JY49CLEKdSoh7oGWFyvEQYY4ypsZJFD_EsP3.YqyjdE5wOTB4kpzbUlQzY7uPdrmj4aDToAL9IHJAR92Nkd2uLIO..iN2VyRBoWlQkayy0mle6UHcoIZOhnGJ._yyhu_IajX0PDczIGa9p8DNJLVbuQSJU9u7M97j7CSitXFCKkLZPVWUrlxHPsfP5LNQNMbEyAIuTOwgNkfynhBjkmKMOKHpKsR3SY9MDyHM5htM94zzTqboVWTAakW4HjCApP5WO5nDxEXqO.jfqGkGRcnNFwoReywyijep2Ky08nXIr_5wiwXk70V6wanbQTNvZEeaR5BI2yyYrMn5JEHVKxnE2LIhQV1MfakBs61hzCUi0BdxhjQgR&ap=29","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15trjddae(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31760810223,dmn$investir.nexity.fr,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1329679,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/C7XMA2jVl9P.UGaw_5Lw1w--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1457363149869-3992.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Du 7 mars au 10 avril, bÃ©nÃ©ficiez jusquâÃ  63000â¬ de rÃ©duction dâimpÃ´t avec la Loi Pinel.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":4,"cposy":4,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31760810223","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":3},{"type":"unknown","id":"6a466d31-af57-3616-83af-a2240da58958","instrument":"4000020780S00008","is_eligible":"false"},{"type":"unknown","id":"2c71214b-5cfa-3f51-b173-d7620ce6c767","instrument":"4000017150S00008","is_eligible":"false"},{"type":"unknown","id":"ef6a306d-f2ee-3c48-be12-04f85dd02700","instrument":"4000010360S00001","is_eligible":"true"},{"type":"unknown","id":"d6d50be1-3b60-32d0-a210-4ef582962e4a","instrument":"4000018090S00008","is_eligible":"false"},{"type":"ad","id":"31487731525","title":"Baskets pour homme dÃ¨s 30â¬ !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=JSHjbFAGIS_ZfKGGbF64SyUB2EEGEWOzSs7aV6jzvzpIyEWx4jhQ8rls4MSQDE_7TTabfKimQ2LEg7VXKQIRyygkXyciHnwgO1.Mu7Iu3Fp7Ja9RBJ6cbX9TJAop4XurSq_tyqNzLBwhlEII_P7nC7GTeDx8.SXcp2LtzcALnObVfmCmd2yu3Ruia8qzXWoSodpuLWuLNwY79QgXj4IEiFEV9OFq6Ioiq3UTnIwB3gcCGp07g1l56ao4HNPaLPSznVS9vh07MN15_U_dVshYlshqevglAjMWcLQK2AaiOEnyCk1eNtnNyaxox2s3rauYAiQNm0.8ltY9RN54aPdHSkTGRGGS9OlDVzJ67.UMvMwAcRtxq0..eJUCC94U3BJs.oqx0pO4dKWwNj9uvcPq.WpmMh3nPME_ueNNTJX0sox1dC6ICzUOU2tcA9E.m7ugzmN4eg.IdRtsix8Nl27HEYm.Bf_DFCjMxC3H5BteiZlh_IMCDZlYVtCGfjdhJ4sZqJYWS85CLwnzI6Pare1ZZLX8b6p6hbYwPZT7op0XmpHGILl3zQOMm210hjAONXye8Y3KeSvDBlzdzHPc.1mCt66zrmqCR_YQ%26lp=https%3A%2F%2Ftrack.adform.net%2FC%2F%3Fbn%3D5598989%3Bcpdir%3Dhttps%3A%2F%2Fwww.zalando.fr%2Fbaskets-basses-homme%2F_blanc%2F%3Fwmc%3DDIS33_RH_FR_DS_2015208.%26opc%3D2211","publisher":"Zalando","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=RcU1zlQGIS_Ks3hKEHZdrKgjf0gYOaZSyyht9ipJZEc6L3nprRDVMwU_LK3v1_bDIADOtBetsolvFks2cPg3IkM72GUC36W69D76R9A29Q8JcTX_alFeOlcPnnD0LoHss61FQNVII4jG8x00d7jsbts4Ce3TbJQ9uh3nr0y7RnhIJugtFe74MpvhnGUqMcNfrGMsNhcLzynJ3HwBMzwFQWrNDY3ynzoZpgvZQlOyGPmDapU.jbmwkO6WNR3vx4cJNoRd2EL_kkJLsnoJGzQwKHgHEb3Vwp2FvPgVMTG.jGxxqyPOGjURwU_rJclumsF_PSmmd1mdX7vZcqvE_yExoOlNwCGOijuUyUeFEsTMuYaCT0XP90iDnJwfsJzB6evHyboHrJ25zAc-&ap=34","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15khrofq1(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31487731525,dmn$zalando.fr,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$978823,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/jlDrRfHMh8Z0rmCOr.tNAQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1448883421638-9512.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Large choix de baskets vous attendent sur Zalando. Livraison et retour gratuits !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":5,"cposy":5,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31487731525","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":4},{"type":"unknown","id":"1a81c659-82f6-3f0c-ae9f-659850294120","instrument":"4000008730S00001","is_eligible":"true"},{"type":"unknown","id":"1ea126e0-9849-3403-9c55-8c9c31a44a38","instrument":"4000004320S00008","is_eligible":"false"},{"type":"unknown","id":"4e180354-f057-320a-b15b-227b6330b1e1","instrument":"4000003960S00004","is_eligible":"true"},{"type":"unknown","id":"1dd7c041-5922-32db-b4b3-3cb72048a4ca","instrument":"4000016289S00008","is_eligible":"false"},{"type":"ad","id":"31697955465","title":"Explorez les palaces des rois dâautrefois","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=qWgnPqQGIS.VrGc0.n01RiCgvxde_YUj7Sm9DbM2LJxjnrs6CvIgxF2e5098Jl3dQX_8JKjnQRHZGlSiHkXQxM_f6aXy8bh9hzGKLOHH5VvJ31_uu4CV2CbsOIzqxNM7f7eYJSKCLsSADLl5K1NsIKgneCi5ytInGHOqu6bBSLxBHp_tOpL4LKWGivSuoQaQYfXfd0uHprJIbfpOoW1_MIbn9BckI820qd_p0SjFdu0ebXedal7xMoxyTDs4aMr8TwvJ7p0rQuSxUym6Gbf34Gu_TWeJh70skK9hlQHeI5mVgRClV864smzt4A.M3bDqKbOg1aGQhgRwmpDfRZk2LGZK7Ad0cFeGj8UhHvMASMmX1EEzZIIEn0C0P4AF_BZxMzJtD_gBADGMFlQ4sJ2OG2J2nwL1NlqjDVKwAfjifRjVOSmAFogfXGrL5_tX.9gp_ODCo_fxL48mtHoZqdFEGi4CS4QQc.PNRxIb3fdZorbiVrilafdMIp_7bDDuWaj_UT115OdlJdwJoWfl5p_uiAPQUcnpfpratOmweEQkKbFPRtQJqtGdQIGTnUF0DwivHby2BQOy_Fm8.vkhgjiTD._fXuCo4Jz02YW6s2_wfhyMTVn2.UcdZSTwqbNZ0v6JFTsrgpHeuVvqmRwu2Q--%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN6198.282706.YAHOO%2FB9362362.128254628%3Bdc_trk_aid%3D301271079%3Bdc_trk_cid%3D68690228%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D","publisher":"VisitBritain","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=bkoArAgGIS929F8LFBDyxgbvaIl5ISipva4wYKQ58iqdZITvc0ue0rgo76.qoqIgMRG7BUXpEjk0CqWHCSa_grohItrNQl4YtpY5zqyjUNcd3r0bfrPkUR05OMo0kNk8Dx8yUXP7LPVNnlQbYo8O1SOJOyYVPtir7nfV5wTcYUgYVbJYkwaBRW.i.elDsSO8RqKFicYRj5GtBAQADscc6jXvcYzWVB9hpNI4Cg6_M0kuH9kSLlZ2KS0YFrKqpiKB7MDP..ayxum1HcmwZxc3pK4yrUBSE_rUAVVEHWK0Qy3vkoBuyYPlNJ3LO_eHZ8YjM9TsRO1iWBFzN7nEkRPj2XJJq0IQ28uYDoMqyFP2oehg8.YXuDXidK_l4ZWEs4mY37r81z3C6_oQ&ap=39","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15rj8ljnc(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31697955465,dmn$visitbritain.com,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1322287,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/Yibku6AI.cIWEW6qPF6otw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1454518047209-6863.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Visitez Hampton Court, ses magnifiques piÃ¨ces et jardins, et apprenez-en plus sur les rois et princes britanniques les plus hauts en couleur.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":6,"cposy":6,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31697955465","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":5},{"type":"unknown","id":"7642db61-cc19-3814-a628-ea9593485f99","instrument":"4000014680S00001","is_eligible":"true"},{"type":"unknown","id":"23a10e56-dfb7-396d-8f36-08ee740968f1","instrument":"4000013000S00008","is_eligible":"false"},{"type":"unknown","id":"fed7dd96-7101-3095-9802-6ec6516b2084","instrument":"4000009580S00002","is_eligible":"true"},{"type":"unknown","id":"c85e6540-507a-3c98-aab8-eb9ec87efe0d","instrument":"4000009830S00001","is_eligible":"true"},{"type":"ad","id":"31756504751","title":"Birchbox fÃªte le Printemps !","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=YxoitHsGIS.AbjI9rMeFC3vv.GbOZcKycvogzjQ3SDLwoZYD75QnPzOXAAIdC7flzU8_NJ_AbJtDmQfns_8JRP.W7GhIUmpZftQC_0oQwY5D0d4iJWHDlRE_lo4HUL8D0930rZUMJS0UJ.yAybwraDdEFvxoH5cCZwwzSncft2FMpjXyfPMQ0hdXiX8zA.5TqaKMKSGYg8AbpAEiWkHd7XonzgAm7q3p_DCFmiv58qm0x.aDIPrLFlCK8NxDQMqFm3g3t9eIXSQg5oTK0dMJSUlSOJtt6yu3Fg3nC.Wk9Z7ebknNWFcZkNxSXRjLWf5IAQAFSwGSLdxFBIBxmEhJmW_jAT._d3mrL8vJIjUWmGpvJYDv7525A75hMKeqKGJmALBHlW67aGHn36oZx2lasvTRFiY_pWmGczu.PLjAAePNLqF3FjNpP2TymBFJrhrl_GeV7r.RHxSoVFIXNK08e_LOfj1uiZw0zjQqUG4_R7NvodMKEsAH8i1b7orpvsmomFmeoqhqccb8F8gmK7JKULxqiwunpHQfDkYY6SlUfCiWN1G_rtb1oHD55mV4_NXk87NFoHk3Jhq1dRIie_gc%26lp=https%3A%2F%2Fbirchbox.fr%2Fbox%3Futm_source%3Dnativeads%26utm_medium%3DGemini%26utm_campaign%3Dinteret%26utm_term%3Dhorsmarque%26utm_content%3Dinteret","publisher":"Birchbox","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=LxBW834GIS9lNlzwUXi7rVrjGYkqtQmiKkQhCQekIAi3LzVrKFDcs2DS_Qisswy21g_Acv53Evr21MhA8dOvVen.F7aTaC3xPNxf0DiAFljTabqMm11RGfrD8Y60x1aN0xfUdhKuW5pfzD8lKJg0bZ6ikYxBFkZQl3no3RMdCZrG6a6jaQBm_xvGMABO9_XQsJCBs_3j9hfw.e23n_k3x0iY33EYN5O.e4qhCx2d0xKozrPzt28QeVLsuAdgjBlUz7cDFpnrSavLYc60wI.47euKWhqYpBFyDXFMrvJwtypXoa6TsjZT_6Ma_RVsIRhVYjU4NGD6Gys9aI42IJReqJk63W7qjA1EUk0TVrkHIcIQvIwN.mhcyclowtjOLn6YFqFqAXcUBWAz&ap=44","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15m6midve(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31756504751,dmn$birchbox.fr,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1168738,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/obiAmwug4QBmzTu_rQBMiA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1456845052174-7927.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"C'est le retour du Printemps ! Pour fÃªter le mois de mars commandez la Box Hello Spring. Une sÃ©lection de 5 produits personnalisÃ©e pour seulement 13â¬.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":7,"cposy":7,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31756504751","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":6},{"type":"unknown","id":"6bf0032f-7037-3974-b471-fc7ee9de67fb","instrument":"4000005660S00001","is_eligible":"true"},{"type":"unknown","id":"5552b1c7-6599-3588-afdd-bcee0c2ec346","instrument":"4000001900S00008","is_eligible":"false"},{"type":"unknown","id":"ff93f036-54c7-31d3-b486-02c991f9c193","instrument":"4000019870S00001","is_eligible":"true"},{"type":"unknown","id":"3f5b56e4-323d-354b-877b-d962bd158a4a","instrument":"4000002250S00008","is_eligible":"false"},{"type":"ad","id":"31748183947","title":"Jusqu'Ã  -40% sur votre assurance auto sur-mesure ?","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=mYZIqqAGIS8v836iApnoGZDbEEVCT2Gn4VWENu0JajRHqy8Wv5eRtN.mTMrzFU1J1cAMQ8Wh8zM1Mbn4hsoPh9PE_CaUmp50cz5c6NSAds03KqDHm309MQGCLmrW52NHl2Nqaa.JcrFEmJCKXVdQNI3ixxpzAhcLx4QdSupRAmxGhBNNOwkqNkntCaSZ8UFjYym5eAitLxnbDsQtx5YtKjgCrWTBTFcVCdJuvDAXkKr7f3BiX18Um4O.yfXiAVkVoqEjVR.z5vYqlyYLVNeXmQvaGCTCAyLYfVW_0vLFmsbCpUzlG3VHu1tp.3kQPJroTxlyTboal54e5WQrspRFj8dVvggMVZztXQRbzI2sF5h87yLLJEFQ9kZD1MUDnPDF.r_jWNgIPmKJGcdGzmujYpnDgGIUoZTX3QBJ.rfrD0iwj0zzybcuwWwagO9MouO.ehXFEIuc4UEtQOx9Idm831M6xN1AEwJxbCLrhCVcLLg1bg8-%26lp=http%3A%2F%2Fad.doubleclick.net%2Fddm%2Fclk%2F297611419%3B124711560%3Bc","publisher":"Axa","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=V75Op0wGIS_5t5pG92qao1ChmET.H4LIs9qZvVE6SHreLorPSEZ3VTDc5MawL6OOfzrJfI2L2wyopnBPtKQmSMYHxCQ2GjTlDQNwubciqd769EulwHuo3eJq1pVoe.8A3Gv0pStsXUv.G9TB4gfw_5CWrcNzyc6znwZMc0xa74E0wZxwykgokMaxg.GGAek3aZ2Gqdg5BPeVklSyrzn8nUUUZ2CizIfNx9Wcs._XnVs0iYKoRlmdaPmjwn3AULp_ItVZuBBDmc7YFSdyzuYGFY1aaP4Y2JKQjKAYmJAS0mwIbmnRC25iGlGbe2rZ73ODPtXYLmvOEfhdfS3sZx_0ACuqZLucoYtlely8mcEmkanspUlCBfxcWSvuUkqI4d49Rb.voMnPhA--&ap=49","ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15g67s2q1(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31748183947,dmn$axa.fr,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$984886,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/OV5mVQCjpfh6DOMOLbXUcg--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1455524450605-7928.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"A la recherche d'une assurance auto modulable et adaptÃ©e Ã  votre budget ? DÃ©couvrez toutes les offres AXA personnalisÃ©es !","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":8,"cposy":8,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31748183947","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":7},{"type":"unknown","id":"91cf98b4-a52f-3365-9b68-b3478dea6470","instrument":"4000013620S00008","is_eligible":"false"},{"type":"unknown","id":"0f9b773d-4d76-32a3-a693-62a178c5d645","instrument":"4000017640S00008","is_eligible":"false"},{"type":"unknown","id":"bbd0c833-05eb-3b45-b758-c4afbf97cde4","instrument":"4000007320S00001","is_eligible":"true"},{"type":"unknown","id":"fec185f5-e9c6-318b-beea-48bade566af9","instrument":"4000005700S00001","is_eligible":"true"},{"type":"ad","id":"31756032813","title":"Exit le stress, place Ã  la dÃ©tente","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=S71Qa_QGIS.cGmTTOVJLbUKatD_Q.lk1Nk8D2biEGlqDZrSSgBPdBYyUGbln4A1d3Rla9YdEngQcvhECwPeHnaeIG3F9XkamSamY9dexM4OJAEUeutsXondzduy7ZfoN0qma1pqzxjNsaTAbyLdjqTlEtmF0IUdfH0s7KCxTYEKD4grN5lvvJVKtJ47wUqfmNxlwoG_vfij0hqsXnyatxQP9S6QBEfMRPjiwv1JtTYefzZ4LwdWCY6lU9cV1hSX6wmiL7TXUQ00PMn3RMI_Ns2HpbF3URMhSK7M.UKJrylGUVhc6Sg9ITE8kEvtk4PYfcRwhWsx0fUUjW8nMIyb7NSfqUR22afIwWOWixwSrNcZN_JtGiJW6JCE8Y6BH2Y8DbSP7sEskNwmOKbuqxFJH8mpUS.bXOAodIspajhfaf_E.dBMN0sq0kZ4pfaXRZ2jl1cW.tZQ.WJwouDdfMBOTJS7i3L0HIOCQnchK_A26_kJxh0gxfoxkiuR0ymlvP1zLewlXft89hFWurEldG0sKBGGpESCJrzEbPJjI1LW4Uq5O.QVPvGC4s444Vy0r3H21pVXBOBrQsvJoZt_4MSRoUDIbBL4uQRrb6_.35uRSA65NDx5sXBVSbGBAlLKkNeiopCcC6tt1DpfI1CPQC8Y-%26lp=https%3A%2F%2Fad.doubleclick.net%2Fddm%2Ftrackclk%2FN232003.2125501YAHOOGEMINI%2FB9431060.129248564%3Bdc_trk_aid%3D302194956%3Bdc_trk_cid%3D68687000%3Bdc_lat%3D%3Bdc_rdid%3D%3Btag_for_child_directed_treatment%3D","publisher":"Kayak","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=0UNre1EGIS9yWbYQUTcvXorH_TBEM0KzAmItn6CecRaRraIxDbJcdwbPlCIE8M5YjlFTldcPYzgduRT47MG6vYvaY61UJGgOvltLP9T92Wr2tIZql2oOoyJjOid4AcgKukj4Shze4u1fCCfQviBU6xQeMBtzMTfmZdzCcRfdSEg6MBqEocQUr2yCgNxzwAhytzuLNtHGqsCKi4rihfwvNYLXwT6Ilsj_3MnYOCg74c3QFsFQ8Oaa1SQN_UdexBXqYur.mHQpDK5SbgEIeBKULAiva6ZF1m1zQVgEZDjNnTVE3aZ.rvRhRXz_yPzuE7Q14a2Q9pP_zJ4dL0vGJNE.JhsCKwl.PpwrkJ_Tfjd8wJjoHTFP2JltwXBdMf.qG38cRmW9S3kOyg--&ap=54","imprTrackingUrls":["https://ad.doubleclick.net/ddm/trackimp/N232003.2125501YAHOOGEMINI/B9431060.129248564;dc_trk_aid=302194956;dc_trk_cid=68687000;ord=1459183093;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=?"],"ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15j0fiq8r(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31756032813,dmn$kayak.fr,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1351972,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/WBdNLGzUk5CTsd7eAU.YwA--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1456766445681-7091.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"KAYAK recherche sur des centaines de sites de vols et d'hÃ´tels pour vous aider Ã  voyager de maniÃ¨re Ã©conomique, rapide et fiable.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":9,"cposy":9,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31756032813","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":8},{"type":"unknown","id":"e760d8d9-45f4-3386-abff-86bc555a8251","instrument":"4000000260S00004","is_eligible":"true"},{"type":"unknown","id":"45757405-04b4-35c1-80cd-b87b1866d00a","instrument":"4000004560S00002","is_eligible":"true"},{"type":"unknown","id":"395e2fc3-39a4-39ff-8994-4973527bc3fc","instrument":"4000005620S00001","is_eligible":"true"},{"type":"unknown","id":"d6db2b22-0376-31d1-b605-43bbbcce9451","instrument":"4000000990S00008","is_eligible":"false"},{"type":"ad","id":"31762450023","title":"Le tÃ©lÃ©phone est le reflet de son utilisateur","link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=XdBM4gUGIS992GJ6Ic4qPhpy7s1gMTxB3Hm8jRBRjGs63b7VDjJzRAJs_vm5eRFaYnQ_uZ2TQ37KAdE1SltvlReB3Tf_DmDz9UWtV.Y_GOJu6GWPRj9HcD4TwET4rlfkOY8mvgR.nCobNiN5PfrL5ucduDpO19Ds1yl5Zw.jHd9L8yHreSYUQcccPiOnkUYRWJj9lazVuBjPqLQaMxOtcp_ddEiBT.S660eP9_nzTSAAS9_DcV2AWb2FfTJR_gyIy5PyI.51XYOSWe3bU0UAs1OPwgt48aMH4UBgRzOXKJK0Qv3rpFTsSTdEjNgiOIgBPF_xQQHXPl6TsxdZLnAc_CioLuCr7crA7D3ESwYTWBKYisoP1HbgHeW_ng300VWklpyuJMdKyzFiy5eySY_MZZ4hatX3w9v8YUdRte8r.014_Kt9iJUU6ild5IcdYc91ZCmG2kH_5NmTW_ZmJaURp3KD0_GL54FahdvpwP9FUaa8BtjWzIHww._jfww5O1tfPZ7JfNU6zxu1Wcn3IuxL3PwTBNewBqxTRNEgX93AHdx_tore7VS9qzq5hfKi0FWZajCmog--%26lp=https%3A%2F%2Fbs.serving-sys.com%2FBurstingPipe%2FadServer.bs%3Fcn%3Dtf%26c%3D20%26mc%3Dclick%26pli%3D16865133%26PluID%3D0%26ord%3D%5Btimestamp%5D","publisher":"Samsung","beacon":"https://beap.gemini.yahoo.com/mbcsc?bv=1.0.0&es=1y_vJ8cGIS.CPo823vZjtpUR46UjwNY7NpjV9HKvNcLmOWNZhUy5mkh_yjAscXr9pHjDx_j6gKu15H1IHcoBwHVTE6TOtsVnL9oY8apDKuKvxh94qQq9GgY02U1e.z60vKntxhj5tJ4SXg.QbpCSMWgsGwjt35ZvGu7yNj5Lj.6MP80W7O93FFAMOiTwsMCNDMVXPirTLmYMbmth4Bop6hVbzzkDqcLUf8YSaCLJBu6gFqtAPUa6wub3GUvKALsZXRQ2xplMqWiq2H8uTsORWoMElwT9pAf9ZaSeR0kSgYJ0x1oHW5AcN.IVMgumJJBJEZprPHfpj2qJHUemxuN9_70QcXBTqQ3d0Xy1.nMCYMWFanlPzrsu_AgmsmP56ChlVjmLhy__FFGDI2U-&ap=59","imprTrackingUrls":["https://pixel.adlooxtracking.com/ads/image.php?banniere=scsmgglxardonentvads&campagne=starcom_samsunggalaxya_radiumone&client=starcom_samsung_galaxya216&type=regie_quotidienne"],"ad_feedback_beacon":"https://us.af.beap.bc.yahoo.com/af?bv=1.0.0&bs=(15m6r09p7(gid$76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700,st$1459183093282000,li$0,cr$31762450023,dmn$samsung.com,srv$3,exp$1459190293282000,ct$27,v$1.0,adv$1340700,pbid$1,seid$4250754))&r=1459183093282&al=$(AD_FEEDBACK)","full_template":"featured_ad","image":{"url":"https://s.yimg.com/uu/api/res/1.2/GduOaSjv5wVq4Yx0ECzMRw--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1457530148947-2528.jpg.cf.jpg","width":"190","height":"107"},"storylineImageCount":2,"storylineMultiImage":false,"summary":"Disponibles en trois coloris, les nouveaux Galaxy A, Ã  la pointe de la technologie, permettent Ã  chacun dâaffirmer sa singularitÃ©. The way you are.","storyLabel":"","adChoicesUrl":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","i13n":{"cpos":10,"cposy":10,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31762450023","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"actionsEnabled":true,"index":9},{"type":"unknown","id":"7c09d3e9-5877-3e75-a54f-727271249dbc","instrument":"4000005660S00001","is_eligible":"true"},{"type":"unknown","id":"4780b2ce-cb4f-31b6-936c-d842caed3542","instrument":"4000010440S00008","is_eligible":"false"},{"type":"unknown","id":"ec28aaf7-1a82-3a09-b4f5-f6a124d09a91","instrument":"4000015280S00008","is_eligible":"false"},{"type":"unknown","id":"44bf0771-dfbd-3b94-be3f-5bd148794136","instrument":"4000017760S00008","is_eligible":"false"},{"type":"unknown","id":"68f8a814-46e0-3b3d-a24b-7c90d281317b","instrument":"4000013580S00008","is_eligible":"false"},{"type":"unknown","id":"4f023195-6a77-352c-98a4-2fd2ae4d6529","instrument":"4000006900S00001","is_eligible":"true"},{"type":"unknown","id":"9dad69c2-59b5-3538-b3f3-75ba6b122270","instrument":"4000006670S00001","is_eligible":"true"},{"type":"unknown","id":"3d9fd6f7-dd4f-3b95-bdaa-10d23fb00feb","instrument":"4000016140S00008","is_eligible":"false"},{"type":"unknown","id":"aaa0050f-9948-3013-9d7f-108ae515cf7e","instrument":"4000016830S00008","is_eligible":"false"},{"type":"unknown","id":"c8633cd1-7c37-392d-ad3f-ce13aeb17016","instrument":"4000005020S00002","is_eligible":"true"},{"type":"unknown","id":"58f5b49f-8346-30ad-a56a-dc59e4de6e37","instrument":"4000004550S00008","is_eligible":"false"},{"type":"unknown","id":"3aa1e51a-f82f-307b-a020-a9f6bed05700","instrument":"4000010090S00001","is_eligible":"true"},{"type":"unknown","id":"35d45d9e-4556-39c2-b062-759b1bafa5ca","instrument":"4000015180S00008","is_eligible":"false"},{"type":"unknown","id":"f794ce6a-068e-39fa-acb8-16325cff1522","instrument":"4000012840S00008","is_eligible":"false"},{"type":"unknown","id":"3c9bbcc6-d658-3760-987f-92275d66c896","instrument":"4000014980S00008","is_eligible":"false"},{"type":"unknown","id":"339f2651-f6e5-32d7-96dd-0ea95932004a","instrument":"4000008590S00001","is_eligible":"true"},{"type":"unknown","id":"2ca49010-470f-375a-94fa-2d79c58445b4","instrument":"4000007910S00001","is_eligible":"true"},{"type":"unknown","id":"42b61ed2-4279-33ee-9e9a-d6a8ea7c8ecb","instrument":"4000007860S00001","is_eligible":"true"},{"type":"unknown","id":"b2042375-78ae-3b63-bf3b-c050910f8ab3","instrument":"4000004510S00004","is_eligible":"true"},{"type":"unknown","id":"ec29c3a2-37bf-3833-a98b-4489d3c8bd71","instrument":"4000007010S00001","is_eligible":"true"},{"type":"unknown","id":"26544747-7efe-3d23-81dd-1d18ec6afa19","instrument":"4000016570S00001","is_eligible":"true"},{"type":"unknown","id":"fc58c097-49f2-3c44-9448-8664cda8ea8c","instrument":"4000014480S00008","is_eligible":"false"},{"type":"unknown","id":"410009ab-41ea-3804-ab40-7e46ed305c21","instrument":"4000004940S00008","is_eligible":"false"},{"type":"unknown","id":"b5506d6b-841b-3c16-8927-c7d4c440f49f","instrument":"4000008470S00001","is_eligible":"true"},{"type":"unknown","id":"21f405a2-4925-33db-93f4-b80d613a7d8d","instrument":"4000009380S00004","is_eligible":"true"},{"type":"unknown","id":"26ef87da-b29e-3290-afca-1a1eba2fd297","instrument":"4000010010S00001","is_eligible":"true"},{"type":"unknown","id":"fcb6fc9b-407f-3eea-b44d-2d89b208a53a","instrument":"4000017920S00008","is_eligible":"false"},{"type":"unknown","id":"220dc058-984e-31a3-84d7-80f36904a90f","instrument":"4000007350S00001","is_eligible":"true"},{"type":"unknown","id":"ca74e4de-406f-3b69-953f-e0a53df84dec","instrument":"4000002270S00008","is_eligible":"false"},{"type":"unknown","id":"d6992bc1-4815-39bd-aaa8-dded6866ae7e","instrument":"4000026620S00008","is_eligible":"false"},{"type":"unknown","id":"1475f857-f471-348e-85c1-b69c83d65cc0","instrument":"4000007530S00001","is_eligible":"true"},{"type":"unknown","id":"494bc40f-e467-3f2a-9e75-3981bd31be79","instrument":"4000007450S00002","is_eligible":"true"},{"type":"unknown","id":"8fa09fff-bab1-3835-b66b-d94fbe832773","instrument":"4000002990S00008","is_eligible":"false"},{"type":"unknown","id":"f9ec5edb-ceaa-36b3-89c3-2a3455a59f83","instrument":"4000008350S00001","is_eligible":"true"},{"type":"unknown","id":"2d98652b-2145-3f5f-9da4-d2587add4579","instrument":"4000010720S00001","is_eligible":"true"},{"type":"unknown","id":"1d11bb54-dcf7-39b3-bd7d-1a2955a5a8d8","instrument":"4000017180S00008","is_eligible":"false"},{"type":"unknown","id":"590ab0ab-112c-33de-9660-9ee7064525a8","instrument":"4000009860S00001","is_eligible":"true"},{"type":"unknown","id":"ba2384ab-1076-37cd-8245-d7aa92d3ab40","instrument":"4000013820S00008","is_eligible":"false"},{"type":"unknown","id":"f9ef47de-bb01-359c-a88d-96bab216f7a5","instrument":"4000000930S00004","is_eligible":"true"},{"type":"unknown","id":"ff313c3c-d362-391a-aa6f-ffeb2c9068bb","instrument":"4000026630S00001","is_eligible":"true"},{"type":"unknown","id":"453e32a6-2c4e-3c16-8c98-e004681fccaf","instrument":"4000023540S00001","is_eligible":"true"},{"type":"unknown","id":"5bec7048-e6aa-32a3-8031-0ddaa6736ada","instrument":"4000014600S00008","is_eligible":"false"},{"type":"unknown","id":"9fd04601-2b8b-3ad4-99b7-e57469e49883","instrument":"4000007170S00002","is_eligible":"true"},{"type":"unknown","id":"daf8ddfe-ad37-34e4-b7c8-bdbb40261626","instrument":"4000005920S00001","is_eligible":"true"},{"type":"unknown","id":"f3dcd037-678c-3f6d-b45e-4c9e50913621","instrument":"4000014720S00008","is_eligible":"false"},{"type":"unknown","id":"f9832191-2740-3625-9978-7c3c5fc40bd7","instrument":"4000008460S00001","is_eligible":"true"},{"type":"unknown","id":"752c46d7-7522-3ded-bca4-abdc0ceb0d6b","instrument":"4000006660S00001","is_eligible":"true"},{"type":"unknown","id":"bd8454bd-12cb-3c4a-8bf3-defacbba9054","instrument":"4000010190S00001","is_eligible":"true"},{"type":"unknown","id":"3c5a0442-fe15-3c4e-9656-acdde3090157","instrument":"4000006850S00001","is_eligible":"true"},{"type":"unknown","id":"43b5c3cc-2cc4-3235-a196-abed87f15c55","instrument":"4000011240S00001","is_eligible":"true"},{"type":"unknown","id":"e1e3deaf-9b1c-35cd-ae66-485ddb0d6f6d","instrument":"4000011990S00001","is_eligible":"true"},{"type":"unknown","id":"4a10cc1b-4ecd-349b-99fc-310039015e1e","instrument":"4000006630S00001","is_eligible":"true"},{"type":"unknown","id":"f46918ec-13dc-3674-af65-a0330d667750","instrument":"4000006840S00001","is_eligible":"true"},{"type":"unknown","id":"d8c6da26-2acf-3e0e-8dd1-383f68dc24b3","instrument":"4000012110S00001","is_eligible":"true"},{"type":"unknown","id":"ed377d6b-47c5-3141-9345-2d58c0786ca0","instrument":"4000024760S00001","is_eligible":"true"},{"type":"unknown","id":"e93c499e-0df5-3d28-933f-57649bc8423c","instrument":"4000006100S00002","is_eligible":"true"},{"type":"unknown","id":"5e433c78-f760-3840-919a-b2aaf1e8e66c","instrument":"4000007070S00001","is_eligible":"true"},{"type":"unknown","id":"8df1f0b1-1693-30fa-b61e-e01b0b9869e7","instrument":"4000009090S00001","is_eligible":"true"},{"type":"unknown","id":"eb5a4580-3138-38e7-888b-53f5f83bfd0b","instrument":"4000006950S00001","is_eligible":"true"},{"type":"unknown","id":"9a3763c7-3111-3082-b2bd-7be173a38d4e","instrument":"4000006600S00001","is_eligible":"true"},{"type":"unknown","id":"7d522d4d-18c9-310b-b158-2b3365e5d8d2","instrument":"4000004270S00004","is_eligible":"true"},{"type":"unknown","id":"110574ed-4070-3118-8d83-a17d44301e9c","instrument":"4000007840S00001","is_eligible":"true"},{"type":"unknown","id":"6db0cbb8-be63-3e6b-951a-9500ceb61871","instrument":"4000006850S00001","is_eligible":"true"},{"type":"unknown","id":"b8796bb2-a9d5-30e8-8daf-6a1badf0640a","instrument":"4000008290S00001","is_eligible":"true"},{"type":"unknown","id":"b2090da6-b50e-3aaa-b7a6-8ee795c936f5","instrument":"4000007350S00001","is_eligible":"true"},{"type":"unknown","id":"a87ca6d2-b800-3e8d-bf85-c1eef7391797","instrument":"4000008600S00001","is_eligible":"true"},{"type":"unknown","id":"56329444-2df3-3cf0-b60a-3483f3843dec","instrument":"4000018530S00001","is_eligible":"true"},{"type":"unknown","id":"3e6b74c0-be34-3b7b-8794-7ef5286882f0","instrument":"4000006260S00001","is_eligible":"true"},{"type":"unknown","id":"2bb5cefe-3a6f-3cda-a32e-04b7b623d4d3","instrument":"4000015300S00001","is_eligible":"true"},{"type":"unknown","id":"7337688d-f0a9-3271-8197-1469bb5709ea","instrument":"4000006610S00001","is_eligible":"true"},{"type":"unknown","id":"503a1563-d684-3b6d-bb7b-ba6082232b0c","instrument":"4000004130S00002","is_eligible":"true"},{"type":"unknown","id":"887360b3-17dd-30f0-b5ec-63a73087d1c3","instrument":"4000018130S00001","is_eligible":"true"},{"type":"unknown","id":"72d23bd9-8240-3d19-b59b-370c69ab39c6","instrument":"4000009460S00001","is_eligible":"true"},{"type":"unknown","id":"7791870c-5072-301a-88b3-34ba878c7a06","instrument":"4000018390S00001","is_eligible":"true"},{"type":"unknown","id":"065f305c-6c8b-3360-a5c3-444d4803b0b7","instrument":"4000005080S00004","is_eligible":"true"},{"type":"unknown","id":"3d7ef5a0-056c-3435-820d-c3d2898d34ac","instrument":"4000009020S00001","is_eligible":"true"},{"type":"unknown","id":"c8124e50-7a62-32ae-b54d-ef917f8131c7","instrument":"4000019160S00001","is_eligible":"true"},{"type":"unknown","id":"0cee7119-c558-33ad-acb0-1f9d1e8f08ef","instrument":"4000005870S00001","is_eligible":"true"},{"type":"unknown","id":"c844e67b-5252-3555-aa8e-0c2b6ad98889","instrument":"4000007360S00001","is_eligible":"true"},{"type":"unknown","id":"370ed41c-0e55-3938-bc8a-254fcef300c7","instrument":"4000008820S00001","is_eligible":"true"},{"type":"unknown","id":"586b708c-c892-36e1-b276-a07e29aa26fd","instrument":"4000015320S00001","is_eligible":"true"},{"type":"unknown","id":"5d44b838-cbc6-31c1-b0f2-062f86a5a25f","instrument":"4000006600S00001","is_eligible":"true"},{"type":"unknown","id":"18be1877-0413-3e02-96e4-deb9c1d3baf6","instrument":"4000009570S00001","is_eligible":"true"},{"type":"unknown","id":"6264185c-6113-3358-ab02-c81091b786de","instrument":"4000011050S00001","is_eligible":"true"},{"type":"unknown","id":"adce9a45-8fed-3ae8-beef-973b7b965363","instrument":"4000010870S00001","is_eligible":"true"},{"type":"unknown","id":"a2e0a68e-f885-3071-9b83-00ca97e50491","instrument":"4000012070S00001","is_eligible":"true"},{"type":"unknown","id":"a5e7a78b-614b-3631-b8ec-2957d5a0395a","instrument":"4000005490S00002","is_eligible":"true"},{"type":"unknown","id":"7a96ac65-5435-3a45-8cc2-edfee07b1d7c","instrument":"4000000000S00004","is_eligible":"true"},{"type":"unknown","id":"5e9e8508-ee83-3095-a977-44e468a4471f","instrument":"4000008720S00001","is_eligible":"true"},{"type":"unknown","id":"559451ca-a2d7-3566-a349-b990d58ccee7","instrument":"4000006840S00001","is_eligible":"true"},{"type":"unknown","id":"30c17718-c95e-3c32-8d15-91e63df536ba","instrument":"4000009990S00001","is_eligible":"true"},{"type":"unknown","id":"40f89b6a-23b5-3270-b33f-01d19f38c310","instrument":"4000014170S00001","is_eligible":"true"},{"type":"unknown","id":"67a6a96d-d27b-3120-83e6-5463959b848a","instrument":"4000020280S00001","is_eligible":"true"},{"type":"unknown","id":"c0543edc-cb3d-3d3b-bce6-e9c7d9f6f2b4","instrument":"4000012240S00001","is_eligible":"true"},{"type":"unknown","id":"47f2362b-d5ae-3437-95ad-379ca204fec5","instrument":"4000006470S00001","is_eligible":"true"},{"type":"unknown","id":"e443287a-bd76-332b-979c-c2c85d38f846","instrument":"4000009420S00001","is_eligible":"true"},{"type":"unknown","id":"fabce367-244a-32ac-a366-b94cfca89512","instrument":"4000008090S00001","is_eligible":"true"},{"type":"unknown","id":"72298e8e-d00f-3571-a1ab-9ba525757986","instrument":"4000012280S00001","is_eligible":"true"},{"type":"unknown","id":"ee0649e0-ba8a-3a7d-ae66-b322bd12a2e9","instrument":"4000011960S00001","is_eligible":"true"},{"type":"unknown","id":"e9144f05-ae4c-365e-bd37-adb240b6cdd6","instrument":"4000006680S00001","is_eligible":"true"},{"type":"unknown","id":"a0e36048-e1e4-3f35-ae3d-07ac56830010","instrument":"4000011320S00001","is_eligible":"true"},{"type":"unknown","id":"fa143c95-ec53-3972-8789-7371b6d51f69","instrument":"4000010410S00001","is_eligible":"true"},{"type":"unknown","id":"70bf05aa-2f16-39c7-9e55-1d5604148b6f","instrument":"4000008630S00001","is_eligible":"true"},{"type":"unknown","id":"707f764a-2d04-37f2-a7a2-f7db2a9d94f5","instrument":"4000011540S00001","is_eligible":"true"},{"type":"unknown","id":"a6a76356-35e1-3bcb-9bb3-76c839e147f0","instrument":"4000006570S00001","is_eligible":"true"},{"type":"unknown","id":"88cce87d-26e2-35f3-af0a-6a0686ed7826","instrument":"4000010290S00001","is_eligible":"true"},{"type":"unknown","id":"a84d8ded-4770-3931-b1bf-26dcbcee4055","instrument":"4000000380S00004","is_eligible":"true"},{"type":"unknown","id":"337d6b9d-1da6-3d70-83be-0aa1cf34c78c","instrument":"4000008680S00001","is_eligible":"true"},{"type":"unknown","id":"2c209bc5-0e04-3131-9c68-d4ff1ac66a85","instrument":"4000010390S00001","is_eligible":"true"},{"type":"unknown","id":"6b0ff8d6-3dda-33c1-8155-01689643a654","instrument":"4000009090S00001","is_eligible":"true"},{"type":"unknown","id":"7386dd07-d571-33d8-9d4e-1b38691c25e9","instrument":"4000009650S00001","is_eligible":"true"},{"type":"unknown","id":"9ec68a87-d596-3caf-a657-fc28414c8db3","instrument":"4000011780S00001","is_eligible":"true"},{"type":"unknown","id":"71b786e4-2e1b-30f4-be71-ed5586fd4548","instrument":"4000006360S00001","is_eligible":"true"},{"type":"unknown","id":"3872915d-4b5a-342e-8efa-09c64a9948e6","instrument":"4000007950S00001","is_eligible":"true"},{"type":"unknown","id":"383cc912-d11d-390a-8cf5-597052a44b22","instrument":"4000013390S00002","is_eligible":"true"},{"type":"unknown","id":"c1dfa60b-0b42-3068-b47f-f7132ba7a220","instrument":"4000010330S00001","is_eligible":"true"},{"type":"unknown","id":"ac63b9af-f500-3303-9c38-b38d7025405c","instrument":"4000008120S00001","is_eligible":"true"},{"type":"unknown","id":"e494e4d2-09a4-33dc-99d6-17191fd8f62f","instrument":"4000005930S00001","is_eligible":"true"},{"type":"unknown","id":"7af606ae-9119-33ab-b632-7573ca748635","instrument":"4000010940S00001","is_eligible":"true"},{"type":"unknown","id":"16b1953d-0073-37c4-a555-b92b35bd9d6c","instrument":"4000006320S00002","is_eligible":"true"},{"type":"unknown","id":"251e2c3b-686c-33d6-90c8-076fb510fa6e","instrument":"4000006410S00002","is_eligible":"true"},{"type":"unknown","id":"4d4e4425-299a-30a3-b25b-bfc5f6f28f28","instrument":"4000005000S00002","is_eligible":"true"},{"type":"unknown","id":"9ae1636d-3521-3a48-b3fd-547e839f031e","instrument":"4000007270S00002","is_eligible":"true"},{"type":"unknown","id":"666ced7c-868c-37a3-ad93-6714e3290e61","instrument":"4000005940S00002","is_eligible":"true"}],"continuation":"","more":171,"enrichment_ids":[],"cposy":21,"stream_items":[{"id":"id-3599730","cauuid":"0e757951-3f47-31b5-b712-dd25cfd39de2","i13n":{"cpos":1,"cposy":1,"bpos":1,"pos":1,"ss_cid":"0e757951-3f47-31b5-b712-dd25cfd39de2","refcnt":4,"subsec":1738,"imgt":"ss","g":"0e757951-3f47-31b5-b712-dd25cfd39de2","aid":"id-3599730","expb":0,"ct":1,"pkgt":"need_to_know","grpt":"roundup","cnt_tpc":"Need To Know"},"link":"https://gma.yahoo.com/georgia-governor-said-veto-anti-lgbt-bill-145552868--abc-news-topstories.html","type":"article","viewer_eligible":true,"viewer_fetch":true,"storyline":[{"id":"id-3599722","cauuid":"7e5f69cf-40a9-391e-af37-f3a5d6568f2e","link":"http://sports.yahoo.com/blogs/ncaab-the-dagger/why-roy-williams--blood-might-be-north-carolina-s-secret-weapon-045416343.html","type":"video","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3599676","cauuid":"a39b7400-b217-357c-9dd1-20988814dd99","link":"https://www.yahoo.com/katiecouric/house-republicans-speak-candidly-about-trump-210816631.html","type":"video","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3599706","cauuid":"3c616156-c4d1-3fc5-9f5d-b9de0699e241","link":"http://sports.yahoo.com/news/excerpt--in--the-arm---a-search-for-the-new-frontier-of-building-healthy-baseball-pitchers-043238884-mlb.html","type":"video","viewer_eligible":true,"viewer_fetch":true},{"id":"id-3599741","cauuid":"fac6a496-6b6c-3d3d-9543-5c88928a4f04","link":"https://www.yahoo.com/music/music-mogul-diddy-job-charter-school-founder-120029026.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"ae5ddaea-62c7-36f8-a413-0367bc406060","i13n":{"cpos":2,"cposy":6,"bpos":1,"pos":1,"subsec":603,"imgt":"ss","g":"ae5ddaea-62c7-36f8-a413-0367bc406060","ct":1,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"Celebrity"},"link":"https://www.yahoo.com/celebrity/jessica-lowndes-and-jon-lovitz-reveal-their-125602307.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"31777728599","i13n":{"cpos":3,"cposy":7,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31777728599","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=vGn4HIMGIS_V9wcmxr33bOdjJIJ2jRD5FAC9mN4btKDwya9axyQOBOvOGRipUcbbfcRqcJyGo7y_nsV9pgAPC6LoXifvS7gjcm42yg1c_MrYxR1QxOWl1cmOB3W9BfglYernnWG0FRw5gFTZOa9JGODTxSYFmEBLQX2EcuYjvVWXNbwH0bqKNe6s8EywpFsJ8lRNryzkE5djA9Q0F6L4KieAaPyKC9ciVX9MsFcoe1t6IxY1YxAEXs7mtOZm823gQ4FkxJQ1aqF7daJOy8aC7WQ0PQgi94qG3egjKljfjRTcugI..orp5g3rPUEZ_0_aM84rLOegVY12.dEPNmhH.uObwVynRmvLJ4_B07sr1YUxv6yR.v11AuqaXoACyBXQnMw4CG93tUKt.Zzk1IXVqw1AeamspDXxJUl0O61UlLJ3KfQ2u53fXKoFhKSgwg9Blu4LLYnPKB8lQbYGx7mmuHj0ZP1PE1y3vhjFvp.Xcz.QzGDaGEdClQf4S8dT2z2ValszSTf0xj8F2xXt7zQZ.DyVWraXLXjo2NdzRq.lXiARgrMMDbmWm3JqYh9L.t1UjbkIEpWgqzV7yb5yPU9WYqralG6Olsd4BsWhxQ--%26lp=http%3A%2F%2Fferrero.solution.weborama.fr%2Ffcgi-bin%2Fdispatch.fcgi%3Fa.A%3Dcl%26a.si%3D1141%26a.te%3D324%26a.ycp%3D%26a.ra%3D76de129a-f503-11e5-a997-eba151d879b2-7f90735d0700%26g.lu%3D","type":"ad","video_ad_beacons":{"VIDEO_30_SEC":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=Dj3ME8gGIS.ECzDJ5Xf49tjIlq1e_D8GEPABtAQh3nTidF3Slrnmtuml5cuUK7e2R4SKk3w8_DIU0P8Od66y8hqUn8CFHVrg..QbRrBkXs4_aENpfvQxE0NoBVifNySucGzsCqOezzQPb4oace_HAaEnsbzqnDKqO2ptWM2dDmPeK7EdHaCX60O0oLXkWEGoCYTwa4dwmhkOqUhouaZCZyfgm4w34F_VPp1zk0RuzX3T5Fn7WUXQWkSYMuF2sfgy5zJL8dFSEc2cnDOnrOSjTFH0eaZydVuv3FiJMOn1JrJBUy_Pp4.1x90-","VIDEO_CLOSE":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=MULzJ7IGIS_MW6GpTk3kNFskWSCOGj0adaXdehxb0UBEFVqrIXlsgpLzrx.r6IorqEfx2oLAuFcVoViMG8itmW3t3EGk8ielJwBf1yBa7RGVUnJa_Q1VHnSi2eGWRaClNm9fSGDwYsTIisGnbw_IZc9ufqQHdpYvB3re9viAlGhbtABSuQb_18OF9Kp7W9owdRWkeFuvgCXRmdKlQkA7gHVD62VEXwqOiKxIsHpgq9u6O0DoiJud6kNAMC_jdmvUe12g7k14OOscn34tl4QAICA_I2ZG9AHCckBc7U0ZJtRvmiAhKS9Z1Gg-","VIDEO_QUARTILE_100":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=Z4sXk8QGIS_N_P_yHXJxT24h1miGUlLBjqbdF.GqvA.bWte67WctsTyp1DzKj_dzFXfgaTseuvDbMx7BUwt3laaKD6kA1GsPYZdTEOCDziKvtgCY4Ttrti5.nsSQ3SGBAl5t2s4G0byFj2Z.cyN5tGfsfPmtwRe2feVstdhZneU9Z4iFmLQvLeUdek_1CuG3nlYy09I0.XHW3QXXgaUggoG0n3jzt0.B2JJ5QD2fdHzYnchZKQQA3IXLue8J0xGGMV8B2HfhZgBYGD9eJpLoWabP_xfe9HEIIKC3D2iwXPJqlDN7W2_Uq.qWaLWOl_yDiuCaxJ08jCs-&vsa=$(V_SKIP_AVAIL)&va=$(V_AUTOPLAYED)&vph=$(V_PLAYER_HEIGHT)&vpw=$(V_PLAYER_WIDTH)&ve=$(V_EXPANDED)&vpi=$(V_VIEW_INFO)&api=$(V_AUD_INFO)&atv=$(V_AUD_TIME_INVIEW_100)","VIDEO_QUARTILE_25":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=lNztPx4GIS_8Olnlry.6Zw33F94vLsdfzXvZcERUVKIRC5RLy3XCnRU6xwRHHqwG7HqvmMv_xb1bE0ey3vM.8GC3Vg0MKKEev.nY5ob.cHWUgRsT648fYnNSq8EBFvBIUES0sapW79Gpco.yap0MUYgxycyjAyyTqjMseczLD3hg0JeqcYS2row8JdI7rq2H4dcWRdC0SY2KNDisAyTVFCxC96SoxNGMWAFRXQvjT6qGfpBWC0qzZx8Xq03JPLtNMgOBaJVQ.jwCLz0Is86kW_.2BO8iTU4izcgOFC9B2JwFKlDY9qVcVg--&vsa=$(V_SKIP_AVAIL)&va=$(V_AUTOPLAYED)&vph=$(V_PLAYER_HEIGHT)&vpw=$(V_PLAYER_WIDTH)&ve=$(V_EXPANDED)&vpi=$(V_VIEW_INFO)&api=$(V_AUD_INFO)&atv=$(V_AUD_TIME_INVIEW_100)","VIDEO_QUARTILE_50":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=AO5BaRgGIS_7V44g6ZStnmm1qaBARpaG4w5rnm7U7KG._0Xt1QyYXEj6yxAsRtuFB4cvCUxcrEw_QIzEhkL2W_9qfmpRvrQSzCdaMLct6VDRIOhVqpaQOz4D_6JyICX5vyNq3voEleu477nxYDcaouCEnxzYiocysE4B0P6y.6lGQx.2Ucf8UyCZg_UWWQyZaJrRwOCkQebBYlDk6zOx_7L2Ny_fkSt5tDvN4EXxP17c_OBAeXv_tqkVJgriAuRud0ElI4rbOHZlt7.OuE6FH3W0gPkoyDGwCAv.tya64Y60.qJvVBr.pQ--&vsa=$(V_SKIP_AVAIL)&va=$(V_AUTOPLAYED)&vph=$(V_PLAYER_HEIGHT)&vpw=$(V_PLAYER_WIDTH)&ve=$(V_EXPANDED)&vpi=$(V_VIEW_INFO)&api=$(V_AUD_INFO)&atv=$(V_AUD_TIME_INVIEW_100)","VIDEO_QUARTILE_75":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=Cosd4cEGIS9PLwkiaNzgS13pkx1NKx8DKGQ0epjITKIy7MHDu6lMlhGclvxOnKfJ_63CyxBKUKqiQNPqYiKIqoUFzM00aq2NpatAvsfAAxSACLok7JNQ4RU85kHrGoINnLJ83im8DpE3DuUzpaAYrUxchkEFe1hXke3sAR7cvPc5rESm5l2ihtrrh6bcBtUsMKesa_JGXIZRNkzZl9PoAaSaNyjtUhgcmOQIhA4sq48fV7y8lQebt1lQC3CSgupMDMQMCUiYwy4ozyq.5njXu1mm823XbCjd09L55_tVcN4dlLJF6sVyqA--&vsa=$(V_SKIP_AVAIL)&va=$(V_AUTOPLAYED)&vph=$(V_PLAYER_HEIGHT)&vpw=$(V_PLAYER_WIDTH)&ve=$(V_EXPANDED)&vpi=$(V_VIEW_INFO)&api=$(V_AUD_INFO)&atv=$(V_AUD_TIME_INVIEW_100)","VIDEO_SKIP":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=VZvkb1sGIS_liVvbWMe0Ya7cCXqXOU3Qi9kgPkCBpxES9aqepuGIvL8C2Z2dIFY4PWdewKUsDyUBEeAfUbIkgOJS4NP9GX9Rp3YIZzlAJncGlfyedKD4m_VMr0rleGQLTbZlC5lYEH6zkDxuo_bGub7anKe_2bqZAc6oMf7KvSDJHkIKLUyQKTuuZkou0LccrOxkqAfvQefb63QT8fBPuHH0YCQvIuaLCzuQNoLcSNkEbUAc9HL.ZhiEIqvg.pbms0J33omJUnVZ60yuNP_NK.yMkuqnsmDjLer4.x0VLI9vJB40653k4Is-","VIDEO_START":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=yaY9pxEGIS_c.JOe1HJRX9wfXRICIGk8rABX8U0f.wPN6Gx9nSSPQ9IzNDlkDGe0WuNn1CupKUXLr4o4dOH5INymlReky2oMKaV3dcVuNTdwMX5riH2.awAMJ_HJeskxJo91dTg8ifivAP_WPV6ATmV.QX3K2svmgCq7RJxFACMg5JUTuDoBPsfu4vj8zyWeep9qytj_eEEskMcJPHeBN.iXJ2JY151IY8YN8IfGHKKsurlED0jNYDw..1rK7jAXviKPChsQi0ETdIl1obltcxF6AqDZjToSpe1ZV7Lx7UM0QMjzQVysrA--&vsa=$(V_SKIP_AVAIL)&va=$(V_AUTOPLAYED)&vph=$(V_PLAYER_HEIGHT)&vpw=$(V_PLAYER_WIDTH)&ve=$(V_EXPANDED)&vpi=$(V_VIEW_INFO)&api=$(V_AUD_INFO)&atv=$(V_AUD_TIME_INVIEW_100)","VIDEO_VIEW":"https://beap.gemini.yahoo.com/action?bv=1.0.0&es=yaPbLaUGIS9w8_ncqy0Hn6gM12MIoTQEyzt4vAqcxl_QEGwIeBeNGRn9QSdJOnrm1iXnRLbGKR98zBCoR2U2iXuDqZso.ABzqothOaAEEXj1svtX6GVYHSEBwKNSlNLcm8L0zDNSAhshIpC7hvyC6PU0uU6gg1IyJm47CkE1np2DDcW45_ftUMUfKg255.FWUcpHd0UWq42pXVen97lhEtUYkbkHf1nJBmqUn50VCuZeqIbqurAyHMbqwB5b1cqTg2GQi8EoCsiJjFLMJ4gISsNmYd6TlKeDCyrhRpRT0gICu_wFVqEXqRoQY6Ou5q9II.9SkKgqSNSw78gog._uSGUbn2Bt8g--&vsa=$(V_SKIP_AVAIL)&va=$(V_AUTOPLAYED)&vph=$(V_PLAYER_HEIGHT)&vpw=$(V_PLAYER_WIDTH)&ve=$(V_EXPANDED)&atv=$(V_AUD_TIME_INVIEW_100)"},"video_ad_assets":{"usageType":"VIDEO_PRIMARY","mediaInfo":{"url":"https://c-1b5026950897a5be365f7725914df201.http.atlas.cdn.yimg.com/gemini/pr/video_4rjX7F8jWcSqotygP-h1LspY1y1hdk9Xys0zqwkp6SLSG26a6ncuCD_fy65FjZnr6Pp7yEjLw7U-_4.mp4?a=gemini&mr=0&s=2c2f6898e2857a8fff36733edcab690b","contentType":"video/mp4","height":"270","width":"480","bitrate":"470","length":"74"}},"image":{"url":"/sy/uu/api/res/1.2/3gcNV_XtqdYVLCvHVc3KBQ--/Zmk9c3RyaW07aD0yMTQ7cHlvZmY9MDtxPTgwO3c9MzgwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://s.yimg.com/av/moneyball/ads/1458127951192-8173.jpg.cf.jpg","width":"190","height":"107","defer":0}},{"id":"91140e98-8f83-3058-bb7a-cf99e366e9d1","i13n":{"cpos":4,"cposy":8,"bpos":1,"pos":1,"ss_cid":"33c27b12-07d3-4ab5-9853-99a35f669397","refcnt":2,"imgt":"ss","g":"91140e98-8f83-3058-bb7a-cf99e366e9d1","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"World"},"link":"http://www.ibtimes.com/north-korea-calls-its-multiple-launch-rocket-system-horrible-nightmare-us-says-2344018","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"38d40b80-7f4d-3626-aeac-11f6997952a3","link":"http://www.huffingtonpost.com/doug-bandow/how-to-convince-china-to_b_9554496.html?ncid=txtlnkusaolp00000592","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"47501b6f-fe32-3248-a903-87b1dab4bfa2","link":"http://mashable.com/2016/03/26/north-korea-destroys-washington-propaganda-video/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+Mashable+%28Mashable%29","type":"unknown","viewer_eligible":true,"viewer_fetch":false}]},{"id":"087d92e4-b248-3998-afc9-fce7b0bbf815","i13n":{"cpos":5,"cposy":11,"bpos":1,"pos":1,"ss_cid":"040721b0-8948-48be-a55e-a3bd66583e2c","refcnt":2,"imgt":"ss","g":"087d92e4-b248-3998-afc9-fce7b0bbf815","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Entertainment"},"link":"http://nypost.com/2016/03/27/protesters-disrupt-easter-mass-at-st-pats-frighten-worshipers/","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"ba09fd07-77c0-32fa-b9e3-b8f9d4c53a15","link":"http://www.chicagotribune.com/suburbs/daily-southtown/news/ct-sta-easter-oak-forest-st-0328-20160327-story.html","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"48bb1f57-fece-3ba0-989b-e0e467ebef5f","link":"http://news.yahoo.com/video/queen-attends-traditional-easter-mass-163250618.html","type":"unknown","viewer_eligible":true,"viewer_fetch":true}]},{"id":"f31f237f-922f-3c3a-81e9-d2f96c277346","i13n":{"cpos":6,"cposy":14,"bpos":1,"pos":1,"subsec":2208,"imgt":"ss","g":"f31f237f-922f-3c3a-81e9-d2f96c277346","ct":1,"pkgt":"orphan_img","grpt":"storyCluster","cnt_tpc":"U.S."},"link":"http://news.yahoo.com/never-call-her-again-daughter-133319884.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"a426e21e-770e-3815-96d7-f7c400a825e1","i13n":{"cpos":7,"cposy":15,"bpos":1,"pos":1,"subsec":535,"imgt":"ss","g":"a426e21e-770e-3815-96d7-f7c400a825e1","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"U.S."},"link":"http://finance.yahoo.com/news/us-agents-nearly-caught-194-191227843.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"31834348738","i13n":{"cpos":8,"cposy":16,"bpos":1,"pos":1,"ad":1,"elmt":"ad","g":"31834348738","ct":1,"pkgt":"sponsored_img","grpt":"singlestory"},"link":"https://beap.gemini.yahoo.com/mbclk?bv=1.0.0&es=sWrxzggGIS8J6IOt2wxOpz8P.I5dMeMa2NoiZ5nkahCqnNe29z.si1c5.vp1G08is.sO_E0rblr_631sEjvmS7r51SwOWF029NRttLHy9VLjTH.BEz6PHmRiLU44QsZTy_yQnqWICKr8quZ3engPGW1XG4G63cDk0qdgZqDbykR7vV.cjSHf54NrOsV1G2F9eKM.TCXi9Pp2laf71LHCEE8WYabxWAWMycNtYuKiV4k4DJIhfyfqHYVqjXvx5Znxl102q46i9I.FWCYg3j3BfO7Wb_XlcaqIP8Gaxeboo3pPLEBAqhbYdPQYGv7GEh9wmEToqKvVTI8SiGJiyFqhMrFzS2p5D6ijiyPWU1IeGgecY069n32d9gtB21rf9cdpEsKrRYK2UsCf4e_am2k8rTTHGCVY9qDqDt3r2EUcwNpvxpsLoZRAOhThQrEtBiaJbBDtN_3.dhs14g3g2A0ANF_hHRLb1HsndLRdRZt5wRGPL_H0aFD_Hi4WtcL1HyGPC1I-%26lp=http%3A%2F%2Ftrack.fbytemedia.com%2Ff237962d-3d60-4fc4-9627-5445dad3618f%3Fad%3D3","type":"ad"},{"id":"4a468fa6-203d-33df-b889-843ac9ea20e2","i13n":{"cpos":9,"cposy":17,"bpos":1,"pos":1,"subsec":387,"imgt":"ss","g":"4a468fa6-203d-33df-b889-843ac9ea20e2","ct":1,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Science"},"link":"http://news.yahoo.com/cavemans-best-friends-preserved-ice-age-puppies-awe-052035084.html","type":"article","viewer_eligible":true,"viewer_fetch":true},{"id":"72e6998d-d33e-3f67-91e1-c771f4cfd89b","i13n":{"cpos":10,"cposy":18,"bpos":1,"pos":1,"ss_cid":"69d340bc-91d0-4e0d-ac9b-38f81c5a6bfd","refcnt":2,"imgt":"ss","g":"72e6998d-d33e-3f67-91e1-c771f4cfd89b","ct":1,"pkgt":"cluster_all_img","grpt":"storyCluster","cnt_tpc":"Politics"},"link":"http://www.cnn.com/2016/03/27/politics/ted-cruz-could-win-more-delegates-than-donald-trump-in-louisiana/index.html","type":"article","viewer_eligible":true,"viewer_fetch":false,"storyline":[{"id":"ad00743c-5742-3b74-bcf1-96903e5a50e2","link":"http://nation.foxnews.com/2016/03/27/cruz-trump-war-wives-new-campaign-low-underscores-donalds-problem-female-voters","type":"unknown","viewer_eligible":true,"viewer_fetch":false},{"id":"1a775ef3-2024-334f-b5ca-80f5938ee101","link":"https://gma.yahoo.com/donald-trump-blames-ted-cruz-spat-leading-national-150622479.html","type":"article","viewer_eligible":true,"viewer_fetch":true}]},{"id":"52858f64-611c-39ee-9e0d-9957abfabd79","i13n":{"cpos":11,"cposy":21,"bpos":1,"pos":1,"subsec":9,"imgt":"ss","g":"52858f64-611c-39ee-9e0d-9957abfabd79","ct":4,"pkgt":"orphan_img","grpt":"singlestory","cnt_tpc":"Style"},"link":"http://news.yahoo.com/photos/10-leather-tote-bags-help-143131428/","type":"slideshow","viewer_eligible":true,"viewer_fetch":true}],"fetched":11,"interest_data":{"WIKIID:Nathan_Deal":{"name":"Nathan Deal"},"WIKIID:LGBT_community":{"name":"LGBT community"},"WIKIID:Belief":{"name":"religious beliefs"},"YCT:001000780":{"name":"Society & Culture"},"YCT:001000661":{"name":"Politics & Government"},"YCT:001000681":{"name":"Government"},"WIKIID:Jon_Lovitz":{"name":"Jon Lovitz"},"WIKIID:Jessica_Lowndes":{"name":"Jessica Lowndes"},"WIKIID:Twitter":{"name":"Twitter"},"YCT:001000031":{"name":"Arts & Entertainment"},"YCT:001000069":{"name":"Celebrities"},"YCT:001000075":{"name":"Media"},"YCT:001000705":{"name":"Military"},"YCT:001000680":{"name":"Foreign Policy"},"WIKIID:Jacob_Martin":{"name":"Jacob Martin"},"YCT:001000713":{"name":"Unrest, Conflicts & War"},"YCT:001000804":{"name":"Religion"},"YCT:001000717":{"name":"Political Demonstrations"},"WIKIID:Tammy_Miller":{"name":"Tammy Miller"},"YCT:001000288":{"name":"Relationships"},"WIKIID:United_States_Customs_Service":{"name":"United States Customs Service"},"WIKIID:Semi-submersible":{"name":"Semi-submersible"},"WIKIID:Narco-submarine":{"name":"Narco-submarine"},"WIKIID:Pacific_Ocean":{"name":"Pacific Ocean"},"YCT:001000667":{"name":"Crime & Justice"},"WIKIID:Sergei_Fyodorov":{"name":"Sergei Fyodorov"},"YCT:001000742":{"name":"Science, Social Science, & Humanities"},"WIKIID:Donald_Trump":{"name":"Donald Trump"},"WIKIID:Ted_Cruz":{"name":"Ted Cruz"},"YCT:001000671":{"name":"Elections"},"YCT:001000931":{"name":"Technology & Electronics"}},"bnbData":{"title":"Washington Post editor discusses the presidential candidatesâ feuds","id":"c-ac50db7f-d825-487a-854e-d3ef8ae96df8","type":"breakingNewsBar","followId":"60f73942-c8f9-11e5-bc86-fa163e798f6a","followable":true,"userFollowing":false,"prefix":"Coming up","severity":"medium","followName":"Presidential primaries","viewer_fetch":true,"viewer_eligible":true,"article_uuid":"cbc30066-4a69-3a93-b95c-e1c494d29db0","link":"https://www.yahoo.com/katiecouric/donald-trumps-latest-jabs-at-ted-cruz-145642696.html"}}},"items":{"yui_module":"td-applet-stream-items-model-v2","yui_class":"TD.Applet.StreamItemsModel2"},"applet_model":{"yui_module":"td-applet-stream-appletmodel-v2","yui_class":"TD.Applet.StreamAppletModel2","config":{"ads":{"adchoices_text":true,"adchoices_url":"https://info.yahoo.com/privacy/us/yahoo/relevantads.html","ad_polices":true,"advt_text":false,"count":25,"contentType":"","fallback":0,"feedback":true,"frequency":4,"inline_video":true,"pu":"www.yahoo.com","se":4250754,"related_ct_se":"5454650","related_ct_single_ad":true,"related_dedupe_ads":true,"spaceid":"2023538075","sponsored_url":"http://help.yahoo.com/kb/index?page=content&y=PROD_FRONT&locale=en_US&id=SLN14553","start_index":1,"timeout":0,"type":"STRM,STRM_CONTENT,STRM_VIDEO","version":2,"endpoint":"","related_start_index":3},"category":"","js":{"attribution_pos":"bottom","click_context":false,"content_events":true,"filters":false,"full_content_event":false,"pluck_item_fields":"id,cauuid,payoffId,events,i13n,link,type,videoUuid,viewer_eligible,viewer_fetch,video_ad_beacons,video_ad_assets","pluck_today_fields":"image,title,summary,publisher,more_link_text","poll_interval":60000,"related_collapse":true,"related_content":true,"related_count":4,"restore_filter":false,"restore_state":false,"restore_state_storage":"history","show_read":true,"sticker":false,"sticker_toptarget":"","summary_pos":"left","xhr_ss_timeout":500,"stories_like_this":0,"login_url":"https://login.yahoo.com/config/login?.intl=us&.lang=en-US&.src=fpctx&.done="},"linkSupplement":null,"onboarding":{"count":3,"debug":false,"pos":-1,"result":3,"topic":false},"pagination":true,"remoteConfig":{"endpoint":"/_td_remote","params":{"disableAppClass":true,"footer":false,"i13n":{"auto":false},"noAbbreviation":true,"rendermode":"hovercard"},"passLocdropCrumb":true,"type":"td-applet-weather"},"request_xhtrace":0,"signed_in":false,"ss_endpoint":"ga-hr.slingstone.yahoo.com@score/v9/homerun/en-US/unified/mega","ss_timeout":300,"template":{"main":"td-applet-stream-atomic:main","drawer":"td-applet-stream-atomic:drawer_desktop","drawer_action":"td-applet-stream-atomic:drawer_action","drawer_share":"td-applet-stream-atomic:drawer_share"},"timeout":800,"total":170,"ui":{"backfill_property_region":false,"big_click_target":true,"bleed":false,"bnb_enabled":true,"bnb_link_out":false,"cat_label_enabled":true,"comments":true,"comments_allow_supression":true,"comments_inline":true,"comments_inline_ranking":"highestRated","comments_offnet":false,"comments_editorial_suppression":true,"comments_request_count":5,"comments_update":true,"comments_update_maxidle":300000,"comments_update_monitoring":true,"comments_update_throttle":1750,"comments_writes_enabled":true,"followable":true,"followable_signedout":true,"dislike":false,"dollar_sign":true,"editorial_edition":{"label":"EDITION_DEFAULT","top":true,"shouldUpdateLVtimeInCookie":true},"enable_featured_ad_video_gap":true,"enable_hovercolor":true,"enable_stream_notify":false,"enrich_tweet":0,"enrichment":{"types":"","unique":false},"entity_max":9,"exclude_types":"","exclusive_type":"","fauxdal":true,"featured_align":"left","featured_delayed_defer":false,"featured_percent_width":45,"featured_width":460,"featured_height":258,"featured_width_portrait":200,"featured_height_portrait":200,"first_featured_treatment":true,"fixed_layout":false,"fptoday_thumbs_sparse":false,"headline_wrapper":true,"hq_ad_template":"featured_ad","i13n_key_tar_qa":false,"image_quality_override":true,"incremental_count":0,"incremental_delay":300,"incremental_history":100,"inline_filters":false,"inline_filters_article_min":10,"inline_filters_max":6,"inline_video":true,"item_template":"items","like":true,"limit_summary_height":true,"link_to_finance":false,"sticker_top":"94px","mms":false,"needtoknow_actions":true,"needtoknow_template":"filmstrip","needtoknow_storyline_min":4,"ntk_1_link_out":false,"ntk_2_link_out":false,"ntk_3_link_out":false,"ntk_4_link_out":false,"ntk_5_link_out":false,"ntk_allow_no_cauuid":true,"ntk_bypassA3c":false,"off_network_tab":false,"open_in_tab":false,"defaultDrawerAction":{"title":"ACTION_DEFAULT","description":"Content preferences","url":"https://settings.yahoo.com/interests"},"payoffDrawerActions":{"local":{"title":"ACTION_LOCAL","description":"More local news","urlPrefix":"https://news.yahoo.com/local/"},"finance":{"title":"ACTION_FINANCE","description":"Manage portfolios","url":"https://finance.yahoo.com/portfolios"},"sports":{"actionsEnabled":false,"attributionEnabled":false,"title":"ACTION_SPORTS","description":"Edit my teams","url":"https://sports.yahoo.com/myteams"}},"payoffs":"","payoffExpTime":24,"payoffFinanceArticleCount":2,"payoffInterval":6,"payoffInvalidTime":0.5,"payoffSportsLocalTeams":false,"payoffLocalArticleCount":3,"payoffLocalHeadliner":true,"payoffStartIndex":1,"pcsExclusions":false,"preview_enabled":false,"preview_lcp":true,"pubtime_format":"ONE_UNIT_ABBREVIATED","pubtime_maxage":3600,"ribbon_headers":false,"sfl":false,"sfl_get_started_string":"SFL_LINK","scrollbuffer":900,"scroll_node_selector":"body","show_tooltips":true,"smart_crop":true,"stateful_subtle_hint":true,"storyline_cap_image":2,"storyline_cap_noimage":3,"storyline_count":2,"storyline_elapsed_time":true,"storyline_elapsed_time_threshold":720,"storyline_enabled":true,"storyline_items_to_show":2,"storyline_multi_image":true,"storyline_multi_image_roundup":true,"storyline_newsy_disabled":false,"storyline_upsell_count":0,"storyline_upsell_image":"img:190x107|2|80","storyline_upsell_index":3,"stream_actions":true,"stream_actions_minimal":true,"stream_actions_reblog":true,"stream_actions_share":false,"stream_actions_share_panel":true,"stream_actions_likable":true,"stream_actions_tumblr":true,"stream_debug":false,"stream_payoff_actions":false,"stream_item_maxwidth":"none","stream_item_image_fallback":"https://s.yimg.com/dh/ap/default/151015/stream-gradient-230x130.png","stream_item_large_image_fallback":"https://s.yimg.com/dh/ap/default/150902/stream-gradient.png","stream_single_item_image_fallback":"https://s.yimg.com/dh/ap/default/150903/stream-gradient-single-3.png","summary":true,"summary_length":160,"template_label":"js-stream-dense","template_suffix":"","today_count":5,"today_snippet_count":5,"fc_related_minCount":2,"today_featured_image_tag":"pc:size=medium","tap_for_summary":false,"templates":{"all":{"batch_max":20,"gap":0,"max":170,"start":1,"batch":2,"total":2,"last":7},"featured":{"batch_max":20,"gap":0,"max":170,"portrait":false,"start":0,"batch":0,"total":0},"featured_ad":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":2,"total":2,"last":7},"inline_video":{"batch_max":20,"gap":0,"max":170,"start":0,"batch":0,"total":0}},"thumbnail":true,"thumbnail_align":"right","thumbnail_hover":true,"thumbnail_size":160,"thumbnail_specs":{"featured":"img:190x107|2|80","featured_square":"img:190x190|2|80","inline_video":"img:190x107|2|80","items":"img:190x107|2|80","items_large":"img:190x190|2|80","storyline_square":"img:70x70|2|90","related":"img:165x120|2|80","featured_roundup":"img:702x274|1|95","roundup_wide":"img:170x80|2|80"},"toJpg":true,"truncate_summary":false,"tweet_action":true,"uis_inferred_finance":false,"ult_count":10,"ult_host":"hsrd.yahoo.com","ult_links":false,"use_ss_roundup_slot":true,"view_article_button":false,"viewer":true,"viewer_include_all":true,"viewer_off_network":false,"viewer_reblog":false,"viewer_action":true,"visit_state_threshold":1800000,"preview":0,"related_host":"","stream_payoff_v2":1,"stream_payoff_vizify":0,"stream_rc4":0,"show_jump_to_historical_items":0,"item_templates":["items"]},"weather":false,"woeid":7153327,"tz":"Europe/Paris","xcc":"fr"},"settings":{"size":9,"woeid":7153327},"models":["stream","items","related"]},"interest":{"yui_module":"td-applet-interest-model-v2","yui_class":"TD.Applet.InterestModel2"},"comments":{"yui_module":"td-applet-comments-model-v2","yui_class":"TD.Applet.CommentsModel2"},"related":{"yui_module":"td-applet-stream-related-model-atomic","yui_class":"TD.Applet.StreamRelatedModelAtomic"}},"views":{"main":{"yui_module":"td-applet-stream-mainview-v2","yui_class":"TD.Applet.StreamMainView2"},"drawer":{"yui_module":"stream-actiondrawer-v2"}},"templates":{"main":{"yui_module":"td-applet-stream-atomic-templates-main","template_name":"td-applet-stream-atomic-templates-main"},"related":{"yui_module":"td-applet-stream-atomic-templates-related","template_name":"td-applet-stream-atomic-templates-related"},"drawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_desktop","template_name":"td-applet-stream-atomic-templates-drawer_desktop"},"removedItem":{"yui_module":"td-applet-stream-atomic-templates-removeditem","template_name":"td-applet-stream-atomic-templates-removeditem"},"drawerAction":{"yui_module":"td-applet-stream-atomic-templates-drawer_action","template_name":"td-applet-stream-atomic-templates-drawer_action"},"drawerSharePanel":{"yui_module":"td-applet-stream-atomic-templates-drawer_share","template_name":"td-applet-stream-atomic-templates-drawer_share"},"breakingNews":{"yui_module":"td-applet-stream-atomic-templates-breaking_news","template_name":"td-applet-stream-atomic-templates-breaking_news"},"flyoutDrawer":{"yui_module":"td-applet-stream-atomic-templates-drawer_flyout","template_name":"td-applet-stream-atomic-templates-drawer_flyout"},"items":{"yui_module":"td-applet-stream-atomic-templates-items","template_name":"td-applet-stream-atomic-templates-items"},"item_default":{"yui_module":"td-applet-stream-atomic-templates-item-default","template_name":"td-applet-stream-atomic-templates-item-default"},"item_default_clusters":{"yui_module":"td-applet-stream-atomic-templates-item-default_clusters","template_name":"td-applet-stream-atomic-templates-item-default_clusters"},"item_ad":{"yui_module":"td-applet-stream-atomic-templates-item-ad","template_name":"td-applet-stream-atomic-templates-item-ad"},"item_ad_dislike":{"yui_module":"td-applet-stream-atomic-templates-item-ad_dislike","template_name":"td-applet-stream-atomic-templates-item-ad_dislike"},"item_featured_ad":{"yui_module":"td-applet-stream-atomic-templates-item-featured_ad","template_name":"td-applet-stream-atomic-templates-item-featured_ad"},"item_featured":{"yui_module":"td-applet-stream-atomic-templates-item-featured","template_name":"td-applet-stream-atomic-templates-item-featured"},"item_inline_video":{"yui_module":"td-applet-stream-atomic-templates-item-inline_video","template_name":"td-applet-stream-atomic-templates-item-inline_video"}},"i18n":{"AD_FDB_HEADING":"Why don't you like this ad?","AD_FDB_TELL_US":"Tell us why","AD_FDB_THANKYOU":"Thank you for your feedback. We will remove this and make the changes needed.","AD_FDB1":"It is offensive to me","AD_FDB2":"It is not relevant to me","AD_FDB3":"I keep seeing this","AD_FDB4":"Something else","ABCLOGO":"ABC","ACTION_MORE_LIKE_THIS":"Got it! Tell us more about what you like:","ACTION_FEWER_LIKE_THIS":"Got it! Tell us more about what you dislike:","ACTION_DEFAULT":"Content preferences Â»","ACTION_SPORTS":"Edit my teams Â»","ACTION_FINANCE":"Manage portfolios Â»","ACTION_LOCAL":"More local news Â»","ACTION_STORY_REMOVED":"Story removed","ACTION_SEE_LESS":"Okay. You'll see fewer like this.","ACTION_SEE_MORE":"Great! You'll see more like this.","ACTION_SEE_NO_MORE":"This item has been removed from your stream.","ACTION_SIGN_IN_TO_DISLIKE":"{linkstart}Sign-in{linkend} and we'll show you less like this in the future.","ACTION_SIGN_IN_TO_DISLIKE_TUMBLR":"{linkstart}Sign-in{linkend} to dislike","ACTION_SIGN_IN_TO_LIKE":"{linkstart}Sign-in{linkend} and we'll show you more like this in the future.","ACTION_SIGN_IN_TO_LIKE_SIMPLE":"Sign in to like","ACTION_SIGN_IN_TO_LIKE_TUMBLR":"{linkstart}Sign-in{linkend} to like","ACTION_SIGN_IN_TO_PERSONALIZE":"{linkstart}Sign-in{linkend} to personalize!","ACTION_SIGN_IN_TO_SAVE":"{linkstart}Sign-in{linkend} to save this story to read later.","ACTION_SIGN_IN_TO_SAVE_SHORT":"Sign in to save","ACTION_TELL_LIKE":"Tell us more about what you like:","ACTION_TELL_DISLIKE":"Tell us more about what you dislike:","ACTION_MORE_LESS":"Show more or less of","ACTION_OPTIONS":"Options","ACTION_REMOVE":"Remove from my stream","ACTION_STORIES_LIKE_THIS":"Stories like this","ACTION_OPEN":"Open","ACTION_CLOSE":"Close","ADCHOICES":"AdChoices","ADVERTISEMENT":"Ad","ALL_ARTICLES":"All Articles","ALL_CELEBRITY":"All Celebrity News","ALL_FINANCE":"All Finance","ALL_MOVIES":"All Movies News","ALL_MUSIC":"All Music News","ALL_NEWS":"All News","ALL_SPORTS":"All Sports","ALL_STORIES":"All Stories","ALL_TRAVEL":"All Travel Ideas","ALL_TV":"All TV News","AROUND_THE_WEB":"Around The Web","ALSO_LIKE":"You may also like","ASTRO_GO_TO":"Go to Horoscopes Â»","ASTRO_NAME_AQUARIUS":"Aquarius","ASTRO_NAME_ARIES":"Aries","ASTRO_NAME_CANCER":"Cancer","ASTRO_NAME_CAPRICORN":"Capricorn","ASTRO_NAME_GEMINI":"Gemini","ASTRO_NAME_LEO":"Leo","ASTRO_NAME_LIBRA":"Libra","ASTRO_NAME_PISCES":"Pisces","ASTRO_NAME_SAGITTARIUS":"Sagittarius","ASTRO_NAME_SCORPIO":"Scorpio","ASTRO_NAME_TAURUS":"Taurus","ASTRO_NAME_VIRGO":"Virgo","ASTRO_TITLE":"Today's overview","AUCTION":"Action","AUTHOR_AT_PUBLISHER":"{author} at {publisher}","BREAKING_NEWS":"BREAKING NEWS","CNBCLOGO":"","COMMENT_ARTICLE_SIGN_IN":"Sign in to perform this action.","COMMENTS":"Comments","COMMENTS_ZERO":"Start the conversation","CONTENT_PREF":"Content preferences","DISLIKE":"Dislike","DONE":"Done","DYNAMIC_FILTERS":"You Might Like","EDITION_DEFAULT":"Need To Know","FEATURED_MOVIE":"Featured Movie","FLAGGED_COMMENT":"Flagged as inappropriate. Thanks for your feedback.","FOLLOW":"Follow","FOLLOW_GAME":"Follow Game","FOLLOWING":"Following {name}","FOLLOWING_STORY":"Following","FOLLOW_UPDATES":"Follow to receive updates on","FULL_HOROSCOPE":"Full Horoscope","GAME_PERIOD:baseball":"Innings","GAME_PERIOD:football":"Quarters","GAME_PERIOD_1":"1st","GAME_PERIOD_2":"2nd","GAME_PERIOD_3":"3rd","GAME_PERIOD_4":"4th","GAME_COVERAGE":"See Full Coverage","GAME_FINAL":"Final","GAME_RECAP":"Game Recap","GAME_TITLE":"{away_team} vs. {home_team}","HALFTIME":"Halftime","HOROSCOPE":"Horoscope","IN_THEATERS_NOW":"In Theaters Now","IN_THEATERS_TODAY":"In Theaters Today","INSTALL_APP":"Install now","IN_WATCHLIST":"In watchlist","IS_IN_FAVORITES":"is in your favorites","JUMP_TO_HISTORICAL_ITEMS":"Go to where you left off","LATEST_NEWS":"Latest News","LESS":"Less","LIKABLE_ARTICLE_SIGN_IN":"Sign in to save your preferences.","LIKE":"Like","LIKE_THIS_TOPIC":"Like this topic","LIVE":"Live","LIVE_GAME":"Live","LOAD_MORE":"Load more stories","LOCAL":"Local","LOCAL_BLOWING_SNOW":"Blowing Snow","LOCAL_BLUSTERY":"Blustery","LOCAL_CELSIUS":"Â°C","LOCAL_CLEAR":"Clear","LOCAL_CLOUDY":"Cloudy","LOCAL_COLD":"Cold","LOCAL_DRIZZLE":"Drizzle","LOCAL_DUST":"Dust","LOCAL_FAIR":"Fair","LOCAL_FOGGY":"Foggy","LOCAL_FAHRENHEIT":"Â°F","LOCAL_FREEZING_DRIZZLE":"Freezing Drizzle","LOCAL_FREEZING_RAIN":"Freezing Rain","LOCAL_GO_TO":"Go to Weather Â»","LOCAL_HAIL":"Hail","LOCAL_HAZE":"Haze","LOCAL_HEAVY_SNOW":"Heavy Snow","LOCAL_HOT":"Hot","LOCAL_HURRICANE":"Hurricane","LOCAL_ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","LOCAL_ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LOCAL_LIGHT_SNOW_SHOWERS":"Light Snow Showers","LOCAL_MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","LOCAL_MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","LOCAL_MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","LOCAL_MOSTLY_CLOUDY":"Mostly Cloudy","LOCAL_NEWS":"Local news","LOCAL_NO_STORIES":"Sorry, there are no news articles related to this location. Try setting your location to the nearest large city for more stories.","LOCAL_PARTLY_CLOUDY":"Partly Cloudy","LOCAL_SCATTERED_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","LOCAL_SCATTERED_THUNDERSTORMS":"Scattered Thunderstorms","LOCAL_SEVERE_THUNDERSTORMS":"Severe Thunderstorms","LOCAL_SHOWERS":"Showers","LOCAL_SLEET":"Sleet","LOCAL_SMOKY":"Smoky","LOCAL_SNOW":"Snow","LOCAL_SNOW_FLURRIES":"Snow Flurries","LOCAL_SNOW_SHOWERS":"Snow Showers","LOCAL_SUNNY":"Sunny","LOCAL_THUNDERSHOWERS":"Thundershowers","LOCAL_THUNDERSTORMS":"Thunderstorms","LOCAL_TITLE":"{city} News","LOCAL_TORNADO":"Tornado","LOCAL_TROPICAL_STORM":"Tropical Storm","LOCAL_UNKNOWN":"Unknown Conditions","LOCAL_WINDY":"Windy","MIDFIELD":"midfield","MLB_BALLS":"Balls","MLB_ERRORS":"Errors","MLB_HITS":"Hits","MLB_OUTS":"Outs","MLB_RUNS":"Runs","MLB_STRIKES":"Strikes","MORE":"More","MORE_FROM_ROUNDUP":"More from this Roundup","MORE_FROM_TOPIC":"More from {topic} news","MYQUOTES_ADD_MORE":"Add more investments to your {0}Portfolio{1} to stay up to date whenever you visit Yahoo Finance.","MYQUOTES_LOGIN":"{0}Sign-in{1} to see the latest news from the investments in your portfolio.","MYTEAMS_ADD_TEAMS":"{0}Add your favorite teams{1} to start getting news about them today.","MYTEAMS_LOGIN":"{0}Sign-in{1} to get news for your favorite teams.","MYTEAMS_NO_CONTENT":"We can't find recent news for your teams. {0}Edit your teams{1} or visit the {2}All Sports news{3}","MYSAVES":"My Saves","NEW_SINCE_YOUR_LAST_VISIT":"New since your last visit","NEW_STORIES":"New for you","NEW_STORIES_COUNT":"View {new_item_count} new {new_item_count, plural, one {story} other {stories}}","NEXT":"Next","NFL_PROGRESS":"on {field_side} {yard_line}","NO_STORIES_HEADER":"We couldn't find any new stories for you.","NO_STORIES_BODY":"Please check back later or {0}try again{1}","OFF":"OFF","OFFNETWORK":"Read this on {0}","ONBOARDING_CONFIRMATION_INITIAL":"Got it! Almost there","ONBOARDING_CONFIRMATION_ONE":"Got it! One more time","ONBOARDING_CONFIRMATION_ZERO":"Got it! Youâre done","ONBOARDING_TITLE1_FINAL":"Here are some stories based on your preferences","ONBOARDING_TITLE1_INITIAL":"Which would you rather read?","ONBOARDING_TITLE1_ONE":"Now which do you prefer?","ONBOARDING_TITLE1_ZERO":"Awesome! What is your final pick?","OPENS_TODAY":"Opens Today","PORTFOLIO":"Go to your portfolio","PORTFOLIO_GAINERS":"Gainers","PORTFOLIO_LOSERS":"Losers","PORTFOLIO_MARKET_CLOSED":"Market close","PLAY":"Play","PLAY_VIDEO":"Play Video","PLAYING":"playing","PLAYING_NEAR_YOU":"Playing near you","PREVIEW_GAME":"Preview Game","PREVIOUS":"Previous","PREVIOUSLY_VIEWED":"From your last visit","PROGRESS":"{possession_team} {down} & {to_go} at {field_side} {yard_line}","READ_MORE":"Read More","REBLOG":"Reblog on Tumblr","REMOVE":"Remove","SAVE":"Save","SCORE":"Score!","SCORE_ATTRIBUTION":"Click here to track the {display_name} &rarr;","SEE_ALL_STORIES":"See all stories Â»","SEE_DETAILS":"See details Â»","SEE_MORE_STORIES":"See more stories Â»","SEE_FULL_BOXSCORE":"See full boxscore Â»","SFL_HEADER":"Hi {0}, you have no saves. Here's how to get started:","SFL_LINK":"Get started now on the {0}Yahoo Homepage{1}.","SFL_LINK_ATT":"Get started now on the {0}att.net Homepage{1}.","SFL_LINK_FRONTIER":"Get started now on the {0}Frontier Yahoo Homepage{1}.","SFL_LINK_ROGERS":"Get started now on the {0}Rogers Yahoo Homepage{1}.","SFL_LINK_VERIZON":"Get started now on the {0}Verizon Yahoo Homepage{1}.","SFL_STEP_ONE":"Step 1","SFL_STEP_TWO":"Step 2","SFL_TITLE_ONE":"Click on {0} in the stream and on articles across Yahoo to save stories for later.","SFL_TITLE_TWO":"Access your saves from the profile menu {0} on desktop & tablet or menu {1} on your smartphone.","SHARE_THIS":"SHARE THIS","SHOPPING":"Shopping","SIGN_IN":"Sign in!","SIGN_IN_2":"Sign in","SIGN_IN_FOR_MORE":"Looking for past stories?","SIGN_IN_TO_SEE_MORE_TEAMS":" to save your preferences. You'll see more of this team next time you visit.","SLIDESHOW_COUNT":"({0} photos)","SLIDESHOW_PREVIEW_COUNT":"1 of {0}","SPONSORED":"Sponsored","STATUS":"{time} {period}","STORE":"Store","STORIES":"Stories:","STORIES_ABOUT":"Stories about:","STORYLINE_NEXT":"Next","STORYLINE_PREVIOUS":"Previous","STORYLINE_UPSELL":"Follow these developing stories to get updates when you come back to Yahoo","SYNOPSIS":"Synopsis","TIME_AGO":"ago","TIME_DAY":"day","TIME_H":"h","TIME_M":"m","TITLE":"Recommended for You","TITLE_LOCAL":"{city} News","TO":"to","TWITTER_REPLY":"Reply","TWITTER_RETWEET":"Retweet","TWITTER_FAVORITE":"Favorite","UNFOLLOW":"Unfollow","UNDO":"Undo","UPCOMING_GAME":"Today at {game_time}","UP_OVER":"Up over","DOWN_OVER":"Down over","VIEW":"View","VIEW_ALL":"View All","VIEW_SLIDESHOW":"View Slideshow","VIEW_FULL_ARTICLE":"VIEW FULL ARTICLE","VIEW_FULL_QUOTE":"View full quote Â»","YAHOO_MOVIES":"Yahoo Movies","YAHOO_ORIGINALS":"Yahoo Originals","YCT:001000931":"Technology","YCUSTOM:COMMERCE":"Commerce","YCUSTOM:MYQUOTES":"My Quotes","YCUSTOM:MYTEAMS":"My Teams","YLABEL:autos":"Autos","YLABEL:business":"Business","YLABEL:beauty":"Beauty","YLABEL:celebrity":"Celebrity","YLABEL:diy":"DIY","YLABEL:entertainment":"entertainment","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:lifestyle":"Lifestyle","YLABEL:makers":"Projects","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:politics":"Politics","YLABEL:science":"science","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:technology":"Technology","YLABEL:travel":"Travel","YLABEL:trending":"Trending","YLABEL:tv":"TV","YLABEL:us":"US","YLABEL:world":"World","YPROP:FINANCE":"Business","YPROP:TOPSTORIES":"All Stories","YPROP:LOCAL":"Local","YPROP:NEWS":"News","YPROP:OMG":"Entertainment","YPROP:SCIENCE":"Science","YPROP:SPORTS":"Sports","YPROV:ABCNEWS":"News","YPROV:ap.org":"AP","YPROV:CNBC":"CNBC","YPROV:NBCSPORT":"NBC Sports","YPROV:reuters.com":"Reuters","YPROV:ROLLINGSTONES":"Rolling Stone","YPROV:us.sports.yahoo.com":"Experts","YTYPE:VIDEO":"Video","YPROP:STYLE":"Style","YMAG:food":"Food","YMAG:tech":"Tech","YMAG:travel":"Travel"},"i13n":{"pv":2,"pp":{"ccode_st":"mega_global_ranking_hlv2_up_based"}},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-trending-atomic":{"base":"/sy/os/mit/td/td-applet-trending-atomic-0.0.50/","root":"os/mit/td/td-applet-trending-atomic-0.0.50/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_32209491"] = {"applet_type":"td-applet-trending-atomic","views":{"main":{"yui_module":"td-applet-trending-atomic-mainview","yui_class":"TD.Applet.TrendingAtomicMainView","config":{"clickableTitles":true,"rotationEnabled":true,"rotationInterval":10000,"giftsEnabled":false}}},"templates":{"main":{"yui_module":"td-applet-trending-atomic-templates-main","template_name":"td-applet-trending-atomic-templates-main"}},"i18n":{"GIFTS_TITLE":"Easter Searches","TITLE":"Trending","TRENDING_NEWS":"Trending News","TRENDING_NOW":"Trending Now"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-weather-atomic":{"base":"/sy/os/mit/td/td-applet-weather-atomic-0.0.25/","root":"os/mit/td/td-applet-weather-atomic-0.0.25/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_63794"] = {"applet_type":"td-applet-weather-atomic","models":{"applet_model":{"yui_module":"td-applet-weather-atomic-appletmodel","yui_class":"TD.Applet.WeatherAppletModel","models":["weather"],"data":{"i13n":{"sec":"app-wea","bucket":"201"},"current":7153327,"favorite":null,"useplus":false},"user_settings":{"unit":"f","curloc":"7153327"},"config":{"moreinfo":{"link":"http://www.weather.com/?par=yahoonews","img":"http://l.yimg.com/dh/ap/default/130915/wcl1.gif"},"enableFav":false,"enableWeatherYQLP":true,"baseLink":"https://weather.yahoo.com","showWidgetLink":true,"urlGeneratorRules":{"enabled":false,"url_format_local":"","url_format_international":"","url_format_local_ajax":"","local_country_code":[]},"daysLimit":4,"fixed_layout":false}},"weather":{"yui_module":"td-applet-weather-atomic-model","yui_class":"TD.Applet.WeatherModel","data":{"weatherDataList":[{"location":{"woeid":"7153327","city":"Poitou-Charentes","country":"FR"},"link":{"href":"https://weather.yahoo.com/fr/state/poitou-charentes-7153327/"},"timezone":"CEST","woeid":"7153327","current":{"temp":{"unit":"f","now":56},"condition":{"description":" Scattered Thunderstorms","code":39,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png"}}}},"forecast":{"day":[{"woeid":7153327,"condition":{"description":"Showers","code":12,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/rain_day_night.png"}}},"timezone":"Europe/Paris","temp":{"high":57,"low":47,"unit":"f"},"label":"Today","day_of_week":"1"},{"woeid":7153327,"condition":{"description":"Mostly Cloudy","code":28,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/mostly_cloudy_day_night.png"}}},"timezone":"Europe/Paris","temp":{"high":58,"low":44,"unit":"f"},"label":"Tue","day_of_week":"2"},{"woeid":7153327,"condition":{"description":"Mostly Cloudy","code":28,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/mostly_cloudy_day_night.png"}}},"timezone":"Europe/Paris","temp":{"high":64,"low":47,"unit":"f"},"label":"Wed","day_of_week":"3"},{"woeid":7153327,"condition":{"description":" Scattered Thunderstorms","code":39,"images":{"thumb":{"url":"/sy/os/mit/ape/w/d8f6e02/dark/scattered_showers_day_night.png"}}},"timezone":"Europe/Paris","temp":{"high":52,"low":45,"unit":"f"},"label":"Thu","day_of_week":"4"}]},"accordianState":"e"}],"currentLoc":{"woeid":"7153327","city":"Poitou-Charentes","country":"FR"}}}},"views":{"main":{"yui_module":"td-applet-weather-atomic-mainview","yui_class":"TD.Applet.WeatherMainView"},"header":{"yui_module":"td-applet-weather-atomic-headerview","yui_class":"TD.Applet.WeatherHeaderView","config":{}}},"templates":{"main":{"yui_module":"td-applet-weather-atomic-templates-main","template_name":"td-applet-weather-atomic-templates-main"}},"i18n":{"CURRENT_LOCATION":"Current Location","WEATHER":"Weather","HIGH":"High","LOW":"Low","TODAY":"Today","TOMORROW":"Tomorrow","SUNDAY":"Sunday","MONDAY":"Monday","TUESDAY":"Tuesday","WEDNESDAY":"Wednesday","THURSDAY":"Thursday","FRIDAY":"Friday","SATURDAY":"Saturday","ABBR_SUNDAY":"Sun","ABBR_MONDAY":"Mon","ABBR_TUESDAY":"Tue","ABBR_WEDNESDAY":"Wed","ABBR_THURSDAY":"Thu","ABBR_FRIDAY":"Fri","ABBR_SATURDAY":"Sat","BLOWING_SNOW":"Blowing Snow","BLUSTERY":"Blustery","CLEAR":"Clear","CLOUDY":"Cloudy","COLD":"Cold","DRIZZLE":"Drizzle","DUST":"Dust","FAIR":"Fair","FREEZING_DRIZZLE":"Freezing Drizzle","FREEZING_RAIN":"Freezing Rain","FOGGY":"Foggy","FORECAST":"Forecast","FULL_FORECAST":"Full forecast","HAIL":"Hail","HAZE":"Haze","HEAVY_SNOW":"Heavy Snow","HOT":"Hot","HURRICANE":"Hurricane","ISOLATED_THUNDERSHOWERS":"Isolated Thundershowers","ISOLATED_THUNDERSTORMS":"Isolated Thunderstorms","LIGHT_SNOW_SHOWERS":"Light Snow Showers","MIXED_RAIN_AND_HAIL":"Mixed Rain and Hail","MIXED_RAIN_AND_SLEET":"Mixed Rain and Sleet","MIXED_RAIN_AND_SNOW":"Mixed Rain and Snow","MIXED_SNOW_AND_SLEET":"Mixed Snow and Sleet","MOSTLY_CLOUDY":"Mostly Cloudy","PARTLY_CLOUDY":"Partly Cloudy","SCATTERED_SHOWERS":"Scattered Showers","SCATTERED_SNOW_SHOWERS":"Scattered Snow Showers","SCATTERED_THUNDERSTORMS":" Scattered Thunderstorms","SEARCH_CITY":"Kindly search your city","SEVERE_THUNDERSTORMS":"Severe Thunderstorms","SHOWERS":"Showers","SLEET":"Sleet","SMOKY":"Smoky","SNOW":"Snow","SNOW_FLURRIES":"Snow Flurries","SNOW_SHOWERS":"Snow Showers","SUNNY":"Sunny","THUNDERSHOWERS":"Thundershowers","THUNDERSTORMS":"Thunderstorms","TORNADO":"Tornado","TROPICAL_STORM":"Tropical Storm","WINDY":"Windy","WEATHER_CHICLET_ERROR":"Visit {linkstart}Yahoo Weather{linkend} for a detailed forecast.","GOTO_WEATHER":"See more Â»","NO_WEATHER_MESSAGE":"Visit {linkstart}Yahoo Weather{linkend} for the latest forecast or {detectstart}click here{detectend} to select your precise location.","TEN_DAY":"10 day"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-scores-atomic":{"base":"/sy/os/mit/td/td-applet-scores-atomic-1.0.10/","root":"os/mit/td/td-applet-scores-atomic-1.0.10/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_63796"] = {"applet_type":"td-applet-scores-atomic","models":{"scores":{"yui_module":"td-scores-atomic-modellist","yui_class":"TD.Scores.ModelList","data":[{"gameid":"mlb.g.360328123","comet_channel":"/sports/games/mlb.g.360328123","start_time":"Mon, 28 Mar 2016 17:05:00 +0000","start_ts":1459184700000,"last_update":"2016-03-28 09:37:28","lastUpdated":"2016-03-28 09:37:28","lastUpdatedTs":"1459183048","status":{"type":"pregame","label":"1:05 pm ET","link":{"href":"https://sports.yahoo.com/mlb/minnesota-twins-pittsburgh-pirates-360328123/"},"base_runners":{"first":"","second":"","third":"","offset":null},"game_stats":{"home_team":{"runs":"","hits":"","errors":""},"away_team":{"runs":"","hits":"","errors":""},"current_period":{"balls":"","strikes":"","outs":""}}},"league":{"id":"mlb","abbr":"MLB","link":{"href":"https://sports.yahoo.com/mlb/"}},"home_team":{"id":"mlb.pit","sportsid":"mlb.t.23","first_name":"Pittsburgh","last_name":"Pirates","display_name":"Pittsburgh","abbr":"PIT","link":{"href":"https://sports.yahoo.com/mlb/teams/pit/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/hH1KFOCft37n5vEYz7WFPQ--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/pit.png"}}},"away_team":{"id":"mlb.min","sportsid":"mlb.t.9","first_name":"Minnesota","last_name":"Twins","display_name":"Minnesota","abbr":"MIN","link":{"href":"https://sports.yahoo.com/mlb/teams/min/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/ipt/min_nonWhite_70x70.png"}}},"seq_num":"1","seq":"1","tv_coverage":{"channels":["RTPT"]},"fav":"false","region":"","relativeTime":"in 27 minutes","startDay":"Mar 28","absoluteTime":"3/28 4:38 PM UTC","icon":"IconScoresMlb"},{"gameid":"mlb.g.360328102","comet_channel":"/sports/games/mlb.g.360328102","start_time":"Mon, 28 Mar 2016 17:05:00 +0000","start_ts":1459184700000,"last_update":"2016-03-28 09:37:21","lastUpdated":"2016-03-28 09:37:21","lastUpdatedTs":"1459183041","status":{"type":"pregame","label":"1:05 pm ET","link":{"href":"https://sports.yahoo.com/mlb/baltimore-orioles-boston-red-sox-360328102/"},"base_runners":{"first":"","second":"","third":"","offset":null},"game_stats":{"home_team":{"runs":"","hits":"","errors":""},"away_team":{"runs":"","hits":"","errors":""},"current_period":{"balls":"","strikes":"","outs":""}}},"league":{"id":"mlb","abbr":"MLB","link":{"href":"https://sports.yahoo.com/mlb/"}},"home_team":{"id":"mlb.bos","sportsid":"mlb.t.2","first_name":"Boston","last_name":"Red Sox","display_name":"Boston","abbr":"BOS","link":{"href":"https://sports.yahoo.com/mlb/teams/bos/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/udeRnIoJ1umOWq4EBwLh1g--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/bos.png"}}},"away_team":{"id":"mlb.bal","sportsid":"mlb.t.1","first_name":"Baltimore","last_name":"Orioles","display_name":"Baltimore","abbr":"BAL","link":{"href":"https://sports.yahoo.com/mlb/teams/bal/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/hup4V3rUE93lhosKYEfWnA--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/bal.png"}}},"seq_num":"1","seq":"1","tv_coverage":{"channels":["ESPN"]},"fav":"false","region":"","relativeTime":"in 27 minutes","startDay":"Mar 28","absoluteTime":"3/28 4:38 PM UTC","icon":"IconScoresMlb"},{"gameid":"mlb.g.360328124","comet_channel":"/sports/games/mlb.g.360328124","start_time":"Mon, 28 Mar 2016 17:05:00 +0000","start_ts":1459184700000,"last_update":"2016-03-28 04:10:31","lastUpdated":"2016-03-28 04:10:31","lastUpdatedTs":"1459163431","status":{"type":"pregame","label":"1:05 pm ET","link":{"href":"https://sports.yahoo.com/mlb/new-york-mets-st-louis-cardinals-360328124/"},"base_runners":{"first":"","second":"","third":"","offset":null},"game_stats":{"home_team":{"runs":"","hits":"","errors":""},"away_team":{"runs":"","hits":"","errors":""},"current_period":{"balls":"","strikes":"","outs":""}}},"league":{"id":"mlb","abbr":"MLB","link":{"href":"https://sports.yahoo.com/mlb/"}},"home_team":{"id":"mlb.stl","sportsid":"mlb.t.24","first_name":"St. Louis","last_name":"Cardinals","display_name":"St. Louis","abbr":"STL","link":{"href":"https://sports.yahoo.com/mlb/teams/stl/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/pCfG0fYWcjONmUUeVNsg7A--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/stl.png"}}},"away_team":{"id":"mlb.nym","sportsid":"mlb.t.21","first_name":"New York","last_name":"Mets","display_name":"NY Mets","abbr":"NYM","link":{"href":"https://sports.yahoo.com/mlb/teams/nym/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/L0TvpYyHNca2hLv.5N1Wuw--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/nym.png"}}},"seq_num":"1","seq":"1","tv_coverage":{"channels":["FSMW"]},"fav":"false","region":"","relativeTime":"in 27 minutes","startDay":"Mar 28","absoluteTime":"3/28 4:38 PM UTC","icon":"IconScoresMlb"},{"gameid":"mlb.g.360328120","comet_channel":"/sports/games/mlb.g.360328120","start_time":"Mon, 28 Mar 2016 17:05:00 +0000","start_ts":1459184700000,"last_update":"2016-03-28 09:21:39","lastUpdated":"2016-03-28 09:21:39","lastUpdatedTs":"1459182099","status":{"type":"pregame","label":"1:05 pm ET","link":{"href":"https://sports.yahoo.com/mlb/miami-marlins-washington-nationals-360328120/"},"base_runners":{"first":"","second":"","third":"","offset":null},"game_stats":{"home_team":{"runs":"","hits":"","errors":""},"away_team":{"runs":"","hits":"","errors":""},"current_period":{"balls":"","strikes":"","outs":""}}},"league":{"id":"mlb","abbr":"MLB","link":{"href":"https://sports.yahoo.com/mlb/"}},"home_team":{"id":"mlb.was","sportsid":"mlb.t.20","first_name":"Washington","last_name":"Nationals","display_name":"Washington","abbr":"WAS","link":{"href":"https://sports.yahoo.com/mlb/teams/was/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/7v74wzyG7loC9TD05mbMHA--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/was.png"}}},"away_team":{"id":"mlb.mia","sportsid":"mlb.t.28","first_name":"Miami","last_name":"Marlins","display_name":"Miami","abbr":"MIA","link":{"href":"https://sports.yahoo.com/mlb/teams/mia/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/nHinqaJ6E4O4WLAw6KVJdg--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/mia.png"}}},"seq_num":"1","seq":"1","tv_coverage":null,"fav":"false","region":"","relativeTime":"in 27 minutes","startDay":"Mar 28","absoluteTime":"3/28 4:38 PM UTC","icon":"IconScoresMlb"},{"gameid":"mlb.g.360328114","comet_channel":"/sports/games/mlb.g.360328114","start_time":"Mon, 28 Mar 2016 17:07:00 +0000","start_ts":1459184820000,"last_update":"2016-03-28 09:24:49","lastUpdated":"2016-03-28 09:24:49","lastUpdatedTs":"1459182289","status":{"type":"pregame","label":"1:07 pm ET","link":{"href":"https://sports.yahoo.com/mlb/philadelphia-phillies-toronto-blue-jays-360328114/"},"base_runners":{"first":"","second":"","third":"","offset":null},"game_stats":{"home_team":{"runs":"","hits":"","errors":""},"away_team":{"runs":"","hits":"","errors":""},"current_period":{"balls":"","strikes":"","outs":""}}},"league":{"id":"mlb","abbr":"MLB","link":{"href":"https://sports.yahoo.com/mlb/"}},"home_team":{"id":"mlb.tor","sportsid":"mlb.t.14","first_name":"Toronto","last_name":"Blue Jays","display_name":"Toronto","abbr":"TOR","link":{"href":"https://sports.yahoo.com/mlb/teams/tor/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/jPz_e971v9bjpfhXnBgIhg--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/tor.png"}}},"away_team":{"id":"mlb.phi","sportsid":"mlb.t.22","first_name":"Philadelphia","last_name":"Phillies","display_name":"Philadelphia","abbr":"PHI","link":{"href":"https://sports.yahoo.com/mlb/teams/phi/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/iu/api/res/1.2/eOKlKRA2_9oz1bSP1PGrOQ--/YXBwaWQ9c2hhcmVkO2ZpPWZpbGw7aD03MDtxPTEwMDt3PTcw/http://l.yimg.com/xe/i/us/sp/v/mlb/teams/83/70x70/phi.png"}}},"seq_num":"1","seq":"1","tv_coverage":null,"fav":"false","region":"","relativeTime":"in 29 minutes","startDay":"Mar 28","absoluteTime":"3/28 4:38 PM UTC","icon":"IconScoresMlb"},{"gameid":"mlb.g.360328107","comet_channel":"/sports/games/mlb.g.360328107","start_time":"Mon, 28 Mar 2016 20:05:00 +0000","start_ts":1459195500000,"last_update":"2016-03-28 04:10:31","lastUpdated":"2016-03-28 04:10:31","lastUpdatedTs":"1459163431","status":{"type":"pregame","label":"4:05 pm ET","link":{"href":"https://sports.yahoo.com/mlb/san-diego-padres-kansas-city-royals-360328107/"},"base_runners":{"first":"","second":"","third":"","offset":null},"game_stats":{"home_team":{"runs":"","hits":"","errors":""},"away_team":{"runs":"","hits":"","errors":""},"current_period":{"balls":"","strikes":"","outs":""}}},"league":{"id":"mlb","abbr":"MLB","link":{"href":"https://sports.yahoo.com/mlb/"}},"home_team":{"id":"mlb.kan","sportsid":"mlb.t.7","first_name":"Kansas City","last_name":"Royals","display_name":"Kansas City","abbr":"KC","link":{"href":"https://sports.yahoo.com/mlb/teams/kan/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/ipt/kan_nonWhite_70x70.png"}}},"away_team":{"id":"mlb.sdg","sportsid":"mlb.t.25","first_name":"San Diego","last_name":"Padres","display_name":"San Diego","abbr":"SD","link":{"href":"https://sports.yahoo.com/mlb/teams/sdg/"},"images":{"scores_team_logo":{"width":"70","height":"70","url":"https://s.yimg.com/xe/i/us/sp/v/mlb/teams/20131113/84/500x500/sdg_70x70.png"}}},"seq_num":"1","seq":"1","tv_coverage":null,"fav":"false","region":"","relativeTime":"in 3 hours","startDay":"Mar 28","absoluteTime":"3/28 4:38 PM UTC","icon":"IconScoresMlb"}],"config":{"enablePipe":true}},"applet_model":{"settings":{"league":"","count":6,"lang":"en-US","useSportacularLogos":true,"useWhiteSportacularLogos":true,"rendermode":"scorethin","timeout":300,"rapidStr":{"tabRapidStr":"t1:a4;t2:scrbrd;sec:scrbrd;elm:tab;elmt:day;itc:1;","dropdownRapidStr":"t1:a4;t2:scrbrd;sec:scrbrd;elm:itm;elmt:cat;itc:1;","gameRapidStr":"t1:a4;t2:scrbrd;sec:scrbrd;elm:img;elmt:game;itc:0;","moreRapidStr":"t1:a4;t2:scrbrd;sec:scrbrd;elm:itm;elmt:mr;itc:0;"},"showTennis":false},"config":{"sizes":[3,4,5,6,7,8,9,10],"scores_links":{"trending":"http://sports.yahoo.com/","my":"http://sports.yahoo.com/","mlb":"http://sports.yahoo.com/mlb/scoreboard","nba":"http://sports.yahoo.com/nba/scoreboard","ncaab":"http://sports.yahoo.com/college-basketball/scoreboard/","ncaaf":"http://sports.yahoo.com/college-football/scoreboard/","nfl":"http://sports.yahoo.com/nfl/scoreboard","nhl":"http://sports.yahoo.com/nhl/scoreboard","tennis":"http://sports.yahoo.com/tennis/scoreboard","soccer.l.fbgb":"http://sports.yahoo.com/soccer/premier-league/scoreboard/","soccer.l.fbchampions":"http://sports.yahoo.com/soccer/champions-league/scoreboard/","soccer.l.fbes":"http://sports.yahoo.com/soccer/la-liga/scoreboard/","soccer.l.fbde":"http://sports.yahoo.com/soccer/bundesliga/scoreboard/","soccer.l.fbit":"http://sports.yahoo.com/soccer/serie-a/scoreboard/","soccer.l.fbfr":"http://sports.yahoo.com/soccer/ligue-1/"},"states":{"not-live":{"interval":300000},"live":{"interval":30000}},"pipe":{"min_polling_interval":300000}},"models":["scores"],"state":{"range":"curr","teams":""}}},"views":{"main":{"yui_module":"td-scores-atomic-mainview","yui_class":"TD.Scores.MainView","config":{"auto_update":true}},"footer":{"yui_module":"td-scores-atomic-footerview","yui_class":"TD.Scores.FooterView"},"header":{"yui_module":"td-scores-atomic-headerview","yui_class":"TD.Scores.HeaderView"}},"templates":{"header":{"yui_module":"td-applet-scores-atomic-templates-header","template_name":"td-applet-scores-atomic-templates-header"},"main":{"yui_module":"td-applet-scores-atomic-templates-main.scorethin","template_name":"td-applet-scores-atomic-templates-main.scorethin"},"footer":{"yui_module":"td-applet-scores-atomic-templates-footer","template_name":"td-applet-scores-atomic-templates-footer"},"settings":{"yui_module":"td-applet-scores-atomic-templates-settings","template_name":"td-applet-scores-atomic-templates-settings"}},"i18n":{"SCOREBOARD":"Scoreboard","TRENDING":"Trending","MY_TEAMS":"My Teams","NFL":"NFL","MLB":"MLB","NBA":"NBA","NHL":"NHL","MLS":"MLS","NCAAF":"NCAAF","NCAAB":"NCAAB","TENNIS":"Tennis","TENNIS_SETS":"Sets","HIGHLIGHTS":"Highlights","TENNIS_SCHED_DATE":"Date","TENNIS_SCHED_TOURNAMENT":"Tournament","TENNIS_SCHED_LOCATION":"Location","TENNIS_SCHED_SURFACE":"Surface","TENNIS_SCHED_CHAMPION":"Champion","TENNIS_ROUND_1":"First Round","TENNIS_ROUND_2":"Second Round","TENNIS_ROUND_3":"Third Round","TENNIS_ROUND_4":"Fourth Round","TENNIS_ROUND_5":"Quarterfinals","TENNIS_ROUND_6":"Semifinals","TENNIS_ROUND_7":"Finals","TENNIS_SIDE1_BOXSCORE":"Side 1 Boxscore","TENNIS_SIDE2_BOXSCORE":"Side 2 Boxscore","TOURNAMENT_HASNT_STARTED":"Tournament hasn't started","TENNIS_LIVE":"LIVE","TENNIS_LOADING_MESSAGE":"Loading... Please Wait","TENNIS_ALL_GAMES":"All Games","TENNIS_ROUNDS_1_4":"Rounds 1-4","TENNIS_FINALS":"Finals","TENNIS_TBD":" - TBD","ALL_TOURNAMENTS":"All Tournaments Today","BRACKET_VIEW":"Bracket view","GTYPE_PLURAL_":"All Types","GTYPE_PLURAL_mens-singles":"Men's Singles","GTYPE_PLURAL_womens-singles":"Women's Singles","GTYPE_PLURAL_mens-doubles":"Men's Doubles","GTYPE_PLURAL_womens-doubles":"Women's Doubles","PREMIER_LEAGUE":"Premier League","CHAMPIONS_LEAGUE":"Champions League","LA_LIGA":"La Liga","BUNDESLIGA":"Bundesliga","SERIE_A":"Serie A","LIGUE_1":"Ligue 1","YESTERDAY":"Yesterday","TODAY":"Today","TOMORROW":"Tomorrow","LAST_WEEK":"Last Week","CURRENT_WEEK":"Current Week","NEXT_WEEK":"Next Week","MORE_SCORES":"More scores","ADD_TO_CALENDAR":"Add to Calendar","SET_FAVORITE_TEAMS":"Set favorite teams","FINAL":"Final","LIVE":"Live","DRAW":"Draw","FULLTIME":"Full Time","FULLTIME_ABBR":"FT","HALFTIME":"Half","HALFTIME_ABBR":"HT","POSTPONED":"Postponed","POSTPONED_ABBR":"P","OWN_GOAL_ABBR":"og","PENALTY_ABBR":"pen","NO_GAMES":"No Games Scheduled","WORLD_CUP":"World Cup","WOMENS_WORLD_CUP":"Women's World Cup","Monday":"Mon","Tuesday":"Tue","Wednesday":"Wed","Thursday":"Thu","Friday":"Fri","Saturday":"Sat","Sunday":"Sun","GO_TO_SPORTS":"Go to Sports","SEE_ALL":"See All","V":"v","WATCH_LATER":"Watch Later","WATCH_NOW":"Watch Now"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
YMedia.applyConfig({"groups":{"td-applet-activitylist-atomic":{"base":"/sy/os/mit/td/td-applet-activitylist-atomic-0.0.60/","root":"os/mit/td/td-applet-activitylist-atomic-0.0.60/","combine":true,"filter":"min","comboBase":"/sy/zz/combo?","comboSep":"&"}}});window.Af=window.Af||{};window.Af.bootstrap=window.Af.bootstrap||{};window.Af.bootstrap["p_50000171"] = {"applet_type":"td-applet-activitylist-atomic","templates":{"list":{"yui_module":"td-applet-activitylist-atomic-templates-list.tumblr","template_name":"td-applet-activitylist-atomic-templates-list.tumblr"}},"i18n":{"ACTION_POST":"Posted","FAVORITE":"favorite","FIND_OUT_WHY":"Find out why Â»","LIVE":"Live","REASON:BREAKING":"Breaking","REASON:COMMENTS":"{count, number, integer} Comments","REASON:EVERGREEN":"","REASON:EXCLUSIVE":"Exclusive","REASON:NEW":"New","REASON:NEW_ON":"New on {site}","REASON:ONLYONYAHOO":"Only on Yahoo","REASON:POPULARONSEARCH":"Popular on Search","REASON:READINGNOW":"{count, number, integer} Reading Now","REASON:SEARCHVISITS":"{count, number, integer} Search Visits","REASON:SHARES":"{count, number, integer} Shares","REASON:SOCIALVISITS":"{count, number, integer} Social Visits","REASON:VIDEOPLAYS":"{count, number, integer} Video Plays","REASON:VIEWS":"{count, number, integer} Views","REASON:VISITS":"{count, number, integer} Visits","REASON:WATCHINGNOW":"{count, number, integer} Watching Now","REASON:WATCHLIVE":"Watch Live","REPLY":"reply","RETWEET":"retweet","TIME_FORMAT":"h:mm A","TIME_FORMAT_SAME_DAY":"[Today]","TIME_FORMAT_NEXT_DAY":"[Tomorrow]","TIME_FORMAT_NEXT_WEEK":"dddd","TIME_FORMAT_ELSE":"MMM D","TITLE":"Only from Yahoo","TWEET_NAVIGATE":"navigate to tweet","VIA_TWITTER":"via Twitter","YLABEL:autos":"Autos","YLABEL:beauty":"Beauty","YLABEL:diy":"DIY","YLABEL:finance":"Finance","YLABEL:food":"Food","YLABEL:games":"Games","YLABEL:gma":"News","YLABEL:health":"Health","YLABEL:homes":"Homes","YLABEL:ivy":"Screen","YLABEL:movies":"Movies","YLABEL:music":"Music","YLABEL:news":"News","YLABEL:omg":"Celebrity","YLABEL:parenting":"Parenting","YLABEL:shopping":"Shopping","YLABEL:sports":"Sports","YLABEL:style":"Style","YLABEL:tech":"Tech","YLABEL:travel":"Travel","YLABEL:tv":"TV","YPROP:autos":"Yahoo Autos","YPROP:beauty":"Yahoo Beauty","YPROP:diy":"Yahoo DIY","YPROP:finance":"Yahoo Finance","YPROP:food":"Yahoo Food","YPROP:games":"Yahoo Games","YPROP:gma":"Yahoo News","YPROP:health":"Yahoo Health","YPROP:homes":"Yahoo Homes","YPROP:ivy":"Yahoo Screen","YPROP:makers":"Yahoo Makers","YPROP:movies":"Yahoo Movies","YPROP:music":"Yahoo Music","YPROP:news":"Yahoo News","YPROP:omg":"Yahoo Celebrity","YPROP:parenting":"Yahoo Parenting","YPROP:politics":"Yahoo Politics","YPROP:shopping":"Yahoo Shopping","YPROP:sports":"Yahoo Sports","YPROP:style":"Yahoo Style","YPROP:tech":"Yahoo Tech","YPROP:travel":"Yahoo Travel","YPROP:tv":"Yahoo TV","YPROP:yahoo":"Yahoo"},"transport":{"xhr":"/_td_api"},"context":{"bucket":"201","device":"desktop","lang":"en-US","region":"US","site":"fp"}};
    window.Af = window.Af || {};
    window.Af.config = window.Af.config || {};
    window.Af.config.spaceid = "2023538075";
    window.Af.context= {
        crumb : 'Z8XTmlDkX4x',
        guid : '',
        mcCrumb: 'Jmuunq4AAnJ',
        ucsCrumb: 'cGQx9OfRPbj',
        device: 'desktop',
        rid : 'fmuulqdbfinfk',
        default_page : 'p1',
        _p : 'p1',
        site : 'fp',
        lang : 'en-US',
        region : 'US',
        authed: 0,
        enable_dd : '',
        default_appletinit : 'viewport',
        locdrop_crumb: 'SWQ2MFpoV2dtQlU-', woeid: '7153327',
        ssl: 1,
        
        bucket: '201'
        
        
    };

    window.Af.config.transport = {
        crumbForGET: true,
        xhr: '/fpjs',
        consolidate: true,
        timeout: 6000
    };

    window.Af.config.onepush = {
        subscribeTimeout : 5000,
        subscribeMaxTries : 1,
        trackInitComet : true,
        trackLatency : true,
        latencySampleSize : 50,
        publicCometHost: 'https://comet.yahoo.com/comet',
        shutdown: false,
        trackShutdown: false
    };

    window.Af.config.beacon = {
        beaconUncaughtErr: true,
        sampleSizeUncaughtErr: 1,
        maxUncaughtErrCount: 5,
        beaconPageLoadPerf: false,
        sampleSize : 1,
        pathPrefix : '/_td_api/beacon',
        batch: false,
        batchInterval: 3
    };

    window.Af.config.pipe = {
        msg_throttle: 2000,
        err_maxstreak: 2
    };

    window.Af.config.td = {
        remote: '/_td_remote',
        xhr: '/_td_api'
    };

    
    YUI.namespace('Env.My.settings').context = {
        uh_js_file : '',
                videoAsyncEnabled: 0,
        videoplayerScriptElementId: 'videoplayerJs',
        videoplayerUrl: 'https://www.yahoo.com/sy/rx/builds/6.256.0.1456781727/en-us/videoplayer-nextgen-flash-min.js',
        videoAutoplay: 1,
        videoLooping: 0,
        videoForcedError: 0,
        videoFullscreen: 1,
        videoHtml5: 0,
        videoMinControls: 0,
        videoCmsEnv: 'prod',
        videoMustWatch: 1,
        videoMWSticky: 0,
        videoMute: 1,
        videoQosRate: 1,
        videoBuffering: 0,
        videoRelated: 0,
        videoYwaRate: 0,
        videoMaxLoops: 3,
        videoModeEnabled: 0,
        videoTiltbackModeEnabled: 0,
        videoSSButton: 0,
        videoPausescreen: 0,
        videoSingleVideo: 0,
        videoMaxInstances: 12,
        videoInitEvent: 'domready',
        videoExpName: 'advance',
        videoComscoreC4: 'US fp',
        videoplayerScrollThrottle: '200',
        
        themeCssEnabled : false,
        themeBgImageEnabled: false,
        transparentPixelImage: 'https://s.yimg.com/os/mit/media/m/base/images/transparent-95031.png',
        pageMessage: {
            type: '',
            msg: ''
        },
        servingBeaconUrl: 'https://bs.serving-sys.com/BurstingPipe/ActivityServer.bs',
        enablePerfBeacon: '0',
        test_id: '201',
        customizedEnabled: false,
        trendingNowOffScreen: 0
    };

    YUI.namespace('Env.Af.Perf').secondYUIUseStart = (new Date()).getTime();

    var yuiPreloadModules = ["type_advance_desktop_viewer","type_video_manager","type_idletimer","type_sdarotate","type_sda"];

    yuiPreloadModules = yuiPreloadModules.concat((function (appletTypes) {
        if (!appletTypes || appletTypes.length === 0) {
            return [];
        }
        var yui_modules = [],
            types = YMedia.Array.hash(appletTypes);
        YMedia.Object.each(window.Af.bootstrap, function (bootstrap, guid) {
            if (!types[bootstrap.applet_type]) {
                 return;
            }
            YMedia.Array.each(['models', 'views', 'templates'], function (type) {
                YMedia.Object.each(bootstrap[type], function (config, name) {
                    if (config.yui_module) {
                         yui_modules.push(config.yui_module);
                    }
                });
            });
        });
        return yui_modules;
    })([]));

    YMedia.use(yuiPreloadModules, function (Y, NAME) {
        YUI.namespace('Env.Af.Perf').secondYUIUseStop = (new Date()).getTime();
        var pageMessage = YUI.Env.My.settings.context.pageMessage,
        enablePerfBeacon = YUI.Env.My.settings.context.enablePerfBeacon,
        test_id = YUI.Env.My.settings.context.test_id,
        pausePerfBeacon = false,
        perfBeacon = {}
                ,
        rapidConfig = rapidPageConfig.rapidConfig,
        YWA_CF_MAP = rapidPageConfig.ywaCF,
        YWA_ACTION_MAP = rapidPageConfig.ywaActionMap,
        YWA_OUTCOME_MAP = rapidPageConfig.ywaOutcomeMap;
        if(YAHOO.i13n) {
            YAHOO.i13n.WEBWORKER_FILE = '/lib/metro/g/myy/rapidworker_1_2_0.0.3.js';
            YAHOO.i13n.SPACEID = '2023538075';
            YAHOO.i13n.TEST_ID = '201';
        }
        if (pageMessage.msg != '') {
            Y.Af.Message.show('body', {level: pageMessage.type, content: pageMessage.msg});
        }

        if (1 === 1) {
            Y.Lang.later(45000, null, function() {
                var failedModules  = Y.all("#myColumns .js-applet .App-loading");
                failedModules.each(function (module) {
                    module.setContent('Sorry! We are temporarily unable to load the content. Please refresh or try again later.');
                    module.removeClass('App-loading');
                    module.addClass('App-failed');
                });
            });
        }
        
        
        
        

        YUI.namespace('Env.Af.Perf').YMyAppCreateStart = (new Date()).getTime();
        YMedia.My.App = new Y.My.App(
            {                i13nConfig: {
                    rapid: rapidConfig,
                    rapidInstance: rapidPageConfig.rapidSingleInstance && YAHOO && YAHOO.i13n && YAHOO.i13n.rapidInstance || null,
                    ywaMaps: {
                        YWA_OUTCOME_MAP: YWA_OUTCOME_MAP,
                        YWA_CF_MAP: YWA_CF_MAP,
                        YWA_ACTION_MAP: YWA_ACTION_MAP
                    }
                },
                inlineViewer: 0,
                summaryView: 0,
                stickerTarget: "#SearchBar-Wrapper-Mini",
                magazineViewerEnabled: 1,
                viewerBlacklistDisable: 1,
                viewer: {
                    viewerAnimation: "slide",
                    pageAnimation: "zoom",
                    scrollTopAmount: 0,
                    highlanderMode: "mega-modal"
                }}
        );
        

        var firePerfBeacon = function(e) {
            var key, value, beaconNode, queryString = [];

            if (Y.Object.size(perfBeacon) < 1 || pausePerfBeacon) {
                return;
            }
            pausePerfBeacon = true;
            for (key in perfBeacon) {
                if (perfBeacon.hasOwnProperty(key)) {
                    queryString.push(key+'='+perfBeacon[key]);
                }
            }
            queryString.push('test='+test_id);
            beaconNode = Y.Node.create('<img src="myperf.php?'+queryString.join('&')+'" style="width:1px;height:1px;position:absolute;left:-900px;">');
            Y.one('body').append(beaconNode);
        };

        if (enablePerfBeacon == '1') {
            Y.one('window').once('scroll', firePerfBeacon);
            Y.Lang.later(10000, null, firePerfBeacon);

            YMedia.My.App.after('appletsinit', function (e) {
                if (pausePerfBeacon == true) {
                    return;
                }
                perfBeacon[e.applet.type] = (new Date()) - myYahoostartTime;
            });
        }
        YMedia.on('appletsinit', function (e) {
            
        });
        if (Y.IdleTimer) {
    Y.IdleTimer.start(parseInt(5400000,10));
    Y.IdleTimer.on(['idle','hidden'], function(e){
        if (e && e.type) {
            var isModalOpen = Y.one('html').hasClass('Reader-open'),
                location = Y.config.win.location,
                type = e.type,
                paramMap = {
                    idle: 'vi',
                    hidden: 'vh'
                },
                url = location.protocol + '//' + location.host,
                expires = new Date();

            if (paramMap[type]) {
                expires.setMinutes(expires.getMinutes() + parseInt(1,10));
                Y.Cookie.set('autorf', paramMap[type], {expires: expires, domain: location.hostname});

                if (isModalOpen) {
                    Y.config.win.location = url
                } else {
                    Y.config.win.location.replace(url);
                }
            }
        }
    });
}
    });
            });
     });
</script>
    <!-- bottom -->

    <!-- Comscore -->
            <!-- Begin comScore Tag -->
		<script>
		  var _comscore = _comscore || [];
		  _comscore.push({
		    c1: "2",
		    c2: "7241469",
		    c5: "2023538075",
		    c7: "https://www.yahoo.com/"
		  });
		  (function() {
		    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		    s.src = "/sy/lq/lib/3pm/cs_0.2.js";
		    el.parentNode.insertBefore(s, el);
		  })();
		</script>
		<noscript>
		  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=7241469&c7=https%3A%2F%2Fwww.yahoo.com%2F&c5=2023538075&cv=2.0&cj=1" />
		</noscript>
		<!-- End comScore Tag -->

    <!-- Nielsen -->
    

    <!-- Lighthouse  -->
    

    <!-- yaft -->
            <script type="text/javascript" src="/sy/zz/combo?/os/yaft/yaft-0.3.3.min.js&/os/yaft/yaft-plugin-aftnoad-0.1.3.min.js&os/yaft/yaft-plugin-harbeacon-0.1.3.min.js"> </script>        
        <script type="text/javascript">
            if (window.YAFT !== undefined) {
                var __yaftConfig = {
                    modules: ["UH","mega-uh","mega-topbar","applet_p_","ad-north-base","fea-","my-adsFPAD-base","my-adsLREC-base","my-adsMAST","my-adsTXTL","content-modal-","hl-ad-LREC-","modal-sidekick-","hl-ad-LREC-0","hl-ad-MON-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","hl-ad-SPL-0"], 
                    modulesExclude: ["UH-Search","UH-ColWrap","mega-uh-wrapper"],
                    canShowVisualReport: false,
                    useNormalizeCoverage: true,
                    generateHAR: false,
                    includeOnlyAft2: true,
                    useNativeStartRender : true,
                    modulesAft2Container: ["hl-viewer"],
                    maxWaitTime: 6000
                };
                __yaftConfig.plugins = [];                __yaftConfig.plugins.push({
                     name: 'aftnoad',
                     isPre: true,
                     config: {
                         useNormalizeCoverage: true,
                         adModules:["ad-north-base","my-adsFPAD-base","my-adsLREC-base","my-adsTL1","my-adsMAST","hl-ad-LREC-0","hl-ad-MAST-0","hl-ad-LDRB-0","hl-ad-SPL2-0","my-adsTXTL","hl-ad-SPL-0"]
                     }
                });
                            __yaftConfig.plugins.push({
                name: 'har',
                config: {
                    sec: 'har',
                    rapid: function() { if(YMedia && YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) {return YMedia.My.App.getRapidTracker(); } else {return null;}},
                    aftThreshold: 15000,
                    onlySlowest: false
                }
            });
            __yaftConfig.generateHAR = true;
                        if (window.LH && window.LH.isInitialized) {
            window.LH.tag('b',{val: '201'});               
            window.LH.tag("l", {val: false});
        }
                        window.aft2CB = function(data, error) {
        if (!error) {
            var aft2 = Math.round(data.aft);
            var vic2 = data.visuallyComplete;
            var srt2 = Math.round(data.startRender);

            if (window.LH && window.LH.isInitialized) {
                var lhRecord = function lhRecord(name, type, startTime, duration) {
                    window.LH.record(name, {
                        name: name, type: type, startTime: startTime, duration: duration
                    });
                };

                lhRecord('AFT', 'mark', aft2, 0);
                lhRecord('AFT2', 'mark', aft2, 0);
                lhRecord('VIC2', 'mark', vic2, 0);
                lhRecord('STR2', 'mark', srt2, 0);
                window.LH.beacon({
                    clearMarks:false,
                    clearMeasures: false,
                    clearCustomEntries: true,
                    clearTags: false
                });
            }

            var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
            if (rapidInstance) {
                var afterPageLoad = {
                    AFT: aft2,
                    AFT2: aft2,
                    STR: srt2,
                    VIC: vic2
                };
                var perfData = {
                    perf_commontime: {afterPageLoad: afterPageLoad}
                };
                var pageParamsObject = null;            if (rapidInstance.getRefererSpaceid) {
                pageParamsObject  = {
                    ref_sp: rapidInstance.getRefererSpaceid(),
                    visit_sp: (window.Af && window.Af.config && window.Af.config.spaceid)
                };
            }
                rapidInstance.beaconPerformanceData(perfData, pageParamsObject);
            }
        }
    };
                window.YAFT.init(__yaftConfig, function(data, error) {
                    if (!error) {
                        try {
                                    if (window.LH && window.LH.isInitialized) {
            var results = [0], module = '', index = '', lhRecord = '';
            for (module in data.modulesReport) {
                for (index in data.modulesReport[module].resources) {
                    results.push(Math.round(data.modulesReport[module].resources[index].durationFromNStart));
                }
            }
            lhRecord = function (ma, va, custom) {
                var res = '';                                
                if (undefined === custom) {
                    if (va && !isNaN(va)) {
                        res = Math.round(va);
                    } else {
                        res = 0;
                    }
                } else {
                    res = va;
                }
                    window.LH.record(ma, { name: ma, type: 'mark', startTime: res, duration: 0 });
            };
                            
            lhRecord('AFT', data.aft);
            lhRecord('AFT1', data.aft);
            if (data.aftNoAd) {
                lhRecord('AFTNOAD', data.aftNoAd);
            }
            lhRecord('X_FB1', 50);
            lhRecord('X_FBN', 81);
            lhRecord('PLT_CACHED_REQs', data.httpRequests.onloadCached);
            lhRecord('COSTLY_RESOURCE', Math.max.apply(null, results));
            lhRecord('StartRender', data.startRender);
            if (screen) {
                lhRecord('SCR_H', screen.height);
                lhRecord('SCR_W', screen.width);
            }
            if (window.FPAD_rendered) {
                lhRecord('xAFT', data.aft);
                lhRecord('xPLT', data.pageLoadTime);
                if (window.rtAdStart && window.rtAdDone) {
                    lhRecord('ADSTART_FPAD', Math.round(rtAdStart));
                    lhRecord('ADEND_FPAD', Math.round(rtAdDone));
                }
            }

            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    lhRecord(_adLT[i][0], _adLT[i][1]);
                }
            }
            window.LH.beacon({ clearMarks: false, clearMeasures: false, clearCustomEntries: true, clearTags: false });   
        }
                                    var rapidInstance = (YMedia.My && YMedia.My.App && YMedia.My.App.getRapidTracker) ? YMedia.My.App.getRapidTracker() : null;
        if(rapidInstance) {
            var initialPageLoad = {
                AFT: Math.round(data.aft),
                AFT1: Math.round(data.aft),
                STR: data.startRender,
                VIC: data.visuallyComplete,
                PLT: data.pageLoadTime,
                DOMC: data.domElementsCount,
                HTTPC: data.httpRequests.count,
                CP: data.totalCoveragePercentage,
                NCP: data.normTotalCoveragePercentage
            };

            if(data.aftNoAd) {
                initialPageLoad.AFTNOAD = Math.round(data.aftNoAd);
            }              

            var customPerfData = {},
                pagePerfData = {},
                results = [];
            
            var yaftResults = [0], yaftModule = '', yaftIndex = '';
            // Find costly resource time
            for (yaftModule in data.modulesReport) {
                for (yaftIndex in data.modulesReport[yaftModule].resources) {
                    yaftResults.push(Math.round(data.modulesReport[yaftModule].resources[yaftIndex].durationFromNStart));
                }
            }
            pagePerfData['COSTLY_RESOURCE'] = Math.max.apply(null, yaftResults);
            pagePerfData['X_FB1'] = 50;
            pagePerfData['X_FBN'] = 81; 
   
            // Log ad perf data to rapid perf metric
            if (window.FPAD_rendered) {
                pagePerfData['xAFT'] = data.aft;
                pagePerfData['xPLT'] = data.pageLoadTime;
                if (window.rtAdStart && window.rtAdDone) {
                    pagePerfData['ADSTART_FPAD'] = Math.round(rtAdStart);
                    pagePerfData['ADEND_FPAD'] = Math.round(rtAdDone);
                }
            }
            
            // Track ad metrics
            if ( window._adLT) {
                for (var i = 0; i< _adLT.length; i++) {
                    pagePerfData[_adLT[i][0]]  = _adLT[i][1];
                }
            }
                        if(resourceTimingAssets) {
                for(i in resourceTimingAssets) {
                    if (resourceTimingAssets.hasOwnProperty(i)) {
                        resourceName = window.performance.getEntriesByName(resourceTimingAssets[i]);
                        if(resourceName && resourceName.length) {
                            var resourceFinish = resourceName[0].responseEnd;
                            pagePerfData[i] = Math.round(resourceFinish);
                        }
                    }
                }
            }

            customPerfData['utm'] = pagePerfData;
            var perfData = {
                perf_commontime: {initialPageLoad: initialPageLoad},
                perf_usertime: customPerfData
            };
            rapidInstance.beaconPerformanceData(perfData);
        }
                        } catch (e) {}                                                        
                    }
               });
            }
      </script>

    <!-- Via https/1.1 ir14.fp.ir2.yahoo.com[D992B340] (YahooTrafficServer), http/1.1 usproxy2.fp.bf1.yahoo.com[448EE3A1] (YahooTrafficServer) -->
    <!-- sid=2023538075 -->
    

    </body>
</html>
<!-- myproperty:myservice-us:0:Success -->
<!-- slw72.fp.bf1.yahoo.com compressed/chunked Mon Mar 28 16:38:13 UTC 2016 -->
