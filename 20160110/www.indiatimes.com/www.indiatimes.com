<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.72" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.72" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.72"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.72"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.72"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.72"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.72"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0;
var isNewYear = 1;
</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle">
				</span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    
</div><!--pull-ad end-->
</div>
    <!--testing 16-01-10 23:50:02-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             13 hours ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/24-year-old-quits-comfy-ias-job-to-help-students-across-the-country-realise-their-dreams-249253.html" class=" tint" title="24-Year-Old Quits Comfy IAS Job To Help Students Across The Country Realise Their Dreams">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Jan/rs_1452401135_1452401143_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/rs_1452401135_1452401143_236x111.jpg"  border="0" alt="24-Year-Old Quits Comfy IAS Job To Help Students Across The Country Realise Their Dreams"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/24-year-old-quits-comfy-ias-job-to-help-students-across-the-country-realise-their-dreams-249253.html" title="24-Year-Old Quits Comfy IAS Job To Help Students Across The Country Realise Their Dreams">
                            24-Year-Old Quits Comfy IAS Job To Help Students Across The Country Realise Their Dreams                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            4 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/weird/american-company-invents-a-jeans-that-doesn-t-have-to-be-washed-ever-249278.html" title="American Company Invents A Jeans That Doesn't Have To Be Washed. Ever" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Jan/jeans_1452430490_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/jeans_1452430490_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/american-company-invents-a-jeans-that-doesn-t-have-to-be-washed-ever-249278.html" title="American Company Invents A Jeans That Doesn't Have To Be Washed. Ever">
                            American Company Invents A Jeans That Doesn't Have To Be Washed. Ever                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/nasa-does-it-again-discovers-100-new-alien-planets-249276.html" title="NASA Does It Again, Discovers 100 New Alien Planets!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Jan/planet34_1452428576_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/planet34_1452428576_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/nasa-does-it-again-discovers-100-new-alien-planets-249276.html" title="NASA Does It Again, Discovers 100 New Alien Planets!">
                            NASA Does It Again, Discovers 100 New Alien Planets!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/mumbai-man-drowns-saving-a-girl-s-life-after-she-falls-off-cliff-taking-selfies-249275.html" title="Mumbai Man Drowns Trying To Save A Girl Who Fell Off A Cliff Into The Sea While Taking Selfies" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Jan/wall-pix6_1452428444_1452428506_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/wall-pix6_1452428444_1452428506_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/mumbai-man-drowns-saving-a-girl-s-life-after-she-falls-off-cliff-taking-selfies-249275.html" title="Mumbai Man Drowns Trying To Save A Girl Who Fell Off A Cliff Into The Sea While Taking Selfies">
                            Mumbai Man Drowns Trying To Save A Girl Who Fell Off A Cliff Into The Sea While Taking Selfies                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            6 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/tourist-harassment-at-the-taj-mahal-is-so-bad-that-the-city-s-tourism-industry-is-suffering-heavily-249274.html" title="Tourist Harassment At the Taj Mahal Is So Bad That The City's Tourism Industry Is Suffering Heavily" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2016/Jan/ranopamasflickr6_1452428734_1452428742_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/ranopamasflickr6_1452428734_1452428742_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/tourist-harassment-at-the-taj-mahal-is-so-bad-that-the-city-s-tourism-industry-is-suffering-heavily-249274.html" title="Tourist Harassment At the Taj Mahal Is So Bad That The City's Tourism Industry Is Suffering Heavily">
                            Tourist Harassment At the Taj Mahal Is So Bad That The City's Tourism Industry Is Suffering Heavily                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

            

                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/recipes/make-this-low-carb-margarita-pizza-with-a-super-healthy-cauliflower-crust-249230.html'>video</a>                           
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/recipes/make-this-low-carb-margarita-pizza-with-a-super-healthy-cauliflower-crust-249230.html" class="tint" title="Make This Low-Carb Margarita Pizza With A Super Healthy Cauliflower Crust!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/recipes/make-this-low-carb-margarita-pizza-with-a-super-healthy-cauliflower-crust-249230.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2016/Jan/cover_1452251045_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2016/Jan/cover_1452251045_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/recipes/make-this-low-carb-margarita-pizza-with-a-super-healthy-cauliflower-crust-249230.html" title="Make This Low-Carb Margarita Pizza With A Super Healthy Cauliflower Crust!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/health/recipes/make-this-low-carb-margarita-pizza-with-a-super-healthy-cauliflower-crust-249230.html');">
                            Make This Low-Carb Margarita Pizza With A Super Healthy Cauliflower Crust!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/12-characters-from-bollywood-who-would-make-perfect-life-partners-part-ii-249166.html" class="tint" title="12 Characters From Bollywood Who Would Make Perfect Life Partners (Part II)" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/12-characters-from-bollywood-who-would-make-perfect-life-partners-part-ii-249166.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Jan/fawad2_1452087836_1452087841_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/fawad2_1452087836_1452087841_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/12-characters-from-bollywood-who-would-make-perfect-life-partners-part-ii-249166.html" title="12 Characters From Bollywood Who Would Make Perfect Life Partners (Part II)" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/self/12-characters-from-bollywood-who-would-make-perfect-life-partners-part-ii-249166.html');">
                            12 Characters From Bollywood Who Would Make Perfect Life Partners (Part II)                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                   
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/technology/soon-there-can-be-moon-villages-to-swoon-over-acting-as-man-s-first-pit-stop-into-deep-space-249277.html" class="tint" title="Soon There Can Be Moon Villages To Swoon Over, Acting As Man's First Pit-Stop Into Deep Space" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/technology/soon-there-can-be-moon-villages-to-swoon-over-acting-as-man-s-first-pit-stop-into-deep-space-249277.html');">
                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Jan/space-moon_1452429949_1452429957_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/space-moon_1452429949_1452429957_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/soon-there-can-be-moon-villages-to-swoon-over-acting-as-man-s-first-pit-stop-into-deep-space-249277.html" title="Soon There Can Be Moon Villages To Swoon Over, Acting As Man's First Pit-Stop Into Deep Space" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/technology/soon-there-can-be-moon-villages-to-swoon-over-acting-as-man-s-first-pit-stop-into-deep-space-249277.html');">
                            Soon There Can Be Moon Villages To Swoon Over, Acting As Man's First Pit-Stop Into Deep Space                        </a>
                    </figcaption>
                </div>
                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-ll-relate-to-if-you-re-mistaken-for-a-rude-person-but-are-actually-kind-hearted-249215.html" class="tint" title="11 Things You'll Relate To If You're Mistaken For A Rude Person But Are Actually Kind-Hearted">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Jan/card_1452240511_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/card_1452240511_218x102.jpg" border="0" alt="11 Things You'll Relate To If You're Mistaken For A Rude Person But Are Actually Kind-Hearted"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-ll-relate-to-if-you-re-mistaken-for-a-rude-person-but-are-actually-kind-hearted-249215.html" title="11 Things You'll Relate To If You're Mistaken For A Rude Person But Are Actually Kind-Hearted">
                            11 Things You'll Relate To If You're Mistaken For A Rude Person But Are Actually Kind-Hearted                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                                                <a href="http://www.indiatimes.com/culture/who-we-are/comedian-kapil-sharma-s-elder-brother-was-one-of-the-bravehearts-who-guarded-the-pathankot-airbase-during-the-attack-249264.html" class="tint" title="Comedian Kapil Sharma's Elder Brother Was One Of The Bravehearts Who Guarded The Pathankot Airbase During The Attack!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Jan/kp-card_1452415567_1452415574_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/kp-card_1452415567_1452415574_218x102.jpg" border="0" alt="Comedian Kapil Sharma's Elder Brother Was One Of The Bravehearts Who Guarded The Pathankot Airbase During The Attack!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/comedian-kapil-sharma-s-elder-brother-was-one-of-the-bravehearts-who-guarded-the-pathankot-airbase-during-the-attack-249264.html" title="Comedian Kapil Sharma's Elder Brother Was One Of The Bravehearts Who Guarded The Pathankot Airbase During The Attack!">
                            Comedian Kapil Sharma's Elder Brother Was One Of The Bravehearts Who Guarded The Pathankot Airbase During The Attack!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                                                <a href="http://www.indiatimes.com/entertainment/bollywood/12-reasons-why-hrithik-roshan-is-the-real-trendsetter-of-bollywood-249204.html" class="tint" title="12 Reasons Why Hrithik Roshan Is The Real Trendsetter Of Bollywood">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Jan/hr-card_1452328195_1452328200_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/hr-card_1452328195_1452328200_218x102.jpg" border="0" alt="12 Reasons Why Hrithik Roshan Is The Real Trendsetter Of Bollywood"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/12-reasons-why-hrithik-roshan-is-the-real-trendsetter-of-bollywood-249204.html" title="12 Reasons Why Hrithik Roshan Is The Real Trendsetter Of Bollywood">
                            12 Reasons Why Hrithik Roshan Is The Real Trendsetter Of Bollywood                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/12-characters-from-bollywood-who-would-make-perfect-life-partners-part-ii-249166.html" class="tint" title="12 Characters From Bollywood Who Would Make Perfect Life Partners (Part II)">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Jan/fawad2_1452087836_1452087841_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/fawad2_1452087836_1452087841_218x102.jpg" border="0" alt="12 Characters From Bollywood Who Would Make Perfect Life Partners (Part II)"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/12-characters-from-bollywood-who-would-make-perfect-life-partners-part-ii-249166.html" title="12 Characters From Bollywood Who Would Make Perfect Life Partners (Part II)">
                            12 Characters From Bollywood Who Would Make Perfect Life Partners (Part II)                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/meet-aravinda-and-ravi-the-inspiration-behind-shahrukh-khan-s-movie-swades-249108.html" class="tint" title="Meet Aravinda and Ravi, The Inspiration Behind Shahrukh Khan's Movie 'Swades'">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2016/Jan/card_1451990186_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Jan/card_1451990186_218x102.jpg" border="0" alt="Meet Aravinda and Ravi, The Inspiration Behind Shahrukh Khan's Movie 'Swades'"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/meet-aravinda-and-ravi-the-inspiration-behind-shahrukh-khan-s-movie-swades-249108.html" title="Meet Aravinda and Ravi, The Inspiration Behind Shahrukh Khan's Movie 'Swades'">
                            Meet Aravinda and Ravi, The Inspiration Behind Shahrukh Khan's Movie 'Swades'                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/with-100-and-99-9-percentile-scores-in-cat-2015-these-are-the-padhakus-who-are-giving-headaches-to-lot-of-slackers-249270.html" title="With 100 And 99.9 Percentile Scores In CAT 2015, These Are The Padhakus Who Are Giving Headaches To Lot Of Slackers" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cat-scores-nerdies_1452423826_1452423839_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/with-100-and-99-9-percentile-scores-in-cat-2015-these-are-the-padhakus-who-are-giving-headaches-to-lot-of-slackers-249270.html" title="With 100 And 99.9 Percentile Scores In CAT 2015, These Are The Padhakus Who Are Giving Headaches To Lot Of Slackers">
                            With 100 And 99.9 Percentile Scores In CAT 2015, These Are The Padhakus Who Are Giving Headaches To Lot Of Slackers                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/for-the-first-time-in-indian-history-french-soldiers-will-march-with-india-on-republic-day-249273.html" title="For The First Time In India's History, French Soldiers Will March With Indians On Republic Day" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/5_1452423288_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/for-the-first-time-in-indian-history-french-soldiers-will-march-with-india-on-republic-day-249273.html" title="For The First Time In India's History, French Soldiers Will March With Indians On Republic Day">
                            For The First Time In India's History, French Soldiers Will March With Indians On Republic Day                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            7 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/despite-restrictions-on-freedom-of-expression-saudi-arabia-is-the-third-happiest-country-in-the-world-249268.html" title="Despite Restrictions On Freedom Of Expression, Saudi Arabia Is The Third Happiest Country In The World!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/sa1_1452420455_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/despite-restrictions-on-freedom-of-expression-saudi-arabia-is-the-third-happiest-country-in-the-world-249268.html" title="Despite Restrictions On Freedom Of Expression, Saudi Arabia Is The Third Happiest Country In The World!">
                            Despite Restrictions On Freedom Of Expression, Saudi Arabia Is The Third Happiest Country In The World!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/world/beijing-will-shut-down-2500-polluting-firms-to-make-its-air-breathable-249267.html" title="Beijing Will Shut Down 2500 Polluting Firms To Make Its Air Breathable" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/afp6_1452419783_1452419787_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/world/beijing-will-shut-down-2500-polluting-firms-to-make-its-air-breathable-249267.html" title="Beijing Will Shut Down 2500 Polluting Firms To Make Its Air Breathable">
                            Beijing Will Shut Down 2500 Polluting Firms To Make Its Air Breathable                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            8 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/here-s-an-inspiring-story-of-a-kerala-village-that-chose-to-give-up-on-alcohol-and-replace-it-with-chess-249266.html" title="Here's An Inspiring Story Of A Kerala Village That Chose To Give Up On Alcohol And Replace It With Chess!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/chess_1452419678_1452419684_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/here-s-an-inspiring-story-of-a-kerala-village-that-chose-to-give-up-on-alcohol-and-replace-it-with-chess-249266.html" title="Here's An Inspiring Story Of A Kerala Village That Chose To Give Up On Alcohol And Replace It With Chess!">
                            Here's An Inspiring Story Of A Kerala Village That Chose To Give Up On Alcohol And Replace It With Chess!                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/hindustan-zaroor-azad-hoga-were-netaji-s-last-words-according-to-a-uk-website-249272.html" class="tint" title="'Hindustan Zaroor Azad Hoga' Were Netaji's Last Words According To A UK Website">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/netaji_1452422966_1452422971_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/hindustan-zaroor-azad-hoga-were-netaji-s-last-words-according-to-a-uk-website-249272.html" title="'Hindustan Zaroor Azad Hoga' Were Netaji's Last Words According To A UK Website">
                            'Hindustan Zaroor Azad Hoga' Were Netaji's Last Words According To A UK Website                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/technology/google-and-lenovo-s-project-tango-phone-that-promises-navigation-ability-for-the-blind-out-this-year-249271.html" class="tint" title="Google And Lenovo's Project Tango Phone That Promises Navigation Ability For The Blind Out This Year">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452422183_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/google-and-lenovo-s-project-tango-phone-that-promises-navigation-ability-for-the-blind-out-this-year-249271.html" title="Google And Lenovo's Project Tango Phone That Promises Navigation Ability For The Blind Out This Year">
                            Google And Lenovo's Project Tango Phone That Promises Navigation Ability For The Blind Out This Year                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/celebs/another-blow-for-aamir-after-incredible-india-now-he-s-being-dropped-from-the-road-safety-campaign-249269.html" class="tint" title="Another Blow For Aamir, After Incredible India, Now He's Being Dropped From The Road Safety Campaign!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/aamir-khan-road-safety_1452422195_1452422201_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/another-blow-for-aamir-after-incredible-india-now-he-s-being-dropped-from-the-road-safety-campaign-249269.html" title="Another Blow For Aamir, After Incredible India, Now He's Being Dropped From The Road Safety Campaign!">
                            Another Blow For Aamir, After Incredible India, Now He's Being Dropped From The Road Safety Campaign!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-reasons-twenty-somethings-are-the-most-hardworking-lot-today-249236.html" class="tint" title="11 Reasons Twenty-Somethings Are The Most Hardworking Lot Today">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/pitch_1452263592_218x102.jpg" border="0" alt="11 Reasons Twenty-Somethings Are The Most Hardworking Lot Today"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/11-reasons-twenty-somethings-are-the-most-hardworking-lot-today-249236.html" title="11 Reasons Twenty-Somethings Are The Most Hardworking Lot Today">
                            11 Reasons Twenty-Somethings Are The Most Hardworking Lot Today                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/technology/apple-might-just-ditch-the-3-5mm-headphone-jack-for-good-in-the-upcoming-iphone-7_-249238.html" class="tint" title="Apple Might Just Ditch The 3.5mm Headphone Jack For Good In The Upcoming iPhone 7">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp_1452320006_1452320015_218x102.jpg" border="0" alt="Apple Might Just Ditch The 3.5mm Headphone Jack For Good In The Upcoming iPhone 7"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/technology/apple-might-just-ditch-the-3-5mm-headphone-jack-for-good-in-the-upcoming-iphone-7_-249238.html" title="Apple Might Just Ditch The 3.5mm Headphone Jack For Good In The Upcoming iPhone 7">
                            Apple Might Just Ditch The 3.5mm Headphone Jack For Good In The Upcoming iPhone 7                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/lifestyle/self/this-is-what-2016-looks-like-according-to-your-zodiac-249014.html" class="tint" title="This Is What 2016 Looks Like, According To Your Zodiac">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp1_1451736227_218x102.jpg" border="0" alt="This Is What 2016 Looks Like, According To Your Zodiac"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/this-is-what-2016-looks-like-according-to-your-zodiac-249014.html" title="This Is What 2016 Looks Like, According To Your Zodiac">
                            This Is What 2016 Looks Like, According To Your Zodiac                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/bollywood/12-moments-from-the-2015-star-screen-awards-that-made-it-extremely-special-249245.html" class="tint" title="12 Moments From The 2015 Star Screen Awards That Made It Extremely Special">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/star-card_1452335116_1452335121_218x102.jpg" border="0" alt="12 Moments From The 2015 Star Screen Awards That Made It Extremely Special"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/12-moments-from-the-2015-star-screen-awards-that-made-it-extremely-special-249245.html" title="12 Moments From The 2015 Star Screen Awards That Made It Extremely Special">
                            12 Moments From The 2015 Star Screen Awards That Made It Extremely Special                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/culture/travel/nasa-found-all-26-english-alphabets-hiding-in-plain-sight-on-the-earth-s-surface-249110.html" class="tint" title="NASA Found All 26 English Alphabets Hiding In Plain Sight On The Earth's Surface!">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1451991004_218x102.jpg" border="0" alt="NASA Found All 26 English Alphabets Hiding In Plain Sight On The Earth's Surface!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/travel/nasa-found-all-26-english-alphabets-hiding-in-plain-sight-on-the-earth-s-surface-249110.html" title="NASA Found All 26 English Alphabets Hiding In Plain Sight On The Earth's Surface!">
                            NASA Found All 26 English Alphabets Hiding In Plain Sight On The Earth's Surface!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            8 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/world/12-things-you-need-to-know-about-world-s-deadliest-drug-lord-since-escobar-el-chapo-guzman-249262.html" title="12 Things You Need To Know About Worldâs Deadliest Drug Lord Since Escobar â El Chapo Guzman" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/2_1452418309_1452418313_236x111.jpg" border="0" alt="12 Things You Need To Know About Worldâs Deadliest Drug Lord Since Escobar â El Chapo Guzman"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/12-things-you-need-to-know-about-world-s-deadliest-drug-lord-since-escobar-el-chapo-guzman-249262.html" title="12 Things You Need To Know About Worldâs Deadliest Drug Lord Since Escobar â El Chapo Guzman">
                            12 Things You Need To Know About Worldâs Deadliest Drug Lord Since Escobar â El Chapo Guzman                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/british-comedian-james-veitch-s-email-exchange-with-a-spammer-is-the-most-hilarious-thing-you-will-watch-today-249263.html" title="This British Comedian's Email Exchange With A Spammer Is The Most Hilarious Thing You Will Watch Today" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452411740_236x111.jpg" border="0" alt="This British Comedian's Email Exchange With A Spammer Is The Most Hilarious Thing You Will Watch Today"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/british-comedian-james-veitch-s-email-exchange-with-a-spammer-is-the-most-hilarious-thing-you-will-watch-today-249263.html" title="This British Comedian's Email Exchange With A Spammer Is The Most Hilarious Thing You Will Watch Today">
                            This British Comedian's Email Exchange With A Spammer Is The Most Hilarious Thing You Will Watch Today                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            10 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/since-oddeven-began-delhi-pollution-has-gone-up-by-by-50-lets-end-it-249260.html" title="Since The Odd-Even Scheme Began, Delhi Pollution Has Actually Gone Up By 50% #Shocking" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/afp6_1452410982_1452410985_236x111.jpg" border="0" alt="Since The Odd-Even Scheme Began, Delhi Pollution Has Actually Gone Up By 50% #Shocking"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/since-oddeven-began-delhi-pollution-has-gone-up-by-by-50-lets-end-it-249260.html" title="Since The Odd-Even Scheme Began, Delhi Pollution Has Actually Gone Up By 50% #Shocking">
                            Since The Odd-Even Scheme Began, Delhi Pollution Has Actually Gone Up By 50% #Shocking                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/telegraph-calls-colonel-niranjan-s-sacrifice-stupidity-major-navdeep-singh-explains-why-we-need-to-honour-our-martyrs-249254.html" title="Maj Navdeep Singh Writes A Fitting Reply To A Newspaper That Called Lt Col Niranjan's Heroics 'Stupid'" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/5_1452404307_1452404316_236x111.jpg" border="0" alt="Maj Navdeep Singh Writes A Fitting Reply To A Newspaper That Called Lt Col Niranjan's Heroics 'Stupid'"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/telegraph-calls-colonel-niranjan-s-sacrifice-stupidity-major-navdeep-singh-explains-why-we-need-to-honour-our-martyrs-249254.html" title="Maj Navdeep Singh Writes A Fitting Reply To A Newspaper That Called Lt Col Niranjan's Heroics 'Stupid'">
                            Maj Navdeep Singh Writes A Fitting Reply To A Newspaper That Called Lt Col Niranjan's Heroics 'Stupid'                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/specials/this-indian-man-walked-on-water-and-cycled-on-high-seas-back-in-the-1980s_-249258.html" title="This Indian Man Walked On Water And Cycled On High Seas Back In The 1980s" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/loplp_1452409907_1452409913_236x111.jpg" border="0" alt="This Indian Man Walked On Water And Cycled On High Seas Back In The 1980s"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/specials/this-indian-man-walked-on-water-and-cycled-on-high-seas-back-in-the-1980s_-249258.html" title="This Indian Man Walked On Water And Cycled On High Seas Back In The 1980s">
                            This Indian Man Walked On Water And Cycled On High Seas Back In The 1980s                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/ever-wondered-what-goes-on-inside-your-body-this-video-takes-you-through-a-typical-day-249226.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/healthyliving/ever-wondered-what-goes-on-inside-your-body-this-video-takes-you-through-a-typical-day-249226.html" class="tint" title="Ever Wondered What Goes On Inside Your Body? This Video Takes You Through A Typical Day!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/cover_1452248482_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/healthyliving/ever-wondered-what-goes-on-inside-your-body-this-video-takes-you-through-a-typical-day-249226.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/ever-wondered-what-goes-on-inside-your-body-this-video-takes-you-through-a-typical-day-249226.html" title="Ever Wondered What Goes On Inside Your Body? This Video Takes You Through A Typical Day!">
                            Ever Wondered What Goes On Inside Your Body? This Video Takes You Through A Typical Day!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/lifestyle/self/15-quotes-on-winter-so-beautiful-you-d-want-this-time-to-last-forever-248973.html" class="tint" title="15 Quotes On Winter So Beautiful, You'd Want This Time To Last Forever">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Dec/card_1451558490_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/self/15-quotes-on-winter-so-beautiful-you-d-want-this-time-to-last-forever-248973.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/15-quotes-on-winter-so-beautiful-you-d-want-this-time-to-last-forever-248973.html" title="15 Quotes On Winter So Beautiful, You'd Want This Time To Last Forever">
                            15 Quotes On Winter So Beautiful, You'd Want This Time To Last Forever                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/here-s-an-epic-tribute-to-the-world-s-favourite-reporter-tintin-who-turns-87-today-249259.html" class="tint" title="Here's An Epic Tribute To The World's Favourite Reporter Tintin, Who Turns 87 Today!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/tin-tin-card_1452410425_1452410433_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/who-we-are/here-s-an-epic-tribute-to-the-world-s-favourite-reporter-tintin-who-turns-87-today-249259.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/here-s-an-epic-tribute-to-the-world-s-favourite-reporter-tintin-who-turns-87-today-249259.html" title="Here's An Epic Tribute To The World's Favourite Reporter Tintin, Who Turns 87 Today!">
                            Here's An Epic Tribute To The World's Favourite Reporter Tintin, Who Turns 87 Today!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/these-husbands-thought-that-their-wives-shop-just-to-be-happy-watch-this-video-to-see-their-reactions-when-they-realized-how-wrong-they-were-249174.html'>video</a>		

                        <a href="http://www.indiatimes.com/culture/who-we-are/these-husbands-thought-that-their-wives-shop-just-to-be-happy-watch-this-video-to-see-their-reactions-when-they-realized-how-wrong-they-were-249174.html" class="tint" title="These Husbands Thought That Their Wives Shop Just To Be Happy. Hereâs What Happened When They Found Out The Truth">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/card_1452254099_218x102.jpg" border="0" alt="These Husbands Thought That Their Wives Shop Just To Be Happy. Hereâs What Happened When They Found Out The Truth"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/these-husbands-thought-that-their-wives-shop-just-to-be-happy-watch-this-video-to-see-their-reactions-when-they-realized-how-wrong-they-were-249174.html" title="These Husbands Thought That Their Wives Shop Just To Be Happy. Hereâs What Happened When They Found Out The Truth">
                            These Husbands Thought That Their Wives Shop Just To Be Happy. Hereâs What Happened When They Found Out The Truth                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/it-s-not-possible-i-won-t-find-love-5-revelations-by-hrithik-roshan-about-life-249232.html" class="tint" title="'It's Not Possible I Won't Find Love'+ 5 Revelations By Hrithik Roshan About Life!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/62004_1452251256_1452251260_218x102.jpg" border="0" alt="'It's Not Possible I Won't Find Love'+ 5 Revelations By Hrithik Roshan About Life!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/it-s-not-possible-i-won-t-find-love-5-revelations-by-hrithik-roshan-about-life-249232.html" title="'It's Not Possible I Won't Find Love'+ 5 Revelations By Hrithik Roshan About Life!">
                            'It's Not Possible I Won't Find Love'+ 5 Revelations By Hrithik Roshan About Life!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/9-actresses-who-performed-stunts-themselves-in-their-films-249217.html" class="tint" title="Shraddha Kapoor Does A Daredevil Stunt From The 34th Floor Of A Building + 9 Actresses Who Performed Their Own Stunts">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/picmonkey-collage_1452240490_1452240496_218x102.jpg" border="0" alt="Shraddha Kapoor Does A Daredevil Stunt From The 34th Floor Of A Building + 9 Actresses Who Performed Their Own Stunts"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/9-actresses-who-performed-stunts-themselves-in-their-films-249217.html" title="Shraddha Kapoor Does A Daredevil Stunt From The 34th Floor Of A Building + 9 Actresses Who Performed Their Own Stunts">
                            Shraddha Kapoor Does A Daredevil Stunt From The 34th Floor Of A Building + 9 Actresses Who Performed Their Own Stunts                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/bollywood/mumbai-police-reduces-security-cover-of-srk-aamir-khan-and-38-others-in-bollywood-249209.html" class="tint" title="Mumbai Police Reduces Security Cover Of SRK, Aamir Khan, And 38 Others In Bollywood">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card-srk_1452233904_1452233912_218x102.jpg" border="0" alt="Mumbai Police Reduces Security Cover Of SRK, Aamir Khan, And 38 Others In Bollywood"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/mumbai-police-reduces-security-cover-of-srk-aamir-khan-and-38-others-in-bollywood-249209.html" title="Mumbai Police Reduces Security Cover Of SRK, Aamir Khan, And 38 Others In Bollywood">
                            Mumbai Police Reduces Security Cover Of SRK, Aamir Khan, And 38 Others In Bollywood                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/lifestyle/self/17-images-of-women-from-around-the-world-that-prove-beauty-lies-in-diversity-249107.html" class="tint" title="17 Images Of Women From Around The World That Prove Beauty Lies In Diversity">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card2_1451990775_1451990782_218x102.jpg" border="0" alt="17 Images Of Women From Around The World That Prove Beauty Lies In Diversity"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/17-images-of-women-from-around-the-world-that-prove-beauty-lies-in-diversity-249107.html" title="17 Images Of Women From Around The World That Prove Beauty Lies In Diversity">
                            17 Images Of Women From Around The World That Prove Beauty Lies In Diversity                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/pathankot-attack-mastermind-masood-azhar-mocks-india-says-indian-defence-agencies-couldn-t-tackle-six-mujaheedins-249252.html" title="Pathankot Attack Mastermind Masood Azhar Mocks India, Says Indian Defence Agencies Couldn't Tackle Six Mujaheedins" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/jaish-card_1452343344_236x111.jpg" border="0" alt="Pathankot Attack Mastermind Masood Azhar Mocks India, Says Indian Defence Agencies Couldn't Tackle Six Mujaheedins"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/pathankot-attack-mastermind-masood-azhar-mocks-india-says-indian-defence-agencies-couldn-t-tackle-six-mujaheedins-249252.html" title="Pathankot Attack Mastermind Masood Azhar Mocks India, Says Indian Defence Agencies Couldn't Tackle Six Mujaheedins">
                            Pathankot Attack Mastermind Masood Azhar Mocks India, Says Indian Defence Agencies Couldn't Tackle Six Mujaheedins                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/female-isis-jihadist-tortures-syrian-girl-to-death-for-violating-islamic-dress-code-249250.html" title="Female ISIS Jihadist Tortures Syrian Girl To Death For Violating Islamic Dress Code" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452339343_236x111.jpg" border="0" alt="Female ISIS Jihadist Tortures Syrian Girl To Death For Violating Islamic Dress Code"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/female-isis-jihadist-tortures-syrian-girl-to-death-for-violating-islamic-dress-code-249250.html" title="Female ISIS Jihadist Tortures Syrian Girl To Death For Violating Islamic Dress Code">
                            Female ISIS Jihadist Tortures Syrian Girl To Death For Violating Islamic Dress Code                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/nasa-s-aviation-innovations-could-cut-carbon-emissions-and-save-money-for-airlines-249247.html" title="NASA's Aviation Innovations Could Cut Carbon Emissions And Save Money For Airlines" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp_1452343252_1452343260_236x111.jpg" border="0" alt="NASA's Aviation Innovations Could Cut Carbon Emissions And Save Money For Airlines"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/nasa-s-aviation-innovations-could-cut-carbon-emissions-and-save-money-for-airlines-249247.html" title="NASA's Aviation Innovations Could Cut Carbon Emissions And Save Money For Airlines">
                            NASA's Aviation Innovations Could Cut Carbon Emissions And Save Money For Airlines                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/atithi-devo-bhava-india-to-provide-gift-kits-to-tourists-arriving-on-e-visa-249248.html" title="Atithi Devo Bhava! India To Provide 'Gift Kits' To Tourists Arriving On E-Visa" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452336783_236x111.jpg" border="0" alt="Atithi Devo Bhava! India To Provide 'Gift Kits' To Tourists Arriving On E-Visa"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/atithi-devo-bhava-india-to-provide-gift-kits-to-tourists-arriving-on-e-visa-249248.html" title="Atithi Devo Bhava! India To Provide 'Gift Kits' To Tourists Arriving On E-Visa">
                            Atithi Devo Bhava! India To Provide 'Gift Kits' To Tourists Arriving On E-Visa                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/a-9-11-style-attack-might-strike-europe-very-soon-warn-security-experts-249244.html" title="A 9/11 Style Attack Might Strike Europe Very Soon, Warn Security Experts" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp_1452334814_1452334818_236x111.jpg" border="0" alt="A 9/11 Style Attack Might Strike Europe Very Soon, Warn Security Experts"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/a-9-11-style-attack-might-strike-europe-very-soon-warn-security-experts-249244.html" title="A 9/11 Style Attack Might Strike Europe Very Soon, Warn Security Experts">
                            A 9/11 Style Attack Might Strike Europe Very Soon, Warn Security Experts                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/comedian-kapil-sharma-s-elder-brother-was-one-of-the-bravehearts-who-guarded-the-pathankot-airbase-during-the-attack-249264.html" class="tint" title="Comedian Kapil Sharma's Elder Brother Was One Of The Bravehearts Who Guarded The Pathankot Airbase During The Attack!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/kp-card_1452415567_1452415574_502x234.jpg" border="0" alt="Comedian Kapil Sharma's Elder Brother Was One Of The Bravehearts Who Guarded The Pathankot Airbase During The Attack!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/comedian-kapil-sharma-s-elder-brother-was-one-of-the-bravehearts-who-guarded-the-pathankot-airbase-during-the-attack-249264.html" title="Comedian Kapil Sharma's Elder Brother Was One Of The Bravehearts Who Guarded The Pathankot Airbase During The Attack!">
                        Comedian Kapil Sharma's Elder Brother Was One Of The Bravehearts Who Guarded The Pathankot Airbase During The Attack!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/19-yo-leonardo-dicaprio-talking-about-his-acting-goals-in-his-first-interview-will-take-you-back-in-time-249257.html" class="tint" title="19 YO Leonardo DiCaprio Talking About His Acting Goals In His First Interview Will Take You Back In Time">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/aa_1452408195_1452408198_502x234.jpg" border="0" alt="19 YO Leonardo DiCaprio Talking About His Acting Goals In His First Interview Will Take You Back In Time" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/19-yo-leonardo-dicaprio-talking-about-his-acting-goals-in-his-first-interview-will-take-you-back-in-time-249257.html" title="19 YO Leonardo DiCaprio Talking About His Acting Goals In His First Interview Will Take You Back In Time">
                        19 YO Leonardo DiCaprio Talking About His Acting Goals In His First Interview Will Take You Back In Time                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/spacex-will-try-to-land-a-rocket-on-a-moving-barge-in-the-middle-of-the-ocean-249261.html" class="tint" title="SpaceX Will Try To Land A Rocket On A Moving Barge In The Middle Of The Ocean!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/rocket-card_1452411254_502x234.jpg" border="0" alt="SpaceX Will Try To Land A Rocket On A Moving Barge In The Middle Of The Ocean!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/spacex-will-try-to-land-a-rocket-on-a-moving-barge-in-the-middle-of-the-ocean-249261.html" title="SpaceX Will Try To Land A Rocket On A Moving Barge In The Middle Of The Ocean!">
                        SpaceX Will Try To Land A Rocket On A Moving Barge In The Middle Of The Ocean!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/recipes/make-the-most-of-this-strawberry-season-with-this-super-healthy-ice-cream-recipe-248048.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/recipes/make-the-most-of-this-strawberry-season-with-this-super-healthy-ice-cream-recipe-248048.html" class="tint" title="Make The Most Of This Strawberry Season With This Super Healthy Ice Cream Recipe!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1449218606_502x234.jpg" border="0" alt="Make The Most Of This Strawberry Season With This Super Healthy Ice Cream Recipe!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/recipes/make-the-most-of-this-strawberry-season-with-this-super-healthy-ice-cream-recipe-248048.html" title="Make The Most Of This Strawberry Season With This Super Healthy Ice Cream Recipe!">
                        Make The Most Of This Strawberry Season With This Super Healthy Ice Cream Recipe!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/mrs-funnybones-hilarious-take-on-dieting-adnan-sami-s-indian-citizenship-airlift-will-leave-you-in-splits-249255.html" class="tint" title="Mrs Funnybones' Hilarious Take On Dieting, Adnan Sami's Indian Citizenship & Airlift Will Leave You In Splits!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/tk12_1452411602_1452411606_502x234.jpg" border="0" alt="Mrs Funnybones' Hilarious Take On Dieting, Adnan Sami's Indian Citizenship & Airlift Will Leave You In Splits!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/mrs-funnybones-hilarious-take-on-dieting-adnan-sami-s-indian-citizenship-airlift-will-leave-you-in-splits-249255.html" title="Mrs Funnybones' Hilarious Take On Dieting, Adnan Sami's Indian Citizenship & Airlift Will Leave You In Splits!">
                        Mrs Funnybones' Hilarious Take On Dieting, Adnan Sami's Indian Citizenship & Airlift Will Leave You In Splits!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-reasons-falling-sick-when-you-re-living-alone-is-equal-to-hell-on-earth-249223.html" class="tint" title="9 Reasons Falling Sick When You're Living Alone Is Equal To Hell On Earth">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp_1452245538_1452245548_502x234.jpg" border="0" alt="9 Reasons Falling Sick When You're Living Alone Is Equal To Hell On Earth" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-reasons-falling-sick-when-you-re-living-alone-is-equal-to-hell-on-earth-249223.html" title="9 Reasons Falling Sick When You're Living Alone Is Equal To Hell On Earth">
                        9 Reasons Falling Sick When You're Living Alone Is Equal To Hell On Earth                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/manipur-earthquake-damaged-the-only-all-women-s-market-in-the-world-249256.html" class="tint" title="Manipur Earthquake Damaged The Only All-Womenâs Market In The World!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card-2_1452405600_1452405612_502x234.jpg" border="0" alt="Manipur Earthquake Damaged The Only All-Womenâs Market In The World!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/manipur-earthquake-damaged-the-only-all-women-s-market-in-the-world-249256.html" title="Manipur Earthquake Damaged The Only All-Womenâs Market In The World!">
                        Manipur Earthquake Damaged The Only All-Womenâs Market In The World!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-reasons-why-hrithik-roshan-is-the-real-trendsetter-of-bollywood-249204.html" class="tint" title="12 Reasons Why Hrithik Roshan Is The Real Trendsetter Of Bollywood">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/hr-card_1452328195_1452328200_502x234.jpg" border="0" alt="12 Reasons Why Hrithik Roshan Is The Real Trendsetter Of Bollywood" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-reasons-why-hrithik-roshan-is-the-real-trendsetter-of-bollywood-249204.html" title="12 Reasons Why Hrithik Roshan Is The Real Trendsetter Of Bollywood">
                        12 Reasons Why Hrithik Roshan Is The Real Trendsetter Of Bollywood                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/25-edible-flowers-that-will-add-colour-flavour-and-nutrition-to-your-food-249224.html" class="tint" title="25 Edible Flowers That Will Add Colour, Flavour And Nutrition To Your Food">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cover_1452246064_502x234.jpg" border="0" alt="25 Edible Flowers That Will Add Colour, Flavour And Nutrition To Your Food" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/25-edible-flowers-that-will-add-colour-flavour-and-nutrition-to-your-food-249224.html" title="25 Edible Flowers That Will Add Colour, Flavour And Nutrition To Your Food">
                        25 Edible Flowers That Will Add Colour, Flavour And Nutrition To Your Food                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-ll-relate-to-if-you-re-mistaken-for-a-rude-person-but-are-actually-kind-hearted-249215.html" class="tint" title="11 Things You'll Relate To If You're Mistaken For A Rude Person But Are Actually Kind-Hearted">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452240511_502x234.jpg" border="0" alt="11 Things You'll Relate To If You're Mistaken For A Rude Person But Are Actually Kind-Hearted" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/11-things-you-ll-relate-to-if-you-re-mistaken-for-a-rude-person-but-are-actually-kind-hearted-249215.html" title="11 Things You'll Relate To If You're Mistaken For A Rude Person But Are Actually Kind-Hearted">
                        11 Things You'll Relate To If You're Mistaken For A Rude Person But Are Actually Kind-Hearted                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-reasons-twenty-somethings-are-the-most-hardworking-lot-today-249236.html" class="tint" title="11 Reasons Twenty-Somethings Are The Most Hardworking Lot Today">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/pitch_1452263592_502x234.jpg" border="0" alt="11 Reasons Twenty-Somethings Are The Most Hardworking Lot Today" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-reasons-twenty-somethings-are-the-most-hardworking-lot-today-249236.html" title="11 Reasons Twenty-Somethings Are The Most Hardworking Lot Today">
                        11 Reasons Twenty-Somethings Are The Most Hardworking Lot Today                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/style/this-indian-film-company-bagged-the-2nd-spot-on-the-list-of-popular-offices-around-the-world-249251.html" class="tint" title="This Indian Film Company Bagged The 2nd Spot On The List Of Popular Offices Around The World">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp_1452342669_502x234.jpg" border="0" alt="This Indian Film Company Bagged The 2nd Spot On The List Of Popular Offices Around The World" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/style/this-indian-film-company-bagged-the-2nd-spot-on-the-list-of-popular-offices-around-the-world-249251.html" title="This Indian Film Company Bagged The 2nd Spot On The List Of Popular Offices Around The World">
                        This Indian Film Company Bagged The 2nd Spot On The List Of Popular Offices Around The World                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/8-things-that-prove-vin-diesel-s-xxx-is-the-perfect-hollywood-launch-for-deepika-padukone-249249.html" class="tint" title="8 Things That Prove Vin Diesel's 'XXX' Is The Perfect Hollywood Launch For Deepika Padukone">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/deep-c_1452339090_1452339097_502x234.jpg" border="0" alt="8 Things That Prove Vin Diesel's 'XXX' Is The Perfect Hollywood Launch For Deepika Padukone" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/8-things-that-prove-vin-diesel-s-xxx-is-the-perfect-hollywood-launch-for-deepika-padukone-249249.html" title="8 Things That Prove Vin Diesel's 'XXX' Is The Perfect Hollywood Launch For Deepika Padukone">
                        8 Things That Prove Vin Diesel's 'XXX' Is The Perfect Hollywood Launch For Deepika Padukone                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/this-is-what-2016-looks-like-according-to-your-zodiac-249014.html" class="tint" title="This Is What 2016 Looks Like, According To Your Zodiac">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp1_1451736227_502x234.jpg" border="0" alt="This Is What 2016 Looks Like, According To Your Zodiac" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/this-is-what-2016-looks-like-according-to-your-zodiac-249014.html" title="This Is What 2016 Looks Like, According To Your Zodiac">
                        This Is What 2016 Looks Like, According To Your Zodiac                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-moments-from-the-2015-star-screen-awards-that-made-it-extremely-special-249245.html" class="tint" title="12 Moments From The 2015 Star Screen Awards That Made It Extremely Special">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/star-card_1452335116_1452335121_502x234.jpg" border="0" alt="12 Moments From The 2015 Star Screen Awards That Made It Extremely Special" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-moments-from-the-2015-star-screen-awards-that-made-it-extremely-special-249245.html" title="12 Moments From The 2015 Star Screen Awards That Made It Extremely Special">
                        12 Moments From The 2015 Star Screen Awards That Made It Extremely Special                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/14-reasons-farhan-akhtar-is-the-man-we-love-to-love-249186.html" class="tint" title="14 Reasons Farhan Akhtar Is The Man We Love To Love">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/farhan-akhtar-card_1452185128_1452185142_502x234.jpg" border="0" alt="14 Reasons Farhan Akhtar Is The Man We Love To Love" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/14-reasons-farhan-akhtar-is-the-man-we-love-to-love-249186.html" title="14 Reasons Farhan Akhtar Is The Man We Love To Love">
                        14 Reasons Farhan Akhtar Is The Man We Love To Love                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/meet-aravinda-and-ravi-the-inspiration-behind-shahrukh-khan-s-movie-swades-249108.html" class="tint" title="Meet Aravinda and Ravi, The Inspiration Behind Shahrukh Khan's Movie 'Swades'">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1451990186_502x234.jpg" border="0" alt="Meet Aravinda and Ravi, The Inspiration Behind Shahrukh Khan's Movie 'Swades'" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/meet-aravinda-and-ravi-the-inspiration-behind-shahrukh-khan-s-movie-swades-249108.html" title="Meet Aravinda and Ravi, The Inspiration Behind Shahrukh Khan's Movie 'Swades'">
                        Meet Aravinda and Ravi, The Inspiration Behind Shahrukh Khan's Movie 'Swades'                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/apple-might-just-ditch-the-3-5mm-headphone-jack-for-good-in-the-upcoming-iphone-7_-249238.html" class="tint" title="Apple Might Just Ditch The 3.5mm Headphone Jack For Good In The Upcoming iPhone 7">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp_1452320006_1452320015_502x234.jpg" border="0" alt="Apple Might Just Ditch The 3.5mm Headphone Jack For Good In The Upcoming iPhone 7" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/apple-might-just-ditch-the-3-5mm-headphone-jack-for-good-in-the-upcoming-iphone-7_-249238.html" title="Apple Might Just Ditch The 3.5mm Headphone Jack For Good In The Upcoming iPhone 7">
                        Apple Might Just Ditch The 3.5mm Headphone Jack For Good In The Upcoming iPhone 7                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/9-actresses-who-performed-stunts-themselves-in-their-films-249217.html" class="tint" title="Shraddha Kapoor Does A Daredevil Stunt From The 34th Floor Of A Building + 9 Actresses Who Performed Their Own Stunts">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/picmonkey-collage_1452240490_1452240496_502x234.jpg" border="0" alt="Shraddha Kapoor Does A Daredevil Stunt From The 34th Floor Of A Building + 9 Actresses Who Performed Their Own Stunts" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/9-actresses-who-performed-stunts-themselves-in-their-films-249217.html" title="Shraddha Kapoor Does A Daredevil Stunt From The 34th Floor Of A Building + 9 Actresses Who Performed Their Own Stunts">
                        Shraddha Kapoor Does A Daredevil Stunt From The 34th Floor Of A Building + 9 Actresses Who Performed Their Own Stunts                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/it-s-not-possible-i-won-t-find-love-5-revelations-by-hrithik-roshan-about-life-249232.html" class="tint" title="'It's Not Possible I Won't Find Love'+ 5 Revelations By Hrithik Roshan About Life!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/62004_1452251256_1452251260_502x234.jpg" border="0" alt="'It's Not Possible I Won't Find Love'+ 5 Revelations By Hrithik Roshan About Life!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/it-s-not-possible-i-won-t-find-love-5-revelations-by-hrithik-roshan-about-life-249232.html" title="'It's Not Possible I Won't Find Love'+ 5 Revelations By Hrithik Roshan About Life!">
                        'It's Not Possible I Won't Find Love'+ 5 Revelations By Hrithik Roshan About Life!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/pisces-this-is-your-complete-horoscope-for-the-year-2016_-249007.html" class="tint" title="Pisces! This Is Your Complete Horoscope For The Year 2016">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp_1451734251_502x234.jpg" border="0" alt="Pisces! This Is Your Complete Horoscope For The Year 2016" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/pisces-this-is-your-complete-horoscope-for-the-year-2016_-249007.html" title="Pisces! This Is Your Complete Horoscope For The Year 2016">
                        Pisces! This Is Your Complete Horoscope For The Year 2016                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/sunny-deol-plans-to-screen-mohalla-assi-the-controversial-film-that-still-awaits-a-release-249228.html" class="tint" title="Sunny Deol Plans To Screen 'Mohalla Assi', The Controversial Film That Still Awaits A Release!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/sunny-card_1452251377_1452251381_502x234.jpg" border="0" alt="Sunny Deol Plans To Screen 'Mohalla Assi', The Controversial Film That Still Awaits A Release!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/sunny-deol-plans-to-screen-mohalla-assi-the-controversial-film-that-still-awaits-a-release-249228.html" title="Sunny Deol Plans To Screen 'Mohalla Assi', The Controversial Film That Still Awaits A Release!">
                        Sunny Deol Plans To Screen 'Mohalla Assi', The Controversial Film That Still Awaits A Release!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/a-day-on-the-roads-of-a-new-delhi-with-the-even-odd-formula-249208.html" class="tint" title="I Took To The Streets Of Delhi To Gift Roses To Even/Odd Offenders And This Is What I Learnt">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card2_1452175753_502x234.jpg" border="0" alt="I Took To The Streets Of Delhi To Gift Roses To Even/Odd Offenders And This Is What I Learnt" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/a-day-on-the-roads-of-a-new-delhi-with-the-even-odd-formula-249208.html" title="I Took To The Streets Of Delhi To Gift Roses To Even/Odd Offenders And This Is What I Learnt">
                        I Took To The Streets Of Delhi To Gift Roses To Even/Odd Offenders And This Is What I Learnt                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/travel/nasa-found-all-26-english-alphabets-hiding-in-plain-sight-on-the-earth-s-surface-249110.html" class="tint" title="NASA Found All 26 English Alphabets Hiding In Plain Sight On The Earth's Surface!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1451991004_502x234.jpg" border="0" alt="NASA Found All 26 English Alphabets Hiding In Plain Sight On The Earth's Surface!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/travel/nasa-found-all-26-english-alphabets-hiding-in-plain-sight-on-the-earth-s-surface-249110.html" title="NASA Found All 26 English Alphabets Hiding In Plain Sight On The Earth's Surface!">
                        NASA Found All 26 English Alphabets Hiding In Plain Sight On The Earth's Surface!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-trailer-of-the-conjuring-2-is-out-and-it-is-creepier-than-its-prequel-249222.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/the-trailer-of-the-conjuring-2-is-out-and-it-is-creepier-than-its-prequel-249222.html" class="tint" title="The Trailer Of 'The Conjuring 2' Is Out And We Could Hardly Watch It!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/conjuring2_card_1452243591_502x234.jpg" border="0" alt="The Trailer Of 'The Conjuring 2' Is Out And We Could Hardly Watch It!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/the-trailer-of-the-conjuring-2-is-out-and-it-is-creepier-than-its-prequel-249222.html" title="The Trailer Of 'The Conjuring 2' Is Out And We Could Hardly Watch It!">
                        The Trailer Of 'The Conjuring 2' Is Out And We Could Hardly Watch It!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/culture/who-we-are/these-husbands-thought-that-their-wives-shop-just-to-be-happy-watch-this-video-to-see-their-reactions-when-they-realized-how-wrong-they-were-249174.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/these-husbands-thought-that-their-wives-shop-just-to-be-happy-watch-this-video-to-see-their-reactions-when-they-realized-how-wrong-they-were-249174.html" class="tint" title="These Husbands Thought That Their Wives Shop Just To Be Happy. Hereâs What Happened When They Found Out The Truth">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/card_1452254099_502x234.jpg" border="0" alt="These Husbands Thought That Their Wives Shop Just To Be Happy. Hereâs What Happened When They Found Out The Truth" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/these-husbands-thought-that-their-wives-shop-just-to-be-happy-watch-this-video-to-see-their-reactions-when-they-realized-how-wrong-they-were-249174.html" title="These Husbands Thought That Their Wives Shop Just To Be Happy. Hereâs What Happened When They Found Out The Truth">
                        These Husbands Thought That Their Wives Shop Just To Be Happy. Hereâs What Happened When They Found Out The Truth                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/watch-lamborghini-enters-severely-flooded-intersection-gets-submerged-zooms-off-like-a-boss-249213.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/watch-lamborghini-enters-severely-flooded-intersection-gets-submerged-zooms-off-like-a-boss-249213.html" class="tint" title="WATCH: Lamborghini Enters Severely Flooded Intersection, Gets Submerged, Zooms Off Like A Boss">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/lamborghini_card_1452234868_502x234.jpg" border="0" alt="WATCH: Lamborghini Enters Severely Flooded Intersection, Gets Submerged, Zooms Off Like A Boss" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/watch-lamborghini-enters-severely-flooded-intersection-gets-submerged-zooms-off-like-a-boss-249213.html" title="WATCH: Lamborghini Enters Severely Flooded Intersection, Gets Submerged, Zooms Off Like A Boss">
                        WATCH: Lamborghini Enters Severely Flooded Intersection, Gets Submerged, Zooms Off Like A Boss                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/6-unbelievable-acts-of-courage-that-made-heroes-out-of-ordinary-people-249199.html" class="tint" title="6 Unbelievable Acts Of Courage That Made Heroes Out Of Ordinary People">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/courage---card2_1452167156_502x234.jpg" border="0" alt="6 Unbelievable Acts Of Courage That Made Heroes Out Of Ordinary People" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/6-unbelievable-acts-of-courage-that-made-heroes-out-of-ordinary-people-249199.html" title="6 Unbelievable Acts Of Courage That Made Heroes Out Of Ordinary People">
                        6 Unbelievable Acts Of Courage That Made Heroes Out Of Ordinary People                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/mumbai-police-reduces-security-cover-of-srk-aamir-khan-and-38-others-in-bollywood-249209.html" class="tint" title="Mumbai Police Reduces Security Cover Of SRK, Aamir Khan, And 38 Others In Bollywood">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card-srk_1452233904_1452233912_502x234.jpg" border="0" alt="Mumbai Police Reduces Security Cover Of SRK, Aamir Khan, And 38 Others In Bollywood" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/mumbai-police-reduces-security-cover-of-srk-aamir-khan-and-38-others-in-bollywood-249209.html" title="Mumbai Police Reduces Security Cover Of SRK, Aamir Khan, And 38 Others In Bollywood">
                        Mumbai Police Reduces Security Cover Of SRK, Aamir Khan, And 38 Others In Bollywood                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/13-little-ways-you-can-make-2016-special-for-yourself-249104.html" class="tint" title="13 Little Ways You Can Make 2016 Special For Yourself">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/1_1451986535_502x234.jpg" border="0" alt="13 Little Ways You Can Make 2016 Special For Yourself" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/13-little-ways-you-can-make-2016-special-for-yourself-249104.html" title="13 Little Ways You Can Make 2016 Special For Yourself">
                        13 Little Ways You Can Make 2016 Special For Yourself                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/aamir-khan-issues-statement-on-incredible-india-says-he-wasn-t-sacked-249201.html" class="tint" title="Aamir Khan Issues Statement On 'Incredible India', Says He Wasn't Sacked">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452168611_502x234.jpg" border="0" alt="Aamir Khan Issues Statement On 'Incredible India', Says He Wasn't Sacked" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/aamir-khan-issues-statement-on-incredible-india-says-he-wasn-t-sacked-249201.html" title="Aamir Khan Issues Statement On 'Incredible India', Says He Wasn't Sacked">
                        Aamir Khan Issues Statement On 'Incredible India', Says He Wasn't Sacked                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/hollywood/vin-diesel-remembers-paul-walker-sings-emotional-tribute-at-people-s-choice-awards-ceremony-249202.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/hollywood/vin-diesel-remembers-paul-walker-sings-emotional-tribute-at-people-s-choice-awards-ceremony-249202.html" class="tint" title="Vin Diesel Remembers Paul Walker, Sings Emotional Tribute At People's Choice Awards Ceremony">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/seeyouagain_card_1452168986_502x234.jpg" border="0" alt="Vin Diesel Remembers Paul Walker, Sings Emotional Tribute At People's Choice Awards Ceremony" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/hollywood/vin-diesel-remembers-paul-walker-sings-emotional-tribute-at-people-s-choice-awards-ceremony-249202.html" title="Vin Diesel Remembers Paul Walker, Sings Emotional Tribute At People's Choice Awards Ceremony">
                        Vin Diesel Remembers Paul Walker, Sings Emotional Tribute At People's Choice Awards Ceremony                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/aquarius-this-is-your-complete-horoscope-for-the-year-2016_-249001.html" class="tint" title="Aquarius! This Is Your Complete Horoscope For The Year 2016!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cp_1451734919_502x234.jpg" border="0" alt="Aquarius! This Is Your Complete Horoscope For The Year 2016!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/aquarius-this-is-your-complete-horoscope-for-the-year-2016_-249001.html" title="Aquarius! This Is Your Complete Horoscope For The Year 2016!">
                        Aquarius! This Is Your Complete Horoscope For The Year 2016!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/10-unforgettable-couplets-by-sahir-ludhianvi-that-are-still-alive-in-our-hearts-249182.html" class="tint" title="10 Unforgettable Couplets By Sahir Ludhianvi That Are Still Alive In Our Hearts">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card-1_1452165903_502x234.jpg" border="0" alt="10 Unforgettable Couplets By Sahir Ludhianvi That Are Still Alive In Our Hearts" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/10-unforgettable-couplets-by-sahir-ludhianvi-that-are-still-alive-in-our-hearts-249182.html" title="10 Unforgettable Couplets By Sahir Ludhianvi That Are Still Alive In Our Hearts">
                        10 Unforgettable Couplets By Sahir Ludhianvi That Are Still Alive In Our Hearts                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-mash-up-of-nahi-saamne-ishq-bina-is-one-of-the-best-tributes-to-a-r-rahman-249193.html" class="tint" title="This Mash-Up Of Nahi Saamne & Ishq Bina Is One Of The Best Tributes To AR Rahman!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/jam-card_1452161447_1452161450_502x234.jpg" border="0" alt="This Mash-Up Of Nahi Saamne & Ishq Bina Is One Of The Best Tributes To AR Rahman!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/this-mash-up-of-nahi-saamne-ishq-bina-is-one-of-the-best-tributes-to-a-r-rahman-249193.html" title="This Mash-Up Of Nahi Saamne & Ishq Bina Is One Of The Best Tributes To AR Rahman!">
                        This Mash-Up Of Nahi Saamne & Ishq Bina Is One Of The Best Tributes To AR Rahman!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/17-images-of-women-from-around-the-world-that-prove-beauty-lies-in-diversity-249107.html" class="tint" title="17 Images Of Women From Around The World That Prove Beauty Lies In Diversity">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card2_1451990775_1451990782_502x234.jpg" border="0" alt="17 Images Of Women From Around The World That Prove Beauty Lies In Diversity" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/17-images-of-women-from-around-the-world-that-prove-beauty-lies-in-diversity-249107.html" title="17 Images Of Women From Around The World That Prove Beauty Lies In Diversity">
                        17 Images Of Women From Around The World That Prove Beauty Lies In Diversity                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/after-winning-people-s-choice-award-priyanka-chopra-now-wants-to-add-another-feather-in-her-cap-249180.html" class="tint" title="After Winning People's Choice Award, Priyanka Chopra Now Wants To Add Another Feather In Her Cap!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452150556_502x234.jpg" border="0" alt="After Winning People's Choice Award, Priyanka Chopra Now Wants To Add Another Feather In Her Cap!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/after-winning-people-s-choice-award-priyanka-chopra-now-wants-to-add-another-feather-in-her-cap-249180.html" title="After Winning People's Choice Award, Priyanka Chopra Now Wants To Add Another Feather In Her Cap!">
                        After Winning People's Choice Award, Priyanka Chopra Now Wants To Add Another Feather In Her Cap!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/14-nostalgic-pictures-of-india-from-when-there-was-no-need-for-the-odd-even-formula-249078.html" class="tint" title="14 Nostalgic Pictures Of India From When There Was No Need For The Odd Even Formula">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/1_1451971819_502x234.jpg" border="0" alt="14 Nostalgic Pictures Of India From When There Was No Need For The Odd Even Formula" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/14-nostalgic-pictures-of-india-from-when-there-was-no-need-for-the-odd-even-formula-249078.html" title="14 Nostalgic Pictures Of India From When There Was No Need For The Odd Even Formula">
                        14 Nostalgic Pictures Of India From When There Was No Need For The Odd Even Formula                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/already-broken-your-new-year-s-diet-resolution-research-says-you-ll-be-more-successful-in-spring-249189.html" class="tint" title="Already Broken Your New Yearâs Diet Resolution? Research Says You'll Be More Successful In Spring!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cover_1452155611_502x234.jpg" border="0" alt="Already Broken Your New Yearâs Diet Resolution? Research Says You'll Be More Successful In Spring!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/already-broken-your-new-year-s-diet-resolution-research-says-you-ll-be-more-successful-in-spring-249189.html" title="Already Broken Your New Yearâs Diet Resolution? Research Says You'll Be More Successful In Spring!">
                        Already Broken Your New Yearâs Diet Resolution? Research Says You'll Be More Successful In Spring!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-hilariously-proves-why-sarojini-nagar-is-a-shopping-paradise-for-every-delhi-girl-249183.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-hilariously-proves-why-sarojini-nagar-is-a-shopping-paradise-for-every-delhi-girl-249183.html" class="tint" title="This Video Hilariously Proves Why Sarojini Nagar Is A Shopping Paradise For Every Delhi Girl!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/snmkt_card_1452153847_502x234.jpg" border="0" alt="This Video Hilariously Proves Why Sarojini Nagar Is A Shopping Paradise For Every Delhi Girl!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-hilariously-proves-why-sarojini-nagar-is-a-shopping-paradise-for-every-delhi-girl-249183.html" title="This Video Hilariously Proves Why Sarojini Nagar Is A Shopping Paradise For Every Delhi Girl!">
                        This Video Hilariously Proves Why Sarojini Nagar Is A Shopping Paradise For Every Delhi Girl!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/eye-to-eye-singer-taher-shah-ready-with-his-second-song-ranveer-singh-are-you-listening-249177.html" class="tint" title="'Eye To Eye' Singer Taher Shah Ready With His Second Song. Ranveer Singh, Are You Listening?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/eye-card_1452150160_1452150184_502x234.jpg" border="0" alt="'Eye To Eye' Singer Taher Shah Ready With His Second Song. Ranveer Singh, Are You Listening?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/eye-to-eye-singer-taher-shah-ready-with-his-second-song-ranveer-singh-are-you-listening-249177.html" title="'Eye To Eye' Singer Taher Shah Ready With His Second Song. Ranveer Singh, Are You Listening?">
                        'Eye To Eye' Singer Taher Shah Ready With His Second Song. Ranveer Singh, Are You Listening?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/10-things-we-ll-always-remember-bipasha-basu-for-249172.html" class="tint" title="10 Things We'll Always Remember Bipasha Basu For!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/picmonkey-collage_1452146678_1452146685_502x234.jpg" border="0" alt="10 Things We'll Always Remember Bipasha Basu For!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/10-things-we-ll-always-remember-bipasha-basu-for-249172.html" title="10 Things We'll Always Remember Bipasha Basu For!">
                        10 Things We'll Always Remember Bipasha Basu For!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/here-s-everything-we-learnt-from-our-free-30-day-trial-on-netflix-249168.html" class="tint" title="Here's Everything We Learnt From Our Free 30-Day Trial On Netflix">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452148006_502x234.jpg" border="0" alt="Here's Everything We Learnt From Our Free 30-Day Trial On Netflix" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/here-s-everything-we-learnt-from-our-free-30-day-trial-on-netflix-249168.html" title="Here's Everything We Learnt From Our Free 30-Day Trial On Netflix">
                        Here's Everything We Learnt From Our Free 30-Day Trial On Netflix                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-makes-india-proud-yet-again-becomes-the-first-south-asian-actress-to-win-a-people-s-choice-award-249167.html" class="tint" title="Priyanka Chopra Makes India Proud Yet Again, Becomes The First South Asian Actress To Win A Peopleâs Choice Award!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452152459_502x234.jpg" border="0" alt="Priyanka Chopra Makes India Proud Yet Again, Becomes The First South Asian Actress To Win A Peopleâs Choice Award!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-makes-india-proud-yet-again-becomes-the-first-south-asian-actress-to-win-a-people-s-choice-award-249167.html" title="Priyanka Chopra Makes India Proud Yet Again, Becomes The First South Asian Actress To Win A Peopleâs Choice Award!">
                        Priyanka Chopra Makes India Proud Yet Again, Becomes The First South Asian Actress To Win A Peopleâs Choice Award!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/2016-begins-on-a-great-note-india-s-first-transgender-band-finally-released-its-first-song-249169.html" class="tint" title="2016 Begins On A Great Note , India's first Transgender Band Finally Released Its First Song!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/pack1_1452145847_1452145861_502x234.jpg" border="0" alt="2016 Begins On A Great Note , India's first Transgender Band Finally Released Its First Song!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/2016-begins-on-a-great-note-india-s-first-transgender-band-finally-released-its-first-song-249169.html" title="2016 Begins On A Great Note , India's first Transgender Band Finally Released Its First Song!">
                        2016 Begins On A Great Note , India's first Transgender Band Finally Released Its First Song!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/12-pictures-that-show-it-will-take-more-than-just-oddeven-to-rid-delhi-of-its-pollution-249138.html" class="tint" title="12 Pictures That Show It Will Take More Than Just #OddEven To Rid Delhi Of Its Pollution">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452077023_1452077036_502x234.jpg" border="0" alt="12 Pictures That Show It Will Take More Than Just #OddEven To Rid Delhi Of Its Pollution" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/12-pictures-that-show-it-will-take-more-than-just-oddeven-to-rid-delhi-of-its-pollution-249138.html" title="12 Pictures That Show It Will Take More Than Just #OddEven To Rid Delhi Of Its Pollution">
                        12 Pictures That Show It Will Take More Than Just #OddEven To Rid Delhi Of Its Pollution                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/11-amazing-uses-for-the-leftover-whey-that-is-leftover-when-you-make-paneer-249142.html" class="tint" title="11 Amazing Uses For The Whey That Gets Leftover When You Make Paneer!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/cover_1452068129_502x234.jpg" border="0" alt="11 Amazing Uses For The Whey That Gets Leftover When You Make Paneer!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/11-amazing-uses-for-the-leftover-whey-that-is-leftover-when-you-make-paneer-249142.html" title="11 Amazing Uses For The Whey That Gets Leftover When You Make Paneer!">
                        11 Amazing Uses For The Whey That Gets Leftover When You Make Paneer!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-bollywood-actresses-pay-packages-revealed-but-it-s-the-men-who-are-raking-in-the-moolah-247365.html" class="tint" title="Deepika Becomes Highest Paid Actress In Bollywood. But It's Still The Men Who Rake In The Moolah!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452074648_502x234.jpg" border="0" alt="Deepika Becomes Highest Paid Actress In Bollywood. But It's Still The Men Who Rake In The Moolah!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/11-bollywood-actresses-pay-packages-revealed-but-it-s-the-men-who-are-raking-in-the-moolah-247365.html" title="Deepika Becomes Highest Paid Actress In Bollywood. But It's Still The Men Who Rake In The Moolah!">
                        Deepika Becomes Highest Paid Actress In Bollywood. But It's Still The Men Who Rake In The Moolah!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-suggestions-that-could-help-make-india-a-better-country-249153.html" class="tint" title="11 Suggestions That Will Help Make India Better">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/12_1452077783_502x234.jpg" border="0" alt="11 Suggestions That Will Help Make India Better" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/11-suggestions-that-could-help-make-india-a-better-country-249153.html" title="11 Suggestions That Will Help Make India Better">
                        11 Suggestions That Will Help Make India Better                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-man-quit-his-mnc-job-became-a-police-constable-just-to-fulfill-his-father-s-dream-hatsoff-249154.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-man-quit-his-mnc-job-became-a-police-constable-just-to-fulfill-his-father-s-dream-hatsoff-249154.html" class="tint" title="This Man Quit His MNC Job & Became A Police Constable Just To Fulfill His Father's Dream! #HatsOff">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/roopesh_card_1452078513_502x234.jpg" border="0" alt="This Man Quit His MNC Job & Became A Police Constable Just To Fulfill His Father's Dream! #HatsOff" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-man-quit-his-mnc-job-became-a-police-constable-just-to-fulfill-his-father-s-dream-hatsoff-249154.html" title="This Man Quit His MNC Job & Became A Police Constable Just To Fulfill His Father's Dream! #HatsOff">
                        This Man Quit His MNC Job & Became A Police Constable Just To Fulfill His Father's Dream! #HatsOff                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/hollywood/vin-diesel-remembers-paul-walker-sings-emotional-tribute-at-people-s-choice-awards-ceremony-249202.html'>video</a>					
                        <a href="http://www.indiatimes.com/entertainment/hollywood/vin-diesel-remembers-paul-walker-sings-emotional-tribute-at-people-s-choice-awards-ceremony-249202.html" class="tint" title="Vin Diesel Remembers Paul Walker, Sings Emotional Tribute At People's Choice Awards Ceremony">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Jan/seeyouagain_card_1452168986_218x102.jpg" border="0" alt="Vin Diesel Remembers Paul Walker, Sings Emotional Tribute At People's Choice Awards Ceremony"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/hollywood/vin-diesel-remembers-paul-walker-sings-emotional-tribute-at-people-s-choice-awards-ceremony-249202.html" title="Vin Diesel Remembers Paul Walker, Sings Emotional Tribute At People's Choice Awards Ceremony">
                            Vin Diesel Remembers Paul Walker, Sings Emotional Tribute At People's Choice Awards Ceremony                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-nostalgic-pictures-of-india-from-when-there-was-no-need-for-the-odd-even-formula-249078.html" class="tint" title="14 Nostalgic Pictures Of India From When There Was No Need For The Odd Even Formula">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/1_1451971819_218x102.jpg" border="0" alt="14 Nostalgic Pictures Of India From When There Was No Need For The Odd Even Formula"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-nostalgic-pictures-of-india-from-when-there-was-no-need-for-the-odd-even-formula-249078.html" title="14 Nostalgic Pictures Of India From When There Was No Need For The Odd Even Formula">
                            14 Nostalgic Pictures Of India From When There Was No Need For The Odd Even Formula                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/10-unforgettable-couplets-by-sahir-ludhianvi-that-are-still-alive-in-our-hearts-249182.html" class="tint" title="10 Unforgettable Couplets By Sahir Ludhianvi That Are Still Alive In Our Hearts">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card-1_1452165903_218x102.jpg" border="0" alt="10 Unforgettable Couplets By Sahir Ludhianvi That Are Still Alive In Our Hearts"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/10-unforgettable-couplets-by-sahir-ludhianvi-that-are-still-alive-in-our-hearts-249182.html" title="10 Unforgettable Couplets By Sahir Ludhianvi That Are Still Alive In Our Hearts">
                            10 Unforgettable Couplets By Sahir Ludhianvi That Are Still Alive In Our Hearts                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/aamir-khan-issues-statement-on-incredible-india-says-he-wasn-t-sacked-249201.html" class="tint" title="Aamir Khan Issues Statement On 'Incredible India', Says He Wasn't Sacked">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/card_1452168611_218x102.jpg" border="0" alt="Aamir Khan Issues Statement On 'Incredible India', Says He Wasn't Sacked"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/aamir-khan-issues-statement-on-incredible-india-says-he-wasn-t-sacked-249201.html" title="Aamir Khan Issues Statement On 'Incredible India', Says He Wasn't Sacked">
                            Aamir Khan Issues Statement On 'Incredible India', Says He Wasn't Sacked                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/this-mash-up-of-nahi-saamne-ishq-bina-is-one-of-the-best-tributes-to-a-r-rahman-249193.html" class="tint" title="This Mash-Up Of Nahi Saamne & Ishq Bina Is One Of The Best Tributes To AR Rahman!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2016/Jan/jam-card_1452161447_1452161450_218x102.jpg" border="0" alt="This Mash-Up Of Nahi Saamne & Ishq Bina Is One Of The Best Tributes To AR Rahman!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/this-mash-up-of-nahi-saamne-ishq-bina-is-one-of-the-best-tributes-to-a-r-rahman-249193.html" title="This Mash-Up Of Nahi Saamne & Ishq Bina Is One Of The Best Tributes To AR Rahman!">
                            This Mash-Up Of Nahi Saamne & Ishq Bina Is One Of The Best Tributes To AR Rahman!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2016/Jan/cat-scores-nerdies_1452423826_1452423839_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/5_1452423288_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/sa1_1452420455_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/afp6_1452419783_1452419787_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/chess_1452419678_1452419684_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/netaji_1452422966_1452422971_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452422183_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/aamir-khan-road-safety_1452422195_1452422201_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/pitch_1452263592_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp_1452320006_1452320015_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp1_1451736227_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/star-card_1452335116_1452335121_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1451991004_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2016/Jan/2_1452418309_1452418313_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452411740_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/afp6_1452410982_1452410985_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/5_1452404307_1452404316_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/loplp_1452409907_1452409913_236x111.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/cover_1452248482_502x234.jpg","http://media.indiatimes.in/media/content/2015/Dec/card_1451558490_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/tin-tin-card_1452410425_1452410433_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/card_1452254099_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/62004_1452251256_1452251260_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/picmonkey-collage_1452240490_1452240496_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/card-srk_1452233904_1452233912_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/card2_1451990775_1451990782_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2016/Jan/jaish-card_1452343344_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452339343_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp_1452343252_1452343260_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452336783_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp_1452334814_1452334818_236x111.jpg","http://media.indiatimes.in/media/content/2016/Jan/kp-card_1452415567_1452415574_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/aa_1452408195_1452408198_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/rocket-card_1452411254_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Dec/cover_1449218606_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/tk12_1452411602_1452411606_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp_1452245538_1452245548_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card-2_1452405600_1452405612_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/hr-card_1452328195_1452328200_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cover_1452246064_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452240511_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/pitch_1452263592_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp_1452342669_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/deep-c_1452339090_1452339097_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp1_1451736227_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/star-card_1452335116_1452335121_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/farhan-akhtar-card_1452185128_1452185142_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1451990186_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp_1452320006_1452320015_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/picmonkey-collage_1452240490_1452240496_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/62004_1452251256_1452251260_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp_1451734251_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/sunny-card_1452251377_1452251381_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card2_1452175753_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1451991004_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/conjuring2_card_1452243591_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/card_1452254099_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/lamborghini_card_1452234868_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/courage---card2_1452167156_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card-srk_1452233904_1452233912_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/1_1451986535_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452168611_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/seeyouagain_card_1452168986_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cp_1451734919_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card-1_1452165903_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/jam-card_1452161447_1452161450_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card2_1451990775_1451990782_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452150556_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/1_1451971819_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cover_1452155611_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/snmkt_card_1452153847_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/eye-card_1452150160_1452150184_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/picmonkey-collage_1452146678_1452146685_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452148006_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452152459_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/pack1_1452145847_1452145861_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452077023_1452077036_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/cover_1452068129_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452074648_502x234.jpg","http://media.indiatimes.in/media/content/2016/Jan/12_1452077783_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/roopesh_card_1452078513_502x234.jpg","http://media.indiatimes.in/media/videocafe/2016/Jan/seeyouagain_card_1452168986_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/1_1451971819_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/card-1_1452165903_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/card_1452168611_218x102.jpg","http://media.indiatimes.in/media/content/2016/Jan/jam-card_1452161447_1452161450_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    4,225,351<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>51,739 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    function socialRHSHide(){}
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.72" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.72"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.72"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.72"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.72"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>