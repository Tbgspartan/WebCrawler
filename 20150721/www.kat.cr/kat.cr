<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-82dc9f3.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-82dc9f3.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-82dc9f3.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-82dc9f3.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-82dc9f3.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '82dc9f3',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-82dc9f3.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom" style="color: #ffeeb4; font-size: 34px; float:left; height: 50px; line-height: 50px;"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080%20ted%202/" class="tag2">1080 ted 2</a>
	<a href="/search/1080p/" class="tag5">1080p</a>
	<a href="/search/1080p/" class="tag4">1080p</a>
	<a href="/search/2014/" class="tag3">2014</a>
	<a href="/search/2015/" class="tag9">2015</a>
	<a href="/search/2015/" class="tag5">2015</a>
	<a href="/search/3d/" class="tag4">3d</a>
	<a href="/search/3d%20remux/" class="tag2">3d remux</a>
	<a href="/search/android/" class="tag9">android</a>
	<a href="/search/android/" class="tag3">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/arrow/" class="tag2">arrow</a>
	<a href="/search/avengers/" class="tag2">avengers</a>
	<a href="/search/avengers%20age%20of%20ultron/" class="tag2">avengers age of ultron</a>
	<a href="/search/bahubali/" class="tag2">bahubali</a>
	<a href="/search/bajrangi%20bhaijaan/" class="tag4">bajrangi bhaijaan</a>
	<a href="/search/batman/" class="tag3">batman</a>
	<a href="/search/discography/" class="tag3">discography</a>
	<a href="/search/flac/" class="tag2">flac</a>
	<a href="/search/french/" class="tag4">french</a>
	<a href="/search/game%20of%20thrones/" class="tag3">game of thrones</a>
	<a href="/search/hamari%20adhuri%20kahani/" class="tag2">hamari adhuri kahani</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi/" class="tag7">hindi</a>
	<a href="/search/hindi%202015/" class="tag2">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag2">inside out</a>
	<a href="/search/insurgent/" class="tag2">insurgent</a>
	<a href="/search/ita/" class="tag3">ita</a>
	<a href="/search/kat%20ph%20com/" class="tag2">kat ph com</a>
	<a href="/search/mad%20max/" class="tag2">mad max</a>
	<a href="/search/malayalam/" class="tag2">malayalam</a>
	<a href="/search/minions/" class="tag3">minions</a>
	<a href="/search/movies/" class="tag3">movies</a>
	<a href="/search/nezu/" class="tag10">nezu</a>
	<a href="/search/nl/" class="tag3">nl</a>
	<a href="/search/power/" class="tag2">power</a>
	<a href="/search/san%20andreas/" class="tag2">san andreas</a>
	<a href="/search/spy%202015/" class="tag2">spy 2015</a>
	<a href="/search/tamil/" class="tag3">tamil</a>
	<a href="/search/ted%202/" class="tag2">ted 2</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/terminator%20genisys/" class="tag2">terminator genisys</a>
	<a href="/search/the%20big%20bang%20theory/" class="tag4">the big bang theory</a>
	<a href="/search/the%20walking%20dead/" class="tag3">the walking dead</a>
	<a href="/search/true%20detective/" class="tag2">true detective</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag4">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag4">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="rsssign" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10952644,0" class="icomment icommentjs icon16" href="/ted-2-2015-uncensored-1080p-hc-hdrip-x264-aac-jyk-t10952644.html#comment"> <em class="iconvalue">120</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ted-2-2015-uncensored-1080p-hc-hdrip-x264-aac-jyk-t10952644.html" class="cellMainLink">Ted 2 2015 UNCENSORED 1080p HC HDRip x264 AAC-JYK</a></div>
			</td>
			<td class="nobr center">2.48 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">16103</td>
			<td class="red lasttd center">10336</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10956888,0" class="icomment icommentjs icon16" href="/terminator-genisys-2015-truefrench-ts-hd-md-xvid-extreme-avi-t10956888.html#comment"> <em class="iconvalue">21</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/terminator-genisys-2015-truefrench-ts-hd-md-xvid-extreme-avi-t10956888.html" class="cellMainLink">Terminator Genisys 2015 TRUEFRENCH TS HD MD XviD EXTREME.avi</a></div>
			</td>
			<td class="nobr center">1.77 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">12453</td>
			<td class="red lasttd center">6319</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10954218,0" class="icomment icommentjs icon16" href="/titanium-2014-french-dvdrip-xvid-extreme-avi-t10954218.html#comment"> <em class="iconvalue">23</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/titanium-2014-french-dvdrip-xvid-extreme-avi-t10954218.html" class="cellMainLink">Titanium 2014 FRENCH DVDRip XviD-EXTREME avi</a></div>
			</td>
			<td class="nobr center">696.22 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">12523</td>
			<td class="red lasttd center">3757</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10954796,0" class="icomment icommentjs icon16" href="/the-last-survivors-2014-720p-brrip-x264-yify-t10954796.html#comment"> <em class="iconvalue">51</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-last-survivors-2014-720p-brrip-x264-yify-t10954796.html" class="cellMainLink">The Last Survivors (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center">756.47 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">5735</td>
			<td class="red lasttd center">3394</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10957901,0" class="icomment icommentjs icon16" href="/bajrangi-bhaijaan-2015-dvd-scr-1-3-rip-ictv-7th-anniversary-exclusive-sparrow-t10957901.html#comment"> <em class="iconvalue">59</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bajrangi-bhaijaan-2015-dvd-scr-1-3-rip-ictv-7th-anniversary-exclusive-sparrow-t10957901.html" class="cellMainLink">Bajrangi Bhaijaan (2015) DVD-SCR-[1-3] rip- IcTv 7th Anniversary Exclusive -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center">1.45 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">3713</td>
			<td class="red lasttd center">4523</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10952309,0" class="icomment icommentjs icon16" href="/lila-and-eve-2015-hdrip-xvid-ac3-evo-t10952309.html#comment"> <em class="iconvalue">62</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lila-and-eve-2015-hdrip-xvid-ac3-evo-t10952309.html" class="cellMainLink">Lila And Eve 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center">1.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">4172</td>
			<td class="red lasttd center">2808</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10960171,0" class="icomment icommentjs icon16" href="/ant-man-2015-full-cam-xvid-ac3-mrg-t10960171.html#comment"> <em class="iconvalue">50</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ant-man-2015-full-cam-xvid-ac3-mrg-t10960171.html" class="cellMainLink">Ant-Man {2015} FULL CAM XVID AC3-MRG</a></div>
			</td>
			<td class="nobr center">1.16 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">3743</td>
			<td class="red lasttd center">2183</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10953156,0" class="icomment icommentjs icon16" href="/x-men-dias-del-futuro-pasado-rogue-cut-spanish-espaÃol-hdrip-xvid-elitetorrent-t10953156.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/x-men-dias-del-futuro-pasado-rogue-cut-spanish-espaÃol-hdrip-xvid-elitetorrent-t10953156.html" class="cellMainLink">X-Men - Dias del futuro pasado Rogue Cut SPANISH ESPAÃOL HDRip XviD-ELITETORRENT</a></div>
			</td>
			<td class="nobr center">2.54 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">3174</td>
			<td class="red lasttd center">1981</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10952301,0" class="icomment icommentjs icon16" href="/far-from-the-madding-crowd-2015-hdrip-xvid-ac3-evo-t10952301.html#comment"> <em class="iconvalue">21</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/far-from-the-madding-crowd-2015-hdrip-xvid-ac3-evo-t10952301.html" class="cellMainLink">Far from the Madding Crowd 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center">1.44 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">2778</td>
			<td class="red lasttd center">2148</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10951345,0" class="icomment icommentjs icon16" href="/chappie-2015-brrip-blu-ray-1080p-5-1ch-dublado-torrent-download-t10951345.html#comment"> <em class="iconvalue">18</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/chappie-2015-brrip-blu-ray-1080p-5-1ch-dublado-torrent-download-t10951345.html" class="cellMainLink">Chappie (2015) BRrip Blu-Ray 1080p 5.1Ch Dublado â Torrent Download</a></div>
			</td>
			<td class="nobr center">2.15 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">3310</td>
			<td class="red lasttd center">1520</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10964936,0" class="icomment icommentjs icon16" href="/ant-man-2015-new-hqcam-x264-aac-mrg-t10964936.html#comment"> <em class="iconvalue">19</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ant-man-2015-new-hqcam-x264-aac-mrg-t10964936.html" class="cellMainLink">ANT-MAN {2015} NEW HQCAM X264 AAC-MRG</a></div>
			</td>
			<td class="nobr center">1.57 <span>GB</span></td>
			<td class="center">17</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">2200</td>
			<td class="red lasttd center">2565</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10961293,0" class="icomment icommentjs icon16" href="/brother-s-keeper-2013-1080p-brrip-x264-yify-t10961293.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/brother-s-keeper-2013-1080p-brrip-x264-yify-t10961293.html" class="cellMainLink">Brother&#039;s Keeper (2013) 1080p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center">1.84 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">2390</td>
			<td class="red lasttd center">2169</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10955587,0" class="icomment icommentjs icon16" href="/ant-man-2015-cam-xvid-mp3-readnfo-mrg-t10955587.html#comment"> <em class="iconvalue">76</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ant-man-2015-cam-xvid-mp3-readnfo-mrg-t10955587.html" class="cellMainLink">ANT-MAN {2015} CAM XVID MP3 READNFO-MRG</a></div>
			</td>
			<td class="nobr center">867.26 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">2439</td>
			<td class="red lasttd center">1314</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10967259,0" class="icomment icommentjs icon16" href="/bajrangi-bhaijaan-2015-576p-dvd-scr-rip-x264-ictv-7th-anniversary-exclusive-sparrow-t10967259.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bajrangi-bhaijaan-2015-576p-dvd-scr-rip-x264-ictv-7th-anniversary-exclusive-sparrow-t10967259.html" class="cellMainLink">Bajrangi Bhaijaan (2015) 576p DvD Scr Rip - X264 - IcTv 7th Anniversary Exclusive -={SPARROW}=</a></div>
			</td>
			<td class="nobr center">1.85 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">10&nbsp;hours</td>
			<td class="green center">341</td>
			<td class="red lasttd center">2419</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10968912,0" class="icomment icommentjs icon16" href="/the-road-within-2014-720p-brrip-x264-yify-t10968912.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-road-within-2014-720p-brrip-x264-yify-t10968912.html" class="cellMainLink">The Road Within (2014) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center">760.97 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">3&nbsp;hours</td>
			<td class="green center">867</td>
			<td class="red lasttd center">1538</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="rsssign" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10964988,0" class="icomment icommentjs icon16" href="/true-detective-s02e05-hdtv-x264-asap-ettv-t10964988.html#comment"> <em class="iconvalue">101</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/true-detective-s02e05-hdtv-x264-asap-ettv-t10964988.html" class="cellMainLink">True Detective S02E05 HDTV x264-ASAP[ettv]</a></div>
			</td>
			<td class="nobr center">326.45 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">25462</td>
			<td class="red lasttd center">6113</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10965679,0" class="icomment icommentjs icon16" href="/wwe-battleground-2015-ppv-webrip-h264-wd-sparrow-t10965679.html#comment"> <em class="iconvalue">75</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/wwe-battleground-2015-ppv-webrip-h264-wd-sparrow-t10965679.html" class="cellMainLink">WWE Battleground 2015 PPV WEBRip h264-WD -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center">2.08 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">19&nbsp;hours</td>
			<td class="green center">8624</td>
			<td class="red lasttd center">6122</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10964965,0" class="icomment icommentjs icon16" href="/the-last-ship-s02e06-hdtv-x264-lol-ettv-t10964965.html#comment"> <em class="iconvalue">61</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-last-ship-s02e06-hdtv-x264-lol-ettv-t10964965.html" class="cellMainLink">The Last Ship S02E06 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center">313.11 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">10701</td>
			<td class="red lasttd center">3398</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10965601,0" class="icomment icommentjs icon16" href="/falling-skies-s05e04-hdtv-x264-killers-ettv-t10965601.html#comment"> <em class="iconvalue">39</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/falling-skies-s05e04-hdtv-x264-killers-ettv-t10965601.html" class="cellMainLink">Falling Skies S05E04 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center">344.53 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">10069</td>
			<td class="red lasttd center">2972</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10965404,0" class="icomment icommentjs icon16" href="/ballers-2015-s01e05-hdtv-x264-asap-ettv-t10965404.html#comment"> <em class="iconvalue">33</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ballers-2015-s01e05-hdtv-x264-asap-ettv-t10965404.html" class="cellMainLink">Ballers 2015 S01E05 HDTV x264-ASAP[ettv]</a></div>
			</td>
			<td class="nobr center">223.64 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">8387</td>
			<td class="red lasttd center">2055</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10964796,0" class="icomment icommentjs icon16" href="/ray-donovan-s03e02-hdtv-x264-lol-ettv-t10964796.html#comment"> <em class="iconvalue">38</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ray-donovan-s03e02-hdtv-x264-lol-ettv-t10964796.html" class="cellMainLink">Ray Donovan S03E02 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center">301.51 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">21&nbsp;hours</td>
			<td class="green center">7598</td>
			<td class="red lasttd center">2157</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10965604,0" class="icomment icommentjs icon16" href="/the-strain-s02e02-720p-hdtv-x264-killers-rartv-t10965604.html#comment"> <em class="iconvalue">14</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-strain-s02e02-720p-hdtv-x264-killers-rartv-t10965604.html" class="cellMainLink">The Strain S02E02 720p HDTV x264-KILLERS[rartv]</a></div>
			</td>
			<td class="nobr center">934.52 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">5771</td>
			<td class="red lasttd center">2195</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10964315,0" class="icomment icommentjs icon16" href="/humans-s01e06-hdtv-x264-river-ettv-t10964315.html#comment"> <em class="iconvalue">30</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/humans-s01e06-hdtv-x264-river-ettv-t10964315.html" class="cellMainLink">Humans S01E06 HDTV x264-RiVER[ettv]</a></div>
			</td>
			<td class="nobr center">283.77 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">5952</td>
			<td class="red lasttd center">1996</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10955735,0" class="icomment icommentjs icon16" href="/dark-matter-s01e06-hdtv-x264-killers-ettv-t10955735.html#comment"> <em class="iconvalue">99</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dark-matter-s01e06-hdtv-x264-killers-ettv-t10955735.html" class="cellMainLink">Dark Matter S01E06 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center">227.27 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">5686</td>
			<td class="red lasttd center">656</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10965607,0" class="icomment icommentjs icon16" href="/the-brink-s01e05-hdtv-x264-killers-ettv-t10965607.html#comment"> <em class="iconvalue">22</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-brink-s01e05-hdtv-x264-killers-ettv-t10965607.html" class="cellMainLink">The Brink S01E05 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center">228.61 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">4571</td>
			<td class="red lasttd center">1167</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10966491,0" class="icomment icommentjs icon16" href="/tut-part-1-720p-hdtv-x264-dhd-ettv-t10966491.html#comment"> <em class="iconvalue">11</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tut-part-1-720p-hdtv-x264-dhd-ettv-t10966491.html" class="cellMainLink">Tut Part 1 720p HDTV x264-DHD[ettv]</a></div>
			</td>
			<td class="nobr center">2.67 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">2001</td>
			<td class="red lasttd center">2139</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10960069,0" class="icomment icommentjs icon16" href="/power-2014-s02e06-hdtv-x264-asap-rartv-t10960069.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/power-2014-s02e06-hdtv-x264-asap-rartv-t10960069.html" class="cellMainLink">Power 2014 S02E06 HDTV x264-ASAP[rartv]</a></div>
			</td>
			<td class="nobr center">310.2 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">3532</td>
			<td class="red lasttd center">602</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10965694,0" class="icomment icommentjs icon16" href="/last-week-tonight-with-john-oliver-s02e21-hdtv-x264-batv-ettv-t10965694.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/last-week-tonight-with-john-oliver-s02e21-hdtv-x264-batv-ettv-t10965694.html" class="cellMainLink">Last Week Tonight With John Oliver S02E21 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center">246.38 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">19&nbsp;hours</td>
			<td class="green center">2832</td>
			<td class="red lasttd center">766</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10965567,0" class="icomment icommentjs icon16" href="/masters-of-sex-s03e02-hdtv-x264-lol-ettv-t10965567.html#comment"> <em class="iconvalue">15</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/masters-of-sex-s03e02-hdtv-x264-lol-ettv-t10965567.html" class="cellMainLink">Masters of Sex S03E02 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center">298.19 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">2659</td>
			<td class="red lasttd center">755</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10958940,0" class="icomment icommentjs icon16" href="/ufc-fight-night-72-hdtv-x264-killers-glodls-t10958940.html#comment"> <em class="iconvalue">23</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-fight-night-72-hdtv-x264-killers-glodls-t10958940.html" class="cellMainLink">UFC Fight Night 72 HDTV x264-KILLERS [GloDLS]</a></div>
			</td>
			<td class="nobr center">1.75 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">2311</td>
			<td class="red lasttd center">599</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="rsssign" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10952533,0" class="icomment icommentjs icon16" href="/va-now-that-s-what-i-call-music-91-2015-mp3-320kbps-h4ckus-glodls-t10952533.html#comment"> <em class="iconvalue">33</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-now-that-s-what-i-call-music-91-2015-mp3-320kbps-h4ckus-glodls-t10952533.html" class="cellMainLink">VA - Now That&#039;s What I Call Music! 91 [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center">367.52 <span>MB</span></td>
			<td class="center">51</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">1084</td>
			<td class="red lasttd center">469</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10951710,0" class="icomment icommentjs icon16" href="/tame-impala-currents-2015-bbm-t10951710.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/tame-impala-currents-2015-bbm-t10951710.html" class="cellMainLink">Tame Impala - Currents (2015) BBM</a></div>
			</td>
			<td class="nobr center">119.92 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">524</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10954446,0" class="icomment icommentjs icon16" href="/va-summer-top-house-music-2015-mp3-320-kbps-t10954446.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-summer-top-house-music-2015-mp3-320-kbps-t10954446.html" class="cellMainLink">VA - Summer Top House Music (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">1.56 <span>GB</span></td>
			<td class="center">129</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">307</td>
			<td class="red lasttd center">192</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10954957,0" class="icomment icommentjs icon16" href="/now-that-s-what-i-call-drive-mp3-divxtotal-t10954957.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/now-that-s-what-i-call-drive-mp3-divxtotal-t10954957.html" class="cellMainLink">NOW ThatÂs What I Call Drive MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">332.63 <span>MB</span></td>
			<td class="center">61</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">381</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10954949,0" class="icomment icommentjs icon16" href="/ed-sheeran-5-ep-5cd-2015-cdrip-t10954949.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ed-sheeran-5-ep-5cd-2015-cdrip-t10954949.html" class="cellMainLink">Ed Sheeran 5 EP [5CD 2015] CDRIP</a></div>
			</td>
			<td class="nobr center">221.87 <span>MB</span></td>
			<td class="center">37</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">299</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10954727,0" class="icomment icommentjs icon16" href="/best-of-disco-mp3-divxtotal-t10954727.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/best-of-disco-mp3-divxtotal-t10954727.html" class="cellMainLink">Best Of Disco MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">349.98 <span>MB</span></td>
			<td class="center">39</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">276</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10958764,0" class="icomment icommentjs icon16" href="/va-golden-disco-dance-rmx-2015-mp3-320-kbps-t10958764.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-golden-disco-dance-rmx-2015-mp3-320-kbps-t10958764.html" class="cellMainLink">VA - Golden Disco Dance RMX (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">699.55 <span>MB</span></td>
			<td class="center">68</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">203</td>
			<td class="red lasttd center">138</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10954973,0" class="icomment icommentjs icon16" href="/mega-dance-music-mp3-divxtotal-t10954973.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/mega-dance-music-mp3-divxtotal-t10954973.html" class="cellMainLink">Mega Dance Music MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">685.09 <span>MB</span></td>
			<td class="center">70</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">219</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10967916,0" class="icomment icommentjs icon16" href="/ibiza-romantic-chillout-and-lounge-mp3-divxtotal-t10967916.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ibiza-romantic-chillout-and-lounge-mp3-divxtotal-t10967916.html" class="cellMainLink">Ibiza Romantic Chillout and Lounge MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">775.48 <span>MB</span></td>
			<td class="center">61</td>
			<td class="center">7&nbsp;hours</td>
			<td class="green center">63</td>
			<td class="red lasttd center">179</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10959445,0" class="icomment icommentjs icon16" href="/the-complete-stax-volt-soul-singles-vol-2-1968-1971-mp3-320-frankfoo-t10959445.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-complete-stax-volt-soul-singles-vol-2-1968-1971-mp3-320-frankfoo-t10959445.html" class="cellMainLink">The Complete Stax-Volt Soul Singles Vol 2 1968-1971 (MP3@320) [FrankFoo]</a></div>
			</td>
			<td class="nobr center">1.52 <span>GB</span></td>
			<td class="center">266</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">161</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-very-best-hits-summer-mp3-divxtotal-t10967891.html" class="cellMainLink">The Very Best Hits Summer MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">156.06 <span>MB</span></td>
			<td class="center">19</td>
			<td class="center">7&nbsp;hours</td>
			<td class="green center">142</td>
			<td class="red lasttd center">54</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10967308,0" class="icomment icommentjs icon16" href="/all-is-well-2015-full-album-mp3-groo-t10967308.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/all-is-well-2015-full-album-mp3-groo-t10967308.html" class="cellMainLink">All Is Well (2015) Full Album Mp3 Groo</a></div>
			</td>
			<td class="nobr center">25.13 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center">10&nbsp;hours</td>
			<td class="green center">112</td>
			<td class="red lasttd center">69</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10967681,0" class="icomment icommentjs icon16" href="/chilled-the-collection-mp3-divxtotal-t10967681.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/chilled-the-collection-mp3-divxtotal-t10967681.html" class="cellMainLink">Chilled - The Collection MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">546.83 <span>MB</span></td>
			<td class="center">55</td>
			<td class="center">8&nbsp;hours</td>
			<td class="green center">89</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/anthems-of-lounge-music-mp3-divxtotal-t10967692.html" class="cellMainLink">Anthems of Lounge Music MP3-DIVXTOTAL</a></div>
			</td>
			<td class="nobr center">828.26 <span>MB</span></td>
			<td class="center">71</td>
			<td class="center">8&nbsp;hours</td>
			<td class="green center">66</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10954356,0" class="icomment icommentjs icon16" href="/va-idol-classic-rock-2015-mp3-320-kbps-t10954356.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-idol-classic-rock-2015-mp3-320-kbps-t10954356.html" class="cellMainLink">VA - Idol Classic Rock (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center">825.16 <span>MB</span></td>
			<td class="center">112</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">52</td>
			<td class="red lasttd center">63</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="rsssign" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10957038,0" class="icomment icommentjs icon16" href="/graandd-theeft-auuttoo-fiivee-update-4-5-rus-eng-w-all-dlcs-repack-by-rg-steamgames-3dm-ver-5-t10957038.html#comment"> <em class="iconvalue">109</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/graandd-theeft-auuttoo-fiivee-update-4-5-rus-eng-w-all-dlcs-repack-by-rg-steamgames-3dm-ver-5-t10957038.html" class="cellMainLink">Graandd Theeft Auuttoo Fiivee [Update 4/5] [RUS | ENG] w/ All DLCs RePack by RG Steamgames | 3DM ver. 5</a></div>
			</td>
			<td class="nobr center">37.97 <span>GB</span></td>
			<td class="center">12</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">650</td>
			<td class="red lasttd center">1290</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10960271,0" class="icomment icommentjs icon16" href="/the-witcher-3-wild-hunt-v1-07-14-dlc-repack-by-rg-steamgames-t10960271.html#comment"> <em class="iconvalue">48</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-witcher-3-wild-hunt-v1-07-14-dlc-repack-by-rg-steamgames-t10960271.html" class="cellMainLink">The Witcher 3: Wild Hunt [v1.07 + 14 DLC] RePack by RG Steamgames</a></div>
			</td>
			<td class="nobr center">21.09 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">567</td>
			<td class="red lasttd center">880</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10959571,0" class="icomment icommentjs icon16" href="/men-of-war-vietnam-v1-00-2-rus-eng-rip-by-rg-mechanics-t10959571.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/men-of-war-vietnam-v1-00-2-rus-eng-rip-by-rg-mechanics-t10959571.html" class="cellMainLink">Men of War - Vietnam [v1.00.2] [RUS | ENG] RiP by RG Mechanics</a></div>
			</td>
			<td class="nobr center">1.73 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">402</td>
			<td class="red lasttd center">150</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10951803,0" class="icomment icommentjs icon16" href="/3dmgame-act-of-aggression-beta-cracked-3dm-t10951803.html#comment"> <em class="iconvalue">28</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/3dmgame-act-of-aggression-beta-cracked-3dm-t10951803.html" class="cellMainLink">3DMGAME-Act of Aggression Beta Cracked-3DM</a></div>
			</td>
			<td class="nobr center">7.51 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">227</td>
			<td class="red lasttd center">259</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10959937,0" class="icomment icommentjs icon16" href="/dragon-ball-xenoverse-bundle-edition-v1-07-dlcs-multi9-fitgirl-repack-100-lossless-t10959937.html#comment"> <em class="iconvalue">16</em><span></span> </a> 					<a class="istill icon16" href="/dragon-ball-xenoverse-bundle-edition-v1-07-dlcs-multi9-fitgirl-repack-100-lossless-t10959937.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/dragon-ball-xenoverse-bundle-edition-v1-07-dlcs-multi9-fitgirl-repack-100-lossless-t10959937.html" class="cellMainLink">Dragon Ball Xenoverse: Bundle Edition (v1.07 + DLCs, MULTI9) [FitGirl Repack, 100% Lossless]</a></div>
			</td>
			<td class="nobr center">7.12 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">82</td>
			<td class="red lasttd center">282</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10959297,0" class="icomment icommentjs icon16" href="/the-chronicles-of-riddick-assault-on-dark-athena-gog-t10959297.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-chronicles-of-riddick-assault-on-dark-athena-gog-t10959297.html" class="cellMainLink">The Chronicles of Riddick: Assault on Dark Athena (GOG)</a></div>
			</td>
			<td class="nobr center">6.12 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">114</td>
			<td class="red lasttd center">235</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10952635,0" class="icomment icommentjs icon16" href="/resident-evil-revelations-2-episode-1-4-v5-0-rus-eng-w-all-dlcs-repack-by-rg-catalyst-t10952635.html#comment"> <em class="iconvalue">25</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/resident-evil-revelations-2-episode-1-4-v5-0-rus-eng-w-all-dlcs-repack-by-rg-catalyst-t10952635.html" class="cellMainLink">Resident Evil Revelations 2: Episode 1-4 [v5.0] [RUS | ENG] w/ All DLCs RePack by RG Catalyst</a></div>
			</td>
			<td class="nobr center">6.72 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">171</td>
			<td class="red lasttd center">172</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10963809,0" class="icomment icommentjs icon16" href="/lords-of-football-complete-prophet-t10963809.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/lords-of-football-complete-prophet-t10963809.html" class="cellMainLink">Lords of Football Complete-PROPHET</a></div>
			</td>
			<td class="nobr center">2.47 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">173</td>
			<td class="red lasttd center">161</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10961897,0" class="icomment icommentjs icon16" href="/f1-2015-r-g-mechanics-t10961897.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/f1-2015-r-g-mechanics-t10961897.html" class="cellMainLink">F1 2015 [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center">7.15 <span>GB</span></td>
			<td class="center">24</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">166</td>
			<td class="red lasttd center">166</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10961373,0" class="icomment icommentjs icon16" href="/project-cars-digital-edition-2015-pc-steam-rip-Ð¾Ñ-let-sÐ lay-t10961373.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/project-cars-digital-edition-2015-pc-steam-rip-Ð¾Ñ-let-sÐ lay-t10961373.html" class="cellMainLink">Project CARS: Digital Edition (2015) PC | Steam-Rip Ð¾Ñ Let&#039;sÐ lay</a></div>
			</td>
			<td class="nobr center">16.29 <span>GB</span></td>
			<td class="center">9</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">139</td>
			<td class="red lasttd center">191</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10968219,0" class="icomment icommentjs icon16" href="/trials-fusion-awesome-level-max-edition-skidrow-t10968219.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/trials-fusion-awesome-level-max-edition-skidrow-t10968219.html" class="cellMainLink">Trials Fusion Awesome Level Max Edition-SKIDROW</a></div>
			</td>
			<td class="nobr center">10.35 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">6&nbsp;hours</td>
			<td class="green center">54</td>
			<td class="red lasttd center">231</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10950783,0" class="icomment icommentjs icon16" href="/portal-stories-mel-v1-02-rus-eng-repack-by-xatab-t10950783.html#comment"> <em class="iconvalue">18</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/portal-stories-mel-v1-02-rus-eng-repack-by-xatab-t10950783.html" class="cellMainLink">Portal Stories Mel [v1.02] [RUS | ENG] RePack by xatab</a></div>
			</td>
			<td class="nobr center">3.92 <span>GB</span></td>
			<td class="center">8</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">225</td>
			<td class="red lasttd center">48</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10958918,0" class="icomment icommentjs icon16" href="/euro-truck-simulator-2-v1-19-0-10s-26-dlc-2-click-run-t10958918.html#comment"> <em class="iconvalue">18</em><span></span> </a> 					<a class="istill icon16" href="/euro-truck-simulator-2-v1-19-0-10s-26-dlc-2-click-run-t10958918.html#stills" title="Torrent Has Screenshots"><span></span></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/euro-truck-simulator-2-v1-19-0-10s-26-dlc-2-click-run-t10958918.html" class="cellMainLink">Euro Truck Simulator 2 v1.19.0.10s (26 DLC)(2-click run)</a></div>
			</td>
			<td class="nobr center">2.4 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">80</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10964260,0" class="icomment icommentjs icon16" href="/need-for-speed-anthology-rus-eng-repack-by-rg-mechanics-t10964260.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/need-for-speed-anthology-rus-eng-repack-by-rg-mechanics-t10964260.html" class="cellMainLink">Need for Speed [ANTHOLOGY] [RUS | ENG] RePack by RG Mechanics</a></div>
			</td>
			<td class="nobr center">42.16 <span>GB</span></td>
			<td class="center">217</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">6</td>
			<td class="red lasttd center">115</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10964258,0" class="icomment icommentjs icon16" href="/gas-guzzlers-duology-rus-eng-repack-by-rg-mechanics-t10964258.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/gas-guzzlers-duology-rus-eng-repack-by-rg-mechanics-t10964258.html" class="cellMainLink">Gas Guzzlers [DUOLOGY] [RUS | ENG] RePack by RG Mechanics</a></div>
			</td>
			<td class="nobr center">8.03 <span>GB</span></td>
			<td class="center">37</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">49</td>
			<td class="red lasttd center">68</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="rsssign" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10962470,0" class="icomment icommentjs icon16" href="/windows-10-rtm-build-10240-16384-pro-home-ms-original-oem-thumperdc-t10962470.html#comment"> <em class="iconvalue">178</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-rtm-build-10240-16384-pro-home-ms-original-oem-thumperdc-t10962470.html" class="cellMainLink">Windows 10 RTM Build 10240 16384 Pro-Home MS Original OEM [ThumperDC]</a></div>
			</td>
			<td class="nobr center">3.81 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">403</td>
			<td class="red lasttd center">866</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10959977,0" class="icomment icommentjs icon16" href="/windows-7-ultimate-sp1-x64-en-us-esd-july-2015-pre-activation-team-os-t10959977.html#comment"> <em class="iconvalue">60</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-7-ultimate-sp1-x64-en-us-esd-july-2015-pre-activation-team-os-t10959977.html" class="cellMainLink">Windows 7 Ultimate Sp1 x64 En-Us ESD July 2015 Pre-Activation=-{TEAM OS}=</a></div>
			</td>
			<td class="nobr center">3.08 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">311</td>
			<td class="red lasttd center">426</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10955648,0" class="icomment icommentjs icon16" href="/windows-8-1-pro-vl-update-3-x64-en-us-esd-july-2015-pre-activate-team-os-t10955648.html#comment"> <em class="iconvalue">48</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-8-1-pro-vl-update-3-x64-en-us-esd-july-2015-pre-activate-team-os-t10955648.html" class="cellMainLink">Windows 8.1 Pro Vl Update 3 x64 En-Us ESD July 2015 Pre-Activate-=TEAM OS=</a></div>
			</td>
			<td class="nobr center">3.79 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">220</td>
			<td class="red lasttd center">329</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10966451,0" class="icomment icommentjs icon16" href="/cinema-4d-studio-r16-win-mac-t10966451.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/cinema-4d-studio-r16-win-mac-t10966451.html" class="cellMainLink">Cinema 4D Studio R16 Win - Mac</a></div>
			</td>
			<td class="nobr center">5.84 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">257</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10953362,0" class="icomment icommentjs icon16" href="/the-keys-for-eset-nod32-kaspersky-avast-dr-web-avira-on-07-16-2015-appzdam-t10953362.html#comment"> <em class="iconvalue">24</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-keys-for-eset-nod32-kaspersky-avast-dr-web-avira-on-07-16-2015-appzdam-t10953362.html" class="cellMainLink">The keys for ESET NOD32, Kaspersky, Avast, Dr.Web, Avira on 07/16/2015 - AppzDam</a></div>
			</td>
			<td class="nobr center">2.8 <span>MB</span></td>
			<td class="center">728</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">215</td>
			<td class="red lasttd center">82</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10950576,0" class="icomment icommentjs icon16" href="/microsoft-office-2013-sp1-x86-pro-plus-vl-multi-13-generation2-t10950576.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/microsoft-office-2013-sp1-x86-pro-plus-vl-multi-13-generation2-t10950576.html" class="cellMainLink">Microsoft Office 2013 SP1 X86 Pro Plus VL MULTi-13 {Generation2}</a></div>
			</td>
			<td class="nobr center">3.36 <span>GB</span></td>
			<td class="center">16</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">143</td>
			<td class="red lasttd center">149</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10954118,0" class="icomment icommentjs icon16" href="/google-nik-collection-1-2-11-0-multilanguage-fix-vvk-at-team-t10954118.html#comment"> <em class="iconvalue">19</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/google-nik-collection-1-2-11-0-multilanguage-fix-vvk-at-team-t10954118.html" class="cellMainLink">Google Nik Collection 1.2.11.0 [Multilanguage] [Fix VVK] [AT-TEAM]</a></div>
			</td>
			<td class="nobr center">429.53 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">198</td>
			<td class="red lasttd center">44</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10953540,0" class="icomment icommentjs icon16" href="/android-apps-games-12-07-15-t10953540.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/android-apps-games-12-07-15-t10953540.html" class="cellMainLink">Android Apps &amp; Games 12.07.15</a></div>
			</td>
			<td class="nobr center">647.24 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">132</td>
			<td class="red lasttd center">99</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10951769,0" class="icomment icommentjs icon16" href="/bitdefender-total-security-2015-32-64bit-neosoft-t10951769.html#comment"> <em class="iconvalue">37</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/bitdefender-total-security-2015-32-64bit-neosoft-t10951769.html" class="cellMainLink">Bitdefender Total Security 2015 (32 &amp; 64bit)-NEOSOFT</a></div>
			</td>
			<td class="nobr center">706.17 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">125</td>
			<td class="red lasttd center">78</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10957765,0" class="icomment icommentjs icon16" href="/windows-xp-professional-sp3-x86-black-edition-2015-7-18-t10957765.html#comment"> <em class="iconvalue">27</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-xp-professional-sp3-x86-black-edition-2015-7-18-t10957765.html" class="cellMainLink">Windows XP Professional SP3 x86 - Black Edition 2015.7.18</a></div>
			</td>
			<td class="nobr center">693.44 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">126</td>
			<td class="red lasttd center">59</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10956651,0" class="icomment icommentjs icon16" href="/red-giant-all-suites-2015-key-appzdam-t10956651.html#comment"> <em class="iconvalue">13</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/red-giant-all-suites-2015-key-appzdam-t10956651.html" class="cellMainLink">Red Giant All Suites 2015 + Key - AppzDam</a></div>
			</td>
			<td class="nobr center">737.49 <span>MB</span></td>
			<td class="center">7202</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">120</td>
			<td class="red lasttd center">45</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/an-update-rollup-updatepack7r2-15-7-20-multilanguage-appzdam-t10967951.html" class="cellMainLink">An update rollup UpdatePack7R2 15.7.20 Multilanguage - AppzDam</a></div>
			</td>
			<td class="nobr center">681.72 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">7&nbsp;hours</td>
			<td class="green center">102</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10958885,0" class="icomment icommentjs icon16" href="/windows-10-build-10240-professional-x86-multi-iso-s-ar-ru-sp-ch-by-whitedeath-teamos-t10958885.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-build-10240-professional-x86-multi-iso-s-ar-ru-sp-ch-by-whitedeath-teamos-t10958885.html" class="cellMainLink">Windows 10 Build 10240 Professional x86 Multi ISO&#039;S AR/RU/SP/CH by:WhiteDeath[TeamOS]</a></div>
			</td>
			<td class="nobr center">11.24 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">19</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10966486,0" class="icomment icommentjs icon16" href="/image-line-fl-studio-12-1-x86-x-64-beta-1-signature-bundle-crack-r4e-deepstatus-t10966486.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/image-line-fl-studio-12-1-x86-x-64-beta-1-signature-bundle-crack-r4e-deepstatus-t10966486.html" class="cellMainLink">Image-Line FL Studio 12.1 x86 x 64 Beta 1 Signature Bundle Crack -r4e [deepstatus]</a></div>
			</td>
			<td class="nobr center">527.76 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">15&nbsp;hours</td>
			<td class="green center">44</td>
			<td class="red lasttd center">64</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10956146,0" class="icomment icommentjs icon16" href="/windows-10-pro-build-10240-sta1-th1-el-gr-x64-by-whitedeath-teamos-t10956146.html#comment"> <em class="iconvalue">27</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-pro-build-10240-sta1-th1-el-gr-x64-by-whitedeath-teamos-t10956146.html" class="cellMainLink">Windows 10 Pro Build 10240 STA1-TH1 el-GR x64 by:WhiteDeath [TeamOS]</a></div>
			</td>
			<td class="nobr center">3.84 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">43</td>
			<td class="red lasttd center">54</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="rsssign" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10962543,0" class="icomment icommentjs icon16" href="/horriblesubs-god-eater-02-720p-mkv-t10962543.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-god-eater-02-720p-mkv-t10962543.html" class="cellMainLink">[HorribleSubs] GOD EATER - 02 [720p].mkv</a></div>
			</td>
			<td class="nobr center">596.38 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">2731</td>
			<td class="red lasttd center">601</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10954524,0" class="icomment icommentjs icon16" href="/leopard-raws-gate-jieitai-kanochi-nite-kaku-tatakaeri-03-raw-mx-1280x720-x264-aac-mp4-t10954524.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-gate-jieitai-kanochi-nite-kaku-tatakaeri-03-raw-mx-1280x720-x264-aac-mp4-t10954524.html" class="cellMainLink">[Leopard-Raws] Gate - Jieitai Kanochi nite, Kaku Tatakaeri - 03 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">542.82 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">1826</td>
			<td class="red lasttd center">705</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10962070,0" class="icomment icommentjs icon16" href="/animerg-dragon-ball-super-03-720p-mkv-t10962070.html#comment"> <em class="iconvalue">43</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-03-720p-mkv-t10962070.html" class="cellMainLink">[AnimeRG] Dragon Ball Super - 03 [720p].mkv</a></div>
			</td>
			<td class="nobr center">334.69 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">2103</td>
			<td class="red lasttd center">366</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10962650,0" class="icomment icommentjs icon16" href="/dragon-ball-super-episode-003-vostfr-mp4-t10962650.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-episode-003-vostfr-mp4-t10962650.html" class="cellMainLink">Dragon Ball Super Episode 003 VOSTFR.mp4</a></div>
			</td>
			<td class="nobr center">246.51 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">1678</td>
			<td class="red lasttd center">197</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10954181,0" class="icomment icommentjs icon16" href="/leopard-raws-idolmaster-cinderella-girls-2nd-01-raw-bs11-1280x720-x264-aac-mp4-t10954181.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-idolmaster-cinderella-girls-2nd-01-raw-bs11-1280x720-x264-aac-mp4-t10954181.html" class="cellMainLink">[Leopard-Raws] Idolmaster - Cinderella Girls 2nd - 01 RAW (BS11 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">228.63 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">977</td>
			<td class="red lasttd center">249</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/island-one-piece-702-vostfr-french-v1-8bit-720p-774f6f33-mp4-t10962030.html" class="cellMainLink">[ISLAND]One Piece 702 [VOSTFR] FRENCH [V1] [8bit] [720p] [774F6F33] .mp4</a></div>
			</td>
			<td class="nobr center">347.43 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">800</td>
			<td class="red lasttd center">426</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-senki-zesshou-symphogear-gx-03-acdc889a-mkv-t10958158.html" class="cellMainLink">[Commie] Senki Zesshou Symphogear GX - 03 [ACDC889A].mkv</a></div>
			</td>
			<td class="nobr center">461.97 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">542</td>
			<td class="red lasttd center">387</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-senki-zesshou-symphogear-gx-03-raw-mbs-1280x720-x264-aac-mp4-t10956307.html" class="cellMainLink">[Leopard-Raws] Senki Zesshou Symphogear GX - 03 RAW (MBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">464.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">552</td>
			<td class="red lasttd center">306</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-the-disappearance-of-nagato-yuki-chan-16-end-mx-1280x720-x264-aac-mp4-t10956305.html" class="cellMainLink">[Leopard-Raws] The Disappearance of Nagato Yuki-chan - 16 END (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">345.53 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">507</td>
			<td class="red lasttd center">301</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10951917,0" class="icomment icommentjs icon16" href="/leopard-raws-ranpo-kitan-game-of-laplace-03-raw-cx-1280x720-x264-aac-mp4-t10951917.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-ranpo-kitan-game-of-laplace-03-raw-cx-1280x720-x264-aac-mp4-t10951917.html" class="cellMainLink">[Leopard-Raws] Ranpo Kitan - Game of Laplace - 03 RAW (CX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">449.75 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">571</td>
			<td class="red lasttd center">186</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10951918,0" class="icomment icommentjs icon16" href="/leopard-raws-joukamachi-no-dandelion-03-raw-tbs-1280x720-x264-aac-mp4-t10951918.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-joukamachi-no-dandelion-03-raw-tbs-1280x720-x264-aac-mp4-t10951918.html" class="cellMainLink">[Leopard-Raws] Joukamachi no Dandelion - 03 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">310.29 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">581</td>
			<td class="red lasttd center">138</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-chaos-dragon-sekiryuu-seneki-03-raw-bs11-1280x720-x264-aac-mp4-t10951915.html" class="cellMainLink">[Leopard-Raws] Chaos Dragon - Sekiryuu Seneki - 03 RAW (BS11 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">403.58 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">499</td>
			<td class="red lasttd center">141</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-kyoukai-no-rinne-16-raw-nhke-1280x720-x264-aac-mp4-t10957344.html" class="cellMainLink">[Leopard-Raws] Kyoukai no Rinne - 16 RAW (NHKE 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">346.68 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">372</td>
			<td class="red lasttd center">149</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10956306,0" class="icomment icommentjs icon16" href="/leopard-raws-shokugeki-no-souma-15-raw-tbs-1280x720-x264-aac-mp4-t10956306.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-shokugeki-no-souma-15-raw-tbs-1280x720-x264-aac-mp4-t10956306.html" class="cellMainLink">[Leopard-Raws] Shokugeki no Souma - 15 RAW (TBS 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">431.28 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">423</td>
			<td class="red lasttd center">94</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-denpa-kyoushi-tv-15-raw-ntv-1280x720-x264-aac-mp4-t10957569.html" class="cellMainLink">[Leopard-Raws] Denpa Kyoushi TV - 15 RAW (NTV 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center">311.77 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">363</td>
			<td class="red lasttd center">138</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="rsssign" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10963051,0" class="icomment icommentjs icon16" href="/do-not-open-an-encyclopedia-of-the-world-s-best-kept-secrets-dk-publishing-2010-pdf-gooner-t10963051.html#comment"> <em class="iconvalue">10</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/do-not-open-an-encyclopedia-of-the-world-s-best-kept-secrets-dk-publishing-2010-pdf-gooner-t10963051.html" class="cellMainLink">Do Not Open - An Encyclopedia of the World&#039;s Best-Kept Secrets (DK Publishing) (2010).pdf Gooner</a></div>
			</td>
			<td class="nobr center">48.87 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">884</td>
			<td class="red lasttd center">124</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10952021,0" class="icomment icommentjs icon16" href="/computer-gadget-gamer-magazines-july-17-2015-true-pdf-t10952021.html#comment"> <em class="iconvalue">12</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/computer-gadget-gamer-magazines-july-17-2015-true-pdf-t10952021.html" class="cellMainLink">Computer Gadget &amp; Gamer Magazines - July 17 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">344.35 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">541</td>
			<td class="red lasttd center">163</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10964571,0" class="icomment icommentjs icon16" href="/assorted-magazines-bundle-july-20-2015-true-pdf-t10964571.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-july-20-2015-true-pdf-t10964571.html" class="cellMainLink">Assorted Magazines Bundle - July 20 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">476.37 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center">22&nbsp;hours</td>
			<td class="green center">349</td>
			<td class="red lasttd center">327</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10964690,0" class="icomment icommentjs icon16" href="/photography-magazines-bundle-july-20-2015-true-pdf-t10964690.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/photography-magazines-bundle-july-20-2015-true-pdf-t10964690.html" class="cellMainLink">Photography Magazines Bundle - July 20 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">126.89 <span>MB</span></td>
			<td class="center">7</td>
			<td class="center">22&nbsp;hours</td>
			<td class="green center">410</td>
			<td class="red lasttd center">95</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10960773,0" class="icomment icommentjs icon16" href="/home-garden-magazines-july-19-2015-true-pdf-t10960773.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-garden-magazines-july-19-2015-true-pdf-t10960773.html" class="cellMainLink">Home &amp; Garden Magazines - July 19 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">531.61 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">306</td>
			<td class="red lasttd center">140</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10956396,0" class="icomment icommentjs icon16" href="/automobile-magazines-bundle-july-18-2015-true-pdf-t10956396.html#comment"> <em class="iconvalue">8</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/automobile-magazines-bundle-july-18-2015-true-pdf-t10956396.html" class="cellMainLink">Automobile Magazines Bundle - July 18 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">727.12 <span>MB</span></td>
			<td class="center">29</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">255</td>
			<td class="red lasttd center">138</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10960837,0" class="icomment icommentjs icon16" href="/food-magazines-bundle-july-19-2015-true-pdf-t10960837.html#comment"> <em class="iconvalue">9</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/food-magazines-bundle-july-19-2015-true-pdf-t10960837.html" class="cellMainLink">Food Magazines Bundle - July 19 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">170.87 <span>MB</span></td>
			<td class="center">8</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">320</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10965537,0" class="icomment icommentjs icon16" href="/motorcycle-magazines-bundle-july-20-2015-true-pdf-t10965537.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/motorcycle-magazines-bundle-july-20-2015-true-pdf-t10965537.html" class="cellMainLink">Motorcycle Magazines Bundle - July 20 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">314.31 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center">20&nbsp;hours</td>
			<td class="green center">198</td>
			<td class="red lasttd center">100</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10959698,0" class="icomment icommentjs icon16" href="/justice-league-000-042-tpbs-v01-v05-trinity-war-extras-2011-ongoing-digital-minutemen-empire-nem-t10959698.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/justice-league-000-042-tpbs-v01-v05-trinity-war-extras-2011-ongoing-digital-minutemen-empire-nem-t10959698.html" class="cellMainLink">Justice League (000-042+TPBs v01-v05+ Trinity War &amp; Extras) (2011-Ongoing) (Digital) (Minutemen+Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center">4.56 <span>GB</span></td>
			<td class="center">79</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">139</td>
			<td class="red lasttd center">152</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10965661,0" class="icomment icommentjs icon16" href="/tabloid-magazines-bundle-july-20-2015-true-pdf-t10965661.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/tabloid-magazines-bundle-july-20-2015-true-pdf-t10965661.html" class="cellMainLink">Tabloid Magazines Bundle - July 20 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center">254.43 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center">19&nbsp;hours</td>
			<td class="green center">178</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10963935,0" class="icomment icommentjs icon16" href="/superman-000-041-tpbs-extras-2011-ongoing-megan-g85-thornn-empire-nem-t10963935.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/superman-000-041-tpbs-extras-2011-ongoing-megan-g85-thornn-empire-nem-t10963935.html" class="cellMainLink">Superman (000-041+ TPBs &amp; Extras) (2011-Ongoing) (Megan+G85+Thornn-Empire) (- Nem -)</a></div>
			</td>
			<td class="nobr center">4.42 <span>GB</span></td>
			<td class="center">66</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">80</td>
			<td class="red lasttd center">167</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/encyclopedia-of-aerospace-engineering-2010-pdf-gooner-t10969059.html" class="cellMainLink">Encyclopedia of Aerospace Engineering (2010).pdf Gooner</a></div>
			</td>
			<td class="nobr center">743.75 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center">3&nbsp;hours</td>
			<td class="green center">1</td>
			<td class="red lasttd center">153</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10967411,0" class="icomment icommentjs icon16" href="/slave-labor-graphics-amaze-ink-comics-collection-from-demonoid-fully-seeded-original-by-billygatez-newlife-given-by-nem-t10967411.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/slave-labor-graphics-amaze-ink-comics-collection-from-demonoid-fully-seeded-original-by-billygatez-newlife-given-by-nem-t10967411.html" class="cellMainLink">!Slave Labor Graphics - Amaze Ink Comics Collection (from Demonoid) (Fully Seeded) (Original by BillyGatez - NewLife given by - Nem -)</a></div>
			</td>
			<td class="nobr center">6.1 <span>GB</span></td>
			<td class="center">504</td>
			<td class="center">9&nbsp;hours</td>
			<td class="green center">15</td>
			<td class="red lasttd center">111</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10952646,0" class="icomment icommentjs icon16" href="/mega-man-001-051-tpbs-v01-v07-extras-2011-ongoing-scans-digital-nem-t10952646.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/mega-man-001-051-tpbs-v01-v07-extras-2011-ongoing-scans-digital-nem-t10952646.html" class="cellMainLink">Mega Man (001-051+TPBs v01-v07 &amp; Extras) (2011-Ongoing) (Scans+digital) (- Nem -)</a></div>
			</td>
			<td class="nobr center">2.18 <span>GB</span></td>
			<td class="center">65</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">36</td>
			<td class="red lasttd center">37</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10960588,0" class="icomment icommentjs icon16" href="/brian-mcclellan-promise-of-blood-powder-mage-1-t10960588.html#comment"> <em class="iconvalue">4</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/brian-mcclellan-promise-of-blood-powder-mage-1-t10960588.html" class="cellMainLink">Brian McClellan - Promise of Blood (Powder Mage #1)</a></div>
			</td>
			<td class="nobr center">525.82 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">18</td>
			<td class="red lasttd center">39</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="rsssign" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad">torrent name</th>
			<th class="center">size</th>
			<th class="center">files</th>
			<th class="center">age</th>
			<th class="center">seed</th>
			<th class="lasttd nobr center">leech</th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10960768,0" class="icomment icommentjs icon16" href="/lana-del-rey-collection-flac-vtwin88cube-t10960768.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/lana-del-rey-collection-flac-vtwin88cube-t10960768.html" class="cellMainLink">Lana Del Rey Collection {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">2.55 <span>GB</span></td>
			<td class="center">129</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">99</td>
			<td class="red lasttd center">154</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10960698,0" class="icomment icommentjs icon16" href="/the-cars-original-album-series-5cd-box-2012-flac-t10960698.html#comment"> <em class="iconvalue">7</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-cars-original-album-series-5cd-box-2012-flac-t10960698.html" class="cellMainLink">The Cars - Original Album Series (5CD-Box 2012) [FLAC]</a></div>
			</td>
			<td class="nobr center">1.48 <span>GB</span></td>
			<td class="center">54</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">176</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10960771,0" class="icomment icommentjs icon16" href="/future-ds2-dirty-sprite-2-deluxe-edition-2015-l-audio-l-album-track-l-flac-version-l-sn3h1t87-t10960771.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/future-ds2-dirty-sprite-2-deluxe-edition-2015-l-audio-l-album-track-l-flac-version-l-sn3h1t87-t10960771.html" class="cellMainLink">Future - DS2 (Dirty Sprite 2) [Deluxe Edition] (2015) l Audio l Album Track l FLAC Version l sn3h1t87</a></div>
			</td>
			<td class="nobr center">371.82 <span>MB</span></td>
			<td class="center">22</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">121</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10967721,0" class="icomment icommentjs icon16" href="/pink-floyd-bbc-archives-1974-dark-side-of-the-moon-live-sbd-flac-t10967721.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/pink-floyd-bbc-archives-1974-dark-side-of-the-moon-live-sbd-flac-t10967721.html" class="cellMainLink">Pink Floyd - BBC Archives 1974 (Dark Side of The Moon live SBD) [FLAC]</a></div>
			</td>
			<td class="nobr center">430.91 <span>MB</span></td>
			<td class="center">30</td>
			<td class="center">8&nbsp;hours</td>
			<td class="green center">143</td>
			<td class="red lasttd center">66</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10952604,0" class="icomment icommentjs icon16" href="/bobby-womack-original-album-classics-3cd-box-2011-flac-t10952604.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/bobby-womack-original-album-classics-3cd-box-2011-flac-t10952604.html" class="cellMainLink">Bobby Womack - Original Album Classics (3CD-Box 2011) [FLAC]</a></div>
			</td>
			<td class="nobr center">848.7 <span>MB</span></td>
			<td class="center">65</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">137</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10954310,0" class="icomment icommentjs icon16" href="/miles-davis-live-evil-1971-2014-24-96-hd-flac-t10954310.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/miles-davis-live-evil-1971-2014-24-96-hd-flac-t10954310.html" class="cellMainLink">Miles Davis - Live-Evil (1971; 2014) [24-96 HD; FLAC]</a></div>
			</td>
			<td class="nobr center">2.21 <span>GB</span></td>
			<td class="center">21</td>
			<td class="center">3&nbsp;days</td>
			<td class="green center">104</td>
			<td class="red lasttd center">27</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10966692,0" class="icomment icommentjs icon16" href="/smokie-original-remastered-albums-1975-1982-2007-2008-flac-t10966692.html#comment"> <em class="iconvalue">6</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/smokie-original-remastered-albums-1975-1982-2007-2008-flac-t10966692.html" class="cellMainLink">Smokie - Original Remastered Albums [1975-1982] (2007-2008) FLAC</a></div>
			</td>
			<td class="nobr center">3.71 <span>GB</span></td>
			<td class="center">154</td>
			<td class="center">14&nbsp;hours</td>
			<td class="green center">73</td>
			<td class="red lasttd center">50</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10956114,0" class="icomment icommentjs icon16" href="/traffic-1970-john-barleycorn-must-die-web-24-96-flac-t10956114.html#comment"> <em class="iconvalue">5</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/traffic-1970-john-barleycorn-must-die-web-24-96-flac-t10956114.html" class="cellMainLink">Traffic 1970 John Barleycorn Must Die [WEB 24-96 FLAC]</a></div>
			</td>
			<td class="nobr center">840.84 <span>MB</span></td>
			<td class="center">11</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">90</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10961477,0" class="icomment icommentjs icon16" href="/ronnie-earl-the-broadcasters-father-s-day-2015-flac-t10961477.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/ronnie-earl-the-broadcasters-father-s-day-2015-flac-t10961477.html" class="cellMainLink">Ronnie Earl &amp; The Broadcasters - Father&#039;s Day (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center">596.3 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">83</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10956531,0" class="icomment icommentjs icon16" href="/super-retro-mix-80x-90x-ver-50x50-vol-1-26-t10956531.html#comment"> <em class="iconvalue">3</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/super-retro-mix-80x-90x-ver-50x50-vol-1-26-t10956531.html" class="cellMainLink">Super Retro Mix 80x - 90x (ver.50x50) vol.1-26</a></div>
			</td>
			<td class="nobr center">19.65 <span>GB</span></td>
			<td class="center">349</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">32</td>
			<td class="red lasttd center">71</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10967698,0" class="icomment icommentjs icon16" href="/savoy-brown-the-collection-1993-flac-vtwin88cube-t10967698.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/savoy-brown-the-collection-1993-flac-vtwin88cube-t10967698.html" class="cellMainLink">Savoy Brown - The Collection (1993) {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">876.2 <span>MB</span></td>
			<td class="center">38</td>
			<td class="center">8&nbsp;hours</td>
			<td class="green center">55</td>
			<td class="red lasttd center">35</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10962896,0" class="icomment icommentjs icon16" href="/extreme-ii-pornograffitti-a-funked-up-fairytale-flac-mp3-big-papi-1990-hard-rock-t10962896.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/extreme-ii-pornograffitti-a-funked-up-fairytale-flac-mp3-big-papi-1990-hard-rock-t10962896.html" class="cellMainLink">Extreme II; Pornograffitti (A Funked Up Fairytale)[FLAC+MP3](Big Papi) 1990 Hard Rock</a></div>
			</td>
			<td class="nobr center">562.88 <span>MB</span></td>
			<td class="center">32</td>
			<td class="center">1&nbsp;day</td>
			<td class="green center">62</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="10955554,0" class="icomment icommentjs icon16" href="/va-ministry-of-sound-chilled-house-ibiza-2015-2015-flac-t10955554.html#comment"> <em class="iconvalue">1</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-ministry-of-sound-chilled-house-ibiza-2015-2015-flac-t10955554.html" class="cellMainLink">VA - Ministry Of Sound - Chilled House Ibiza 2015 (2015) FLAC</a></div>
			</td>
			<td class="nobr center">2.18 <span>GB</span></td>
			<td class="center">40</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">38</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="10956554,0" class="icomment icommentjs icon16" href="/1971-1978-nazareth-t10956554.html#comment"> <em class="iconvalue">2</em><span></span> </a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/1971-1978-nazareth-t10956554.html" class="cellMainLink">1971 - 1978 Nazareth</a></div>
			</td>
			<td class="nobr center">3.37 <span>GB</span></td>
			<td class="center">116</td>
			<td class="center">2&nbsp;days</td>
			<td class="green center">29</td>
			<td class="red lasttd center">43</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/loaded-for-bear-the-best-of-ted-nugent-the-amboy-dukes-1999-flac-vtwin88cube-t10968465.html" class="cellMainLink">Loaded For Bear The Best Of Ted Nugent &amp; The Amboy Dukes (1999) {FLAC} vtwin88cube</a></div>
			</td>
			<td class="nobr center">444.66 <span>MB</span></td>
			<td class="center">23</td>
			<td class="center">5&nbsp;hours</td>
			<td class="green center">30</td>
			<td class="red lasttd center">23</td>
        </tr>
			</table>


		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
                
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=16689391">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/jesusuchrist/">jesusuchrist</a></span></span> 52&nbsp;sec.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/love-yify-can-t-find-these-they-re-here-thread-69404/?unread=16689390">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Love Yify,but can&#039;t find these? They&#039;re here!!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_2"><a class="plain" href="/user/Sphinctone1/">Sphinctone1</a></span></span> 2&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/hello-kat-thread-108721/?unread=16689387">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Hello Kat !
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/SparXuTorrent/">SparXuTorrent</a></span></span> 7&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/please-request-ebooks-and-audio-books-here-v10/?unread=16689385">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Please request ebooks and audio books here. V10
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_verified"><a class="plain" href="/user/Angel_of_Deth/">Angel_of_Deth</a></span></span> 10&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/adopt-uploader-program-v11-all-users-help/?unread=16689382">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				**Adopt an uploader Program v11-- For all Users to Help**
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_2"><a class="plain" href="/user/steelballz/">steelballz</a></span></span> 17&nbsp;min.&nbsp;ago</span>
	</li>
		<li>
		<a href="/community/show/monthly-music-series/?unread=16689373">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Monthly Music Series
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/Opeth666/">Opeth666</a></span></span> 23&nbsp;min.&nbsp;ago</span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents 2&nbsp;months&nbsp;ago</span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents 3&nbsp;months&nbsp;ago</span>
	</li>
	<li>
		<a href="/blog/post/technical-maintenance-march-17/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Technical maintenance (March, 17)
			</p>
		</a>
		<span class="explanation">by KickassTorrents 4&nbsp;months&nbsp;ago</span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/steelballz/post/for-the-love-of/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> for the love of...</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/steelballz/">steelballz</a> 22&nbsp;hours&nbsp;ago</span></li>
	<li><a href="/blog/ZombieQueen/post/dear-mr-gooner/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Dear Mr.Gooner.......</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/ZombieQueen/">ZombieQueen</a> yesterday</span></li>
	<li><a href="/blog/deep-60/post/can-you-spot-my-tits/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> Can you spot my tits?</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/deep-60/">deep-60</a> yesterday</span></li>
	<li><a href="/blog/left2month/post/the-death-of-the-sim-card-is-nigh-what-is-an-e-sim-and-how-will-it-change-smartphones-for-the-better/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> The Death of the SIM Card Is Nigh - What is an e-SIM and how will it change smartphones for the better?</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/left2month/">left2month</a> yesterday</span></li>
	<li><a href="/blog/Star.Sapphire/post/the-web-part-1/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> The Web: Part 1</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Star.Sapphire/">Star.Sapphire</a> yesterday</span></li>
	<li><a href="/blog/Touro73/post/new-movie-and-blu-ray-releases-week-30-july-20-july-26/"><i class="ka ka16 ka-comment latest-icon"></i><p class="latest-title"> New Movie and Blu-ray releases Week 30 (July 20 - July 26)</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/Touro73/">Touro73</a> 2&nbsp;days&nbsp;ago</span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/lucy%201080p/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				lucy 1080p
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ass%20everywhere%204/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ass everywhere 4
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ryu%20pov/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				ryu pov
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/x%20art%20mila%20k%20au%20paradis/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				x art mila k au paradis
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/ambassadeurs/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Ambassadeurs
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/directia%205/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				directia 5
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/rome%20gold/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				rome gold
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/enlightened%2Bs02/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				enlightened+s02
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/fixvts/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				fixvts
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/siren/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				Siren
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/vicha/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				vicha
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>
</body>
</html>
