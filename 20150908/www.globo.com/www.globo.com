



<!DOCTYPE html>

<!--[if IE]><![endif]--><html><head><meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /><title>globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento</title><meta name="google-site-verification" content="BKmmuVQac1JM6sKlj3IoXQvffyIRJvJfbicMouA2a88" /><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5" /><meta property="og:title" content="globo.com - Absolutamente tudo sobre notÃ­cias, esportes e entretenimento"/><meta property="og:type" content="website"/><meta property="og:url" content="http://www.globo.com/"/><meta property="og:image" content="http://s.glbimg.com/en/ho/static/globocom2012/img/fb_marca.png"/><meta property="og:site_name" content="globo.com"/><meta property="og:description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta property="fb:page_id" content="224969370851736" /><meta property='busca:title' content='Globo.com' /><meta property='busca:species' content='Home' /><meta property='busca:issued' content='07/09/2015 21:21:41' /><meta property='busca:modified' content='07/09/2015 21:21:41' /><meta property='busca:publisher' content='www.globo.com' /><meta name="description" content="SÃ³ na globo.com vocÃª encontra tudo sobre o conteÃºdo e marcas do Grupo Globo. O melhor acervo de vÃ­deos online sobre entretenimento, esportes e jornalismo do Brasil."/><meta name="keywords" content="NotÃ­cias, Entretenimento, Esporte, Tecnologia, Portal, ConteÃºdo, Rede Globo, TV Globo, VÃ­deos, TelevisÃ£o"/><meta name="application-name" content="Globo.com"/><meta name="msapplication-TileColor" content="#0669DE"/><meta name="msapplication-TileImage" content="http://s.glbimg.com/en/ho/static/globocom2012/img/globo-win-tile.png"/><link rel="canonical" href="http://www.globo.com/" /><link rel="shortcut icon" href="http://s.glbimg.com/en/ho/static/globocom2012/img/favicon.png" type="image/ico" /><link rel="apple-touch-icon-precomposed" sizes="57x57" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-ipad.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://s.glbimg.com/en/ho/static/touchphone/img/apple-touch-icon-iphone-retina.png" /><link rel="stylesheet" href="http://s.glbimg.com/en/ho/static/CACHE/css/3c48889f8f35.css" type="text/css" media="screen" /><!--[if lt IE 10]><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/d783f8b1c48f.js"></script><![endif]--><script type="text/javascript" src="http://s.glbimg.com/en/ho/static/CACHE/js/12ba0da8a716.js"></script></head><!--[if lt IE 7 ]><body class="ie ie6"><![endif]--><!--[if IE 7 ]><body class="ie ie7"><![endif]--><!--[if IE 8 ]><body class="ie ie8"><![endif]--><!--[if IE 9 ]><body class="ie ie9gt ie9"><![endif]--><!--[if gt IE 9 ]><body class="ie ie9gt ie10gt"><![endif]--><!--[if !IE]><!--><body><!--<![endif]--><div id="home-push-menu"><div class="home-push-menu-container"><div class="home-push-menu-initial"><div class="home-push-menu-header">NAVEGUE</div><ul class="barra-itens" data-analytics-context="Link Produto"><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://g1.globo.com" accesskey="n" class="barra-item-g1 link-produto analytics-area analytics-id-T">g1</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://globoesporte.globo.com" accesskey="e" class="barra-item-globoesporte link-produto analytics-area analytics-id-T">globoesporte</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://gshow.globo.com" accesskey="t" class="barra-item-gshow link-produto analytics-area analytics-id-T">gshow</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://famosos.globo.com" accesskey="t" class="barra-item-famosos-etc link-produto analytics-area analytics-id-T">famosos &amp; etc</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://techtudo.com.br" accesskey="b" class="barra-item-tech link-produto analytics-area analytics-id-T">tecnologia</a></li><li class="analytics-product analytics-multi-product"><div class="v-separator"></div><a target="_top" href="http://video.globo.com" accesskey="v" class="barra-item-videos link-produto analytics-area analytics-id-T">vÃ­deos</a></li><li><a href="#" id="home-push-menu-show-email">e-mail</a><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="2.423,0 0,2.399 5.153,7.5 0,12.602 2.423,15 10,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-right.png"></image></svg></span></li><li><a href="https://meuperfil.globo.com/">central globo.com</a></li></ul></div><div class="home-push-menu-email"><ul><li><span class="arrow"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="15px" viewBox="0 0 10 15" enable-background="new 0 0 10 15" xml:space="preserve"><polygon fill="#DCDBDB" points="7.577,0 10,2.399 4.847,7.5 10,12.602 7.577,15 0,7.5 "></polygon><image src="http://s.glbimg.com/gl/ba/img/common/arrow-left.png"></image></svg></span><a id="home-push-menu-hide-email" href="#">e-mail</a></li><li><a href="https://login.globo.com/login/1948">globomail free</a></li><li><a href="https://login.globo.com/login/1">globomail pro</a></li></ul></div></div></div><div id="home-pagecontent" class=" home-menu-rendered" style=""><script type="text/javascript">
function comScore(C){var A="comScore",j=document,y=j.location,B="",z="undefined",x=2048,D,v,i,w,c="characterSet",l="defaultCharset",u=(typeof encodeURIComponent!=z?encodeURIComponent:escape);if(j.cookie.indexOf(A+"=")!=-1){i=j.cookie.split(";");for(w=0,y=i.length;w<y;w++){var d=i[w].indexOf(A+"=");if(d!=-1){B="&"+unescape(i[w].substring(d+A.length+1))}}}C=C+"&ns__t="+(new Date().getTime());C=C+"&ns_c="+(j[c]?j[c]:(j[l]?j[l]:""))+"&c8="+u(j.title)+B+"&c7="+u(y&&y.href?y.href:j.URL)+"&c9="+u(j.referrer);if(C.length>x&&C.indexOf("&")>0){D=C.substr(0,x-8).lastIndexOf("&");C=(C.substring(0,D)+"&ns_cut="+u(C.substring(D+1))).substr(0,x)}if(j.images){v=new Image();if(typeof ns_p==z){ns_p=v}v.src=C}else{j.write('<p><img src="'+C+'" height="1" width="1" alt="*"/></p>')}}comScore("http"+(document.location.href.charAt(4)=="s"?"s://sb":"://b")+".scorecardresearch.com/p?c1=2&c2=6035227");</script><noscript><p><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6035227&amp;c4=http%3A%2F%2Fwww.globo.com%2Findex.html" height="1" width="1" alt="*"/></p></noscript><script>
libby.loadScript('http://b.scorecardresearch.com/c2/6035227/cs.js');</script><script type="text/javascript">
var utag_data = {"structure_tree": "[\"globocom\"]", "ad_site_page": "globo.com/globo.com/home", "page_name": "index"};</script><script type="text/javascript">
(function (a,b,c,d) {a = '//tags.globo.com/utag/globo/home/prod/utag.js';b=document;c = 'script';d=b.createElement(c);d.src=a;d.type = 'text/java' + c;d.async=true;a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);})();
</script><header><div id="base-container-width-element" class="container"><div class="header-mobile-top analytics-area analytics-id-H"><a id="open-menu" class="open-menu" href="#"><svg version="1.1" class="burger-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19px" height="13px" viewBox="0 0 19 13" enable-background="new 0 0 19 13" xml:space="preserve"><g><path fill="#1063E0" d="M17.493,5H1.485C0.665,5,0,5.665,0,6.485C0,7.305,0.665,7.97,1.485,7.97h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,5.665,18.313,5,17.493,5z M1.485,2.971h16.008c0.82,0,1.486-0.666,1.486-1.486C18.979,0.666,18.313,0,17.493,0H1.485C0.665,0,0,0.666,0,1.484C0,2.305,0.665,2.971,1.485,2.971z M17.493,10H1.485C0.665,10,0,10.666,0,11.485c0,0.82,0.665,1.485,1.485,1.485h16.008c0.82,0,1.486-0.665,1.486-1.485C18.979,10.666,18.313,10,17.493,10z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/menu-button.png"></image></svg></a><a class="logo-globo analytics-area analytics-id-T" href="http://globo.com" title="globo.com"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 98 22" enable-background="new 0 0 98 22" xml:space="preserve"><g><path fill="#1063E0" d="M92.982,5.033c-1.115,0-2.549,0.601-3.459,1.625c-0.865-1.069-2.026-1.625-3.392-1.625c-1.205,0-2.252,0.489-3.049,1.336c-0.114-0.757-0.661-1.203-1.502-1.203c-0.957,0-1.639,0.712-1.639,1.781v8.551c0,1.069,0.682,1.781,1.639,1.781s1.638-0.712,1.638-1.781V9.909c0-1.292,0.592-2.205,1.936-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.682,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.909c0-1.292,0.728-2.205,1.934-2.205c1.184,0,1.912,0.913,1.912,2.205v5.589c0,1.069,0.683,1.781,1.638,1.781c0.957,0,1.64-0.712,1.64-1.781V9.352C97.466,6.547,95.645,5.033,92.982,5.033z M10.036,5.166c-0.91,0-1.434,0.49-1.639,1.336c-0.614-0.98-1.889-1.469-3.004-1.469C2.093,5.033,0,8.039,0,11.268c0,3.184,1.616,6.146,5.052,6.146c1.502,0,2.617-0.535,3.299-1.515h0.045v0.757c0,1.625-1.183,2.538-2.776,2.538c-1.889,0-3.049-1.001-4.097-1.001c-0.637,0-1.319,0.8-1.319,1.403C0.205,21.354,4.028,22,5.371,22c3.732,0,6.304-1.938,6.304-5.791V6.947C11.674,5.878,10.991,5.166,10.036,5.166z M5.826,14.474c-1.707,0-2.549-1.559-2.549-3.273c0-1.492,0.842-3.229,2.549-3.229c1.797,0,2.571,1.781,2.571,3.339C8.397,12.871,7.578,14.474,5.826,14.474z M14.318,0c-0.956,0-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781s1.639-0.712,1.639-1.781V1.782C15.957,0.712,15.274,0,14.318,0z M22.479,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169c3.527,0,5.872-2.829,5.872-6.169C28.35,7.994,25.938,5.033,22.479,5.033z M22.479,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.797-3.273,2.594-3.273c1.798,0,2.594,1.737,2.594,3.273C25.073,12.782,24.254,14.474,22.479,14.474z M35.371,5.033c-1.138,0-2.253,0.378-3.118,1.113V1.782C32.253,0.712,31.57,0,30.614,0s-1.639,0.712-1.639,1.782v13.716c0,1.069,0.683,1.781,1.639,1.781c0.911,0,1.434-0.49,1.639-1.336c0.614,0.979,1.889,1.471,3.004,1.471c3.3,0,5.393-3.008,5.393-6.036C40.65,8.261,38.897,5.033,35.371,5.033z M34.825,14.474c-1.798,0-2.572-1.781-2.572-3.34c0-1.559,0.819-3.162,2.572-3.162c1.707,0,2.548,1.759,2.548,3.273C37.373,12.737,36.531,14.474,34.825,14.474z M46.863,5.033c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.344,6.169,5.871,6.169s5.872-2.829,5.872-6.169C52.735,7.994,50.322,5.033,46.863,5.033z M46.863,14.474c-1.775,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273s2.594,1.737,2.594,3.273C49.458,12.782,48.638,14.474,46.863,14.474z M55.425,13.939c-0.978,0-1.774,0.779-1.774,1.736s0.796,1.738,1.774,1.738c0.979,0,1.775-0.781,1.775-1.738S56.404,13.939,55.425,13.939z M66.583,13.805c-0.386,0-1.616,0.669-2.436,0.669c-1.774,0-2.753-1.648-2.753-3.229c0-1.648,0.888-3.273,2.753-3.273c1.048,0,1.776,0.623,2.594,0.623c0.775,0,1.253-0.868,1.253-1.536c0-1.56-2.709-2.026-4.006-2.026c-3.459,0-5.871,2.961-5.871,6.212c0,3.34,2.343,6.169,5.871,6.169c1.184,0,4.006-0.558,4.006-2.206C67.994,14.451,67.355,13.805,66.583,13.805z M73.446,5.033c-3.459,0-5.872,2.961-5.872,6.212c0,3.34,2.344,6.169,5.872,6.169c3.527,0,5.871-2.829,5.871-6.169C79.317,7.994,76.904,5.033,73.446,5.033z M73.446,14.474c-1.774,0-2.594-1.691-2.594-3.229c0-1.536,0.796-3.273,2.594-3.273c1.797,0,2.594,1.737,2.594,3.273C76.04,12.782,75.221,14.474,73.446,14.474z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/logo.png"></image></svg></a><form id="search-form" action="http://www.globo.com/busca/" method="get"><div id="search-container" class="search-container"><span><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><g><path fill="#999999" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/search-small.png"></image></svg></span><div class="search-internal-container"><input type="text" id="search-globo" name="q" placeholder="encontre na globo.com"></div><a id="close-search" class="close-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11px" height="11px" viewBox="0 0 11 11" enable-background="new 0 0 11 11" xml:space="preserve"><g><path fill="#666766" d="M6.884,5.501c1.955-1.955,3.733-3.732,3.809-3.808c0.388-0.388,0.388-1.015,0-1.403 c-0.387-0.386-1.015-0.386-1.402,0C9.215,0.365,7.437,2.144,5.482,4.099c-1.947-1.947-3.714-3.714-3.79-3.79 c-0.387-0.388-1.015-0.388-1.402,0c-0.387,0.387-0.387,1.016,0,1.401c0.076,0.076,1.842,1.843,3.79,3.791 C2.135,7.446,0.372,9.209,0.297,9.285c-0.387,0.387-0.387,1.015,0,1.401c0.387,0.389,1.015,0.389,1.402,0 c0.075-0.075,1.838-1.838,3.784-3.783c1.953,1.952,3.727,3.727,3.802,3.802c0.387,0.387,1.015,0.387,1.402,0 c0.387-0.387,0.387-1.015,0-1.402C10.611,9.228,8.837,7.454,6.884,5.501z"></path></g><image src="http://s.glbimg.com/gl/ba/img/common/close.png"></image></svg></a></div><a id="open-search" class="open-search" href="#"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path fill="#1063e0" d="M15.49,13.286l-3.099-3.09C13.027,9.174,13.4,7.973,13.4,6.683C13.4,2.992,10.4,0,6.7,0S0,2.992,0,6.683 c0,3.69,3,6.683,6.7,6.683c1.248,0,2.413-0.347,3.413-0.939l3.119,3.111c0.623,0.621,1.634,0.621,2.258,0 C16.113,14.915,16.113,13.909,15.49,13.286z M6.699,11.278c-2.543,0-4.606-2.058-4.606-4.595c0-2.538,2.062-4.594,4.606-4.594 c2.544,0,4.607,2.056,4.607,4.594C11.306,9.22,9.243,11.278,6.699,11.278z"></path><image src="http://s.glbimg.com/gl/ba/img/common/search.png"></image></svg></a></form></div><h1 class="analytics-area analytics-id-H"><a class="logo-topo analytics-area analytics-id-T" href="/" title="globo.com">
                globo.com
            </a></h1><div class="complementos analytics-area analytics-id-T"><div id="busca-padrao"><form action="http://www.globo.com/busca/" method="get" accept-charset="utf-8"><fieldset><legend>buscar</legend><label for="busca-campo" class="label-for-search">buscar</label><input id="busca-campo" type="text" name="q" autocomplete="off" accesskey="s" lang="pt-BR" x-webkit-speech speech /><button type="submit">buscar</button></fieldset></form></div><div id="libby-box-previsao-tempo" class="analytics-area analytics-id-L"></div></div><nav><div class="spacer"></div><script type='text/javascript'> window.SETTINGS=window.SETTINGS || {};window.SETTINGS.optimizelyId = "2223881511";window.OptimizelyAbCallback.includeABScript(SETTINGS.optimizelyId); </script>



<ul id="home-menu" class="nested widget-menu">
    
        <li class="g1 analytics-product analytics-multi-product">
            <a href="http://g1.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">g1</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://g1.globo.com/" data-menu-id="g1">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">g1</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="jornais-revistas-e-radio">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">jornais, revistas e rÃ¡dio</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="servicos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">serviÃ§os</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-g1">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-g1" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/carros/">
                                                <span class="titulo">Carros</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/">
                                                <span class="titulo">G1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/economia/">
                                                <span class="titulo">Economia</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://fantastico.globo.com/">
                                                <span class="titulo">FantÃ¡stico</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/mundo/">
                                                <span class="titulo">Mundo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/concursos-e-emprego/">
                                                <span class="titulo">Concursos e Empregos</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/politica/">
                                                <span class="titulo">PolÃ­tica</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/">
                                                <span class="titulo">Jornal Extra</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/">
                                                <span class="titulo">Jornal O Globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://g1.globo.com/jornal-hoje/">
                                                <span class="titulo">Jornal Hoje</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="g1">
                                        <div class="submenu-title">g1</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Carros</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/concursos-e-emprego/">
                                                    <span class="titulo">Concursos e Empregos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/">
                                                    <span class="titulo">Economia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/educacao/">
                                                    <span class="titulo">EducaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/imposto-de-renda/2015/index.html">
                                                    <span class="titulo">Imposto de Renda 2015</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/mundo/">
                                                    <span class="titulo">Mundo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/planeta-bizarro/">
                                                    <span class="titulo">Planeta Bizarro</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/politica/">
                                                    <span class="titulo">PolÃ­tica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/">
                                                    <span class="titulo">Pop &amp; Arte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/tecnologia/">
                                                    <span class="titulo">Tecnologia &amp; Games</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/">
                                                    <span class="titulo">Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bom-dia-brasil/">
                                                    <span class="titulo">Bom dia Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://fantastico.globo.com/">
                                                    <span class="titulo">FantÃ¡stico</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/agronegocios/">
                                                    <span class="titulo">Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/globo-news/">
                                                    <span class="titulo">GloboNews</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/hora1">
                                                    <span class="titulo">Hora 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-da-globo/">
                                                    <span class="titulo">Jornal da Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-hoje/">
                                                    <span class="titulo">Jornal Hoje</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/jornal-nacional/">
                                                    <span class="titulo">Jornal Nacional</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="jornais-revistas-e-radio">
                                        <div class="submenu-title">jornais, revistas e rÃ¡dio</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/">
                                                    <span class="titulo">Jornal Extra</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/">
                                                    <span class="titulo">Jornal O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://cbn.globoradio.globo.com/home/HOME.htm">
                                                    <span class="titulo">RÃ¡dio CBN</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaautoesporte.globo.com/">
                                                    <span class="titulo">Revista Auto Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaepoca.globo.com/">
                                                    <span class="titulo">Revista Ãpoca</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagalileu.globo.com/">
                                                    <span class="titulo">Revista Galileu</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistagloborural.globo.com/">
                                                    <span class="titulo">Revista Globo Rural</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radioglobo.globoradio.globo.com/">
                                                    <span class="titulo">RÃ¡dio Globo</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="servicos">
                                        <div class="submenu-title">serviÃ§os</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/agenda.html">
                                                    <span class="titulo">Agenda de Shows</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/">
                                                    <span class="titulo">Conversor de Moedas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">
                                                    <span class="titulo">CotaÃ§Ãµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/">
                                                    <span class="titulo">Enem e Vestibular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/loteria/">
                                                    <span class="titulo">Loterias</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/previsao-do-tempo.html">
                                                    <span class="titulo">PrevisÃ£o do tempo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/carros/tabela-fipe/index.html">
                                                    <span class="titulo">Tabela FIPE</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://educacao.globo.com/telecurso/">
                                                    <span class="titulo">Telecurso</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.techtudo.com.br/velocimetro.html">
                                                    <span class="titulo">VelocÃ­metro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-g1"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="globoesporte analytics-product analytics-multi-product">
            <a href="http://globoesporte.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">globoesporte</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-globoesporte">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/" data-menu-id="globoesportecom">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globoesporte.com</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://sportv.globo.com/" data-menu-id="sportv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sportv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com" data-menu-id="na-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">na tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://globoesporte.globo.com/futebol/" data-menu-id="futebol">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">futebol</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="mais-esportes">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mais esportes</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-globoesporte">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-globoesporte" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/cartola-fc/">
                                                <span class="titulo">Cartola FC</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                <span class="titulo">FÃ³rmula 1</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                <span class="titulo">Futebol Internacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/">
                                                <span class="titulo">Globo Esporte</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/tenis/">
                                                <span class="titulo">TÃªnis</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://globoesporte.globo.com/futebol/">
                                                <span class="titulo">Futebol Nacional</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://sportv.globo.com/site/combate/">
                                                <span class="titulo">MMA</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="globoesportecom">
                                        <div class="submenu-title">globoesporte.com</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://futpedia.globo.com/">
                                                    <span class="titulo">FutpÃ©dia</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/cartola-fc/">
                                                    <span class="titulo">Cartola FC</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="sportv">
                                        <div class="submenu-title">sportv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/bolao/">
                                                    <span class="titulo">BolÃ£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="na-tv">
                                        <div class="submenu-title">na tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/eventos/combate/">
                                                    <span class="titulo">Combate</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/corujao-do-esporte/">
                                                    <span class="titulo">CorujÃ£o do Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/programas/esporte-espetacular/">
                                                    <span class="titulo">Esporte Espetacular</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/">
                                                    <span class="titulo">Globo Esporte</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://app.globoesporte.globo.com/tv/planeta-extremo/">
                                                    <span class="titulo">Planeta Extremo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sociopremiere.globo.com/">
                                                    <span class="titulo">Premiere</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/">
                                                    <span class="titulo">SporTV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/lutas/tuf-brasil/">
                                                    <span class="titulo">The Ultimate Fighter Brasil</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="futebol">
                                        <div class="submenu-title">futebol</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie A</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie B</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/brasileirao-serie-c/">
                                                    <span class="titulo">BrasileirÃ£o SÃ©rie C</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-america/">
                                                    <span class="titulo">Copa AmÃ©rica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/copa-do-brasil/">
                                                    <span class="titulo">Copa do Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/futebol-internacional/">
                                                    <span class="titulo">Futebol Internacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/">
                                                    <span class="titulo">Futebol Nacional</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">
                                                    <span class="titulo">Liga dos CampeÃµes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/futebol/selecao-brasileira/">
                                                    <span class="titulo">SeleÃ§Ã£o Brasileira</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="mais-esportes">
                                        <div class="submenu-title">mais esportes</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/atletismo/">
                                                    <span class="titulo">Atletismo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/basquete/">
                                                    <span class="titulo">Basquete</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eu-atleta/">
                                                    <span class="titulo">Eu Atleta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/formula-1/">
                                                    <span class="titulo">FÃ³rmula 1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/eventos/futsal/">
                                                    <span class="titulo">Futsal</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://sportv.globo.com/site/combate/">
                                                    <span class="titulo">MMA</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/natacao/">
                                                    <span class="titulo">NataÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/motor/stock-car/">
                                                    <span class="titulo">StockCar</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/tenis/">
                                                    <span class="titulo">TÃªnis</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://globoesporte.globo.com/volei/">
                                                    <span class="titulo">VÃ´lei</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-globoesporte"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="gshow analytics-product analytics-multi-product">
            <a href="http://gshow.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">gshow</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/" data-menu-id="gshow">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gshow</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="programas">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">programas</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="series">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">sÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://gshow.globo.com/programas/webseries/" data-menu-id="webseries">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">websÃ©ries</span>
                                    </a>
                                </li>
                            
                                <li class="has-link has-children">
                                    <a href="http://redeglobo.globo.com/" data-menu-id="rede-globo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">rede globo</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-gshow">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-gshow" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/mais-voce/">
                                                <span class="titulo">Mais VocÃª</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/programacao.html">
                                                <span class="titulo">ProgramaÃ§Ã£o</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/">
                                                <span class="titulo">Gshow</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                <span class="titulo">Encontro com FÃ¡tima</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                <span class="titulo">CaldeirÃ£o do Huck</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                <span class="titulo">VÃ­deo Show</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://redeglobo.globo.com/">
                                                <span class="titulo">Rede globo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                <span class="titulo">Estrelas</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                <span class="titulo">Programa do JÃ´</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="gshow">
                                        <div class="submenu-title">gshow</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Bastidores/">
                                                    <span class="titulo">Bastidores</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/como-fazer/">
                                                    <span class="titulo">Como Fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo">
                                                    <span class="titulo">Estilo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/especial-blog/gshow-troll/1.html">
                                                    <span class="titulo">Gshow Troll</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/participe/">
                                                    <span class="titulo">Participe</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas-da-Ana-Maria/">
                                                    <span class="titulo">Receitas da Ana Maria</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/tv/plantao/">
                                                    <span class="titulo">TV</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="programas">
                                        <div class="submenu-title">programas</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://gshow.globo.com/programas/altas-horas/">
                                                    <span class="titulo">Altas Horas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/caldeirao-do-huck/">
                                                    <span class="titulo">CaldeirÃ£o do Huck</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/domingao-do-faustao/">
                                                    <span class="titulo">DomingÃ£o do FaustÃ£o</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/e-de-casa/">
                                                    <span class="titulo">Ã de Casa</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/encontro-com-fatima-bernardes/">
                                                    <span class="titulo">Encontro com FÃ¡tima</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/esquenta/">
                                                    <span class="titulo">Esquenta</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/estrelas/index.html">
                                                    <span class="titulo">Estrelas</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/">
                                                    <span class="titulo">Mais VocÃª</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/programa-do-jo/">
                                                    <span class="titulo">Programa do JÃ´</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/video-show/index.html">
                                                    <span class="titulo">VÃ­deo Show</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="series">
                                        <div class="submenu-title">sÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/amorteamo/">
                                                    <span class="titulo">Amorteamo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/chapa-quente/">
                                                    <span class="titulo">Chapa Quente</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/dupla-identidade/index.html">
                                                    <span class="titulo">Dupla Identidade</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">
                                                    <span class="titulo">Felizes para sempre?</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/os-experientes/">
                                                    <span class="titulo">Os Experientes</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/pe-na-cova/index.html">
                                                    <span class="titulo">PÃ© na Cova</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/especial-blog/ta-no-ar-a-tv-na-tv/1.html">
                                                    <span class="titulo">TÃ¡ no ar: a TV na TV </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/series/tapas-e-beijos/">
                                                    <span class="titulo">Tapas &amp; Beijos</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/zorra/">
                                                    <span class="titulo">Zorra</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="webseries">
                                        <div class="submenu-title">websÃ©ries</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/como-fazer/no-ar.html">
                                                    <span class="titulo">Como fazer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/dulce-delight/">
                                                    <span class="titulo">Dulce Delight</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/look-do-dia-com-tia-suelly/no-ar.html">
                                                    <span class="titulo">Look do Dia com Tia Suelly</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/marrom-combina-com-tudo/no-ar.html">
                                                    <span class="titulo">Marrom Combina com Tudo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/morri-na-tv/no-ar.html">
                                                    <span class="titulo">Morri na TV</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sebastiana-quebra-galho/no-ar.html">
                                                    <span class="titulo">Sebastiana Quebra-Galho</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/sete-vidas-casos-reais/">
                                                    <span class="titulo">Sete Vidas: casos reais</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/o-incrivel-superonix/no-ar.html">
                                                    <span class="titulo">SuperÃnix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/tome-prumo/">
                                                    <span class="titulo">Tome Prumo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/webseries/vlog-da-valeska/no-ar.html">
                                                    <span class="titulo">Vlog da Valeska</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="rede-globo">
                                        <div class="submenu-title">rede globo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/programacao.html">
                                                    <span class="titulo">ProgramaÃ§Ã£o</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-gshow"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="famosos-etc analytics-product analytics-multi-product">
            <a href="http://famosos.globo.com" class="analytics-area analytics-id-T">
                <span class="titulo">famosos &amp; etc</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                                <li class="has-children">
                                    <a data-menu-id="top-famosos-etc">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <div class='barrinhas'>
                                            <div class='primeira barrinha'></div>
                                            <div class='segunda barrinha'></div>
                                            <div class='terceira barrinha'></div>
                                        </div>
                                        <span class="titulo">
                                            top pÃ¡ginas
                                        </span>
                                    </a>
                                </li>
                            
                            
                                <li class="has-link has-children">
                                    <a href="http://famosos.globo.com/" data-menu-id="famosos">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">famosos</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="moda-estilo">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">moda & estilo</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="bem-estar-saude">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">bem-estar & saÃºde</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="casa-decoracao">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">casa & decoraÃ§Ã£o</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="gastronomia">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">gastronomia</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="musica-cinema-e-teatro">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">mÃºsica, cinema e teatro</span>
                                    </a>
                                </li>
                            
                                <li class=" has-children">
                                    <a  data-menu-id="canais-de-tv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">canais de tv</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-famosos-etc">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                        <ul class="submenu">
                            <li data-menu-id="top-famosos-etc" class="top-menu-home">
                                <div class="submenu-title">top pÃ¡ginas</div>
                                <div class="top-link">
                                    
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://ego.globo.com/">
                                                <span class="titulo">Ego</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://paparazzo.globo.com/">
                                                <span class="titulo">Paparazzo</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://revistaquem.globo.com/">
                                                <span class="titulo">Quem</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://oglobo.globo.com/cultura/kogut/">
                                                <span class="titulo">Patricia Kogut</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://extra.globo.com/famosos/">
                                                <span class="titulo">Retratos da Vida</span>
                                            </a>
                                        </div>
                                    
                                        <div>
                                            <div class="bullet"></div>
                                            <a href="http://famosos.globo.com/">
                                                <span class="titulo">Famosos</span>
                                            </a>
                                        </div>
                                    
                                </div>
                            </li>
                            
                                
                                    <li data-menu-id="famosos">
                                        <div class="submenu-title">famosos</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/">
                                                    <span class="titulo">Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://paparazzo.globo.com/">
                                                    <span class="titulo">Paparazzo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://oglobo.globo.com/cultura/kogut/">
                                                    <span class="titulo">Patricia Kogut</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaquem.globo.com/">
                                                    <span class="titulo">Quem</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://extra.globo.com/famosos/">
                                                    <span class="titulo">Retratos da Vida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="moda-estilo">
                                        <div class="submenu-title">moda &amp; estilo</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/moda/index.html">
                                                    <span class="titulo">Moda no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ela.oglobo.globo.com/">
                                                    <span class="titulo">Ela no O Globo</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Estilo/">
                                                    <span class="titulo">Estilo no Gshow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistaglamour.globo.com/">
                                                    <span class="titulo">Revista Glamour</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gq.globo.com/">
                                                    <span class="titulo">Revista GQ Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistamarieclaire.globo.com/">
                                                    <span class="titulo">Revista Marie Claire </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/moda/">
                                                    <span class="titulo">Moda no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://vogue.globo.com/">
                                                    <span class="titulo">Revista Vogue</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="bem-estar-saude">
                                        <div class="submenu-title">bem-estar &amp; saÃºde</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://ego.globo.com/beleza/index.html">
                                                    <span class="titulo">Beleza no Ego</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/beleza/">
                                                    <span class="titulo">Beleza no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.com.br/bemestar">
                                                    <span class="titulo">Bem Estar no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacrescer.globo.com/">
                                                    <span class="titulo">Revista Crescer</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/bemestar/">
                                                    <span class="titulo">Bem Estar no G1</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/maes-e-filhos/">
                                                    <span class="titulo">MÃ£es no GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/">
                                                    <span class="titulo">Meus 5 Minutos </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://horoscopo.ego.globo.com/">
                                                    <span class="titulo">HorÃ³scopo no Ego</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="casa-decoracao">
                                        <div class="submenu-title">casa &amp; decoraÃ§Ã£o</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/">
                                                    <span class="titulo">Revista Casa e Jardim</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/casa-e-decoracao/">
                                                    <span class="titulo">Casa no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://casavogue.globo.com/">
                                                    <span class="titulo">Casa Vogue </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revista.zapimoveis.com.br/?utm_source=globo.com-etc&amp;utm_medium=botao-casa-decoracao&amp;utm_campaign=Revista-Home">
                                                    <span class="titulo">Revista do Zap</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="gastronomia">
                                        <div class="submenu-title">gastronomia</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/receitas/">
                                                    <span class="titulo">Receitas no GNT </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/programas/mais-voce/Receitas/index.html">
                                                    <span class="titulo">Receitas no Mais VocÃª </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/receitas/">
                                                    <span class="titulo">Receitas.com</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://meus5minutos.globo.com/blogs/Colheradas/">
                                                    <span class="titulo">Colheradas </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://revistacasaejardim.globo.com/Casa-e-Comida/">
                                                    <span class="titulo">Casa e Comida</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="musica-cinema-e-teatro">
                                        <div class="submenu-title">mÃºsica, cinema e teatro</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/pop-arte/cinema/">
                                                    <span class="titulo">G1 Cinema</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://g1.globo.com/musica/">
                                                    <span class="titulo">G1 MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gshow.globo.com/Musica/">
                                                    <span class="titulo">Gshow MÃºsica</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://musica.com.br/">
                                                    <span class="titulo">MÃºsica.com.br</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://radiobeat.com.br/">
                                                    <span class="titulo">RADIOBEAT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://www.agentesevenoteatro.com.br/">
                                                    <span class="titulo">Teatro</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                                
                                    <li data-menu-id="canais-de-tv">
                                        <div class="submenu-title">canais de tv</div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://redeglobo.globo.com/">
                                                    <span class="titulo">Rede Globo </span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalbrasil.globo.com/">
                                                    <span class="titulo">Canal Brasil</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://mundogloob.globo.com/">
                                                    <span class="titulo">Gloob</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://gnt.globo.com/">
                                                    <span class="titulo">GNT</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://megapix.globo.com/">
                                                    <span class="titulo">Megapix</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://multishow.globo.com/">
                                                    <span class="titulo">Multishow</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://telecine.globo.com/">
                                                    <span class="titulo">Telecine</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://uc.globo.com/">
                                                    <span class="titulo">Universal Channel</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href="http://canalviva.globo.com/">
                                                    <span class="titulo">Viva</span>
                                                </a>
                                            </div>
                                        
                                            <div>
                                                <div class="bullet"></div>
                                                <a href=" http://canaloff.globo.com">
                                                    <span class="titulo">Off</span>
                                                </a>
                                            </div>
                                        
                                    </li>
                                
                            
                            <li class="search-area" data-menu-id="search-area-famosos-etc"></li>
                        </ul>
                        <div class="extra analytics-custom-product analytics-id-W"></div>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
        <li class="tecnologia analytics-product analytics-multi-product">
            <a href="http://www.techtudo.com.br/" class="analytics-area analytics-id-T">
                <span class="titulo">tecnologia</span>

        
        
            </a>
        </li>
        
    
        <li class="videos analytics-product analytics-multi-product">
            <a href="http://globotv.globo.com/" class="analytics-area analytics-id-T">
                <span class="titulo">vÃ­deos</span>

        
        

                <span class="seta"></span>
            </a>

            <div class="float-box analytics-area analytics-id-S sem-widget">
                <div class="seta-borda"></div>
                    <div class="menu-columns">
                        <div class="setinha-submenu"></div>
                        <ul class="first-column">
                            
                            
                                <li class="has-link ">
                                    <a href="http://globotv.globo.com/" data-menu-id="globotv">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globo.tv</span>
                                    </a>
                                </li>
                            
                                <li class="has-link ">
                                    <a href="http://globosatplay.globo.com/" data-menu-id="globosat-play">
                                        <div class='left-marker'></div>
                                        <div class='arrow-marker'></div>
                                        <span class="titulo">globosat play</span>
                                    </a>
                                </li>
                            
                            <li class="has-children search-item">
                                <a data-menu-id="search-area-videos">
                                    <div class='left-marker'></div>
                                    <div class='arrow-marker'></div>
                                    <span class="titulo">busca</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </div>
                    <div class="analytics-custom-product analytics-id-D">
                    
                    </div>
            </div>
        </li>
        
    
</ul>
<!-- gerado em: 2015-09-0721:40:51Z --><div id="assinante-menu"></div></nav></div><div class="header-bottom"></div></header><script>
libby.loadScript('http://s.glbimg.com/en/ho/static/etc/busca/js/jquery.buscaPadrao.v2.compressed.js', function(){var searchArea=document.getElementById('busca-padrao'),
placeholderValue = 'encontre na globo.com';if(typeof searchWidget !== "undefined" && searchWidget){searchWidget(searchArea, {suggestionsEnabled: true,
baseSearchUrl: 'http://www.globo.com',
addClass:'home-search',
placeholder: placeholderValue,
buscaHome: true,
qtd_min_letras_pro_suggest: 2,
qtd_min_letras_pra_busca: 2,
addStyleSheet: ''});}
searchArea.style.display = "block";if(searchArea && searchArea.querySelectorAll){var inputElement=searchArea.querySelectorAll('input')[0];inputElement.onwebkitspeechchange=inputElement.onspeechchange=function (ev) {inputElement.value=inputElement.value.replace(placeholderValue, '');inputElement.form.submit();};}});</script><div id="x60" class="opec-area opec-mobile opec-x60 grid-12"><div id="banner_mobile_topo" class="tag-manager-publicidade-container"></div></div><div id="urgente" class="container urgente analytics-area analytics-id-U "><div class="grid-base super-wide"></div></div><div id="bloco-principal" class="container first-scroll-container clearfix"><div class="grid-base wide analytics-area analytics-id-A"><div class="grid-base narrow pull-left primeira-area"><div class="destaque-principal-vertical-foto-topo destaque destaque-primeiro-scroll principal glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/globo-news/noticia/2015/09/forca-policial-brasileira-e-que-mais-mata-no-mundo-diz-relatorio.html" class=" " title="PolÃ­cia do Brasil Ã© a que mais mata no mundo, diz relatÃ³rio
"><div class="conteudo"><h2>PolÃ­cia do Brasil Ã© a que mais mata no mundo, diz relatÃ³rio<br /></h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="mobile-grid-partial"><a href="http://g1.globo.com/sao-paulo/transito/radar-tempo-transito-agora.html" class=" " title="Bandeirantes e FernÃ£o tÃªm mais de 30 km de filas em SP; acompanhe"><div class="conteudo"><h2>Bandeirantes e FernÃ£o tÃªm mais de 30 km de filas em SP; acompanhe</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="No RJ, NiterÃ³i-Manilha tem 41 km de lentidÃ£o; siga aqui" href="http://g1.globo.com/rio-de-janeiro/transito/radar-tempo-transito-agora.html">No RJ, NiterÃ³i-Manilha tem 41 km de lentidÃ£o; siga aqui</a></div></li></ul></div><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/noticia/2015/09/corpo-de-jovem-de-rio-preto-e-identificado-apos-acidente-em-paraty.html" class="foto " title="Estudante estÃ¡ entre os mortos em Paraty, RJ (ReproduÃ§Ã£o/TV TEM)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/7e4dfc5_A9Kpv9lSdAOo3h6BgvA=/filters:quality(10):strip_icc()/s2.glbimg.com/26J7BjghJLYUjMxXe2REubTlWwc=/0x97:300x232/155x70/s.glbimg.com/jo/g1/f/original/2015/09/07/kenia_g1.jpg" alt="Estudante estÃ¡ entre os mortos em Paraty, RJ (ReproduÃ§Ã£o/TV TEM)" title="Estudante estÃ¡ entre os mortos em Paraty, RJ (ReproduÃ§Ã£o/TV TEM)"
         data-original-image="s2.glbimg.com/26J7BjghJLYUjMxXe2REubTlWwc=/0x97:300x232/155x70/s.glbimg.com/jo/g1/f/original/2015/09/07/kenia_g1.jpg" data-url-smart_horizontal="sKJgWnmW5UoVdSWmhJOd0L_eFaM=/90x56/smart/filters:strip_icc()/" data-url-smart="sKJgWnmW5UoVdSWmhJOd0L_eFaM=/90x56/smart/filters:strip_icc()/" data-url-feature="sKJgWnmW5UoVdSWmhJOd0L_eFaM=/90x56/smart/filters:strip_icc()/" data-url-tablet="-1LHoDs4sLbhMbAjW89uvbUYWqk=/160xorig/smart/filters:strip_icc()/" data-url-desktop="H3mgF0KjF9BLKAwuF6wxhInjlqk=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Estudante estÃ¡ entre os mortos em Paraty, RJ</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/rj/regiao-serrana/noticia/2015/09/dupla-sertaneja-e-detida-em-show-por-suspeita-de-apologia-drogas.html" class="foto " title="Dupla sertaneja Ã© detida depois de cantar funk (DivulgaÃ§Ã£o)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/P8rj_UpYGjD-swgU15Xuox3F5VE=/filters:quality(10):strip_icc()/s2.glbimg.com/ah9xkRgvxQbpq7tyfjjImehiFpg=/86x47:549x256/155x70/s.glbimg.com/jo/g1/f/original/2015/09/07/1424566_398615916937126_529491223_n.jpg" alt="Dupla sertaneja Ã© detida depois de cantar funk (DivulgaÃ§Ã£o)" title="Dupla sertaneja Ã© detida depois de cantar funk (DivulgaÃ§Ã£o)"
         data-original-image="s2.glbimg.com/ah9xkRgvxQbpq7tyfjjImehiFpg=/86x47:549x256/155x70/s.glbimg.com/jo/g1/f/original/2015/09/07/1424566_398615916937126_529491223_n.jpg" data-url-smart_horizontal="KPwzbgR1m85OF978_zV4LQC3H_E=/90x56/smart/filters:strip_icc()/" data-url-smart="KPwzbgR1m85OF978_zV4LQC3H_E=/90x56/smart/filters:strip_icc()/" data-url-feature="KPwzbgR1m85OF978_zV4LQC3H_E=/90x56/smart/filters:strip_icc()/" data-url-tablet="MxcF7fN_T51rEFRSLZvWzJxJxnw=/160xorig/smart/filters:strip_icc()/" data-url-desktop="eqXv6_9c0DNZcF2l2VrfnffCFNI=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Dupla sertaneja Ã© detida depois de cantar funk</h2></div></a></div></div></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/vem-por-ai/noticia/2015/09/alisson-merlo-e-denis-ficam-beira-da-morte-amarrados-dentro-de-barraco-em-chamas.html" class="foto " title="&#39;Regra&#39;: chamas cercam MerlÃ´ (TV Globo)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/VSDShzjSLkM2v9TLBZGq-Kq9iPU=/filters:quality(10):strip_icc()/s2.glbimg.com/rTZ7sEEDtrw46Tws0-1yUfEIyAE=/0x15:524x269/155x75/s.glbimg.com/et/gs/f/original/2015/09/07/merlo_2.jpg" alt="&#39;Regra&#39;: chamas cercam MerlÃ´ (TV Globo)" title="&#39;Regra&#39;: chamas cercam MerlÃ´ (TV Globo)"
         data-original-image="s2.glbimg.com/rTZ7sEEDtrw46Tws0-1yUfEIyAE=/0x15:524x269/155x75/s.glbimg.com/et/gs/f/original/2015/09/07/merlo_2.jpg" data-url-smart_horizontal="dzfSFtzuzmekJi-ADHblNoaxVeA=/90x56/smart/filters:strip_icc()/" data-url-smart="dzfSFtzuzmekJi-ADHblNoaxVeA=/90x56/smart/filters:strip_icc()/" data-url-feature="dzfSFtzuzmekJi-ADHblNoaxVeA=/90x56/smart/filters:strip_icc()/" data-url-tablet="TUknryqh8xMW-jn6pD7crYrRr9I=/160xorig/smart/filters:strip_icc()/" data-url-desktop="-Zp0lSXoOc-SvbfY-xoxzsILNK0=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>&#39;Regra&#39;: chamas cercam MerlÃ´</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Janete descobre traiÃ§Ã£o" href="http://extra.globo.com/tv-e-lazer/novela-a-regra-do-jogo/em-regra-do-jogo-suzana-pires-sofrera-duro-golpe-apos-descobrir-que-traida-pelo-marido-que-sustenta-17410682.html">Janete descobre traiÃ§Ã£o</a></div></li></ul></div></div></div><div class="grid-base narrow ultimo"><div class="destaque-secundario-duplo-foto-topo destaque destaque-primeiro-scroll secundario duplo glb-hl-style-noticia analytics-area  analytics-id-J
        "><div class="esquerda"><div class="mobile-grid-partial"><a href="http://g1.globo.com/ciencia-e-saude/noticia/2015/09/bola-de-fogo-e-registrada-sobre-o-ceu-de-bangcoc.html" class="foto " title="Meteoro explode e bola de fogo cai na TailÃ¢ndia (BBC)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/8ClKYV021xF9OZgxBbCYxitDZTM=/filters:quality(10):strip_icc()/s2.glbimg.com/ozRYF0lY4PZThwGpDKJYpvHcNu4=/256x118:523x337/155x127/s.glbimg.com/jo/g1/f/original/2015/09/07/bola-de-fogo2.jpg" alt="Meteoro explode e bola de fogo cai na TailÃ¢ndia (BBC)" title="Meteoro explode e bola de fogo cai na TailÃ¢ndia (BBC)"
         data-original-image="s2.glbimg.com/ozRYF0lY4PZThwGpDKJYpvHcNu4=/256x118:523x337/155x127/s.glbimg.com/jo/g1/f/original/2015/09/07/bola-de-fogo2.jpg" data-url-smart_horizontal="LEWsySf7sSP6AZtLLqE6D70JhBM=/90x56/smart/filters:strip_icc()/" data-url-smart="LEWsySf7sSP6AZtLLqE6D70JhBM=/90x56/smart/filters:strip_icc()/" data-url-feature="LEWsySf7sSP6AZtLLqE6D70JhBM=/90x56/smart/filters:strip_icc()/" data-url-tablet="xXHSIoFJDbXU4w59rSxrPKZICDs=/160xorig/smart/filters:strip_icc()/" data-url-desktop="QshW2Gynbusc__tkvutznyUVI44=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Meteoro explode e bola de fogo cai na TailÃ¢ndia</h2></div></a></div></div><div class="direita"><div class="mobile-grid-partial"><a href="http://g1.globo.com/pr/norte-noroeste/noticia/2015/09/seis-pessoas-morrem-em-acidente-entre-cinco-carros-na-rodovia-pr-486.html" class="foto " title="Acidente mata 6 e fere cinco em batida frontal (RogÃ©rio Pinheiro/ RPC)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/2ngMu8aTyTuYUZi0sLA51OtoKhw=/filters:quality(10):strip_icc()/s2.glbimg.com/9OC6E2OpvRemMOTTVaoAFMazehA=/109x123:525x465/155x127/s.glbimg.com/jo/g1/f/original/2015/09/07/acidente_pr-486_certo.jpg" alt="Acidente mata 6 e fere cinco em batida frontal (RogÃ©rio Pinheiro/ RPC)" title="Acidente mata 6 e fere cinco em batida frontal (RogÃ©rio Pinheiro/ RPC)"
         data-original-image="s2.glbimg.com/9OC6E2OpvRemMOTTVaoAFMazehA=/109x123:525x465/155x127/s.glbimg.com/jo/g1/f/original/2015/09/07/acidente_pr-486_certo.jpg" data-url-smart_horizontal="SVdo7t3AbMrCAr71nbGxQfgrK1I=/90x56/smart/filters:strip_icc()/" data-url-smart="SVdo7t3AbMrCAr71nbGxQfgrK1I=/90x56/smart/filters:strip_icc()/" data-url-feature="SVdo7t3AbMrCAr71nbGxQfgrK1I=/90x56/smart/filters:strip_icc()/" data-url-tablet="X6QCxdouyZHuQwpcVW4GbTCV_Q8=/160xorig/smart/filters:strip_icc()/" data-url-desktop="WpNkim6Nf98GJP_Z9tS2jjBBQ9Q=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Acidente mata 6 e fere cinco em batida frontal</h2></div></a></div></div></div><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/ce/futebol/brasileirao-serie-b/jogo/07-09-2015/ceara-nautico/" class="foto " title="CearÃ¡ vence o NÃ¡utico, mas segue no Z-4 (Kid JÃºnior/AgÃªncia DiÃ¡rio)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/tDn21iyML55SfsigLMCDcePgzaI=/filters:quality(10):strip_icc()/s2.glbimg.com/JY-jOWhAsQ9l8vdXN545tZe1TQ8=/406x393:1735x994/155x70/s.glbimg.com/es/ge/f/original/2015/09/07/bg-0197_5.jpg" alt="CearÃ¡ vence o NÃ¡utico, mas segue no Z-4 (Kid JÃºnior/AgÃªncia DiÃ¡rio)" title="CearÃ¡ vence o NÃ¡utico, mas segue no Z-4 (Kid JÃºnior/AgÃªncia DiÃ¡rio)"
         data-original-image="s2.glbimg.com/JY-jOWhAsQ9l8vdXN545tZe1TQ8=/406x393:1735x994/155x70/s.glbimg.com/es/ge/f/original/2015/09/07/bg-0197_5.jpg" data-url-smart_horizontal="7SeBuKDsyoEMAZC8DglfwgmlpCU=/90x56/smart/filters:strip_icc()/" data-url-smart="7SeBuKDsyoEMAZC8DglfwgmlpCU=/90x56/smart/filters:strip_icc()/" data-url-feature="7SeBuKDsyoEMAZC8DglfwgmlpCU=/90x56/smart/filters:strip_icc()/" data-url-tablet="K6l9ECyZaJzOfghuS_GLrtUqng8=/160xorig/smart/filters:strip_icc()/" data-url-desktop="F7lAbI-iNw9v74AISPBd4-2ZvKI=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>CearÃ¡ vence o NÃ¡utico, mas segue no Z-4</h2></div></a></div></div><div class="destaque-secundario-foto-topo destaque destaque-primeiro-scroll secundario glb-hl-style-esporte analytics-area  analytics-id-E
        "><div class="mobile-grid-partial"><a href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/07-09-2015/escocia-alemanha/" class=" " title="Euro: Alemanha bate a EscÃ³cia (AP)"><div class="conteudo"><h2>Euro: Alemanha bate a EscÃ³cia</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Portugal vence a AlbÃ¢nia no fim e se mantÃ©m lÃ­der" href="http://globoesporte.globo.com/futebol/futebol-internacional/jogo/07-09-2015/albania-portugal/">Portugal vence a AlbÃ¢nia no fim e se mantÃ©m lÃ­der</a></div></li></ul></div><div class="ultimo-destaque"><div class="destaque-secundario-foto-lado destaque destaque-primeiro-scroll secundario foto-lado glb-hl-style-entretenimento analytics-area  analytics-id-M
        "><div class="mobile-grid-partial"><a href="http://gshow.globo.com/novelas/verdades-secretas/Vem-por-ai/noticia/2015/09/alex-faz-ameaca-por-amor-angel-veja-o-teaser.html" class="foto " title="Em &#39;Verdades&#39;, Alex faz ameaÃ§a (Isabella Pinheiro / Gshow)"><span class="borda-foto"><span></span><img src="http://s2.glbimg.com/fWT59g8qYDOERNAUJe4F18aUGo0=/filters:quality(10):strip_icc()/s2.glbimg.com/rpoYHqA5uNaDKcNQLSKsPKkWjAc=/0x53:690x387/155x75/s.glbimg.com/et/gs/f/original/2015/09/02/alex-guilherme.jpg" alt="Em &#39;Verdades&#39;, Alex faz ameaÃ§a (Isabella Pinheiro / Gshow)" title="Em &#39;Verdades&#39;, Alex faz ameaÃ§a (Isabella Pinheiro / Gshow)"
         data-original-image="s2.glbimg.com/rpoYHqA5uNaDKcNQLSKsPKkWjAc=/0x53:690x387/155x75/s.glbimg.com/et/gs/f/original/2015/09/02/alex-guilherme.jpg" data-url-smart_horizontal="KSOpXjwon-XZTnZ1d6hErKG82rE=/90x56/smart/filters:strip_icc()/" data-url-smart="KSOpXjwon-XZTnZ1d6hErKG82rE=/90x56/smart/filters:strip_icc()/" data-url-feature="KSOpXjwon-XZTnZ1d6hErKG82rE=/90x56/smart/filters:strip_icc()/" data-url-tablet="Rvr0pdcTANTW8Q19ZAEO6sNcTeg=/160xorig/smart/filters:strip_icc()/" data-url-desktop="h2QggnE_B6aRuzUagz_jvGUHqtE=/155xorig/smart/filters:strip_icc()/"
    /></span><div class="conteudo"><h2>Em &#39;Verdades&#39;, Alex faz ameaÃ§a</h2></div></a></div><ul class="chamada-relacionada"><li><div class="mobile-grid-partial"><a title="Carolina cita abandono" href="http://kogut.oglobo.globo.com/noticias-da-tv/novelas/noticia/2015/09/carolina-ameacara-abandonar-alex-depois-de-ser-humilhada.html">Carolina cita abandono</a></div></li></ul></div></div></div><div class="grid-base wide pull-left wide-inferior"></div></div><script>
var init,
stid;if(typeof window.homeColumns === 'undefined' && !window.homeColumns){window.homeColumns=new HomeColumns({});}
init=function(ev) {window.homeColumns.initFirstScroll();if(!window.homeColumns.mergedFirstScroll){responsiveHub.updateImages();}
window.removeEventListener("resize", init);clearTimeout(stid);};stid=setTimeout(function(){init();}, 200);window.addEventListener("resize", init);</script><div id="area-widgets-direita-topo" class="grid-base narrow area-widgets-direta-topo analytics-area analytics-id-V"><div id="ad-position-top1" class="opec"><div id="banner_slim_topo" class="tag-manager-publicidade-container"></div></div><div class="libby-agrupador-destaque-globotv-com-canais length-5"><div class="main-area mobile-grid-full"><div class="header-area analytics-area analytics-id-header"><a class="logo-link" href="http://globotv.globo.com"><div class="logo"></div></a><h3><a class="title-link" href="http://globotv.globo.com/busca/?q=futebol">futebol</a></h3></div><div class="hover-area"><div class="image-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-alemanha-muller-bate-de-esquerda-e-conta-com-desvio-para-fazer-aos-18-do-1o-tempo/4449650/"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-alemanha-muller-bate-de-esquerda-e-conta-com-desvio-para-fazer-aos-18-do-1o-tempo/4449650/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4449650.jpg" alt="MÃ¼ller bate de esquerda e conta com desvio para abrir o placar" /><div class="time">00:44</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">eliminatÃ³rias da eurocopa</span><span class="title">MÃ¼ller bate de esquerda e conta com desvio para abrir o placar</span></div></a></li><li class=" analytics-area analytics-id-2 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-escocia-depois-de-defesa-de-neuer-hummels-marca-contra-aos-28-do-1o-tempo/4449658/"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-escocia-depois-de-defesa-de-neuer-hummels-marca-contra-aos-28-do-1o-tempo/4449658/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4449658.jpg" alt="Depois de defesa, Hummels marca contra e EscÃ³cia empata" /><div class="time">00:41</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">bobeira!</span><span class="title">Depois de defesa, Hummels marca contra e EscÃ³cia empata</span></div></a></li><li class=" analytics-area analytics-id-3 analytics-id-I glb-hl-style-esporte" data-href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-alemanha-muller-aproveita-rebote-e-faz-o-segundo-aos-34-do-1o-tempo/4449667/"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-alemanha-muller-aproveita-rebote-e-faz-o-segundo-aos-34-do-1o-tempo/4449667/"><div class="image-container"><div class="image-wrapper"><img src="http://s01.video.glbimg.com/320x200/4449667.jpg" alt="MÃ¼ller aproveita rebote e faz o segundo para a seleÃ§Ã£o alemÃ£" /><div class="time">01:03</div></div><div class="overlay"></div><div class="logo-watch-now"></div></div><div class="text-container"><span class="subtitle">de novo ele!</span><span class="title">MÃ¼ller aproveita rebote e faz o segundo para a seleÃ§Ã£o alemÃ£</span></div></a></li></ul></div><div class="title-area"><ul><li class="active analytics-area analytics-id-1 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-alemanha-muller-bate-de-esquerda-e-conta-com-desvio-para-fazer-aos-18-do-1o-tempo/4449650/"><span class="subtitle">eliminatÃ³rias da eurocopa</span><span class="title">MÃ¼ller bate de esquerda e conta com desvio para abrir o placar</span></a></li><li class=" analytics-area analytics-id-2 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-escocia-depois-de-defesa-de-neuer-hummels-marca-contra-aos-28-do-1o-tempo/4449658/"><span class="subtitle">bobeira!</span><span class="title">Depois de defesa, Hummels marca contra e EscÃ³cia empata</span></a></li><li class=" analytics-area analytics-id-3 analytics-id-T glb-hl-style-esporte"><a href="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-alemanha-muller-aproveita-rebote-e-faz-o-segundo-aos-34-do-1o-tempo/4449667/"><span class="subtitle">de novo ele!</span><span class="title">MÃ¼ller aproveita rebote e faz o segundo para a seleÃ§Ã£o alemÃ£</span></a></li></ul></div></div><div class="controls-area"><ul class="step-marker glb-hl-style-esporte"><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-1"><a href="#" data-related="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-alemanha-muller-bate-de-esquerda-e-conta-com-desvio-para-fazer-aos-18-do-1o-tempo/4449650/"><div class="ball active"></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-2"><a href="#" data-related="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-escocia-depois-de-defesa-de-neuer-hummels-marca-contra-aos-28-do-1o-tempo/4449658/"><div class="ball "></div></a></div></li><li class="analytics-area analytics-id-ball"><div class="analytics-area analytics-id-3"><a href="#" data-related="http://globotv.globo.com/sportv/futebol-internacional/v/gol-da-alemanha-muller-aproveita-rebote-e-faz-o-segundo-aos-34-do-1o-tempo/4449667/"><div class="ball "></div></a></div></li></ul><ul class="navigation-buttons"><li><a href="#left"><div class="left"></div></a></li><li><a href="#right"><div class="right"></div></a></li></ul></div><div class="menu-area"><ul><li><a href="http://globotv.globo.com/rede-globo/"><div class="channels rede-globo"></div></a></li><li><a href="http://globotv.globo.com/sportv/"><div class="channels sportv"></div></a></li><li><a href="http://globotv.globo.com/globo-news/"><div class="channels globo-news"></div></a></li><li><a href="http://globotv.globo.com/multishow/"><div class="channels multishow"></div></a></li><li><a href="http://globotv.globo.com/gnt/"><div class="channels gnt"></div></a></li></ul></div><div class="footer-area analytics-area analytics-id-footer"><a class="title-link" href="http://globotv.globo.com/">mais<strong> vÃ­deos</strong><span class="arrow"> âº</span></a></div></div></div><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded-agrupador-destaque-globotv-com-canais']);</script></div></div><div class="separator-first-scroll-container"><section><div><div id="opec-banner-middle-container" class="opec-banner-middle-container"><div id="ad-position-middle" class="opec"><div id="banner_slb_meio" class="tag-manager-publicidade-container"></div></div></div></div></section></div><div id="x62" class="opec-area opec-mobile opec-x62 grid-12"><div id="banner_mobile_meio" class="tag-manager-publicidade-container"></div></div><div id="container-columns" class="container columns clearfix glb-area-colunas"><section class="area news-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/goias/noticia/2015/09/empresaria-reafirma-que-foi-jogada-de-predio-pelo-marido-me-lancou.html" class="foto" title="EmpresÃ¡ria reafirma que foi jogada de prÃ©dio pelo marido: &#39;Me lanÃ§ou para fora&#39;
 (ReproduÃ§Ã£o/TV Anhanguera)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/MSZE6mvH7gYMMjQQi30mKQqqVlY=/filters:quality(10):strip_icc()/s2.glbimg.com/a6RWRNiRj9HKsK1do4Xx8L857Tk=/0x0:620x333/335x180/s.glbimg.com/jo/g1/f/original/2015/09/07/montagem-certa.jpg" alt="EmpresÃ¡ria reafirma que foi jogada de prÃ©dio pelo marido: &#39;Me lanÃ§ou para fora&#39;
 (ReproduÃ§Ã£o/TV Anhanguera)" title="EmpresÃ¡ria reafirma que foi jogada de prÃ©dio pelo marido: &#39;Me lanÃ§ou para fora&#39;
 (ReproduÃ§Ã£o/TV Anhanguera)"
                data-original-image="s2.glbimg.com/a6RWRNiRj9HKsK1do4Xx8L857Tk=/0x0:620x333/335x180/s.glbimg.com/jo/g1/f/original/2015/09/07/montagem-certa.jpg" data-url-smart_horizontal="LrhZ0Z_7DI7Kf7WOBxP4fUWa7JM=/90x0/smart/filters:strip_icc()/" data-url-smart="LrhZ0Z_7DI7Kf7WOBxP4fUWa7JM=/90x0/smart/filters:strip_icc()/" data-url-feature="LrhZ0Z_7DI7Kf7WOBxP4fUWa7JM=/90x0/smart/filters:strip_icc()/" data-url-tablet="EPlenAszP4Yxisof3YeiVv3KgxQ=/220x125/smart/filters:strip_icc()/" data-url-desktop="QB1RE3tYE_IZ60kQUDkOklkAFvM=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>EmpresÃ¡ria reafirma que foi jogada de prÃ©dio pelo marido: &#39;Me lanÃ§ou para fora&#39;<br /></p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 18:56:16" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pe/caruaru-regiao/noticia/2015/09/mae-e-filha-morrem-apos-descarga-eletrica-em-varal-de-roupas-em-pe.html" class="foto" title="Testemunhas dizem que garota de 6 anos morreu tentando salvar a mÃ£e (DivulgaÃ§Ã£o/ PolÃ­cia Militar)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/BqiHN_p0iANrQE4WCAQFNuHiE-A=/filters:quality(10):strip_icc()/s2.glbimg.com/JXxK_u7oU_co3WtMtvkfYz9s8QU=/0x446:1200x1246/120x80/s.glbimg.com/jo/g1/f/original/2015/09/07/img_5304.jpg" alt="Testemunhas dizem que garota de 6 anos morreu tentando salvar a mÃ£e (DivulgaÃ§Ã£o/ PolÃ­cia Militar)" title="Testemunhas dizem que garota de 6 anos morreu tentando salvar a mÃ£e (DivulgaÃ§Ã£o/ PolÃ­cia Militar)"
                data-original-image="s2.glbimg.com/JXxK_u7oU_co3WtMtvkfYz9s8QU=/0x446:1200x1246/120x80/s.glbimg.com/jo/g1/f/original/2015/09/07/img_5304.jpg" data-url-smart_horizontal="EsZnlM0nnK8y1yrnzOpXdNsh-rQ=/90x0/smart/filters:strip_icc()/" data-url-smart="EsZnlM0nnK8y1yrnzOpXdNsh-rQ=/90x0/smart/filters:strip_icc()/" data-url-feature="EsZnlM0nnK8y1yrnzOpXdNsh-rQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="i0Qp8YdozINfJcXpn2qhSJ_vhXQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="9vF8fBOXpUs8Qmav6nWh4wtIJic=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Testemunhas dizem que garota de 6 anos morreu tentando salvar a mÃ£e</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 16:00:01" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/pr/norte-noroeste/noticia/2015/09/corpos-das-vitimas-de-acidente-na-br-153-sao-velados-em-ginasio.html" class="foto" title="Filha de prefeito e mais trÃªs jovens morrem em batida no PR; corpos sÃ£o velados (reproduÃ§Ã£o / facebook)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/exA6PLoR0YA2-LE0pTeYcWZHczw=/filters:quality(10):strip_icc()/s2.glbimg.com/swjzueTlzHMLCoCjmPohFTajexo=/0x137:448x435/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/filha-prefeito1.jpg" alt="Filha de prefeito e mais trÃªs jovens morrem em batida no PR; corpos sÃ£o velados (reproduÃ§Ã£o / facebook)" title="Filha de prefeito e mais trÃªs jovens morrem em batida no PR; corpos sÃ£o velados (reproduÃ§Ã£o / facebook)"
                data-original-image="s2.glbimg.com/swjzueTlzHMLCoCjmPohFTajexo=/0x137:448x435/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/filha-prefeito1.jpg" data-url-smart_horizontal="LVZCF85GC0B4H-EEMPw4c21sO_U=/90x0/smart/filters:strip_icc()/" data-url-smart="LVZCF85GC0B4H-EEMPw4c21sO_U=/90x0/smart/filters:strip_icc()/" data-url-feature="LVZCF85GC0B4H-EEMPw4c21sO_U=/90x0/smart/filters:strip_icc()/" data-url-tablet="TYZDjkq2DmWf07ejmIX7d1zW1HI=/70x50/smart/filters:strip_icc()/" data-url-desktop="2inH1-Cla6kUuxpOU2MyVXRLz8g=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Filha de prefeito e mais trÃªs jovens morrem em batida no PR; corpos sÃ£o velados</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 20:01:06" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/sorocaba-jundiai/noticia/2015/09/dono-de-cachorro-que-fazia-vigilia-em-porte-de-hospital-recebe-alta.html" class="" title="Homem que infartou revÃª cÃ£o que ficou uma semana na porta de hospital; vÃ­deo (ReproduÃ§Ã£o)" rel="bookmark"><span class="conteudo"><p>Homem que infartou revÃª cÃ£o que ficou uma semana na porta de hospital; vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 20:39:24" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/mato-grosso/noticia/2015/09/cobra-e-vista-atravessando-avenida-em-cuiaba-e-para-o-transito.html" class="foto" title="Cobra leva 10 minutos para atravessar via e para trÃ¢nsito em CuiabÃ¡; veja as fotos (AntÃ´nio de PÃ¡dua Faria/Arquivo Pessoal)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/usyIitfM6nWeiptZy1uKI2VJbrc=/filters:quality(10):strip_icc()/s2.glbimg.com/WezA8OTubSgLCBntWWFhwbGf6AQ=/195x103:586x363/120x80/s.glbimg.com/jo/g1/f/original/2015/09/07/cobra_na_calcada_2_620x465_antonio_padua.jpg" alt="Cobra leva 10 minutos para atravessar via e para trÃ¢nsito em CuiabÃ¡; veja as fotos (AntÃ´nio de PÃ¡dua Faria/Arquivo Pessoal)" title="Cobra leva 10 minutos para atravessar via e para trÃ¢nsito em CuiabÃ¡; veja as fotos (AntÃ´nio de PÃ¡dua Faria/Arquivo Pessoal)"
                data-original-image="s2.glbimg.com/WezA8OTubSgLCBntWWFhwbGf6AQ=/195x103:586x363/120x80/s.glbimg.com/jo/g1/f/original/2015/09/07/cobra_na_calcada_2_620x465_antonio_padua.jpg" data-url-smart_horizontal="qQQkbogebVYAob7UD9hmq6kaCpU=/90x0/smart/filters:strip_icc()/" data-url-smart="qQQkbogebVYAob7UD9hmq6kaCpU=/90x0/smart/filters:strip_icc()/" data-url-feature="qQQkbogebVYAob7UD9hmq6kaCpU=/90x0/smart/filters:strip_icc()/" data-url-tablet="um_BRXkQm9RA12TdQV-ZV4PWHrc=/70x50/smart/filters:strip_icc()/" data-url-desktop="XJLcCNYttee9yzv-APxO2R-oV2A=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Cobra leva 10 minutos para atravessar via e para trÃ¢nsito em CuiabÃ¡; veja as fotos</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 10:47:37" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/09/testamos-huawei-mate-s-na-ifa-2015-top-chines-rouba-forca-da-apple-ifa2015.html" class="foto" title="ConheÃ§a o smart que deu &#39;rasteira&#39; no novo iPhone e lanÃ§ou tecnologia antes (Laura Martins/TechTudo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/I_R0p6worhVaxR64oPWSQPgAbss=/filters:quality(10):strip_icc()/s2.glbimg.com/f2XZQPI5BcElz5nvhKeNx4wZGRw=/83x130:1237x900/120x80/s.glbimg.com/po/tt2/f/original/2015/09/05/mates1.jpg" alt="ConheÃ§a o smart que deu &#39;rasteira&#39; no novo iPhone e lanÃ§ou tecnologia antes (Laura Martins/TechTudo)" title="ConheÃ§a o smart que deu &#39;rasteira&#39; no novo iPhone e lanÃ§ou tecnologia antes (Laura Martins/TechTudo)"
                data-original-image="s2.glbimg.com/f2XZQPI5BcElz5nvhKeNx4wZGRw=/83x130:1237x900/120x80/s.glbimg.com/po/tt2/f/original/2015/09/05/mates1.jpg" data-url-smart_horizontal="WPk-tW7GNmAMBn60I2iPyRw70So=/90x0/smart/filters:strip_icc()/" data-url-smart="WPk-tW7GNmAMBn60I2iPyRw70So=/90x0/smart/filters:strip_icc()/" data-url-feature="WPk-tW7GNmAMBn60I2iPyRw70So=/90x0/smart/filters:strip_icc()/" data-url-tablet="pwqheLm8zOuHLpDJ-Y-MgbGnuZU=/70x50/smart/filters:strip_icc()/" data-url-desktop="lrftXqOEu4CciSAPEZpkttwa9AQ=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>ConheÃ§a o smart que deu &#39;rasteira&#39; no novo iPhone e lanÃ§ou tecnologia antes</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 15:20:22" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/09/testamos-o-zenwatch-2-asus-cria-smartwatch-com-cara-de-relogio-ifa2015.html" class="" title="Nova versÃ£o do relÃ³gio inteligente da Asus tem bateria &#39;turbinada&#39; e promete ser &#39;baratinho&#39; (Melissa Cruz/TechTudo)" rel="bookmark"><span class="conteudo"><p>Nova versÃ£o do relÃ³gio inteligente da Asus tem bateria &#39;turbinada&#39; e promete ser &#39;baratinho&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 16:11:00" class="chamada chamada-principal mobile-grid-full"><a href="http://revistacrescer.globo.com/Curiosidades/noticia/2015/09/bombeiro-coloca-animacao-happy-feet-no-celular-para-acalmar-crianca-que-sofreu-acidente.html" class="foto" title="Bombeiro coloca &#39;Happy Feet&#39; para crianÃ§a assistir e se acalmar apÃ³s acidente (reproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Otxr__F_GW-7komS7b0-0NE20Kw=/filters:quality(10):strip_icc()/s2.glbimg.com/oAM8BP3-HLktHsoUqGCn16FzD6s=/121x129:485x372/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/bombeiro1.jpg" alt="Bombeiro coloca &#39;Happy Feet&#39; para crianÃ§a assistir e se acalmar apÃ³s acidente (reproduÃ§Ã£o)" title="Bombeiro coloca &#39;Happy Feet&#39; para crianÃ§a assistir e se acalmar apÃ³s acidente (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/oAM8BP3-HLktHsoUqGCn16FzD6s=/121x129:485x372/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/bombeiro1.jpg" data-url-smart_horizontal="-dzAchlrsPpZJVm4OzezReRxkk4=/90x0/smart/filters:strip_icc()/" data-url-smart="-dzAchlrsPpZJVm4OzezReRxkk4=/90x0/smart/filters:strip_icc()/" data-url-feature="-dzAchlrsPpZJVm4OzezReRxkk4=/90x0/smart/filters:strip_icc()/" data-url-tablet="vJhJwQfYfxK4bOzzQOfEHHk13cM=/70x50/smart/filters:strip_icc()/" data-url-desktop="WQT-gim1dckQYFU7Ffawh0ZJ54g=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Bombeiro coloca &#39;Happy Feet&#39; para crianÃ§a assistir e se acalmar apÃ³s acidente</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 18:48:16" class="chamada chamada-principal mobile-grid-full"><a href="http://g1.globo.com/sao-paulo/noticia/2015/09/corpo-de-menino-achado-ha-3-dias-em-freezer-segue-no-iml-em-sp.html" class="foto" title="Corpo de garoto de 5 anos achado em freezer segue em IML em SÃ£o Paulo (Paula Paiva Paulo/G1)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/zdiTtX3iFz04A4O-DyFmEFqFTSc=/filters:quality(10):strip_icc()/s2.glbimg.com/hQNboJIZxZcJmsrWrCmKNQCBPuw=/94x173:383x366/120x80/s.glbimg.com/jo/g1/f/original/2015/09/05/geladeira2.jpg" alt="Corpo de garoto de 5 anos achado em freezer segue em IML em SÃ£o Paulo (Paula Paiva Paulo/G1)" title="Corpo de garoto de 5 anos achado em freezer segue em IML em SÃ£o Paulo (Paula Paiva Paulo/G1)"
                data-original-image="s2.glbimg.com/hQNboJIZxZcJmsrWrCmKNQCBPuw=/94x173:383x366/120x80/s.glbimg.com/jo/g1/f/original/2015/09/05/geladeira2.jpg" data-url-smart_horizontal="QNqaUa1sbl_HqMM9c5hS7kslLKs=/90x0/smart/filters:strip_icc()/" data-url-smart="QNqaUa1sbl_HqMM9c5hS7kslLKs=/90x0/smart/filters:strip_icc()/" data-url-feature="QNqaUa1sbl_HqMM9c5hS7kslLKs=/90x0/smart/filters:strip_icc()/" data-url-tablet="WEu1UcqJZyXA9oM_wspvywHJuS4=/70x50/smart/filters:strip_icc()/" data-url-desktop="0iXWmf-5WWFWymZCx6Pp-cTPrIg=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Corpo de garoto de 5 anos achado em freezer segue em IML em SÃ£o Paulo</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area sports-column   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/pe/caruaru-regiao/futebol/noticia/2015/09/ibis-provoca-o-vasco-na-web-e-sugere-desafio-dos-piores-times-do-mundo.html" class="foto" title="AtÃ© Ãbis zoa o Vasco e pede disputa por tÃ­tulo de &#39;pior time do mundo&#39;: &#39;Valendo&#39; (ReproduÃ§Ã£o Twitter)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/jWm2D-1o37LUI4L3Fiq5UBD72bY=/filters:quality(10):strip_icc()/s2.glbimg.com/0OMgtvaH2z4PS6CgOVmSuWsZov4=/0x4:620x337/335x180/s.glbimg.com/es/ge/f/original/2015/09/07/ibis.jpg" alt="AtÃ© Ãbis zoa o Vasco e pede disputa por tÃ­tulo de &#39;pior time do mundo&#39;: &#39;Valendo&#39; (ReproduÃ§Ã£o Twitter)" title="AtÃ© Ãbis zoa o Vasco e pede disputa por tÃ­tulo de &#39;pior time do mundo&#39;: &#39;Valendo&#39; (ReproduÃ§Ã£o Twitter)"
                data-original-image="s2.glbimg.com/0OMgtvaH2z4PS6CgOVmSuWsZov4=/0x4:620x337/335x180/s.glbimg.com/es/ge/f/original/2015/09/07/ibis.jpg" data-url-smart_horizontal="TvrW1ZpoTYqenurzxRjxfpSgsGQ=/90x0/smart/filters:strip_icc()/" data-url-smart="TvrW1ZpoTYqenurzxRjxfpSgsGQ=/90x0/smart/filters:strip_icc()/" data-url-feature="TvrW1ZpoTYqenurzxRjxfpSgsGQ=/90x0/smart/filters:strip_icc()/" data-url-tablet="6ITiXEuPeFcW_ATLDThWotHn_wU=/220x125/smart/filters:strip_icc()/" data-url-desktop="IYXAHpEAcCDeh6ZVJtPdV5Hdlkc=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>AtÃ© Ãbis zoa o Vasco e pede disputa por tÃ­tulo de &#39;pior time do mundo&#39;: &#39;Valendo&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 18:14:15" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/corinthians/noticia/2015/09/torcedor-corintiano-grava-gol-de-love-do-meio-da-torcida-rival-e-contem-festa.html" class="foto" title="Corintiano filma gol de Love no meio da torcida do Palmeiras; veja a reaÃ§Ã£o (reproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/PCeSsYuP_dnswwp9C2gQk35mf2k=/filters:quality(10):strip_icc()/s2.glbimg.com/D-fpxGS9DRBLF2giU6ijM1cDJy4=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/torcedor.jpg" alt="Corintiano filma gol de Love no meio da torcida do Palmeiras; veja a reaÃ§Ã£o (reproduÃ§Ã£o)" title="Corintiano filma gol de Love no meio da torcida do Palmeiras; veja a reaÃ§Ã£o (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/D-fpxGS9DRBLF2giU6ijM1cDJy4=/0x0:120x80/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/torcedor.jpg" data-url-smart_horizontal="aybd5JkWaTp6GbtvI3tH3_ks78w=/90x0/smart/filters:strip_icc()/" data-url-smart="aybd5JkWaTp6GbtvI3tH3_ks78w=/90x0/smart/filters:strip_icc()/" data-url-feature="aybd5JkWaTp6GbtvI3tH3_ks78w=/90x0/smart/filters:strip_icc()/" data-url-tablet="WWplNhMnMOu05G-7w3Jsj7j0xoM=/70x50/smart/filters:strip_icc()/" data-url-desktop="Q_D-PbArKUTutw4aUpP1rZAclDw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Corintiano filma gol de Love no meio da torcida do Palmeiras; veja a reaÃ§Ã£o</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 17:47:14" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/selecao-brasileira/noticia/2015/09/dunga-faz-misterio-em-virtude-de-duvida-para-ganhar-ou-observar.html" class="foto" title="Dunga diz que ainda vai ver se usa forÃ§a total ou testa novatos contra os EUA (Leo Correa / Mowa Press)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/9D7zG8ikV1l7FSwQ1M9jKr7DL5A=/filters:quality(10):strip_icc()/s2.glbimg.com/PkqYX80xR4IkobLGFTH9kSPWbyU=/226x233:1133x838/120x80/s.glbimg.com/es/ge/f/original/2015/09/07/20150907161131_55ede163668cf_1.jpg" alt="Dunga diz que ainda vai ver se usa forÃ§a total ou testa novatos contra os EUA (Leo Correa / Mowa Press)" title="Dunga diz que ainda vai ver se usa forÃ§a total ou testa novatos contra os EUA (Leo Correa / Mowa Press)"
                data-original-image="s2.glbimg.com/PkqYX80xR4IkobLGFTH9kSPWbyU=/226x233:1133x838/120x80/s.glbimg.com/es/ge/f/original/2015/09/07/20150907161131_55ede163668cf_1.jpg" data-url-smart_horizontal="eULQDZ8iR3lgOvGb9lHTOl6o9fI=/90x0/smart/filters:strip_icc()/" data-url-smart="eULQDZ8iR3lgOvGb9lHTOl6o9fI=/90x0/smart/filters:strip_icc()/" data-url-feature="eULQDZ8iR3lgOvGb9lHTOl6o9fI=/90x0/smart/filters:strip_icc()/" data-url-tablet="ZS22H-rdOBWb6TZF5vA3jvba48A=/70x50/smart/filters:strip_icc()/" data-url-desktop="tNeXo8jlqpMdOsAW9YzaHvgbk70=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Dunga diz que ainda vai ver se usa forÃ§a total ou testa novatos contra os EUA</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 16:37:03" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/flamengo/flamengo-reconhece-irregularidade-no-gol-de-sheik-repudia-erros-de-arbitragem-no-brasileiro-17423462.html" class="" title="Flamengo reconhece irregularidade no gol de Sheik e repudia erros de arbitragem no Brasileiro" rel="bookmark"><span class="conteudo"><p>Flamengo reconhece irregularidade no gol de Sheik e repudia erros de arbitragem no Brasileiro</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 18:17:07" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/futebol/times/atletico-mg/noticia/2015/09/rock-familia-simpsons-e-cachorro-conheca-tatuagens-de-lucas-pratto.html" class="foto" title="Letra de heavy metal, desenho animado, leÃ£o... veja as tatuagens de Pratto (Bruno Cantini/CAM)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/t3nn26o_J4FxYZbvgXGCCUgBiKw=/filters:quality(10):strip_icc()/s2.glbimg.com/1yrV1WrvFAB4YAtYCI-u14iEfFo=/1106x501:2613x1507/120x80/s.glbimg.com/es/ge/f/original/2015/08/23/20826671095_014e927dc0_o.jpg" alt="Letra de heavy metal, desenho animado, leÃ£o... veja as tatuagens de Pratto (Bruno Cantini/CAM)" title="Letra de heavy metal, desenho animado, leÃ£o... veja as tatuagens de Pratto (Bruno Cantini/CAM)"
                data-original-image="s2.glbimg.com/1yrV1WrvFAB4YAtYCI-u14iEfFo=/1106x501:2613x1507/120x80/s.glbimg.com/es/ge/f/original/2015/08/23/20826671095_014e927dc0_o.jpg" data-url-smart_horizontal="cg5FuSfyOBccKPhggRwS4dZeXgk=/90x0/smart/filters:strip_icc()/" data-url-smart="cg5FuSfyOBccKPhggRwS4dZeXgk=/90x0/smart/filters:strip_icc()/" data-url-feature="cg5FuSfyOBccKPhggRwS4dZeXgk=/90x0/smart/filters:strip_icc()/" data-url-tablet="G2H-WprTsqImzZf55xZ4FK57QQM=/70x50/smart/filters:strip_icc()/" data-url-desktop="wOPEkYme5vCyDs3NiyML3OWTrTw=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Letra de heavy metal, desenho animado, leÃ£o... veja as tatuagens de Pratto</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 16:07:13" class="chamada chamada-principal mobile-grid-full"><a href="http://extra.globo.com/esporte/flamengo/2015/09/07/81-em-dia-de-folga-no-flamengo-emerson-sheik-faz-passeio-de-barco-com-amigas-para-angra" class="foto" title="Em vÃ­deo, Sheik comemora aniversÃ¡rio em lancha bem acompanhado; assista (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/aM8iOv1DlHqhlxBgFN0HBy0Be9Q=/filters:quality(10):strip_icc()/s2.glbimg.com/nCD-fSiBzK7COJ0fljeIOdh01xg=/0x61:345x291/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/amigas-sheik.jpg" alt="Em vÃ­deo, Sheik comemora aniversÃ¡rio em lancha bem acompanhado; assista (ReproduÃ§Ã£o)" title="Em vÃ­deo, Sheik comemora aniversÃ¡rio em lancha bem acompanhado; assista (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/nCD-fSiBzK7COJ0fljeIOdh01xg=/0x61:345x291/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/amigas-sheik.jpg" data-url-smart_horizontal="WDjVHO4xeCA1MGE7qiD4il26VZ8=/90x0/smart/filters:strip_icc()/" data-url-smart="WDjVHO4xeCA1MGE7qiD4il26VZ8=/90x0/smart/filters:strip_icc()/" data-url-feature="WDjVHO4xeCA1MGE7qiD4il26VZ8=/90x0/smart/filters:strip_icc()/" data-url-tablet="Ak7MrpitPWZSpoPvoAU80_Rl7wc=/70x50/smart/filters:strip_icc()/" data-url-desktop="x6rLyWceRlQOyeAHhjWYO845-vY=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Em vÃ­deo, Sheik comemora aniversÃ¡rio em lancha bem acompanhado; assista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 16:45:28" class="chamada chamada-principal mobile-grid-full"><a href="http://www.techtudo.com.br/noticias/noticia/2015/09/pes-2016-fecha-exclusividade-com-flamengo-e-maracana.html" class="" title="Com lanÃ§amento previsto para quarta-feira, PES 2016 fecha exclusividade com Fla e MaracanÃ£" rel="bookmark"><span class="conteudo"><p>Com lanÃ§amento previsto para quarta-feira, PES 2016 fecha exclusividade com Fla e MaracanÃ£</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 16:55:11" class="chamada chamada-principal mobile-grid-full"><a href="http://globoesporte.globo.com/blogs/especial-blog/brasil-mundial-fc/post/trombada-na-area-provoca-fratura-exposta-em-jogador-na-argentina.html" class="foto" title="Goleiro de time argentino chega em dividida e meia tem fratura exposta; vÃ­deo (ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/icf7B2EmU5avPAYZdc9aNQbcvZw=/filters:quality(10):strip_icc()/s2.glbimg.com/xZUjn0Ky3jzAe4GSEX8T70Rc7wU=/143x210:454x418/120x80/s.glbimg.com/es/ge/f/original/2015/09/07/fratura.jpg" alt="Goleiro de time argentino chega em dividida e meia tem fratura exposta; vÃ­deo (ReproduÃ§Ã£o)" title="Goleiro de time argentino chega em dividida e meia tem fratura exposta; vÃ­deo (ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/xZUjn0Ky3jzAe4GSEX8T70Rc7wU=/143x210:454x418/120x80/s.glbimg.com/es/ge/f/original/2015/09/07/fratura.jpg" data-url-smart_horizontal="0LPdYnOwU9wCfo2DNXCHYp1ZyGk=/90x0/smart/filters:strip_icc()/" data-url-smart="0LPdYnOwU9wCfo2DNXCHYp1ZyGk=/90x0/smart/filters:strip_icc()/" data-url-feature="0LPdYnOwU9wCfo2DNXCHYp1ZyGk=/90x0/smart/filters:strip_icc()/" data-url-tablet="SNgazYuRaLGYTnxlkiuR3O1Ip_0=/70x50/smart/filters:strip_icc()/" data-url-desktop="2z8PlfIQ1QMSOxQLoFuHb4-7RJM=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Goleiro de time argentino chega em dividida e meia tem fratura exposta; vÃ­deo</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 17:08:56" class="chamada chamada-principal mobile-grid-full"><a href="http://sportv.globo.com/site/combate/blogs/especial-blog/ultimmato/post/de-topless-claudinha-gadelha-sensualiza-e-homenageia-irmas-em-rede-social.html" class="foto" title="Lutadora do UFC posta foto de topless junto com as duas irmÃ£s; amplie aqui (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Zr-EUeKV9e7IxFSFLh2pOGaxpec=/filters:quality(10):strip_icc()/s2.glbimg.com/4EruCr14YLlXXR5b2HBGykePdkI=/0x0:690x459/120x80/s.glbimg.com/es/ge/f/original/2015/09/07/gadelhagostosa1.jpg" alt="Lutadora do UFC posta foto de topless junto com as duas irmÃ£s; amplie aqui (ReproduÃ§Ã£o/Instagram)" title="Lutadora do UFC posta foto de topless junto com as duas irmÃ£s; amplie aqui (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/4EruCr14YLlXXR5b2HBGykePdkI=/0x0:690x459/120x80/s.glbimg.com/es/ge/f/original/2015/09/07/gadelhagostosa1.jpg" data-url-smart_horizontal="dtt321eQGVsnRSKzM1jhF-24XWo=/90x0/smart/filters:strip_icc()/" data-url-smart="dtt321eQGVsnRSKzM1jhF-24XWo=/90x0/smart/filters:strip_icc()/" data-url-feature="dtt321eQGVsnRSKzM1jhF-24XWo=/90x0/smart/filters:strip_icc()/" data-url-tablet="rQwKOtifctoPPwckRp_xyLSFRH8=/70x50/smart/filters:strip_icc()/" data-url-desktop="XvlYCSUkl0Xrw2VKE0VtppdB-CU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Lutadora do UFC posta foto de topless junto com as duas irmÃ£s; amplie aqui</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section><section class="area etc-column analytics-area   last-has-photo
            
        
        "><div class="destaques-container analytics-area analytics-id-L"><div class="destaque destaque-secundario destaque-secundario-medio-foto-topo hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/Bastidores/noticia/2015/09/gracyanne-barbosa-comemora-chegada-do-setimo-cachorro-em-casa-mamae-muito-feliz.html" class="foto" title="Gracy e Belo celebram o 7Âº cÃ£o em casa: &#39;MamÃ£e muito feliz&#39;; veja as fotos aqui (Arquivo pessoal)" rel="bookmark"><span class="borda-foto"><span></span><img width="335" height="180" src="http://s2.glbimg.com/F6DxkhrT4X2e3kj17SN55lpIJ3c=/filters:quality(10):strip_icc()/s2.glbimg.com/5eoGkkxDOQZodaUGLo4G8jOgwCc=/266x89:690x317/335x180/s.glbimg.com/et/gs/f/original/2015/09/07/4_1.jpg" alt="Gracy e Belo celebram o 7Âº cÃ£o em casa: &#39;MamÃ£e muito feliz&#39;; veja as fotos aqui (Arquivo pessoal)" title="Gracy e Belo celebram o 7Âº cÃ£o em casa: &#39;MamÃ£e muito feliz&#39;; veja as fotos aqui (Arquivo pessoal)"
                data-original-image="s2.glbimg.com/5eoGkkxDOQZodaUGLo4G8jOgwCc=/266x89:690x317/335x180/s.glbimg.com/et/gs/f/original/2015/09/07/4_1.jpg" data-url-smart_horizontal="hdIpPSUpR-G6N5FyiUxb1XrjwFw=/90x0/smart/filters:strip_icc()/" data-url-smart="hdIpPSUpR-G6N5FyiUxb1XrjwFw=/90x0/smart/filters:strip_icc()/" data-url-feature="hdIpPSUpR-G6N5FyiUxb1XrjwFw=/90x0/smart/filters:strip_icc()/" data-url-tablet="Rp9anFXAu2NF6XChEvHwdNvKjlg=/220x125/smart/filters:strip_icc()/" data-url-desktop="YU5ltF-LCgzLggapzJu4II66y8s=/335x180/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Gracy e Belo celebram o 7Âº cÃ£o em casa: &#39;MamÃ£e muito feliz&#39;; veja as fotos aqui</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 17:21:21" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/paparazzo/noticia/2015/09/aos-59-anos-monique-evans-posa-com-caca-werneck-sem-plasticas.html" class="foto" title="Evans posa e fala que nÃ£o vai mais a praia: &#39;Acham que a gente nÃ£o envelhece&#39; (Mari Gibara / Paparazzo)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/uSEqdQOeAWPLLvMUbvFLt0hTJNA=/filters:quality(10):strip_icc()/s2.glbimg.com/rnsSkwuuys1_UiWyJR-7kLrQPtk=/263x84:737x401/120x80/s.glbimg.com/jo/eg/f/original/2015/09/02/cacamonique2352352.jpg" alt="Evans posa e fala que nÃ£o vai mais a praia: &#39;Acham que a gente nÃ£o envelhece&#39; (Mari Gibara / Paparazzo)" title="Evans posa e fala que nÃ£o vai mais a praia: &#39;Acham que a gente nÃ£o envelhece&#39; (Mari Gibara / Paparazzo)"
                data-original-image="s2.glbimg.com/rnsSkwuuys1_UiWyJR-7kLrQPtk=/263x84:737x401/120x80/s.glbimg.com/jo/eg/f/original/2015/09/02/cacamonique2352352.jpg" data-url-smart_horizontal="J6Q0tdBFq_gGao_f0sFAOBRSrcg=/90x0/smart/filters:strip_icc()/" data-url-smart="J6Q0tdBFq_gGao_f0sFAOBRSrcg=/90x0/smart/filters:strip_icc()/" data-url-feature="J6Q0tdBFq_gGao_f0sFAOBRSrcg=/90x0/smart/filters:strip_icc()/" data-url-tablet="JFpb9Hb9CQXPEtmWqDZ8hfHDcwQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="EyeesUVuHE2Z72hs3CZTg8rodd8=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Evans posa e fala que nÃ£o vai mais a praia: &#39;Acham que a gente nÃ£o envelhece&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 17:17:48" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/musica/noticia/2015/09/tati-zaqui-mostra-seios-rebola-e-beija-homem-e-mulher-em-novo-clipe.html" class="foto" title="Tati Zaqui lanÃ§a clipe de topless e beijando homem e mulher no MÃ©xico; assista (Youtube / ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/W-hp78Rr2xb70ZWBSaDIOkrtBZI=/filters:quality(10):strip_icc()/s2.glbimg.com/nvtbY4SDlWxf2j9oCBXw-nNfTYY=/406x81:812x352/120x80/s.glbimg.com/jo/eg/f/original/2015/09/07/tatizaqui4.jpg" alt="Tati Zaqui lanÃ§a clipe de topless e beijando homem e mulher no MÃ©xico; assista (Youtube / ReproduÃ§Ã£o)" title="Tati Zaqui lanÃ§a clipe de topless e beijando homem e mulher no MÃ©xico; assista (Youtube / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/nvtbY4SDlWxf2j9oCBXw-nNfTYY=/406x81:812x352/120x80/s.glbimg.com/jo/eg/f/original/2015/09/07/tatizaqui4.jpg" data-url-smart_horizontal="iX90pS7JaX_eC_ZP6QojC2O1VyY=/90x0/smart/filters:strip_icc()/" data-url-smart="iX90pS7JaX_eC_ZP6QojC2O1VyY=/90x0/smart/filters:strip_icc()/" data-url-feature="iX90pS7JaX_eC_ZP6QojC2O1VyY=/90x0/smart/filters:strip_icc()/" data-url-tablet="ndSxbWGyDHq05Gk2nhx9GKwNCz0=/70x50/smart/filters:strip_icc()/" data-url-desktop="E0wgY5LoVEjSe-eFj_TP5BlGzSU=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Tati Zaqui lanÃ§a clipe de topless e beijando homem e mulher no MÃ©xico; assista</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/Bastidores/noticia/2015/09/tatiele-polyana-fala-sobre-vida-pos-bbb-e-comenta-repercussao-do-bumbum-da-rival-aline.html" class="" title="Ex-BBB fala da vida pÃ³s-reality e da repercussÃ£o do bumbum da &#39;rival&#39; Aline (Arquivo pessoal)" rel="bookmark"><span class="conteudo"><p>Ex-BBB fala da vida pÃ³s-reality e da repercussÃ£o do bumbum da &#39;rival&#39; Aline</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 18:48:03" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/musica/noticia/2015/09/empresario-da-banda-calypso-confirme-deslize-de-chimbinha.html" class="foto" title="EmpresÃ¡rio da Calypso fala em &#39;deslize&#39; de Chimbinha: &#39;NÃ£o tem como esconder&#39; (DivulgaÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/DEEiTnFyQMD3kjMnMuhxIBksr5w=/filters:quality(10):strip_icc()/s2.glbimg.com/Wj1Bq7AsvlC1bRVIhuPhJOEsokk=/105x14:520x290/120x80/e.glbimg.com/og/ed/f/original/2015/08/19/joelma_e_chimbinha.jpg" alt="EmpresÃ¡rio da Calypso fala em &#39;deslize&#39; de Chimbinha: &#39;NÃ£o tem como esconder&#39; (DivulgaÃ§Ã£o)" title="EmpresÃ¡rio da Calypso fala em &#39;deslize&#39; de Chimbinha: &#39;NÃ£o tem como esconder&#39; (DivulgaÃ§Ã£o)"
                data-original-image="s2.glbimg.com/Wj1Bq7AsvlC1bRVIhuPhJOEsokk=/105x14:520x290/120x80/e.glbimg.com/og/ed/f/original/2015/08/19/joelma_e_chimbinha.jpg" data-url-smart_horizontal="D2Jd6NVxuFT_aEecJRj4NPAC-T0=/90x0/smart/filters:strip_icc()/" data-url-smart="D2Jd6NVxuFT_aEecJRj4NPAC-T0=/90x0/smart/filters:strip_icc()/" data-url-feature="D2Jd6NVxuFT_aEecJRj4NPAC-T0=/90x0/smart/filters:strip_icc()/" data-url-tablet="ldXuqHMc7SC07w5H8ChsllPewRs=/70x50/smart/filters:strip_icc()/" data-url-desktop="0WO22IPm0V1WHQWAgptOUduMs6Q=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>EmpresÃ¡rio da Calypso fala em &#39;deslize&#39; de Chimbinha: &#39;NÃ£o tem como esconder&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 15:52:57" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/09/gloria-pires-posta-foto-com-familia-em-dia-da-independencia-do-brasil.html" class="foto" title="GlÃ³ria Pires reÃºne a famÃ­lia toda e divulga mensagens: &#39;Viva a IndependÃªncia!&#39; (ReproduÃ§Ã£o/Instagram)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/Jf65guHCbsa4Gmt3-iiNs35CMXE=/filters:quality(10):strip_icc()/s2.glbimg.com/vDl8KEINKglT7ZvZwwgye8jRigg=/0x0:860x574/120x80/s.glbimg.com/jo/eg/f/original/2015/09/07/gloria_pires_com_a_familia.png" alt="GlÃ³ria Pires reÃºne a famÃ­lia toda e divulga mensagens: &#39;Viva a IndependÃªncia!&#39; (ReproduÃ§Ã£o/Instagram)" title="GlÃ³ria Pires reÃºne a famÃ­lia toda e divulga mensagens: &#39;Viva a IndependÃªncia!&#39; (ReproduÃ§Ã£o/Instagram)"
                data-original-image="s2.glbimg.com/vDl8KEINKglT7ZvZwwgye8jRigg=/0x0:860x574/120x80/s.glbimg.com/jo/eg/f/original/2015/09/07/gloria_pires_com_a_familia.png" data-url-smart_horizontal="9mo1UiE2QpP7ad_VX1KejSV-1es=/90x0/smart/filters:strip_icc()/" data-url-smart="9mo1UiE2QpP7ad_VX1KejSV-1es=/90x0/smart/filters:strip_icc()/" data-url-feature="9mo1UiE2QpP7ad_VX1KejSV-1es=/90x0/smart/filters:strip_icc()/" data-url-tablet="xqwdLDlnhEeqF6HqcWsfKmF4WGM=/70x50/smart/filters:strip_icc()/" data-url-desktop="1QCmwGSImiEiAibBUeu3TRKdjwE=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>GlÃ³ria Pires reÃºne a famÃ­lia toda e divulga mensagens: &#39;Viva a IndependÃªncia!&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 16:55:10" class="chamada chamada-principal mobile-grid-full"><a href="http://ego.globo.com/famosos/noticia/2015/09/diogo-nogueira-abre-academia-com-mulher-e-fala-de-saude-e-barriguinha.html" class="" title="Casado com personal, Diogo Nogueira fala de boa forma e planeja 2Âº filho: &#39;JÃ¡ teria tido&#39; (Marcos Serra Lima/EGO)" rel="bookmark"><span class="conteudo"><p>Casado com personal, Diogo Nogueira fala de boa forma e planeja 2Âº filho: &#39;JÃ¡ teria tido&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 18:48:16" class="chamada chamada-principal mobile-grid-full"><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/09/fernanda-machado-posa-com-o-filho-e-diz-que-deu-pausa-no-trabalho-o-resto-pode-esperar.html" class="foto" title="Fernanda Machado fala do filho e diz que deu um tempo no trabalho: &#39;Pode esperar&#39; (Instagram / ReproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/KNzrhiFFQ8D8lnC8iaubrygmGOY=/filters:quality(10):strip_icc()/s2.glbimg.com/IrHvXID2p_OspVgG-t_FuppiJiw=/0x96:640x523/120x80/e.glbimg.com/og/ed/f/original/2015/09/07/11939549_1479526235676906_1129846855_n.jpg" alt="Fernanda Machado fala do filho e diz que deu um tempo no trabalho: &#39;Pode esperar&#39; (Instagram / ReproduÃ§Ã£o)" title="Fernanda Machado fala do filho e diz que deu um tempo no trabalho: &#39;Pode esperar&#39; (Instagram / ReproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/IrHvXID2p_OspVgG-t_FuppiJiw=/0x96:640x523/120x80/e.glbimg.com/og/ed/f/original/2015/09/07/11939549_1479526235676906_1129846855_n.jpg" data-url-smart_horizontal="TyiyQQD6jdCdmI_gy3XgjTHpHLY=/90x0/smart/filters:strip_icc()/" data-url-smart="TyiyQQD6jdCdmI_gy3XgjTHpHLY=/90x0/smart/filters:strip_icc()/" data-url-feature="TyiyQQD6jdCdmI_gy3XgjTHpHLY=/90x0/smart/filters:strip_icc()/" data-url-tablet="m29En4cv1otu1M3h5gAkNBuS2DQ=/70x50/smart/filters:strip_icc()/" data-url-desktop="yqjzi_0WkJ9HfZkRVD5q87Hf2ic=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>Fernanda Machado fala do filho e diz que deu um tempo no trabalho: &#39;Pode esperar&#39;</p></span></a></div></div><div class="separator"></div><div class="destaque destaque-terciario destaque-terciario-medio-foto-lado hentry "><div data-photo-subtitle="2015-09-07 13:04:01" class="chamada chamada-principal mobile-grid-full"><a href="http://gshow.globo.com/receitas/finaestampa/souffle-de-queijo-4f15a441712ad92875001914" class="foto" title="SuflÃª com dois queijos leva noz moscada e fica pronto rapidinho; prepare e inove (reproduÃ§Ã£o)" rel="bookmark"><span class='borda-foto'><span></span><img width="120" height="80" src="http://s2.glbimg.com/oxGRnNDByKuPW4Tn1F4t_Wbdl4Q=/filters:quality(10):strip_icc()/s2.glbimg.com/xW3hNjdwVyiOdyvr1WLecaV27uE=/119x14:533x289/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/souffle_de_queijo.jpg" alt="SuflÃª com dois queijos leva noz moscada e fica pronto rapidinho; prepare e inove (reproduÃ§Ã£o)" title="SuflÃª com dois queijos leva noz moscada e fica pronto rapidinho; prepare e inove (reproduÃ§Ã£o)"
                data-original-image="s2.glbimg.com/xW3hNjdwVyiOdyvr1WLecaV27uE=/119x14:533x289/120x80/s.glbimg.com/en/ho/f/original/2015/09/07/souffle_de_queijo.jpg" data-url-smart_horizontal="U5B19BG9FuxoHiaE_6Y1DEyACiI=/90x0/smart/filters:strip_icc()/" data-url-smart="U5B19BG9FuxoHiaE_6Y1DEyACiI=/90x0/smart/filters:strip_icc()/" data-url-feature="U5B19BG9FuxoHiaE_6Y1DEyACiI=/90x0/smart/filters:strip_icc()/" data-url-tablet="cAkRx4J7UJskSFqOBuVA1mqOriU=/70x50/smart/filters:strip_icc()/" data-url-desktop="UOtttLhSOPrbBLWnS3AN0EIX6TA=/120x80/smart/filters:strip_icc()/"
            /></span><span class="conteudo"><p>SuflÃª com dois queijos leva noz moscada e fica pronto rapidinho; prepare e inove</p></span></a></div></div></div><div class="show-more"><span class="setinha-show-more"></span></div></section></div><div id="ad-position-middle3" class="opec"><div id="banner_slb_fim" class="tag-manager-publicidade-container"></div></div><div class="container columns clearfix glb-area-colunas second-area"><section class="area news-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-noticias
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-noticias').data('widgetData', {"BA": {"ordering": 10, "url": "http://g1.globo.com/bahia", "regions": {}, "name": "Bahia"}, "DF": {"ordering": 30, "url": "http://g1.globo.com/distrito-federal", "regions": {}, "name": "Distrito Federal"}, "PR": {"ordering": 100, "url": "http://g1.globo.com/pr/parana/", "regions": {"norte-e-noroeste": {"ordering": 1, "url": "http://g1.globo.com/pr/norte-noroeste/", "name": "Norte e Noroeste"}, "campos-gerais-e-sul": {"ordering": 1, "url": "http://g1.globo.com/pr/campos-gerais-sul/", "name": "Campos Gerais e Sul"}, "curitiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pr/parana/", "name": "Curitiba e Regi\u00e3o"}, "oeste-e-sudoeste": {"ordering": 1, "url": "http://g1.globo.com/pr/oeste-sudoeste/", "name": "Oeste e Sudoeste"}}, "name": "Paran\u00e1"}, "RR": {"ordering": 113, "url": "http://g1.globo.com/rr/roraima/", "regions": {}, "name": "Roraima"}, "RS": {"ordering": 115, "url": "http://g1.globo.com/rs/rio-grande-do-sul/", "regions": {}, "name": "Rio Grande do Sul"}, "PB": {"ordering": 90, "url": "http://g1.globo.com/pb/paraiba/", "regions": {}, "name": "Para\u00edba"}, "TO": {"ordering": 140, "url": "http://g1.globo.com/to/tocantins/", "regions": {}, "name": "Tocantins"}, "PA": {"ordering": 85, "url": "http://g1.globo.com/pa/para/", "regions": {"santarem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/santarem-regiao/", "name": "Santar\u00e9m e regi\u00e3o"}, "belem-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pa/para/", "name": "Bel\u00e9m e Regi\u00e3o"}}, "name": "Par\u00e1"}, "PE": {"ordering": 95, "url": "http://g1.globo.com/pernambuco", "regions": {"caruaru-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pe/caruaru-regiao/", "name": "Caruaru e Regi\u00e3o"}, "petrolina": {"ordering": 1, "url": "http://g1.globo.com/pe/petrolina-regiao", "name": "Petrolina"}, "recife-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/pernambuco/", "name": "Recife e Regi\u00e3o"}}, "name": "Pernambuco"}, "RN": {"ordering": 111, "url": "http://g1.globo.com/rn/rio-grande-do-norte/", "regions": {}, "name": "Rio Grande do Norte"}, "RO": {"ordering": 112, "url": "http://g1.globo.com/ro/rondonia", "regions": {}, "name": "Rond\u00f4nia"}, "RJ": {"ordering": 110, "url": "http://g1.globo.com/rio-de-janeiro", "regions": {"regiao-serrana": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-serrana/", "name": "Regi\u00e3o Serrana"}, "regiao-dos-lagos": {"ordering": 1, "url": "http://g1.globo.com/rj/regiao-dos-lagos/", "name": "Regi\u00e3o dos Lagos"}, "rio-de-janeiro-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/rio-de-janeiro/", "name": "Rio de Janeiro e Regi\u00e3o"}, "norte-fluminense": {"ordering": 1, "url": "http://g1.globo.com/rj/norte-fluminense/", "name": "Norte Fluminense"}, "sul-e-costa-verde": {"ordering": 1, "url": "http://g1.globo.com/rj/sul-do-rio-costa-verde/", "name": "Sul e Costa Verde"}}, "name": "Rio de Janeiro"}, "AC": {"ordering": 1, "url": "http://g1.globo.com/ac/acre/", "regions": {}, "name": "Acre"}, "AM": {"ordering": 5, "url": "http://g1.globo.com/am/amazonas/", "regions": {}, "name": "Amazonas"}, "AL": {"ordering": 3, "url": "http://g1.globo.com/al/alagoas/", "regions": {}, "name": "Alagoas"}, "CE": {"ordering": 20, "url": "http://g1.globo.com/ceara", "regions": {}, "name": "Cear\u00e1"}, "AP": {"ordering": 4, "url": "http://g1.globo.com/ap/amapa/", "regions": {}, "name": "Amap\u00e1"}, "GO": {"ordering": 50, "url": "http://g1.globo.com/goias", "regions": {}, "name": "Goi\u00e1s"}, "ES": {"ordering": 40, "url": "http://g1.globo.com/espirito-santo", "regions": {}, "name": "Esp\u00edrito Santo"}, "MG": {"ordering": 60, "url": "http://g1.globo.com/minas-gerais", "regions": {"zona-da-mata": {"ordering": 1, "url": "http://g1.globo.com/mg/zona-da-mata/", "name": "Zona da Mata"}, "triangulo-mineiro": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/triangulo-mineiro/", "name": "Tri\u00e2ngulo Mineiro"}, "centro-oeste": {"ordering": 1, "url": "http://g1.globo.com/mg/centro-oeste/", "name": "Centro-Oeste"}, "sul-de-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/sul-de-minas/", "name": "Sul de Minas"}, "belo-horizonte-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/minas-gerais/", "name": "Belo Horizonte e Regi\u00e3o"}, "grande-minas": {"ordering": 1, "url": "http://g1.globo.com/mg/grande-minas/", "name": "Grande Minas"}, "vales-de-minas-gerais": {"ordering": 1, "url": "http://g1.globo.com/mg/vales-mg/", "name": "Vales de Minas Gerais"}}, "name": "Minas Gerais"}, "PI": {"ordering": 97, "url": "http://g1.globo.com/pi/piaui/", "regions": {}, "name": "Piau\u00ed"}, "MA": {"ordering": 55, "url": "http://g1.globo.com/ma/maranhao/", "regions": {}, "name": "Maranh\u00e3o"}, "SP": {"ordering": 130, "url": "http://g1.globo.com/sao-paulo", "regions": {"presidente-prudente": {"ordering": 1, "url": "http://g1.globo.com/sp/presidente-prudente-regiao/", "name": "Presidente Prudente"}, "itapetininga-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/itapetininga-regiao/", "name": "Itapetininga e Regi\u00e3o"}, "rio-preto-e-aracatuba": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sao-jose-do-rio-preto-aracatuba/", "name": "Rio Preto e Ara\u00e7atuba"}, "ribeirao-preto-e-franca": {"ordering": 1, "url": "http://g1.globo.com/sp/ribeirao-preto-franca/", "name": "Ribeir\u00e3o Preto e Franca"}, "sao-paulo-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/", "name": "S\u00e3o Paulo e Regi\u00e3o"}, "sao-carlos-e-araraquara": {"ordering": 1, "url": "http://g1.globo.com/sp/sao-carlos-regiao/", "name": "S\u00e3o Carlos e Araraquara"}, "sorocaba-e-jundiai": {"ordering": 1, "url": "http://g1.globo.com/sao-paulo/sorocaba-jundiai/", "name": "Sorocaba e Jundia\u00ed"}, "vale-do-paraiba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/vale-do-paraiba-regiao/", "name": "Vale do Para\u00edba e Regi\u00e3o"}, "piracicaba-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/piracicaba-regiao/", "name": "Piracicaba e Regi\u00e3o"}, "campinas-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/campinas-regiao/", "name": "Campinas e Regi\u00e3o"}, "santos-e-regiao": {"ordering": 1, "url": "http://g1.globo.com/sp/santos-regiao/", "name": "Santos e Regi\u00e3o"}, "bauru-e-marilia": {"ordering": 1, "url": "http://g1.globo.com/sp/bauru-marilia/", "name": "Bauru e Mar\u00edlia"}}, "name": "S\u00e3o Paulo"}, "MT": {"ordering": 75, "url": "http://g1.globo.com/mato-grosso", "regions": {}, "name": "Mato Grosso"}, "MS": {"ordering": 70, "url": "http://g1.globo.com/mato-grosso-do-sul", "regions": {}, "name": "Mato Grosso do Sul"}, "SC": {"ordering": 118, "url": "http://g1.globo.com/sc/santa-catarina/", "regions": {}, "name": "Santa Catarina"}, "SE": {"ordering": 120, "url": "http://g1.globo.com/se/sergipe/", "regions": {}, "name": "Sergipe"}});}});}
</script><div id="libby-home-widget-economia" class="libby-home-widgetBase libby-home-widget-economia analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais notÃ­cias" href="http://g1.globo.com/"><span class="text">mais notÃ­cias</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area sports-column"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-times soccer-teams
"></div><div id="libby-home-widget-brasileirao" class="libby-home-widgetBase libby-home-widget-brasileirao analytics-area analytics-id-W"><input type="hidden" name="rodada_selecionada" value="current"></div><a class="link-see-more analytics-area analytics-id-L" title="mais esportes" href="http://globoesporte.globo.com/"><span class="text">mais esportes</span><span class="arrow">&rsaquo;</span></a></div></section><section class="area etc-column analytics-area"><div class="widget"><div class="analytics-area analytics-id-P box-widgets-personalizaveis libby-widgets-personalizaveis-novelas
"></div><script type="text/javascript">if(window.jQuery){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}else{libby.eventsArray.push({fn: function(){$('div.libby-widgets-personalizaveis-novelas').data('widgetData', [{"ordering": 30, "default": false, "cor": "#E12559", "name": "Malha\u00e7\u00e3o", "url": "http://gshow.globo.com/novelas/malhacao/2015/", "logo": "http://s2.glbimg.com/tjsQl1SMvddbD-BGiSoC2Nb6kqg=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-36x20.jpg", "slug": "malhacao", "url_feed": "http://gshow.globo.com/novelas/malhacao/2015/rss/", "logo_tv": "http://s2.glbimg.com/y8thjGY4IQuSvc4VfunyXi0d8C4=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/17/malhacao-2015-45x30.jpg"}, {"ordering": 35, "default": false, "cor": "#FF7F00", "name": "Al\u00e9m do tempo", "url": "http://gshow.globo.com/novelas/alem-do-tempo/", "logo": "http://s2.glbimg.com/wDQDURw8tQncX6dLdavJtiUCwkE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/07/14/logo38x20.jpg", "slug": "alem-do-tempo", "url_feed": "http://gshow.globo.com/novelas/alem-do-tempo/rss/", "logo_tv": "http://s2.glbimg.com/APKhqTvAHfh9O2TFUkXyG2JQqdw=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/07/14/logo45x30.jpg"}, {"ordering": 50, "default": false, "cor": "#FF7F00", "name": "I Love Parais\u00f3polis", "url": "http://gshow.globo.com/novelas/i-love-paraisopolis/index.html", "logo": "http://s2.glbimg.com/ChecsIkfHlI4H_yHoe_c_6tXTA4=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis36x20.jpg", "slug": "alto-astral", "url_feed": "http://gshow.globo.com/novelas/i-love-paraisopolis/rss/", "logo_tv": "http://s2.glbimg.com/6Kj2EtayTr5_t0ya4Iy8HHIn-vg=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/05/11/logo-paraisopolis45x30.jpg"}, {"ordering": 60, "default": true, "cor": "#FF7F00", "name": "A Regra do Jogo", "url": "http://gshow.globo.com/novelas/a-regra-do-jogo/", "logo": "http://s2.glbimg.com/K-Nvvvm7zp2F7zS8TcY6TkIcxoE=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-36x20.jpg", "slug": "babilonia", "url_feed": "http://gshow.globo.com/novelas/a-regra-do-jogo/rss/", "logo_tv": "http://s2.glbimg.com/4B7Jk0R6K1HlAAhuZIrDOJp8hbs=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/08/31/a-regra-do-jogo-45x30.jpg"}, {"ordering": 70, "default": false, "cor": "#FF7F00", "name": "Verdades Secretas", "url": "http://gshow.globo.com/novelas/verdades-secretas/index.html", "logo": "http://s2.glbimg.com/ouhqJ9mLuAXh-gcEUW8wk165DUQ=/0x0:36x20/36x20/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_1.jpg", "slug": "verdades-secretas", "url_feed": "http://gshow.globo.com/novelas/verdades-secretas/rss", "logo_tv": "http://s2.glbimg.com/Huyh0y1AcqgjDpRaBLWCCkQY-Oo=/0x0:45x30/45x30/s.glbimg.com/en/ho/f/original/2015/06/08/logo_verdades_secretas_menu_2.jpg"}]);}});}
</script><div id="libby-home-widget-horoscopo" class="libby-home-widgetBase libby-home-widget-horoscopo analytics-area analytics-id-W"></div><a class="link-see-more analytics-area analytics-id-L" title="mais entretenimento" href="http://variedades.globo.com/"><span class="text">mais variedades</span><span class="arrow">&rsaquo;</span></a></div></section></div><section class="area central container analytics-area analytics-id-O"><div class="separator"></div><section class="agrupador-quadruplo-tecnologia analytics-area analytics-id-T clearfix franja-inferior tecnologia"><div class="cabecalho"><h2><a href="http://www.techtudo.com.br/" title="TECNOLOGIA &amp; GAMES"><span class="word word-0">TECNOLOGIA</span><span class="word word-1">&</span><span class="word word-2">GAMES</span></a></h2><div id="ad-position-x34" class="opec"><div id="banner_selo4" class="tag-manager-publicidade-container"></div></div><aside class="links analytics-area analytics-id-T"><ul><li><a href="http://www.techtudo.com.br/jogos/" title="jogos">jogos</a></li><li><a href="http://www.techtudo.com.br/downloads/" title="downloads">downloads</a></li><li><a href="http://forum.techtudo.com.br/" title="fÃ³rum">fÃ³rum</a></li><li><a href="http://www.techtudo.com.br/mobile/" title="celulares">celulares</a></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaque"><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/09/forza-6-tem-pista-no-rio-de-janeiro-cheia-de-bizarrices-e-erros.html" title="Forza 6 traz pista no Rio cheio de &#39;bizarrices&#39; pela cidade; confira"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/VuD2zjPb7lYXR3ShUwEZhigrgFs=/filters:quality(10):strip_icc()/s2.glbimg.com/Eqat9hHckp-Nex2XzlBEr8QU6NY=/0x22:694x391/245x130/s.glbimg.com/po/tt2/f/original/2015/09/04/forza-6-partida.jpg" alt="Forza 6 traz pista no Rio cheio de &#39;bizarrices&#39; pela cidade; confira (Forza 6 tem pista no Rio de Janeiro com conceitos bizarros (Foto: ReproduÃ§Ã£o/Murilo Molina))" title="Forza 6 traz pista no Rio cheio de &#39;bizarrices&#39; pela cidade; confira (Forza 6 tem pista no Rio de Janeiro com conceitos bizarros (Foto: ReproduÃ§Ã£o/Murilo Molina))"
             data-original-image="s2.glbimg.com/Eqat9hHckp-Nex2XzlBEr8QU6NY=/0x22:694x391/245x130/s.glbimg.com/po/tt2/f/original/2015/09/04/forza-6-partida.jpg" data-url-smart_horizontal="kLNeWxOo9JQcOevg5PqmjG_YpZY=/90x56/smart/filters:strip_icc()/" data-url-smart="kLNeWxOo9JQcOevg5PqmjG_YpZY=/90x56/smart/filters:strip_icc()/" data-url-feature="kLNeWxOo9JQcOevg5PqmjG_YpZY=/90x56/smart/filters:strip_icc()/" data-url-tablet="kSLETAixur6rV-H2ZA5vT_5Alzg=/160x96/smart/filters:strip_icc()/" data-url-desktop="XfygzTEvZ15_bNfQ9GUGUqw61LU=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Forza 6 traz pista no Rio cheio de &#39;bizarrices&#39; pela cidade; confira</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/09/tecnologia-transfere-dados-atraves-do-corpo-e-e-comparada-ao-bluetooth.html" title="Tecnologia supera o Bluetooth e envia arquivos usando seu corpo"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/PQ9rFHxv0yy1S2guCKXozmfhLTk=/filters:quality(10):strip_icc()/s2.glbimg.com/3L1TwTEyrncWstoL7QsndiijhcM=/89x106:643x400/245x130/s.glbimg.com/po/tt2/f/original/2015/09/04/corpo-humano-troca-de-dados.png" alt="Tecnologia supera o Bluetooth e envia arquivos usando seu corpo (Pesquisadores usaram membros do corpo para criar bobinas, que induzem campo magnÃ©tico para a troca de informaÃ§Ãµes (Foto: DivulgaÃ§Ã£o/UC San Diego))" title="Tecnologia supera o Bluetooth e envia arquivos usando seu corpo (Pesquisadores usaram membros do corpo para criar bobinas, que induzem campo magnÃ©tico para a troca de informaÃ§Ãµes (Foto: DivulgaÃ§Ã£o/UC San Diego))"
             data-original-image="s2.glbimg.com/3L1TwTEyrncWstoL7QsndiijhcM=/89x106:643x400/245x130/s.glbimg.com/po/tt2/f/original/2015/09/04/corpo-humano-troca-de-dados.png" data-url-smart_horizontal="M1kBDsSCWJvci9w0T4MHCDzY2Lw=/90x56/smart/filters:strip_icc()/" data-url-smart="M1kBDsSCWJvci9w0T4MHCDzY2Lw=/90x56/smart/filters:strip_icc()/" data-url-feature="M1kBDsSCWJvci9w0T4MHCDzY2Lw=/90x56/smart/filters:strip_icc()/" data-url-tablet="ZRo1PrjJU1oW1AgZL3BLo6oHAvU=/160x96/smart/filters:strip_icc()/" data-url-desktop="Uo3YkgpeewUgPgD0MUazq9wPvGQ=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Tecnologia supera o Bluetooth e envia arquivos usando seu corpo</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/noticias/noticia/2015/09/oito-smartphones-com-4g-vendidos-por-ate-r-600-no-brasil.html" title="Veja melhores opÃ§Ãµes para ter um smart com 4G gastando pouco"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/D0Wzl8TViBRiHcPat-HMOu1q3Uk=/filters:quality(10):strip_icc()/s2.glbimg.com/fghY8d2rwqOWtUVcQYPUUcCubTg=/0x64:635x401/245x130/s.glbimg.com/po/tt2/f/original/2015/09/02/motoe.jpg" alt="Veja melhores opÃ§Ãµes para ter um smart com 4G gastando pouco (Moto E da Motorola traz tecnologia 4G, mesmo sendo um smartphone considero de entrada. Foto: DivulgaÃ§Ã£o)" title="Veja melhores opÃ§Ãµes para ter um smart com 4G gastando pouco (Moto E da Motorola traz tecnologia 4G, mesmo sendo um smartphone considero de entrada. Foto: DivulgaÃ§Ã£o)"
             data-original-image="s2.glbimg.com/fghY8d2rwqOWtUVcQYPUUcCubTg=/0x64:635x401/245x130/s.glbimg.com/po/tt2/f/original/2015/09/02/motoe.jpg" data-url-smart_horizontal="33jOanJrG0MAzO3uQWN6fusZnHA=/90x56/smart/filters:strip_icc()/" data-url-smart="33jOanJrG0MAzO3uQWN6fusZnHA=/90x56/smart/filters:strip_icc()/" data-url-feature="33jOanJrG0MAzO3uQWN6fusZnHA=/90x56/smart/filters:strip_icc()/" data-url-tablet="xY5rytrlysLbf5YpIRf4g2iPHc0=/160x96/smart/filters:strip_icc()/" data-url-desktop="AM171kL2My_p43itCOiDdY0phmc=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>Veja melhores opÃ§Ãµes para ter um smart com 4G gastando pouco</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://www.techtudo.com.br/tudo-sobre/wifi-free.html" title="App &#39;mÃ¡gico&#39; encontra redes de Wi-Fi em qualquer lugar; baixe"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/_8PrXxgai-RstTKgcClpSuOwurA=/filters:quality(10):strip_icc()/s2.glbimg.com/4xhF-e6rWvjhjf6pKAoOgLpvDF4=/0x0:400x212/245x130/s.glbimg.com/po/tt2/f/original/2015/07/15/stock-footage-connecting-to-wifi-with-smartphone.jpg" alt="App &#39;mÃ¡gico&#39; encontra redes de Wi-Fi em qualquer lugar; baixe (Pond5)" title="App &#39;mÃ¡gico&#39; encontra redes de Wi-Fi em qualquer lugar; baixe (Pond5)"
             data-original-image="s2.glbimg.com/4xhF-e6rWvjhjf6pKAoOgLpvDF4=/0x0:400x212/245x130/s.glbimg.com/po/tt2/f/original/2015/07/15/stock-footage-connecting-to-wifi-with-smartphone.jpg" data-url-smart_horizontal="U73n5ek6UIfIJ_tzizCyn9hGQ1g=/90x56/smart/filters:strip_icc()/" data-url-smart="U73n5ek6UIfIJ_tzizCyn9hGQ1g=/90x56/smart/filters:strip_icc()/" data-url-feature="U73n5ek6UIfIJ_tzizCyn9hGQ1g=/90x56/smart/filters:strip_icc()/" data-url-tablet="O6AA8H_1ez2grCXu3iHX5B99gXw=/160x96/smart/filters:strip_icc()/" data-url-desktop="wVMaQRdm53HRy0g3cRT0ANcCPpM=/245x130/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><p>App &#39;mÃ¡gico&#39; encontra redes de Wi-Fi em qualquer lugar; baixe</p></span></a></li></ul><div class="destaque-inferior analytics-area analytics-id-B"><ul><li class="techtudo"><a href="http://www.techtudo.com.br/" title="Teste agora a velocidade da sua internet"><span class="logo"></span><span class="title">Teste agora a velocidade da sua internet &rsaquo;</span></a></li><li class="exty"><a href="http://www.techtudo.com.br/tudo-sobre/extensao-globo-com.html" title="Adicione e veja as notÃ­cias em tempo real"><span class="logo"></span><span class="title">Adicione e veja as notÃ­cias em tempo real &rsaquo;</span></a></li></ul></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-tecnologia']);</script><div class="separator"></div><section class="agrupador-sextuplo analytics-area analytics-id-S"><section class="analytics-area analytics-id-E esquerda tvg"><div class="cabecalho"><h2><span class="word word-0">moda</span><span class="word word-1">&amp;</span><span class="word word-2">beleza</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://vogue.globo.com/moda/moda-news/noticia/2015/09/patricia-bonaldi-furacao-da-moda-brasileira-e-queridinha-das-celebs.html" alt="ConheÃ§a a designer que jÃ¡ foi operadora de telemarketing"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/oT_gRhHPtHyXhEtofhN08Dqu9sM=/filters:quality(10):strip_icc()/s2.glbimg.com/UtFIJyFfs_jrTSEsQW_th3zcAF0=/305x53:890x430/155x100/e.glbimg.com/og/ed/f/original/2015/09/07/patricia.jpg" alt="ConheÃ§a a designer que jÃ¡ foi operadora de telemarketing (zee nunes)" title="ConheÃ§a a designer que jÃ¡ foi operadora de telemarketing (zee nunes)"
            data-original-image="s2.glbimg.com/UtFIJyFfs_jrTSEsQW_th3zcAF0=/305x53:890x430/155x100/e.glbimg.com/og/ed/f/original/2015/09/07/patricia.jpg" data-url-smart_horizontal="xQ7E7DYbxzAo0T293l2znzoEsDc=/90x56/smart/filters:strip_icc()/" data-url-smart="xQ7E7DYbxzAo0T293l2znzoEsDc=/90x56/smart/filters:strip_icc()/" data-url-feature="xQ7E7DYbxzAo0T293l2znzoEsDc=/90x56/smart/filters:strip_icc()/" data-url-tablet="k1YJ9dvXLRG9k0zyiMOfq2O0BGg=/122x75/smart/filters:strip_icc()/" data-url-desktop="kiaIFKyFHM4L_qQ7a5ByanKyqWc=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>queridinha das celebs</h3><p>ConheÃ§a a designer que jÃ¡ foi operadora de telemarketing</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/moda/votacoes/saiba-como-usar-listras-para-misturar-estampas-vote-nos-seus-looks-favoritos.htm" alt="Clique e saiba como usar as listras para misturar vÃ¡rias estampas"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/OgBLqp9obc45VrHqJtOG96lE6as=/filters:quality(10):strip_icc()/s2.glbimg.com/cJTy6zGNXUa7ibeA6IaUvXCHUEI=/0x34:380x279/155x100/s.glbimg.com/en/ho/f/original/2015/09/07/looks-listras-mix-estampas-01.jpg" alt="Clique e saiba como usar as listras para misturar vÃ¡rias estampas (gnt)" title="Clique e saiba como usar as listras para misturar vÃ¡rias estampas (gnt)"
            data-original-image="s2.glbimg.com/cJTy6zGNXUa7ibeA6IaUvXCHUEI=/0x34:380x279/155x100/s.glbimg.com/en/ho/f/original/2015/09/07/looks-listras-mix-estampas-01.jpg" data-url-smart_horizontal="7TA334t29KgbELzegjKfntS4h4M=/90x56/smart/filters:strip_icc()/" data-url-smart="7TA334t29KgbELzegjKfntS4h4M=/90x56/smart/filters:strip_icc()/" data-url-feature="7TA334t29KgbELzegjKfntS4h4M=/90x56/smart/filters:strip_icc()/" data-url-tablet="dNYQVw_DZLzehAsTGWgWKQvp-2c=/122x75/smart/filters:strip_icc()/" data-url-desktop="1yfjcBH5cUKcqDAK7VRSnx7CltU=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>vote em seus looks favoritos</h3><p>Clique e saiba como usar as listras para misturar vÃ¡rias estampas</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistamarieclaire.globo.com/Moda/noticia/2015/09/jardim-dark-estampa-floral-da-vez-carrega-ares-sofisticados.html" alt="Estampa floral da vez carrega ares sofisticados; saiba mais aqui"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/MRkxMTrbZ6es5K8KWeLjltmJmQk=/filters:quality(10):strip_icc()/s2.glbimg.com/xb021qfQbaEGOWOEhjIxiOeGpiU=/0x0:620x399/155x100/s.glbimg.com/en/ho/f/original/2015/09/07/floral-dark-moda.jpg" alt="Estampa floral da vez carrega ares sofisticados; saiba mais aqui (divulgaÃ§Ã£o)" title="Estampa floral da vez carrega ares sofisticados; saiba mais aqui (divulgaÃ§Ã£o)"
            data-original-image="s2.glbimg.com/xb021qfQbaEGOWOEhjIxiOeGpiU=/0x0:620x399/155x100/s.glbimg.com/en/ho/f/original/2015/09/07/floral-dark-moda.jpg" data-url-smart_horizontal="LMLW09jZzBg1GILojJUJSQSlmZ4=/90x56/smart/filters:strip_icc()/" data-url-smart="LMLW09jZzBg1GILojJUJSQSlmZ4=/90x56/smart/filters:strip_icc()/" data-url-feature="LMLW09jZzBg1GILojJUJSQSlmZ4=/90x56/smart/filters:strip_icc()/" data-url-tablet="1WkR5qZW3BJF_5FKJYkkd5dNXaI=/122x75/smart/filters:strip_icc()/" data-url-desktop="Ag8dyoAyFYAb4zBWbcaoaVid-tI=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>jardim dark</h3><p>Estampa floral da vez carrega ares sofisticados; saiba mais aqui</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://gnt.globo.com/" title="GNT">GNT</a></h3></li><li><h3><a href="http://vogue.globo.com/" title="vogue">vogue</a></h3></li><li></li></ul></aside></section><div class="separator"></div><section class="analytics-area analytics-id-D direita tvg"><div class="cabecalho"><h2><span class="word word-0">CASA</span><span class="word word-1">&amp;</span><span class="word word-2">DECORAÃÃO</span></h2></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="destaque mobile-grid-full"><a class="foto" href="http://casavogue.globo.com/Interiores/Ambientes/noticia/2015/09/top-10-piscinas-o-paraiso-em-casa.html" alt="Lista traz dez paraÃ­sos aquÃ¡ticos construÃ­dos de diferentes formas"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/I1MnyhReICr7S1COr1wR1VOh1j0=/filters:quality(10):strip_icc()/s2.glbimg.com/DhBX9eA2wb3jq5GqkZaq9vf8dww=/242x91:545x287/155x100/e.glbimg.com/og/ed/f/original/2015/09/04/arquitetura_pool_k_02.jpg" alt="Lista traz dez paraÃ­sos aquÃ¡ticos construÃ­dos de diferentes formas (Divulga)" title="Lista traz dez paraÃ­sos aquÃ¡ticos construÃ­dos de diferentes formas (Divulga)"
            data-original-image="s2.glbimg.com/DhBX9eA2wb3jq5GqkZaq9vf8dww=/242x91:545x287/155x100/e.glbimg.com/og/ed/f/original/2015/09/04/arquitetura_pool_k_02.jpg" data-url-smart_horizontal="mLmzR_Dd_ViG6Wqno0UpDt-dg-8=/90x56/smart/filters:strip_icc()/" data-url-smart="mLmzR_Dd_ViG6Wqno0UpDt-dg-8=/90x56/smart/filters:strip_icc()/" data-url-feature="mLmzR_Dd_ViG6Wqno0UpDt-dg-8=/90x56/smart/filters:strip_icc()/" data-url-tablet="Xx1cOcLTv7iAQPruIkLL27ZLI3A=/122x75/smart/filters:strip_icc()/" data-url-desktop="Obaflor5ozCoLLmus9XtNlSELu4=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>piscinas deslumbrantes</h3><p>Lista traz dez paraÃ­sos aquÃ¡ticos construÃ­dos de diferentes formas</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://gnt.globo.com/programas/santa-ajuda/videos/4436071.htm" alt="Confira dicas para mudar totalmente a cara da sua cozinha; assista aqui"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/8NNPFyaG-UsEvavcOJrtnWjA43k=/filters:quality(10):strip_icc()/s2.glbimg.com/NC4y73vfcys-yhqJcUuqZgUtv2M=/28x0:579x356/155x100/s.glbimg.com/en/ho/f/original/2015/09/07/cozinha.jpg" alt="Confira dicas para mudar totalmente a cara da sua cozinha; assista aqui (ReproduÃ§Ã£o)" title="Confira dicas para mudar totalmente a cara da sua cozinha; assista aqui (ReproduÃ§Ã£o)"
            data-original-image="s2.glbimg.com/NC4y73vfcys-yhqJcUuqZgUtv2M=/28x0:579x356/155x100/s.glbimg.com/en/ho/f/original/2015/09/07/cozinha.jpg" data-url-smart_horizontal="a1czKuIL5NzmkaY0M9Qin-pzROU=/90x56/smart/filters:strip_icc()/" data-url-smart="a1czKuIL5NzmkaY0M9Qin-pzROU=/90x56/smart/filters:strip_icc()/" data-url-feature="a1czKuIL5NzmkaY0M9Qin-pzROU=/90x56/smart/filters:strip_icc()/" data-url-tablet="VIOkwUkpp45HaXt1ewP7BGRR8PQ=/122x75/smart/filters:strip_icc()/" data-url-desktop="CiTYEoEKHYJXHV7R133xURiPIeA=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>xÃ´ bagunÃ§a!</h3><p>Confira dicas para mudar totalmente a cara da sua cozinha; assista aqui</p></span></a></li><li class="destaque mobile-grid-full"><a class="foto" href="http://revistacasaejardim.globo.com/Casa-e-Jardim/Decoracao/Pequenos-espacos/fotos/2013/05/10-dicas-de-deco-para-o-primeiro-ape.html#F2" alt="Acompanhe ideias para montar o seu primeiro apartamento sem gastar muito"><span class="borda-foto"><span></span><img width="155" height="100" src="http://s2.glbimg.com/GUDlhyvpKPLGPk1vSIorYbKOXKc=/filters:quality(10):strip_icc()/s2.glbimg.com/qMmL8HdKxH3IbJPradJd7fvayFk=/97x51:878x555/155x100/s.glbimg.com/en/ho/f/original/2015/09/07/sala.jpg" alt="Acompanhe ideias para montar o seu primeiro apartamento sem gastar muito (Victor Afarro / Casa e Jardim)" title="Acompanhe ideias para montar o seu primeiro apartamento sem gastar muito (Victor Afarro / Casa e Jardim)"
            data-original-image="s2.glbimg.com/qMmL8HdKxH3IbJPradJd7fvayFk=/97x51:878x555/155x100/s.glbimg.com/en/ho/f/original/2015/09/07/sala.jpg" data-url-smart_horizontal="6Jd9ODuO1-TEtcMbmqrgiTdj14Q=/90x56/smart/filters:strip_icc()/" data-url-smart="6Jd9ODuO1-TEtcMbmqrgiTdj14Q=/90x56/smart/filters:strip_icc()/" data-url-feature="6Jd9ODuO1-TEtcMbmqrgiTdj14Q=/90x56/smart/filters:strip_icc()/" data-url-tablet="DLkhACQLSUvG6YY7mSdO59FbBhw=/122x75/smart/filters:strip_icc()/" data-url-desktop="PkfX_CjSGKJZABd87E-PG-XddoU=/155x100/smart/filters:strip_icc()/"
        /></span><span class="conteudo"><h3>vai sair da casa dos pais?</h3><p>Acompanhe ideias para montar o seu primeiro apartamento sem gastar muito</p></span></a></li></ul><aside class="links analytics-area analytics-id-B"><h3>veja tambÃ©m</h3><ul><li><h3><a href="http://revistacasaejardim.globo.com/" title="CASA E JARDIM">CASA E JARDIM</a></h3></li><li><h3><a href="http://casavogue.globo.com/" title="CASA VOGUE">CASA VOGUE</a></h3></li><li></li></ul></aside></section></section><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior famosos-style"><div class="cabecalho"><h2><a title="famosos"><span class='logo'>famosos</span><span class='texto'>famosos</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://ego.globo.com/">ego</a></h3></li><li class=""><h3><a href="http://revistaquem.globo.com/">quem</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/QUEM-News/noticia/2015/09/com-juju-salimeni-thais-bianca-quase-mostra-demais-com-look-decotado.html" title="Thais Bianca posa com decotÃ£o em feira de beleza; veja as fotos"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/7BwKVgDIFKIwenx96af06GfmniY=/filters:quality(10):strip_icc()/s2.glbimg.com/Ahd0ZAzxToXxP44qk9Hf0Hwn7ms=/0x172:620x501/245x130/e.glbimg.com/og/ed/f/original/2015/09/07/thais4.jpg" alt="Thais Bianca posa com decotÃ£o em feira de beleza; veja as fotos (Thais Aline/ Ag Fio Condutor)" title="Thais Bianca posa com decotÃ£o em feira de beleza; veja as fotos (Thais Aline/ Ag Fio Condutor)"
                    data-original-image="s2.glbimg.com/Ahd0ZAzxToXxP44qk9Hf0Hwn7ms=/0x172:620x501/245x130/e.glbimg.com/og/ed/f/original/2015/09/07/thais4.jpg" data-url-smart_horizontal="ZR5PN8Y9zSPGMuBhOtb8gnXB2CA=/90x56/smart/filters:strip_icc()/" data-url-smart="ZR5PN8Y9zSPGMuBhOtb8gnXB2CA=/90x56/smart/filters:strip_icc()/" data-url-feature="ZR5PN8Y9zSPGMuBhOtb8gnXB2CA=/90x56/smart/filters:strip_icc()/" data-url-tablet="fFTB10cN3_eUvueDPrOcwgOQeyE=/160x95/smart/filters:strip_icc()/" data-url-desktop="nuKc41Gwy7EwNtLencV5-WpkAMs=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Thais Bianca posa com decotÃ£o em feira de beleza; veja as fotos</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://kogut.oglobo.globo.com/noticias-da-tv/noticia/2015/09/de-volta-ao-ar-no-caldeirao-cicarelli-fala-da-rotina-de-triatleta-e-diz-que-nao-se-incomoda-com-perguntas-sobre-seu-passado.html" title="Cicarelli volta Ã  TV em reality e diz preservar a sua vida pessoal"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/hjuYii1DV_lQiuqRNIQ2Skk-Sn0=/filters:quality(10):strip_icc()/s2.glbimg.com/0QH1dpkKUU8Aux0Mvhx18QHkx4Y=/0x89:475x341/245x130/s.glbimg.com/en/ho/f/original/2015/09/07/cicarelli.jpg" alt="Cicarelli volta Ã  TV em reality e diz preservar a sua vida pessoal (reproduÃ§Ã£o)" title="Cicarelli volta Ã  TV em reality e diz preservar a sua vida pessoal (reproduÃ§Ã£o)"
                    data-original-image="s2.glbimg.com/0QH1dpkKUU8Aux0Mvhx18QHkx4Y=/0x89:475x341/245x130/s.glbimg.com/en/ho/f/original/2015/09/07/cicarelli.jpg" data-url-smart_horizontal="0M7--EArNYAKtZFcyQ_MXxECdm8=/90x56/smart/filters:strip_icc()/" data-url-smart="0M7--EArNYAKtZFcyQ_MXxECdm8=/90x56/smart/filters:strip_icc()/" data-url-feature="0M7--EArNYAKtZFcyQ_MXxECdm8=/90x56/smart/filters:strip_icc()/" data-url-tablet="gouzowhAGbfTSJT8u-2xMrLf4ro=/160x95/smart/filters:strip_icc()/" data-url-desktop="kclJoUm6l_WW2LR3BBGxeYClhi4=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Cicarelli volta Ã  TV em reality e diz preservar a sua vida pessoal</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://ego.globo.com/famosos/noticia/2015/09/thammy-miranda-lanca-biografia-na-bienal-do-livro-no-rio.html" title="Thammy beija a namorada ao lanÃ§ar biografia na Bienal do Rio"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/mHYDN_jDaaBNzh4OY_Eq1ypkEbg=/filters:quality(10):strip_icc()/s2.glbimg.com/fDnk7ViBs1uXOcCKdd8pqXsu_Ig=/0x55:620x383/245x130/e.glbimg.com/og/ed/f/original/2015/09/07/8.jpg" alt="Thammy beija a namorada ao lanÃ§ar biografia na Bienal do Rio (GraÃ§a Paes - Photo Rio News)" title="Thammy beija a namorada ao lanÃ§ar biografia na Bienal do Rio (GraÃ§a Paes - Photo Rio News)"
                    data-original-image="s2.glbimg.com/fDnk7ViBs1uXOcCKdd8pqXsu_Ig=/0x55:620x383/245x130/e.glbimg.com/og/ed/f/original/2015/09/07/8.jpg" data-url-smart_horizontal="MPCCZbVnxuSFEM54q6ukH4arVeY=/90x56/smart/filters:strip_icc()/" data-url-smart="MPCCZbVnxuSFEM54q6ukH4arVeY=/90x56/smart/filters:strip_icc()/" data-url-feature="MPCCZbVnxuSFEM54q6ukH4arVeY=/90x56/smart/filters:strip_icc()/" data-url-tablet="vv4SZeWUd8E4wNXZ35Y2VVoyyg0=/160x95/smart/filters:strip_icc()/" data-url-desktop="0sVsb5I7W9jeYsbhqnXyD87Hdto=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Thammy beija a namorada ao lanÃ§ar biografia na Bienal do Rio</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://revistamonet.globo.com/Listas/noticia/2015/09/como-eram-e-como-estao-25-musas-adolescentes.html" title="Veja como eram e como estÃ£o hoje 25 musas adolescentes"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/aPXp0P45z5gUiyBQAlJ0mNYFQYM=/filters:quality(10):strip_icc()/s2.glbimg.com/Ri2_hj0WmRrBYnH9E3lj93zfVL4=/0x24:600x342/245x130/s.glbimg.com/en/ho/f/original/2015/09/07/25_musas_adolescentes.jpg" alt="Veja como eram e como estÃ£o hoje 25 musas adolescentes (getty images)" title="Veja como eram e como estÃ£o hoje 25 musas adolescentes (getty images)"
                    data-original-image="s2.glbimg.com/Ri2_hj0WmRrBYnH9E3lj93zfVL4=/0x24:600x342/245x130/s.glbimg.com/en/ho/f/original/2015/09/07/25_musas_adolescentes.jpg" data-url-smart_horizontal="uEvBTm-4y5x9yTZHQ8ShyPbmIZc=/90x56/smart/filters:strip_icc()/" data-url-smart="uEvBTm-4y5x9yTZHQ8ShyPbmIZc=/90x56/smart/filters:strip_icc()/" data-url-feature="uEvBTm-4y5x9yTZHQ8ShyPbmIZc=/90x56/smart/filters:strip_icc()/" data-url-tablet="WPLHYUqeM1Pv10Z2BILE2X7W-Qo=/160x95/smart/filters:strip_icc()/" data-url-desktop="PSHPC7krM3KE9pVq4BGhTTyb0C0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Veja como eram e como estÃ£o hoje 25 musas adolescentes</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_famosos-style']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-super-wide-quadruplo-com-widget analytics-product franja-inferior gshow"><div class="cabecalho"><h2><a href="http://gshow.globo.com/" title="novelas, sÃ©ries, programas e muito mais"><span class='logo'>novelas, sÃ©ries, programas e muito mais</span><span class='texto'>novelas, sÃ©ries, programas e muito mais</span></a></h2><aside class="links analytics-custom-product analytics-id-T"><ul><li class=""><h3><a href="http://gshow.globo.com/estilo-tv/">ESTILO TV</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/a-regra-do-jogo/">A REGRA DO JOGO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/i-love-paraisopolis/index.html">I LOVE PARAISÃPOLIS</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/alem-do-tempo/">ALÃM DO TEMPO</a></h3></li><li class=""><h3><a href="http://gshow.globo.com/novelas/malhacao/2015/">MALHAÃÃO</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/i-love-paraisopolis/Vem-por-ai/noticia/2015/09/chega-o-dia-do-julgamento-de-soraya-pronta-para-o-apedrejamento.html" title="Em &#39;I Love ParaisÃ³polis&#39;, Soraya se prepara para o julgamento
"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ovGKp-2z-JAjJOvaVOdH7hptGMY=/filters:quality(10):strip_icc()/s2.glbimg.com/tny8JYejhRuHmmDJri7d1qXlS-Q=/0x53:690x419/245x130/s.glbimg.com/et/gs/f/original/2015/09/04/soraya_2.jpg" alt="Em &#39;I Love ParaisÃ³polis&#39;, Soraya se prepara para o julgamento
 (TV Globo)" title="Em &#39;I Love ParaisÃ³polis&#39;, Soraya se prepara para o julgamento
 (TV Globo)"
                    data-original-image="s2.glbimg.com/tny8JYejhRuHmmDJri7d1qXlS-Q=/0x53:690x419/245x130/s.glbimg.com/et/gs/f/original/2015/09/04/soraya_2.jpg" data-url-smart_horizontal="RUe3gXlMke_yxjFrUh1_2P3NQCo=/90x56/smart/filters:strip_icc()/" data-url-smart="RUe3gXlMke_yxjFrUh1_2P3NQCo=/90x56/smart/filters:strip_icc()/" data-url-feature="RUe3gXlMke_yxjFrUh1_2P3NQCo=/90x56/smart/filters:strip_icc()/" data-url-tablet="1TONq63r4GqAq40aUtaXGYVxnBA=/160x95/smart/filters:strip_icc()/" data-url-desktop="QfH2SKexb4aJ9k9mdX-r6S0TVp0=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;I Love ParaisÃ³polis&#39;, Soraya se prepara para o julgamento<br /></p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/Bastidores/noticia/2015/09/henri-castelli-conta-habitos-saudaveis-apos-mostrar-galao-de-agua-no-encontro.html" title="Henri diz que cortes de roupas de Gabo exigem um corpo magro"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/ayZjvzQHmWpupwPtinLe2Vd3W0k=/filters:quality(10):strip_icc()/s2.glbimg.com/X9tCm-C6sPDqLDYzCfvyRTNkQWs=/0x67:690x433/245x130/s.glbimg.com/et/gs/f/original/2015/08/26/henri_castelli_agua.jpg" alt="Henri diz que cortes de roupas de Gabo exigem um corpo magro (Carolina Berger/Gshow)" title="Henri diz que cortes de roupas de Gabo exigem um corpo magro (Carolina Berger/Gshow)"
                    data-original-image="s2.glbimg.com/X9tCm-C6sPDqLDYzCfvyRTNkQWs=/0x67:690x433/245x130/s.glbimg.com/et/gs/f/original/2015/08/26/henri_castelli_agua.jpg" data-url-smart_horizontal="0k4zMBAfxyP1I_Im29NiQ5f6R28=/90x56/smart/filters:strip_icc()/" data-url-smart="0k4zMBAfxyP1I_Im29NiQ5f6R28=/90x56/smart/filters:strip_icc()/" data-url-feature="0k4zMBAfxyP1I_Im29NiQ5f6R28=/90x56/smart/filters:strip_icc()/" data-url-tablet="zX8fJfirbVqOW0Qa1W4QT6hVyOY=/160x95/smart/filters:strip_icc()/" data-url-desktop="wEBxQKhoseWRr-VZ0VLVFLtOzqU=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Henri diz que cortes de roupas de Gabo exigem um corpo magro</p></span></a></li><li class="mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/alem-do-tempo/vem-por-ai/noticia/2015/09/melissa-questiona-livia-sobre-felipe.html" title="Em &#39;AlÃ©m&#39;, Melissa se faz de sonsa como amiga de LÃ­via"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/bcM2vj9P4LaJEQ6XAR8c3rtaTDg=/filters:quality(10):strip_icc()/s2.glbimg.com/ThPcLruZ3qVFp8KCxpe-Pl9sfHw=/47x127:532x385/245x130/s.glbimg.com/et/gs/f/original/2015/09/03/melissa-se-faz-de-amiga-de-.jpg" alt="Em &#39;AlÃ©m&#39;, Melissa se faz de sonsa como amiga de LÃ­via (TV Globo)" title="Em &#39;AlÃ©m&#39;, Melissa se faz de sonsa como amiga de LÃ­via (TV Globo)"
                    data-original-image="s2.glbimg.com/ThPcLruZ3qVFp8KCxpe-Pl9sfHw=/47x127:532x385/245x130/s.glbimg.com/et/gs/f/original/2015/09/03/melissa-se-faz-de-amiga-de-.jpg" data-url-smart_horizontal="pAWLTKHFpwv1rHMzq7k331SO-lE=/90x56/smart/filters:strip_icc()/" data-url-smart="pAWLTKHFpwv1rHMzq7k331SO-lE=/90x56/smart/filters:strip_icc()/" data-url-feature="pAWLTKHFpwv1rHMzq7k331SO-lE=/90x56/smart/filters:strip_icc()/" data-url-tablet="oQiEWzvBuPxtblRxpDMBW9V_7mQ=/160x95/smart/filters:strip_icc()/" data-url-desktop="Dppf-BbhNahDyVYZAatlwcABORo=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>Em &#39;AlÃ©m&#39;, Melissa se faz de sonsa como amiga de LÃ­via</p></span></a></li><li class="ultimo mobile-grid-full"><a class="foto" href="http://gshow.globo.com/novelas/malhacao/2015/Vem-por-ai/noticia/2015/09/luan-flagra-luciana-e-rodrigo-no-maior-love-e-rola-bafao.html" title="&#39;MalhaÃ§Ã£o&#39;: Luan flagra Luciana e Rodrigo juntos e rola bafÃ£o"><span class="borda-foto"><span></span><img width="245" height="130" src="http://s2.glbimg.com/LMiYQyWbttag8qZQTfDHwX0sEvM=/filters:quality(10):strip_icc()/s2.glbimg.com/F0FLPzLM7YLY5EDO6G7UmZA-O6A=/0x80:690x446/245x130/s.glbimg.com/et/gs/f/original/2015/09/07/luan_bolado.jpg" alt="&#39;MalhaÃ§Ã£o&#39;: Luan flagra Luciana e Rodrigo juntos e rola bafÃ£o (TV Globo)" title="&#39;MalhaÃ§Ã£o&#39;: Luan flagra Luciana e Rodrigo juntos e rola bafÃ£o (TV Globo)"
                    data-original-image="s2.glbimg.com/F0FLPzLM7YLY5EDO6G7UmZA-O6A=/0x80:690x446/245x130/s.glbimg.com/et/gs/f/original/2015/09/07/luan_bolado.jpg" data-url-smart_horizontal="Fl0-212gHba9inmA0W5p3xvQN-o=/90x56/smart/filters:strip_icc()/" data-url-smart="Fl0-212gHba9inmA0W5p3xvQN-o=/90x56/smart/filters:strip_icc()/" data-url-feature="Fl0-212gHba9inmA0W5p3xvQN-o=/90x56/smart/filters:strip_icc()/" data-url-tablet="HlfJO8EtRMVT0oe4AMKnjtk8y10=/160x95/smart/filters:strip_icc()/" data-url-desktop="ELJT_fBWFfNQYuiprD3a7RtjOhc=/245x130/smart/filters:strip_icc()/"
                /></span><span class="conteudo"><p>&#39;MalhaÃ§Ã£o&#39;: Luan flagra Luciana <br />e Rodrigo juntos e rola bafÃ£o</p></span></a></li></ul><div class="separator"></div><div class="widget analytics-custom-product analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-super-wide-quadruplo-com-widget_gshow']);</script><div class="separator"></div><section class="destaque agrupador-super-wide agrupador-musica-triplo analytics-area analytics-id-U franja-inferior"><div class="cabecalho"><h2><a href="http://musica.com.br/" title="MÃSICA">
            MÃSICA
        </a></h2><aside class="links analytics-area analytics-id-T"><ul><li class="mobile-grid-full "><h3><a href="http://musica.com.br/">MÃSICA.COM.BR</a></h3></li><li class="mobile-grid-full "><h3><a href="http://g1.globo.com/musica/">G1 MÃSICA</a></h3></li><li class="mobile-grid-full "><h3><a href="http://multishow.globo.com/musica/">MULTISHOW</a></h3></li><li class="mobile-grid-full "><h3><a href="http://radiobeat.com.br/">RADIOBEAT</a></h3></li></ul></aside></div><div class="borda"><span class="rodape"></span></div><ul class="destaques"><li class="mobile-grid-full"><a class="foto" href="http://g1.globo.com/musica/rock-in-rio/2015/noticia/2015/09/rod-stewart-faz-show-com-os-faces-pela-primeira-vez-em-40-anos.html" title="Antes do Rock in Rio, Rod Stewart faz show com Faces pela primeira vez em 40 anos (ReproduÃ§Ã£o / Twitter)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/cZhljEdOuVS3UgAvXb1La97Kq2A=/filters:quality(10):strip_icc()/s2.glbimg.com/MIeJLMI0FQ83d_VSDnh1qygGbRE=/342x274:600x431/215x130/s.glbimg.com/jo/g1/f/original/2015/09/07/rod-stewart.jpg"
                data-original-image="s2.glbimg.com/MIeJLMI0FQ83d_VSDnh1qygGbRE=/342x274:600x431/215x130/s.glbimg.com/jo/g1/f/original/2015/09/07/rod-stewart.jpg" data-url-smart_horizontal="ry9mn6UjBzsv-cGU2zHNnjpBBBU=/90x56/smart/filters:strip_icc()/" data-url-smart="ry9mn6UjBzsv-cGU2zHNnjpBBBU=/90x56/smart/filters:strip_icc()/" data-url-feature="ry9mn6UjBzsv-cGU2zHNnjpBBBU=/90x56/smart/filters:strip_icc()/" data-url-tablet="S58Y9s6x_4YrxW5CwunH3WSrwkQ=/120x80/smart/filters:strip_icc()/" data-url-desktop="AqPDE1tISnjh_peYP3gD6fCttlc=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Antes do Rock in Rio, Rod Stewart faz show com Faces pela primeira vez em 40 anos</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/QUEM-News/noticia/2015/09/shakira-aparece-de-surpresa-em-show-com-look-curtinho-e-decotado.html" title="Shakira aparece de surpresa em show do ManÃ¡ com look curtinho e decotado; confira (ReproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/YC9M6xp1YtfaAAIyMv3pCQ8ot9I=/filters:quality(10):strip_icc()/s2.glbimg.com/RRxOmqmtQyLqjlcxRJlwTDpLuLE=/369x113:597x251/215x130/e.glbimg.com/og/ed/f/original/2015/09/07/shak2.jpg"
                data-original-image="s2.glbimg.com/RRxOmqmtQyLqjlcxRJlwTDpLuLE=/369x113:597x251/215x130/e.glbimg.com/og/ed/f/original/2015/09/07/shak2.jpg" data-url-smart_horizontal="fDm5PQHnSsWN548KxqEfY-yqyA4=/90x56/smart/filters:strip_icc()/" data-url-smart="fDm5PQHnSsWN548KxqEfY-yqyA4=/90x56/smart/filters:strip_icc()/" data-url-feature="fDm5PQHnSsWN548KxqEfY-yqyA4=/90x56/smart/filters:strip_icc()/" data-url-tablet="D3RYlflMxOV6xdTV1b4jtHw_gXY=/120x80/smart/filters:strip_icc()/" data-url-desktop="bU1AiBH3ftR16euI7vHT6WZyND0=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Shakira aparece de surpresa em show do ManÃ¡ com look curtinho e decotado; confira</div></div></a></li><li class="mobile-grid-full"><a class="foto" href="http://revistaquem.globo.com/Popquem/noticia/2015/09/filme-de-zombies-de-nick-carter-reune-membros-de-boyband-dos-anos-90.html" title="Astros de boys bands serÃ£o zumbis em filme dirigido por cantor do Backstreet Boys (ReproduÃ§Ã£o)"><div class="borda-foto"><span></span><img width="215" height="130" src="http://s2.glbimg.com/YKzJ9lgaCx5BhSAZOZALiABpHPU=/filters:quality(10):strip_icc()/s2.glbimg.com/Be08gjiJmQezMUpLVaYZaaVVdaE=/0x69:620x444/215x130/e.glbimg.com/og/ed/f/original/2015/09/07/nick_e_lauren.jpg"
                data-original-image="s2.glbimg.com/Be08gjiJmQezMUpLVaYZaaVVdaE=/0x69:620x444/215x130/e.glbimg.com/og/ed/f/original/2015/09/07/nick_e_lauren.jpg" data-url-smart_horizontal="6AY1mbf93ppvr9A9pCtrXkeWcsc=/90x56/smart/filters:strip_icc()/" data-url-smart="6AY1mbf93ppvr9A9pCtrXkeWcsc=/90x56/smart/filters:strip_icc()/" data-url-feature="6AY1mbf93ppvr9A9pCtrXkeWcsc=/90x56/smart/filters:strip_icc()/" data-url-tablet="_XWWgySBiZ1YzKcv9bSLoHN07DI=/120x80/smart/filters:strip_icc()/" data-url-desktop="lrOBmdmR8OM54wccVNwfcNHaD2M=/215x130/smart/filters:strip_icc()/"
            /></div><div class="conteudo"><div class="title">Astros de boys bands serÃ£o zumbis em filme dirigido por cantor do Backstreet Boys</div></div></a></li></ul><div class="top-container analytics-area analytics-id-L mobile-grid-full"><h3><a href="http://musica.com.br/">
            TOP HITS
        </a></h3><ul><li><a class="foto" href="http://musica.com.br/artistas/luan-santana/m/escreve-ai/letra.html"><div class="area-foto"><div class="number">1</div><img width="45" height="45" src="http://s2.glbimg.com/DejQr6wzwaLjEToKO0IBZYMpp9E=/filters:quality(10):strip_icc()/s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg"
                data-original-image="s2.glbimg.com/gffJ6jy3x9TdEFawxnsku-K_Igs=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/12/20/artista-65018-2013151926.jpg" data-url-smart_horizontal="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-smart="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-feature="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/" data-url-tablet="jBcm2gM8Cbg_Vkcg-0n1BAJmuKE=/60x60/smart/filters:strip_icc()/" data-url-desktop="Qq5pp_5Gc6D6kwiQv4yIWZg2DHc=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Luan Santana</div><div class="title">Escreve AÃ­</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li><a class="foto" href="http://musica.com.br/artistas/mc-anitta/m/no-meu-talento-part-mc-guime/letra.html"><div class="area-foto"><div class="number">2</div><img width="45" height="45" src="http://s2.glbimg.com/yHnJPHom5FPA3tk69JCcIphdNJE=/filters:quality(10):strip_icc()/s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg"
                data-original-image="s2.glbimg.com/i6tbAg-HNpqidGWWFJqVJ788g6o=/65x65/smart/s.glbimg.com/po/ms/f/original/2014/03/28/anitta-nova-cut.jpg" data-url-smart_horizontal="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-smart="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-feature="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/" data-url-tablet="AODuTqXeNCV61xTQZim-SUSZDBg=/60x60/smart/filters:strip_icc()/" data-url-desktop="-BjenKFGvdNpMJyVgxz1-P6zXGg=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Anitta</div><div class="title">No Meu Talento (part. MC GuimÃª)</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li><li class="last-top"><a class="foto" href="http://musica.com.br/artistas/jorge-e-mateus/m/nocaute/letra.html"><div class="area-foto"><div class="number">3</div><img width="45" height="45" src="http://s2.glbimg.com/jdC6lPjKcSpWF8v5jzFy0uuol6c=/filters:quality(10):strip_icc()/s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg"
                data-original-image="s2.glbimg.com/JgDLLARwYvvuCkqHPgmPue-pxa8=/65x65/smart/s.glbimg.com/po/ms/f/original/2013/06/26/jorge.jpg" data-url-smart_horizontal="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-smart="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-feature="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/" data-url-tablet="EfTWuRDHHuk_LklhIijQiqahj3A=/60x60/smart/filters:strip_icc()/" data-url-desktop="La985A4RX1weZTj_vT8Ud9TNFo8=/45x45/smart/filters:strip_icc()/"
            /></div><div class="top-item"><div class="name">Jorge e Mateus</div><div class="title">Nocaute</div></div><div class="listen"><div class="btn"></div>
            OUVIR
        </div></a></li></ul></div><div class="separator"></div><div class="widget analytics-area analytics-id-B"></div></section><script>
window.libby && libby.eventsArray && libby.eventsArray.push(['loaded_agrupador-musica-triplo']);</script></section><section class="area topglobocom"><div class="container area"><div class="separator"></div><div id="topglobocom3" class="clearfix destaque topglobocom3"><div class="topo"><h3 class="titulo">
            top <strong>globo</strong><span class="subtitulo">tÃ¡ todo mundo clicando...</span></h3></div><div id="mais-vistas" class="clearfix mais-vistas tab-content analytics-area analytics-id-R"><div class="slider"><ol class="noticias analytics-area analytics-id-J"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/sp/santos-regiao/noticia/2015/09/jovem-que-destruiu-carro-ao-usa-lo-como-motel-lamenta-garota-sumiu.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Jovem que destruiu carro ao usÃ¡-lo como motel lamenta: &#39;A garota sumiu&#39;</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://extra.globo.com/noticias/rio/fotos-videos-mostram-momentos-apos-acidente-com-onibus-em-paraty-17419500.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Fotos e vÃ­deos mostram momentos apÃ³s acidente com Ã´nibus em Paraty</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/mundo/noticia/2015/09/ratos-gigantes-sao-treinados-para-encontrar-explosivos-no-camboja.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ratos gigantes sÃ£o treinados para encontrar explosivos no Camboja<br /></span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/concursos-e-emprego/noticia/2015/09/120-concursos-com-inscricoes-abertas-reunem-157-mil-vagas.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">120 concursos com inscriÃ§Ãµes abertas reÃºnem 15,7 mil vagas<br /></span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://g1.globo.com/ceara/noticia/2015/09/noivos-ocupam-rua-de-fortaleza-com-tapete-vermelho-entre-igreja-e-buffet.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Sem autorizaÃ§Ã£o, noivos ocupam rua com tapete vermelho entre igreja e buffet</span></span></a></div></div></li></ol><ol class="esportes analytics-area analytics-id-E"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/blogs/especial-blog/meio-de-campo/post/video-registra-desentendimento-de-fred-no-camarote-do-maracana.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">VÃ­deo registra desentendimento de Fred no camarote do MaracanÃ£</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/noticia/2015/09/com-26-de-chances-de-g-4-fla-arranca-e-acirra-briga-por-ultima-vaga.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Com 26% de chances de G-4, Flamengo arranca e acirra briga por Ãºltima vaga</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/fluminense/noticia/2015/09/flu-agitado-segunda-pior-campanha-do-returno-deixa-nervos-flor-da-pele.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Bronca de Fred, insatisfaÃ§Ã£o, R10 no banco... derrotas pioram clima no Flu<br /></span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/sp/futebol/brasileirao-serie-a/jogo/06-09-2015/palmeiras-corinthians/" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Palmeiras e Corinthians fazem jogaÃ§o e empatam por 3 a 3 na casa alviverde</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://globoesporte.globo.com/futebol/times/flamengo/noticia/2015/09/wallace-ve-fla-consistente-e-lamenta-erro-da-arbitragem-no-toque-de-mao.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Wallace vÃª Fla consistente e lamenta erro da arbitragem no toque de mÃ£o<br /></span></span></a></div></div></li></ol><ol class="entretenimento analytics-area analytics-id-M"><li class="item-1 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/paparazzo/noticia/2015/09/cristianne-rodriguez-revela-que-vezes-dispensa-preliminares-no-sexo.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Cristianne Rodriguez revela que Ã s vezes dispensa preliminares no sexo</span></span></a></div></div></li><li class="item-2 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/09/ex-atriz-mirim-belinda-chama-atencao-em-fotos-de-biquini.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Ex-atriz mirim, Belinda chama a atenÃ§Ã£o em fotos de biquÃ­ni</span></span></a></div></div></li><li class="item-3 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://gshow.globo.com/novelas/verdades-secretas/Vem-por-ai/noticia/2015/09/carolina-descobre-segredo-de-angel.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">&#39;Verdades Secretas&#39;: Carolina descobre segredo de Angel e fica em choque</span></span></a></div></div></li><li class="item-4 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://revistaquem.globo.com/QUEM-News/noticia/2015/09/familia-reunida-cleo-pires-posa-de-biquini-acompanhada-dos-irmaos.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">FamÃ­lia reunida! Cleo Pires posa de biquÃ­ni acompanhada dos irmÃ£os</span></span></a></div></div></li><li class="item-5 clearfix"><div class="item"><div class="chamada "><span class="border"></span><a href="http://ego.globo.com/show/noticia/2015/09/prevenida-ivete-sangalo-usa-shortinho-por-baixo-de-vestido-curto-em-show.html" class="titulo entry-title"><span class="number"></span><span class="text"><span class="text-align">Prevenida, Ivete Sangalo usa shortinho por baixo de vestido curto em show<br /></span></span></a></div></div></li></ol></div></div><div class="controls-area"><ul class="step-marker"><li class=" glb-hl-style-noticias"><div class=""><a href="#" data-related=""><div class="ball active"></div></a></div></li><li class=" glb-hl-style-esporte"><div class=""><a href="#" data-related=""><div class="ball"></div></a></div></li><li class=" glb-hl-style-entretenimento"><div class=""><a href="#" data-related=""><div class="ball "></div></a></div></li></ul></div></div><section class="top-videos analytics-area analytics-id-R"><h1><a href="http://globotv.globo.com/">globo.tv</a></h1><div class="separator-line"></div><ul class="destaque analytics-area analytics-id-V"><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">1</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">2</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">3</span><span class="conteudo"><p>descr</p></span></a></li><li ><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">4</span><span class="conteudo"><p>descr</p></span></a></li><li class="ult"><a class="foto" href="#"><span class="borda-foto"><span></span><img width="190" height="110"><div class="duration"></div><div class="play-button"></div></span><span class="number">5</span><span class="conteudo"><p>descr</p></span></a></li></ul></section></div></section><section class="area globo-servicos container analytics-area analytics-id-C"><h2 class="titulo">serviÃ§os</h2><div class="borda"><span class="rodape"></span></div><div class="content"><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-loterias
" 
    data-lotteries-url="http://api.globo.com/lotteries/megasena.jsonp"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-fipe
"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"><div class="box-widget-servicos css3pie libby-widget-servicos-zap
" 
    data-zap-url="http://api.globo.com/zap/search/"><div class="content-widget-servicos"><div class="responsive-arrow"><div class="arrow"></div></div></div></div></div><div class="widget-servico"></div></div></section><section class="area widgets_inferior franja-inferior container analytics-area analytics-id-C"><div class="box-links-servicos"><div class="links"><p>MAIS SERVIÃOS</p><a href="http://revistapegn.globo.com/Franquias/compare/">Abra uma franquia</a><a href="http://globoesporte.globo.com/eu-atleta/calculo-imc.html">AvaliaÃ§Ã£o fÃ­sica</a><a href="http://g1.globo.com/economia/mercados/cotacoes/moedas/">Conversor de moedas</a><a href="http://www.techtudo.com.br/softwares/">Downloads</a><a href="http://g1.globo.com/previsao-do-tempo/">PrevisÃ£o do tempo</a><a href="http://g1.globo.com/radar-g1/platb/">TrÃ¢nsito</a></div></div><div id="glb-shopping-widget" class="area globoshopping container" data-url="http://vitrines.globo.com/vitrine/vitrine.min.js" data-sitepage="globocom/home" data-responsive="0" data-branding-color="0669DE"></div></section><section class="area diretorio"><div class="container analytics-area analytics-id-Z"><div class="diretorio-responsive"><a href="/todos-os-sites.html">veja todos os sites da <strong>globo.com</strong> âº</a></div><div id="glb-diretorio"><div class="glb-conteudo"><div class="glb-bloco"><div class="glb-grid-12"><ul class="diretorio-indice"><li class="diretorio-linha diretorio-first "><ul><li class="diretorio-first-level analytics-product glb-menu-g1 diretorio-quebra "><a title="g1" href="http://g1.globo.com/">g1</a><ul><li class="diretorio-second-level"><a title="Acre" href="http://g1.globo.com/ac/acre/">Acre</a></li><li class="diretorio-second-level"><a title="Alagoas" href="http://g1.globo.com/al/alagoas/">Alagoas</a></li><li class="diretorio-second-level"><a title="Amazonas" href="http://g1.globo.com/am/amazonas/">Amazonas</a></li><li class="diretorio-second-level"><a title="Bahia" href="http://g1.globo.com/bahia/?noAudience=true">Bahia</a></li><li class="diretorio-second-level"><a title="Bem Estar" href="http://g1.globo.com/bemestar/">Bem Estar</a></li><li class="diretorio-second-level"><a title="Bom Dia Brasil" href="http://g1.globo.com/bom-dia-brasil/">Bom Dia Brasil</a></li><li class="diretorio-second-level"><a title="Brasil" href="http://g1.globo.com/brasil/">Brasil</a></li><li class="diretorio-second-level"><a title="Carnaval" href="http://g1.globo.com/carnaval/2013/">Carnaval</a></li><li class="diretorio-second-level"><a title="Carros" href="http://g1.globo.com/carros/">Carros</a></li><li class="diretorio-second-level"><a title="CearÃ¡" href="http://g1.globo.com/ceara/">CearÃ¡</a></li><li class="diretorio-second-level"><a title="CiÃªncia e SaÃºde" href="http://g1.globo.com/ciencia-e-saude/">CiÃªncia e SaÃºde</a></li><li class="diretorio-second-level"><a title="Como SerÃ¡?" href="http://redeglobo.globo.com/como-sera/index.html">Como SerÃ¡?</a></li><li class="diretorio-second-level"><a title="Concursos e Emprego" href="http://g1.globo.com/concursos-e-emprego/">Concursos e Emprego</a></li><li class="diretorio-second-level"><a title="Distrito Federal" href="http://g1.globo.com/distrito-federal/">Distrito Federal</a></li><li class="diretorio-second-level"><a title="Economia" href="http://g1.globo.com/economia/">Economia</a></li><li class="diretorio-second-level"><a title="EducaÃ§Ã£o" href="http://www.educacao.globo.com/">EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="EspÃ­rito Santo" href="http://g1.globo.com/espirito-santo/">EspÃ­rito Santo</a></li><li class="diretorio-second-level"><a title="FantÃ¡stico" href="http://fantastico.globo.com/">FantÃ¡stico</a></li><li class="diretorio-second-level"><a title="G1 EducaÃ§Ã£o" href="http://g1.globo.com/educacao/">G1 EducaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Globo Natureza" href="http://g1.globo.com/natureza/">Globo Natureza</a></li><li class="diretorio-second-level"><a title="Globo News" href="http://globonews.globo.com/">Globo News</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://g1.globo.com/economia/agronegocios/">Globo Rural</a></li><li class="diretorio-second-level"><a title="Globo RepÃ³rter" href="http://g1.globo.com/globo-reporter/">Globo RepÃ³rter</a></li><li class="diretorio-second-level"><a title="GoiÃ¡s" href="http://g1.globo.com/goias/">GoiÃ¡s</a></li><li class="diretorio-second-level"><a title="Jornal da Globo" href="http://g1.globo.com/jornal-da-globo/">Jornal da Globo</a></li><li class="diretorio-second-level"><a title="Jornal Hoje" href="http://g1.globo.com/jornal-hoje/">Jornal Hoje</a></li><li class="diretorio-second-level"><a title="Jornal Nacional" href="http://g1.globo.com/jornal-nacional/">Jornal Nacional</a></li><li class="diretorio-second-level"><a title="MaranhÃ£o" href="http://g1.globo.com/ma/maranhao/">MaranhÃ£o</a></li><li class="diretorio-second-level"><a title="Mato Grosso" href="http://g1.globo.com/mato-grosso/">Mato Grosso</a></li><li class="diretorio-second-level"><a title="Mato Grosso do Sul" href="http://g1.globo.com/mato-grosso-do-sul/">Mato Grosso do Sul</a></li><li class="diretorio-second-level"><a title="Minas Gerais" href="http://g1.globo.com/minas-gerais/">Minas Gerais</a></li><li class="diretorio-second-level"><a title="Mundo" href="http://g1.globo.com/mundo/">Mundo</a></li><li class="diretorio-second-level"><a title="ParÃ¡" href="http://g1.globo.com/pa/para/">ParÃ¡</a></li><li class="diretorio-second-level"><a title="ParaÃ­ba" href="http://g1.globo.com/paraiba/">ParaÃ­ba</a></li><li class="diretorio-second-level"><a title="ParanÃ¡" href="http://g1.globo.com/parana/">ParanÃ¡</a></li><li class="diretorio-second-level"><a title="Pernambuco" href="http://g1.globo.com/pernambuco/">Pernambuco</a></li><li class="diretorio-second-level"><a title="PiauÃ­" href="http://g1.globo.com/pi/piaui/">PiauÃ­</a></li><li class="diretorio-second-level"><a title="Planeta Bizarro" href="http://g1.globo.com/planeta-bizarro/">Planeta Bizarro</a></li><li class="diretorio-second-level"><a title="PolÃ­tica" href="http://g1.globo.com/politica/">PolÃ­tica</a></li><li class="diretorio-second-level"><a title="Pop &amp; Arte" href="http://g1.globo.com/pop-arte/">Pop &amp; Arte</a></li><li class="diretorio-second-level"><a title="PrevisÃ£o do Tempo" href="http://g1.globo.com/previsao-do-tempo.html">PrevisÃ£o do Tempo</a></li><li class="diretorio-second-level"><a title="ProfissÃ£o RepÃ³rter" href="http://g1.globo.com/profissao-reporter/">ProfissÃ£o RepÃ³rter</a></li><li class="diretorio-second-level"><a title="Rio de Janeiro" href="http://g1.globo.com/rio-de-janeiro/">Rio de Janeiro</a></li><li class="diretorio-second-level"><a title="Rio Grande do Norte" href="http://g1.globo.com/rn/rio-grande-do-norte/">Rio Grande do Norte</a></li><li class="diretorio-second-level"><a title="Rio Grande do Sul" href="http://g1.globo.com/rs/rio-grande-do-sul/">Rio Grande do Sul</a></li><li class="diretorio-second-level"><a title="RondÃ´nia" href="http://g1.globo.com/ro/rondonia/">RondÃ´nia</a></li><li class="diretorio-second-level"><a title="Santa Catarina" href="http://g1.globo.com/sc/santa-catarina/">Santa Catarina</a></li><li class="diretorio-second-level"><a title="SÃ£o Paulo" href="http://g1.globo.com/sao-paulo/">SÃ£o Paulo</a></li><li class="diretorio-second-level"><a title="Sergipe" href="http://g1.globo.com/se/sergipe/">Sergipe</a></li><li class="diretorio-second-level"><a title="Serra, Lagos e Norte" href="http://g1.globo.com/rj/serra-lagos-norte/">Serra, Lagos e Norte</a></li><li class="diretorio-second-level"><a title="Tecnologia e Games" href="http://g1.globo.com/tecnologia/">Tecnologia e Games</a></li><li class="diretorio-second-level"><a title="Turismo e Viagem" href="http://g1.globo.com/turismo-e-viagem/">Turismo e Viagem</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-globoesporte diretorio-quebra "><a title="globoesporte" href="http://globoesporte.globo.com/">globoesporte</a><ul><li class="diretorio-second-level"><a title="Atletismo" href="http://globoesporte.globo.com/atletismo/">Atletismo</a></li><li class="diretorio-second-level"><a title="Basquete" href="http://globoesporte.globo.com/basquete/">Basquete</a></li><li class="diretorio-second-level"><a title="Basquete/NBB" href="http://globoesporte.globo.com/basquete/nbb/">Basquete/NBB</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie A" href="http://globoesporte.globo.com/futebol/brasileirao-serie-a/">BrasileirÃ£o SÃ©rie A</a></li><li class="diretorio-second-level"><a title="BrasileirÃ£o SÃ©rie B" href="http://globoesporte.globo.com/futebol/brasileirao-serie-b/">BrasileirÃ£o SÃ©rie B</a></li><li class="diretorio-second-level"><a title="Campeonato Carioca" href="http://globoesporte.globo.com/rj/futebol/campeonato-carioca/index.html">Campeonato Carioca</a></li><li class="diretorio-second-level"><a title="Campeonato GaÃºcho" href="http://globoesporte.globo.com/rs/futebol/campeonato-gaucho/index.html">Campeonato GaÃºcho</a></li><li class="diretorio-second-level"><a title="Campeonato Mineiro" href="http://globoesporte.globo.com/mg/futebol/campeonato-mineiro/index.html">Campeonato Mineiro</a></li><li class="diretorio-second-level"><a title="Campeonato Paulista" href="http://globoesporte.globo.com/sp/futebol/campeonato-paulista/index.html">Campeonato Paulista</a></li><li class="diretorio-second-level"><a title="Cartola FC" href="http://globoesporte.globo.com/cartola-fc/">Cartola FC</a></li><li class="diretorio-second-level"><a title="Copa do Mundo da Fifa" href="http://globoesporte.globo.com/futebol/copa-do-mundo/index.html">Copa do Mundo da Fifa</a></li><li class="diretorio-second-level"><a title="Copa AmÃ©rica" href="http://globoesporte.globo.com/futebol/copa-america/">Copa AmÃ©rica</a></li><li class="diretorio-second-level"><a title="Copa Sul-Americana" href="http://globoesporte.globo.com/futebol/copa-sul-americana/index.html">Copa Sul-Americana</a></li><li class="diretorio-second-level"><a title="Desafio de Futsal" href="http://globoesporte.globo.com/eventos/desafio-de-futsal/">Desafio de Futsal</a></li><li class="diretorio-second-level"><a title="Esporte Espetacular" href="http://globoesporte.globo.com/programas/esporte-espetacular/">Esporte Espetacular</a></li><li class="diretorio-second-level"><a title="Esportes AquÃ¡ticos" href="http://globoesporte.globo.com/aquaticos/">Esportes AquÃ¡ticos</a></li><li class="diretorio-second-level"><a title="Esportes Radicais" href="http://globoesporte.globo.com/radicais/">Esportes Radicais</a></li><li class="diretorio-second-level"><a title="Eu Atleta" href="http://globoesporte.globo.com/eu-atleta/">Eu Atleta</a></li><li class="diretorio-second-level"><a title="FÃ³rmula 1" href="http://globoesporte.globo.com/motor/formula-1/">FÃ³rmula 1</a></li><li class="diretorio-second-level"><a title="Futebol" href="http://globoesporte.globo.com/futebol/">Futebol</a></li><li class="diretorio-second-level"><a title="Futebol Internacional" href="http://globoesporte.globo.com/futebol/futebol-internacional/">Futebol Internacional</a></li><li class="diretorio-second-level"><a title="FutpÃ©dia" href="http://futpedia.globo.com/">FutpÃ©dia</a></li><li class="diretorio-second-level"><a title="Game Futebol" href="http://gamefutebol.globoesporte.globo.com/">Game Futebol</a></li><li class="diretorio-second-level"><a title="Liga dos CampeÃµes" href="http://globoesporte.globo.com/futebol/liga-dos-campeoes/">Liga dos CampeÃµes</a></li><li class="diretorio-second-level"><a title="OlimpÃ­adas" href="http://globoesporte.globo.com/olimpiadas/">OlimpÃ­adas</a></li><li class="diretorio-second-level"><a title="SeleÃ§Ã£o Brasileira" href="http://globoesporte.globo.com/futebol/selecao-brasileira/">SeleÃ§Ã£o Brasileira</a></li><li class="diretorio-second-level"><a title="StockCar" href="http://globoesporte.globo.com/motor/stock-car/">StockCar</a></li><li class="diretorio-second-level"><a title="Surfe" href="http://globoesporte.globo.com/radicais/surfe/">Surfe</a></li><li class="diretorio-second-level"><a title="TÃªnis" href="http://globoesporte.globo.com/tenis/">TÃªnis</a></li><li class="diretorio-second-level"><a title="Vai-e-vem do Mercado" href="http://globoesporte.globo.com/futebol/vai-e-vem-do-mercado/">Vai-e-vem do Mercado</a></li><li class="diretorio-second-level"><a title="VerÃ£o Espetacular" href="http://globoesporte.globo.com/programas/verao-espetacular/">VerÃ£o Espetacular</a></li><li class="diretorio-second-level"><a title="VÃ´lei" href="http://globoesporte.globo.com/volei/">VÃ´lei</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-gshow diretorio-quebra "><a title="gshow" href="http://gshow.globo.com/">gshow</a><ul><li class="diretorio-second-level"><a title="Amorteamo" href="http://gshow.globo.com/programas/amorteamo/">Amorteamo</a></li><li class="diretorio-second-level"><a title="A Teia" href="http://gshow.globo.com/programas/a-teia/">A Teia</a></li><li class="diretorio-second-level"><a title="Altas Horas" href="http://altashoras.globo.com/">Altas Horas</a></li><li class="diretorio-second-level"><a title="Alto Astral" href="http://gshow.globo.com/novelas/alto-astral/index.html">Alto Astral</a></li><li class="diretorio-second-level"><a title="Amor &amp; Sexo" href="http://amoresexo.globo.com/">Amor &amp; Sexo</a></li><li class="diretorio-second-level"><a title="BabilÃ´nia" href="http://gshow.globo.com/novelas/babilonia/index.html">BabilÃ´nia</a></li><li class="diretorio-second-level"><a title="BBB" href="http://bbb.globo.com/">BBB</a></li><li class="diretorio-second-level"><a title="Boogie Oogie" href="http://gshow.globo.com/novelas/boogie-oogie/index.html">Boogie Oogie</a></li><li class="diretorio-second-level"><a title="CaldeirÃ£o do Huck" href="http://caldeiraodohuck.globo.com/">CaldeirÃ£o do Huck</a></li><li class="diretorio-second-level"><a title="Cobras &amp; Lagartos" href="http://gshow.globo.com/novelas/cobras-e-lagartos/videos">Cobras &amp; Lagartos</a></li><li class="diretorio-second-level"><a title="DomingÃ£o do FaustÃ£o" href="http://domingaodofaustao.globo.com/">DomingÃ£o do FaustÃ£o</a></li><li class="diretorio-second-level"><a title="Dupla Identidade" href="http://gshow.globo.com/programas/dupla-identidade/index.html">Dupla Identidade</a></li><li class="diretorio-second-level"><a title="Encontro com FÃ¡tima Bernardes" href="http://tvg.globo.com/programas/encontro-com-fatima-bernardes/">Encontro com FÃ¡tima Bernardes</a></li><li class="diretorio-second-level"><a title="Esquenta!" href="http://tvg.globo.com/programas/esquenta/">Esquenta!</a></li><li class="diretorio-second-level"><a title="Estrelas" href="http://estrelas.globo.com/">Estrelas</a></li><li class="diretorio-second-level"><a title="Felizes para sempre?" href="http://gshow.globo.com/programas/felizes-para-sempre/index.html">Felizes para sempre?</a></li><li class="diretorio-second-level"><a title="ImpÃ©rio" href="http://gshow.globo.com/novelas/imperio/index.html">ImpÃ©rio</a></li><li class="diretorio-second-level"><a title="Mais VocÃª" href="http://maisvoce.globo.com/">Mais VocÃª</a></li><li class="diretorio-second-level"><a title="MalhaÃ§Ã£o" href="http://malhacao.globo.com/">MalhaÃ§Ã£o</a></li><li class="diretorio-second-level"><a title="Na Moral" href="http://gshow.globo.com/programas/na-moral/">Na Moral</a></li><li class="diretorio-second-level"><a title="O Rei do Gado" href="http://gshow.globo.com/novelas/o-rei-do-gado/videos/">O Rei do Gado</a></li><li class="diretorio-second-level"><a title="PÃ© na Cova" href="http://tvg.globo.com/programas/pe-na-cova/index.html">PÃ© na Cova</a></li><li class="diretorio-second-level"><a title="Programa do JÃ´" href="http://programadojo.globo.com/">Programa do JÃ´</a></li><li class="diretorio-second-level"><a title="Receitas.com" href="http://www.receitas.com/">Receitas.com</a></li><li class="diretorio-second-level"><a title="Receitas Mais VocÃª" href="http://www.receitas.com/maisvoce/">Receitas Mais VocÃª</a></li><li class="diretorio-second-level"><a title="Sete Vidas" href="http://gshow.globo.com/novelas/sete-vidas/index.html">Sete Vidas</a></li><li class="diretorio-second-level"><a title="SuperStar" href="http://gshow.globo.com/programas/superstar/index.html">SuperStar</a></li><li class="diretorio-second-level"><a title="TÃ¡ no Ar" href="http://gshow.globo.com/programas/ta-no-ar-a-tv-na-tv/index.html">TÃ¡ no Ar</a></li><li class="diretorio-second-level"><a title="Tapas &amp; Beijos" href="http://tapasebeijos.globo.com/">Tapas &amp; Beijos</a></li><li class="diretorio-second-level"><a title="Sexo e as Negas" href="http://gshow.globo.com/programas/sexo-e-as-negas/index.html">Sexo e as Negas</a></li><li class="diretorio-second-level"><a title="The Voice Brasil" href="http://tvg.globo.com/programas/the-voice-brasil/">The Voice Brasil</a></li><li class="diretorio-second-level"><a title="The Voice Kids" href="http://gshow.globo.com/programas/the-voice-kids/">The Voice Kids</a></li><li class="diretorio-second-level"><a title="Verdades Secretas" href="http://gshow.globo.com/novelas/verdades-secretas/index.html">Verdades Secretas</a></li><li class="diretorio-second-level"><a title="Video Show" href="http://videoshow.globo.com/">Video Show</a></li><li class="diretorio-second-level"><a title="WebsÃ©ries " href="http://gshow.globo.com/programas/webseries/videos/">WebsÃ©ries </a></li><li class="diretorio-second-level"><a title="ZORRA" href="http://gshow.globo.com/programas/zorra/">ZORRA</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-famosos-etc diretorio-quebra "><a title="famosos &amp; etc" href="http://famosos.globo.com/">famosos &amp; etc</a><ul><li class="diretorio-second-level"><a title="EGO" href="http://ego.globo.com/">EGO</a></li><li class="diretorio-second-level"><a title="G1 Cinema" href="http://g1.globo.com/pop-arte/cinema/">G1 Cinema</a></li><li class="diretorio-second-level"><a title="G1 MÃºsica" href="http://g1.globo.com/pop-arte/musica/">G1 MÃºsica</a></li><li class="diretorio-second-level"><a title="Globo Jogos" href="http://jogos.globo.com/">Globo Jogos</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="HorÃ³scopo" href="http://horoscopo.ego.globo.com/ne/home.html">HorÃ³scopo</a></li><li class="diretorio-second-level"><a title="Meus 5 minutos" href="http://www.meus5minutos.com.br/">Meus 5 minutos</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="MÃºsica" href="http://musica.com.br/">MÃºsica</a></li><li class="diretorio-second-level"><a title="Namoro na Web" href="http://tracking.parperfeito.com.br/ppbanner/bannerTracker?originId=13&amp;identifierId=41763&amp;actionId=1">Namoro na Web</a></li><li class="diretorio-second-level"><a title="Paparazzo" href="http://paparazzo.globo.com/">Paparazzo</a></li><li class="diretorio-second-level"><a title="Patricia Kogut" href="http://oglobo.globo.com/cultura/kogut/">Patricia Kogut</a></li><li class="diretorio-second-level"><a title="Retratos da Vida" href="http://extra.globo.com/lazer/retratosdavida/">Retratos da Vida</a></li><li class="diretorio-second-level"><a title="Senninha" href="http://senninha.globo.com/">Senninha</a></li><li class="diretorio-second-level"><a title="Teatro" href="http://www.agentesevenoteatro.com.br/">Teatro</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-middle "><ul><li class="diretorio-first-level analytics-product glb-menu-grupo-globo diretorio-quebra "><a title="Grupo Globo" href="http://grupoglobo.globo.com/">Grupo Globo</a><ul><li class="diretorio-second-level"><a title="princÃ­pios editoriais" href="http://g1.globo.com/principios-editoriais-do-grupo-globo.html">princÃ­pios editoriais</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-editora-globo diretorio-sem-quebra "><a title="editora globo" href="http://editoraglobo.globo.com/">editora globo</a><ul><li class="diretorio-second-level"><a title="Auto Esporte" href="http://revistaautoesporte.globo.com/">Auto Esporte</a></li><li class="diretorio-second-level"><a title="Casa e Comida" href="http://revistacasaejardim.globo.com/Casa-e-Comida/">Casa e Comida</a></li><li class="diretorio-second-level"><a title="Casa e Jardim" href="http://revistacasaejardim.globo.com/">Casa e Jardim</a></li><li class="diretorio-second-level"><a title="Casa Vogue" href="http://casavogue.globo.com/">Casa Vogue</a></li><li class="diretorio-second-level"><a title="Crescer" href="http://revistacrescer.globo.com/">Crescer</a></li><li class="diretorio-second-level"><a title="Ãpoca" href="http://revistaepoca.globo.com/">Ãpoca</a></li><li class="diretorio-second-level"><a title="Ãpoca NegÃ³cios" href="http://epocanegocios.globo.com/">Ãpoca NegÃ³cios</a></li><li class="diretorio-second-level"><a title="Galileu" href="http://revistagalileu.globo.com/">Galileu</a></li><li class="diretorio-second-level"><a title="Glamour" href="http://revistaglamour.globo.com/">Glamour</a></li><li class="diretorio-second-level"><a title="Globo Rural" href="http://revistagloborural.globo.com/">Globo Rural</a></li><li class="diretorio-second-level"><a title="GQ Brasil" href="http://gq.globo.com/">GQ Brasil</a></li><li class="diretorio-second-level"><a title="Marie Claire" href="http://revistamarieclaire.globo.com/">Marie Claire</a></li><li class="diretorio-second-level"><a title="Meus 5 Minutos" href="http://meus5minutos.globo.com/">Meus 5 Minutos</a></li><li class="diretorio-second-level"><a title="Monet" href="http://revistamonet.globo.com/">Monet</a></li><li class="diretorio-second-level"><a title="Mundo do SÃ­tio" href="http://mundodositio.globo.com/">Mundo do SÃ­tio</a></li><li class="diretorio-second-level"><a title="PEGN" href="http://revistapegn.globo.com/">PEGN</a></li><li class="diretorio-second-level"><a title="Quem" href="http://revistaquem.globo.com/">Quem</a></li><li class="diretorio-second-level"><a title="Vogue" href="http://vogue.globo.com/">Vogue</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-infoglobo diretorio-sem-quebra "><a title="infoglobo" href="http://infoglobo.com.br/Anuncie/Home.aspx">infoglobo</a><ul><li class="diretorio-second-level"><a title="Ela Digital" href="http://ela.oglobo.globo.com/">Ela Digital</a></li><li class="diretorio-second-level"><a title="Extra" href="http://extra.globo.com/">Extra</a></li><li class="diretorio-second-level"><a title="O Globo" href="http://oglobo.globo.com/">O Globo</a></li></ul></li></ul></li><li class="diretorio-linha diretorio-last "><ul><li class="diretorio-first-level analytics-product glb-menu-globosat diretorio-quebra "><a title="globosat" href="http://canaisglobosat.globo.com/">globosat</a><ul><li class="diretorio-second-level"><a title="Globosat Play" href="http://globosatplay.globo.com/">Globosat Play</a></li><li class="diretorio-second-level"><a title="+Globosat" href="http://maisglobosat.globo.com/">+Globosat</a></li><li class="diretorio-second-level"><a title="BIS" href="http://canalbis.globo.com/">BIS</a></li><li class="diretorio-second-level"><a title="Canal Brasil" href="http://canalbrasil.globo.com/">Canal Brasil</a></li><li class="diretorio-second-level"><a title="Canal Futura" href="http://www.futura.org.br/">Canal Futura</a></li><li class="diretorio-second-level"><a title="Combate" href="http://combate.globo.com/">Combate</a></li><li class="diretorio-second-level"><a title="GloboNews" href="http://globonews.globo.com/">GloboNews</a></li><li class="diretorio-second-level"><a title="Gloob" href="http://mundogloob.globo.com/">Gloob</a></li><li class="diretorio-second-level"><a title="GNT" href="http://gnt.globo.com/">GNT</a></li><li class="diretorio-second-level"><a title="Megapix" href="http://megapix.globo.com/">Megapix</a></li><li class="diretorio-second-level"><a title="Multishow" href="http://multishow.globo.com/">Multishow</a></li><li class="diretorio-second-level"><a title="Off" href="http://canaloff.globo.com/">Off</a></li><li class="diretorio-second-level"><a title="Premiere" href="http://sociopremiere.globo.com/">Premiere</a></li><li class="diretorio-second-level"><a title="SporTV" href="http://sportv.globo.com/">SporTV</a></li><li class="diretorio-second-level"><a title="Studio Universal" href="http://studiouniversal.globo.com/">Studio Universal</a></li><li class="diretorio-second-level"><a title="Syfy" href="http://syfy.globo.com/">Syfy</a></li><li class="diretorio-second-level"><a title="Telecine" href="http://telecine.globo.com/">Telecine</a></li><li class="diretorio-second-level"><a title="Universal Channel" href="http://uc.globo.com/">Universal Channel</a></li><li class="diretorio-second-level"><a title="Viva" href="http://canalviva.globo.com/">Viva</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-sistema-globo-de-radio diretorio-sem-quebra "><a title="sistema globo de rÃ¡dio" href="http://globoradio.globo.com/home/HOME.htm">sistema globo de rÃ¡dio</a><ul><li class="diretorio-second-level"><a title="BHFM" href="http://bhfm.globoradio.globo.com/home/HOME.htm">BHFM</a></li><li class="diretorio-second-level"><a title="CBN" href="http://cbn.globoradio.globo.com/home/HOME.htm">CBN</a></li><li class="diretorio-second-level"><a title="RADIOBEAT" href="http://radiobeat.com.br/">RADIOBEAT</a></li><li class="diretorio-second-level"><a title="RÃ¡dio Globo" href="http://radioglobo.globoradio.globo.com/home/HOME.htm">RÃ¡dio Globo</a></li></ul></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-globo-filmes diretorio-sem-quebra "><a title="globo filmes" href="http://globofilmes.globo.com/">globo filmes</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-som-livre diretorio-sem-quebra "><a title="som livre" href="http://www.somlivre.com/">som livre</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-blog-globocom diretorio-sem-quebra "><a title="blog globo.com" href="http://blog.globo.com/1.html">blog globo.com</a></li></ul><ul><li class="diretorio-first-level analytics-product glb-menu-zap-imoveis diretorio-sem-quebra "><a title="zap imÃ³veis" href="http://www.zapimoveis.com.br/">zap imÃ³veis</a></li></ul></li></ul></div></div></div></div></div></section><div id="x61" class="opec-area opec-mobile opec-x61 grid-12"><div id="banner_mobile_fim" class="tag-manager-publicidade-container"></div></div><footer><div class="container analytics-area analytics-id-B"><a href="http://www.globo.com/" title="Globo.com">Globo.com</a><span class="copyright">&copy; Copyright 2000-2015 Globo ComunicaÃ§Ã£o e ParticipaÃ§Ãµes S.A.</span><span class="copyright-480 hidden"><p>&copy; 2000-2015.</p><p>Todos os direitos reservados.</p></span><ul class="footer-menu"><li><a href="/privacidade.html">polÃ­tica de privacidade</a></li><li><a href="/todos-os-sites.html">todos os sites</a></li><li><a href="http://anuncie.globo.com/">anuncie conosco</a></li><li><a href="http://meuperfil.globo.com">central globo.com</a></li></ul></div></footer></div><!--[if lt IE 9]><script src="http://s.glbimg.com/en/ho/static/libby/css3pie/js/PIE.js" type="text/javascript"></script><![endif]--><script>
responsiveHub.updateImages();</script><script>
(function(){var scr=document.createElement('script');scr.type = 'text/javascript';scr.async=true;scr.src = 'http://s.glbimg.com/en/ho/static/CACHE/js/b43da6b3453a.js';document.getElementsByTagName('head')[0].appendChild(scr);}());</script><div id="opec-frame1"><div id="banner_floating" class="tag-manager-publicidade-container"></div></div><style> @media (max-width:959px){#banner_slb_fim>div>iframe,#banner_slb_meio>div>iframe{-ms-zoom:.72;-moz-transform:scale(.72);-moz-transform-origin:0 0;-o-transform:scale(.72);-o-transform-origin:0 0;-webkit-transform:scale(.72);-webkit-transform-origin:0 0;vertical-align:middle;display:inline-block}#banner_slb_fim>div,#banner_slb_meio>div{background-color:white;overflow:hidden}#banner_slb_fim,#banner_slb_meio{overflow:visible}}.without-opec{display:block !important}.opec-x60{height:auto}</style><script>
window.glb=window.glb || {};window.glb.analyticsConfig=window.glb.analyticsConfig  || {};window.glb.analyticsConfig = {scrollEventChance: 10};</script><script>
document.cookie = 'globoapi.geo.geolocation=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.estado=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.novela=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';document.cookie = 'p.time=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/; domain=globo.com';setTimeout(function(){window.glb.barra=window.glb.barra || {};window.glb.barra.component=window.glb.barra.component || {};window.glb.barra.component.barraDiv = {className: ''};}, 1000);</script><style>
#assinante-menu .barra-notifications-area .float-box.config-active .save-area{bottom:0;position:absolute;width:100%}#assinante-menu .barra-notifications-area .float-box.config-active .config-box{padding-bottom:44px}</style><style>
header .container{height:150px}.box-destaque-uber .destaques-primario .titulo{font-family:"ProximaNovaA-Bold"}@media (min-width:1600px){.conteudo-uber .box-destaque-uber{position:static;max-width:1600px;margin:0 auto}}</style><style>
.agrupador-quadruplo-tecnologia .destaque-inferior ul{display:none;// css de folder}</style></body></html>
<!-- PÃ¡gina gerada em 07/09/2015 21:21:43 -->
