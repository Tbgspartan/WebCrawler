<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Search and discuss new and favorite TV shows & TV series, movies, music and games."/>
    <title>KAT - Kickass Torrents</title>
    <link rel="stylesheet" type="text/css" href="//kastatic.com/all-46aa35a.css" charset="utf-8" />
    <link rel="shortcut icon" href="//kastatic.com/images/favicon.ico" />
    

    <link rel="apple-touch-icon" href="//kastatic.com/images/apple-touch-icon.png" />

    <!--[if IE 7]>
    <link href="//kastatic.com/css/ie7-46aa35a.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if IE 8]>
    <link href="//kastatic.com/css/ie8.css" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//kastatic.com/js/html5.min-46aa35a.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if gte IE 9]>
    <link href="//kastatic.com/css/ie9-46aa35a.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script type="text/javascript">
        +function(S,p,a,r,e,C,l,i,c,k)
        { S[r]=S[r]||[];S[e]||(S[e]=function(){
        S[r].push(Array.prototype.slice.call(arguments)) });
        i=p.createElement(a);c=p.getElementsByTagName(a)[0];
        i.src=C;i.async=true;c.parentNode.insertBefore(i,c)}
        (window,document,'script','_scq','sc', '//a.kat.cr/sc-46aa35a.js');

        sc('setHost', 'a.kat.cr');
        sc('setAccount', '_b894d6cb1e370fb9ad89f8d6d99eeb33');
            var kat = {
            release_id: '46aa35a',
            detect_lang: 0,
            spare_click: 1,
            mobile: false
        };
    </script>
    <script src="//kastatic.com/js/all-46aa35a.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="Subscribe to RSS feed" href="/?rss=1"/>
        <meta name="verify-v1" content="YccN/iP28SifHNEFY6u92i0ou3tAegQAIk2OyOJLp1s="/>
    <meta name="y_key" content="f0b40c3f5fee758f"/>
    <meta name="google-site-verification" content="C1rNEC4fJIvFoyyccMV2PbuqX3P-SFtlD2MNZ9D2uy0" />
    <link rel="search" type="application/opensearchdescription+xml" title="KickassTorrents Torrent Search" href="/opensearch.xml"/>
    <meta property="fb:app_id" content="123694587642603"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <script type="text/javascript">var _scq = _scq || [];</script></head>
<body>
<div id="wrapper">
    <div id="wrapperInner">
<div id="_60318cd4e8d28f6fb76fe34e9bd9c498"></div>
<div id="_39ecb76dd457e5ac33776fdf11500d56"></div>
    <div id="logindiv"></div>
    <header>
	<nav id="menu">
		<a href="/" id="logo"></a>
		<i id="showHideSearch" class="ka ka-zoom"></i>
		<div id="torrentSearch">
			<form action="/usearch/" method="get" id="searchform" accept-charset="utf-8" onsubmit="return doSearch(this.q.value);">
				<input id="contentSearch" class="input-big" type="text" name="q" value="" autocomplete="off" placeholder="Search query" /><div id="searchTool"><a title="Advanced search" href="/advanced/" class="ajaxLink"><i class="ka">e</i></a><button title="search" type="submit" value="" onfocus="this.blur();" onclick="this.blur();"><i class="ka">g</i></button></div>
			</form>
		</div>
        <div id="_277923e5f9d753c5b0630c28e641790c"></div>
		<ul id="navigation">
			
			<li> <a href="/browse/"> <i class="ka ka-torrent"></i><span class="menuItem">browse</span></a>
				<ul class="dropdown dp-middle dropdown-msg upper">
										
						<li class="topMsg"><a href="/new/"><i class="ka ka16 ka-torrent"></i>latest</a></li>
										<li class="topMsg"><a href="/movies/"><i class="ka ka16 ka-movie lower"></i>Movies</a></li>
					<li class="topMsg"><a href="/tv/"><i class="ka ka16 ka-movie lower"></i>TV</a></li>
					<li class="topMsg"><a href="/music/"><i class="ka ka16 ka-music-note lower"></i>Music</a></li>
					<li class="topMsg"><a href="/games/"><i class="ka ka16 ka-settings lower"></i>Games</a></li>
					<li class="topMsg"><a href="/books/"><i class="ka ka16 ka-bookmark"></i>Books</a></li>
					<li class="topMsg"><a href="/applications/"><i class="ka ka16 ka-settings lower"></i>Apps</a></li>
					<li class="topMsg"><a href="/anime/"><i class="ka ka16 ka-movie lower"></i>Anime</a></li>
					<li class="topMsg"><a href="/other/"><i class="ka ka16 ka-torrent"></i>Other</a></li>
											<li class="topMsg"><a href="/xxx/"><i class="ka ka16 ka-delete"></i>XXX</a></li>
									</ul>
			</li>
			</li>
			<li><a data-nop href="/community/"> <i class="ka ka-community"></i><span class="menuItem">community</span></a>
			<li><a data-nop href="/blog/"><i class="ka ka-rss lower"></i><span class="menuItem">Blog</span></a></li>
			<li><a data-nop href="/faq/"><i class="ka ka-faq lower"></i><span class="menuItem">FAQ</span></a></li>
			</li>
			
			<li> <a data-nop href="/auth/login/" class="ajaxLink"><i class="ka ka-user"></i><span class="menuItem">Register / Sign In</span></a></li>
		</ul>
	</nav>
</header>

<div class="pusher"></div>
<div id="tagcloud" class="tagcloud">
	<a href="/search/1080p/" class="tag7">1080p</a>
	<a href="/search/2014/" class="tag3">2014</a>
	<a href="/search/2015/" class="tag10">2015</a>
	<a href="/search/3d/" class="tag3">3d</a>
	<a href="/search/3d%20remux/" class="tag2">3d remux</a>
	<a href="/search/android/" class="tag2">android</a>
	<a href="/search/ant%20man/" class="tag2">ant man</a>
	<a href="/search/batman/" class="tag2">batman</a>
	<a href="/search/breaking%20bad/" class="tag2">breaking bad</a>
	<a href="/search/discography/" class="tag4">discography</a>
	<a href="/search/dragon%20ball%20super/" class="tag1">dragon ball super</a>
	<a href="/search/fear%20the%20walking%20dead/" class="tag2">fear the walking dead</a>
	<a href="/search/french/" class="tag4">french</a>
	<a href="/search/game%20of%20thrones/" class="tag2">game of thrones</a>
	<a href="/search/hand%20of%20god/" class="tag2">hand of god</a>
	<a href="/search/hannibal/" class="tag2">hannibal</a>
	<a href="/search/hindi/" class="tag10">hindi</a>
	<a href="/search/hindi%202015/" class="tag5">hindi 2015</a>
	<a href="/search/inside%20out/" class="tag3">inside out</a>
	<a href="/search/ita/" class="tag2">ita</a>
	<a href="/search/mad%20max/" class="tag5">mad max</a>
	<a href="/search/malayalam/" class="tag3">malayalam</a>
	<a href="/search/metal%20gear%20solid/" class="tag2">metal gear solid</a>
	<a href="/search/metal%20gear%20solid%20v/" class="tag2">metal gear solid v</a>
	<a href="/search/minions/" class="tag3">minions</a>
	<a href="/search/movies/" class="tag2">movies</a>
	<a href="/search/mr%20robot/" class="tag3">mr robot</a>
	<a href="/search/narcos/" class="tag2">narcos</a>
	<a href="/search/nl/" class="tag2">nl</a>
	<a href="/search/pixels/" class="tag2">pixels</a>
	<a href="/search/ripsalot/" class="tag5">ripsalot</a>
	<a href="/search/san%20andreas/" class="tag2">san andreas</a>
	<a href="/search/southpaw/" class="tag2">southpaw</a>
	<a href="/search/star%20wars/" class="tag2">star wars</a>
	<a href="/search/straight%20outta%20compton/" class="tag2">straight outta compton</a>
	<a href="/search/tamil/" class="tag4">tamil</a>
	<a href="/search/telugu/" class="tag3">telugu</a>
	<a href="/search/telugu%202015/" class="tag3">telugu 2015</a>
	<a href="/search/the%20walking%20dead/" class="tag2">the walking dead</a>
	<a href="/search/trilli/" class="tag3">trilli</a>
	<a href="/search/ufc/" class="tag3">ufc</a>
	<a href="/search/ufc%20191/" class="tag4">ufc 191</a>
	<a href="/search/under%20the%20dome/" class="tag2">under the dome</a>
	<a href="/search/welcome%20back/" class="tag2">welcome back</a>
	<a href="/search/windows%2010/" class="tag2">windows 10</a>
	<a href="/search/yify/" class="tag10">yify</a>
	<a href="/search/yify%201080p/" class="tag5">yify 1080p</a>
	<a href="/search/yify%20720p/" class="tag6">yify 720p</a>
</div>
<a class="line50perc showmore botmarg0" onClick="toggleTags(this);" title="hide tagcloud"><span class="font80perc">&#x25B2;</span></a>
<div class="mainpart">
    
<table width="100%" cellspacing="0" cellpadding="0" class="doublecelltable">
	<tr>
		<td width="100%">
			<h2><a class="plain" href="/movies/">Movies Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/movies/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11199622,0" class="icommentjs icon16" href="/hitman-agent-47-2015-hdcam-shq-mic-xvid-ac3-hq-hive-cm8-t11199622.html#comment"> <em class="iconvalue">133</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hitman-agent-47-2015-hdcam-shq-mic-xvid-ac3-hq-hive-cm8-t11199622.html" class="cellMainLink">Hitman-Agent-47 2015 HDCAM SHQ-MiC XVID AC3 HQ Hive-CM8</a></div>
			</td>
			<td class="nobr center" data-sort="1433023939">1.33 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Sep 2015, 03:30">3&nbsp;days</span></td>
			<td class="green center">4492</td>
			<td class="red lasttd center">3483</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11202533,0" class="icommentjs icon16" href="/tale-of-tales-2015-1080p-brrip-x264-dts-jyk-t11202533.html#comment"> <em class="iconvalue">39</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/tale-of-tales-2015-1080p-brrip-x264-dts-jyk-t11202533.html" class="cellMainLink">Tale of Tales 2015 1080p BRRip x264 DTS-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="3619539196">3.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Sep 2015, 16:55">3&nbsp;days</span></td>
			<td class="green center">4555</td>
			<td class="red lasttd center">3218</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11201825,0" class="icommentjs icon16" href="/dirty-weekend-2015-hdrip-xvid-ac3-evo-t11201825.html#comment"> <em class="iconvalue">24</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dirty-weekend-2015-hdrip-xvid-ac3-evo-t11201825.html" class="cellMainLink">Dirty Weekend 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1512798802">1.41 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Sep 2015, 13:11">3&nbsp;days</span></td>
			<td class="green center">4177</td>
			<td class="red lasttd center">3328</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11206944,0" class="icommentjs icon16" href="/attack-on-titan-2015-720p-hdrip-x264-aac-jyk-t11206944.html#comment"> <em class="iconvalue">67</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/attack-on-titan-2015-720p-hdrip-x264-aac-jyk-t11206944.html" class="cellMainLink">Attack on Titan 2015 720p HDRip x264 AAC-JYK</a></div>
			</td>
			<td class="nobr center" data-sort="2186553245">2.04 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="05 Sep 2015, 13:28">2&nbsp;days</span></td>
			<td class="green center">3938</td>
			<td class="red lasttd center">2999</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11202242,0" class="icommentjs icon16" href="/poltergeist-2015-extended-hdrip-xvid-etrg-t11202242.html#comment"> <em class="iconvalue">57</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/poltergeist-2015-extended-hdrip-xvid-etrg-t11202242.html" class="cellMainLink">Poltergeist 2015 EXTENDED HDRip XviD-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="740190460">705.9 <span>MB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="04 Sep 2015, 15:12">3&nbsp;days</span></td>
			<td class="green center">3840</td>
			<td class="red lasttd center">2424</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11209278,0" class="icommentjs icon16" href="/american-ultra-2015-cam-aac-ssm-mp4-incl-sample-t11209278.html#comment"> <em class="iconvalue">45</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/american-ultra-2015-cam-aac-ssm-mp4-incl-sample-t11209278.html" class="cellMainLink">American Ultra (2015) CAM AAC-SSM.mp4 {Incl Sample}</a></div>
			</td>
			<td class="nobr center" data-sort="277478159">264.62 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 00:12">1&nbsp;day</span></td>
			<td class="green center">3478</td>
			<td class="red lasttd center">1705</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11201946,0" class="icommentjs icon16" href="/welcome-back-2015-desiscr-rip-xvid-1-3-team-ictv-exclusive-sparrow-t11201946.html#comment"> <em class="iconvalue">78</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/welcome-back-2015-desiscr-rip-xvid-1-3-team-ictv-exclusive-sparrow-t11201946.html" class="cellMainLink">Welcome Back (2015) DesiSCR Rip - XviD - [1-3] - Team IcTv Exclusive -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1583002364">1.47 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="04 Sep 2015, 13:52">3&nbsp;days</span></td>
			<td class="green center">1846</td>
			<td class="red lasttd center">2673</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11211367,0" class="icommentjs icon16" href="/no-escape-2015-cam-aac-ssm-mp4-incl-sample-t11211367.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/no-escape-2015-cam-aac-ssm-mp4-incl-sample-t11211367.html" class="cellMainLink">No Escape (2015) CAM AAC-SSM.mp4 {Incl Sample}</a></div>
			</td>
			<td class="nobr center" data-sort="227385919">216.85 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 10:29">1&nbsp;day</span></td>
			<td class="green center">2362</td>
			<td class="red lasttd center">1543</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11208136,0" class="icommentjs icon16" href="/hairbrained-2013-720p-brrip-x264-yify-t11208136.html#comment"> <em class="iconvalue">19</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hairbrained-2013-720p-brrip-x264-yify-t11208136.html" class="cellMainLink">HairBrained (2013) 720p BrRip x264 - YIFY</a></div>
			</td>
			<td class="nobr center" data-sort="847571755">808.31 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="05 Sep 2015, 18:16">2&nbsp;days</span></td>
			<td class="green center">2291</td>
			<td class="red lasttd center">1433</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11211431,0" class="icommentjs icon16" href="/minions-2015-hdts-aac-ssm-mp4-incl-sample-t11211431.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/minions-2015-hdts-aac-ssm-mp4-incl-sample-t11211431.html" class="cellMainLink">Minions (2015) HDTS AAC-SSM.mp4 {Incl Sample}</a></div>
			</td>
			<td class="nobr center" data-sort="293047756">279.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 10:53">1&nbsp;day</span></td>
			<td class="green center">2175</td>
			<td class="red lasttd center">1442</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11200572,0" class="icommentjs icon16" href="/contracted-phase-ii-2015-hdrip-xvid-ac3-evo-t11200572.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/contracted-phase-ii-2015-hdrip-xvid-ac3-evo-t11200572.html" class="cellMainLink">Contracted Phase II 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1497321964">1.39 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Sep 2015, 07:49">3&nbsp;days</span></td>
			<td class="green center">1707</td>
			<td class="red lasttd center">1555</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11203145,0" class="icommentjs icon16" href="/dark-was-the-night-2014-brrip-xvid-ac3-evo-t11203145.html#comment"> <em class="iconvalue">26</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dark-was-the-night-2014-brrip-xvid-ac3-evo-t11203145.html" class="cellMainLink">Dark Was the Night 2014 BRRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1473974143">1.37 <span>GB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Sep 2015, 19:36">3&nbsp;days</span></td>
			<td class="green center">1716</td>
			<td class="red lasttd center">1515</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11211997,0" class="icommentjs icon16" href="/mera-target-2015-hd-cam-rip-x264-1cd-eng-subs-team-ictv-exclusive-t11211997.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mera-target-2015-hd-cam-rip-x264-1cd-eng-subs-team-ictv-exclusive-t11211997.html" class="cellMainLink">Mera Target (2015) HD Cam Rip - x264 - [1CD] - Eng Subs - Team IcTv Exclusive</a></div>
			</td>
			<td class="nobr center" data-sort="775156772">739.25 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Sep 2015, 12:02">1&nbsp;day</span></td>
			<td class="green center">507</td>
			<td class="red lasttd center">1724</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11213353,0" class="icommentjs icon16" href="/the-chosen-2015-hdrip-xvid-ac3-etrg-t11213353.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-chosen-2015-hdrip-xvid-ac3-etrg-t11213353.html" class="cellMainLink">The Chosen.2015.HDRip.XViD.AC3-ETRG</a></div>
			</td>
			<td class="nobr center" data-sort="1490046267">1.39 <span>GB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="06 Sep 2015, 18:51">1&nbsp;day</span></td>
			<td class="green center">699</td>
			<td class="red lasttd center">1126</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11210598,0" class="icommentjs icon16" href="/regular-show-the-movie-2015-hdrip-xvid-ac3-evo-t11210598.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/regular-show-the-movie-2015-hdrip-xvid-ac3-evo-t11210598.html" class="cellMainLink">Regular Show The Movie 2015 HDRip XviD AC3-EVO</a></div>
			</td>
			<td class="nobr center" data-sort="1485636422">1.38 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Sep 2015, 06:49">1&nbsp;day</span></td>
			<td class="green center">584</td>
			<td class="red lasttd center">671</td>
        </tr>
			</table>

<h2><a class="plain" href="/tv/">TV Shows Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/tv/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11210403,0" class="icommentjs icon16" href="/ufc-191-johnson-vs-dodson-2-webrip-x264-jkkk-sparrow-t11210403.html#comment"> <em class="iconvalue">39</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-191-johnson-vs-dodson-2-webrip-x264-jkkk-sparrow-t11210403.html" class="cellMainLink">UFC 191 Johnson Vs Dodson 2 WEBRip x264-jkkk -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1839733780">1.71 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="06 Sep 2015, 05:29">1&nbsp;day</span></td>
			<td class="green center">5126</td>
			<td class="red lasttd center">1445</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11214432,0" class="icommentjs icon16" href="/the-last-ship-s02e13-hdtv-x264-lol-ettv-t11214432.html#comment"> <em class="iconvalue">75</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-last-ship-s02e13-hdtv-x264-lol-ettv-t11214432.html" class="cellMainLink">The Last Ship S02E13 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="389734812">371.68 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="07 Sep 2015, 02:02">21&nbsp;hours</span></td>
			<td class="green center">2044</td>
			<td class="red lasttd center">3902</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11198846,0" class="icommentjs icon16" href="/under-the-dome-s03e12-hdtv-x264-lol-ettv-t11198846.html#comment"> <em class="iconvalue">87</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/under-the-dome-s03e12-hdtv-x264-lol-ettv-t11198846.html" class="cellMainLink">Under the Dome S03E12 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="330839076">315.51 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Sep 2015, 00:00">3&nbsp;days</span></td>
			<td class="green center">3383</td>
			<td class="red lasttd center">1211</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11214589,0" class="icommentjs icon16" href="/the-strain-s02e09-hdtv-x264-batv-ettv-t11214589.html#comment"> <em class="iconvalue">63</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/the-strain-s02e09-hdtv-x264-batv-ettv-t11214589.html" class="cellMainLink">The Strain S02E09 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="328424491">313.21 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="07 Sep 2015, 03:10">20&nbsp;hours</span></td>
			<td class="green center">1164</td>
			<td class="red lasttd center">2900</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11214418,0" class="icommentjs icon16" href="/ray-donovan-s03e09-hdtv-x264-lol-ettv-t11214418.html#comment"> <em class="iconvalue">44</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ray-donovan-s03e09-hdtv-x264-lol-ettv-t11214418.html" class="cellMainLink">Ray Donovan S03E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="309607107">295.26 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="07 Sep 2015, 01:55">21&nbsp;hours</span></td>
			<td class="green center">1644</td>
			<td class="red lasttd center">2114</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11206873,0" class="icommentjs icon16" href="/fear-the-walking-dead-s01e02-so-close-yet-so-far-1080p-web-dl-dd5-1-x264-ntb-t11206873.html#comment"> <em class="iconvalue">68</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/fear-the-walking-dead-s01e02-so-close-yet-so-far-1080p-web-dl-dd5-1-x264-ntb-t11206873.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/fear-the-walking-dead-s01e02-so-close-yet-so-far-1080p-web-dl-dd5-1-x264-ntb-t11206873.html" class="cellMainLink">Fear The Walking Dead S01E02 So Close, Yet So Far 1080p WEB-DL DD5.1 x264-NTb</a></div>
			</td>
			<td class="nobr center" data-sort="1793493534">1.67 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Sep 2015, 13:15">2&nbsp;days</span></td>
			<td class="green center">1790</td>
			<td class="red lasttd center">954</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11209813,0" class="icommentjs icon16" href="/ufc-191-preliminary-fights-pdtv-x264-tx-sparrow-t11209813.html#comment"> <em class="iconvalue">29</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ufc-191-preliminary-fights-pdtv-x264-tx-sparrow-t11209813.html" class="cellMainLink">UFC 191 Preliminary Fights PDTV x264 - TX -={SPARROW}=-</a></div>
			</td>
			<td class="nobr center" data-sort="1757156882">1.64 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="06 Sep 2015, 02:24">1&nbsp;day</span></td>
			<td class="green center">1698</td>
			<td class="red lasttd center">504</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11199417,0" class="icommentjs icon16" href="/mistresses-us-s03e13-hdtv-x264-killers-ettv-t11199417.html#comment"> <em class="iconvalue">14</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/mistresses-us-s03e13-hdtv-x264-killers-ettv-t11199417.html" class="cellMainLink">Mistresses US S03E13 HDTV x264-KILLERS[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="298267102">284.45 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="04 Sep 2015, 02:13">3&nbsp;days</span></td>
			<td class="green center">1181</td>
			<td class="red lasttd center">359</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11209702,0" class="icommentjs icon16" href="/blunt-talk-s01e03-hdtv-x264-batv-ettv-t11209702.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/blunt-talk-s01e03-hdtv-x264-batv-ettv-t11209702.html" class="cellMainLink">Blunt Talk S01E03 HDTV x264-BATV[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="178726646">170.45 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="06 Sep 2015, 01:39">1&nbsp;day</span></td>
			<td class="green center">1063</td>
			<td class="red lasttd center">342</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11204494,0" class="icommentjs icon16" href="/hannibal-s03-season-3-1080p-5-1ch-web-dl-reenc-deejayahmed-t11204494.html#comment"> <em class="iconvalue">31</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hannibal-s03-season-3-1080p-5-1ch-web-dl-reenc-deejayahmed-t11204494.html" class="cellMainLink">Hannibal S03 Season 3 1080p 5.1Ch Web-DL ReEnc-DeeJayAhmed</a></div>
			</td>
			<td class="nobr center" data-sort="7487116339">6.97 <span>GB</span></td>
			<td class="center">28</td>
			<td class="center"><span title="05 Sep 2015, 03:49">2&nbsp;days</span></td>
			<td class="green center">496</td>
			<td class="red lasttd center">699</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11214538,0" class="icommentjs icon16" href="/masters-of-sex-s03e09-hdtv-x264-lol-ettv-t11214538.html#comment"> <em class="iconvalue">15</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/masters-of-sex-s03e09-hdtv-x264-lol-ettv-t11214538.html" class="cellMainLink">Masters of Sex S03E09 HDTV x264-LOL[ettv]</a></div>
			</td>
			<td class="nobr center" data-sort="349738033">333.54 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="07 Sep 2015, 02:58">20&nbsp;hours</span></td>
			<td class="green center">404</td>
			<td class="red lasttd center">738</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11211904,0" class="icommentjs icon16" href="/hand-of-god-s01e01-e02-e03-e04-e05-e06-e07-e08-e09-e10-webrip-x264-nogrp-complete-season-glodls-t11211904.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/hand-of-god-s01e01-e02-e03-e04-e05-e06-e07-e08-e09-e10-webrip-x264-nogrp-complete-season-glodls-t11211904.html" class="cellMainLink">Hand Of God S01E01 E02 E03 E04 E05 E06 E07 E08 E09 E10 WEBRiP x264-NoGRP [Complete Season] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="5193508484">4.84 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="06 Sep 2015, 11:42">1&nbsp;day</span></td>
			<td class="green center">252</td>
			<td class="red lasttd center">708</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11214480,0" class="icommentjs icon16" href="/big-brother-us-s17e33-hdtv-x264-wrcr-t11214480.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/big-brother-us-s17e33-hdtv-x264-wrcr-t11214480.html" class="cellMainLink">Big Brother US S17E33 HDTV x264-[WRCR]</a></div>
			</td>
			<td class="nobr center" data-sort="369228409">352.12 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="07 Sep 2015, 02:26">21&nbsp;hours</span></td>
			<td class="green center">501</td>
			<td class="red lasttd center">356</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11213293,0" class="icommentjs icon16" href="/formula-1-2015-italian-grand-prix-race-720p-hd-t11213293.html#comment"> <em class="iconvalue">18</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/formula-1-2015-italian-grand-prix-race-720p-hd-t11213293.html" class="cellMainLink">Formula 1 2015 Italian Grand Prix Race 720p HD</a></div>
			</td>
			<td class="nobr center" data-sort="5818296969">5.42 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 18:42">1&nbsp;day</span></td>
			<td class="green center">433</td>
			<td class="red lasttd center">344</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11214473,0" class="icommentjs icon16" href="/bachelor-in-paradise-s02e11-hdtv-x264-t11214473.html#comment"> <em class="iconvalue">16</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bachelor-in-paradise-s02e11-hdtv-x264-t11214473.html" class="cellMainLink">Bachelor in Paradise S02E11 HDTV x264</a></div>
			</td>
			<td class="nobr center" data-sort="756712813">721.66 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="07 Sep 2015, 02:24">21&nbsp;hours</span></td>
			<td class="green center">191</td>
			<td class="red lasttd center">499</td>
        </tr>
			</table>

<h2><a class="plain" href="/music/">Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/music/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11201142,0" class="icommentjs icon16" href="/rick-ross-black-dollar-2015-320-kbps-t11201142.html#comment"> <em class="iconvalue">20</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rick-ross-black-dollar-2015-320-kbps-t11201142.html" class="cellMainLink">Rick Ross â Black Dollar (2015) 320 KBPS</a></div>
			</td>
			<td class="nobr center" data-sort="174329223">166.25 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="04 Sep 2015, 10:30">3&nbsp;days</span></td>
			<td class="green center">992</td>
			<td class="red lasttd center">176</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11204571,0" class="icommentjs icon16" href="/the-official-uk-top-40-singles-chart-04th-september-2015-rz-rg-mp3-320kbps-glodls-t11204571.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-official-uk-top-40-singles-chart-04th-september-2015-rz-rg-mp3-320kbps-glodls-t11204571.html" class="cellMainLink">The Official UK Top 40 Singles Chart (04th September 2015) (RZ-RG) [MP3-320KBPS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="351154281">334.89 <span>MB</span></td>
			<td class="center">43</td>
			<td class="center"><span title="05 Sep 2015, 04:17">2&nbsp;days</span></td>
			<td class="green center">545</td>
			<td class="red lasttd center">295</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11200382,0" class="icommentjs icon16" href="/scarface-deeply-rooted-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11200382.html#comment"> <em class="iconvalue">22</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/scarface-deeply-rooted-2015-mp3-320kbps-cbr-sn3h1t87-glodls-t11200382.html" class="cellMainLink">Scarface â Deeply Rooted [2015] [MP3-320Kbps] [CBR] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="162138850">154.63 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="04 Sep 2015, 06:58">3&nbsp;days</span></td>
			<td class="green center">713</td>
			<td class="red lasttd center">94</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11205279,0" class="icommentjs icon16" href="/va-club-music-hits-september-novation-320kbps-2015-mp3-t11205279.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-club-music-hits-september-novation-320kbps-2015-mp3-t11205279.html" class="cellMainLink">VA - Club Music Hits September Novation [320kbps] (2015) MP3</a></div>
			</td>
			<td class="nobr center" data-sort="1022752779">975.37 <span>MB</span></td>
			<td class="center">80</td>
			<td class="center"><span title="05 Sep 2015, 07:16">2&nbsp;days</span></td>
			<td class="green center">321</td>
			<td class="red lasttd center">144</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11200634,0" class="icommentjs icon16" href="/now-that-s-what-i-call-house-2015-3cd-cbr-mp3-glodls-t11200634.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/now-that-s-what-i-call-house-2015-3cd-cbr-mp3-glodls-t11200634.html" class="cellMainLink">Now That&#039;s What I Call House 2015 - [3CD]-[CBR]-[MP3] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="570374052">543.95 <span>MB</span></td>
			<td class="center">72</td>
			<td class="center"><span title="04 Sep 2015, 08:09">3&nbsp;days</span></td>
			<td class="green center">288</td>
			<td class="red lasttd center">102</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11200520,0" class="icommentjs icon16" href="/k-camp-only-way-is-up-deluxe-explicit-2015-cbr-320-kbps-aryan-l33t-t11200520.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/k-camp-only-way-is-up-deluxe-explicit-2015-cbr-320-kbps-aryan-l33t-t11200520.html" class="cellMainLink">K CAMP - Only Way Is Up (Deluxe) [EXPLICIT] (2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center" data-sort="169505669">161.65 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="04 Sep 2015, 07:31">3&nbsp;days</span></td>
			<td class="green center">333</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11205056,0" class="icommentjs icon16" href="/slayer-repentless-2015-bbm-t11205056.html#comment"> <em class="iconvalue">17</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/slayer-repentless-2015-bbm-t11205056.html" class="cellMainLink">Slayer - Repentless (2015) BBM</a></div>
			</td>
			<td class="nobr center" data-sort="106153996">101.24 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="05 Sep 2015, 06:13">2&nbsp;days</span></td>
			<td class="green center">225</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11206628,0" class="icommentjs icon16" href="/travis-scott-rodeo-deluxe-edition-digital-booklet-2015-cbr-320-kbps-aryan-l33t-t11206628.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/travis-scott-rodeo-deluxe-edition-digital-booklet-2015-cbr-320-kbps-aryan-l33t-t11206628.html" class="cellMainLink">Travis Scott - Rodeo (Deluxe Edition) + Digital Booklet (2015) CBR 320 Kbps [AryaN_L33T]</a></div>
			</td>
			<td class="nobr center" data-sort="183407552">174.91 <span>MB</span></td>
			<td class="center">21</td>
			<td class="center"><span title="05 Sep 2015, 12:02">2&nbsp;days</span></td>
			<td class="green center">125</td>
			<td class="red lasttd center">57</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11205094,0" class="icommentjs icon16" href="/va-relaxing-piano-jazz-essentials-2015-mp3-320-kbps-t11205094.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-relaxing-piano-jazz-essentials-2015-mp3-320-kbps-t11205094.html" class="cellMainLink">VA - Relaxing Piano Jazz Essentials (2015) MP3 [320 kbps]</a></div>
			</td>
			<td class="nobr center" data-sort="420723499">401.23 <span>MB</span></td>
			<td class="center">44</td>
			<td class="center"><span title="05 Sep 2015, 06:29">2&nbsp;days</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11201060,0" class="icommentjs icon16" href="/album-k-camp-only-way-is-up-deluxe-edition-beast-coast-t11201060.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/album-k-camp-only-way-is-up-deluxe-edition-beast-coast-t11201060.html" class="cellMainLink">[Album] K Camp â Only Way Is Up (Deluxe Edition) {Beast Coast }</a></div>
			</td>
			<td class="nobr center" data-sort="148353776">141.48 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="04 Sep 2015, 10:07">3&nbsp;days</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">83</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11203625,0" class="icommentjs icon16" href="/iron-maiden-the-book-of-souls-2015-vbrv0-proper-cdrip-freak37-t11203625.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/iron-maiden-the-book-of-souls-2015-vbrv0-proper-cdrip-freak37-t11203625.html" class="cellMainLink">Iron Maiden The Book Of Souls [2015] VBRV0-PROPER CDRIP - Freak37</a></div>
			</td>
			<td class="nobr center" data-sort="183718161">175.21 <span>MB</span></td>
			<td class="center">18</td>
			<td class="center"><span title="04 Sep 2015, 21:45">3&nbsp;days</span></td>
			<td class="green center">42</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/chaska-2015-punjabi-album-mp3-songs-vbr-kajal-t11211207.html" class="cellMainLink">Chaska (2015) ~ Punjabi ~ Album ~ Mp3 ~ Songs ~ VBR ~ [kajal]</a></div>
			</td>
			<td class="nobr center" data-sort="58744388">56.02 <span>MB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="06 Sep 2015, 09:39">1&nbsp;day</span></td>
			<td class="green center">27</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/r3hab-r3hab-inspired-ministry-of-sound-2cd-2015-mp3-320kbps-h4ckus-glodls-t11212387.html" class="cellMainLink">R3hab - R3HAB : Inspired - Ministry of Sound [2CD] [2015] [MP3-320KBPS] [H4CKUS] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="731913383">698.01 <span>MB</span></td>
			<td class="center">38</td>
			<td class="center"><span title="06 Sep 2015, 13:48">1&nbsp;day</span></td>
			<td class="green center">29</td>
			<td class="red lasttd center">28</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-100-techno-minimal-tracks-2015-t11213377.html" class="cellMainLink">VA - 100 Techno Minimal Tracks (2015)</a></div>
			</td>
			<td class="nobr center" data-sort="1508516955">1.4 <span>GB</span></td>
			<td class="center">103</td>
			<td class="center"><span title="06 Sep 2015, 18:59">1&nbsp;day</span></td>
			<td class="green center">13</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11209212,0" class="icommentjs icon16" href="/drake-tell-your-friends-freestyle-beast-coast-t11209212.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/drake-tell-your-friends-freestyle-beast-coast-t11209212.html" class="cellMainLink">Drake - Tell Your Friends Freestyle { Beast Coast }</a></div>
			</td>
			<td class="nobr center" data-sort="2961866">2.82 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Sep 2015, 23:25">2&nbsp;days</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">32</td>
        </tr>
			</table>

<h2><a class="plain" href="/games/">Games Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/games/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11200687,0" class="icommentjs icon16" href="/metal-gear-solid-v-the-phantom-pain-inc-update-2-prepack-by-corepack-t11200687.html#comment"> <em class="iconvalue">193</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/metal-gear-solid-v-the-phantom-pain-inc-update-2-prepack-by-corepack-t11200687.html" class="cellMainLink">Metal Gear Solid V: The Phantom Pain Inc. Update 2 - PrePack by CorePack</a></div>
			</td>
			<td class="nobr center" data-sort="13304800821">12.39 <span>GB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="04 Sep 2015, 08:20">3&nbsp;days</span></td>
			<td class="green center">827</td>
			<td class="red lasttd center">2049</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11198742,0" class="icommentjs icon16" href="/act-of-aggression-updated-to-v515-multi5-fitgirl-repack-t11198742.html#comment"> <em class="iconvalue">49</em><i class="ka ka-comment"></i></a> 					<a class="icon16" href="/act-of-aggression-updated-to-v515-multi5-fitgirl-repack-t11198742.html#stills" title="Torrent Has Screenshots"><i class="ka ka16 ka-camera"></i></a>
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/act-of-aggression-updated-to-v515-multi5-fitgirl-repack-t11198742.html" class="cellMainLink">Act of Aggression (Updated to v515, MULTI5) [FitGirl Repack]</a></div>
			</td>
			<td class="nobr center" data-sort="7180068377">6.69 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="03 Sep 2015, 23:35">3&nbsp;days</span></td>
			<td class="green center">131</td>
			<td class="red lasttd center">211</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11201075,0" class="icommentjs icon16" href="/the-witcher-3-wild-hunt-patch-1-08-4-gog-t11201075.html#comment"> <em class="iconvalue">63</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-witcher-3-wild-hunt-patch-1-08-4-gog-t11201075.html" class="cellMainLink">The Witcher 3: Wild Hunt (Patch 1.08.4) (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="2199126905">2.05 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="04 Sep 2015, 10:13">3&nbsp;days</span></td>
			<td class="green center">205</td>
			<td class="red lasttd center">112</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11204705,0" class="icommentjs icon16" href="/dragon-age-inquisition-1-10-u9-repack-r-g-games-naswari-zohaib-t11204705.html#comment"> <em class="iconvalue">62</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/dragon-age-inquisition-1-10-u9-repack-r-g-games-naswari-zohaib-t11204705.html" class="cellMainLink">Dragon Age: Inquisition (1.10/u9) Repack R.G Games NASWARI+ZOHAIB</a></div>
			</td>
			<td class="nobr center" data-sort="36862412917">34.33 <span>GB</span></td>
			<td class="center">14</td>
			<td class="center"><span title="05 Sep 2015, 04:41">2&nbsp;days</span></td>
			<td class="green center">51</td>
			<td class="red lasttd center">169</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11205586,0" class="icommentjs icon16" href="/where-angels-cry-2-tears-of-the-fallen-ce-2015-pc-final-t11205586.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/where-angels-cry-2-tears-of-the-fallen-ce-2015-pc-final-t11205586.html" class="cellMainLink">Where Angels Cry 2: Tears of the Fallen CE (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="1262297163">1.18 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Sep 2015, 08:33">2&nbsp;days</span></td>
			<td class="green center">156</td>
			<td class="red lasttd center">55</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11205247,0" class="icommentjs icon16" href="/eador-masters-of-the-broken-world-r-g-mechanics-t11205247.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/eador-masters-of-the-broken-world-r-g-mechanics-t11205247.html" class="cellMainLink">Eador: Masters of the Broken World [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="721065819">687.66 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="05 Sep 2015, 07:09">2&nbsp;days</span></td>
			<td class="green center">185</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11208369,0" class="icommentjs icon16" href="/car-mechanic-simulator-2015-r-g-mechanics-t11208369.html#comment"> <em class="iconvalue">12</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/car-mechanic-simulator-2015-r-g-mechanics-t11208369.html" class="cellMainLink">Car Mechanic Simulator 2015 [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="897097796">855.54 <span>MB</span></td>
			<td class="center">12</td>
			<td class="center"><span title="05 Sep 2015, 19:02">2&nbsp;days</span></td>
			<td class="green center">76</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11208538,0" class="icommentjs icon16" href="/candy-crush-saga-v1-58-0-4-apk-game-mod-shopping-full-2015-sn3h1t87-glodls-t11208538.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/candy-crush-saga-v1-58-0-4-apk-game-mod-shopping-full-2015-sn3h1t87-glodls-t11208538.html" class="cellMainLink">Candy Crush Saga v1.58.0.4 [APK Game] (Mod Shopping) Full [2015] [sn3h1t87] [GloDLS]</a></div>
			</td>
			<td class="nobr center" data-sort="94078095">89.72 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Sep 2015, 19:48">2&nbsp;days</span></td>
			<td class="green center">84</td>
			<td class="red lasttd center">23</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11206222,0" class="icommentjs icon16" href="/shadowrun-dragonfall-director-s-cut-r-g-mechanics-t11206222.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/shadowrun-dragonfall-director-s-cut-r-g-mechanics-t11206222.html" class="cellMainLink">Shadowrun Dragonfall - Director&#039;s Cut [R.G. Mechanics]</a></div>
			</td>
			<td class="nobr center" data-sort="1106058398">1.03 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="05 Sep 2015, 10:08">2&nbsp;days</span></td>
			<td class="green center">71</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11208693,0" class="icommentjs icon16" href="/renowned-explorers-international-society-gog-t11208693.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/renowned-explorers-international-society-gog-t11208693.html" class="cellMainLink">Renowned Explorers: International Society (GOG)</a></div>
			</td>
			<td class="nobr center" data-sort="710309174">677.4 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="05 Sep 2015, 20:23">2&nbsp;days</span></td>
			<td class="green center">64</td>
			<td class="red lasttd center">13</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11205500,0" class="icommentjs icon16" href="/weeping-skies-2015-pc-final-t11205500.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/weeping-skies-2015-pc-final-t11205500.html" class="cellMainLink">Weeping Skies (2015) PC [FINAL]</a></div>
			</td>
			<td class="nobr center" data-sort="1009095472">962.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Sep 2015, 08:16">2&nbsp;days</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11204606,0" class="icommentjs icon16" href="/painters-guild-v1-071-t11204606.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/painters-guild-v1-071-t11204606.html" class="cellMainLink">Painters Guild v1.071</a></div>
			</td>
			<td class="nobr center" data-sort="48028387">45.8 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Sep 2015, 04:26">2&nbsp;days</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11213433,0" class="icommentjs icon16" href="/rust-client-experimental-v1316-x64-crack-lumaemu-kortal-t11213433.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/rust-client-experimental-v1316-x64-crack-lumaemu-kortal-t11213433.html" class="cellMainLink">Rust Client Experimental v1316 x64 crack LumaEmu #Kortal</a></div>
			</td>
			<td class="nobr center" data-sort="1947650285">1.81 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 19:15">1&nbsp;day</span></td>
			<td class="green center">13</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11212870,0" class="icommentjs icon16" href="/tomb-raider-1-2-3-gog-classic-i-know-t11212870.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/tomb-raider-1-2-3-gog-classic-i-know-t11212870.html" class="cellMainLink">Tomb Raider 1 2 3 GoG Classic-I_KnoW</a></div>
			</td>
			<td class="nobr center" data-sort="877641831">836.98 <span>MB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="06 Sep 2015, 16:19">1&nbsp;day</span></td>
			<td class="green center">12</td>
			<td class="red lasttd center">8</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType Type">
                    <a href="/grim-fandango-remastered-ver-1-4-0-linux-gog-t11213672.html" class="cellMainLink">Grim Fandango Remastered ver. 1.4.0 LINUX - GOG</a></div>
			</td>
			<td class="nobr center" data-sort="4610962645">4.29 <span>GB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="06 Sep 2015, 20:52">1&nbsp;day</span></td>
			<td class="green center">3</td>
			<td class="red lasttd center">9</td>
        </tr>
			</table>

<h2><a class="plain" href="/applications/">Applications Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/applications/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11202881,0" class="icommentjs icon16" href="/the-kmplayer-4-0-0-0-2015-Ð Ð¡-repack-by-cuta-frank-t11202881.html#comment"> <em class="iconvalue">43</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/the-kmplayer-4-0-0-0-2015-Ð Ð¡-repack-by-cuta-frank-t11202881.html" class="cellMainLink">The KMPlayer 4.0.0.0 (2015) Ð Ð¡ | RePack by CUTA-FRANK</a></div>
			</td>
			<td class="nobr center" data-sort="58171133">55.48 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Sep 2015, 18:27">3&nbsp;days</span></td>
			<td class="green center">481</td>
			<td class="red lasttd center">39</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11203773,0" class="icommentjs icon16" href="/disable-windows-10-tracking-x32-x64-eng-t11203773.html#comment"> <em class="iconvalue">44</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/disable-windows-10-tracking-x32-x64-eng-t11203773.html" class="cellMainLink">Disable Windows 10 Tracking (x32/x64)[ENG]</a></div>
			</td>
			<td class="nobr center" data-sort="7021655">6.7 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Sep 2015, 22:32">3&nbsp;days</span></td>
			<td class="green center">313</td>
			<td class="red lasttd center">33</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11202331,0" class="icommentjs icon16" href="/windows-10-login-theme-changer-0-3-pbs-t11202331.html#comment"> <em class="iconvalue">65</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/windows-10-login-theme-changer-0-3-pbs-t11202331.html" class="cellMainLink">Windows 10 Login Theme Changer 0.3 [PBS]</a></div>
			</td>
			<td class="nobr center" data-sort="1012659">988.92 <span>KB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="04 Sep 2015, 15:46">3&nbsp;days</span></td>
			<td class="green center">244</td>
			<td class="red lasttd center">14</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11204409,0" class="icommentjs icon16" href="/mojosoft-businesscards-mx-5-00-x32-x64-multi-serial-t11204409.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/mojosoft-businesscards-mx-5-00-x32-x64-multi-serial-t11204409.html" class="cellMainLink">Mojosoft BusinessCards MX 5.00 (x32/x64)[Multi][Serial]</a></div>
			</td>
			<td class="nobr center" data-sort="92707629">88.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Sep 2015, 03:29">2&nbsp;days</span></td>
			<td class="green center">147</td>
			<td class="red lasttd center">21</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11206341,0" class="icommentjs icon16" href="/tubemate-2-2-5-641-mod-apk-adfree-material-design-md-modded-osmdroid-t11206341.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/tubemate-2-2-5-641-mod-apk-adfree-material-design-md-modded-osmdroid-t11206341.html" class="cellMainLink">TubeMate 2.2.5.641 Mod apk AdFree Material Design MD Modded {OsmDroid}</a></div>
			</td>
			<td class="nobr center" data-sort="2524290">2.41 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="05 Sep 2015, 10:40">2&nbsp;days</span></td>
			<td class="green center">127</td>
			<td class="red lasttd center">11</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11212894,0" class="icommentjs icon16" href="/windows-8-1-valentine-edition-2015-x64-en-us-pre-activated-team-os-t11212894.html#comment"> <em class="iconvalue">25</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-8-1-valentine-edition-2015-x64-en-us-pre-activated-team-os-t11212894.html" class="cellMainLink">Windows 8.1 Valentine Edition 2015 X64 En-Us Pre-Activated-=TEAM OS=</a></div>
			</td>
			<td class="nobr center" data-sort="6954220294">6.48 <span>GB</span></td>
			<td class="center">6</td>
			<td class="center"><span title="06 Sep 2015, 16:27">1&nbsp;day</span></td>
			<td class="green center">26</td>
			<td class="red lasttd center">80</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11211286,0" class="icommentjs icon16" href="/saavn-pro-4-2-mod-apk-cracked-adfree-android-modded-app-download-songs-for-free-osmdroid-t11211286.html#comment"> <em class="iconvalue">8</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/saavn-pro-4-2-mod-apk-cracked-adfree-android-modded-app-download-songs-for-free-osmdroid-t11211286.html" class="cellMainLink">Saavn Pro 4.2 Mod apk Cracked AdFree Android Modded app | Download Songs For Free {OsmDroid}</a></div>
			</td>
			<td class="nobr center" data-sort="21628524">20.63 <span>MB</span></td>
			<td class="center">2</td>
			<td class="center"><span title="06 Sep 2015, 10:04">1&nbsp;day</span></td>
			<td class="green center">62</td>
			<td class="red lasttd center">18</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11209704,0" class="icommentjs icon16" href="/daz3d-swimsuit-for-genesis-3-female-t11209704.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-swimsuit-for-genesis-3-female-t11209704.html" class="cellMainLink">Daz3d - Swimsuit for Genesis 3 Female</a></div>
			</td>
			<td class="nobr center" data-sort="95460320">91.04 <span>MB</span></td>
			<td class="center">9</td>
			<td class="center"><span title="06 Sep 2015, 01:40">1&nbsp;day</span></td>
			<td class="green center">68</td>
			<td class="red lasttd center">12</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11206004,0" class="icommentjs icon16" href="/acdsee-pro-8-2-build-287-32-bit-and-64-bit-keygen-core-t11206004.html#comment"> <em class="iconvalue">10</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType exeType">
                    <a href="/acdsee-pro-8-2-build-287-32-bit-and-64-bit-keygen-core-t11206004.html" class="cellMainLink">ACDSee Pro 8.2 Build 287 (32 Bit and 64 Bit) + Keygen [CORE]</a></div>
			</td>
			<td class="nobr center" data-sort="145397488">138.66 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="05 Sep 2015, 09:47">2&nbsp;days</span></td>
			<td class="green center">45</td>
			<td class="red lasttd center">19</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11201989,0" class="icommentjs icon16" href="/native-instruments-scarbee-funk-guitarist-scd-kontakt-audiop2p-t11201989.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/native-instruments-scarbee-funk-guitarist-scd-kontakt-audiop2p-t11201989.html" class="cellMainLink">Native Instruments Scarbee Funk Guitarist SCD KONTAKT-AudioP2P</a></div>
			</td>
			<td class="nobr center" data-sort="7962894489">7.42 <span>GB</span></td>
			<td class="center">43</td>
			<td class="center"><span title="04 Sep 2015, 14:02">3&nbsp;days</span></td>
			<td class="green center">25</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/big-fish-audio-steel-and-wood-songwriter-acoustic-sessions-multiformat-magnetrixx-t11202151.html" class="cellMainLink">Big Fish Audio Steel and Wood Songwriter Acoustic Sessions MULTiFORMAT-MAGNETRiXX</a></div>
			</td>
			<td class="nobr center" data-sort="4643790217">4.32 <span>GB</span></td>
			<td class="center">49</td>
			<td class="center"><span title="04 Sep 2015, 14:42">3&nbsp;days</span></td>
			<td class="green center">12</td>
			<td class="red lasttd center">36</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/zero-g-desert-tracks-multiformat-audiostrike-oddsox-t11208793.html" class="cellMainLink">Zero G Desert Tracks MULTiFORMAT-AUDIOSTRiKE [oddsox]</a></div>
			</td>
			<td class="nobr center" data-sort="2117037192">1.97 <span>GB</span></td>
			<td class="center">26</td>
			<td class="center"><span title="05 Sep 2015, 20:56">2&nbsp;days</span></td>
			<td class="green center">20</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11210425,0" class="icommentjs icon16" href="/big-fish-audio-legendary-smooth-rnb-multiformat-t11210425.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/big-fish-audio-legendary-smooth-rnb-multiformat-t11210425.html" class="cellMainLink">Big Fish Audio Legendary Smooth RnB MULTiFORMAT</a></div>
			</td>
			<td class="nobr center" data-sort="4673740505">4.35 <span>GB</span></td>
			<td class="center">7</td>
			<td class="center"><span title="06 Sep 2015, 05:39">1&nbsp;day</span></td>
			<td class="green center">21</td>
			<td class="red lasttd center">10</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11201562,0" class="icommentjs icon16" href="/windows-10-ga-aio-22in1-pre-activated-dvd-version-x86-x64-en-us-t11201562.html#comment"> <em class="iconvalue">40</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/windows-10-ga-aio-22in1-pre-activated-dvd-version-x86-x64-en-us-t11201562.html" class="cellMainLink">Windows 10 GA AIO | 22in1 | Pre-Activated | DVD Version | x86 x64 | en-us</a></div>
			</td>
			<td class="nobr center" data-sort="4510253056">4.2 <span>GB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Sep 2015, 12:27">3&nbsp;days</span></td>
			<td class="green center">12</td>
			<td class="red lasttd center">16</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType zipType">
                    <a href="/daz3d-poser-ds-pstx2731b-ebony-expansion-light-skin-dim-t11213627.html" class="cellMainLink">DAZ3D - POSER DS-pstx2731b Ebony Expansion Light Skin (DIM)</a></div>
			</td>
			<td class="nobr center" data-sort="85105443">81.16 <span>MB</span></td>
			<td class="center">4</td>
			<td class="center"><span title="06 Sep 2015, 20:27">1&nbsp;day</span></td>
			<td class="green center">14</td>
			<td class="red lasttd center">9</td>
        </tr>
			</table>

<h2><a class="plain" href="/anime/">Anime Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/anime/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/leopard-raws-god-eater-07-raw-mx-1280x720-x264-aac-mp4-t11212992.html" class="cellMainLink">[Leopard-Raws] God Eater - 07 RAW (MX 1280x720 x264 AAC).mp4</a></div>
			</td>
			<td class="nobr center" data-sort="492782726">469.95 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 17:00">1&nbsp;day</span></td>
			<td class="green center">771</td>
			<td class="red lasttd center">171</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11218675,0" class="icommentjs icon16" href="/horriblesubs-jitsu-wa-watashi-wa-10-720p-mkv-t11218675.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/horriblesubs-jitsu-wa-watashi-wa-10-720p-mkv-t11218675.html" class="cellMainLink">[HorribleSubs] Jitsu wa Watashi wa - 10 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="341409552">325.59 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="07 Sep 2015, 18:40">4&nbsp;hours</span></td>
			<td class="green center">102</td>
			<td class="red lasttd center">668</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11212030,0" class="icommentjs icon16" href="/animerg-dragon-ball-super-09-720p-mkv-t11212030.html#comment"> <em class="iconvalue">13</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/animerg-dragon-ball-super-09-720p-mkv-t11212030.html" class="cellMainLink">[AnimeRG] Dragon Ball Super - 09 [720p].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="379900781">362.3 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 12:16">1&nbsp;day</span></td>
			<td class="green center">222</td>
			<td class="red lasttd center">51</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11207397,0" class="icommentjs icon16" href="/deanzel-fate-stay-night-unlimited-blade-works-00-12-bd-1080p-hi10p-dual-audio-flac-t11207397.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/deanzel-fate-stay-night-unlimited-blade-works-00-12-bd-1080p-hi10p-dual-audio-flac-t11207397.html" class="cellMainLink">[deanzel] Fate/stay night: Unlimited Blade Works - 00-12 [BD 1080p Hi10p Dual Audio FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="27271733426">25.4 <span>GB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="05 Sep 2015, 15:22">2&nbsp;days</span></td>
			<td class="green center">45</td>
			<td class="red lasttd center">156</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11209938,0" class="icommentjs icon16" href="/ipunisher-one-piece-708-360p-aac-mp4-t11209938.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/ipunisher-one-piece-708-360p-aac-mp4-t11209938.html" class="cellMainLink">[iPUNISHER] One Piece - 708 [360p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="83013118">79.17 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 03:00">1&nbsp;day</span></td>
			<td class="green center">166</td>
			<td class="red lasttd center">24</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11204508,0" class="icommentjs icon16" href="/commie-ghost-in-the-shell-arise-alternative-architecture-bd-1080p-aac-t11204508.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-ghost-in-the-shell-arise-alternative-architecture-bd-1080p-aac-t11204508.html" class="cellMainLink">[Commie] Ghost in the Shell Arise Alternative Architecture [BD 1080p AAC]</a></div>
			</td>
			<td class="nobr center" data-sort="8654720601">8.06 <span>GB</span></td>
			<td class="center">3</td>
			<td class="center"><span title="05 Sep 2015, 03:55">2&nbsp;days</span></td>
			<td class="green center">106</td>
			<td class="red lasttd center">67</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11202230,0" class="icommentjs icon16" href="/naruto-shippuden-427-428-eng-sub-480p-l-mbert-t11202230.html#comment"> <em class="iconvalue">23</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/naruto-shippuden-427-428-eng-sub-480p-l-mbert-t11202230.html" class="cellMainLink">Naruto Shippuden 427 &amp; 428 [EnG SuB] 480p L@mBerT</a></div>
			</td>
			<td class="nobr center" data-sort="116722446">111.32 <span>MB</span></td>
			<td class="center">10</td>
			<td class="center"><span title="04 Sep 2015, 15:07">3&nbsp;days</span></td>
			<td class="green center">117</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11212744,0" class="icommentjs icon16" href="/bakedfish-god-eater-07-720p-aac-mp4-t11212744.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/bakedfish-god-eater-07-720p-aac-mp4-t11212744.html" class="cellMainLink">[BakedFish] God Eater - 07 [720p][AAC].mp4</a></div>
			</td>
			<td class="nobr center" data-sort="330386795">315.08 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 15:41">1&nbsp;day</span></td>
			<td class="green center">102</td>
			<td class="red lasttd center">22</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11211377,0" class="icommentjs icon16" href="/dragon-ball-super-episode-009-english-subbed-720p-arizone-t11211377.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/dragon-ball-super-episode-009-english-subbed-720p-arizone-t11211377.html" class="cellMainLink">DRAGON BALL SUPER Episode - 009 [ENGLISH SUBBED] 720p ~ARIZONE</a></div>
			</td>
			<td class="nobr center" data-sort="174650224">166.56 <span>MB</span></td>
			<td class="center">5</td>
			<td class="center"><span title="06 Sep 2015, 10:33">1&nbsp;day</span></td>
			<td class="green center">82</td>
			<td class="red lasttd center">34</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/vivid-gakkou-gurashi-09-670c6f83-mkv-t11200706.html" class="cellMainLink">[Vivid] Gakkou Gurashi! - 09 [670C6F83].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="394945712">376.65 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Sep 2015, 08:25">3&nbsp;days</span></td>
			<td class="green center">14</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11201238,0" class="icommentjs icon16" href="/lupin-iii-s01e01-11-bdmux1080p-ita-jap-a-c-u-m-t11201238.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/lupin-iii-s01e01-11-bdmux1080p-ita-jap-a-c-u-m-t11201238.html" class="cellMainLink">Lupin III S01e01-11 [BDMux1080p Ita-Jap][A.C.U.M.]</a></div>
			</td>
			<td class="nobr center" data-sort="7785508738">7.25 <span>GB</span></td>
			<td class="center">11</td>
			<td class="center"><span title="04 Sep 2015, 11:07">3&nbsp;days</span></td>
			<td class="green center">32</td>
			<td class="red lasttd center">47</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-bikini-warriors-09-7c66cc78-mkv-t11219024.html" class="cellMainLink">[Commie] Bikini Warriors - 09 [7C66CC78].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="54889198">52.35 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="07 Sep 2015, 20:05">3&nbsp;hours</span></td>
			<td class="green center">28</td>
			<td class="red lasttd center">40</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-prison-school-05-170bd34f-mkv-t11200574.html" class="cellMainLink">[Commie] Prison School - 05 [170BD34F].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="192794345">183.86 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="04 Sep 2015, 07:50">3&nbsp;days</span></td>
			<td class="green center">15</td>
			<td class="red lasttd center">31</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/commie-teekyuu-58-e2cfe073-mkv-t11218898.html" class="cellMainLink">[Commie] Teekyuu - 58 [E2CFE073].mkv</a></div>
			</td>
			<td class="nobr center" data-sort="40185976">38.32 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="07 Sep 2015, 19:20">4&nbsp;hours</span></td>
			<td class="green center">13</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11209162,0" class="icommentjs icon16" href="/reinforce-isuca-bdrip-1920x1080-x264-flac-t11209162.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType filmType">
                    <a href="/reinforce-isuca-bdrip-1920x1080-x264-flac-t11209162.html" class="cellMainLink">[ReinForce] ISUCA (BDRip 1920x1080 x264 FLAC)</a></div>
			</td>
			<td class="nobr center" data-sort="28421084044">26.47 <span>GB</span></td>
			<td class="center">42</td>
			<td class="center"><span title="05 Sep 2015, 23:05">2&nbsp;days</span></td>
			<td class="green center">3</td>
			<td class="red lasttd center">32</td>
        </tr>
			</table>

<h2><a class="plain" href="/books/">Books Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/books/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11204645,0" class="icommentjs icon16" href="/assorted-magazines-bundle-september-5-2015-true-pdf-t11204645.html#comment"> <em class="iconvalue">9</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/assorted-magazines-bundle-september-5-2015-true-pdf-t11204645.html" class="cellMainLink">Assorted Magazines Bundle - September 5 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="603528514">575.57 <span>MB</span></td>
			<td class="center">37</td>
			<td class="center"><span title="05 Sep 2015, 04:30">2&nbsp;days</span></td>
			<td class="green center">312</td>
			<td class="red lasttd center">236</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11210128,0" class="icommentjs icon16" href="/food-magazines-bundle-september-6-2015-true-pdf-t11210128.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/food-magazines-bundle-september-6-2015-true-pdf-t11210128.html" class="cellMainLink">Food Magazines Bundle - September 6 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="206203908">196.65 <span>MB</span></td>
			<td class="center">13</td>
			<td class="center"><span title="06 Sep 2015, 04:08">1&nbsp;day</span></td>
			<td class="green center">342</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11209257,0" class="icommentjs icon16" href="/tom-clancy-ebooks-collection-epub-mobi-t11209257.html#comment"> <em class="iconvalue">11</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/tom-clancy-ebooks-collection-epub-mobi-t11209257.html" class="cellMainLink">Tom Clancy eBooks Collection (epub/mobi)</a></div>
			</td>
			<td class="nobr center" data-sort="200733657">191.43 <span>MB</span></td>
			<td class="center">172</td>
			<td class="center"><span title="05 Sep 2015, 23:54">1&nbsp;day</span></td>
			<td class="green center">301</td>
			<td class="red lasttd center">123</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11210113,0" class="icommentjs icon16" href="/athletic-health-sports-magazines-sept-6-2015-unisex-t11210113.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/athletic-health-sports-magazines-sept-6-2015-unisex-t11210113.html" class="cellMainLink">Athletic Health &amp; Sports Magazines - Sept 6 2015 (Unisex)</a></div>
			</td>
			<td class="nobr center" data-sort="297905491">284.1 <span>MB</span></td>
			<td class="center">20</td>
			<td class="center"><span title="06 Sep 2015, 04:04">1&nbsp;day</span></td>
			<td class="green center">229</td>
			<td class="red lasttd center">121</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11205011,0" class="icommentjs icon16" href="/womens-magazines-bundle-september-5-2015-true-pdf-t11205011.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/womens-magazines-bundle-september-5-2015-true-pdf-t11205011.html" class="cellMainLink">Womens Magazines Bundle - September 5 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="473674014">451.73 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="05 Sep 2015, 06:04">2&nbsp;days</span></td>
			<td class="green center">184</td>
			<td class="red lasttd center">91</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11206997,0" class="icommentjs icon16" href="/dark-reign-marvel-event-2009-2010-digital-empire-dcp-minutemen-nem-t11206997.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dark-reign-marvel-event-2009-2010-digital-empire-dcp-minutemen-nem-t11206997.html" class="cellMainLink">Dark Reign (Marvel Event) (2009-2010) (digital) (Empire+DCP+Minutemen) (- Nem -)</a></div>
			</td>
			<td class="nobr center" data-sort="11188726008">10.42 <span>GB</span></td>
			<td class="center">340</td>
			<td class="center"><span title="05 Sep 2015, 13:46">2&nbsp;days</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">144</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11214478,0" class="icommentjs icon16" href="/home-magazines-bundle-september-7-2015-true-pdf-t11214478.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/home-magazines-bundle-september-7-2015-true-pdf-t11214478.html" class="cellMainLink">Home Magazines Bundle - September 7 2015 (True PDF)</a></div>
			</td>
			<td class="nobr center" data-sort="383553737">365.79 <span>MB</span></td>
			<td class="center">16</td>
			<td class="center"><span title="07 Sep 2015, 02:25">21&nbsp;hours</span></td>
			<td class="green center">60</td>
			<td class="red lasttd center">72</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11213751,0" class="icommentjs icon16" href="/a-dictionary-of-medieval-terms-and-phrases-true-pdf-prg-pdf-t11213751.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/a-dictionary-of-medieval-terms-and-phrases-true-pdf-prg-pdf-t11213751.html" class="cellMainLink">A Dictionary of Medieval Terms and Phrases True PDf {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="1541519">1.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 21:22">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">128</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11213829,0" class="icommentjs icon16" href="/the-psychology-of-religion-fourth-edition-an-empirical-approach-fourth-edition-edition-true-pdf-prg-pdf-t11213829.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/the-psychology-of-religion-fourth-edition-an-empirical-approach-fourth-edition-edition-true-pdf-prg-pdf-t11213829.html" class="cellMainLink">The Psychology of Religion, Fourth Edition An Empirical Approach Fourth Edition Edition true PDF {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="4512706">4.3 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 22:01">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">100</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11213284,0" class="icommentjs icon16" href="/conversation-and-cognition-1st-edition-prg-pdf-t11213284.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/conversation-and-cognition-1st-edition-prg-pdf-t11213284.html" class="cellMainLink">Conversation and Cognition 1st Edition {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="2590910">2.47 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 18:41">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">90</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11213673,0" class="icommentjs icon16" href="/dictionary-of-media-and-communications-true-pdf-prg-pdf-t11213673.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/dictionary-of-media-and-communications-true-pdf-prg-pdf-t11213673.html" class="cellMainLink">Dictionary of Media and Communications True PDf {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="3764236">3.59 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 20:53">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">85</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11213796,0" class="icommentjs icon16" href="/philosophy-of-science-and-its-discontents-second-edition-second-edition-edition-true-pdf-prg-pdf-t11213796.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/philosophy-of-science-and-its-discontents-second-edition-second-edition-edition-true-pdf-prg-pdf-t11213796.html" class="cellMainLink">Philosophy of Science and Its Discontents, Second Edition Second Edition Edition True PDf {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="1467757">1.4 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 21:40">1&nbsp;day</span></td>
			<td class="green center">1</td>
			<td class="red lasttd center">79</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11213405,0" class="icommentjs icon16" href="/lexical-categories-verbs-nouns-and-adjectives-cambridge-studies-in-linguistics-true-pdf-prg-pdf-t11213405.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/lexical-categories-verbs-nouns-and-adjectives-cambridge-studies-in-linguistics-true-pdf-prg-pdf-t11213405.html" class="cellMainLink">Lexical Categories Verbs, Nouns and Adjectives (Cambridge Studies in Linguistics) True PDF {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="3002189">2.86 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 19:05">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">76</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11213632,0" class="icommentjs icon16" href="/routledge-dictionary-of-religious-and-spiritual-quotations-2014-true-pdf-prg-pdf-t11213632.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/routledge-dictionary-of-religious-and-spiritual-quotations-2014-true-pdf-prg-pdf-t11213632.html" class="cellMainLink">Routledge Dictionary of Religious and Spiritual Quotations 2014 true PDf {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="1839631">1.75 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 20:29">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">70</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11213436,0" class="icommentjs icon16" href="/a-glossary-of-corpus-linguistics-glossaries-in-linguistics-eup-true-pdf-prg-pdf-t11213436.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType pdfType">
                    <a href="/a-glossary-of-corpus-linguistics-glossaries-in-linguistics-eup-true-pdf-prg-pdf-t11213436.html" class="cellMainLink">A Glossary of Corpus Linguistics (Glossaries in Linguistics EUP) True PDF {PRG}.pdf</a></div>
			</td>
			<td class="nobr center" data-sort="1052151">1 <span>MB</span></td>
			<td class="center">1</td>
			<td class="center"><span title="06 Sep 2015, 19:15">1&nbsp;day</span></td>
			<td class="green center">0</td>
			<td class="red lasttd center">67</td>
        </tr>
			</table>

<h2><a class="plain" href="/lossless/">Lossless Music Torrents</a> <a class="ka ka16 ka-rss normalText rsssign ka-red" target="_blank" href="/lossless/?rss=1" title="category feed"></a></h2>
<div>
	<table cellpadding="0" cellspacing="0" class="data frontPageWidget" style="width: 100%">
		<tr class="firstr">
			<th class="width100perc nopad"><a class="fpw-sort">torrent name</a></th>
			<th class="center"><a class="fpw-sort">size</a></th>
			<th class="center"><a class="fpw-sort">files</a></th>
			<th class="center"><a class="fpw-sort">age</a></th>
			<th class="center"><a class="fpw-sort">seed</a></th>
			<th class="lasttd nobr center"><a class="fpw-sort">leech</a></th>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11202918,0" class="icommentjs icon16" href="/va-cuban-cafÃ©-3-cd-boxed-set-flac-t11202918.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-cuban-cafÃ©-3-cd-boxed-set-flac-t11202918.html" class="cellMainLink">VA - Cuban CafÃ© 3 CD Boxed Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1149776070">1.07 <span>GB</span></td>
			<td class="center">33</td>
			<td class="center"><span title="04 Sep 2015, 18:35">3&nbsp;days</span></td>
			<td class="green center">179</td>
			<td class="red lasttd center">75</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11200639,0" class="icommentjs icon16" href="/va-arabic-cafÃ©-3-cd-boxed-set-flac-t11200639.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-arabic-cafÃ©-3-cd-boxed-set-flac-t11200639.html" class="cellMainLink">VA - Arabic CafÃ© 3 CD Boxed Set [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1017882227">970.73 <span>MB</span></td>
			<td class="center">33</td>
			<td class="center"><span title="04 Sep 2015, 08:10">3&nbsp;days</span></td>
			<td class="green center">157</td>
			<td class="red lasttd center">61</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11208584,0" class="icommentjs icon16" href="/iron-maiden-2015-the-book-of-souls-deluxe-edition-2cd-eac-flac-t11208584.html#comment"> <em class="iconvalue">6</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/iron-maiden-2015-the-book-of-souls-deluxe-edition-2cd-eac-flac-t11208584.html" class="cellMainLink">Iron Maiden 2015 - The Book of Souls (Deluxe Edition 2CD) (EAC-FLAC)</a></div>
			</td>
			<td class="nobr center" data-sort="881742067">840.89 <span>MB</span></td>
			<td class="center">28</td>
			<td class="center"><span title="05 Sep 2015, 19:58">2&nbsp;days</span></td>
			<td class="green center">161</td>
			<td class="red lasttd center">42</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11208071,0" class="icommentjs icon16" href="/va-jazz-greatest-2005-flac-tfm-t11208071.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-jazz-greatest-2005-flac-tfm-t11208071.html" class="cellMainLink">VA - Jazz Greatest - (2005) - [FLAC] - [TFM]</a></div>
			</td>
			<td class="nobr center" data-sort="238718819">227.66 <span>MB</span></td>
			<td class="center">25</td>
			<td class="center"><span title="05 Sep 2015, 18:00">2&nbsp;days</span></td>
			<td class="green center">126</td>
			<td class="red lasttd center">20</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11199937,0" class="icommentjs icon16" href="/5cd-music-sampler-great-obscure-music-from-obscure-artists-flac-metadata-reseed-t11199937.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/5cd-music-sampler-great-obscure-music-from-obscure-artists-flac-metadata-reseed-t11199937.html" class="cellMainLink">5CD Music Sampler-great obscure music from obscure artists FLAC, metadata reseed</a></div>
			</td>
			<td class="nobr center" data-sort="2615278504">2.44 <span>GB</span></td>
			<td class="center">98</td>
			<td class="center"><span title="04 Sep 2015, 04:47">3&nbsp;days</span></td>
			<td class="green center">93</td>
			<td class="red lasttd center">46</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11208597,0" class="icommentjs icon16" href="/rock-collection-1984-flac-t11208597.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rock-collection-1984-flac-t11208597.html" class="cellMainLink">Rock Collection 1984 (FLAC)</a></div>
			</td>
			<td class="nobr center" data-sort="9918392966">9.24 <span>GB</span></td>
			<td class="center">310</td>
			<td class="center"><span title="05 Sep 2015, 20:01">2&nbsp;days</span></td>
			<td class="green center">40</td>
			<td class="red lasttd center">92</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11200377,0" class="icommentjs icon16" href="/miles-davis-at-newport-1955-1975-the-bootleg-series-vol-4-2015-flac-t11200377.html#comment"> <em class="iconvalue">5</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/miles-davis-at-newport-1955-1975-the-bootleg-series-vol-4-2015-flac-t11200377.html" class="cellMainLink">Miles Davis - At Newport 1955-1975 - The Bootleg Series Vol. 4 (2015) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1905304665">1.77 <span>GB</span></td>
			<td class="center">107</td>
			<td class="center"><span title="04 Sep 2015, 06:58">3&nbsp;days</span></td>
			<td class="green center">95</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11203397,0" class="icommentjs icon16" href="/va-one-world-a-musical-journey-around-the-globe-double-cd-flac-t11203397.html#comment"> <em class="iconvalue">4</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-one-world-a-musical-journey-around-the-globe-double-cd-flac-t11203397.html" class="cellMainLink">VA - One World A Musical Journey Around the Globe Double CD [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1101572059">1.03 <span>GB</span></td>
			<td class="center">41</td>
			<td class="center"><span title="04 Sep 2015, 20:44">3&nbsp;days</span></td>
			<td class="green center">90</td>
			<td class="red lasttd center">30</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11208608,0" class="icommentjs icon16" href="/falco-original-album-classics-5-cd-box-set-2015-flac-naftamusic-t11208608.html#comment"> <em class="iconvalue">3</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/falco-original-album-classics-5-cd-box-set-2015-flac-naftamusic-t11208608.html" class="cellMainLink">Falco - Original Album Classics (5 CD Box Set 2015) [FLAC] - Naftamusic</a></div>
			</td>
			<td class="nobr center" data-sort="2051663365">1.91 <span>GB</span></td>
			<td class="center">100</td>
			<td class="center"><span title="05 Sep 2015, 20:03">2&nbsp;days</span></td>
			<td class="green center">81</td>
			<td class="red lasttd center">38</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11208473,0" class="icommentjs icon16" href="/motley-crue-discography-1981-2008-t11208473.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/motley-crue-discography-1981-2008-t11208473.html" class="cellMainLink">Motley Crue - Discography 1981-2008</a></div>
			</td>
			<td class="nobr center" data-sort="18585088691">17.31 <span>GB</span></td>
			<td class="center">141</td>
			<td class="center"><span title="05 Sep 2015, 19:35">2&nbsp;days</span></td>
			<td class="green center">16</td>
			<td class="red lasttd center">89</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11202217,0" class="icommentjs icon16" href="/the-band-the-band-2014-24-192-hd-flac-t11202217.html#comment"> <em class="iconvalue">7</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/the-band-the-band-2014-24-192-hd-flac-t11202217.html" class="cellMainLink">The Band - The Band (2014) [24-192 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1700629936">1.58 <span>GB</span></td>
			<td class="center">30</td>
			<td class="center"><span title="04 Sep 2015, 15:01">3&nbsp;days</span></td>
			<td class="green center">72</td>
			<td class="red lasttd center">29</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11210496,0" class="icommentjs icon16" href="/rolling-stones-hot-rocks-2005-24-88-hd-flac-t11210496.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/rolling-stones-hot-rocks-2005-24-88-hd-flac-t11210496.html" class="cellMainLink">Rolling Stones - Hot Rocks (2005) [24-88 HD FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="1647925627">1.53 <span>GB</span></td>
			<td class="center">58</td>
			<td class="center"><span title="06 Sep 2015, 06:06">1&nbsp;day</span></td>
			<td class="green center">55</td>
			<td class="red lasttd center">25</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright"> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-american-anthems-2010-3cd-flac-soup-t11213308.html" class="cellMainLink">VA - American Anthems (2010) 3CD FLAC Soup</a></div>
			</td>
			<td class="nobr center" data-sort="1652959798">1.54 <span>GB</span></td>
			<td class="center">82</td>
			<td class="center"><span title="06 Sep 2015, 18:44">1&nbsp;day</span></td>
			<td class="green center">46</td>
			<td class="red lasttd center">26</td>
        </tr>
				<tr class="odd">
			<td>
                <div class="iaconbox floatright">  <a rel="11213065,0" class="icommentjs icon16" href="/va-cafÃ©-do-brasil-a-pure-blend-of-cool-brazilian-music-flac-t11213065.html#comment"> <em class="iconvalue">2</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-cafÃ©-do-brasil-a-pure-blend-of-cool-brazilian-music-flac-t11213065.html" class="cellMainLink">VA - CafÃ© Do Brasil (A Pure Blend of Cool Brazilian Music) [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="514002246">490.19 <span>MB</span></td>
			<td class="center">15</td>
			<td class="center"><span title="06 Sep 2015, 17:21">1&nbsp;day</span></td>
			<td class="green center">41</td>
			<td class="red lasttd center">15</td>
        </tr>
				<tr class="even">
			<td>
                <div class="iaconbox floatright">  <a rel="11213181,0" class="icommentjs icon16" href="/va-celtic-heart-flac-t11213181.html#comment"> <em class="iconvalue">1</em><i class="ka ka-comment"></i></a> 					
				</div>
				<div class="markeredBlock torType musicType">
                    <a href="/va-celtic-heart-flac-t11213181.html" class="cellMainLink">VA - Celtic Heart [FLAC]</a></div>
			</td>
			<td class="nobr center" data-sort="298634689">284.8 <span>MB</span></td>
			<td class="center">17</td>
			<td class="center"><span title="06 Sep 2015, 17:58">1&nbsp;day</span></td>
			<td class="green center">25</td>
			<td class="red lasttd center">16</td>
        </tr>
			</table>


	<script type="text/javascript">
		if ($.cookie('kat_settings[fpw_column]') != null && $.cookie('kat_settings[fpw_order]')) {
			var colIndex = $.cookie('kat_settings[fpw_column]')
			var ascend = $.cookie('kat_settings[fpw_order]');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			sortTables(colIndex, ascend);
		}
		$('.frontPageWidget .firstr th .fpw-sort').click(function() {
			var colIndex = $(this).parent().index();
			var ascend = !$(this).hasClass('sortedAsc')?1:0;
			$('.fpw-sort').removeClass('sortedAsc sortedDesc');
			$('.frontPageWidget').each(function() {
				$(this).find('.firstr th .fpw-sort:eq('+colIndex+')').addClass((ascend ? 'sortedAsc' : 'sortedDesc'));
			});
			updateFPWsettings(colIndex, ascend);
			sortTables(colIndex, ascend);
		});
		function sortTables(colIndex, ascend) {	
			$('.frontPageWidget').each(function() {
				var data = [];
				$('tr:not(.firstr)', $(this)).each(function() {
					var name = $('td:first .cellMainLink', $(this)).text();
					var id = $('td:first .cellMainLink', $(this)).attr('href').match(/.*-t(\d+)\.html/)[1];
					var fileSize = $('td:eq(1)', $(this)).attr('data-sort');
					data.push({
						'col0':name, 'col1':padNum(fileSize), 'col2':padNum($('td:eq(2)',$(this)).text()), 'col3':padNum(id), 'col4':padNum($('td:eq(4)',$(this)).text()), 'col5':padNum($('td:eq(5)',$(this)).text()), 'html':$(this).html()
					});
				});
				data.sort(function(a, b) {
					var x = a['col'+colIndex].toLowerCase(); var y = b['col'+colIndex].toLowerCase();
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				});
				if (!ascend||ascend=='0') data.reverse();
				$(this).find('tr:not(.firstr)').remove();
				for (var i=0;i<data.length;i++) {
					$(this).append('<tr class="'+(i+1%2==0?'even':'odd')+'">'+data[i].html+'</tr>');
				}
			});
		}
		function padNum(val) {
			val = '0000000000000000'+val;
			return val.substring(val.length-15);
		}
		function updateFPWsettings(t, o) {
			$.cookie('kat_settings[fpw_column]', t, { expires: 365, path: '/' });
			$.cookie('kat_settings[fpw_order]', o, { expires: 365, path: '/' });
		}
	</script>
		</td>
		<td class="sidebarCell">
			
<div id="sidebar" >
    
            
        <div id="_119b0a17fab5493361a252d04bf527db"></div>
    
                
    	    <div class="advertising">
    <div class="legend">Advertising (<a href="/auth/login/register/" class="ajaxLink removeAdv" title="Login or register to remove advertising">remove</a>)</div>
    <div id="_7063408f1c01d50e0dc2d833186ce962"></div>
</div>

    
        <div class="sliderbox">
<h3><a href="/community/">Latest Forum Threads</a><i id="hideLatestThreads" style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestForum" rel="latestForum" class="showBlockJS">
		<li>
		<a href="/community/show/quiz-can-you-guess-movie-v13/?unread=16869898">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Quiz; Can you guess this movie? V13
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/titovicanjoey/">titovicanjoey</a></span></span> <span title="07 Sep 2015, 23:33">1&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/activity/?unread=16869897">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Activity
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/TrenxT/">TrenxT</a></span></span> <span title="07 Sep 2015, 23:33">1&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/what-are-you-listening-right-now-v4-thread-108003/?unread=16869896">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				What are you listening to right now? V4
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/OvOvO/">OvOvO</a></span></span> <span title="07 Sep 2015, 23:32">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/marvel-comic-universe-fixing-seeding/?unread=16869894">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Marvel Comic Universe, Fixing seeding.
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="online" title="online"></span> <span class="aclColor_1"><a class="plain" href="/user/lefty2446/">lefty2446</a></span></span> <span title="07 Sep 2015, 23:32">2&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/lyrical-duels/?unread=16869891">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Lyrical Duels :
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_verified"><a class="plain" href="/user/ehhhhtozebec/">ehhhhtozebec</a></span></span> <span title="07 Sep 2015, 23:31">3&nbsp;min.&nbsp;ago</span></span>
	</li>
		<li>
		<a href="/community/show/kickass-anime-community-v-6/?unread=16869889">
			<i class="ka ka16 ka-community latest-icon"></i>
			<p class="latest-title">
				Kickass Anime Community V.6!
			</p>
		</a>
		<span class="explanation">by <span class="badgeInline"><span class="offline" title="offline"></span> <span class="aclColor_1"><a class="plain" href="/user/jeffcosta123/">jeffcosta123</a></span></span> <span title="07 Sep 2015, 23:30">4&nbsp;min.&nbsp;ago</span></span>
	</li>
	</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3><a href="/blog/">Latest News</a><i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestNews" rel="latestNews" class="showBlockJS">
	<li>
		<a href="/blog/post/summer-updates-september-1/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Summer updates [September, 1]
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="01 Sep 2015, 16:13">6&nbsp;days&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/kickasstorrents-is-moving-to-kat-cr-domain/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				KickassTorrents is moving to kat.cr domain
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="24 Apr 2015, 10:13">4&nbsp;months&nbsp;ago</span></span>
	</li>
	<li>
		<a href="/blog/post/happy-torrents-day-2015/">
			<i class="ka ka16 ka-rss latest-icon"></i>
			<p class="latest-title">
				Happy Torrents Day 2015!
			</p>
		</a>
		<span class="explanation">by KickassTorrents <span title="30 Mar 2015, 10:06">5&nbsp;months&nbsp;ago</span></span>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
<div class="sliderbox">
<h3>Blogroll<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="blogroll" rel="blogroll" class="showBlockJS">
	<li><a href="/blog/eclipze_angel/post/a-poem-by-yivlx/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> A Poem by yivlx..</p></a><span class="explanation">by <a class="plain aclColor_1" href="/user/eclipze_angel/">eclipze_angel</a> <span title="07 Sep 2015, 20:42">2&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/olderthangod/post/some-info-about-me/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Some info about me...</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/olderthangod/">olderthangod</a> <span title="07 Sep 2015, 19:41">3&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/magicpotions/post/sojourner-by-watertiger/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Sojourner by watertiger</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/magicpotions/">magicpotions</a> <span title="07 Sep 2015, 15:21">8&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/floofiness/post/beginning/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> Beginning</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/floofiness/">floofiness</a> <span title="07 Sep 2015, 14:09">9&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/HuLkWaR/post/first-words-from-a-former-mute/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> First Words From A Former Mute</p></a><span class="explanation">by <a class="plain aclColor_4" href="/user/HuLkWaR/">HuLkWaR</a> <span title="07 Sep 2015, 12:51">10&nbsp;hours&nbsp;ago</span></span></li>
	<li><a href="/blog/ScribeOfGoD/post/evil-overlord/"><i class="ka ka16 ka-rss latest-icon"></i><p class="latest-title"> EVIL OVERLORD</p></a><span class="explanation">by <a class="plain aclColor_2" href="/user/ScribeOfGoD/">ScribeOfGoD</a> <span title="07 Sep 2015, 02:03">21&nbsp;hours&nbsp;ago</span></span></li>
</ul>
</div><!-- div class="sliderbox" -->

    <div class="sliderbox">
<h3>Goodies<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="goodies" rel="goodies" class="showBlockJS">

	<li>
		<a data-nop target="_blank" rel="external" href="http://addons.mozilla.org/en-US/firefox/addon/11412" target="_blank" rel="external">
			<span class="ifirefox thirdPartIcons"></span>Firefox search plugin
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/content/utorrent.btsearch">
			<span class="iutorrent thirdPartIcons"></span>uTorrent search template
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://twitter.com/kickasstorrents">
			<span class="ifollow thirdPartIcons"></span>Follow us on Twitter
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="/blog/post/30/">
			<span class="ikat thirdPartIcons"></span>Kickass wallpapers
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external" href="http://www.facebook.com/official.KAT.fanclub">
			<span class="ifacebook thirdPartIcons"></span>Like us on Facebook
		</a>
	</li>
	<li>
		<a data-nop target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph"><span class="iirc thirdPartIcons"></span>IRC official chat</a>
	</li>
</ul>
</div><!-- div class="sliderbox" -->
    <div class="sliderbox">
<h3>Latest Searches<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
<ul id="latestSearches" rel="latestSearches" class="showBlockJS">
	<li>
		<a href="/search/wtfpod/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				wtfpod
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/daz%20tos/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				daz tos
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/graham%20norton%20show%20s16e17%20hdtv/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				graham norton show s16e17 hdtv
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/computeractive%20uk%20best/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				computeractive uk best
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20godfather%201080/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				the godfather 1080
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/gta/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				gta
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/the%20visitor/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				The visitor
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/nashville%20pussy/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				nashville pussy
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/rogue/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				rogue
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/parody/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				parody
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

	<li>
		<a href="/search/forever/">
			<i class="ka ka16 ka-zoom latest-icon"></i>
			<p class="latest-title">
				forever
			</p>
		</a>
				<span class="explanation">just&nbsp;now</span>
	</li>

</ul>
</div><!-- div class="sliderbox" -->
        	<div class="sliderbox">
	<h3>Friends Links<i style="float:right; margin-top:-3px;" class="ka ka16 ka-arrow2-up foldClose"></i></h3>
    <ul id="friendsLinks" rel="friendsLinks" class="showBlockJS">
		
		<li>
			<a data-nop href="http://torrents.to/" target="_blank" rel="external">
				<span class="itorrentsto thirdPartIcons"></span>Torrents.to
			</a>
		</li>
		<li>
			<a data-nop href="http://www.torrentdownloads.net/" target="_blank" rel="external">
				<span class="itorrentdownloads thirdPartIcons"></span>Torrent Downloads
			</a>
		</li>
		
		<li>
			<a data-nop href="http://www.torrentreactor.net/" target="_blank" rel="external">
				<span class="itorreact thirdPartIcons"></span>TorrentReactor
			</a>
		</li>
		

		<li>
			<a data-nop href="http://torrent-finder.info/" target="_blank" rel="external">
				<span class="itorrentfinder thirdPartIcons"></span>Torrent Finder
			</a>
		</li>
	</ul>
</div><!-- div class="sliderbox" -->
        
</div>
<a class="showSidebar" id="showsidebar" onclick="showSidebar();" style="display:none;"></a>

		</td>
	</tr>
</table>
</div>
<div id="translate_site" style="display:none">
    <h3>Select Your Language</h3>
    <div class="textcontent">
        <div style="-moz-column-width: 12em; -moz-columns: 12em; -webkit-columns: 12em; columns:12em;">
            <ul>
                                <li class="current_lang"><a onclick="setLanguage('en', '.kat.cr');return false;" class="plain"><strong>English</strong></a></li>
                                <li><a onclick="setLanguage('af', '.kat.cr');return false;" class="plain">Afrikaans</a></li>
                                <li><a onclick="setLanguage('al', '.kat.cr');return false;" class="plain">Albanian</a></li>
                                <li><a onclick="setLanguage('ar', '.kat.cr');return false;" class="plain">Arabic (Modern)</a></li>
                                <li><a onclick="setLanguage('eu', '.kat.cr');return false;" class="plain">Basque</a></li>
                                <li><a onclick="setLanguage('bn', '.kat.cr');return false;" class="plain">Bengali</a></li>
                                <li><a onclick="setLanguage('bs', '.kat.cr');return false;" class="plain">Bosnian</a></li>
                                <li><a onclick="setLanguage('br', '.kat.cr');return false;" class="plain">Brazilian Portuguese</a></li>
                                <li><a onclick="setLanguage('bg', '.kat.cr');return false;" class="plain">Bulgarian</a></li>
                                <li><a onclick="setLanguage('ch', '.kat.cr');return false;" class="plain">Chinese Simplified</a></li>
                                <li><a onclick="setLanguage('tw', '.kat.cr');return false;" class="plain">Chinese Traditional</a></li>
                                <li><a onclick="setLanguage('hr', '.kat.cr');return false;" class="plain">Croatian</a></li>
                                <li><a onclick="setLanguage('cz', '.kat.cr');return false;" class="plain">Czech</a></li>
                                <li><a onclick="setLanguage('da', '.kat.cr');return false;" class="plain">Danish</a></li>
                                <li><a onclick="setLanguage('nl', '.kat.cr');return false;" class="plain">Dutch</a></li>
                                <li><a onclick="setLanguage('tl', '.kat.cr');return false;" class="plain">Filipino</a></li>
                                <li><a onclick="setLanguage('fi', '.kat.cr');return false;" class="plain">Finnish</a></li>
                                <li><a onclick="setLanguage('fr', '.kat.cr');return false;" class="plain">French</a></li>
                                <li><a onclick="setLanguage('ka', '.kat.cr');return false;" class="plain">Georgian</a></li>
                                <li><a onclick="setLanguage('de', '.kat.cr');return false;" class="plain">German</a></li>
                                <li><a onclick="setLanguage('el', '.kat.cr');return false;" class="plain">Greek</a></li>
                                <li><a onclick="setLanguage('hi', '.kat.cr');return false;" class="plain">Hindi</a></li>
                                <li><a onclick="setLanguage('hu', '.kat.cr');return false;" class="plain">Hungarian</a></li>
                                <li><a onclick="setLanguage('id', '.kat.cr');return false;" class="plain">Indonesian</a></li>
                                <li><a onclick="setLanguage('it', '.kat.cr');return false;" class="plain">Italian</a></li>
                                <li><a onclick="setLanguage('kn', '.kat.cr');return false;" class="plain">Kannada</a></li>
                                <li><a onclick="setLanguage('ko', '.kat.cr');return false;" class="plain">Korean</a></li>
                                <li><a onclick="setLanguage('lt', '.kat.cr');return false;" class="plain">Lithuanian</a></li>
                                <li><a onclick="setLanguage('mk', '.kat.cr');return false;" class="plain">Macedonian</a></li>
                                <li><a onclick="setLanguage('ml', '.kat.cr');return false;" class="plain">Malayalam</a></li>
                                <li><a onclick="setLanguage('ms', '.kat.cr');return false;" class="plain">Malaysian</a></li>
                                <li><a onclick="setLanguage('no', '.kat.cr');return false;" class="plain">Norwegian</a></li>
                                <li><a onclick="setLanguage('pr', '.kat.cr');return false;" class="plain">Pirate</a></li>
                                <li><a onclick="setLanguage('pl', '.kat.cr');return false;" class="plain">Polish</a></li>
                                <li><a onclick="setLanguage('pt', '.kat.cr');return false;" class="plain">Portuguese</a></li>
                                <li><a onclick="setLanguage('pa', '.kat.cr');return false;" class="plain">Punjabi</a></li>
                                <li><a onclick="setLanguage('ro', '.kat.cr');return false;" class="plain">Romanian</a></li>
                                <li><a onclick="setLanguage('ru', '.kat.cr');return false;" class="plain">Russian</a></li>
                                <li><a onclick="setLanguage('sr', '.kat.cr');return false;" class="plain">Serbian</a></li>
                                <li><a onclick="setLanguage('src', '.kat.cr');return false;" class="plain">Serbian-Cyrillic</a></li>
                                <li><a onclick="setLanguage('sk', '.kat.cr');return false;" class="plain">Slovak</a></li>
                                <li><a onclick="setLanguage('es', '.kat.cr');return false;" class="plain">Spanish</a></li>
                                <li><a onclick="setLanguage('sv', '.kat.cr');return false;" class="plain">Swedish</a></li>
                                <li><a onclick="setLanguage('ta', '.kat.cr');return false;" class="plain">Tamil</a></li>
                                <li><a onclick="setLanguage('te', '.kat.cr');return false;" class="plain">Telugu</a></li>
                                <li><a onclick="setLanguage('tr', '.kat.cr');return false;" class="plain">Turkish</a></li>
                                <li><a onclick="setLanguage('uk', '.kat.cr');return false;" class="plain">Ukrainian</a></li>
                                <li><a onclick="setLanguage('vi', '.kat.cr');return false;" class="plain">Vietnamese</a></li>
                            </ul>
        </div>
    </div><!-- div class="textcontent" -->
</div>
</div><!--id="main"-->
</div><!--id="wrap"-->

<footer class="lightgrey">
	<ul>
		<li><a class="plain" href="#translate_site" id="translate_link"><strong>change language</strong></a></li>
		<li><a href="/rules/" class="lower">rules</a></li>
        <li><a href="/ideabox/">idea box</a></li>
		<li class="lower"><a href="/achievements/">Achievements</a></li>
		<li><a href="/trends/">trends</a></li>
		<li class="lower"><a href="/latest-searches/">Latest Searches</a></li>
        <li><a href="/request/">torrent requests</a></li>        	</ul>
	<ul>
		<li><a href="/about/">about</a></li>
		<li><a href="/welcome/">welcome</a></li>
		<li><a href="/privacy/">privacy</a></li>
		<li><a href="/dmca/">dmca</a></li>
        		<li><a href="/logos/">logos</a></li>
				<li><a href="/contacts/">contacts</a></li>
        <li><a href="/api/">api</a></li>
		<li><a target="_blank" rel="external nofollow" href="http://chat.efnet.org:9090/?channels=%23KAT.ph">chat</a></li>
	</ul>
        </footer>
<a class="feedbackButton eventsButtons" href="/issue/create/" id="feedback"><span>Report a bug</span></a>
    <div id="_673e31f53f8166159b8e996c4124765b"></div>
        <div id="_e7050fb15fd39b3e4e99a5be4a57b6ea"></div>
<script type="text/javascript" charset="utf-8">$(document).ready(function() {  $("#contentSearch").focus(); });</script><script>
 sc('addGlobal', 'pagetype', 'front');
 sc('addSlot', '_60318cd4e8d28f6fb76fe34e9bd9c498');
sc('addSlot', '_39ecb76dd457e5ac33776fdf11500d56');
sc('addSlot', '_277923e5f9d753c5b0630c28e641790c');
sc('addSlot', '_119b0a17fab5493361a252d04bf527db');
sc('addSlot', '_7063408f1c01d50e0dc2d833186ce962', { 'searchQuery': '' });
sc('addSlot', '_673e31f53f8166159b8e996c4124765b');
sc('addSlot', '_e7050fb15fd39b3e4e99a5be4a57b6ea');
</script>
<script type="text/javascript"><!--
document.write("<a style='display:none;' href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='0' height='0'><\/a>")
//--></script>

</body>
</html>
