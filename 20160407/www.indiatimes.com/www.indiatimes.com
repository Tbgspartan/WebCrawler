<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=119" media="screen"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=119" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=119"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=119"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');fbq("track","ViewContent");</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=119"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0;
var isNewYear = 1;
</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                <a href="http://www.indiatimes.com/news/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/health/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Health');">Health</a> 
                                                               <a href="http://www.indiatimes.com/play/"  onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Slog Overs');">Slog Overs</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  class="#fffff" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Trending');">Trending</a> 
                                                            <a href='http://www.indiatimes.com/panama-papers/'  class="#fffff" onmousedown="ga('send', 'event', 'HeaderEvents', 'TopNavigation', 'Panama Papers');">Panama Papers</a> 
                                                </div>
                </div>
                    <div class='onScrolled'> 
                        <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                        <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                        <div class="has-tag">
                                                                <a href='http://www.indiatimes.com/panama-papers/'  class="#fffff" >Panama Papers</a> 
                                </div>
                    </div>

            
            </nav>
            
            <div id="sticker" style="display:none;"><span class="readTitle">
                                    </span></div>
                <div class="socls fr share">
                </div>


            <div class="social fr">
                <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
            </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if (this.value == 'Search') {
                        this.value = ''
                    }" onblur="if (this.value == '') {
                                this.value = 'Search'
                            }
                            ;" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data();" id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
					<a href="javascript:void(0)" class="arrow sprite ">
					</a>                </dt>
                                        <dd >
                     
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                            </dd>
                    <div class="clr"></div>
                    <dt  data-color="yellow-bg" ><a href="http://www.indiatimes.com/play/">Slog Overs</a>
					                </dt>
                                <dt data-color="orange-bg" class="">
                <a href="http://www.indiatimes.com/videocafe/">Videos</a>
            
            </dt>
            <dt data-color="" class="">
                <a href="http://www.indiatimes.com/trending/">Trending</a>
            </dt>            
            <dt class="pink-bg">
                <div class="follow">Follow indiatimes </div>
                <div class="follow_cont"> 
                    <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                    <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
                </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
                            <div id="OOP_Inter" style="display: none;">
                        <script type='text/javascript'>
                            googletag.cmd.push(function () {
                                googletag.display('OOP_Inter'); });
                        </script>
                    </div>
                    <div id="PPD" style="display: none;">
                        <script type='text/javascript'>
                            googletag.cmd.push(function () {
                                googletag.display('PPD');
                            });
                        </script>
                    </div>
            
    </div><!--pull-ad end-->
</div>
    <!--testing 16-04-07 22:40:03-->

<section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
    
<div class="big-image">
        	<div class="gradient-b"></div>
		<a href="http://www.indiatimes.com/play/slog-overs/ipl-9-here-s-how-cricket-stadiums-across-the-world-conserve-water-what-drought-hit-maharashtra-can-learn-from-them-253107.html" class="gradient-patten"></a>
            <div class="featureds"><span class="border-left">&nbsp;</span> Featured <span class="border-right">&nbsp;</span></div>
            <div class="bid-card-txt">
                                <a href="http://www.indiatimes.com/play/slog-overs/ipl-9-here-s-how-cricket-stadiums-across-the-world-conserve-water-what-drought-hit-maharashtra-can-learn-from-them-253107.html" class="big-card-small">
                    IPL 9: Here's How Cricket Stadiums Across The World Conserve Water & What Drought-Hit Maharashtra Can Learn From Them!                </a>
              <span class="card-line"></span>  
            </div>
            <a href="http://www.indiatimes.com/play/slog-overs/ipl-9-here-s-how-cricket-stadiums-across-the-world-conserve-water-what-drought-hit-maharashtra-can-learn-from-them-253107.html" class="tint"><img src="http://media.indiatimes.in/media/content/2016/Apr/kohlirainiplbccl_1460031876_1024x477.jpg"/></a>
</div>
    
        

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

        <div class="feature-list cf"><!--feature-list start-->	
                            <figure>

                        <a href="http://www.indiatimes.com/news/india/court-to-look-into-whether-pm-modi-insulted-the-national-flag-during-yoga-day-event-253074.html" class=" tint" title="Court To Look Into Whether PM Modi Insulted The National Flag During Yoga Day Event">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/rtx1hfgc-640_1460007633_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/rtx1hfgc-640_1460007633_236x111.jpg"  border="0" alt="Court To Look Into Whether PM Modi Insulted The National Flag During Yoga Day Event"/></a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/court-to-look-into-whether-pm-modi-insulted-the-national-flag-during-yoga-day-event-253074.html" title="Court To Look Into Whether PM Modi Insulted The National Flag During Yoga Day Event">
    Court To Look Into Whether PM Modi Insulted The National Flag During Yoga Day Event                    </a>
                </figcaption>
                </div><!--feature-list end-->

                    <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/world/the-rare-sumatran-rhino-has-died-within-weeks-of-discovery-now-there-s-less-than-100-left-253052.html" title="The Rare Sumatran Rhino Has Died Within Weeks Of Discovery. Now, There's Less Than 100 Left!" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Apr/main_rhinosdotorgcard_1459941073_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/main_rhinosdotorgcard_1459941073_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/the-rare-sumatran-rhino-has-died-within-weeks-of-discovery-now-there-s-less-than-100-left-253052.html" title="The Rare Sumatran Rhino Has Died Within Weeks Of Discovery. Now, There's Less Than 100 Left!">
    The Rare Sumatran Rhino Has Died Within Weeks Of Discovery. Now, There's Less Than 100 Left!                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/india/meet-the-master-spy-rn-kao-who-is-the-founding-father-of-indian-intelligence-agency-raw-253136.html" title="Meet The Master Spy Who Is The Founding Father Of Indian Intelligence Agency âRAWâ" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Apr/kao_1460035658_1460035671_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/kao_1460035658_1460035671_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/meet-the-master-spy-rn-kao-who-is-the-founding-father-of-indian-intelligence-agency-raw-253136.html" title="Meet The Master Spy Who Is The Founding Father Of Indian Intelligence Agency âRAWâ">
    Meet The Master Spy Who Is The Founding Father Of Indian Intelligence Agency âRAWâ                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
                    <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                <figure>
                        
                        <a href="http://www.indiatimes.com/news/world/10-images-of-the-earth-taken-from-space-that-prove-just-how-mesmerizing-our-planet-is-253132.html" title="10 Images Of The Earth Taken From Space That Prove Just How Mesmerizing Our Planet Is" class=" tint">                       
                        <img src="http://media.indiatimes.in/media/content/2016/Apr/600_1460031633_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/600_1460031633_236x111.jpg" border="0" alt=""/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/10-images-of-the-earth-taken-from-space-that-prove-just-how-mesmerizing-our-planet-is-253132.html" title="10 Images Of The Earth Taken From Space That Prove Just How Mesmerizing Our Planet Is">
    10 Images Of The Earth Taken From Space That Prove Just How Mesmerizing Our Planet Is                    </a>
                </figcaption> 
            </div><!--news-panel-list end-->
            </div><!--news-panel end-->

    <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>

                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/indonesian-singer-irma-bule-dies-on-stage-after-being-bitten-by-a-venomous-cobra-during-performance-253127.html'>video</a>                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/indonesian-singer-irma-bule-dies-on-stage-after-being-bitten-by-a-venomous-cobra-during-performance-253127.html" class="tint" title="Shocking! Indonesian Singer Irma Bule Dies On Stage After Being Bitten By A Venomous Cobra During Performance">
                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Apr/irmabule_card_1460030181_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2016/Apr/irmabule_card_1460030181_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/videocafe/indonesian-singer-irma-bule-dies-on-stage-after-being-bitten-by-a-venomous-cobra-during-performance-253127.html" title="Shocking! Indonesian Singer Irma Bule Dies On Stage After Being Bitten By A Venomous Cobra During Performance" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/videocafe/indonesian-singer-irma-bule-dies-on-stage-after-being-bitten-by-a-venomous-cobra-during-performance-253127.html');">

    Shocking! Indonesian Singer Irma Bule Dies On Stage After Being Bitten By A Venomous Cobra During Performance                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/watch-the-pressures-chinese-women-face-when-they-re-not-married-is-something-we-can-all-relate-to-253126.html" class="tint" title="WATCH: The Pressures Chinese Women Face When They're Not Married Is Something We Can All Relate To">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/cp3_1460029360_1460029363_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/cp3_1460029360_1460029363_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/watch-the-pressures-chinese-women-face-when-they-re-not-married-is-something-we-can-all-relate-to-253126.html" title="WATCH: The Pressures Chinese Women Face When They're Not Married Is Something We Can All Relate To" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/culture/who-we-are/watch-the-pressures-chinese-women-face-when-they-re-not-married-is-something-we-can-all-relate-to-253126.html');">

    WATCH: The Pressures Chinese Women Face When They're Not Married Is Something We Can All Relate To                        </a>
                    </div>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-navy-guy-shut-stand-up-comedians-up-and-stole-their-own-show-by-proposing-to-his-girlfriend-253121.html'>video</a>                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-navy-guy-shut-stand-up-comedians-up-and-stole-their-own-show-by-proposing-to-his-girlfriend-253121.html" class="tint" title="This Navy Guy Shut Stand-Up Comedians Up And Stole Their Own Show By Proposing To His Girlfriend!">
                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Apr/navyguy_card_1460026888_502x234.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2016/Apr/navyguy_card_1460026888_502x234.jpg"  border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/videocafe/this-navy-guy-shut-stand-up-comedians-up-and-stole-their-own-show-by-proposing-to-his-girlfriend-253121.html" title="This Navy Guy Shut Stand-Up Comedians Up And Stole Their Own Show By Proposing To His Girlfriend!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories', 'http://www.indiatimes.com/videocafe/this-navy-guy-shut-stand-up-comedians-up-and-stole-their-own-show-by-proposing-to-his-girlfriend-253121.html');">

    This Navy Guy Shut Stand-Up Comedians Up And Stole Their Own Show By Proposing To His Girlfriend!                        </a>
                    </div>
                </figcaption>
            </div>
                    
    </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
                    
                        <div class="trending-panel-list cf" id="column3_0">
    <span class="strip skyblue-bg"></span>                <figure>
                        <a href="http://www.indiatimes.com/play/slog-overs/top-ten-quotes-on-mahendra-singh-dhoni-by-those-who-played-under-him-for-csk-and-team-india-253124.html" class="tint" title="10 Quotes By MS Dhoni's Teammates That Show He's The Best Captain Of Our Times">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/big640_1460031863_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/big640_1460031863_236x111.jpg" border="0" alt="10 Quotes By MS Dhoni's Teammates That Show He's The Best Captain Of Our Times"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/top-ten-quotes-on-mahendra-singh-dhoni-by-those-who-played-under-him-for-csk-and-team-india-253124.html" title="10 Quotes By MS Dhoni's Teammates That Show He's The Best Captain Of Our Times " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    10 Quotes By MS Dhoni's Teammates That Show He's The Best Captain Of Our Times                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_1">
                    <figure>
                        <a href="http://www.indiatimes.com/entertainment/celebs/putting-all-break-up-rumors-to-rest-virat-kohli-anushka-sharma-went-on-a-dinner-date-last-night-253071.html" class="tint" title="Putting All Break-Up Rumors To Rest, Virat & Anushka Went On A Dinner Date Last Night!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/aa_1460012074_1460012082_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/aa_1460012074_1460012082_236x111.jpg" border="0" alt="Putting All Break-Up Rumors To Rest, Virat & Anushka Went On A Dinner Date Last Night!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/putting-all-break-up-rumors-to-rest-virat-kohli-anushka-sharma-went-on-a-dinner-date-last-night-253071.html" title="Putting All Break-Up Rumors To Rest, Virat & Anushka Went On A Dinner Date Last Night! " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    Putting All Break-Up Rumors To Rest, Virat & Anushka Went On A Dinner Date Last Night!                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_2">
                    <figure>
                        <a href="http://www.indiatimes.com/entertainment/celebs/akshay-kumar-detained-at-london-heathrow-airport-for-travelling-without-a-valid-visa-253090.html" class="tint" title="Akshay Kumar Detained At Heathrow Airport For Travelling Without A Valid Visa">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/akku-card_1460012885_1460012888_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/akku-card_1460012885_1460012888_236x111.jpg" border="0" alt="Akshay Kumar Detained At Heathrow Airport For Travelling Without A Valid Visa"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/akshay-kumar-detained-at-london-heathrow-airport-for-travelling-without-a-valid-visa-253090.html" title="Akshay Kumar Detained At Heathrow Airport For Travelling Without A Valid Visa " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    Akshay Kumar Detained At Heathrow Airport For Travelling Without A Valid Visa                    </a>
                   </div>     
                </figcaption>
            </div>
                <div class="trending-panel-list cf" id="column3_3">
                    <figure>
                        <a href="http://www.indiatimes.com/culture/food/this-indian-whisky-brand-is-named-one-of-the-world-s-best-but-we-bet-you-haven-t-had-a-peg-yet-253057.html" class="tint" title="This Indian Whisky Brand Is Named One Of The Worldâs Best But We Bet You Havenât Had A Peg Yet">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/asds_1459943910_1459943920_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2016/Apr/asds_1459943910_1459943920_236x111.jpg" border="0" alt="This Indian Whisky Brand Is Named One Of The Worldâs Best But We Bet You Havenât Had A Peg Yet"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/food/this-indian-whisky-brand-is-named-one-of-the-world-s-best-but-we-bet-you-haven-t-had-a-peg-yet-253057.html" title="This Indian Whisky Brand Is Named One Of The Worldâs Best But We Bet You Havenât Had A Peg Yet " onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','');">
    This Indian Whisky Brand Is Named One Of The Worldâs Best But We Bet You Havenât Had A Peg Yet                    </a>
                   </div>     
                </figcaption>
            </div>
        </div><!--trending-panel end-->
</section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
    <div id="bigAd1_slot"></div>
    <script>
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

<section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>
                    <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/law-college-professor-makes-sexist-comment-on-female-student-s-dress-entire-class-wear-shorts-in-protest-253134.html" title="Law College Professor Makes Sexist Comment On Female Student's Dress, Entire Class Wear Shorts In Protest" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/girls640_1460033719_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/girls640_1460033719_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/law-college-professor-makes-sexist-comment-on-female-student-s-dress-entire-class-wear-shorts-in-protest-253134.html" title="Law College Professor Makes Sexist Comment On Female Student's Dress, Entire Class Wear Shorts In Protest">
    Law College Professor Makes Sexist Comment On Female Student's Dress, Entire Class Wear Shorts In Protest                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-4"  data-slot="129061" data-position="4" data-section="0" data-cb="adwidgetNew"><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/forced-to-sell-bullocks-farmer-invents-an-even-better-ploughing-machine-from-bicycle-parts-253131.html" title="Forced To Sell Bullocks, Farmer Invents An Even Better Ploughing Machine From Bicycle Parts!" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/4966826802_f6897e4515_b-640_1460031596_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/4966826802_f6897e4515_b-640_1460031596_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/forced-to-sell-bullocks-farmer-invents-an-even-better-ploughing-machine-from-bicycle-parts-253131.html" title="Forced To Sell Bullocks, Farmer Invents An Even Better Ploughing Machine From Bicycle Parts!">
    Forced To Sell Bullocks, Farmer Invents An Even Better Ploughing Machine From Bicycle Parts!                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/hyderabad-woman-kills-newborn-boy-lets-him-die-on-street-because-she-wanted-a-daughter-instead-253129.html" title="Hyderabad Woman Kills Newborn Boy, Lets Him Die On Street Because She Wanted A Daughter Instead" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/mom-640_1460031023_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/mom-640_1460031023_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/hyderabad-woman-kills-newborn-boy-lets-him-die-on-street-because-she-wanted-a-daughter-instead-253129.html" title="Hyderabad Woman Kills Newborn Boy, Lets Him Die On Street Because She Wanted A Daughter Instead">
    Hyderabad Woman Kills Newborn Boy, Lets Him Die On Street Because She Wanted A Daughter Instead                    </a>
                </figcaption> 
            </div>
                        <div class="news-panel-list cf " ><!--news-panel-list start-->
                <figure>

                                            <a href="http://www.indiatimes.com/news/india/after-attack-on-police-horse-now-another-bjp-leader-arrested-for-shooting-down-street-dogs-253123.html" title="After Attack On Police Horse, Now A BJP Leader Arrested For Shooting Down Street Dogs!" class=" tint">


                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1460031588_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1460031588_236x111.jpg" border="0" alt=""/>
                    </a>
                </figure>
                <figcaption>

                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/after-attack-on-police-horse-now-another-bjp-leader-arrested-for-shooting-down-street-dogs-253123.html" title="After Attack On Police Horse, Now A BJP Leader Arrested For Shooting Down Street Dogs!">
    After Attack On Police Horse, Now A BJP Leader Arrested For Shooting Down Street Dogs!                    </a>
                </figcaption> 
            </div>
                </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
                        
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/ever-wondered-why-our-generation-is-so-obsessed-with-losing-its-hair-253044.html" class="tint" title="Ever Wondered Why Our Generation Is So Obsessed With Losing Its Hair?">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1459937899_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1459937899_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/ever-wondered-why-our-generation-is-so-obsessed-with-losing-its-hair-253044.html" title="Ever Wondered Why Our Generation Is So Obsessed With Losing Its Hair?">
    Ever Wondered Why Our Generation Is So Obsessed With Losing Its Hair?                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/pahlaj-nihalani-gives-u-a-certificate-to-jungle-book-and-the-internet-loses-it-s-cool-here-is-why-253133.html" class="tint" title="Pahlaj Nihalani Gives U/A Certificate To 'Jungle Book' And The Internet Loses It's Cool. Here Is Why">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/pahlaj-card_1460032290_1460032294_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/pahlaj-card_1460032290_1460032294_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/pahlaj-nihalani-gives-u-a-certificate-to-jungle-book-and-the-internet-loses-it-s-cool-here-is-why-253133.html" title="Pahlaj Nihalani Gives U/A Certificate To 'Jungle Book' And The Internet Loses It's Cool. Here Is Why">
    Pahlaj Nihalani Gives U/A Certificate To 'Jungle Book' And The Internet Loses It's Cool. Here Is Why                        </a>
                    </div>
                </figcaption>
            </div>
                    
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/play/slog-overs/vinod-kambli-offers-to-coach-pakistan-cricket-team-and-manages-to-break-the-internet-253128.html" class="tint" title="Vinod Kambli Offers To Coach Pakistan Cricket Team And Manages To Break The Internet!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/kpak640_1460030390_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/kpak640_1460030390_502x234.jpg" border="0" alt="" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/vinod-kambli-offers-to-coach-pakistan-cricket-team-and-manages-to-break-the-internet-253128.html" title="Vinod Kambli Offers To Coach Pakistan Cricket Team And Manages To Break The Internet!">
    Vinod Kambli Offers To Coach Pakistan Cricket Team And Manages To Break The Internet!                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
        
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/entertainment/celebs/zayn-malik-surprises-his-indian-pakistani-fans-with-urdu-lyrics-in-latest-song-flower-253047.html" class="tint" title="Zayn Malik Surprises His Indian & Pakistani Fans With Urdu Lyrics In Latest Song 'Flower'">

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/zayn-malik_1459940600_1459940605_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/zayn-malik_1459940600_1459940605_236x111.jpg" border="0" alt="Zayn Malik Surprises His Indian & Pakistani Fans With Urdu Lyrics In Latest Song 'Flower'"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/zayn-malik-surprises-his-indian-pakistani-fans-with-urdu-lyrics-in-latest-song-flower-253047.html" title="Zayn Malik Surprises His Indian & Pakistani Fans With Urdu Lyrics In Latest Song 'Flower'">
            Zayn Malik Surprises His Indian & Pakistani Fans With Urdu Lyrics In Latest Song 'Flower'                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf colombia"  id="div-clmb-ctn-129061-2"  data-slot="129061" data-position="2" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/play/slog-overs/amitabh-bachchan-s-epic-take-down-of-andrew-flintoff-shows-why-he-remains-the-real-shahenshah-253063.html" class="tint" title="Amitabh Bachchan's Epic Take Down Of Andrew Flintoff Shows Why He Remains The Real Shahenshah">

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/card-ab_1459947866_1459947880_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card-ab_1459947866_1459947880_236x111.jpg" border="0" alt="Amitabh Bachchan's Epic Take Down Of Andrew Flintoff Shows Why He Remains The Real Shahenshah"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/amitabh-bachchan-s-epic-take-down-of-andrew-flintoff-shows-why-he-remains-the-real-shahenshah-253063.html" title="Amitabh Bachchan's Epic Take Down Of Andrew Flintoff Shows Why He Remains The Real Shahenshah">
            Amitabh Bachchan's Epic Take Down Of Andrew Flintoff Shows Why He Remains The Real Shahenshah                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/entertainment/celebs/after-battling-cancer-emraan-hashmi-s-6-yo-son-is-sending-thank-you-notes-to-bollywood-actors-in-the-most-adorable-way-253058.html" class="tint" title="After Battling Cancer, Emraan Hashmi's 6 YO Son Is Sending Thank You Notes To B-Town Actors In The Most Adorable Way!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/emraan-card_1459944469_1459944476_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/emraan-card_1459944469_1459944476_236x111.jpg" border="0" alt="After Battling Cancer, Emraan Hashmi's 6 YO Son Is Sending Thank You Notes To B-Town Actors In The Most Adorable Way!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/after-battling-cancer-emraan-hashmi-s-6-yo-son-is-sending-thank-you-notes-to-bollywood-actors-in-the-most-adorable-way-253058.html" title="After Battling Cancer, Emraan Hashmi's 6 YO Son Is Sending Thank You Notes To B-Town Actors In The Most Adorable Way!">
            After Battling Cancer, Emraan Hashmi's 6 YO Son Is Sending Thank You Notes To B-Town Actors In The Most Adorable Way!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
    
            <div class="trending-panel-list cf "  ><!--trending-panel-list start-->
                                        <figure>

    
                    <a href="http://www.indiatimes.com/entertainment/hollywood/game-of-thrones-fans-are-losing-the-plot-over-jon-snow-s-new-white-walker-pictures-253043.html" class="tint" title="'Game Of Thrones' Fans Are Losing The Plot Over Jon Snow's New 'White Walker' Pictures!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/600_1459936966_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/600_1459936966_236x111.jpg" border="0" alt="'Game Of Thrones' Fans Are Losing The Plot Over Jon Snow's New 'White Walker' Pictures!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/game-of-thrones-fans-are-losing-the-plot-over-jon-snow-s-new-white-walker-pictures-253043.html" title="'Game Of Thrones' Fans Are Losing The Plot Over Jon Snow's New 'White Walker' Pictures!">
            'Game Of Thrones' Fans Are Losing The Plot Over Jon Snow's New 'White Walker' Pictures!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div>

</section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
    <div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

<section id="hp_block_3" class="container cf"><!--container start-->

    <div class="news-panel cf"><!--news-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/world/starving-sea-lion-pup-found-sleeping-in-a-restaurant-nursed-back-to-health-released-into-the-ocean-253111.html" title="Starving Sea Lion Pup Found Sleeping In A Restaurant Nursed Back To Health, Released Into The Ocean" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/cp2_1460023958_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/cp2_1460023958_236x111.jpg" border="0" alt="Starving Sea Lion Pup Found Sleeping In A Restaurant Nursed Back To Health, Released Into The Ocean"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/starving-sea-lion-pup-found-sleeping-in-a-restaurant-nursed-back-to-health-released-into-the-ocean-253111.html" title="Starving Sea Lion Pup Found Sleeping In A Restaurant Nursed Back To Health, Released Into The Ocean">
            Starving Sea Lion Pup Found Sleeping In A Restaurant Nursed Back To Health, Released Into The Ocean                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-8"  data-slot="129061" data-position="8" data-section="0" data-cb="adwidgetNew">

                <figure>

                    <a href="http://www.indiatimes.com/news/world/venezuela-government-declares-fridays-off-for-everyone-in-a-bid-to-conserve-energy-253117.html" title="Venezuela Government Declares Fridays Off For Everyone In A Bid To Conserve Energy" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/ven6_1460024904_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/ven6_1460024904_236x111.jpg" border="0" alt="Venezuela Government Declares Fridays Off For Everyone In A Bid To Conserve Energy"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/venezuela-government-declares-fridays-off-for-everyone-in-a-bid-to-conserve-energy-253117.html" title="Venezuela Government Declares Fridays Off For Everyone In A Bid To Conserve Energy">
            Venezuela Government Declares Fridays Off For Everyone In A Bid To Conserve Energy                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/world/iran-pakistan-and-saudi-arabia-tops-the-list-as-2015-witnessed-the-highest-number-of-execution-since-1989_-253122.html" title="Iran, Pakistan and Saudi Arabia Tops The List As 2015 Witnessed The Highest Number Of Execution Since 1989" class=" tint">
            

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/hanging-rope-640_1460027348_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/hanging-rope-640_1460027348_236x111.jpg" border="0" alt="Iran, Pakistan and Saudi Arabia Tops The List As 2015 Witnessed The Highest Number Of Execution Since 1989"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/iran-pakistan-and-saudi-arabia-tops-the-list-as-2015-witnessed-the-highest-number-of-execution-since-1989_-253122.html" title="Iran, Pakistan and Saudi Arabia Tops The List As 2015 Witnessed The Highest Number Of Execution Since 1989">
            Iran, Pakistan and Saudi Arabia Tops The List As 2015 Witnessed The Highest Number Of Execution Since 1989                    </a>
                </figcaption> 
            </div>
                <div class="news-panel-list cf " >

                <figure>

                    <a href="http://www.indiatimes.com/news/india/disturbing-cctv-footage-shows-delhi-s-mercedes-hit-and-run-accident-that-claimed-32-yo-sidharth-sharma-s-life-253114.html" title="Disturbing CCTV Footage Shows Delhi's Mercedes Hit-And-Run Accident That Claimed 32-YO Sidharth Sharma's Life" class=" tint">
            <a class='video-btn sprite' href='http://www.indiatimes.com/news/india/disturbing-cctv-footage-shows-delhi-s-mercedes-hit-and-run-accident-that-claimed-32-yo-sidharth-sharma-s-life-253114.html'>video</a>

                        <img  src="http://media.indiatimes.in/media/videocafe/2016/Apr/hitandrun_card_1460024560_236x111.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Apr/hitandrun_card_1460024560_236x111.jpg" border="0" alt="Disturbing CCTV Footage Shows Delhi's Mercedes Hit-And-Run Accident That Claimed 32-YO Sidharth Sharma's Life"/>
                    </a>

                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/disturbing-cctv-footage-shows-delhi-s-mercedes-hit-and-run-accident-that-claimed-32-yo-sidharth-sharma-s-life-253114.html" title="Disturbing CCTV Footage Shows Delhi's Mercedes Hit-And-Run Accident That Claimed 32-YO Sidharth Sharma's Life">
            Disturbing CCTV Footage Shows Delhi's Mercedes Hit-And-Run Accident That Claimed 32-YO Sidharth Sharma's Life                    </a>
                </figcaption> 
            </div>
        </div><!--news-panel end-->

    <div class="life-panel cf"><!--life-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
         
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/photographer-uses-17-square-meters-of-real-mirror-to-create-the-most-spectacular-images-you-ve-seen-253119.html" class="tint" title="Photographer Uses 17 Square Meters Of Real Mirror To Create The Most Spectacular Images You've Seen">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/mirror6_1460026854_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/mirror6_1460026854_502x234.jpg" border="0" alt="http://www.indiatimes.com/lifestyle/self/photographer-uses-17-square-meters-of-real-mirror-to-create-the-most-spectacular-images-you-ve-seen-253119.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/photographer-uses-17-square-meters-of-real-mirror-to-create-the-most-spectacular-images-you-ve-seen-253119.html" title="Photographer Uses 17 Square Meters Of Real Mirror To Create The Most Spectacular Images You've Seen">
            Photographer Uses 17 Square Meters Of Real Mirror To Create The Most Spectacular Images You've Seen                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/srk-adds-another-hollywood-biggie-to-his-fan-list-sweeps-matrix-director-off-her-feet-253106.html" class="tint" title="SRK Adds Another Hollywood Biggie To His Fan List, Sweeps Matrix Director Off Her Feet!">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/srk-card_1460022892_1460022897_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/srk-card_1460022892_1460022897_502x234.jpg" border="0" alt="http://www.indiatimes.com/entertainment/bollywood/srk-adds-another-hollywood-biggie-to-his-fan-list-sweeps-matrix-director-off-her-feet-253106.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/srk-adds-another-hollywood-biggie-to-his-fan-list-sweeps-matrix-director-off-her-feet-253106.html" title="SRK Adds Another Hollywood Biggie To His Fan List, Sweeps Matrix Director Off Her Feet!">
            SRK Adds Another Hollywood Biggie To His Fan List, Sweeps Matrix Director Off Her Feet!                        </a>
                    </div>
                </figcaption>
            </div>
                     
            <div class="life-panel-list cf">
                <figure>

                        <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/there-is-a-website-that-has-a-secret-sex-island-for-those-who-love-to-cheat-on-their-partners-253101.html" class="tint" title="There Is A Website That Has A Secret Sex Island For Those Who Love To Cheat On Their Partners">
                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/couple_1460028359_1460028367_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/couple_1460028359_1460028367_502x234.jpg" border="0" alt="http://www.indiatimes.com/culture/who-we-are/there-is-a-website-that-has-a-secret-sex-island-for-those-who-love-to-cheat-on-their-partners-253101.html" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                        <a class="bold-txt" href="http://www.indiatimes.com/culture/who-we-are/there-is-a-website-that-has-a-secret-sex-island-for-those-who-love-to-cheat-on-their-partners-253101.html" title="There Is A Website That Has A Secret Sex Island For Those Who Love To Cheat On Their Partners">
            There Is A Website That Has A Secret Sex Island For Those Who Love To Cheat On Their Partners                        </a>
                    </div>
                </figcaption>
            </div>
                </div><!--life-panel end-->

    <div class="trending-panel cf"><!--trending-panel start-->
        <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/pratyusha-banerjee-s-parents-reveal-mind-numbing-details-about-their-daughter-s-relationship-253020.html" class="tint" title="Pratyusha Banerjee's Parents Reveal Mind-Numbing Details About Their Daughter's Relationship!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/cc_1459922652_1459922660_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/cc_1459922652_1459922660_236x111.jpg" border="0" alt="Pratyusha Banerjee's Parents Reveal Mind-Numbing Details About Their Daughter's Relationship!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/pratyusha-banerjee-s-parents-reveal-mind-numbing-details-about-their-daughter-s-relationship-253020.html" title="Pratyusha Banerjee's Parents Reveal Mind-Numbing Details About Their Daughter's Relationship!">
    Pratyusha Banerjee's Parents Reveal Mind-Numbing Details About Their Daughter's Relationship!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-6"  data-slot="129061" data-position="6" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/lifestyle/technology/whatsapp-just-introduced-a-feature-to-secure-the-data-of-1-billion-users-from-hackers-253018.html" class="tint" title="WhatsApp Just Introduced A Feature To Secure The Data Of 1 Billion Users From Hackers">

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1459922244_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1459922244_236x111.jpg" border="0" alt="WhatsApp Just Introduced A Feature To Secure The Data Of 1 Billion Users From Hackers"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/whatsapp-just-introduced-a-feature-to-secure-the-data-of-1-billion-users-from-hackers-253018.html" title="WhatsApp Just Introduced A Feature To Secure The Data Of 1 Billion Users From Hackers">
    WhatsApp Just Introduced A Feature To Secure The Data Of 1 Billion Users From Hackers                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-might-get-a-clean-chit-as-cops-find-kangana-ranaut-s-behaviour-suspicious-253036.html" class="tint" title="Hrithik Roshan Might Get A Clean Chit As Cops Find Kangana Ranaut's Behaviour Suspicious!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/picmonkey-collage_1459931165_1459931171_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/picmonkey-collage_1459931165_1459931171_236x111.jpg" border="0" alt="Hrithik Roshan Might Get A Clean Chit As Cops Find Kangana Ranaut's Behaviour Suspicious!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-might-get-a-clean-chit-as-cops-find-kangana-ranaut-s-behaviour-suspicious-253036.html" title="Hrithik Roshan Might Get A Clean Chit As Cops Find Kangana Ranaut's Behaviour Suspicious!">
    Hrithik Roshan Might Get A Clean Chit As Cops Find Kangana Ranaut's Behaviour Suspicious!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
                <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                            <figure>

            		

                    <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-has-a-beautiful-message-for-all-the-curvy-girls-it-ll-inspire-you-to-no-end-253019.html" class="tint" title="Priyanka Chopra Has A Beautiful Message For All The Curvy Girls & It'll Inspire You To No End!">

                        <img  src="http://media.indiatimes.in/media/content/2016/Apr/priyanka-chopra-68672_1459922413_1459922419_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/priyanka-chopra-68672_1459922413_1459922419_236x111.jpg" border="0" alt="Priyanka Chopra Has A Beautiful Message For All The Curvy Girls & It'll Inspire You To No End!"/>
                    </a>
                </figure>
                <figcaption>
                    <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-has-a-beautiful-message-for-all-the-curvy-girls-it-ll-inspire-you-to-no-end-253019.html" title="Priyanka Chopra Has A Beautiful Message For All The Curvy Girls & It'll Inspire You To No End!">
    Priyanka Chopra Has A Beautiful Message For All The Curvy Girls & It'll Inspire You To No End!                    </a>
                    </div>    
                </figcaption>
            </div><!--trending-panel-list end-->
        </div><!--trending-panel end-->

</section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
    <div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

<section class="container cf" id="container4"><!--container start-->
    <div class="news-panel cf ">
        <h2><a target="_blank" href="http://www.indiatimes.com/news/">news</a></h2>

                    <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/assam-rifles-inducts-its-first-batch-of-100-women-soldiers-253112.html" title="Assam Rifles Inducts Its First Batch Of Women Soldiers To Its 181-Year-Old Regiment" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1460024127_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1460024127_236x111.jpg" border="0" alt="Assam Rifles Inducts Its First Batch Of Women Soldiers To Its 181-Year-Old Regiment"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/assam-rifles-inducts-its-first-batch-of-100-women-soldiers-253112.html" title="Assam Rifles Inducts Its First Batch Of Women Soldiers To Its 181-Year-Old Regiment">
        Assam Rifles Inducts Its First Batch Of Women Soldiers To Its 181-Year-Old Regiment                    </a>
                </figcaption> 
            </div>
                <div class='container1'>            <div class="news-panel-list cf colombia" id="div-clmb-ctn-129061-12"  data-slot="129061" data-position="12" data-section="0" data-cb="adwidgetNew">
                <figure>                      
                    <a href="http://www.indiatimes.com/news/remember-the-guy-who-bought-an-iphone-5s-for-rs-68-turns-out-snapdeal-still-won-t-give-it-to-him-253110.html" title="Remember The Guy Who Bought An iPhone 5S For Rs 68? Turns Out Snapdeal Still Won't Give It To Him!" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1460023746_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1460023746_236x111.jpg" border="0" alt="Remember The Guy Who Bought An iPhone 5S For Rs 68? Turns Out Snapdeal Still Won't Give It To Him!"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/remember-the-guy-who-bought-an-iphone-5s-for-rs-68-turns-out-snapdeal-still-won-t-give-it-to-him-253110.html" title="Remember The Guy Who Bought An iPhone 5S For Rs 68? Turns Out Snapdeal Still Won't Give It To Him!">
        Remember The Guy Who Bought An iPhone 5S For Rs 68? Turns Out Snapdeal Still Won't Give It To Him!                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/world/this-77-yo-man-died-of-a-broken-heart-after-he-failed-to-find-the-only-friend-he-had-his-dog-253104.html" title="This 77-YO Man Died Of A 'Broken Heart' After He Failed To Find The Only Friend He Had, His Dog" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Apr/6_1460020032_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/6_1460020032_236x111.jpg" border="0" alt="This 77-YO Man Died Of A 'Broken Heart' After He Failed To Find The Only Friend He Had, His Dog"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/world/this-77-yo-man-died-of-a-broken-heart-after-he-failed-to-find-the-only-friend-he-had-his-dog-253104.html" title="This 77-YO Man Died Of A 'Broken Heart' After He Failed To Find The Only Friend He Had, His Dog">
        This 77-YO Man Died Of A 'Broken Heart' After He Failed To Find The Only Friend He Had, His Dog                    </a>
                </figcaption> 
            </div>
                            <div class="news-panel-list cf " >
                <figure>                      
                    <a href="http://www.indiatimes.com/news/india/villagers-hit-by-frozen-airplane-toilet-waste-think-it-s-god-anger-raining-down-on-them-253108.html" title="Villagers Hit By Frozen Airplane Toilet Waste, Think It's God Anger Raining Down On Them" class=" tint">
                                                <img  src="http://media.indiatimes.in/media/content/2016/Apr/lop_1460031109_1460031118_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/lop_1460031109_1460031118_236x111.jpg" border="0" alt="Villagers Hit By Frozen Airplane Toilet Waste, Think It's God Anger Raining Down On Them"/>
                    </a>
                </figure>
                <figcaption>
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/villagers-hit-by-frozen-airplane-toilet-waste-think-it-s-god-anger-raining-down-on-them-253108.html" title="Villagers Hit By Frozen Airplane Toilet Waste, Think It's God Anger Raining Down On Them">
        Villagers Hit By Frozen Airplane Toilet Waste, Think It's God Anger Raining Down On Them                    </a>
                </figcaption> 
            </div>
                 
    </div>
</div><!--news-panel end-->

<div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/lifestyle/">lifestyle</a></h2>
        
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/top-ten-quotes-on-mahendra-singh-dhoni-by-those-who-played-under-him-for-csk-and-team-india-253124.html" class="tint" title="10 Quotes By MS Dhoni's Teammates That Show He's The Best Captain Of Our Times">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/big640_1460031863_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/big640_1460031863_502x234.jpg" border="0" alt="10 Quotes By MS Dhoni's Teammates That Show He's The Best Captain Of Our Times" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/top-ten-quotes-on-mahendra-singh-dhoni-by-those-who-played-under-him-for-csk-and-team-india-253124.html" title="10 Quotes By MS Dhoni's Teammates That Show He's The Best Captain Of Our Times">
    10 Quotes By MS Dhoni's Teammates That Show He's The Best Captain Of Our Times                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/samuel-l-jackson-has-fanboy-moment-clicks-selfie-with-the-most-beautiful-deepika-padukone-253105.html" class="tint" title="Samuel L Jackson Has A Fanboy Moment, Clicks Selfie With 'The Most Beautiful' Deepika Padukone">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1460022307_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1460022307_502x234.jpg" border="0" alt="Samuel L Jackson Has A Fanboy Moment, Clicks Selfie With 'The Most Beautiful' Deepika Padukone" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/samuel-l-jackson-has-fanboy-moment-clicks-selfie-with-the-most-beautiful-deepika-padukone-253105.html" title="Samuel L Jackson Has A Fanboy Moment, Clicks Selfie With 'The Most Beautiful' Deepika Padukone">
    Samuel L Jackson Has A Fanboy Moment, Clicks Selfie With 'The Most Beautiful' Deepika Padukone                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/this-doordarshan-s-desi-review-of-got-is-so-spot-on-that-it-ll-leave-you-laughing-for-days-253102.html" class="tint" title="Doordarshan's Desi Review Of GoT Is So Spot On That It'll Leave You Laughing For Days!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1460018934_1460018937_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1460018934_1460018937_502x234.jpg" border="0" alt="Doordarshan's Desi Review Of GoT Is So Spot On That It'll Leave You Laughing For Days!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/this-doordarshan-s-desi-review-of-got-is-so-spot-on-that-it-ll-leave-you-laughing-for-days-253102.html" title="Doordarshan's Desi Review Of GoT Is So Spot On That It'll Leave You Laughing For Days!">
    Doordarshan's Desi Review Of GoT Is So Spot On That It'll Leave You Laughing For Days!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/ranveer-singh-to-dethrone-srk-become-the-highest-paid-celeb-in-advertising-world-253094.html" class="tint" title="Ranveer Singh To Dethrone SRK & Become The Highest Paid Celeb In Advertising World!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card1_1460014799_1460014812_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card1_1460014799_1460014812_502x234.jpg" border="0" alt="Ranveer Singh To Dethrone SRK & Become The Highest Paid Celeb In Advertising World!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/ranveer-singh-to-dethrone-srk-become-the-highest-paid-celeb-in-advertising-world-253094.html" title="Ranveer Singh To Dethrone SRK & Become The Highest Paid Celeb In Advertising World!">
    Ranveer Singh To Dethrone SRK & Become The Highest Paid Celeb In Advertising World!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/kangana-s-lawyer-accuses-hrithik-of-circulating-her-objectionable-pictures-to-the-media-253099.html" class="tint" title="Kangana's Lawyer Accuses Hrithik Of Circulating Her 'Objectionable Pictures' To the Media">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/kan-crd_1460016682_1460016686_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/kan-crd_1460016682_1460016686_502x234.jpg" border="0" alt="Kangana's Lawyer Accuses Hrithik Of Circulating Her 'Objectionable Pictures' To the Media" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/kangana-s-lawyer-accuses-hrithik-of-circulating-her-objectionable-pictures-to-the-media-253099.html" title="Kangana's Lawyer Accuses Hrithik Of Circulating Her 'Objectionable Pictures' To the Media">
    Kangana's Lawyer Accuses Hrithik Of Circulating Her 'Objectionable Pictures' To the Media                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/50-of-asthmatics-don-t-really-have-the-disease-often-misdiagnosed-say-experts-253093.html" class="tint" title="50% Of 'Asthmatics' Don't Really Have The Disease, Often Misdiagnosed, Say Experts">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1460014762_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1460014762_502x234.jpg" border="0" alt="50% Of 'Asthmatics' Don't Really Have The Disease, Often Misdiagnosed, Say Experts" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/50-of-asthmatics-don-t-really-have-the-disease-often-misdiagnosed-say-experts-253093.html" title="50% Of 'Asthmatics' Don't Really Have The Disease, Often Misdiagnosed, Say Experts">
    50% Of 'Asthmatics' Don't Really Have The Disease, Often Misdiagnosed, Say Experts                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/gucci-just-had-its-latest-ad-banned-because-its-model-was-unhealthily-thin-253095.html" class="tint" title="Gucci Just Had Its Latest Ad Banned Because The Model Was âUnhealthily Thinâ">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/cover_1460015763_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/cover_1460015763_502x234.jpg" border="0" alt="Gucci Just Had Its Latest Ad Banned Because The Model Was âUnhealthily Thinâ" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/gucci-just-had-its-latest-ad-banned-because-its-model-was-unhealthily-thin-253095.html" title="Gucci Just Had Its Latest Ad Banned Because The Model Was âUnhealthily Thinâ">
    Gucci Just Had Its Latest Ad Banned Because The Model Was âUnhealthily Thinâ                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-is-how-a-person-feels-standing-next-to-india-s-fastest-train-gatimaan-express-travelling-at-its-top-speed-of-160-kmph-253100.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/videocafe/this-is-how-a-person-feels-standing-next-to-india-s-fastest-train-gatimaan-express-travelling-at-its-top-speed-of-160-kmph-253100.html" class="tint" title="This Is How One Feels Standing Next To India's Fastest Train  Gatimaan Express  Travelling At 160 Kmph Top Speed!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Apr/gatimaanexpress_card_1460018311_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Apr/gatimaanexpress_card_1460018311_502x234.jpg" border="0" alt="This Is How One Feels Standing Next To India's Fastest Train  Gatimaan Express  Travelling At 160 Kmph Top Speed!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/videocafe/this-is-how-a-person-feels-standing-next-to-india-s-fastest-train-gatimaan-express-travelling-at-its-top-speed-of-160-kmph-253100.html" title="This Is How One Feels Standing Next To India's Fastest Train  Gatimaan Express  Travelling At 160 Kmph Top Speed!">
    This Is How One Feels Standing Next To India's Fastest Train  Gatimaan Express  Travelling At 160 Kmph Top Speed!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/akshay-kumar-detained-at-london-heathrow-airport-for-travelling-without-a-valid-visa-253090.html" class="tint" title="Akshay Kumar Detained At Heathrow Airport For Travelling Without A Valid Visa">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/akku-card_1460012885_1460012888_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/akku-card_1460012885_1460012888_502x234.jpg" border="0" alt="Akshay Kumar Detained At Heathrow Airport For Travelling Without A Valid Visa" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/akshay-kumar-detained-at-london-heathrow-airport-for-travelling-without-a-valid-visa-253090.html" title="Akshay Kumar Detained At Heathrow Airport For Travelling Without A Valid Visa">
    Akshay Kumar Detained At Heathrow Airport For Travelling Without A Valid Visa                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/the-trailer-of-nagesh-kukunoor-s-dhanak-has-2-kids-an-exciting-rajasthan-trip-craze-for-srk-253085.html" class="tint" title="The Trailer Of Nagesh Kukunoor's Dhanak Has 2 Kids, An Exciting Rajasthan Trip & Craze For SRK!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card3_1460012378_1460012384_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card3_1460012378_1460012384_502x234.jpg" border="0" alt="The Trailer Of Nagesh Kukunoor's Dhanak Has 2 Kids, An Exciting Rajasthan Trip & Craze For SRK!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/the-trailer-of-nagesh-kukunoor-s-dhanak-has-2-kids-an-exciting-rajasthan-trip-craze-for-srk-253085.html" title="The Trailer Of Nagesh Kukunoor's Dhanak Has 2 Kids, An Exciting Rajasthan Trip & Craze For SRK!">
    The Trailer Of Nagesh Kukunoor's Dhanak Has 2 Kids, An Exciting Rajasthan Trip & Craze For SRK!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/slut-shaming-won-t-work-with-me-says-kangana-ranaut-on-her-legal-tussle-with-hrithik-roshan-253079.html" class="tint" title="Slut-Shaming Won't Work With Me, Says Kangana Ranaut On Her Legal Tussle With Hrithik Roshan">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/kangana-and-hrithik_1460008634_1460008646_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/kangana-and-hrithik_1460008634_1460008646_502x234.jpg" border="0" alt="Slut-Shaming Won't Work With Me, Says Kangana Ranaut On Her Legal Tussle With Hrithik Roshan" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/slut-shaming-won-t-work-with-me-says-kangana-ranaut-on-her-legal-tussle-with-hrithik-roshan-253079.html" title="Slut-Shaming Won't Work With Me, Says Kangana Ranaut On Her Legal Tussle With Hrithik Roshan">
    Slut-Shaming Won't Work With Me, Says Kangana Ranaut On Her Legal Tussle With Hrithik Roshan                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/salman-khan-wasn-t-driving-during-the-2002-hit-run-accident-says-supreme-court-253083.html" class="tint" title="Salman Khan Wasn't Driving During The 2002 Hit & Run Accident, Says Supreme Court">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/sal-card_1460009409_1460009420_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/sal-card_1460009409_1460009420_502x234.jpg" border="0" alt="Salman Khan Wasn't Driving During The 2002 Hit & Run Accident, Says Supreme Court" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/salman-khan-wasn-t-driving-during-the-2002-hit-run-accident-says-supreme-court-253083.html" title="Salman Khan Wasn't Driving During The 2002 Hit & Run Accident, Says Supreme Court">
    Salman Khan Wasn't Driving During The 2002 Hit & Run Accident, Says Supreme Court                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/kick-off/now-a-drone-is-helping-indian-football-team-monitor-its-progress-and-improve-playing-standards-253081.html" class="tint" title="Now A Drone Is Helping Indian Football Team Monitor Its Progress And Improve Playing Standards">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/drone640_1460009032_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/drone640_1460009032_502x234.jpg" border="0" alt="Now A Drone Is Helping Indian Football Team Monitor Its Progress And Improve Playing Standards" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/kick-off/now-a-drone-is-helping-indian-football-team-monitor-its-progress-and-improve-playing-standards-253081.html" title="Now A Drone Is Helping Indian Football Team Monitor Its Progress And Improve Playing Standards">
    Now A Drone Is Helping Indian Football Team Monitor Its Progress And Improve Playing Standards                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/putting-all-break-up-rumors-to-rest-virat-kohli-anushka-sharma-went-on-a-dinner-date-last-night-253071.html" class="tint" title="Putting All Break-Up Rumors To Rest, Virat & Anushka Went On A Dinner Date Last Night!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/aa_1460012074_1460012082_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/aa_1460012074_1460012082_502x234.jpg" border="0" alt="Putting All Break-Up Rumors To Rest, Virat & Anushka Went On A Dinner Date Last Night!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/putting-all-break-up-rumors-to-rest-virat-kohli-anushka-sharma-went-on-a-dinner-date-last-night-253071.html" title="Putting All Break-Up Rumors To Rest, Virat & Anushka Went On A Dinner Date Last Night!">
    Putting All Break-Up Rumors To Rest, Virat & Anushka Went On A Dinner Date Last Night!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/a-new-twist-in-ipl-2016-as-fans-to-turn-third-umpires-253076.html" class="tint" title="A New Twist In IPL 2016 As Fans To Turn Third Umpires!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/fanumpire640_1460007344_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/fanumpire640_1460007344_502x234.jpg" border="0" alt="A New Twist In IPL 2016 As Fans To Turn Third Umpires!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/a-new-twist-in-ipl-2016-as-fans-to-turn-third-umpires-253076.html" title="A New Twist In IPL 2016 As Fans To Turn Third Umpires!">
    A New Twist In IPL 2016 As Fans To Turn Third Umpires!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/work-hard-to-realise-your-dream-of-playing-for-india-says-sachin-tendulkar-to-aspiring-cricketers-253072.html" class="tint" title="Work Hard To Realise Your Dream Of Playing For India, Says Sachin Tendulkar To Aspiring Cricketers">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/tendulkar1640_1460006084_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/tendulkar1640_1460006084_502x234.jpg" border="0" alt="Work Hard To Realise Your Dream Of Playing For India, Says Sachin Tendulkar To Aspiring Cricketers" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/work-hard-to-realise-your-dream-of-playing-for-india-says-sachin-tendulkar-to-aspiring-cricketers-253072.html" title="Work Hard To Realise Your Dream Of Playing For India, Says Sachin Tendulkar To Aspiring Cricketers">
    Work Hard To Realise Your Dream Of Playing For India, Says Sachin Tendulkar To Aspiring Cricketers                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/12-facts-about-happiness-anyone-sulking-right-now-should-know-253028.html" class="tint" title="12 Facts About Happiness Anyone Sulking Right Now Should Know">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/happy2_1459927929_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/happy2_1459927929_502x234.jpg" border="0" alt="12 Facts About Happiness Anyone Sulking Right Now Should Know" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/12-facts-about-happiness-anyone-sulking-right-now-should-know-253028.html" title="12 Facts About Happiness Anyone Sulking Right Now Should Know">
    12 Facts About Happiness Anyone Sulking Right Now Should Know                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/5-reasons-why-microsoft-hololens-is-better-than-all-other-vr-headsets-253067.html" class="tint" title="5 Reasons Why Microsoft HoloLens Is Better Than All Other VR Headsets">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1459948015_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1459948015_502x234.jpg" border="0" alt="5 Reasons Why Microsoft HoloLens Is Better Than All Other VR Headsets" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/5-reasons-why-microsoft-hololens-is-better-than-all-other-vr-headsets-253067.html" title="5 Reasons Why Microsoft HoloLens Is Better Than All Other VR Headsets">
    5 Reasons Why Microsoft HoloLens Is Better Than All Other VR Headsets                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/after-battling-cancer-emraan-hashmi-s-6-yo-son-is-sending-thank-you-notes-to-bollywood-actors-in-the-most-adorable-way-253058.html" class="tint" title="After Battling Cancer, Emraan Hashmi's 6 YO Son Is Sending Thank You Notes To B-Town Actors In The Most Adorable Way!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/emraan-card_1459944469_1459944476_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/emraan-card_1459944469_1459944476_502x234.jpg" border="0" alt="After Battling Cancer, Emraan Hashmi's 6 YO Son Is Sending Thank You Notes To B-Town Actors In The Most Adorable Way!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/after-battling-cancer-emraan-hashmi-s-6-yo-son-is-sending-thank-you-notes-to-bollywood-actors-in-the-most-adorable-way-253058.html" title="After Battling Cancer, Emraan Hashmi's 6 YO Son Is Sending Thank You Notes To B-Town Actors In The Most Adorable Way!">
    After Battling Cancer, Emraan Hashmi's 6 YO Son Is Sending Thank You Notes To B-Town Actors In The Most Adorable Way!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/it-s-official-deadpool-is-the-biggest-x-men-movie-ever-253051.html" class="tint" title="It's Official! 'Deadpool' Is The Biggest 'X-Men' Movie Ever">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/deadpool-2-boyfriend-pic_1459941150_1459941175_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/deadpool-2-boyfriend-pic_1459941150_1459941175_502x234.jpg" border="0" alt="It's Official! 'Deadpool' Is The Biggest 'X-Men' Movie Ever" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/it-s-official-deadpool-is-the-biggest-x-men-movie-ever-253051.html" title="It's Official! 'Deadpool' Is The Biggest 'X-Men' Movie Ever">
    It's Official! 'Deadpool' Is The Biggest 'X-Men' Movie Ever                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/amitabh-bachchan-s-epic-take-down-of-andrew-flintoff-shows-why-he-remains-the-real-shahenshah-253063.html" class="tint" title="Amitabh Bachchan's Epic Take Down Of Andrew Flintoff Shows Why He Remains The Real Shahenshah">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card-ab_1459947866_1459947880_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card-ab_1459947866_1459947880_502x234.jpg" border="0" alt="Amitabh Bachchan's Epic Take Down Of Andrew Flintoff Shows Why He Remains The Real Shahenshah" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/amitabh-bachchan-s-epic-take-down-of-andrew-flintoff-shows-why-he-remains-the-real-shahenshah-253063.html" title="Amitabh Bachchan's Epic Take Down Of Andrew Flintoff Shows Why He Remains The Real Shahenshah">
    Amitabh Bachchan's Epic Take Down Of Andrew Flintoff Shows Why He Remains The Real Shahenshah                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/travel/9-scientific-facts-that-prove-travelling-is-the-solution-to-all-your-problems-252932.html" class="tint" title="9 Scientific Facts That Prove Travelling Is The Solution To All Your Problems">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/prague2_1459763145_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/prague2_1459763145_502x234.jpg" border="0" alt="9 Scientific Facts That Prove Travelling Is The Solution To All Your Problems" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/travel/9-scientific-facts-that-prove-travelling-is-the-solution-to-all-your-problems-252932.html" title="9 Scientific Facts That Prove Travelling Is The Solution To All Your Problems">
    9 Scientific Facts That Prove Travelling Is The Solution To All Your Problems                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/culture/food/this-indian-whisky-brand-is-named-one-of-the-world-s-best-but-we-bet-you-haven-t-had-a-peg-yet-253057.html" class="tint" title="This Indian Whisky Brand Is Named One Of The Worldâs Best But We Bet You Havenât Had A Peg Yet">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/asds_1459943910_1459943920_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/asds_1459943910_1459943920_502x234.jpg" border="0" alt="This Indian Whisky Brand Is Named One Of The Worldâs Best But We Bet You Havenât Had A Peg Yet" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/culture/food/this-indian-whisky-brand-is-named-one-of-the-world-s-best-but-we-bet-you-haven-t-had-a-peg-yet-253057.html" title="This Indian Whisky Brand Is Named One Of The Worldâs Best But We Bet You Havenât Had A Peg Yet">
    This Indian Whisky Brand Is Named One Of The Worldâs Best But We Bet You Havenât Had A Peg Yet                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/zayn-malik-surprises-his-indian-pakistani-fans-with-urdu-lyrics-in-latest-song-flower-253047.html" class="tint" title="Zayn Malik Surprises His Indian & Pakistani Fans With Urdu Lyrics In Latest Song 'Flower'">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/zayn-malik_1459940600_1459940605_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/zayn-malik_1459940600_1459940605_502x234.jpg" border="0" alt="Zayn Malik Surprises His Indian & Pakistani Fans With Urdu Lyrics In Latest Song 'Flower'" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/zayn-malik-surprises-his-indian-pakistani-fans-with-urdu-lyrics-in-latest-song-flower-253047.html" title="Zayn Malik Surprises His Indian & Pakistani Fans With Urdu Lyrics In Latest Song 'Flower'">
    Zayn Malik Surprises His Indian & Pakistani Fans With Urdu Lyrics In Latest Song 'Flower'                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/local-producers-file-petition-seeking-ban-on-bollywood-films-in-pakistan-253042.html" class="tint" title="Local Producers File Petition Seeking Ban On Bollywood Films In Pakistan">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/van-card_1459936393_1459936396_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/van-card_1459936393_1459936396_502x234.jpg" border="0" alt="Local Producers File Petition Seeking Ban On Bollywood Films In Pakistan" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/local-producers-file-petition-seeking-ban-on-bollywood-films-in-pakistan-253042.html" title="Local Producers File Petition Seeking Ban On Bollywood Films In Pakistan">
    Local Producers File Petition Seeking Ban On Bollywood Films In Pakistan                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/if-you-were-waiting-for-a-superman-solo-movie-rejoice-zack-snyder-and-henry-cavill-confirm-man-of-steel-sequel-253048.html" class="tint" title="If You Were Waiting For A Superman Solo Movie, Rejoice! Zack Snyder And Henry Cavill Confirm Man Of Steel Sequel">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/man-card_1459941014_1459941016_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/man-card_1459941014_1459941016_502x234.jpg" border="0" alt="If You Were Waiting For A Superman Solo Movie, Rejoice! Zack Snyder And Henry Cavill Confirm Man Of Steel Sequel" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/if-you-were-waiting-for-a-superman-solo-movie-rejoice-zack-snyder-and-henry-cavill-confirm-man-of-steel-sequel-253048.html" title="If You Were Waiting For A Superman Solo Movie, Rejoice! Zack Snyder And Henry Cavill Confirm Man Of Steel Sequel">
    If You Were Waiting For A Superman Solo Movie, Rejoice! Zack Snyder And Henry Cavill Confirm Man Of Steel Sequel                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/hollywood/game-of-thrones-fans-are-losing-the-plot-over-jon-snow-s-new-white-walker-pictures-253043.html" class="tint" title="'Game Of Thrones' Fans Are Losing The Plot Over Jon Snow's New 'White Walker' Pictures!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/600_1459936966_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/600_1459936966_502x234.jpg" border="0" alt="'Game Of Thrones' Fans Are Losing The Plot Over Jon Snow's New 'White Walker' Pictures!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/hollywood/game-of-thrones-fans-are-losing-the-plot-over-jon-snow-s-new-white-walker-pictures-253043.html" title="'Game Of Thrones' Fans Are Losing The Plot Over Jon Snow's New 'White Walker' Pictures!">
    'Game Of Thrones' Fans Are Losing The Plot Over Jon Snow's New 'White Walker' Pictures!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/meet-devika-sirohi-the-indian-student-who-was-part-of-the-team-that-decoded-the-zika-virus-253030.html" class="tint" title="Meet Devika Sirohi - The Indian Student Who Was Part Of The Team That Decoded The Zika Virus">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/cover_1459928385_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/cover_1459928385_502x234.jpg" border="0" alt="Meet Devika Sirohi - The Indian Student Who Was Part Of The Team That Decoded The Zika Virus" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/meet-devika-sirohi-the-indian-student-who-was-part-of-the-team-that-decoded-the-zika-virus-253030.html" title="Meet Devika Sirohi - The Indian Student Who Was Part Of The Team That Decoded The Zika Virus">
    Meet Devika Sirohi - The Indian Student Who Was Part Of The Team That Decoded The Zika Virus                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-might-get-a-clean-chit-as-cops-find-kangana-ranaut-s-behaviour-suspicious-253036.html" class="tint" title="Hrithik Roshan Might Get A Clean Chit As Cops Find Kangana Ranaut's Behaviour Suspicious!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/picmonkey-collage_1459931165_1459931171_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/picmonkey-collage_1459931165_1459931171_502x234.jpg" border="0" alt="Hrithik Roshan Might Get A Clean Chit As Cops Find Kangana Ranaut's Behaviour Suspicious!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/hrithik-roshan-might-get-a-clean-chit-as-cops-find-kangana-ranaut-s-behaviour-suspicious-253036.html" title="Hrithik Roshan Might Get A Clean Chit As Cops Find Kangana Ranaut's Behaviour Suspicious!">
    Hrithik Roshan Might Get A Clean Chit As Cops Find Kangana Ranaut's Behaviour Suspicious!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/srk-reading-fan-mail-in-this-video-will-make-you-want-to-write-a-letter-to-him-right-now-253033.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/srk-reading-fan-mail-in-this-video-will-make-you-want-to-write-a-letter-to-him-right-now-253033.html" class="tint" title="SRK Reading Fan Mail In This Video Will Make You Want To Write A Letter To Him Right Now!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Apr/shah-rukh-khan_1459930074_1459930079_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Apr/shah-rukh-khan_1459930074_1459930079_502x234.jpg" border="0" alt="SRK Reading Fan Mail In This Video Will Make You Want To Write A Letter To Him Right Now!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/srk-reading-fan-mail-in-this-video-will-make-you-want-to-write-a-letter-to-him-right-now-253033.html" title="SRK Reading Fan Mail In This Video Will Make You Want To Write A Letter To Him Right Now!">
    SRK Reading Fan Mail In This Video Will Make You Want To Write A Letter To Him Right Now!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/slog-overs/ipl-will-waste-over-66-lakh-litres-of-water-in-drought-hit-maharashtra-but-nothing-will-stop-the-tournament-253034.html" class="tint" title="66 Lakh Litres Of Water Will Be Used To Maintain Pitches In Drought-Hit Maharashtra, But No One Seems To Care!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/ipld640_1459931067_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/ipld640_1459931067_502x234.jpg" border="0" alt="66 Lakh Litres Of Water Will Be Used To Maintain Pitches In Drought-Hit Maharashtra, But No One Seems To Care!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/slog-overs/ipl-will-waste-over-66-lakh-litres-of-water-in-drought-hit-maharashtra-but-nothing-will-stop-the-tournament-253034.html" title="66 Lakh Litres Of Water Will Be Used To Maintain Pitches In Drought-Hit Maharashtra, But No One Seems To Care!">
    66 Lakh Litres Of Water Will Be Used To Maintain Pitches In Drought-Hit Maharashtra, But No One Seems To Care!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/hinting-at-pratyusha-banerjee-mahesh-bhatt-says-actresses-suffer-more-abuse-than-domestic-help-253023.html" class="tint" title="Hinting At Pratyusha Banerjee, Mahesh Bhatt Says Actresses Suffer More Abuse Than Domestic Help">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/bhatt-card_1459923288_1459923293_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/bhatt-card_1459923288_1459923293_502x234.jpg" border="0" alt="Hinting At Pratyusha Banerjee, Mahesh Bhatt Says Actresses Suffer More Abuse Than Domestic Help" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/hinting-at-pratyusha-banerjee-mahesh-bhatt-says-actresses-suffer-more-abuse-than-domestic-help-253023.html" title="Hinting At Pratyusha Banerjee, Mahesh Bhatt Says Actresses Suffer More Abuse Than Domestic Help">
    Hinting At Pratyusha Banerjee, Mahesh Bhatt Says Actresses Suffer More Abuse Than Domestic Help                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/buzz/now-condoms-diapers-and-sanitary-pads-will-be-sold-along-with-disposal-pouches-253032.html" class="tint" title="Now Condoms, Diapers And Sanitary Pads Will Be Sold Along With Disposal Pouches">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1459931705_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1459931705_502x234.jpg" border="0" alt="Now Condoms, Diapers And Sanitary Pads Will Be Sold Along With Disposal Pouches" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/buzz/now-condoms-diapers-and-sanitary-pads-will-be-sold-along-with-disposal-pouches-253032.html" title="Now Condoms, Diapers And Sanitary Pads Will Be Sold Along With Disposal Pouches">
    Now Condoms, Diapers And Sanitary Pads Will Be Sold Along With Disposal Pouches                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/pratyusha-banerjee-s-parents-reveal-mind-numbing-details-about-their-daughter-s-relationship-253020.html" class="tint" title="Pratyusha Banerjee's Parents Reveal Mind-Numbing Details About Their Daughter's Relationship!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/cc_1459922652_1459922660_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/cc_1459922652_1459922660_502x234.jpg" border="0" alt="Pratyusha Banerjee's Parents Reveal Mind-Numbing Details About Their Daughter's Relationship!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/pratyusha-banerjee-s-parents-reveal-mind-numbing-details-about-their-daughter-s-relationship-253020.html" title="Pratyusha Banerjee's Parents Reveal Mind-Numbing Details About Their Daughter's Relationship!">
    Pratyusha Banerjee's Parents Reveal Mind-Numbing Details About Their Daughter's Relationship!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/judge-dismisses-lawsuit-says-porsche-not-responsible-for-paul-walker-s-death-253024.html" class="tint" title="Judge Dismisses Lawsuit, Says Porsche Not Responsible For Paul Walker's Death">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/paulwalker_1459924690_1459924695_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/paulwalker_1459924690_1459924695_502x234.jpg" border="0" alt="Judge Dismisses Lawsuit, Says Porsche Not Responsible For Paul Walker's Death" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/judge-dismisses-lawsuit-says-porsche-not-responsible-for-paul-walker-s-death-253024.html" title="Judge Dismisses Lawsuit, Says Porsche Not Responsible For Paul Walker's Death">
    Judge Dismisses Lawsuit, Says Porsche Not Responsible For Paul Walker's Death                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/celebs/this-short-film-is-a-sweet-reminder-that-love-can-help-you-survive-all-odds-253025.html'>video</a>                <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/this-short-film-is-a-sweet-reminder-that-love-can-help-you-survive-all-odds-253025.html" class="tint" title="This Short Film Is A Sweet Reminder That Love Can Help You Survive All Odds!">
                    <img  src="http://media.indiatimes.in/media/videocafe/2016/Apr/short-film-card_1459925119_1459925124_502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2016/Apr/short-film-card_1459925119_1459925124_502x234.jpg" border="0" alt="This Short Film Is A Sweet Reminder That Love Can Help You Survive All Odds!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/this-short-film-is-a-sweet-reminder-that-love-can-help-you-survive-all-odds-253025.html" title="This Short Film Is A Sweet Reminder That Love Can Help You Survive All Odds!">
    This Short Film Is A Sweet Reminder That Love Can Help You Survive All Odds!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-has-a-beautiful-message-for-all-the-curvy-girls-it-ll-inspire-you-to-no-end-253019.html" class="tint" title="Priyanka Chopra Has A Beautiful Message For All The Curvy Girls & It'll Inspire You To No End!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/priyanka-chopra-68672_1459922413_1459922419_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/priyanka-chopra-68672_1459922413_1459922419_502x234.jpg" border="0" alt="Priyanka Chopra Has A Beautiful Message For All The Curvy Girls & It'll Inspire You To No End!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/priyanka-chopra-has-a-beautiful-message-for-all-the-curvy-girls-it-ll-inspire-you-to-no-end-253019.html" title="Priyanka Chopra Has A Beautiful Message For All The Curvy Girls & It'll Inspire You To No End!">
    Priyanka Chopra Has A Beautiful Message For All The Curvy Girls & It'll Inspire You To No End!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/whatsapp-just-introduced-a-feature-to-secure-the-data-of-1-billion-users-from-hackers-253018.html" class="tint" title="WhatsApp Just Introduced A Feature To Secure The Data Of 1 Billion Users From Hackers">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1459922244_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1459922244_502x234.jpg" border="0" alt="WhatsApp Just Introduced A Feature To Secure The Data Of 1 Billion Users From Hackers" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/whatsapp-just-introduced-a-feature-to-secure-the-data-of-1-billion-users-from-hackers-253018.html" title="WhatsApp Just Introduced A Feature To Secure The Data Of 1 Billion Users From Hackers">
    WhatsApp Just Introduced A Feature To Secure The Data Of 1 Billion Users From Hackers                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/imtiaz-ali-s-progressive-take-on-sex-workers-in-this-latest-short-film-is-a-total-win-253014.html" class="tint" title="Imtiaz Ali's Progressive Take On Sex-Workers In This Latest Short Film Is A Total Win!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/imtiaz-ali-card_1459919135_1459919162_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/imtiaz-ali-card_1459919135_1459919162_502x234.jpg" border="0" alt="Imtiaz Ali's Progressive Take On Sex-Workers In This Latest Short Film Is A Total Win!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/imtiaz-ali-s-progressive-take-on-sex-workers-in-this-latest-short-film-is-a-total-win-253014.html" title="Imtiaz Ali's Progressive Take On Sex-Workers In This Latest Short Film Is A Total Win!">
    Imtiaz Ali's Progressive Take On Sex-Workers In This Latest Short Film Is A Total Win!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/kick-off/an-angry-zlatan-ibrahimovic-once-stuffed-a-teammate-at-ac-milan-into-a-bin-253012.html" class="tint" title="An Angry Zlatan Ibrahimovic Once Stuffed A Teammate At AC Milan Into A Bin!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/bin640_1459918896_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/bin640_1459918896_502x234.jpg" border="0" alt="An Angry Zlatan Ibrahimovic Once Stuffed A Teammate At AC Milan Into A Bin!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/kick-off/an-angry-zlatan-ibrahimovic-once-stuffed-a-teammate-at-ac-milan-into-a-bin-253012.html" title="An Angry Zlatan Ibrahimovic Once Stuffed A Teammate At AC Milan Into A Bin!">
    An Angry Zlatan Ibrahimovic Once Stuffed A Teammate At AC Milan Into A Bin!                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/11-ways-fasting-can-help-you-live-longer-252949.html" class="tint" title="11 Ways Fasting Can Help You Live Longer">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/fb_1459775149_1459775161_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/fb_1459775149_1459775161_502x234.jpg" border="0" alt="11 Ways Fasting Can Help You Live Longer" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/11-ways-fasting-can-help-you-live-longer-252949.html" title="11 Ways Fasting Can Help You Live Longer">
    11 Ways Fasting Can Help You Live Longer                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/11-facts-india-needs-to-know-about-mental-health-after-pratyusha-banerjee-s-death-253010.html" class="tint" title="11 Facts India Needs To Know About Mental Health After Pratyusha Banerjeeâs Death">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/fb1_1459864866_1459864881_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/fb1_1459864866_1459864881_502x234.jpg" border="0" alt="11 Facts India Needs To Know About Mental Health After Pratyusha Banerjeeâs Death" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/11-facts-india-needs-to-know-about-mental-health-after-pratyusha-banerjee-s-death-253010.html" title="11 Facts India Needs To Know About Mental Health After Pratyusha Banerjeeâs Death">
    11 Facts India Needs To Know About Mental Health After Pratyusha Banerjeeâs Death                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/health/healthyliving/we-bet-you-haven-t-heard-of-this-strange-but-fascinating-egg-recipe-252833.html" class="tint" title="We Bet You Havenât Heard Of This Strange But Fascinating Egg Recipe">
                    <img  src="http://media.indiatimes.in/media/content/2016/Mar/card-8_1459428026_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Mar/card-8_1459428026_502x234.jpg" border="0" alt="We Bet You Havenât Heard Of This Strange But Fascinating Egg Recipe" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/we-bet-you-haven-t-heard-of-this-strange-but-fascinating-egg-recipe-252833.html" title="We Bet You Havenât Heard Of This Strange But Fascinating Egg Recipe">
    We Bet You Havenât Heard Of This Strange But Fascinating Egg Recipe                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/play/days-before-ipl-9-supreme-court-blasts-bcci-says-it-has-done-nothing-for-cricket-253001.html" class="tint" title="Days Before IPL-9, Supreme Court Blasts BCCI, Says It Has Done Nothing For Cricket">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/bcci-640_1459857953_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/bcci-640_1459857953_502x234.jpg" border="0" alt="Days Before IPL-9, Supreme Court Blasts BCCI, Says It Has Done Nothing For Cricket" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/play/days-before-ipl-9-supreme-court-blasts-bcci-says-it-has-done-nothing-for-cricket-253001.html" title="Days Before IPL-9, Supreme Court Blasts BCCI, Says It Has Done Nothing For Cricket">
    Days Before IPL-9, Supreme Court Blasts BCCI, Says It Has Done Nothing For Cricket                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/bollywood/farah-khan-rohit-shetty-imtiaz-ali-deconstruct-deepika-padukone-it-s-all-kinds-of-awesome-253005.html" class="tint" title="Farah Khan, Rohit Shetty & Imtiaz Ali 'Deconstruct' Deepika Padukone & It's All Kinds Of Awesome">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/deepika-directors_1459859994_1459860000_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/deepika-directors_1459859994_1459860000_502x234.jpg" border="0" alt="Farah Khan, Rohit Shetty & Imtiaz Ali 'Deconstruct' Deepika Padukone & It's All Kinds Of Awesome" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/bollywood/farah-khan-rohit-shetty-imtiaz-ali-deconstruct-deepika-padukone-it-s-all-kinds-of-awesome-253005.html" title="Farah Khan, Rohit Shetty & Imtiaz Ali 'Deconstruct' Deepika Padukone & It's All Kinds Of Awesome">
    Farah Khan, Rohit Shetty & Imtiaz Ali 'Deconstruct' Deepika Padukone & It's All Kinds Of Awesome                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/news/india/amitabh-bachchan-finally-breaks-his-silence-over-panama-leaks-says-his-name-has-been-misused-253008.html" class="tint" title="Amitabh Bachchan Finally Breaks His Silence Over Panama Leaks, Says His Name Has Been Misused">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/amitabhb_1459864483_1459864490_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/amitabhb_1459864483_1459864490_502x234.jpg" border="0" alt="Amitabh Bachchan Finally Breaks His Silence Over Panama Leaks, Says His Name Has Been Misused" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/news/india/amitabh-bachchan-finally-breaks-his-silence-over-panama-leaks-says-his-name-has-been-misused-253008.html" title="Amitabh Bachchan Finally Breaks His Silence Over Panama Leaks, Says His Name Has Been Misused">
    Amitabh Bachchan Finally Breaks His Silence Over Panama Leaks, Says His Name Has Been Misused                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/technology/nudity-and-fake-names-are-the-top-reasons-social-platforms-take-down-your-posts-253003.html" class="tint" title="Nudity And Fake Names Are The Top Reasons Social Platforms Take Down Your Posts">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/social6_1459858517_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/social6_1459858517_502x234.jpg" border="0" alt="Nudity And Fake Names Are The Top Reasons Social Platforms Take Down Your Posts" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/technology/nudity-and-fake-names-are-the-top-reasons-social-platforms-take-down-your-posts-253003.html" title="Nudity And Fake Names Are The Top Reasons Social Platforms Take Down Your Posts">
    Nudity And Fake Names Are The Top Reasons Social Platforms Take Down Your Posts                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/after-beating-9-years-of-drug-addiction-sanjay-dutt-now-plans-to-save-the-youth-from-drug-abuse-252978.html" class="tint" title="After Beating 9 Years Of Drug Addiction, Sanjay Dutt Now Plans To Save The Youth From Drug Abuse">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/card_1459844260_1459844264_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/card_1459844260_1459844264_502x234.jpg" border="0" alt="After Beating 9 Years Of Drug Addiction, Sanjay Dutt Now Plans To Save The Youth From Drug Abuse" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/after-beating-9-years-of-drug-addiction-sanjay-dutt-now-plans-to-save-the-youth-from-drug-abuse-252978.html" title="After Beating 9 Years Of Drug Addiction, Sanjay Dutt Now Plans To Save The Youth From Drug Abuse">
    After Beating 9 Years Of Drug Addiction, Sanjay Dutt Now Plans To Save The Youth From Drug Abuse                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/lifestyle/self/7-things-that-nail-the-difference-between-dog-lovers-and-dog-parents-253002.html" class="tint" title="7 Things That Nail The Difference Between Dog Lovers And Dog Parents">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/cp3_1459857540_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/cp3_1459857540_502x234.jpg" border="0" alt="7 Things That Nail The Difference Between Dog Lovers And Dog Parents" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/7-things-that-nail-the-difference-between-dog-lovers-and-dog-parents-253002.html" title="7 Things That Nail The Difference Between Dog Lovers And Dog Parents">
    7 Things That Nail The Difference Between Dog Lovers And Dog Parents                    </a>
                </div>    
            </figcaption>
        </div>
            
        <div class="life-panel-list cf"><!--life-panel-list start-->
            <figure>
                        <!--a class="label-tag spon">Sponsored</a-->
                <a href="http://www.indiatimes.com/entertainment/celebs/katrina-kaif-ranveer-singh-and-chris-brown-are-set-to-sizzle-the-ipl-opening-ceremony-this-year-253000.html" class="tint" title="Katrina Kaif, Ranveer Singh And Chris Brown Are Set To Sizzle The IPL Opening Ceremony This Year!">
                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/picmonkey-collage_1459857218_1459857223_502x234.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/picmonkey-collage_1459857218_1459857223_502x234.jpg" border="0" alt="Katrina Kaif, Ranveer Singh And Chris Brown Are Set To Sizzle The IPL Opening Ceremony This Year!" class="img-responsive"/>
                </a>
            </figure>
            <figcaption>
                <div class="white-container">
                    <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/katrina-kaif-ranveer-singh-and-chris-brown-are-set-to-sizzle-the-ipl-opening-ceremony-this-year-253000.html" title="Katrina Kaif, Ranveer Singh And Chris Brown Are Set To Sizzle The IPL Opening Ceremony This Year!">
    Katrina Kaif, Ranveer Singh And Chris Brown Are Set To Sizzle The IPL Opening Ceremony This Year!                    </a>
                </div>    
            </figcaption>
        </div>
    
</div><!--life-panel end-->

<div class="trending-panel cf "><!--trending-panel start-->
    <h2><a target="_blank" href="http://www.indiatimes.com/trending/">trending</a></h2>
                    <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/celebs/hinting-at-pratyusha-banerjee-mahesh-bhatt-says-actresses-suffer-more-abuse-than-domestic-help-253023.html" class="tint" title="Hinting At Pratyusha Banerjee, Mahesh Bhatt Says Actresses Suffer More Abuse Than Domestic Help">

                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/bhatt-card_1459923288_1459923293_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/bhatt-card_1459923288_1459923293_236x111.jpg" border="0" alt="Hinting At Pratyusha Banerjee, Mahesh Bhatt Says Actresses Suffer More Abuse Than Domestic Help"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/hinting-at-pratyusha-banerjee-mahesh-bhatt-says-actresses-suffer-more-abuse-than-domestic-help-253023.html" title="Hinting At Pratyusha Banerjee, Mahesh Bhatt Says Actresses Suffer More Abuse Than Domestic Help">
    Hinting At Pratyusha Banerjee, Mahesh Bhatt Says Actresses Suffer More Abuse Than Domestic Help                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    <div class='container3'>        <div class="trending-panel-list cf colombia" id="div-clmb-ctn-129061-10"  data-slot="129061" data-position="10" data-section="0" data-cb="adwidgetNew"><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/lifestyle/self/11-ways-fasting-can-help-you-live-longer-252949.html" class="tint" title="11 Ways Fasting Can Help You Live Longer">

                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/fb_1459775149_1459775161_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/fb_1459775149_1459775161_236x111.jpg" border="0" alt="11 Ways Fasting Can Help You Live Longer"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/lifestyle/self/11-ways-fasting-can-help-you-live-longer-252949.html" title="11 Ways Fasting Can Help You Live Longer">
    11 Ways Fasting Can Help You Live Longer                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/health/healthyliving/11-facts-india-needs-to-know-about-mental-health-after-pratyusha-banerjee-s-death-253010.html" class="tint" title="11 Facts India Needs To Know About Mental Health After Pratyusha Banerjeeâs Death">

                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/fb1_1459864866_1459864881_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/fb1_1459864866_1459864881_236x111.jpg" border="0" alt="11 Facts India Needs To Know About Mental Health After Pratyusha Banerjeeâs Death"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/health/healthyliving/11-facts-india-needs-to-know-about-mental-health-after-pratyusha-banerjee-s-death-253010.html" title="11 Facts India Needs To Know About Mental Health After Pratyusha Banerjeeâs Death">
    11 Facts India Needs To Know About Mental Health After Pratyusha Banerjeeâs Death                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
            <div class="trending-panel-list cf " ><!--trending-panel-list start-->
                    <figure>
      
    					
                <a href="http://www.indiatimes.com/entertainment/celebs/it-s-special-to-be-ordinary-says-srk-14-quotes-that-prove-he-is-the-most-humble-actor-ever-252960.html" class="tint" title="It's Special To Be Ordinary, Says SRK + 14 Quotes That Prove He Is The Most Humble Actor Ever!">

                    <img  src="http://media.indiatimes.in/media/content/2016/Apr/sk_1459834746_1459834751_236x111.jpg" data-original="http://media.indiatimes.in/media/content/2016/Apr/sk_1459834746_1459834751_236x111.jpg" border="0" alt="It's Special To Be Ordinary, Says SRK + 14 Quotes That Prove He Is The Most Humble Actor Ever!"/>
                </a>
            </figure>
            <figcaption>
            <div class="white-container">
                <a class="bold-txt" href="http://www.indiatimes.com/entertainment/celebs/it-s-special-to-be-ordinary-says-srk-14-quotes-that-prove-he-is-the-most-humble-actor-ever-252960.html" title="It's Special To Be Ordinary, Says SRK + 14 Quotes That Prove He Is The Most Humble Actor Ever!">
    It's Special To Be Ordinary, Says SRK + 14 Quotes That Prove He Is The Most Humble Actor Ever!                </a>
            </div>
            </figcaption>
        </div><!--trending-panel-list end-->
    
</div>

</div><!--trending-panel end-->

</section>
<section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<div class="remove-fixed-home">&nbsp;</div>
<section class="big-ads" id="adfooter">
    <div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">

    $('#bigAd1_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD2('bigAd2_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible     

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd2_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            showBigAD3('bigAd3_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible

            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible  

            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });
    $('#bigAd3_slot').bind('inview', function (event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            // element is now visible in the viewport
            BADros('badRos_slot');
            if (visiblePartY == 'top') {
                // top part of element is visible
            } else if (visiblePartY == 'bottom') {
                // bottom part of element is visible
            } else {
                // whole part of element is visible
            }
        } else {
            // element has gone out of viewport
        }
    });

    $(document).ready(function () {
        /* spotlight onload tracking homepage */
        //console.log("homepage");
    });

</script>    <div class="clr"></div>

	
    <script>
                showFooterCode();
        </script>
<style>
    .bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--131<br>News--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    4,774,793<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>60,496 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>

        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                    <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                            <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                            <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                            <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                                                            <a href='http://www.indiatimes.com/play/' class="yellow size">Slog Overs</a> 
                                                <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a>            
                    <a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                    						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/news/india/'>india</a>
                         
                                        <a href='http://www.indiatimes.com/news/world/'>world</a>
                         
                                        <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                         
                                        <a href='http://www.indiatimes.com/news/weird/'>weird</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                         
                                        <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                         
                                        <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                         
                                        <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                         
                                        <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                         
                                        <a href='http://www.indiatimes.com/culture/food/'>food</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                         
                                        <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                         
                                        <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
            
                            </div>						
                            <div class="sub_link">

                     
                                        <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                         
                                        <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                         
                                        <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                         
                                        <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                         
                                        <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
            
                            </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 

                            <a href="http://timesofindia.indiatimes.com/budgetspecial.cms"  target="_blank">Budget 2016</a> 
                        
                            <a href="http://timesofindia.indiatimes.com/budget-2016/rail-budget-2016/indiabudget/50867848.cms"  target="_blank">Rail Budget 2016</a> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                            <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                            <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                            <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                            <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                            <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                            <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                    </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2016 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $(document).ready(function () {
        $("#subscribers_id").click(function () {
            $("#UserEmail").focus();
        });
    });
    function socialRHSHide() {}
	

</script>

    
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=119"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>




<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=119"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=119"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=119"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>