<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trending  stories on Indian Lifestyle, Culture, Relationships, Food, Travel, Entertainment, News & New Technology News - Indiatimes.com</title>

<meta name="keywords" content="" />
<meta name="description" content="Indiatimes.com brings you the news, articles, stories and videos on entertainment, latest lifestyle, culture & new technologies emerging worldwide." />



		

<link rel="image_src" href="http://media.indiatimes.in/resources/images/fbimage.png" />
<meta property="og:title" content="" />    
<meta property="og:url" content="http://www.indiatimes.com" />
<meta property="og:image" content="http://media.indiatimes.in/resources/images/fbimage.png" />

<meta property="og:site_name" content="indiatimes.com" />
<meta property="fb:app_id" content="117787264903013" />
<meta property="og:description" content="" />
<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />
<meta name="msvalidate.01" content="BAB71AAED7CCC01DB106A58E99625EB4" />
<link href="https://plus.google.com/+indiatimes/posts" rel="publisher" />

<meta property="og:type" content="website" />
    
<!--<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />-->

<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.indiatimes.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">    <link rel="shortcut icon" href="http://media.indiatimes.in/resources/images/favicon.ico?v=25" type="image/x-icon"><link rel="stylesheet" href="http://media.indiatimes.in/resources/css/site.css?v=100.52" media="screen"><link rel="stylesheet" href="http://www.indiatimes.com/fonts/fonts.min.css?v=100.52" media="screen"><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery-1.11.1.min.js?v=100.52"></script><script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/handlebars-v1.3.0.js?v=100.52"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.inview.min.js?v=100.52"></script><script type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.preload.min.js?v=100.52"></script><script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');fbq('init', '853339751421540');fbq('track', 'PageView');</script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=853339751421540&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code -->           <!--  Analytics Code Begin -->    <!-- Begin comScore Tag -->    <script>        var _comscore = _comscore || [];        _comscore.push({ c1: "2", c2: "6036484" });        (function() {            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";            el.parentNode.insertBefore(s, el);        })();    </script>    <noscript>    <img src="http://b.scorecardresearch.com/p?c1=2&c2=6036484&cv=2.0&cj=1" />    </noscript>    <!-- End comScore Tag -->    <!-- Begin Google Analytics Tag -->    <script>        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');        ga('create', 'UA-198011-6', 'indiatimes.com');            ga('require', 'displayfeatures');        ga('send', 'pageview');    </script>    <!-- End Google Analytics Tag -->    <!--  Analytics Code Begin --><!--adcode common script--><script type='text/javascript' src="http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud"></script><script type="text/javascript">    var googletag = googletag || {};    googletag.cmd = googletag.cmd || [];    (function() {        var gads = document.createElement("script");        gads.async = true;        gads.type = "text/javascript";        var useSSL = "https:" == document.location.protocol;        gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";        var node =document.getElementsByTagName("script")[0];        node.parentNode.insertBefore(gads, node);    })();</script> <script type="text/javascript">        var nid='';        var cid='';        var sid='';        var brand_name='';                                                             </script>    <script>
    var contType = "";
    </script>
        <script type="text/javascript" src="http://media.indiatimes.in/resources/js/banner/home.js?v=100.52"></script>
            
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://media.indiatimes.in/js/html5.js?v1.1"></script>
  <![endif]-->
<script>
var isDetailPage = 0</script>
</head>
<body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];   
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script> 	
	
<div id="wrap" class="cf"><!--wrap start-->
    <div class='darkDisabledBg' id='disabledBg'></div>
<header>
    <div class="header-inner">
        <div class="comm-header ">
            <nav>
                <div class='forTop'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>

                    <div class="lnks">
                                                        <a href="http://www.indiatimes.com/news/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','News');">News</a> 
                                                               <a href="http://www.indiatimes.com/lifestyle/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Lifestyle');">Lifestyle</a> 
                                                               <a href="http://www.indiatimes.com/entertainment/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Entertainment');">Entertainment</a> 
                                                               <a href="http://www.indiatimes.com/videocafe/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Videos');">Videos</a> 
                                                               <a href="http://www.indiatimes.com/health/"  target="_blank" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Health');">Health</a> 
                                                                                          <a href='http://www.indiatimes.com/trending'  target="_blank" class="#fffff" onmousedown="ga('send','event', 'HeaderEvents','TopNavigation','Trending');">Trending</a> 
                                                </div>
                </div>
                <div class='onScrolled'> 
                    <a href="http://www.indiatimes.com" class="logo sprite" title="indiatimes">indiatimes logo</a>
                    <a href="javascript:;" class="menu rc sprite" id="headerMenu">Menu</a>
                    <div class="has-tag">
                                           </div>
                </div>

                                
            </nav>
			                 
	    <div id="sticker" style="display:none;"><span class="readTitle"></span></div>
                    <div class="socls fr share">
            </div>
                        
            
			 <div class="social fr">
                            <a title="facebook" target="_blank"  class="sprite fb" href="https://www.facebook.com/indiatimes">facebook</a>
                            <a title="twitter" target="_blank"  class="sprite twt" href="https://twitter.com/indiatimes">twitter</a>
                            <a title="search"  class="sprite serach" onclick="$('body').showSearch();" href="javascript:void(0);">search</a>
                        </div>
        </div>
    </div>
</header>
<div class="clr"></div>
<div id="overlay" class="animated bounceOut"><!--overlay start-->
    <div class="search_conts"><!--search-cont start-->
        <form onsubmit="return false;" class="serach-form">
            <input type="text" onkeyup="search_data();" class="input" onfocus="if(this.value=='Search'){this.value=''}" onblur="if(this.value==''){this.value='Search'};" value="Search" name="q" id="q" autocomplete="off">
            <input type="button" class="sprite src-btn" onclick="search_data();">
            <a class="sprite sclose" id="close" href="javascript:void(0);">X</a>
        </form>
        <div class="scroll-panes" id="scroll-panes">
            <div class="clr"></div>
            <div class="res-msg" id="res-msg"></div>
            <div class="search-list cf" id="results"></div>
            <div class="clr"></div>
            <div class="loadmore" id="progressBar" style="display: none;">Loading......</div>
            <a class="loadmore" style="display:none;" onclick="load_search_data(); " id="load_more_button" href="javascript:void(0);">Load more</a>
        </div>
    </div>
</div>
<div class="blk">
    <div  id="leftMenu" class="leftMenu accordion inner">
        <dl >
            <dt data-color="red-bg" class=""><a href="http://www.indiatimes.com">Home</a></dt>
            <dd> </dd>
                            <dt  data-color="blue-bg" ><a href="http://www.indiatimes.com/news/">News</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/news/india/'>India</a>
                                     
                            <a href='http://www.indiatimes.com/news/world/'>World</a>
                                     
                            <a href='http://www.indiatimes.com/news/sports/'>Sports</a>
                                     
                            <a href='http://www.indiatimes.com/news/weird/'>Weird</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="purple-bg" ><a href="http://www.indiatimes.com/lifestyle/">Lifestyle</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/lifestyle/self/'>Self</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/style/'>Style</a>
                                     
                            <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                     
                            <a href='http://www.indiatimes.com/culture/who-we-are/'>Who we are</a>
                                     
                            <a href='http://www.indiatimes.com/culture/travel/'>Travel</a>
                                     
                            <a href='http://www.indiatimes.com/culture/food/'>Food</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="red-bg" ><a href="http://www.indiatimes.com/entertainment/">Entertainment</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/entertainment/bollywood/'>Bollywood</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/celebs/'>Celebscoop</a>
                                     
                            <a href='http://www.indiatimes.com/entertainment/hollywood/'>Hollywood</a>
                                            </dd>
                    <div class="clr"></div>
                                <dt  data-color="orange-bg" ><a href="http://www.indiatimes.com/videocafe/">Videos</a>
                                </dt>
                                <dt  data-color="green-bg" ><a href="http://www.indiatimes.com/health/">Health</a>
                <a href="javascript:void(0)" class="arrow sprite "></a>                </dt>
                                    <dd >
                                         
                            <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                     
                            <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                     
                            <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                     
                            <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                     
                            <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                            </dd>
                    <div class="clr"></div>
                				<dt data-color="" class=""><a href="http://www.indiatimes.com/trending/">Trending</a>
                           </dt>
            <dt class="pink-bg">
            <div class="follow">Follow indiatimes </div>
            <div class="follow_cont"> 
                <a href="https://www.facebook.com/indiatimes" class="fbleft sprite"> facebook</a> 
                <a href="https://twitter.com/indiatimes" class="twtleft sprite"> twitter</a> 
            </div>
            </dt>

            <dd>&nbsp;</dd>
            <div class="clr"></div>
        </dl>
    </div>
</div>


<div class="clr"></div>
<div class="dummy-cont">&nbsp;</div>
<div class="clr"></div>


<div id="pushdown">
    <div class="pull-ad"><!--pull-ad start-->   
	
                <script type="text/javascript"> 
                    showHeaderCode();
                </script>
	    </div><!--pull-ad end-->
</div>
    <!--testing 15-11-09 23:50:01-->  

    <section id="hp_block_1" class="container cf" style="border: 0px solid red;"><!--container start-->
                

        <div class="news-panel cf"><!--news-panel start-->
            <h2>Big news</h2>

            <div class="feature-list cf"><!--feature-list start-->	
                                    <figure>
                        <div class="hash-tag">
                             1 day ago                         </div>
						 						          
                        <a href="http://www.indiatimes.com/news/india/indian-student-invents-cardboard-incubator-babylifebox-which-will-save-millions-of-lives-across-the-world-247095.html" class=" tint" title="Indian Student Invents Cardboard Incubator Which Will Save Millions Of Lives Across The World">
                            <img  class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/pitchatpalace_1446966413_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/pitchatpalace_1446966413_236x111.jpg"  border="0" alt="Indian Student Invents Cardboard Incubator Which Will Save Millions Of Lives Across The World"/></a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/indian-student-invents-cardboard-incubator-babylifebox-which-will-save-millions-of-lives-across-the-world-247095.html" title="Indian Student Invents Cardboard Incubator Which Will Save Millions Of Lives Across The World">
                            Indian Student Invents Cardboard Incubator Which Will Save Millions Of Lives Across The World                        </a>
                    </figcaption>
                            </div><!--feature-list end-->

                            <div class="news-panel-list cf" id="column1_0"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            5 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/left-without-an-ipl-team-ms-dhoni-could-buy-his-own-franchise-for-2016_-247138.html" title="Left Without An IPL Team, MS Dhoni Could Buy His Own Franchise For 2016" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/dhonicskout_1447071976_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/dhonicskout_1447071976_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/left-without-an-ipl-team-ms-dhoni-could-buy-his-own-franchise-for-2016_-247138.html" title="Left Without An IPL Team, MS Dhoni Could Buy His Own Franchise For 2016">
                            Left Without An IPL Team, MS Dhoni Could Buy His Own Franchise For 2016                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_1"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            7 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/pune-muslims-get-the-spirit-of-diwali-right-donate-rs-12-lakhs-sweets-to-those-who-lost-their-homes-in-a-recent-fire-247131.html" title="Pune Muslims Get The Spirit Of Diwali Right. Donate Rs 12 Lakhs, Sweets To Those Who lost Their Homes In A Recent Fire!" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/donation-card_1447067122_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/donation-card_1447067122_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/pune-muslims-get-the-spirit-of-diwali-right-donate-rs-12-lakhs-sweets-to-those-who-lost-their-homes-in-a-recent-fire-247131.html" title="Pune Muslims Get The Spirit Of Diwali Right. Donate Rs 12 Lakhs, Sweets To Those Who lost Their Homes In A Recent Fire!">
                            Pune Muslims Get The Spirit Of Diwali Right. Donate Rs 12 Lakhs, Sweets To Those Who lost Their Homes In A Recent Fire!                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_2"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            8 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/india/indians-to-sue-queen-of-england-will-fight-tooth-and-nail-to-win-back-the-kohinoor-diamond-247129.html" title="Indians To Sue Queen Of England, Will Fight Tooth And Nail To Win Back The Kohinoor Diamond" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/502_1447064095_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/502_1447064095_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/indians-to-sue-queen-of-england-will-fight-tooth-and-nail-to-win-back-the-kohinoor-diamond-247129.html" title="Indians To Sue Queen Of England, Will Fight Tooth And Nail To Win Back The Kohinoor Diamond">
                            Indians To Sue Queen Of England, Will Fight Tooth And Nail To Win Back The Kohinoor Diamond                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                            <div class="news-panel-list cf" id="column1_3"><!--news-panel-list start-->
                    <figure>
                        <div class="hash-tag">
                            9 hours ago                         </div>
                        		
						 	                        <a href="http://www.indiatimes.com/news/sports/n-srinivasan-s-cricket-reign-ends-removed-as-icc-chairman-bcci-president-shashank-manohar-to-take-over-247116.html" title="N Srinivasan's Cricket Reign Ends, Removed As ICC Chairman. BCCI President Shashank Manohar To Take Over" class=" tint">                       
                            <img class="greyBg23 photolazy23" src="http://media.indiatimes.in/media/content/2015/Nov/srinivasanmanoharbccl_1447050584_236x111.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/srinivasanmanoharbccl_1447050584_236x111.jpg" border="0" alt=""/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/sports/n-srinivasan-s-cricket-reign-ends-removed-as-icc-chairman-bcci-president-shashank-manohar-to-take-over-247116.html" title="N Srinivasan's Cricket Reign Ends, Removed As ICC Chairman. BCCI President Shashank Manohar To Take Over">
                            N Srinivasan's Cricket Reign Ends, Removed As ICC Chairman. BCCI President Shashank Manohar To Take Over                        </a>
                    </figcaption> 
                </div><!--news-panel-list end-->
                    </div><!--news-panel end-->

        <div class="life-panel cf" id="middleBlock1"><!--life-panel start-->
            <h2>lifestyle</h2>

                                <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/news/weird/the-black-knight-alien-satellite-has-kept-conspiracy-theorists-busy-for-half-a-century-now-a-strange-letter-from-a-girl-promises-to-add-to-the-mystery-246564.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/news/weird/the-black-knight-alien-satellite-has-kept-conspiracy-theorists-busy-for-half-a-century-now-a-strange-letter-from-a-girl-promises-to-add-to-the-mystery-246564.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Oct/card_1445607453_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Oct/card_1445607453_502x234.jpg"  border="0" alt="An Alien Satellite Has Kept Conspiracy Theorists Busy For Half A Century. Now A Strange Letter Adds A New Twist" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/news/weird/the-black-knight-alien-satellite-has-kept-conspiracy-theorists-busy-for-half-a-century-now-a-strange-letter-from-a-girl-promises-to-add-to-the-mystery-246564.html" title="An Alien Satellite Has Kept Conspiracy Theorists Busy For Half A Century. Now A Strange Letter Adds A New Twist" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/news/weird/the-black-knight-alien-satellite-has-kept-conspiracy-theorists-busy-for-half-a-century-now-a-strange-letter-from-a-girl-promises-to-add-to-the-mystery-246564.html');">An Alien Satellite Has Kept Conspiracy Theorists Busy For Half A Century. Now A Strange Letter Adds A New Twist</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/lifestyle/technology/10-ways-you-can-stop-burning-money-this-diwali-247001.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/technology/10-ways-you-can-stop-burning-money-this-diwali-247001.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Nov/card-image_1447050564_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card-image_1447050564_502x234.jpg"  border="0" alt="10 Ways You Can Stop Burning Money This Diwali!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/lifestyle/technology/10-ways-you-can-stop-burning-money-this-diwali-247001.html" title="10 Ways You Can Stop Burning Money This Diwali!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/lifestyle/technology/10-ways-you-can-stop-burning-money-this-diwali-247001.html');">10 Ways You Can Stop Burning Money This Diwali!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                                    <div class="life-panel-list cf"><!--life-panel-list start-->
                        <figure><span class="label-tag spon">Partner</span>
                            
                               
                            <a href="http://www.indiatimes.com/culture/who-we-are/9-sports-partnerships-that-really-worked-and-how-247023.html" class=" tint" title="" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/9-sports-partnerships-that-really-worked-and-how-247023.html');">
                                <img  class="greyBg23 lazy23"  src="http://media.indiatimes.in/media/content/2015/Nov/card_1446725621_502x234.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/card_1446725621_502x234.jpg"  border="0" alt="9 Sports Partnerships That Really Worked and How!" class="img-responsive"/>
                            </a>
                        </figure>
                        <figcaption>
                            <a href="http://www.indiatimes.com/culture/who-we-are/9-sports-partnerships-that-really-worked-and-how-247023.html" title="9 Sports Partnerships That Really Worked and How!" onmousedown="ga('send', 'event', 'HomePageMiddleColumn', 'ClickonHomePageMiddleColumnStories','http://www.indiatimes.com/culture/who-we-are/9-sports-partnerships-that-really-worked-and-how-247023.html');">9 Sports Partnerships That Really Worked and How!</a>
                        </figcaption>
                    </div><!--life-panel-list end-->
                

                        
        </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_0">
                    <span class="strip skyblue-bg"></span>                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-message-by-indian-soldiers-is-for-all-those-who-think-that-india-is-intolerant-247113.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/this-message-by-indian-soldiers-is-for-all-those-who-think-that-india-is-intolerant-247113.html" class="tint" title="This Message By Indian Soldiers Is For All Those Who Think That India Is Intolerant!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/indiansoldiers_card_1447051144_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/indiansoldiers_card_1447051144_218x102.jpg" border="0" alt="This Message By Indian Soldiers Is For All Those Who Think That India Is Intolerant!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-message-by-indian-soldiers-is-for-all-those-who-think-that-india-is-intolerant-247113.html" title="This Message By Indian Soldiers Is For All Those Who Think That India Is Intolerant!">
                            This Message By Indian Soldiers Is For All Those Who Think That India Is Intolerant!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_1">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/dilwale-s-trailer-is-out-it-s-so-good-you-d-want-to-give-us-a-hug-for-sharing-it-247140.html'>video</a>                        <a href="http://www.indiatimes.com/entertainment/bollywood/dilwale-s-trailer-is-out-it-s-so-good-you-d-want-to-give-us-a-hug-for-sharing-it-247140.html" class="tint" title="Dilwale's Trailer Is Out & It's So Good You'd Want To Give Us A Hug For Sharing It!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/ds_1447084114_1447084121_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/ds_1447084114_1447084121_218x102.jpg" border="0" alt="Dilwale's Trailer Is Out & It's So Good You'd Want To Give Us A Hug For Sharing It!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/dilwale-s-trailer-is-out-it-s-so-good-you-d-want-to-give-us-a-hug-for-sharing-it-247140.html" title="Dilwale's Trailer Is Out & It's So Good You'd Want To Give Us A Hug For Sharing It!">
                            Dilwale's Trailer Is Out & It's So Good You'd Want To Give Us A Hug For Sharing It!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_2">
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-horror-version-of-taare-zameen-par-is-so-scary-it-will-make-cry-like-a-kid-247136.html'>video</a>                        <a href="http://www.indiatimes.com/videocafe/this-horror-version-of-taare-zameen-par-is-so-scary-it-will-make-cry-like-a-kid-247136.html" class="tint" title="This Horror Version Of 'Taare Zameen Par' Is So Scary It Will Make Cry Like A Kid!">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/videocafe/2015/Nov/tzp_card_1447072447_218x102.jpg" data-original23="http://media.indiatimes.in/media/videocafe/2015/Nov/tzp_card_1447072447_218x102.jpg" border="0" alt="This Horror Version Of 'Taare Zameen Par' Is So Scary It Will Make Cry Like A Kid!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-horror-version-of-taare-zameen-par-is-so-scary-it-will-make-cry-like-a-kid-247136.html" title="This Horror Version Of 'Taare Zameen Par' Is So Scary It Will Make Cry Like A Kid!">
                            This Horror Version Of 'Taare Zameen Par' Is So Scary It Will Make Cry Like A Kid!                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_3">
                                        <figure>

                                                <a href="http://www.indiatimes.com/health/recipes/5-diabetic-friendly-dessert-recipes-for-the-festive-season-247020.html" class="tint" title="5 Diabetic-Friendly Dessert Recipes For The Festive Season">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/cover_1446721026_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/cover_1446721026_218x102.jpg" border="0" alt="5 Diabetic-Friendly Dessert Recipes For The Festive Season"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/recipes/5-diabetic-friendly-dessert-recipes-for-the-festive-season-247020.html" title="5 Diabetic-Friendly Dessert Recipes For The Festive Season">
                            5 Diabetic-Friendly Dessert Recipes For The Festive Season                        </a>
                    </figcaption>
                </div>
                                <div class="trending-panel-list cf" id="column3_4">
                                        <figure>

                                                <a href="http://www.indiatimes.com/lifestyle/self/9-ways-smart-and-intelligent-people-have-trouble-finding-love-246992.html" class="tint" title="9 Ways Smart And Intelligent People Have Trouble Finding Love">


                            <img  class="greyBg23 lazy23" src="http://media.indiatimes.in/media/content/2015/Nov/deepika-card1_1447048987_1447049000_218x102.jpg" data-original23="http://media.indiatimes.in/media/content/2015/Nov/deepika-card1_1447048987_1447049000_218x102.jpg" border="0" alt="9 Ways Smart And Intelligent People Have Trouble Finding Love"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/9-ways-smart-and-intelligent-people-have-trouble-finding-love-246992.html" title="9 Ways Smart And Intelligent People Have Trouble Finding Love">
                            9 Ways Smart And Intelligent People Have Trouble Finding Love                        </a>
                    </figcaption>
                </div>
                        </div><!--trending-panel end-->
    </section><!--container end-->

<!-- -------------------    end Block 1  ------------------------------------- -->
<section class="big-ads" id="ad1"> 
<div id="bigAd1_slot"></div>
    <script>        
        showBigAD1('bigAd1_slot');
    </script> 
</section>
<!-- -------------------    start Block 2  ------------------------------------- -->

    <section id="hp_block_2" class="container cf" style="border: 0px solid red;"><!--container start-->
        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
                            <div class="news-panel-list cf" id="column1_4"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            9 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/this-goan-girl-s-first-hand-account-of-mumbai-s-taxi-scam-will-leave-you-baffled-and-scared-at-the-same-time-247124.html" title="This Goan Girl's First-Hand Account Of Mumbai's Taxi Scam Will Leave You Baffled And Scared At The Same Time!" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447059056_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/this-goan-girl-s-first-hand-account-of-mumbai-s-taxi-scam-will-leave-you-baffled-and-scared-at-the-same-time-247124.html" title="This Goan Girl's First-Hand Account Of Mumbai's Taxi Scam Will Leave You Baffled And Scared At The Same Time!">
                            This Goan Girl's First-Hand Account Of Mumbai's Taxi Scam Will Leave You Baffled And Scared At The Same Time!                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_5"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            10 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/chennai-s-young-and-restless-rise-up-against-the-beef-insanity-beef-parties-are-the-new-norm-in-the-city-247125.html" title="Chennai's Young And Restless Rise Up Against The Beef Insanity. Beef Parties Are The New Norm In The City" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/beef-party-502_1447060466_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/chennai-s-young-and-restless-rise-up-against-the-beef-insanity-beef-parties-are-the-new-norm-in-the-city-247125.html" title="Chennai's Young And Restless Rise Up Against The Beef Insanity. Beef Parties Are The New Norm In The City">
                            Chennai's Young And Restless Rise Up Against The Beef Insanity. Beef Parties Are The New Norm In The City                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_6"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            10 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/maggi-is-back-nestle-relaunches-india-s-favourite-noodles-across-shops-nationwide-and-on-snapdeal-247120.html" title="Maggi Is Back!!! Nestle Relaunches India's Favourite Noodles Across Shops Nationwide And Online" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447056056_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/maggi-is-back-nestle-relaunches-india-s-favourite-noodles-across-shops-nationwide-and-on-snapdeal-247120.html" title="Maggi Is Back!!! Nestle Relaunches India's Favourite Noodles Across Shops Nationwide And Online">
                            Maggi Is Back!!! Nestle Relaunches India's Favourite Noodles Across Shops Nationwide And Online                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_7"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            10 hours ago                         </div>
                       
                         
						
						 <a class='video-btn sprite' href='http://www.indiatimes.com/news/sports/this-video-of-sehwag-sachin-batting-together-will-tell-you-why-we-still-worship-our-heroes-247119.html'>video</a>                        <a href="http://www.indiatimes.com/news/sports/this-video-of-sehwag-sachin-batting-together-will-tell-you-why-we-still-worship-our-heroes-247119.html" title="This Video Of Sehwag, Sachin Batting Together Will Tell You Why We Still Worship Our Heroes" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/sachinsehwagallstars_1447054845_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/sports/this-video-of-sehwag-sachin-batting-together-will-tell-you-why-we-still-worship-our-heroes-247119.html" title="This Video Of Sehwag, Sachin Batting Together Will Tell You Why We Still Worship Our Heroes">
                            This Video Of Sehwag, Sachin Batting Together Will Tell You Why We Still Worship Our Heroes                        </a>
                    </figcaption> 
                </div>
                            <div class="news-panel-list cf" id="column1_8"><!--news-panel-list start-->
                    <figure>
                         <div class="hash-tag">
                            11 hours ago                         </div>
                       
                         
						
						                         <a href="http://www.indiatimes.com/news/india/dadri-victim-s-son-sartaj-calls-bihar-results-a-tribute-to-his-father-mohammad-akhlaq-247110.html" title="#DadriLynching Victim's Son, Sartaj Calls Bihar Results 'A Tribute' To His Father" class=" tint">
                           

                            <img  class="greyBg23 photolazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/sartaj502_1447050559_236x111.jpg" border="0" alt=""/>
                        </a>
                    </figure>
                    <figcaption>
                        
                        <a href="http://www.indiatimes.com/news/india/dadri-victim-s-son-sartaj-calls-bihar-results-a-tribute-to-his-father-mohammad-akhlaq-247110.html" title="#DadriLynching Victim's Son, Sartaj Calls Bihar Results 'A Tribute' To His Father">
                            #DadriLynching Victim's Son, Sartaj Calls Bihar Results 'A Tribute' To His Father                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/entertainment/bollywood/dilwale-s-trailer-is-out-it-s-so-good-you-d-want-to-give-us-a-hug-for-sharing-it-247140.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/entertainment/bollywood/dilwale-s-trailer-is-out-it-s-so-good-you-d-want-to-give-us-a-hug-for-sharing-it-247140.html" class="tint" title="Dilwale's Trailer Is Out & It's So Good You'd Want To Give Us A Hug For Sharing It!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/ds_1447084114_1447084121_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/dilwale-s-trailer-is-out-it-s-so-good-you-d-want-to-give-us-a-hug-for-sharing-it-247140.html" title="Dilwale's Trailer Is Out & It's So Good You'd Want To Give Us A Hug For Sharing It!">
                            Dilwale's Trailer Is Out & It's So Good You'd Want To Give Us A Hug For Sharing It!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/if-you-aren-t-clear-about-diwali-s-history-this-rude-and-hilarious-diwali-carol-is-a-great-place-to-start-247141.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/if-you-aren-t-clear-about-diwali-s-history-this-rude-and-hilarious-diwali-carol-is-a-great-place-to-start-247141.html" class="tint" title="If You Aren't Clear About Diwali's History - This Rude (And Hilarious) Diwali Carol Is A Great Place To Start!">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/diwali-carol502_1447084583_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/if-you-aren-t-clear-about-diwali-s-history-this-rude-and-hilarious-diwali-carol-is-a-great-place-to-start-247141.html" title="If You Aren't Clear About Diwali's History - This Rude (And Hilarious) Diwali Carol Is A Great Place To Start!">
                            If You Aren't Clear About Diwali's History - This Rude (And Hilarious) Diwali Carol Is A Great Place To Start!                        </a>
                    </figcaption>
                </div>
                    
                <div class="life-panel-list cf"><!--life-panel-list start-->
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-times-indian-religious-festivals-became-a-deathtrap-for-innocent-lives-247058.html" class="tint" title="9 Times Indian Religious Festivals Became A Deathtrap For Innocent Lives">
                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446809723_502x234.jpg" border="0" alt="" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/9-times-indian-religious-festivals-became-a-deathtrap-for-innocent-lives-247058.html" title="9 Times Indian Religious Festivals Became A Deathtrap For Innocent Lives">
                            9 Times Indian Religious Festivals Became A Deathtrap For Innocent Lives                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_5"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/stressed-all-the-time-here-s-how-it-is-making-you-sick-247014.html'>video</a>
                        <a href="http://www.indiatimes.com/health/healthyliving/stressed-all-the-time-here-s-how-it-is-making-you-sick-247014.html" class="tint" title="Stressed All The Time? Here's How It Is Making You Sick">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card_1446717649_218x102.jpg" border="0" alt="Stressed All The Time? Here's How It Is Making You Sick"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/stressed-all-the-time-here-s-how-it-is-making-you-sick-247014.html" title="Stressed All The Time? Here's How It Is Making You Sick">
                            Stressed All The Time? Here's How It Is Making You Sick                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_6"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/buzz/dying-doctor-describes-exactly-what-it-feels-like-to-die-from-a-snakebite-247128.html" class="tint" title="Dying Doctor Describes Exactly What It Feels Like To Die From A Snakebite">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447063893_218x102.jpg" border="0" alt="Dying Doctor Describes Exactly What It Feels Like To Die From A Snakebite"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/dying-doctor-describes-exactly-what-it-feels-like-to-die-from-a-snakebite-247128.html" title="Dying Doctor Describes Exactly What It Feels Like To Die From A Snakebite">
                            Dying Doctor Describes Exactly What It Feels Like To Die From A Snakebite                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_7"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/in-an-epic-role-reversal-srk-does-a-prem-from-prdp-and-salman-a-raj-from-ddlj-bromance-much-247108.html" class="tint" title="In An Epic Role Reversal, SRK Does A Prem From PRDP And Salman A Raj From DDLJ! Bromance, Much?">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447045172_1447045179_218x102.jpg" border="0" alt="In An Epic Role Reversal, SRK Does A Prem From PRDP And Salman A Raj From DDLJ! Bromance, Much?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/in-an-epic-role-reversal-srk-does-a-prem-from-prdp-and-salman-a-raj-from-ddlj-bromance-much-247108.html" title="In An Epic Role Reversal, SRK Does A Prem From PRDP And Salman A Raj From DDLJ! Bromance, Much?">
                            In An Epic Role Reversal, SRK Does A Prem From PRDP And Salman A Raj From DDLJ! Bromance, Much?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_8"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/entertainment/bollywood-s-cleanest-filmmaker-sooraj-barjatya-s-prdp-hits-rough-weather-gets-3-cuts-from-the-censor-board-247130.html" class="tint" title="Bollywood's Cleanest Filmmaker Sooraj Barjatya's PRDP Hits Rough Weather, Gets 3 Cuts From The Censor Board">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/13_1447065716_1447065726_218x102.jpg" border="0" alt="Bollywood's Cleanest Filmmaker Sooraj Barjatya's PRDP Hits Rough Weather, Gets 3 Cuts From The Censor Board"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood-s-cleanest-filmmaker-sooraj-barjatya-s-prdp-hits-rough-weather-gets-3-cuts-from-the-censor-board-247130.html" title="Bollywood's Cleanest Filmmaker Sooraj Barjatya's PRDP Hits Rough Weather, Gets 3 Cuts From The Censor Board">
                            Bollywood's Cleanest Filmmaker Sooraj Barjatya's PRDP Hits Rough Weather, Gets 3 Cuts From The Censor Board                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_9"><!--trending-panel-list start-->
                                        <figure>

                        
                        <a href="http://www.indiatimes.com/health/buzz/rice-fine-flour-rotis-and-upma-are-fuelling-the-diabetes-epidemic-across-india-247036.html" class="tint" title="Rice, Fine-Flour Rotis And Upma Are Fuelling The Diabetes Epidemic Across India">

                            <img   class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-1_1446792566_218x102.jpg" border="0" alt="Rice, Fine-Flour Rotis And Upma Are Fuelling The Diabetes Epidemic Across India"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/buzz/rice-fine-flour-rotis-and-upma-are-fuelling-the-diabetes-epidemic-across-india-247036.html" title="Rice, Fine-Flour Rotis And Upma Are Fuelling The Diabetes Epidemic Across India">
                            Rice, Fine-Flour Rotis And Upma Are Fuelling The Diabetes Epidemic Across India                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div>

    </section><!--container end-->

<!--------------------------- end Block2 ------------------------------------------>
<section class="big-ads" id="ad2"> 
<div id="bigAd2_slot"></div>
</section>
<!--------------------------- start Block3 ------------------------------------------>

    <section id="hp_block_3" class="container cf"><!--container start-->

        <div class="news-panel cf"><!--news-panel start-->
            <h2>news</h2>
          
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_9">
                    <figure>
                        <div class="hash-tag">
                            11 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/weird/the-black-knight-alien-satellite-has-kept-conspiracy-theorists-busy-for-half-a-century-now-a-strange-letter-from-a-girl-promises-to-add-to-the-mystery-246564.html" title="An Alien Satellite Has Kept Conspiracy Theorists Busy For Half A Century. Now A Strange Letter Adds A New Twist" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/card_1445607453_236x111.jpg" border="0" alt="An Alien Satellite Has Kept Conspiracy Theorists Busy For Half A Century. Now A Strange Letter Adds A New Twist"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/the-black-knight-alien-satellite-has-kept-conspiracy-theorists-busy-for-half-a-century-now-a-strange-letter-from-a-girl-promises-to-add-to-the-mystery-246564.html" title="An Alien Satellite Has Kept Conspiracy Theorists Busy For Half A Century. Now A Strange Letter Adds A New Twist">
                            An Alien Satellite Has Kept Conspiracy Theorists Busy For Half A Century. Now A Strange Letter Adds A New Twist                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_10">
                    <figure>
                        <div class="hash-tag">
                            12 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/this-man-tells-us-why-he-voted-against-his-mother-a-bjp-candidate-in-recent-elections-she-lost-by-just-1-vote-247112.html" title="This Man Tells Us Why He Voted Against His Mother, A BJP Candidate In Recent Elections. She Lost By Just 1 Vote!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/man-502-vote-mom_1447051425_236x111.jpg" border="0" alt="This Man Tells Us Why He Voted Against His Mother, A BJP Candidate In Recent Elections. She Lost By Just 1 Vote!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/this-man-tells-us-why-he-voted-against-his-mother-a-bjp-candidate-in-recent-elections-she-lost-by-just-1-vote-247112.html" title="This Man Tells Us Why He Voted Against His Mother, A BJP Candidate In Recent Elections. She Lost By Just 1 Vote!">
                            This Man Tells Us Why He Voted Against His Mother, A BJP Candidate In Recent Elections. She Lost By Just 1 Vote!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_11">
                    <figure>
                        <div class="hash-tag">
                            13 hours ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/india/twinkle-khanna-blogs-on-why-she-is-not-a-feminist-and-it-ll-make-bra-burning-feminists-really-angry-247109.html" title="Twinkle Khanna Blogs On Why She Is Not A Feminist And It'll Make 'Bra Burning' Feminists Really Angry!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/car_1447051792_236x111.jpg" border="0" alt="Twinkle Khanna Blogs On Why She Is Not A Feminist And It'll Make 'Bra Burning' Feminists Really Angry!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/twinkle-khanna-blogs-on-why-she-is-not-a-feminist-and-it-ll-make-bra-burning-feminists-really-angry-247109.html" title="Twinkle Khanna Blogs On Why She Is Not A Feminist And It'll Make 'Bra Burning' Feminists Really Angry!">
                            Twinkle Khanna Blogs On Why She Is Not A Feminist And It'll Make 'Bra Burning' Feminists Really Angry!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_12">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/weird/another-first-for-saudi-arabia-bans-rama-and-49-other-blasphemous-names-247106.html" title="Another First For Saudi Arabia. Bans 'Rama' And 49 Other 'Blasphemous' Names!" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/name2_1446980635_1446980640_236x111.jpg" border="0" alt="Another First For Saudi Arabia. Bans 'Rama' And 49 Other 'Blasphemous' Names!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/weird/another-first-for-saudi-arabia-bans-rama-and-49-other-blasphemous-names-247106.html" title="Another First For Saudi Arabia. Bans 'Rama' And 49 Other 'Blasphemous' Names!">
                            Another First For Saudi Arabia. Bans 'Rama' And 49 Other 'Blasphemous' Names!                        </a>
                    </figcaption> 
                </div>
                            <!--news-panel-list start-->
                <div class="news-panel-list cf" id="column1_13">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>

                                                
                        <a href="http://www.indiatimes.com/news/twitter-reacts-to-biharelections-the-only-way-it-can-lolfest-247103.html" title="Twitter Reacts To #BiharElections The Only Way It Can! #LOLfest" class=" tint">
                            

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/be2_1446978533_236x111.jpg" border="0" alt="Twitter Reacts To #BiharElections The Only Way It Can! #LOLfest"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/twitter-reacts-to-biharelections-the-only-way-it-can-lolfest-247103.html" title="Twitter Reacts To #BiharElections The Only Way It Can! #LOLfest">
                            Twitter Reacts To #BiharElections The Only Way It Can! #LOLfest                        </a>
                    </figcaption> 
                </div>
                    </div><!--news-panel end-->

        <div class="life-panel cf"><!--life-panel start-->
            <h2>lifestyle</h2>
                     
                <div class="life-panel-list cf">
                    <figure>

                                                  
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/health/recipes/5-diabetic-friendly-dessert-recipes-for-the-festive-season-247020.html" class="tint" title="5 Diabetic-Friendly Dessert Recipes For The Festive Season">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1446721026_502x234.jpg" border="0" alt="http://www.indiatimes.com/health/recipes/5-diabetic-friendly-dessert-recipes-for-the-festive-season-247020.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/recipes/5-diabetic-friendly-dessert-recipes-for-the-festive-season-247020.html" title="5 Diabetic-Friendly Dessert Recipes For The Festive Season">
                            5 Diabetic-Friendly Dessert Recipes For The Festive Season                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/the-graff-diamonds-robbery-in-london-9-most-brilliant-robberies-ever-caught-on-video-247137.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/the-graff-diamonds-robbery-in-london-9-most-brilliant-robberies-ever-caught-on-video-247137.html" class="tint" title="The Graff Diamonds Robbery In London + 9 Most Brilliant Robberies Ever Caught On Video!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/robberies_card_1447073714_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/the-graff-diamonds-robbery-in-london-9-most-brilliant-robberies-ever-caught-on-video-247137.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/the-graff-diamonds-robbery-in-london-9-most-brilliant-robberies-ever-caught-on-video-247137.html" title="The Graff Diamonds Robbery In London + 9 Most Brilliant Robberies Ever Caught On Video!">
                            The Graff Diamonds Robbery In London + 9 Most Brilliant Robberies Ever Caught On Video!                        </a>
                    </figcaption>
                </div>
                     
                <div class="life-panel-list cf">
                    <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-horror-version-of-taare-zameen-par-is-so-scary-it-will-make-cry-like-a-kid-247136.html'>video</a>                          
                        <!--a class="label-tag spon">Sponsored</a-->
                        <a href="http://www.indiatimes.com/videocafe/this-horror-version-of-taare-zameen-par-is-so-scary-it-will-make-cry-like-a-kid-247136.html" class="tint" title="This Horror Version Of 'Taare Zameen Par' Is So Scary It Will Make Cry Like A Kid!">
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/tzp_card_1447072447_502x234.jpg" border="0" alt="http://www.indiatimes.com/videocafe/this-horror-version-of-taare-zameen-par-is-so-scary-it-will-make-cry-like-a-kid-247136.html" class="img-responsive"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/videocafe/this-horror-version-of-taare-zameen-par-is-so-scary-it-will-make-cry-like-a-kid-247136.html" title="This Horror Version Of 'Taare Zameen Par' Is So Scary It Will Make Cry Like A Kid!">
                            This Horror Version Of 'Taare Zameen Par' Is So Scary It Will Make Cry Like A Kid!                        </a>
                    </figcaption>
                </div>
                    </div><!--life-panel end-->

        <div class="trending-panel cf"><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_10"><!--trending-panel-list start-->
                                        <figure>

                        <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/this-10-minute-full-body-workout-will-blast-that-fat-away-247035.html'>video</a>		

                        <a href="http://www.indiatimes.com/health/healthyliving/this-10-minute-full-body-workout-will-blast-that-fat-away-247035.html" class="tint" title="This 10 Minute Full Body Workout Will Blast That Fat Away!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card-2_1446790598_218x102.jpg" border="0" alt="This 10 Minute Full Body Workout Will Blast That Fat Away!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/healthyliving/this-10-minute-full-body-workout-will-blast-that-fat-away-247035.html" title="This 10 Minute Full Body Workout Will Blast That Fat Away!">
                            This 10 Minute Full Body Workout Will Blast That Fat Away!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_11"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/culture/who-we-are/horror-of-horrors-13-signs-you-re-turning-into-one-of-your-parents-247064.html" class="tint" title="Horror Of Horrors! 13 Signs You're Turning Into One Of Your Parents">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cp_1446896146_218x102.jpg" border="0" alt="Horror Of Horrors! 13 Signs You're Turning Into One Of Your Parents"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/horror-of-horrors-13-signs-you-re-turning-into-one-of-your-parents-247064.html" title="Horror Of Horrors! 13 Signs You're Turning Into One Of Your Parents">
                            Horror Of Horrors! 13 Signs You're Turning Into One Of Your Parents                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_12"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/health/videos/11-famous-ladies-on-how-empowering-it-was-to-openly-discuss-their-orgasm-247133.html" class="tint" title="11 Famous Ladies On How Empowering It Was To Openly Discuss Their Orgasm!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/fb_1447070283_218x102.jpg" border="0" alt="11 Famous Ladies On How Empowering It Was To Openly Discuss Their Orgasm!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/health/videos/11-famous-ladies-on-how-empowering-it-was-to-openly-discuss-their-orgasm-247133.html" title="11 Famous Ladies On How Empowering It Was To Openly Discuss Their Orgasm!">
                            11 Famous Ladies On How Empowering It Was To Openly Discuss Their Orgasm!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_13"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/celebs/deepika-padukone-thinks-she-has-a-raw-and-sensual-chemistry-with-ranveer-singh-well-so-do-we-247114.html" class="tint" title="Deepika Padukone Thinks She Has A Raw And Sensual Chemistry With Ranveer Singh. Well, So Do We!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ramleela_1447051190_1447051199_218x102.jpg" border="0" alt="Deepika Padukone Thinks She Has A Raw And Sensual Chemistry With Ranveer Singh. Well, So Do We!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/deepika-padukone-thinks-she-has-a-raw-and-sensual-chemistry-with-ranveer-singh-well-so-do-we-247114.html" title="Deepika Padukone Thinks She Has A Raw And Sensual Chemistry With Ranveer Singh. Well, So Do We!">
                            Deepika Padukone Thinks She Has A Raw And Sensual Chemistry With Ranveer Singh. Well, So Do We!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_14"><!--trending-panel-list start-->
                                        <figure>

                        		

                        <a href="http://www.indiatimes.com/entertainment/kapil-sharma-s-comic-timing-takes-him-for-a-toss-gets-accused-of-misbehaving-with-a-marathi-actress-at-a-party-247117.html" class="tint" title="Kapil Sharma's Comic Timing Takes Him For A Toss, Gets Accused Of Misbehaving With A Marathi Actress At A Party!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kapil_1447053850_1447053859_218x102.jpg" border="0" alt="Kapil Sharma's Comic Timing Takes Him For A Toss, Gets Accused Of Misbehaving With A Marathi Actress At A Party!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/kapil-sharma-s-comic-timing-takes-him-for-a-toss-gets-accused-of-misbehaving-with-a-marathi-actress-at-a-party-247117.html" title="Kapil Sharma's Comic Timing Takes Him For A Toss, Gets Accused Of Misbehaving With A Marathi Actress At A Party!">
                            Kapil Sharma's Comic Timing Takes Him For A Toss, Gets Accused Of Misbehaving With A Marathi Actress At A Party!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                        </div><!--trending-panel end-->

    </section><!--container end-->

<!------------------------------- end Block 3 ---------------------------------------->
<section class="big-ads">
<div id="bigAd3_slot"></div>
</section>

<!------------------------------- start Block4  -------------------------------------->

    <section class="container cf" id="container4"><!--container start-->
        <div class="news-panel cf ">
            <h2>news</h2>

                            <div class="news-panel-list cf" id="column1_14">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/bihar-results-meet-the-man-behind-nitish-kumar-s-bihar-vijay-and-narendra-modi-s-national-victories-prakash-kishore-247104.html" title="Meet The Man Behind Nitish Kumar's Bihar Vijay And Narendra Modi's 2014 Landslide Victory, Prashant Kishor" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kishor-600_1446979033_1446979038_236x111.jpg" border="0" alt="Meet The Man Behind Nitish Kumar's Bihar Vijay And Narendra Modi's 2014 Landslide Victory, Prashant Kishor"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/bihar-results-meet-the-man-behind-nitish-kumar-s-bihar-vijay-and-narendra-modi-s-national-victories-prakash-kishore-247104.html" title="Meet The Man Behind Nitish Kumar's Bihar Vijay And Narendra Modi's 2014 Landslide Victory, Prashant Kishor">
                            Meet The Man Behind Nitish Kumar's Bihar Vijay And Narendra Modi's 2014 Landslide Victory, Prashant Kishor                        </a>
                    </figcaption> 
                </div>
                <div class='container1'>                <div class="news-panel-list cf" id="column1_15">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/even-as-prime-minister-narendra-modi-travels-the-world-on-taxpayer-money-his-wife-jashodaben-is-struggling-just-to-get-a-passport-in-gujarat-247102.html" title="Even As PM Modi Travels The World On Taxpayer Money, His Wife Is Struggling Just To Get A Passport" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/jashoda-5032_1446982956_236x111.jpg" border="0" alt="Even As PM Modi Travels The World On Taxpayer Money, His Wife Is Struggling Just To Get A Passport"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/even-as-prime-minister-narendra-modi-travels-the-world-on-taxpayer-money-his-wife-jashodaben-is-struggling-just-to-get-a-passport-in-gujarat-247102.html" title="Even As PM Modi Travels The World On Taxpayer Money, His Wife Is Struggling Just To Get A Passport">
                            Even As PM Modi Travels The World On Taxpayer Money, His Wife Is Struggling Just To Get A Passport                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_16">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/world/15-cities-surrounded-by-smog-clouds-that-prove-pollution-is-a-real-problem-the-world-over-247028.html" title="15 Cities Surrounded By Smog Clouds That Prove Pollution Is A Real Problem The World Over!" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/delhifog1_1446955760_1446955772_236x111.jpg" border="0" alt="15 Cities Surrounded By Smog Clouds That Prove Pollution Is A Real Problem The World Over!"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/world/15-cities-surrounded-by-smog-clouds-that-prove-pollution-is-a-real-problem-the-world-over-247028.html" title="15 Cities Surrounded By Smog Clouds That Prove Pollution Is A Real Problem The World Over!">
                            15 Cities Surrounded By Smog Clouds That Prove Pollution Is A Real Problem The World Over!                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_17">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/save-indian-family-foundation-an-ngo-which-helps-male-victims-of-dowry-harassment-laws-every-week-it-adds-30-new-members-247100.html" title="This NGO Which Helps Male Victims Of Dowry Harassment Laws Adds 30 New Members Every Week" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/sad-man-502_1446975761_236x111.jpg" border="0" alt="This NGO Which Helps Male Victims Of Dowry Harassment Laws Adds 30 New Members Every Week"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/save-indian-family-foundation-an-ngo-which-helps-male-victims-of-dowry-harassment-laws-every-week-it-adds-30-new-members-247100.html" title="This NGO Which Helps Male Victims Of Dowry Harassment Laws Adds 30 New Members Every Week">
                            This NGO Which Helps Male Victims Of Dowry Harassment Laws Adds 30 New Members Every Week                        </a>
                    </figcaption> 
                </div>
                                <div class="news-panel-list cf" id="column1_18">
                    <figure>
                        <div class="hash-tag">
                            1 day ago                         </div>
                                              
                        <a href="http://www.indiatimes.com/news/india/new-delhi-s-lotus-temple-in-contention-for-unesco-s-world-heritage-site-status-247098.html" title="New Delhi's Lotus Temple In Contention For Unesco's 'World Heritage Site' Status" class=" tint">
                            
                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-236x111.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/lotus-temple-502-reuters_1446969072_236x111.jpg" border="0" alt="New Delhi's Lotus Temple In Contention For Unesco's 'World Heritage Site' Status"/>
                        </a>
                        
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/news/india/new-delhi-s-lotus-temple-in-contention-for-unesco-s-world-heritage-site-status-247098.html" title="New Delhi's Lotus Temple In Contention For Unesco's 'World Heritage Site' Status">
                            New Delhi's Lotus Temple In Contention For Unesco's 'World Heritage Site' Status                        </a>
                    </figcaption> 
                </div>
                 
        </div>
    </div><!--news-panel end-->

    <div class="life-panel cf container2" id="b4c2"><!--life-panel start-->
        <h2>lifestyle</h2>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-short-film-will-make-you-think-twice-about-trusting-too-much-and-taking-people-for-granted-246877.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-short-film-will-make-you-think-twice-about-trusting-too-much-and-taking-people-for-granted-246877.html" class="tint" title="This Short Film Will Make You Think Twice About Trusting Too Much And Taking People For Granted">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/thegoodadvise_card_1446446982_502x234.jpg" border="0" alt="This Short Film Will Make You Think Twice About Trusting Too Much And Taking People For Granted" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-short-film-will-make-you-think-twice-about-trusting-too-much-and-taking-people-for-granted-246877.html" title="This Short Film Will Make You Think Twice About Trusting Too Much And Taking People For Granted">
                        This Short Film Will Make You Think Twice About Trusting Too Much And Taking People For Granted                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/16-signs-you-re-super-excited-about-winters-247009.html" class="tint" title="16 Signs Youâre Super Excited About Winters">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/chai-card_1446708399_502x234.jpg" border="0" alt="16 Signs Youâre Super Excited About Winters" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/16-signs-you-re-super-excited-about-winters-247009.html" title="16 Signs Youâre Super Excited About Winters">
                        16 Signs Youâre Super Excited About Winters                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood-s-cleanest-filmmaker-sooraj-barjatya-s-prdp-hits-rough-weather-gets-3-cuts-from-the-censor-board-247130.html" class="tint" title="Bollywood's Cleanest Filmmaker Sooraj Barjatya's PRDP Hits Rough Weather, Gets 3 Cuts From The Censor Board">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/13_1447065716_1447065726_502x234.jpg" border="0" alt="Bollywood's Cleanest Filmmaker Sooraj Barjatya's PRDP Hits Rough Weather, Gets 3 Cuts From The Censor Board" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood-s-cleanest-filmmaker-sooraj-barjatya-s-prdp-hits-rough-weather-gets-3-cuts-from-the-censor-board-247130.html" title="Bollywood's Cleanest Filmmaker Sooraj Barjatya's PRDP Hits Rough Weather, Gets 3 Cuts From The Censor Board">
                        Bollywood's Cleanest Filmmaker Sooraj Barjatya's PRDP Hits Rough Weather, Gets 3 Cuts From The Censor Board                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/11-famous-ladies-on-how-empowering-it-was-to-openly-discuss-their-orgasm-247133.html" class="tint" title="11 Famous Ladies On How Empowering It Was To Openly Discuss Their Orgasm!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/fb_1447070283_502x234.jpg" border="0" alt="11 Famous Ladies On How Empowering It Was To Openly Discuss Their Orgasm!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/11-famous-ladies-on-how-empowering-it-was-to-openly-discuss-their-orgasm-247133.html" title="11 Famous Ladies On How Empowering It Was To Openly Discuss Their Orgasm!">
                        11 Famous Ladies On How Empowering It Was To Openly Discuss Their Orgasm!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/stressed-all-the-time-here-s-how-it-is-making-you-sick-247014.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/stressed-all-the-time-here-s-how-it-is-making-you-sick-247014.html" class="tint" title="Stressed All The Time? Here's How It Is Making You Sick">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card_1446717649_502x234.jpg" border="0" alt="Stressed All The Time? Here's How It Is Making You Sick" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/stressed-all-the-time-here-s-how-it-is-making-you-sick-247014.html" title="Stressed All The Time? Here's How It Is Making You Sick">
                        Stressed All The Time? Here's How It Is Making You Sick                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/dying-doctor-describes-exactly-what-it-feels-like-to-die-from-a-snakebite-247128.html" class="tint" title="Dying Doctor Describes Exactly What It Feels Like To Die From A Snakebite">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1447063893_502x234.jpg" border="0" alt="Dying Doctor Describes Exactly What It Feels Like To Die From A Snakebite" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/dying-doctor-describes-exactly-what-it-feels-like-to-die-from-a-snakebite-247128.html" title="Dying Doctor Describes Exactly What It Feels Like To Die From A Snakebite">
                        Dying Doctor Describes Exactly What It Feels Like To Die From A Snakebite                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/horror-of-horrors-13-signs-you-re-turning-into-one-of-your-parents-247064.html" class="tint" title="Horror Of Horrors! 13 Signs You're Turning Into One Of Your Parents">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cp_1446896146_502x234.jpg" border="0" alt="Horror Of Horrors! 13 Signs You're Turning Into One Of Your Parents" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/horror-of-horrors-13-signs-you-re-turning-into-one-of-your-parents-247064.html" title="Horror Of Horrors! 13 Signs You're Turning Into One Of Your Parents">
                        Horror Of Horrors! 13 Signs You're Turning Into One Of Your Parents                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/4-friends-bravely-cycled-through-the-heart-of-himalayas-and-covered-1082-kms-like-pros-247123.html" class="tint" title="4 Friends Bravely Cycled Through The Heart Of Himalayas And Covered 1082 Kms Like Pros!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/himalaya_card_1447059128_502x234.jpg" border="0" alt="4 Friends Bravely Cycled Through The Heart Of Himalayas And Covered 1082 Kms Like Pros!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/4-friends-bravely-cycled-through-the-heart-of-himalayas-and-covered-1082-kms-like-pros-247123.html" title="4 Friends Bravely Cycled Through The Heart Of Himalayas And Covered 1082 Kms Like Pros!">
                        4 Friends Bravely Cycled Through The Heart Of Himalayas And Covered 1082 Kms Like Pros!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/lifestyle/self/did-you-had-a-breakup-recently-this-guy-s-advice-on-how-to-cope-up-is-all-you-need-247046.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/did-you-had-a-breakup-recently-this-guy-s-advice-on-how-to-cope-up-is-all-you-need-247046.html" class="tint" title="Did You Have A Breakup Recently? This Guy's Advice On How To Cope Up Is All You Need!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/smanak_1446796187_1446796201_502x234.jpg" border="0" alt="Did You Have A Breakup Recently? This Guy's Advice On How To Cope Up Is All You Need!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/did-you-had-a-breakup-recently-this-guy-s-advice-on-how-to-cope-up-is-all-you-need-247046.html" title="Did You Have A Breakup Recently? This Guy's Advice On How To Cope Up Is All You Need!">
                        Did You Have A Breakup Recently? This Guy's Advice On How To Cope Up Is All You Need!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/videos/this-man-gave-up-the-internet-for-a-year-and-what-happened-to-him-is-inspiring-246981.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/this-man-gave-up-the-internet-for-a-year-and-what-happened-to-him-is-inspiring-246981.html" class="tint" title="This Man Gave Up The Internet For A Year And What Happened To Him Is Inspiring">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/time-for-a-digital-detox_1446634238_1446634241_502x234.jpg" border="0" alt="This Man Gave Up The Internet For A Year And What Happened To Him Is Inspiring" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/this-man-gave-up-the-internet-for-a-year-and-what-happened-to-him-is-inspiring-246981.html" title="This Man Gave Up The Internet For A Year And What Happened To Him Is Inspiring">
                        This Man Gave Up The Internet For A Year And What Happened To Him Is Inspiring                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/rice-fine-flour-rotis-and-upma-are-fuelling-the-diabetes-epidemic-across-india-247036.html" class="tint" title="Rice, Fine-Flour Rotis And Upma Are Fuelling The Diabetes Epidemic Across India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-1_1446792566_502x234.jpg" border="0" alt="Rice, Fine-Flour Rotis And Upma Are Fuelling The Diabetes Epidemic Across India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/rice-fine-flour-rotis-and-upma-are-fuelling-the-diabetes-epidemic-across-india-247036.html" title="Rice, Fine-Flour Rotis And Upma Are Fuelling The Diabetes Epidemic Across India">
                        Rice, Fine-Flour Rotis And Upma Are Fuelling The Diabetes Epidemic Across India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/kapil-sharma-s-comic-timing-takes-him-for-a-toss-gets-accused-of-misbehaving-with-a-marathi-actress-at-a-party-247117.html" class="tint" title="Kapil Sharma's Comic Timing Takes Him For A Toss, Gets Accused Of Misbehaving With A Marathi Actress At A Party!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kapil_1447053850_1447053859_502x234.jpg" border="0" alt="Kapil Sharma's Comic Timing Takes Him For A Toss, Gets Accused Of Misbehaving With A Marathi Actress At A Party!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/kapil-sharma-s-comic-timing-takes-him-for-a-toss-gets-accused-of-misbehaving-with-a-marathi-actress-at-a-party-247117.html" title="Kapil Sharma's Comic Timing Takes Him For A Toss, Gets Accused Of Misbehaving With A Marathi Actress At A Party!">
                        Kapil Sharma's Comic Timing Takes Him For A Toss, Gets Accused Of Misbehaving With A Marathi Actress At A Party!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/marriage-on-the-cards-for-priyanka-chopra-she-is-allegedly-dating-an-american-these-days-247118.html" class="tint" title="Marriage On The Cards For Priyanka Chopra? She Is Allegedly Dating An American These Days">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447054470_1447054474_502x234.jpg" border="0" alt="Marriage On The Cards For Priyanka Chopra? She Is Allegedly Dating An American These Days" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/marriage-on-the-cards-for-priyanka-chopra-she-is-allegedly-dating-an-american-these-days-247118.html" title="Marriage On The Cards For Priyanka Chopra? She Is Allegedly Dating An American These Days">
                        Marriage On The Cards For Priyanka Chopra? She Is Allegedly Dating An American These Days                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/deepika-padukone-thinks-she-has-a-raw-and-sensual-chemistry-with-ranveer-singh-well-so-do-we-247114.html" class="tint" title="Deepika Padukone Thinks She Has A Raw And Sensual Chemistry With Ranveer Singh. Well, So Do We!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/ramleela_1447051190_1447051199_502x234.jpg" border="0" alt="Deepika Padukone Thinks She Has A Raw And Sensual Chemistry With Ranveer Singh. Well, So Do We!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/deepika-padukone-thinks-she-has-a-raw-and-sensual-chemistry-with-ranveer-singh-well-so-do-we-247114.html" title="Deepika Padukone Thinks She Has A Raw And Sensual Chemistry With Ranveer Singh. Well, So Do We!">
                        Deepika Padukone Thinks She Has A Raw And Sensual Chemistry With Ranveer Singh. Well, So Do We!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/imtiaz-ali-s-tamasha-gets-u-a-certificate-with-saali-other-dialogues-brutally-chopped-off-247111.html" class="tint" title="Imtiaz Ali's Tamasha Gets U/A Certificate With 'Saali' & Other Dialogues Brutally Chopped Off!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447051369_502x234.jpg" border="0" alt="Imtiaz Ali's Tamasha Gets U/A Certificate With 'Saali' & Other Dialogues Brutally Chopped Off!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/imtiaz-ali-s-tamasha-gets-u-a-certificate-with-saali-other-dialogues-brutally-chopped-off-247111.html" title="Imtiaz Ali's Tamasha Gets U/A Certificate With 'Saali' & Other Dialogues Brutally Chopped Off!">
                        Imtiaz Ali's Tamasha Gets U/A Certificate With 'Saali' & Other Dialogues Brutally Chopped Off!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-message-by-indian-soldiers-is-for-all-those-who-think-that-india-is-intolerant-247113.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-message-by-indian-soldiers-is-for-all-those-who-think-that-india-is-intolerant-247113.html" class="tint" title="This Message By Indian Soldiers Is For All Those Who Think That India Is Intolerant!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/indiansoldiers_card_1447051144_502x234.jpg" border="0" alt="This Message By Indian Soldiers Is For All Those Who Think That India Is Intolerant!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-message-by-indian-soldiers-is-for-all-those-who-think-that-india-is-intolerant-247113.html" title="This Message By Indian Soldiers Is For All Those Who Think That India Is Intolerant!">
                        This Message By Indian Soldiers Is For All Those Who Think That India Is Intolerant!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/technology/indian-astronomers-discover-the-dying-remnants-of-a-previously-unseen-giant-radio-galaxy-247115.html" class="tint" title="Indian Astronomers Discover The Dying Remnants Of A Previously Unseen Giant Radio Galaxy">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/galaxy---card_1447051298_1447051310_502x234.jpg" border="0" alt="Indian Astronomers Discover The Dying Remnants Of A Previously Unseen Giant Radio Galaxy" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/technology/indian-astronomers-discover-the-dying-remnants-of-a-previously-unseen-giant-radio-galaxy-247115.html" title="Indian Astronomers Discover The Dying Remnants Of A Previously Unseen Giant Radio Galaxy">
                        Indian Astronomers Discover The Dying Remnants Of A Previously Unseen Giant Radio Galaxy                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/these-11-bollywood-celebs-are-giving-us-major-travelgoals-time-to-pack-up-and-leave-247055.html" class="tint" title="These 11 Bollywood Celebs Are Giving Us Major #TravelGoals! Time To Pack Up And Leave!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446807853_1446807860_502x234.jpg" border="0" alt="These 11 Bollywood Celebs Are Giving Us Major #TravelGoals! Time To Pack Up And Leave!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/these-11-bollywood-celebs-are-giving-us-major-travelgoals-time-to-pack-up-and-leave-247055.html" title="These 11 Bollywood Celebs Are Giving Us Major #TravelGoals! Time To Pack Up And Leave!">
                        These 11 Bollywood Celebs Are Giving Us Major #TravelGoals! Time To Pack Up And Leave!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/9-ways-smart-and-intelligent-people-have-trouble-finding-love-246992.html" class="tint" title="9 Ways Smart And Intelligent People Have Trouble Finding Love">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/deepika-card1_1447048987_1447049000_502x234.jpg" border="0" alt="9 Ways Smart And Intelligent People Have Trouble Finding Love" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/9-ways-smart-and-intelligent-people-have-trouble-finding-love-246992.html" title="9 Ways Smart And Intelligent People Have Trouble Finding Love">
                        9 Ways Smart And Intelligent People Have Trouble Finding Love                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/in-an-epic-role-reversal-srk-does-a-prem-from-prdp-and-salman-a-raj-from-ddlj-bromance-much-247108.html" class="tint" title="In An Epic Role Reversal, SRK Does A Prem From PRDP And Salman A Raj From DDLJ! Bromance, Much?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447045172_1447045179_502x234.jpg" border="0" alt="In An Epic Role Reversal, SRK Does A Prem From PRDP And Salman A Raj From DDLJ! Bromance, Much?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/in-an-epic-role-reversal-srk-does-a-prem-from-prdp-and-salman-a-raj-from-ddlj-bromance-much-247108.html" title="In An Epic Role Reversal, SRK Does A Prem From PRDP And Salman A Raj From DDLJ! Bromance, Much?">
                        In An Epic Role Reversal, SRK Does A Prem From PRDP And Salman A Raj From DDLJ! Bromance, Much?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/health/healthyliving/this-10-minute-full-body-workout-will-blast-that-fat-away-247035.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/this-10-minute-full-body-workout-will-blast-that-fat-away-247035.html" class="tint" title="This 10 Minute Full Body Workout Will Blast That Fat Away!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/card-2_1446790598_502x234.jpg" border="0" alt="This 10 Minute Full Body Workout Will Blast That Fat Away!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/this-10-minute-full-body-workout-will-blast-that-fat-away-247035.html" title="This 10 Minute Full Body Workout Will Blast That Fat Away!">
                        This 10 Minute Full Body Workout Will Blast That Fat Away!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/videos/11-ways-to-blow-off-steam-in-under-60-seconds-247010.html" class="tint" title="11 Ways To Blow Off Steam In Under 60 Seconds">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-gfhg_1446708773_502x234.jpg" border="0" alt="11 Ways To Blow Off Steam In Under 60 Seconds" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/videos/11-ways-to-blow-off-steam-in-under-60-seconds-247010.html" title="11 Ways To Blow Off Steam In Under 60 Seconds">
                        11 Ways To Blow Off Steam In Under 60 Seconds                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-prem-ratan-dhan-payo-dubsmash-starring-munni-aka-harshaali-malhotra-is-by-far-the-best-one-247105.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-prem-ratan-dhan-payo-dubsmash-starring-munni-aka-harshaali-malhotra-is-by-far-the-best-one-247105.html" class="tint" title="This Prem Ratan Dhan Payo Dubsmash Starring Munni AKA Harshaali Malhotra Is By Far The Best One!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/munni_prdp_card_1446980231_502x234.jpg" border="0" alt="This Prem Ratan Dhan Payo Dubsmash Starring Munni AKA Harshaali Malhotra Is By Far The Best One!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-prem-ratan-dhan-payo-dubsmash-starring-munni-aka-harshaali-malhotra-is-by-far-the-best-one-247105.html" title="This Prem Ratan Dhan Payo Dubsmash Starring Munni AKA Harshaali Malhotra Is By Far The Best One!">
                        This Prem Ratan Dhan Payo Dubsmash Starring Munni AKA Harshaali Malhotra Is By Far The Best One!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/indian-americans-raise-200-000-for-healthcare-in-rural-india-247107.html" class="tint" title="Indian-Americans Raise $200,000 For Healthcare In Rural India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card2_1446982406_1446982414_502x234.jpg" border="0" alt="Indian-Americans Raise $200,000 For Healthcare In Rural India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/indian-americans-raise-200-000-for-healthcare-in-rural-india-247107.html" title="Indian-Americans Raise $200,000 For Healthcare In Rural India">
                        Indian-Americans Raise $200,000 For Healthcare In Rural India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/these-8-popular-indian-tv-shows-have-achieved-episodic-milestones-247038.html" class="tint" title="These 8 Popular Indian TV Shows Have Achieved Episodic Milestones!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446792700_1446792706_502x234.jpg" border="0" alt="These 8 Popular Indian TV Shows Have Achieved Episodic Milestones!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/these-8-popular-indian-tv-shows-have-achieved-episodic-milestones-247038.html" title="These 8 Popular Indian TV Shows Have Achieved Episodic Milestones!">
                        These 8 Popular Indian TV Shows Have Achieved Episodic Milestones!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/8-times-when-filmmakers-lavishly-spent-the-movie-s-budget-on-songs-247101.html" class="tint" title="8 Times When Filmmakers Lavishly Spent The Movie's Budget On Songs!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/exp-songs-card_1446976623_1446976676_502x234.jpg" border="0" alt="8 Times When Filmmakers Lavishly Spent The Movie's Budget On Songs!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/8-times-when-filmmakers-lavishly-spent-the-movie-s-budget-on-songs-247101.html" title="8 Times When Filmmakers Lavishly Spent The Movie's Budget On Songs!">
                        8 Times When Filmmakers Lavishly Spent The Movie's Budget On Songs!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/tips-tricks/eat-your-breakfast-lunch-and-dinner-at-these-times-for-a-long-and-healthy-life-247012.html" class="tint" title="Eat Your Breakfast, Lunch And Dinner At These Times For A Long And Healthy Life">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1446711022_502x234.jpg" border="0" alt="Eat Your Breakfast, Lunch And Dinner At These Times For A Long And Healthy Life" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/tips-tricks/eat-your-breakfast-lunch-and-dinner-at-these-times-for-a-long-and-healthy-life-247012.html" title="Eat Your Breakfast, Lunch And Dinner At These Times For A Long And Healthy Life">
                        Eat Your Breakfast, Lunch And Dinner At These Times For A Long And Healthy Life                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-is-how-maggi-not-only-cleared-the-safety-tests-but-also-made-our-moms-tension-free-247099.html" class="tint" title="This Is How Maggi Not Only Cleared The Safety Tests, But Also Made Our Moms Tension Free!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/maggi_card1_1446970639_502x234.jpg" border="0" alt="This Is How Maggi Not Only Cleared The Safety Tests, But Also Made Our Moms Tension Free!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-is-how-maggi-not-only-cleared-the-safety-tests-but-also-made-our-moms-tension-free-247099.html" title="This Is How Maggi Not Only Cleared The Safety Tests, But Also Made Our Moms Tension Free!">
                        This Is How Maggi Not Only Cleared The Safety Tests, But Also Made Our Moms Tension Free!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/14-posters-that-prove-it-s-time-for-us-to-start-focussing-on-the-real-issues-facing-india-247089.html" class="tint" title="14 Posters That Prove It's Time For Us To Start Focussing On The Real Issues Facing India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446959927_502x234.jpg" border="0" alt="14 Posters That Prove It's Time For Us To Start Focussing On The Real Issues Facing India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/14-posters-that-prove-it-s-time-for-us-to-start-focussing-on-the-real-issues-facing-india-247089.html" title="14 Posters That Prove It's Time For Us To Start Focussing On The Real Issues Facing India">
                        14 Posters That Prove It's Time For Us To Start Focussing On The Real Issues Facing India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/here-s-a-map-of-the-most-popular-sports-in-india-and-it-s-spot-on-247097.html" class="tint" title="Here's A Map Of The Most Popular Sports In India And It's Spot On">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/map2_1446967985_1446967991_502x234.jpg" border="0" alt="Here's A Map Of The Most Popular Sports In India And It's Spot On" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/here-s-a-map-of-the-most-popular-sports-in-india-and-it-s-spot-on-247097.html" title="Here's A Map Of The Most Popular Sports In India And It's Spot On">
                        Here's A Map Of The Most Popular Sports In India And It's Spot On                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bigg-boss-9-is-turning-out-to-be-a-snoozefest-salman-khan-seems-like-the-only-saving-grace-247096.html" class="tint" title="Bigg Boss 9 Is Turning Out To Be A Snoozefest & Salman Khan Seems Like The Only Saving Grace!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/w_1446966822_1446966829_502x234.jpg" border="0" alt="Bigg Boss 9 Is Turning Out To Be A Snoozefest & Salman Khan Seems Like The Only Saving Grace!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bigg-boss-9-is-turning-out-to-be-a-snoozefest-salman-khan-seems-like-the-only-saving-grace-247096.html" title="Bigg Boss 9 Is Turning Out To Be A Snoozefest & Salman Khan Seems Like The Only Saving Grace!">
                        Bigg Boss 9 Is Turning Out To Be A Snoozefest & Salman Khan Seems Like The Only Saving Grace!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/babies-are-using-smartphones-for-at-least-20-minutes-a-day-in-the-us-247008.html" class="tint" title="Babies Are Using Smartphones For At Least 20 Minutes A Day In The US!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1446708522_502x234.jpg" border="0" alt="Babies Are Using Smartphones For At Least 20 Minutes A Day In The US!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/babies-are-using-smartphones-for-at-least-20-minutes-a-day-in-the-us-247008.html" title="Babies Are Using Smartphones For At Least 20 Minutes A Day In The US!">
                        Babies Are Using Smartphones For At Least 20 Minutes A Day In The US!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/on-screen-god-morgan-freeman-visits-varanasi-to-shoot-a-documentary-247092.html" class="tint" title="On-Screen God Morgan Freeman Visits Varanasi To Shoot A Documentary!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/q_1446963480_1446963498_502x234.jpg" border="0" alt="On-Screen God Morgan Freeman Visits Varanasi To Shoot A Documentary!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/on-screen-god-morgan-freeman-visits-varanasi-to-shoot-a-documentary-247092.html" title="On-Screen God Morgan Freeman Visits Varanasi To Shoot A Documentary!">
                        On-Screen God Morgan Freeman Visits Varanasi To Shoot A Documentary!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-video-reveals-unheard-diwali-stories-which-will-leave-you-awestruck-247090.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-video-reveals-unheard-diwali-stories-which-will-leave-you-awestruck-247090.html" class="tint" title="This Video Reveals Unheard Diwali Stories Which Will Leave You Awestruck!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/diwali_stories_card1_1446961106_502x234.jpg" border="0" alt="This Video Reveals Unheard Diwali Stories Which Will Leave You Awestruck!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-video-reveals-unheard-diwali-stories-which-will-leave-you-awestruck-247090.html" title="This Video Reveals Unheard Diwali Stories Which Will Leave You Awestruck!">
                        This Video Reveals Unheard Diwali Stories Which Will Leave You Awestruck!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/food/21-great-ways-in-which-beer-is-not-just-your-best-buddy-who-gives-you-a-prominent-belly-246956.html" class="tint" title="21 Great Ways In Which Beer Is Not Just Your Best Buddy Who Gives You A Prominent Belly">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446636151_502x234.jpg" border="0" alt="21 Great Ways In Which Beer Is Not Just Your Best Buddy Who Gives You A Prominent Belly" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/food/21-great-ways-in-which-beer-is-not-just-your-best-buddy-who-gives-you-a-prominent-belly-246956.html" title="21 Great Ways In Which Beer Is Not Just Your Best Buddy Who Gives You A Prominent Belly">
                        21 Great Ways In Which Beer Is Not Just Your Best Buddy Who Gives You A Prominent Belly                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/even-gandhiji-didn-t-give-back-his-law-degree-says-kamal-haasan-on-intolerance-row-247088.html" class="tint" title="Even Gandhiji Didn't Give Back His Law Degree, Says Kamal Haasan On #AwardWapsi">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/kk_1446962116_1446962122_502x234.jpg" border="0" alt="Even Gandhiji Didn't Give Back His Law Degree, Says Kamal Haasan On #AwardWapsi" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/even-gandhiji-didn-t-give-back-his-law-degree-says-kamal-haasan-on-intolerance-row-247088.html" title="Even Gandhiji Didn't Give Back His Law Degree, Says Kamal Haasan On #AwardWapsi">
                        Even Gandhiji Didn't Give Back His Law Degree, Says Kamal Haasan On #AwardWapsi                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/healthyliving/5-do-anywhere-killer-cross-fit-workouts-that-will-help-you-stay-in-shape-246927.html" class="tint" title="5 Do-Anywhere Killer Cross Fit Workouts That Will Help You Stay In Shape!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card-1_1446546722_502x234.jpg" border="0" alt="5 Do-Anywhere Killer Cross Fit Workouts That Will Help You Stay In Shape!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/healthyliving/5-do-anywhere-killer-cross-fit-workouts-that-will-help-you-stay-in-shape-246927.html" title="5 Do-Anywhere Killer Cross Fit Workouts That Will Help You Stay In Shape!">
                        5 Do-Anywhere Killer Cross Fit Workouts That Will Help You Stay In Shape!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-spoof-of-border-will-tell-you-why-taking-leaves-during-festive-season-is-a-bad-idea-247086.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-spoof-of-border-will-tell-you-why-taking-leaves-during-festive-season-is-a-bad-idea-247086.html" class="tint" title="This Spoof of 'Border' Will Tell You Why Taking Leaves During Festive Season Is A Bad Idea!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/borderspoof_card_1446956757_502x234.jpg" border="0" alt="This Spoof of 'Border' Will Tell You Why Taking Leaves During Festive Season Is A Bad Idea!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-spoof-of-border-will-tell-you-why-taking-leaves-during-festive-season-is-a-bad-idea-247086.html" title="This Spoof of 'Border' Will Tell You Why Taking Leaves During Festive Season Is A Bad Idea!">
                        This Spoof of 'Border' Will Tell You Why Taking Leaves During Festive Season Is A Bad Idea!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/lifestyle/self/what-does-your-mole-say-about-your-personality-247030.html" class="tint" title="What Does Your Mole Say About Your Personality?">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/untitled-25_1446800217_1446800235_502x234.jpg" border="0" alt="What Does Your Mole Say About Your Personality?" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/lifestyle/self/what-does-your-mole-say-about-your-personality-247030.html" title="What Does Your Mole Say About Your Personality?">
                        What Does Your Mole Say About Your Personality?                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/neil-nitin-mukesh-will-not-be-a-part-of-game-of-thrones-confirms-hbo-247084.html" class="tint" title="Neil Nitin Mukesh Will Not Be A Part Of Game of Thrones, Confirms HBO">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446900601_1446900605_502x234.jpg" border="0" alt="Neil Nitin Mukesh Will Not Be A Part Of Game of Thrones, Confirms HBO" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/neil-nitin-mukesh-will-not-be-a-part-of-game-of-thrones-confirms-hbo-247084.html" title="Neil Nitin Mukesh Will Not Be A Part Of Game of Thrones, Confirms HBO">
                        Neil Nitin Mukesh Will Not Be A Part Of Game of Thrones, Confirms HBO                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khan-bats-for-srk-says-shah-rukh-is-targetted-because-he-is-a-phenomenon-247079.html" class="tint" title="Salman Khan Bats For SRK. Says Shah Rukh Is Targetted Because He Is A Phenomenon">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446896042_1446896046_502x234.jpg" border="0" alt="Salman Khan Bats For SRK. Says Shah Rukh Is Targetted Because He Is A Phenomenon" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/salman-khan-bats-for-srk-says-shah-rukh-is-targetted-because-he-is-a-phenomenon-247079.html" title="Salman Khan Bats For SRK. Says Shah Rukh Is Targetted Because He Is A Phenomenon">
                        Salman Khan Bats For SRK. Says Shah Rukh Is Targetted Because He Is A Phenomenon                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/culture/who-we-are/21-everyday-things-we-had-no-clue-there-were-names-for-247017.html" class="tint" title="21 Everyday Things We Had No Clue There Were Names For">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/homer502_1446872530_502x234.jpg" border="0" alt="21 Everyday Things We Had No Clue There Were Names For" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/culture/who-we-are/21-everyday-things-we-had-no-clue-there-were-names-for-247017.html" title="21 Everyday Things We Had No Clue There Were Names For">
                        21 Everyday Things We Had No Clue There Were Names For                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/10-reasons-why-rajinikanth-s-robot-2-might-just-be-the-biggest-film-ever-made-in-india-247074.html" class="tint" title="10 Reasons Why Rajinikanth's 'Robot 2' Might Just Be The Biggest Film Ever Made In India">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446890734_1446890740_502x234.jpg" border="0" alt="10 Reasons Why Rajinikanth's 'Robot 2' Might Just Be The Biggest Film Ever Made In India" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/10-reasons-why-rajinikanth-s-robot-2-might-just-be-the-biggest-film-ever-made-in-india-247074.html" title="10 Reasons Why Rajinikanth's 'Robot 2' Might Just Be The Biggest Film Ever Made In India">
                        10 Reasons Why Rajinikanth's 'Robot 2' Might Just Be The Biggest Film Ever Made In India                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/naseeruddin-shah-will-not-return-awards-he-says-they-mean-nothing-to-him-awardwapasi-247067.html" class="tint" title="Naseeruddin Shah 'Will Not Return Awards'. He Says They Mean Nothing To Him! #AwardWapasi">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446878968_1446878972_502x234.jpg" border="0" alt="Naseeruddin Shah 'Will Not Return Awards'. He Says They Mean Nothing To Him! #AwardWapasi" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/naseeruddin-shah-will-not-return-awards-he-says-they-mean-nothing-to-him-awardwapasi-247067.html" title="Naseeruddin Shah 'Will Not Return Awards'. He Says They Mean Nothing To Him! #AwardWapasi">
                        Naseeruddin Shah 'Will Not Return Awards'. He Says They Mean Nothing To Him! #AwardWapasi                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/who-is-rishabh-sinha-7-things-you-should-know-about-the-wild-card-entry-on-bigg-boss-9_-247060.html" class="tint" title="Who Is Rishabh Sinha? 7 Things You Should Know About The 'Wild' Card Entry On Bigg Boss 9">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1446814809_1446814814_502x234.jpg" border="0" alt="Who Is Rishabh Sinha? 7 Things You Should Know About The 'Wild' Card Entry On Bigg Boss 9" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/who-is-rishabh-sinha-7-things-you-should-know-about-the-wild-card-entry-on-bigg-boss-9_-247060.html" title="Who Is Rishabh Sinha? 7 Things You Should Know About The 'Wild' Card Entry On Bigg Boss 9">
                        Who Is Rishabh Sinha? 7 Things You Should Know About The 'Wild' Card Entry On Bigg Boss 9                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/remember-the-epic-rant-from-pyar-ka-punchnama-this-guy-has-its-perfect-office-version-of-it-247066.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/remember-the-epic-rant-from-pyar-ka-punchnama-this-guy-has-its-perfect-office-version-of-it-247066.html" class="tint" title="Remember The Epic Rant From Pyar Ka Punchnama? This Guy Has Its Perfect Office Version Of It!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/office-version-cp_1446876000_502x234.jpg" border="0" alt="Remember The Epic Rant From Pyar Ka Punchnama? This Guy Has Its Perfect Office Version Of It!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/remember-the-epic-rant-from-pyar-ka-punchnama-this-guy-has-its-perfect-office-version-of-it-247066.html" title="Remember The Epic Rant From Pyar Ka Punchnama? This Guy Has Its Perfect Office Version Of It!">
                        Remember The Epic Rant From Pyar Ka Punchnama? This Guy Has Its Perfect Office Version Of It!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-first-time-on-screen-bollywood-couples-who-look-rocking-together-246443.html" class="tint" title="12 First Time On-Screen Bollywood Couples Who Look Rocking Together!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1445326639_1445326644_502x234.jpg" border="0" alt="12 First Time On-Screen Bollywood Couples Who Look Rocking Together!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/bollywood/12-first-time-on-screen-bollywood-couples-who-look-rocking-together-246443.html" title="12 First Time On-Screen Bollywood Couples Who Look Rocking Together!">
                        12 First Time On-Screen Bollywood Couples Who Look Rocking Together!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                    <a class='video-btn sprite' href='http://www.indiatimes.com/videocafe/this-bhajan-version-of-dj-wale-babu-is-destroying-the-internet-right-now-247063.html'>video</a>                     
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/videocafe/this-bhajan-version-of-dj-wale-babu-is-destroying-the-internet-right-now-247063.html" class="tint" title="This Bhajan Version Of 'DJ Wale Babu' Is Destroying The Internet Right Now!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/videocafe/2015/Nov/badshah502_1446834347_502x234.jpg" border="0" alt="This Bhajan Version Of 'DJ Wale Babu' Is Destroying The Internet Right Now!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/videocafe/this-bhajan-version-of-dj-wale-babu-is-destroying-the-internet-right-now-247063.html" title="This Bhajan Version Of 'DJ Wale Babu' Is Destroying The Internet Right Now!">
                        This Bhajan Version Of 'DJ Wale Babu' Is Destroying The Internet Right Now!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/health/buzz/if-you-have-a-tendency-to-put-on-weight-this-tiny-genetic-variation-could-be-the-culprit-246958.html" class="tint" title="If You Have A Tendency To Put On Weight, This Tiny Genetic Variation Could Be The Culprit!">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cover_1446619570_502x234.jpg" border="0" alt="If You Have A Tendency To Put On Weight, This Tiny Genetic Variation Could Be The Culprit!" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/health/buzz/if-you-have-a-tendency-to-put-on-weight-this-tiny-genetic-variation-could-be-the-culprit-246958.html" title="If You Have A Tendency To Put On Weight, This Tiny Genetic Variation Could Be The Culprit!">
                        If You Have A Tendency To Put On Weight, This Tiny Genetic Variation Could Be The Culprit!                    </a>
                </figcaption>
            </div>
                
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>
                                         
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="http://www.indiatimes.com/entertainment/celebs/keith-sequeira-exits-bigg-boss-9-midway-due-to-family-emergency-247056.html" class="tint" title="Keith Sequeira Exits Bigg Boss 9 Midway Due To Brother's Demise">
                        <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-502x234.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/cardd_1446812130_1446812133_502x234.jpg" border="0" alt="Keith Sequeira Exits Bigg Boss 9 Midway Due To Brother's Demise" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="http://www.indiatimes.com/entertainment/celebs/keith-sequeira-exits-bigg-boss-9-midway-due-to-family-emergency-247056.html" title="Keith Sequeira Exits Bigg Boss 9 Midway Due To Brother's Demise">
                        Keith Sequeira Exits Bigg Boss 9 Midway Due To Brother's Demise                    </a>
                </figcaption>
            </div>
        
        <script id="HpMiddleBlock_tpl" type="text/x-handlebars-template">
            {{#if MoreData}}
            {{#each MoreData}} 
            <div class="life-panel-list cf"><!--life-panel-list start-->
                <figure>


                    <a  class='video-btn sprite' href="{{guid}}" style="display:{{card_id}}"> </a>

    				{{#if label_name.length}}
                    <a href="{{guid}}" class="{{label_name}} sticker">&nbsp;</a>
    						 {{/if}} 
                    <!--a class="label-tag spon">Sponsored</a-->
                    <a href="{{guid}}" title="{{carousal_headline}}" class="tint">
                        <img class="greyBg lazy" data-original="{{thumbnail}}" border="0" alt="{{carousal_headline}}" class="img-responsive"/>
                    </a>
                </figure>
                <figcaption>
                    <a href="{{guid}}" title="{{carousal_headline}}">
                        {{carousal_headline}}
                    </a>
                </figcaption>
            </div>  
            {{/each}} 
            {{/if}}

            </script>

        </div><!--life-panel end-->

        <div class="trending-panel cf "><!--trending-panel start-->
            <h2>trending</h2>
                            <div class="trending-panel-list cf" id="column3_15"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/celebs/these-11-bollywood-celebs-are-giving-us-major-travelgoals-time-to-pack-up-and-leave-247055.html" class="tint" title="These 11 Bollywood Celebs Are Giving Us Major #TravelGoals! Time To Pack Up And Leave!">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446807853_1446807860_218x102.jpg" border="0" alt="These 11 Bollywood Celebs Are Giving Us Major #TravelGoals! Time To Pack Up And Leave!"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/celebs/these-11-bollywood-celebs-are-giving-us-major-travelgoals-time-to-pack-up-and-leave-247055.html" title="These 11 Bollywood Celebs Are Giving Us Major #TravelGoals! Time To Pack Up And Leave!">
                            These 11 Bollywood Celebs Are Giving Us Major #TravelGoals! Time To Pack Up And Leave!                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                <div class='container3'>                <div class="trending-panel-list cf" id="column3_16"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/here-s-a-map-of-the-most-popular-sports-in-india-and-it-s-spot-on-247097.html" class="tint" title="Here's A Map Of The Most Popular Sports In India And It's Spot On">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/map2_1446967985_1446967991_218x102.jpg" border="0" alt="Here's A Map Of The Most Popular Sports In India And It's Spot On"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/here-s-a-map-of-the-most-popular-sports-in-india-and-it-s-spot-on-247097.html" title="Here's A Map Of The Most Popular Sports In India And It's Spot On">
                            Here's A Map Of The Most Popular Sports In India And It's Spot On                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_17"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/lifestyle/self/what-does-your-mole-say-about-your-personality-247030.html" class="tint" title="What Does Your Mole Say About Your Personality?">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/untitled-25_1446800217_1446800235_218x102.jpg" border="0" alt="What Does Your Mole Say About Your Personality?"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/lifestyle/self/what-does-your-mole-say-about-your-personality-247030.html" title="What Does Your Mole Say About Your Personality?">
                            What Does Your Mole Say About Your Personality?                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_18"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/entertainment/bollywood/marriage-on-the-cards-for-priyanka-chopra-she-is-allegedly-dating-an-american-these-days-247118.html" class="tint" title="Marriage On The Cards For Priyanka Chopra? She Is Allegedly Dating An American These Days">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/card_1447054470_1447054474_218x102.jpg" border="0" alt="Marriage On The Cards For Priyanka Chopra? She Is Allegedly Dating An American These Days"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/entertainment/bollywood/marriage-on-the-cards-for-priyanka-chopra-she-is-allegedly-dating-an-american-these-days-247118.html" title="Marriage On The Cards For Priyanka Chopra? She Is Allegedly Dating An American These Days">
                            Marriage On The Cards For Priyanka Chopra? She Is Allegedly Dating An American These Days                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                                <div class="trending-panel-list cf" id="column3_19"><!--trending-panel-list start-->
                                        <figure>
                                
                        					
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-posters-that-prove-it-s-time-for-us-to-start-focussing-on-the-real-issues-facing-india-247089.html" class="tint" title="14 Posters That Prove It's Time For Us To Start Focussing On The Real Issues Facing India">

                            <img  class="greyBg23 lazy" src="http://media.indiatimes.in/resources/images/Dummy-Image-218x102.jpg" data-original="http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446959927_218x102.jpg" border="0" alt="14 Posters That Prove It's Time For Us To Start Focussing On The Real Issues Facing India"/>
                        </a>
                    </figure>
                    <figcaption>
                        <a href="http://www.indiatimes.com/culture/who-we-are/14-posters-that-prove-it-s-time-for-us-to-start-focussing-on-the-real-issues-facing-india-247089.html" title="14 Posters That Prove It's Time For Us To Start Focussing On The Real Issues Facing India">
                            14 Posters That Prove It's Time For Us To Start Focussing On The Real Issues Facing India                        </a>
                    </figcaption>
                </div><!--trending-panel-list end-->
                
        </div>

    </div><!--trending-panel end-->

    </section>
    <section class="container cf" id="loader" style="display: none;border: 0px solid red;">
    <div class="news-panel cf ">&nbsp;</div>
    <div class="life-panel cf" style="text-align: center;"><img src="http://media.indiatimes.in/resources/images/loading.gif" border="0" /></div>
    <div class="trending-panel cf ">&nbsp;</div>
</section>
<section class="big-ads remove-fixed-home" id="adfooter">
<div id="badRos_slot"></div> 
</section>   

<!------------------------ end Block 4------------------------------------------------>
<!--container end-->

<script type="text/javascript">
   
    $('#bigAd1_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD2('bigAd2_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible     
       
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd2_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    showBigAD3('bigAd3_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible

    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible  
      
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});
    $('#bigAd3_slot').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
  if (isInView) {
    // element is now visible in the viewport
    BADros('badRos_slot');
    if (visiblePartY == 'top') {
      // top part of element is visible
    } else if (visiblePartY == 'bottom') {
      // bottom part of element is visible
    } else {
      // whole part of element is visible
    }
  } else {
    // element has gone out of viewport
  }
});

$(document).ready(function() {
	/* spotlight onload tracking homepage */
	//console.log("homepage");
			ga('send', 'event', 'OnLoad Partner Stories', '246564', 'homepage', {'nonInteraction': 1});
			ga('send', 'event', 'OnLoad Partner Stories', '247001', 'homepage', {'nonInteraction': 1});
			ga('send', 'event', 'OnLoad Partner Stories', '247023', 'homepage', {'nonInteraction': 1});
	           
});

$(document).ready(function(){
        var trigger_depth={b2:99, b3:499, b4:1199};
        var is_trigger_active={b2:true, b3:true, b4:true};
        var call_on_scroll=true;
        
            $(window).on("scroll",function(){
            if((!(is_trigger_active.b2)&&!(is_trigger_active.b3)&&!(is_trigger_active.b4))==false)
            {
                callPreload(is_trigger_active,trigger_depth);
                
            }
    });
});

function callPreload(active,depth)
{
    var scroll_top = $(window).scrollTop();
    if((active.b2)&&(scroll_top > depth.b2))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/card_1447059056_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/beef-party-502_1447060466_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447056056_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/sachinsehwagallstars_1447054845_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/sartaj502_1447050559_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/ds_1447084114_1447084121_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/diwali-carol502_1447084583_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446809723_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card_1446717649_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447063893_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447045172_1447045179_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/13_1447065716_1447065726_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-1_1446792566_218x102.jpg");
        active.b2=false;
    }
    if((active.b3)&&(scroll_top > depth.b3))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Oct/card_1445607453_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/man-502-vote-mom_1447051425_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/car_1447051792_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/name2_1446980635_1446980640_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/be2_1446978533_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1446721026_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/robberies_card_1447073714_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/tzp_card_1447072447_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card-2_1446790598_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/cp_1446896146_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/fb_1447070283_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/ramleela_1447051190_1447051199_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/kapil_1447053850_1447053859_218x102.jpg");
        active.b3=false;
    }
    
    if((active.b4)&&(scroll_top > depth.b4))
    {
        $.preload("http://media.indiatimes.in/media/content/2015/Nov/kishor-600_1446979033_1446979038_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/jashoda-5032_1446982956_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/delhifog1_1446955760_1446955772_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/sad-man-502_1446975761_236x111.jpg","http://media.indiatimes.in/media/content/2015/Nov/lotus-temple-502-reuters_1446969072_236x111.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/thegoodadvise_card_1446446982_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/chai-card_1446708399_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/13_1447065716_1447065726_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/fb_1447070283_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card_1446717649_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1447063893_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cp_1446896146_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/himalaya_card_1447059128_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/smanak_1446796187_1446796201_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/time-for-a-digital-detox_1446634238_1446634241_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-1_1446792566_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/kapil_1447053850_1447053859_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447054470_1447054474_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/ramleela_1447051190_1447051199_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447051369_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/indiansoldiers_card_1447051144_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/galaxy---card_1447051298_1447051310_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446807853_1446807860_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/deepika-card1_1447048987_1447049000_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447045172_1447045179_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/card-2_1446790598_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-gfhg_1446708773_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/munni_prdp_card_1446980231_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card2_1446982406_1446982414_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446792700_1446792706_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/exp-songs-card_1446976623_1446976676_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1446711022_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/maggi_card1_1446970639_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446959927_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/map2_1446967985_1446967991_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/w_1446966822_1446966829_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1446708522_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/q_1446963480_1446963498_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/diwali_stories_card1_1446961106_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446636151_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/kk_1446962116_1446962122_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card-1_1446546722_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/borderspoof_card_1446956757_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/untitled-25_1446800217_1446800235_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446900601_1446900605_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446896042_1446896046_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/homer502_1446872530_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446890734_1446890740_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446878968_1446878972_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1446814809_1446814814_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/office-version-cp_1446876000_502x234.jpg","http://media.indiatimes.in/media/content/2015/Oct/picmonkey-collage_1445326639_1445326644_502x234.jpg","http://media.indiatimes.in/media/videocafe/2015/Nov/badshah502_1446834347_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cover_1446619570_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/cardd_1446812130_1446812133_502x234.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446807853_1446807860_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/map2_1446967985_1446967991_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/untitled-25_1446800217_1446800235_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/card_1447054470_1447054474_218x102.jpg","http://media.indiatimes.in/media/content/2015/Nov/picmonkey-collage_1446959927_218x102.jpg");
        active.b4=false;
    }
}
</script>    <div class="clr"></div>
      
	
                 <script> 
				                    showFooterCode();
					            </script>
	           <style>
.bottom-strip{background:#ebebeb;display:block;padding:15px 0;overflow:hidden;}.bottom-strip-box{margin:0 auto;display:table;max-width:410px;width:100%;}.square-logo{width:66px;height:64px;float:left;margin-right:10px;}.bottom-strip-content{float:left;margin-top:14px;}.bottom-strip-content span{display:block;color:#737272;font:12px/1 "RobotoRegular", Arial, Helvetica, sans-serif;margin-bottom:2px;}.bottom-strip-content a{color:#1b85dd;font:24px/1 "RobotoRegular", Arial, Helvetica, sans-serif;text-decoration:none;}
</style>
<div class="last-container">
    <!--<br>--> 
    <!--social-panel start-->
    <div class="social-panel red-bg">
        <div class="soc-inner"><!--soc-inner start-->
            <p class="soc-title">Be a Part of The New & Next</p>
            <div class="soc-count"><!--soc-count start-->
                <a href="https://www.facebook.com/indiatimes" target="_blank" title="facebook" class="sprite soc-fb"></a>
                <p>
                    3,851,486<span class="black"> FRIENDS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://twitter.com/indiatimes" target="_blank" title="twitter" class="sprite soc-twt"></a>
                <p>10343  <span class="black">FOLLOWERS</span></p>
            </div>
            <!--soc-count end-->
            <div class="soc-count"><!--soc-count start-->
                <a href="https://plus.google.com/+indiatimes" target="_blank" title="gplus" class="sprite soc-gplus"></a>
                <p>104,163 <span class="black">MEMBERS</span></p>
            </div>
            <div class="soc-count"><!--soc-count start-->
                <a href="javascript:void(0);" title="E-mail" class="sprite soc-mail" id="subscribers_id"></a>
                <p>50,495 <span class="black">SUBSCRIBERS</span></p>
            </div>
            <!--soc-count end-->
        </div>
        <!--soc-inner end-->
    </div>
    <!--social-panel end-->
    <div class="clr"></div>

    <footer>
        <!-- social container start-->
        <div class="socical">
            <div class="containers">
                <div class="fl left_cont"> Get your weekly dose of virality sent to your inbox! </div>
                <div class="fr right_cont">

                    <input type="text" value="Please Enter Email Address" class="Email" name="UserEmail" id="UserEmail" defaultValue="Please Enter Email Address"/>
                    <input type="submit" id="btn-submit" value="SIGN UP" name="btn-submit" class="Subscribe">
                    <span id="subsEmal" style='color: red; padding-left: 66px; text-align: left;'></span>
                </div>

            </div>
        </div><!-- social containers end-->
        <div class="footer"><!-- footer grey start-->
            <div class="sub_container">
                <!-- container start-->
                <div class="cont_new">
                    <h2>Our Channels</h2>
                                                <a href='http://www.indiatimes.com/news/' class="blue size">News</a> 
                                                    <a href='http://www.indiatimes.com/lifestyle/' class="purple size">Lifestyle</a> 
                                                    <a href='http://www.indiatimes.com/entertainment/' class="red size">Entertainment</a> 
                                                    <a href='http://www.indiatimes.com/videocafe/' class="orange size">Videos</a> 
                                                    <a href='http://www.indiatimes.com/health/' class="green size">Health</a> 
                        					<a href='http://www.indiatimes.com/trending' class="size" style="color:white">Trending</a> 
                </div><!-- container closed-->

                <div class="cont"> <!-- container start-->
                    <h2>Sections</h2>
                                            <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/news/india/'>india</a>
                                             
                                    <a href='http://www.indiatimes.com/news/world/'>world</a>
                                             
                                    <a href='http://www.indiatimes.com/news/sports/'>sports</a>
                                             
                                    <a href='http://www.indiatimes.com/news/weird/'>weird</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/lifestyle/self/'>self</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/style/'>style</a>
                                             
                                    <a href='http://www.indiatimes.com/lifestyle/technology/'>Tech</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/who-we-are/'>who we are</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/travel/'>travel</a>
                                             
                                    <a href='http://www.indiatimes.com/culture/food/'>food</a>
                                
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/entertainment/bollywood/'>bollywood</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/celebs/'>celebscoop</a>
                                             
                                    <a href='http://www.indiatimes.com/entertainment/hollywood/'>hollywood</a>
                                
                             </div>                        <div class="sub_link">
                             </div>                        <div class="sub_link">
                                                                         
                                    <a href='http://www.indiatimes.com/health/healthyliving/'>Healthy Living</a>
                                             
                                    <a href='http://www.indiatimes.com/health/recipes/'>Recipes</a>
                                             
                                    <a href='http://www.indiatimes.com/health/videos/'>Inspire</a>
                                             
                                    <a href='http://www.indiatimes.com/health/tips-tricks/'>Tips & Tricks</a>
                                             
                                    <a href='http://www.indiatimes.com/health/buzz/'>Buzz</a>
                                
                             </div>                    <div class="sub_link"><a href="http://www.indiatimes.com/photogallery">Photogallery</a></div>
                </div><!-- container closed-->
                <div class="cont_new"> <!-- container start-->
                    <h2>Indiatimes Lifestyle Network</h2>

                    <div class="sub_link">                             <a href="http://www.mensxp.com"   target="_blank" >MENS XP</a>
                                                    <a href="http://www.idiva.com"   target="_blank" >iDiva</a>
                                                    <a href="http://luxpresso.com"   target="_blank" >Luxury</a>
                                                    <a href="http://in.askmen.com/"   target="_blank" >Ask Men</a>
                                                    <a href="http://www.pursuitist.in/"   target="_blank" >Pursuitist</a>
                        </div>
                </div><!-- container closed-->
                <div class="cont"> <!-- container start-->
                    <h2>Other Sites</h2>
                    <div class="sub_link"> 
                        
                            <a href="http://www.cricbuzz.com"  target="_blank">CricBuzz</a> 
                        
                            <a href="http://www.happytrips.com/"  target="_blank">Happy Trips</a> 
                        
                            <a href="http://www.businessinsider.in/"  target="_blank">Business Insider</a> 
                        
                            <a href="http://www.zoomtv.com"  target="_blank">ZoomTv</a> 
                        
                            <a href="http://www.gizmodo.in/"  target="_blank">Gizmodo</a> 
                        
                            <a href="http://www.lifehacker.co.in/"  target="_blank">Lifehacker</a> 
                        
                            <a href="http://in.ign.com/"  target="_blank">IGN</a> 
                        
                            <a href="http://boxtv.com"  target="_blank">BoxTV</a> 
                        
                            <a href="http://www.gaana.com"  target="_blank">Gaana</a> 
                        
                            <a href="http://shopping.indiatimes.com"  target="_blank">Online Shopping</a> 
                        
                            <a href="http://www.in.techradar.com/"  target="_blank">Techradar</a> 
                        
                            <a href="http://timesdeal.com"  target="_blank">Daily Deals</a> 
                        
                            <a href="http://www.gitanjalishop.com/"  target="_blank">Gitanjali Shop</a> 
                        
                            <a href="http://www.satvikshop.com/"  target="_blank">Satvik Shop</a> 
                        
                            <a href="http://whatshot.in"  target="_blank">WHAT'S HOT</a> 
                        
                            <a href="http://zigwheels.com"  target="_blank">ZigWheels</a> 
                        
                            <a href="http://filmipop.com"  target="_blank">FILMIPOP</a> 
                        
                            <a href="http://www.filmfare.com/"  target="_blank">Filmfare</a> 
                        
                            <a href="http://www.femina.in"  target="_blank">Femina</a> 
                        
                            <a href="http://mobile.indiatimes.com"  target="_blank">Mobile</a> 
                        
                            <a href="http://www.greetzap.com"  target="_blank">Greetings</a> 
                        
                            <a href="http://www.follo.co.in"  target="_blank">Follo</a> 
                        
                            <a href="http://timesjobs.com"  target="_blank">Jobs</a> 
                        
                            <a href="http://magicbricks.com"  target="_blank">Property</a> 
                        
                            <a href="http://www.astrospeak.com"  target="_blank">Astrology</a> 
                        
                            <a href="http://speakingtree.in"  target="_blank">Speaking Tree</a> 
                        
                            <a href="http://simplymarry.com"  target="_blank">Matrimonial</a> 
                        
                            <a href="http://itimes.com"  target="_blank">iTimes</a> 
                        
                            <a href="http://www.ads2book.com/"  target="_blank">Ads2book</a> 
                        
                            <a href="http://www.tcnext.com/"  target="_blank">TcNext</a> 
                        
                            <a href="http://www.romedynow.com"  target="_blank">Romedy Now</a> 
                                            </div>
                </div><!-- container closed-->
                <div class="cont border_none"><!-- container start-->
                    <h2>About us</h2>
                    <div class="sub_link">
                                                    <a href='http://www.indiatimes.com/privacypolicy' >Privacy Policy</a> 
                                                    <a href='http://www.indiatimes.com/sitemap.html' >sitemap</a> 
                                                    <a href='http://www.indiatimes.com/termsandcondition' >Terms & CONDITIONS</a> 
                                                    <a href='http://www.indiatimes.com/seoarchive' >ARCHIVES</a> 
                                                    <a href='http://www.indiatimes.com/contactus' >Contact us</a> 
                                                    <a href='http://www.indiatimes.com/aboutus' >ABOUT INDIATIMES</a> 
                                            </div>
                </div><!-- container closed-->
            </div><!-- footer grey closed-->
        </div><!-- footer black closed-->

        <div class="black_footer"><!-- footer black start-->
            <div class="containers">
                <p><a href="javascript:void(0)" class="copyrt">Copyright Â© 2015 Times Internet Limited. Powered by Indiatimes Lifestyle Network. All rights reserved</a></p>
                <span><a href="http://www.indiatimes.com/seoarchive/">Archive</a> | <a href="http://www.indiatimes.com/sitemap.html">Sitemap</a> | <a href="http://www.indiatimes.com/aboutus/">About Us</a> | <a href="http://www.indiatimes.com/contactus/">Contact Us</a> | <a href="http://www.indiatimes.com/privacypolicy/">Privacy Policy</a> | <a target="_blank" href="http://m.indiatimes.com">Indiatimes Mobile</a> | <a href="http://www.indiatimes.com/termsandcondition/">Terms of Use and Grievance Redressal Policy </a> </span> 
            </div>
        </div><!-- footer black end-->
    </footer>
</div>
<script>
    $( document ).ready(function() {
        $("#subscribers_id").click(function(){
            $("#UserEmail").focus();
        });
    });
    
</script>

    <script  type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.lazyload.min.js?v=100.52" charset="utf-8"></script>
<script defer type="text/javascript" src="http://media.indiatimes.in/resources/js/vendor/jquery.easing.1.3.min.js?v=100.52"></script>

<input type="hidden" name="currentId" id="currentId" value="">
    <div class="animate_cont dawn floating delay5">
        <span class="animate_arrow sprite_image"></span>
    </div>



<script type="text/javascript" src="http://media.indiatimes.in/resources/js/main.js?v=100.52"></script>    
<script type="text/javascript" src="http://media.indiatimes.in/resources/js/common.js?v=100.52"></script>
<script  defer type="text/javascript" src="http://media.indiatimes.in/resources/js/jquery-scrolltofixed.js?v=100.52"></script>

<script type="text/javascript">document.write(unescape("%3Cscript src='" + (("https:" == document.location.protocol) ? "https" : "http") + "://cdn.mouseflow.com/projects/1e238441-c30e-4d1b-8edd-c0f14d8fa6b7.js' type='text/javascript'%3E%3C/script%3E"));</script>
<!-- mouse flow code ends-->
<script  src="http://tags.crwdcntrl.net/c/2818/cc.js?ns=_cc2818" id="LOTCC_2818"></script> 

<script> 
    _cc2818.bcp(); 
</script>

	 <p id="back-top" style="display:none;"><a href="#top"><span></span></a> </p>
</div><!--wrap end-->


</body>
</html>